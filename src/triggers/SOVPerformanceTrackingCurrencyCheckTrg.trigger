trigger SOVPerformanceTrackingCurrencyCheckTrg on Performance_Tracking__c (before insert, before update) {
    
    TriggerDispatcher.run(new AllSOVPerfTrackingCurrencyCheckHandler());
    
}