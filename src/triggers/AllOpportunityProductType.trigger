trigger AllOpportunityProductType on Opp_Product_Type__c (after delete, after insert, after update, before delete, before insert, before update){
		TriggerDispatcher.run(new AllOpportunityProductTypeHandler());
    
}