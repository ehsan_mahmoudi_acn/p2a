/**
        @author - Accenture/Sanjeev
        @date - 05-Oct-2017
        @description - This Trigger is created as a replacement of AssignGroup and CloneUserToAgent
    */  

trigger AllUserTrigger on User (before insert, before update, before delete,after insert, after update, after delete, after undelete){
    TriggerDispatcher.run(new AllUserTriggerHandler());
}