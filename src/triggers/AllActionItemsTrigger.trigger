trigger AllActionItemsTrigger on Action_Item__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerDispatcher.run(new AllActionItemTriggerHandler());   
}