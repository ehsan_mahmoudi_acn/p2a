trigger AllSubscriptionTrigger on csord__Subscription__c (before insert, before update, before delete,after update, after delete, after undelete){
		TriggerDispatcher.run(new AllSubscriptionTriggerHandler());
}