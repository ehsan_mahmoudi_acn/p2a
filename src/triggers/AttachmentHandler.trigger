trigger AttachmentHandler on Attachment (before insert) {
   TriggerDispatcher.run(new AttachmentTriggerHandler());
}