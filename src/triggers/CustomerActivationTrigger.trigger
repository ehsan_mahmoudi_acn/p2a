trigger CustomerActivationTrigger on Customer_Activation_Test__c (before insert, before update, after insert, after update){
      TriggerDispatcher.run(new CustomerActivationClassHandler());
}