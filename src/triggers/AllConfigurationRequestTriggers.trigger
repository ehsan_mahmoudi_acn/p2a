trigger AllConfigurationRequestTriggers on csbb__Product_Configuration_Request__c(before insert, after insert, after update) {
    TriggerDispatcher.run(new AllConfigurationRequestTriggerHandler());
}