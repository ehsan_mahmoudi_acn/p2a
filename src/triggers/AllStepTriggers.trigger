trigger AllStepTriggers on CSPOFA__Orchestration_Step__c (before update, after update) {
	TriggerDispatcher.run(new AllStepTriggerHandler());
}