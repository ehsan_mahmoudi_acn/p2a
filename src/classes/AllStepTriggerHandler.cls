public with sharing class AllStepTriggerHandler extends BaseTriggerHandler{
    public override void beforeUpdate(List<SObject> newSteps, Map<Id,SObject> newStepsMap, Map<Id,SObject> oldStepsMap) {
  if(checkRecursive.stepsBeforeUpdate()){
      System.debug('steps before update');
        Set<Id> stepIds = ((Map<Id, cspofa__Orchestration_Step__c>)newStepsMap).keyset();
        Map<Id, cspofa__Orchestration_Step__c> updatedStepMap = new Map<Id, cspofa__Orchestration_Step__c> ();

        Map<Id, cspofa__Orchestration_Step__c> step2ProcessMap = new Map<Id, cspofa__Orchestration_Step__c>();
        List<CSPOFA__Orchestration_Step__c> steps = [SELECT Id, cspofa__Orchestration_Process__c, CSPOFA__Orchestration_Process__r.Order__c, CSPOFA__Orchestration_Process__r.csordtelcoa__Service__c,CSPOFA__Orchestration_Process__r.Order__r.RAG_Status__c,CSPOFA__Orchestration_Process__r.csordtelcoa__Service__r.RAG_Status__c, CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c, CSPOFA__Orchestration_Step_Template__r.Estimated_Time_To_Complete__c 
                                                     FROM cspofa__Orchestration_Step__c 
                                                     WHERE Id in :stepIds];
        for (CSPOFA__Orchestration_Step__c step : steps){
            step2ProcessMap.put(step.Id, step);
        }

        for (Id stepId : stepIds){
            if (ProcessStepTimeManager.IncludeInPathView((step2ProcessMap).get(stepId))){
                CSPOFA__Orchestration_Step__c step = ((Map<Id, cspofa__Orchestration_Step__c>)newStepsMap).get(stepId);
                CSPOFA__Orchestration_Step__c stepdata = step2ProcessMap.get(stepId);
                
                if (step.CSPOFA__Status__c != ((Map<Id, cspofa__Orchestration_Step__c>)oldStepsMap).get(stepId).CSPOFA__Status__c){
                    if (step.CSPOFA__Status__c == 'In Progress'){
                        if (stepdata.CSPOFA__Orchestration_Process__r.Order__c != null){
                            step.RAG_Status__c = stepdata.CSPOFA__Orchestration_Process__r.Order__r.RAG_Status__c;
                        }
                        else if (stepdata.CSPOFA__Orchestration_Process__r.csordtelcoa__Service__c != null){
                            step.RAG_Status__c = stepdata.CSPOFA__Orchestration_Process__r.csordtelcoa__Service__r.RAG_Status__c;
                        }
                    }
                    if (step.CSPOFA__Status__c == 'Complete')           {
                        step.RAG_Status__c = 'Green';
                    }
                }
            }
        }
    }
    }
    
    public override void afterUpdate(List<SObject> newSteps, Map<Id, SObject> newStepsMap, Map<Id, SObject>  oldStepsMap) {
      if(checkRecursive.stepsAfterUpdate()){
    System.debug('steps After update');
        Map<Id, cspofa__Orchestration_Step__c> updatedStepMap = new Map<Id, cspofa__Orchestration_Step__c> ();
        Map<Id, cspofa__Orchestration_Process__c> updatedProcessMap = new Map<Id, cspofa__Orchestration_Process__c> ();
        List<cspofa__Orchestration_Step__c> updatedSteps = new List<cspofa__Orchestration_Step__c>();
        List<cspofa__Orchestration_Process__c> updatedProcesses = new List<cspofa__Orchestration_Process__c>();
        Set<Id> processIds = new Set<Id>();
        for (cspofa__Orchestration_Step__c step : (List<cspofa__Orchestration_Step__c>)newSteps) {
            processIds.add(step.CSPOFA__Orchestration_Process__c);
        }
        Map<Id, CSPOFA__Orchestration_Process__c> processMap = new Map<Id, CSPOFA__Orchestration_Process__c>(
            [SELECT Id, 
                Start_Date_Time__c, 
                End_Date_Time__c, 
                Estimated_Time_To_Complete__c,
                Order__c, 
                Order__r.Customer_Required_Date__c, 
                (
                    SELECT 
                     Id, 
                     Name, 
                     CSPOFA__Orchestration_Process__r.Id,
                     CSPOFA__Orchestration_Step_Template__r.Name,
                     CSPOFA__Orchestration_Step_Template__r.Estimated_Time_To_Complete__c,
                     CSPOFA__Orchestration_Step_Template__r.CSPOFA__OLA_Unit__c,
                     CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c,
                     Start_Date_Time__c,
                     End_Date_Time__c,
                     CSPOFA__Status__c,
                     Estimated_Time_To_Complete__c,
                     OLA__c
                     FROM 
                     cspofa__Orchestration_Steps__r
                )
            FROM 
                CSPOFA__Orchestration_Process__c 
            WHERE 
                Id in :processIds
        ]);
        Set<Id> orderIds = new Set<Id>();
        Map<Id, List<cspofa__Orchestration_Process__c>> serviceProcesMap = new Map<Id, List<cspofa__Orchestration_Process__c>>();
        Map<Id, List<CSPOFA__Orchestration_Step__c>> serviceStepMap = new Map<Id, List<CSPOFA__Orchestration_Step__c>>();
         
        for (CSPOFA__Orchestration_Process__c process : processMap.values()) {
            if (process.Order__c != null) {
                orderIds.add(process.Order__c);
            }
        }
        
        if (!orderIds.isEmpty()) {
          Map<Id, csord__Service__c> services = new Map<Id, csord__Service__c>([SELECT 
              Id, 
              csord__Subscription__r.csord__order__c 
              FROM 
              csord__Service__c 
              WHERE 
              csord__Subscription__r.csord__Order__c in :orderIds
          ]);
          List <CSPOFA__Orchestration_Process__c> serviceProcesses = null;
          List <CSPOFA__Orchestration_Step__c> serviceSteps = null;
          List<Id> serviceIds = new List<Id> ();
          for (csord__Service__c service : services.values()) {
              serviceIds.add(service.Id);
          }
          serviceProcesses = OrderAndServiceFactory.GetServiceProcessesFromIds(serviceIds);
          serviceSteps = OrderAndServiceFactory.GetProcessStepsFromProcesses(serviceProcesses); 
          
          Map<Id, cspofa__Orchestration_Process__c> procMap = new Map<Id, cspofa__Orchestration_Process__c>();
          for (cspofa__Orchestration_Process__c process : serviceProcesses) {
              Id orderId = process.csordtelcoa__Service__r.csord__Subscription__r.csord__Order__r.Id;
              if (serviceProcesMap.get(orderId) == null) {
                  serviceProcesMap.put(orderId, new List<cspofa__Orchestration_Process__c>());
              }
              procMap.put(process.Id, process);
              serviceProcesMap.get(orderId).add(process);
          }
          for (CSPOFA__Orchestration_Step__c step : serviceSteps) {
              cspofa__Orchestration_Process__c process = procMap.get(step.CSPOFA__Orchestration_Process__r.Id);
              Id orderId = process.csordtelcoa__Service__r.csord__Subscription__r.csord__Order__r.Id;
              if (serviceStepMap.get(orderId) == null) {
                  serviceStepMap.put(orderId, new List<CSPOFA__Orchestration_Step__c>());
              }
              serviceStepMap.get(orderId).add(step);
          }
        }
        

        
        
        for (cspofa__Orchestration_Step__c step : (List<cspofa__Orchestration_Step__c>)newSteps){
            if (((Map<Id, cspofa__Orchestration_Step__c>)oldStepsMap).get(step.Id).CSPOFA__Status__c != step.CSPOFA__Status__c){
                System.debug('**** AllStepTriggerHandler newMap.get(stepId) CSPOFA__Status__c: ' + step);
                cspofa__Orchestration_Process__c process = processMap.get(step.CSPOFA__Orchestration_Process__c);
                List <CSPOFA__Orchestration_Process__c> sProcesses = new List <CSPOFA__Orchestration_Process__c>();
                List <CSPOFA__Orchestration_Step__c> sSteps = new List <CSPOFA__Orchestration_Step__c>();
                if (process.Order__c != null) {
                    sProcesses = serviceProcesMap.get(process.Order__c);
                    sSteps = serviceStepMap.get(process.Order__c);
                }

                if (step.CSPOFA__Status__c == 'In Progress'){
                    System.debug('**** AllStepTriggerHandler In Progress: ' + step);
                    if (((Map<Id, cspofa__Orchestration_Step__c>)oldStepsMap).get(step.Id).Start_Date_Time__c < DateTime.Now()){
               
                        ProcessStepTimeManager.UpdatePathWithTimeBulkified(updatedProcessMap, updatedStepMap, step, DateTime.now(), true, processMap.get(step.CSPOFA__Orchestration_Process__c), sProcesses, sSteps, processMap.get(step.CSPOFA__Orchestration_Process__c).cspofa__Orchestration_Steps__r);
                    }
                }
                if (step.CSPOFA__Status__c == 'Complete'){
                    System.debug('**** AllStepTriggerHandler Complete: ' + step);
                    ProcessStepTimeManager.UpdatePathWithTimeBulkified(updatedProcessMap, updatedStepMap, step, DateTime.now(), false, processMap.get(step.CSPOFA__Orchestration_Process__c), sProcesses, sSteps, processMap.get(step.CSPOFA__Orchestration_Process__c).cspofa__Orchestration_Steps__r);
                }
            }
        }
        
        for (Id stepId : updatedStepMap.keyset()){
            updatedSteps.add(updatedStepMap.get(stepId));
        }
        for (Id processId : updatedProcessMap.keyset()){
            updatedProcesses.add(updatedProcessMap.get(processId));
        }
        
        update updatedSteps;
        update updatedProcesses;
    }
  }
}