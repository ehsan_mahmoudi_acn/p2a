public class QueueableChainedJob implements Queueable {
    private IWorker m_worker;
    private Object m_params;
    private QueueableChainedJob m_next;
    
    public QueueableChainedJob(IWorker worker, Object params) {
        this.m_worker = worker;
        this.m_params = params;
    }
    
    public void setNext(QueueableChainedJob next) {
        this.m_next = next;
    }
    
    public void execute(QueueableContext context) {
        this.m_worker.work(this.m_params, context);
        if(this.m_next != null) {
            Id jobsIds = system.enqueueJob(this.m_next);

            Set<Id> alreadyExecutedIds = new Set<Id>();
            csord__Service__c service = new csord__Service__c();
            csord__Order__c order = new csord__Order__c();
            String sObjName;
			
			for(Id objectId :(List<Id>)this.m_params){
				alreadyExecutedIds.add(objectId);
			}			
			
            if(!alreadyExecutedIds.IsEmpty() && CheckRecursive.canExecuteUpdate(alreadyExecutedIds)){
                for(Id objectId :alreadyExecutedIds){
                    sObjName = objectId.getSObjectType().getDescribe().getName();
                }
				
                if(sObjName == 'csord__Service__c'){
                    service = [Select Solutions__c, csord__Order__c from csord__Service__c where Id IN :alreadyExecutedIds Limit 1];
                } else if(sObjName == 'csord__Order__c'){
                    order = [Select Solutions__c from csord__Order__c where Id IN :alreadyExecutedIds Limit 1];
                }

				Attachment newAttachement = new Attachment();
				
				if(service.Solutions__c != null){
					newAttachement.ParentId = service.Solutions__c;
					newAttachement.Name = 'AsyncApexJobID_OrchestrationProcessCreation-' +service.csord__Order__c +'-' +jobsIds;
					newAttachement.body = Blob.valueOf(newAttachement.Name);
				} else if(order.Solutions__c != null){
					newAttachement.ParentId	 = order.Solutions__c;
					newAttachement.Name = 'AsyncApexJobID_OrchestrationProcessCreation-' +order.Id +'-' +jobsIds;
					newAttachement.body = Blob.valueOf(newAttachement.Name);
				}
				insert newAttachement;
            }
        }
    }
}