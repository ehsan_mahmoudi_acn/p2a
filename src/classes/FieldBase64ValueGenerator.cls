public class FieldBase64ValueGenerator implements FieldValueGenerator {
    
    public Boolean canGenerateValueFor(Schema.DescribeFieldResult fieldDesc) {
        return Schema.DisplayType.BASE64 == fieldDesc.getType();
    }
    
    public Object generate(Schema.DescribeFieldResult fieldDesc) {
        return Blob.valueOf('This is a test');
    }
}