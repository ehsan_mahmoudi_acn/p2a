@IsTest
public class CS_BatchOrderGenerationObserverTest {
    @TestSetup
    static void setup() {
       // P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        
        List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(2,orderRequestList);
         csord__Subscription__c sub = new csord__Subscription__c();
                sub.Name ='Test'; 
                sub.csord__Identification__c = 'Test-Catlyne-4238362';
                sub.csord__Order_Request__c =orderRequestList[0].id;
                
                sub.OrderType__c = null;
        //csord__Subscription__c subt = new csord__Subscription__c();
        sub.Customer_Required_Date__c = system.today();
        //sub.Max_CRD__c = system.today()+5;
        insert sub;
        subList.add(sub);
        system.assertnotEquals(sub.Customer_Required_Date__c,sub.Max_CRD__c);
       // system.assertnotEquals(sub.Customer_Required_Date__c,null);
          //system.assertnotEquals(sub.Max_CRD__c,null);

        List<csord__Service__c> serList = P2A_TestFactoryCls.getServiceHdlr(3,orderRequestList, subList,prodBaskList);
        
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(3,orderRequestList);
       csord__Order__c ord1 = new csord__Order__c();
                ord1.csord__Identification__c = 'Test-JohnSnow-4238362';
                ord1.RAG_Order_Status_RED__c = false;
                ord1.RAG_Reason_Code__c = '';
                ord1.Jeopardy_Case__c = null;
                ord1.csord__Order_Request__c = orderRequestList[0].id;
                ord1.Customer_Required_Date__c = System.today();
                insert ord1;
                Orders.add(ord1);
                
               
        
        List<csord__Service_Line_Item__c> serLineItemList = P2A_TestFactoryCls.getSerLineItem(1,serList,orderRequestList);
       // P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
         
    }
    
     @IsTest
    static void testObserver2() 
    {
    
        List<Id> objIds = new List<Id>();
        for (csord__Subscription__c srv :  [select id from csord__Subscription__c]) {
            objIds.add(srv.Id); 
        }
        CS_BatchOrderGenerationObserver csbog = new CS_BatchOrderGenerationObserver();
        
        Test.startTest();
        csbog.processSubscriptions(objIds);
        
        Test.stopTest();
        list<csord__Subscription__c> srvli=[select id,Customer_Required_Date__c,Max_CRD__c from csord__Subscription__c];
        system.assertEquals(srvli[2].Customer_Required_Date__c,srvli[2].Max_CRD__c);
        
    }
    
    @IsTest
    static void testObserver3() 
    {
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        accList[0].Account_Manager__c = 'testname';
        update accList ;
        
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
         
        List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1); 
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(3,orderRequestList);
        Orders[0].csord__Account__c = accList[0].id;
        update Orders ;
        
      List<User> Userslist = new List<User>(); 
      profile pr = [select id from profile where name='System administrator'];
      User u = new User();
      u.Alias = 'standt';
      u.Email= 'standarduser@testorg.com';
      u.EmailEncodingKey = 'UTF-8';
      u.LastName = 'Testing1';
      u.LocaleSidKey = 'en_US';
      u.ProfileId = pr.Id;
      u.TimeZoneSidKey = 'America/Los_Angeles';
      u.LanguageLocaleKey='en_US';
      u.UserName = 'standarduser123456@testorg.com';
      u.EmployeeNumber  = '1223';
      Userslist.add(u);
      insert Userslist ;
      
       Orders[0].Account_Manager_PC__c = Userslist[0].id;
       update Orders;
         
        List<Id> objIds = new List<Id>();
        for (csord__Order__c srv :  [select id from csord__Order__c]) {
            objIds.add(srv.Id); 
        }
        CS_BatchOrderGenerationObserver csbog = new CS_BatchOrderGenerationObserver();
        
        Test.startTest();
        csbog.processOrders(objIds);
        Test.stopTest();
         list<csord__Order__c> order =[select id,Customer_Required_Date__c,Max_CRD__c,Account_Manager__c,Account_Manager_Contact_Number__c,Account_Manager_PC__c ,Account_Owner_ID__c from csord__Order__c where id in:objIds];
         //system.asserEquals(testname,order[0].Account_Manager__c);
         System.assertEquals(order[0].Account_Manager_PC__c ,order[0].Account_Owner_ID__c );
        system.assertEquals(order[2].Customer_Required_Date__c,order[2].Max_CRD__c);
       
    }
    
    @IsTest
    static void testObserver() {
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        
        List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(2,orderRequestList);
        
        List<Solutions__c> solution1 = new List<Solutions__c>();
        Solutions__c solu1 = new Solutions__c ();
        solu1.Account_Name__c = accList[0].id;
        solu1.Batch_Count__c = 14;
        solu1.Opportunity_Name__c = oppList[0].id;
        solu1.Product_Basket__c = prodBaskList[0].id;
        insert solu1;
        solution1.add(solu1);
        
        prodBaskList[0].Solutions__c = solution1[0].id;
       update prodBaskList ;
       
       oppList[0].Solutions__c = solution1[0].id;
       update oppList ;
        
        List<csord__Service__c> serList = P2A_TestFactoryCls.getServiceHdlr(3,orderRequestList, subList,prodBaskList);
        
        serList[0].Opportunity__c = oppList[0].id; 
       update serList ;
        
        List<Id> objIds = new List<Id>();
        for (csord__Service__c srv : [select id from csord__Service__c where Opportunity__c in:oppList]) {
            objIds.add(srv.Id); 
        }
        CS_BatchOrderGenerationObserver csbog = new CS_BatchOrderGenerationObserver();
        
        Test.startTest();
        csbog.processServices(objIds);
        Test.stopTest();
        List<csord__Service__c> service1 = [select id,name,Full_Termination_Order__c,Solutions__c  from csord__Service__c where id In:objIds];
       // system.assertEquals(false,service1[0].Full_Termination_Order__c);
        system.assertEquals(solution1[0].id,service1[0].Solutions__c);
       // System.assertEquals(service1[0].name,'Test Service');
        system.assertEquals(true,csbog!=null);
        
    }
    
   
}