public with sharing class AllSubscriptionTriggerHandler extends BaseTriggerHandler {
    
    @testvisible
    private static final String SUBSCRIPTION_ORCHESTRATOR_PROCESS_NAME = 'Subscription';

    @testvisible
    private static final String ORCHESTRATIONS_PROCESS_NEW = 'New';
    
    private static boolean setOppIdFlag = true;
    
    public AllSubscriptionTriggerHandler() {
        super();
        allowRecursion();
    }

    public override Integer getMaxRecursion(){
        return 2; //Will allow code to run 2 times
    }
    
    public override Boolean isDisabled(){
        if(TriggerFlags.NoSubscriptionTriggers){
            return true;
        }
        return triggerDisabled;
    }
    
    /**
     * Before Insert handler
     */   
    public override void beforeInsert(List<SObject> newSubscriptions){
        updateCreatedByField((List<csord__Subscription__c>) newSubscriptions);
        /**
         * As part of the Winter17(CR26), all methods of the beforeInsert has been moved to "CS_BatchOrderGenerationObserver" class.
         * If any method call requires an udpate on beforeInsert, the method should be called from the "CS_BatchOrderGenerationObserver" class.
         */
    } 
    
    /**
     * After Insert handler
     */
    public override void afterInsert(List<SObject> newSubscriptions, Map<Id, SObject> newSubscriptionsMap){
        /**
         * As part of the Winter17(CR26), all methods of the afterInsert has been moved to "CS_BatchOrderGenerationObserver" class.
         * If any method call requires an udpate on afterInsert, the method should be called from the "CS_BatchOrderGenerationObserver" class.
         */
    }
    
    /**
     * Before update handler
     */
    public override void beforeUpdate(List<SObject> newSubscriptions, Map<Id,SObject> newSubscriptionsMap, Map<Id,SObject> oldSubscriptionsMap){
        subCurrencymtd((List<csord__Subscription__c>) newSubscriptions,(Map<Id,csord__Subscription__c>) oldSubscriptionsMap);
        setCustomerRequiredDate((List<csord__Subscription__c>) newSubscriptions);
        changeSubscriptionOwner((List<csord__Subscription__c>) newSubscriptions);
        
    }
    
    /**
     * After update trigger
     */
    public override void afterUpdate(List<SObject> newSubscriptions, Map<Id, SObject> newSubscriptionsMap, Map<Id, SObject>  oldSubscriptionsMap){
        updateOrders((List<csord__Subscription__c>) newSubscriptions,(Map<Id,csord__Subscription__c>) oldSubscriptionsMap);
    }

    /**
     * Update CreatedBy field on the Subscription.
     * 
     * @param newSubscriptions the Subscriptions to update
     * Developer: Sapan, Nikola Culej
     * Fix for the ticket T-32625
     */
    public void updateCreatedByField(List<csord__Subscription__c> newSubscriptions){
        try{

            Map<Id, String> PcIdMap = new Map<Id, String>();
            Map<String, Id> orderCreator = new Map<String, Id>();

            for(csord__Subscription__c subs :newSubscriptions){
                if(subs.csord__Identification__c != null && subs.csordtelcoa__Change_Type__c != 'Terminate'){
                    PcIdMap.put(Id.ValueOf(subs.csord__Identification__c.split('_')[subs.csord__Identification__c.split('_').size()-2]), subs.csord__Identification__c);
                }
            }
            
            if(!PcIdMap.IsEmpty()){
                //List<cscfga__Product_Configuration__c> pcList = [Select Id, cscfga__Product_Basket__r.Order_Creator__c from cscfga__Product_Configuration__c where Id IN :PcIdMap.KeySet()];
                
                for(cscfga__Product_Configuration__c pc :[Select Id, cscfga__Product_Basket__r.Order_Creator__c from cscfga__Product_Configuration__c where Id IN :PcIdMap.KeySet()]){
                    if(PcIdMap.get(pc.Id) != null){
                        orderCreator.put(PcIdMap.get(pc.Id), pc.cscfga__Product_Basket__r.Order_Creator__c);
                    }
                }
                
                for(csord__Subscription__c subs :newSubscriptions){
                    if(subs.csord__Identification__c != null && orderCreator.get(subs.csord__Identification__c) != null){
                        subs.createdById = orderCreator.get(subs.csord__Identification__c);
                    }
                }
            }
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='AllSubscriptionTriggerHandler:updateCreatedByField';
            ErrorHandlerException.objectList=newSubscriptions;
            ErrorHandlerException.sendException(e);
            System.debug('Error: "AllSubscriptionTriggerHandler; updateCreatedByField" mehotd update failure - ' +e);
         }      
    }
    
    /**
     * Updates the orders linked to the given subscriptions.
     * 
     * Note: the orders are updated in DB
     * 
     * @params newSubscriptions the subscriptions linke to the orders to update
     */
    @testvisible
    public List<csord__Subscription__c> updateOrders(List<csord__Subscription__c> newSubscriptions,Map<Id,csord__Subscription__c> oldSubscriptionsMap){
        Map<Id, csord__Subscription__c> orderMap = new Map<Id, csord__Subscription__c>();
        
        for (csord__Subscription__c sub : newSubscriptions){
           if(oldSubscriptionsMap==null && sub.csord__Order__c!=null){
               orderMap.put(sub.csord__Order__c, sub);//in case of insert trigger event
           }
           
           else if(oldSubscriptionsMap!=null && sub.csord__Order__c!=null && 
           (   sub.All_Services_Ready__c!=oldSubscriptionsMap.get(sub.id).All_Services_Ready__c
            || sub.Appointment_Arranged__c!=oldSubscriptionsMap.get(sub.id).Appointment_Arranged__c
            || sub.RAG_Status_Amber__c!=oldSubscriptionsMap.get(sub.id).RAG_Status_Amber__c
            || sub.RAG_Status_Red__c!=oldSubscriptionsMap.get(sub.id).RAG_Status_Red__c
            || sub.Ready_For_Deployment__c!=oldSubscriptionsMap.get(sub.id).Ready_For_Deployment__c
            || sub.Ready_Services__c!=oldSubscriptionsMap.get(sub.id).Ready_Services__c
            || sub.Subscription_Clean__c!=oldSubscriptionsMap.get(sub.id).Subscription_Clean__c
            || sub.Count_of_services__c!=oldSubscriptionsMap.get(sub.id).Count_of_services__c
            || sub.Ready_For_Deployment__c!=oldSubscriptionsMap.get(sub.id).Ready_For_Deployment__c
            || sub.Count_of_Service_Decomissioned__c!=oldSubscriptionsMap.get(sub.id).Count_of_Service_Decomissioned__c
            || sub.Count_of_Service_not_provisioned__c!=oldSubscriptionsMap.get(sub.id).Count_of_Service_not_provisioned__c
            || sub.order_type__c!=oldSubscriptionsMap.get(sub.id).order_type__c
            || sub.HasOffnetService__c!=oldSubscriptionsMap.get(sub.id).HasOffnetService__c
           )){           
              orderMap.put(sub.csord__Order__c, sub);//in case of update trigger event
           }
        }
        
        if (orderMap.isEmpty()){
            //Nothing to update
            return newSubscriptions;
        }
        else{
        
        List<csord__Order__c> orders = [SELECT Id, Name, All_Services_Ready__c, Appointment_Arranged__c, 
                                        RAG_Status_Amber__c, RAG_Status_Red__c, Ready_For_Deployment__c, 
                                        Ready_Services__c, Subscription_Clean__c, Latest_Integration_Update_Comment__c, 
                                        Latest_Integration_Update_Date__c, Latest_Integration_Update_Message__c,
                                        Latest_Integration_Update_Name__c, Latest_Integration_Update_Status__c,
                                        Customer_Required_Date__c, RAG_Reason_Code__c, Count_of_services__c,
                                        Count_of_Service_Decomissioned__c, Count_of_Service_provisioned__c,
                                        HasOffnetServices__c,order_type__c
                                        FROM csord__Order__c
                                        WHERE Id IN :orderMap.keySet()];
        
        for (csord__Order__c order :orders){
            csord__Subscription__c sub = orderMap.get(order.Id);
            
            order.All_Services_Ready__c             = sub.All_Services_Ready__c;
            order.Appointment_Arranged__c           = sub.Appointment_Arranged__c;
            order.RAG_Status_Amber__c               = sub.RAG_Status_Amber__c > 0 ? true : false;
            order.RAG_Status_Red__c                 = sub.RAG_Status_Red__c > 0 ? true : false;
            order.Ready_For_Deployment__c           = sub.Ready_For_Deployment__c;
            order.Ready_Services__c                 = sub.Ready_Services__c;
            order.Subscription_Clean__c             = sub.Subscription_Clean__c;
            order.Count_of_services__c              = sub.Count_of_services__c; 
            order.Count_of_Service_Decomissioned__c = sub.Count_of_Service_Decomissioned__c;
            order.Count_of_Service_provisioned__c   = sub.Count_of_Service_not_provisioned__c;
            order.order_type__c                     = sub.order_type__c;
            order.HasOffnetServices__c              = sub.HasOffnetService__c?true:order.HasOffnetServices__c;             
         }
           if(orders.size()>0){update orders;}
        
        return newSubscriptions;
        }
    }
    
    /**
     * Return a collection of processes.
     * 
     * Note: the processes are not saved in the DB.
     * 
     * @param processTemplateId the is of the template to use
     * @subscriptions a list of subscription for which the processes will be created
     * @return a List of processes
     */
    @testvisible
    private List<CSPOFA__Orchestration_Process__c> newProcesses(CSPOFA__Orchestration_Process_Template__c processTemplate,
                                                                   List<csord__Subscription__c> subscriptions) {                                                               
        List<CSPOFA__Orchestration_Process__c> processes = new List<CSPOFA__Orchestration_Process__c>();   
        for (csord__Subscription__c item : subscriptions) {
            CSPOFA__Orchestration_Process__c process = new CSPOFA__Orchestration_Process__c();
            process.csordtelcoa__Subscription__c = item.Id;
            process.Name = item.Name + '_' + ORCHESTRATIONS_PROCESS_NEW;
            process.CSPOFA__Orchestration_Process_Template__c = processTemplate.Id;
            processes.add(process);
        }                                                                
        return processes;
    }
    
    /**
     * Returns the process template for the given name, or null if not found.
     * 
     * @param processName the name of the process template
     * @return the process, or null if not found
     */
    @testvisible
    private CSPOFA__Orchestration_Process_Template__c findProcessTemplateByName(String processName) {
        List<CSPOFA__Orchestration_Process_Template__c> templates = [
            SELECT Id, Name 
            FROM CSPOFA__Orchestration_Process_Template__c 
            WHERE Name = :processName
            LIMIT 1];
        
        if (templates.isEmpty()) {
            return null;
        }
        return templates.get(0);
    }
    
    /**
     * Link the odrer details to the given subscriptions.
     * 
     * @param orderIds a collection of order Id
     */
    @testvisible
    private void addOrderDetails(List<csord__Subscription__c> subscriptions) {
        if (subscriptions == null || subscriptions.isEmpty()) {
            return;
        }
        
        Set<Id> orderIds = SObjectsUtil.getIds(subscriptions, 'csord__Order__c');
        
        if (orderIds.isEmpty()) { return; }
        
        Map<Id, csord__Order__c> orders = new Map<Id, csord__Order__c>([
            SELECT Id, csordtelcoa__Opportunity__c
            FROM csord__Order__c
            WHERE Id IN :orderIds]);
        
        for (csord__Subscription__c subscription: subscriptions) {
            subscription.csord__Order__r = orders.get(subscription.csord__Order__c);
        }
    }

    /**
     * MACD Functionality.
     * 
     * @param subs a collection of subs Id
     */
    @testvisible
    public List<csord__Service__c> mACDFunctionality(List<csord__Subscription__c> newSubscriptions) {
        List<csord__Subscription__c> subs = new List<csord__Subscription__c>(); 
        for(csord__Subscription__c sub : newSubscriptions){
            if(sub.Order_Type__c != 'New' 
                    && sub.csordtelcoa__Replaced_Subscription__c != null
                    && sub.csord__Order__c != null && !sub.Is_Inflight_Order__c){
                subs.add(sub);
            }
        }        
        return (new MACDHandler().afterInsertChangeOrder(subs));
    }

    /**
     * Code Modification: KD
     * Code Modification: 27th-Jan-2017
     * Cause of Code Change: FDR issue for too many rows.
     * Added null check condition for opportunity id value.
     * Added size check for OppID set.
     */    
     @testvisible
     public List<csord__subscription__c> subCurrencymtd(List<csord__subscription__c> newSubscriptions,Map<Id,csord__Subscription__c> oldSubscriptionsMap){
        Map<Id, String> pbMap = new Map<Id, String>();
        Set<Id> oppIds = new Set<Id>();
        
        for(csord__subscription__c cs :newSubscriptions){
            if(oldSubscriptionsMap==null && cs.OpportunityRelatedList__c != null){
                oppIds.add(cs.OpportunityRelatedList__c);//in case of insert trigger event
            }
            else if(oldSubscriptionsMap!=null && cs.OpportunityRelatedList__c != null && cs.OpportunityRelatedList__c!=oldSubscriptionsMap.get(cs.id).OpportunityRelatedList__c){
                oppIds.add(cs.OpportunityRelatedList__c);   //in case of update trigger event           
            }
        } 
        
        if(oppIds.size() > 0){
            /*List<cscfga__product_basket__c> pbList = new List<cscfga__product_basket__c>([Select currencyisocode, cscfga__Opportunity__c 
                                                                                     From cscfga__product_basket__c Where cscfga__Opportunity__c IN :oppIds]);*/ 
            for(cscfga__product_basket__c pb :[Select currencyisocode, cscfga__Opportunity__c 
                                                                                     From cscfga__product_basket__c Where cscfga__Opportunity__c IN :oppIds]){
                pbMap.put(pb.cscfga__Opportunity__c ,pb.currencyisocode);
            }

            for(csord__subscription__c cs :newSubscriptions){
                if(pbMap.Size() > 0){
                    if(pbMap.get(cs.OpportunityRelatedList__c) != null) {
                    cs.currencyisocode = pbMap.get(cs.OpportunityRelatedList__c);
                       } 
                }
            }
        }
        return newSubscriptions;
    }
    
    /**
     * Create Orchestration Process template for Services.
     * Developer: Sapan (CS provided code)
     */
    @testVisible
    private void changeOrchestrationProcessTemplate(List<csord__Subscription__c> newSubscriptions){   
        try{
            String processName = SUBSCRIPTION_ORCHESTRATOR_PROCESS_NAME + '_' + ORCHESTRATIONS_PROCESS_NEW;
            CSPOFA__Orchestration_Process_Template__c processTemplate = findProcessTemplateByName(processName);
          
            if(processTemplate == null){
                //No process template found, log and exit
                System.debug(System.LoggingLevel.WARN,
                             'No Process Template found for name: "' + processName + '"');
                return;
            }
            
            List<CSPOFA__Orchestration_Process__c> newProcessList = 
                newProcesses(processTemplate, (List<csord__Subscription__c>) newSubscriptions);

            if(!newProcessList.isEmpty()){
                insert newProcessList;
            }
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='AllSubscriptionTriggerHandler:changeOrchestrationProcessTemplate';
            ErrorHandlerException.objectList=newSubscriptions;
            ErrorHandlerException.sendException(e);
            system.debug('Error: "changeOrchestrationProcessTemplate" mehotd update failure - ' +e);
          }
    }   

    /**
     * Method to update Customer Required Date in the subscription.
     * Developer: Sapan
     */ 
    @testVisible
    public List<csord__Subscription__c> setCustomerRequiredDate(List<csord__Subscription__c> newSubscriptions){
        for(csord__subscription__c subs: newSubscriptions){
            if(subs.Customer_Required_Date__c != subs.Max_CRD__c && subs.Max_CRD__c != null)
                subs.Customer_Required_Date__c = subs.Max_CRD__c;
        }
        return newSubscriptions;
    }

    /**
     * Developer: Apurva Prastuti
     * Change order owner of subscription.
     * TGP0041763 - Sharing rule implementation
     */
    @testVisible
    public List<csord__Subscription__c> changeSubscriptionOwner(List<csord__Subscription__c> newSubscriptions){   
        try{
            
            for(csord__Subscription__c subs :newSubscriptions){
                if(subs.Account_Owner_ID__c != null && subs.Account_Owner_ID__c != subs.OwnerId){
                    subs.OwnerId = subs.Account_Owner_ID__c;
                }
            }
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='AllSubscriptionTriggerHandler:changeSubscriptionOwner';
            ErrorHandlerException.objectList=newSubscriptions;
            ErrorHandlerException.sendException(e);
            system.debug('Error: "changeSubscriptionOwner" mehotd update failure - ' +e);
          }
        return newSubscriptions;
    }
}