/**    @author - Accenture    
       @date - 25- Jun-2012    
       @version - 1.0    
       @description - This is the test class for ContractafterInsertafterUpdate Trigger
*/
@isTest
private class ContractafterInsertafterUpdateTest {

    static testMethod void testUpdateAccountField() {
        
        Contract__c contractObj = getContract();
        insert contractObj;
        system.assert(contractObj!=null);
        
    }
    
    // Creating the Test Data
    private static Contract__c getContract()
    {
       
       Contract__c contractObj1 = new Contract__c();
       Account accountObj1 = getAccount();
       contractObj1.Account__c = accountObj1.Id;
       contractObj1.Contract_Type__c = 'CRA';
       contractObj1.Status__c  = 'Active';
       contractObj1.Telstra_Contracting_Entity__c = 'Telstra Corporation Limited';
       contractObj1.Address_for_Legal_Notice__c = 'australia';
       contractObj1.Minimum_Spend_Conditions__c = 3;
       contractObj1.Payment_Terms__c = '15 Days';
       contractObj1.Initial_Period__c = '3';
       contractObj1.Customer_Signed_Date__c = System.today();
       contractObj1.Telstra_Signed_Date__c = System.today();
       contractObj1.Contract_Expiry_Date__c = System.today()+2;
       contractObj1.Contract_Effective_Date__c = System.today()+1;
       contractObj1.Tcorp_NDA__c = True;
       contractObj1.NDA_Signed__c = True;
       contractObj1.CIDN_T_Corp_Only__c = 'textarea';
       
       system.assert(contractObj1!=null);
       
       return contractObj1;
       
    }
    
    private static Account getAccount(){        
        Account acc = new Account();        
        Country_Lookup__c cl= getCountry();        
         acc.Name = 'Test Account';       
         acc.Customer_Type__c = 'MNC';        
         acc.Country__c = cl.Id;        
         acc.Selling_Entity__c = 'Telstra INC';  
         acc.Customer_Legal_Entity_Name__c='Test';    
         insert acc; 
         List<account> a = [Select id,name from account where name= 'Test Account']; 
         system.assertequals(acc.name,a[0].name);  
         system.assert(acc!=null);    
         return acc;    
         }
    
    private static Country_Lookup__c getCountry(){     
         Country_Lookup__c cl = new Country_Lookup__c();        
         cl.CCMS_Country_Name__c = 'India';      
         cl.Country_Code__c = 'IND';      
         insert cl; 
         List<Country_Lookup__c> c2 = [Select id,CCMS_Country_Name__c from Country_Lookup__c  where CCMS_Country_Name__c = 'India'];
         system.assertequals(cl.CCMS_Country_Name__c, c2[0].CCMS_Country_Name__c);
         system.assert(cl!= null);     
         return cl;    
         } 
}