/**
 * @name UpdateProcessStepEndDateTimeBatch 
 * @description framework class for batch processes
 * @revision
 * Boris Bjelan 08-03-2016 Created class
 */
public class UpdateProcessStepEndDateTimeBatch implements Database.Batchable<sObject> /*, Database.Stateful*/ {
    
    public UpdateProcessStepEndDateTimeBatch () {
        
    }
     
    public Database.QueryLocator start(Database.BatchableContext ctx) {
        return 
            Database.getQueryLocator([SELECT 
                Id, 
                Order__c, 
                CreatedDate
             FROM 
                cspofa__Orchestration_Process__c 
             WHERE 
                 Order__c <> null and CSPOFA__State__c = 'ACTIVE']);
    }
    
    public void execute(Database.BatchableContext ctx, List<cspofa__Orchestration_Process__c > processes) {
        Set<Id> orderIds = new Set<Id>();
        for (cspofa__Orchestration_Process__c process : processes) {
            orderIds.add(process.Order__c);
        }
        // collect all the steps
        List<OrderAndServices> orderAndServices = OrderAndServiceFactory.makeOrderAndServicesFromOrders(orderIds);
        List<cspofa__Orchestration_Step__c> stepsToCheck = new List<cspofa__Orchestration_Step__c>();
        for (OrderAndServices orderAndService: orderAndServices) {
            stepsToCheck.addAll(getStepsFromWrapper(orderAndService.m_orderProcessSteps));
            stepsToCheck.addAll(getStepsFromWrapper(orderAndService.m_serviceProcessSteps));
        }
		System.Debug('Checking steps '+stepsToCheck);
        // check what steps should be postponed 
        List<cspofa__Orchestration_Step__c> stepsToPostpone = new List<cspofa__Orchestration_Step__c>();
        for (cspofa__Orchestration_Step__c stepToCheck: stepsToCheck) {
            checkAndPostponeEndDateTime(stepsToPostpone, stepToCheck);
        }

        // postpone the steps and the update their processes
        Map<Id, cspofa__Orchestration_Step__c> stepsToUpdateMap = new Map<Id, cspofa__Orchestration_Step__c>();
        Map<Id, CSPOFA__Orchestration_Process__c> processesToUpdateMap = new Map<Id, CSPOFA__Orchestration_Process__c>();
        for (cspofa__Orchestration_Step__c stepToPostpone: stepsToPostpone) {
            ProcessStepTimeManager.UpdatePathWithTimeBulkified(processesToUpdateMap
                , stepsToUpdateMap
                , stepToPostpone
                , stepToPostpone.End_Date_Time__c.addDays(1)
                , false);
        }

        update processesToUpdateMap.values();
        update stepsToUpdateMap.values();
    }
    
    public static List<cspofa__Orchestration_Step__c> getStepsFromWrapper(Map<Id, List<CSPOFA__Orchestration_Step__c>> stepMap) {
        List<CSPOFA__Orchestration_Step__c> resultList = new List<CSPOFA__Orchestration_Step__c>();
        for (List<CSPOFA__Orchestration_Step__c> stepList: stepMap.values()) {
            resultList.addAll(stepList);
        }
        return resultList;
    }

    public static void checkAndPostponeEndDateTime(List<cspofa__Orchestration_Step__c> toUpdateList,  cspofa__Orchestration_Step__c stepToCheck) {
        Date today = System.today();
        if (stepToCheck.CSPOFA__Status__c == 'In Progress' && stepToCheck.End_Date_Time__c.date() < today)
        {
            toUpdateList.add(stepToCheck);
        }
        System.Debug('Steps to update = '+toUpdateList);
    }

    public void finish(Database.BatchableContext ctx) {
        
    }

    public static void executeIt(List<String> orderIds) {
        List<cspofa__Orchestration_Process__c> processes = new List<cspofa__Orchestration_Process__c>([SELECT Id
                , Order__c
                , CreatedDate
            FROM 
                cspofa__Orchestration_Process__c 
            WHERE 
                Order__c in: orderIds and CSPOFA__State__c = 'ACTIVE']);
        UpdateProcessStepEndDateTimeBatch bc = new UpdateProcessStepEndDateTimeBatch();  
        System.Debug('executeIt');
        bc.execute(null, processes);
    }

}