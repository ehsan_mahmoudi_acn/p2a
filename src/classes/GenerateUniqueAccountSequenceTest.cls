/**
This is the test class of GenerateUniqueAccountSequence
*/

@isTest
private class GenerateUniqueAccountSequenceTest {

    static Account a;
    static Account b;
    static Account c;
    static Account d;
    static UniqueIdStore__c Uis;
    static Country_Lookup__c cl;
    
    static testMethod void myUnitTest() {
        uis = getUniqueIdStore();
        a = getAccount();
        String accountId = a.Id;
        //GenerateUniqueAccountSequence.generateUnqiueAccountId(accountId,'BEC-30014');
        GenerateUniqueAccountSequence.generateUnqiueAccountId(accountId,'30014');
        system.assertEquals(true,accountId!=null); 
       
    }
    
    static testMethod void myUnitTest2() {
        uis = getUniqueIdStore();
        b = getAccount2();
        String accountId = b.Id;
        createAccount3();
        createAccount4();
        //GenerateUniqueAccountSequence.generateUnqiueAccountId(accountId,'BEC-50012');
        GenerateUniqueAccountSequence.generateUnqiueAccountId(accountId,'50012');
        system.assertEquals(true,accountId!=null); 
    }
    
    
    private static Account getAccount(){
        if(a == null){    
            a = new Account();
            Country_Lookup__c country = getCountry();
            a.Name = 'Test Account Id';
            a.Customer_Type__c = 'GSP';
            a.Country__c = country.Id;
            a.Selling_Entity__c = 'Telstra Telecommunications Private Limited';
            a.Account_Manager__c = 'Abhita Bansal';
            a.Industry__c= 'Media';
            a.Customer_Legal_Entity_Name__c='Test';
            //a.NewGenId__c= 'BEC-30014';
            insert a;
            system.assert(a!=null);
            List<Account> a2 = [Select id,name from Account where name = 'Test Account Id'];
            system.assertequals(a.name,a2[0].name);
        }
        return a;
    }
     private static Account getAccount2(){
        if(b == null){    
            b = new Account();
            Country_Lookup__c country = getCountry();
            b.Name = 'Test Account Id2';
            b.Customer_Type__c = 'GSP';
            b.Country__c = country.Id;
            b.Selling_Entity__c = 'Telstra Telecommunications Private Limited';
            b.Account_Manager__c = 'Abhita Bansal';
            b.Industry__c= 'Media';
            b.Customer_Legal_Entity_Name__c='Test';
            //a.NewGenId__c= 'BEC-30014';
            insert b;
            system.assert(b!=null);
        }
        return b;
    }
    private static void createAccount3(){
        if(c == null){    
            c = new Account();
            Country_Lookup__c country = getCountry();
            c.Name = 'Test Account Id3';
            c.Customer_Type__c = 'GSP';
            c.Country__c = country.Id;
            c.Selling_Entity__c = 'Telstra Telecommunications Private Limited';
            c.Account_Manager__c = 'Abhita Bansal';
            c.Industry__c= 'Media';
            c.Account_ID__c='50012';
            c.Customer_Legal_Entity_Name__c='Test';
            //a.NewGenId__c= 'BEC-30014';
            insert c;
            system.assert(c!=null);
        }
       
    }
    
     private static void createAccount4(){
        if(d == null){    
            d = new Account();
            Country_Lookup__c country = getCountry();
            d.Name = 'Test Account Id4';
            d.Customer_Type__c = 'GSP';
            d.Country__c = country.Id;
            d.Selling_Entity__c = 'Telstra Telecommunications Private Limited';
            d.Account_Manager__c = 'Abhita Bansal';
            d.Industry__c= 'Media';
            d.Account_ID__c='50013';
            d.Customer_Legal_Entity_Name__c='Test';
            //a.NewGenId__c= 'BEC-30014';
            insert d;
            List<Account> a = [Select id,name from Account where name = 'Test Account Id4'];
            system.assertequals(d.name,a[0].name);
            
            system.assert(d!=null);
            }
       
    }
    
    private static Country_Lookup__c getCountry(){
        if(cl == null){ 
            cl = new Country_Lookup__c();
            cl.CCMS_Country_Code__c = 'SG';
            cl.CCMS_Country_Name__c = 'India';
            cl.Country_Code__c = 'SG';
            insert cl;
            system.assert(cl!=null);
        }
        return cl;
    } 
       
    private static UniqueIdStore__c getUniqueIdStore(){
        if(uis == null){ 
            uis = new UniqueIdStore__c();
            uis.UniqueIdNum__c = 10.0;
            insert uis;
            system.assert(uis!=null);
        }
        return uis;
    }  
   
}