global with sharing class CS_PortRate_Internet extends cscfga.ALookupSearch {
    
    public override String getRequiredAttributes(){ 
        return '[ "Maximum Bandwidth excluding direct China","Direct China Committed Bandwidth","Committed Bandwidth excluding direct China" ,"Port Rating Type" ]';
    }
    
    /*public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){
        List <String> ProductTypeNames = new List<String>();
        
            for (String key : searchFields.keySet()){
                String attValue = searchFields.get(key);
                if(attValue == 'Yes' || attValue == 'true')
                ProductTypeNames.add(key);
            }
            String MDR = searchFields.get("Maximum Bandwidth excluding direct China");
            String ChinaBw = searchFields.get("Direct China Committed Bandwidth");
            String CommittedBw = searchFields.get("Committed Bandwidth excluding direct China");
            String PortRateType = searchFields.get("Port Rating Type");
            
        if(PortRateType == 'Flat'){
            List <CS_Bandwidth__c> committedbwdata = [SELECT Id, Name, Bandwidth_Value_MB__c FROM CS_Bandwidth__c where id =: CommittedBw];
            List <CS_Bandwidth__c> chinaBwdata = [SELECT Id, Name, Bandwidth_Value_MB__c FROM CS_Bandwidth__c where id =: ChinaBw];
            Integer Sum = committedbwdata[0].Bandwidth_Value_MB__c + chinaBwdata[0].Bandwidth_Value_MB__c;
            System.debug('SUM+++++++++++'+Sum);
            List <CS_Bandwidth__c> data = [SELECT Id, Name, Bandwidth_Value_MB__c FROM CS_Bandwidth__c where Bandwidth_Value_MB__c > Sum];
            System.debug('DATAIF+++++++++++'+data);
            
        }
        
        if(PortRateType != 'Flat'){
            //List <CS_Bandwidth__c> committedbwdata = [SELECT Id, Name, Bandwidth_Value_MB__c FROM CS_Bandwidth__c where id =: CommittedBw];
            List <CS_Bandwidth__c> mdrBwdata = [SELECT Id, Name, Bandwidth_Value_MB__c FROM CS_Bandwidth__c where id =: MDR];
            //Integer Sum = committedbwdata[0].Bandwidth_Value_MB__c + chinaBwdata[0].Bandwidth_Value_MB__c;
            System.debug('SUM+++++++++++'+Sum);
            List <CS_Bandwidth__c> data = [SELECT Id, Name, Bandwidth_Value_MB__c FROM CS_Bandwidth__c where Bandwidth_Value_MB__c >= mdrBwdata[0].Bandwidth_Value_MB__c);
            System.debug('DATAELSE+++++++++++'+data);
            
        }    
        return data;
    }*/
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
    Integer pageOffset, Integer pageLimit){
        
        
       /* final Integer SELECT_LIST_LOOKUP_PAGE_SIZE = 25;
        final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = 26;
        Integer recordOffset = pageOffset * SELECT_LIST_LOOKUP_PAGE_SIZE;
        
          String aEndCountry = searchFields.get('A-End Country');
        Set <Id> bwIds = new Set<Id>();
        List<CS_Bandwidth__c> bandlist = new List<CS_Bandwidth__c>();
        
        List <String> ProductTypeNames = new List<String>();
        
        for (String key : searchFields.keySet()){
            String attValue = searchFields.get(key);
            if(attValue == 'Yes' || attValue == 'true')
            ProductTypeNames.add(key);
        }
        
        List<CS_Bandwidth__c> bandlist = [SELECT Id 
                                            FROM CS_Bandwidth__c];
        
        for(CS_Bandwidth__c item : bandlist){
            bwIds.add(item.Id);
        } 
        
        System.Debug('doLookupSearch');
        System.Debug(searchFields);
        String searchValue = '%' + searchFields.get('searchValue') +'%';
        List <CS_Bandwidth__c> data = [SELECT Id, Name, Bandwidth_Value_MB__c 
            FROM 
                CS_Bandwidth__c 
            WHERE 
                Name LIKE :searchValue
            ORDER BY 
                Bandwidth_Value_MB__c 
            LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset];
            
        System.Debug(data);
        return data;*/
         final Integer SELECT_LIST_LOOKUP_PAGE_SIZE = 25;
        final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = 26;
        Integer recordOffset = pageOffset * SELECT_LIST_LOOKUP_PAGE_SIZE;
        Decimal bw1,bw2;
        String searchValue = '%' + searchFields.get('searchValue') +'%';
        List <CS_Bandwidth__c> data = new List <CS_Bandwidth__c>();
        String MDR = searchFields.get('Maximum Bandwidth excluding direct China');
            String ChinaBw = searchFields.get('Direct China Committed Bandwidth');
            String CommittedBw = searchFields.get('Committed Bandwidth excluding direct China');
            String PortRateType = searchFields.get('Port Rating Type');
            
        if(PortRateType == 'Flat'){
        System.debug('Inside IF ++++++++++++++');
            List <CS_Bandwidth__c> committedbwdata = [SELECT Id, Name, Bandwidth_Value_MB__c FROM CS_Bandwidth__c where id =: CommittedBw];
            List <CS_Bandwidth__c> chinaBwdata = [SELECT Id, Name, Bandwidth_Value_MB__c FROM CS_Bandwidth__c where id =: ChinaBw];
            System.debug('SIZE++++++++++++++'+committedbwdata.size());
            System.debug('SIZE++++++++++++++'+chinaBwdata.size());
            System.debug('BWMB++++++++++++++'+committedbwdata[0].Bandwidth_Value_MB__c);
            if(committedbwdata.size() > 0){
            bw1 = committedbwdata[0].Bandwidth_Value_MB__c;
            }
            else{
            bw1 = 0;
            }
            if(chinaBwdata.size() > 0){
            bw2 = chinaBwdata[0].Bandwidth_Value_MB__c;
            }
            else{
            bw2 = 0;
            }
            Decimal Sum = bw1+bw2;
            System.debug('SUM+++++++++++'+Sum);
            data = [SELECT Id, Name, Bandwidth_Value_MB__c FROM CS_Bandwidth__c where (Bandwidth_Value_MB__c >: Sum or Bandwidth_Value_MB__c =: Sum) AND Name LIKE :searchValue
            ORDER BY 
                Bandwidth_Value_MB__c 
            LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset];
            System.debug('DATAIF+++++++++++'+data);
            
        }
        
        if(PortRateType != 'Flat'){
         
            List <CS_Bandwidth__c> mdrBwdata = [SELECT Id, Name, Bandwidth_Value_MB__c FROM CS_Bandwidth__c where id =: MDR];
            
           
           data = [SELECT Id, Name, Bandwidth_Value_MB__c FROM CS_Bandwidth__c where (Bandwidth_Value_MB__c >: mdrBwdata[0].Bandwidth_Value_MB__c or Bandwidth_Value_MB__c =: mdrBwdata[0].Bandwidth_Value_MB__c) AND Name LIKE :searchValue
            ORDER BY 
                Bandwidth_Value_MB__c 
            LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset];
            System.debug('DATAELSE+++++++++++'+data);
            
        }    
        return data;
        
    }
    
}