@isTest(SeeAllData = false)
private class CS_VLANPriceItemLookupTest {

    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    
    private static List<CS_Country__c> countryList;
    private static List<CS_City__c> cityList;
    private static List<CS_Bandwidth__c> bandwidthList;
    private static List<CS_Bandwidth_Product_Type__c> bandwidthProdTypeList;
    private static List<cspmb__Price_Item__c> priceItemList;
    private static List<Account> accountList;
    
    private static void initTestData(){

        accountList = new List<Account>{
            new Account(Name = 'Account 1', Customer_Type_New__c = 'MNC'),
            new Account(Name = 'Account 2', Customer_Type_New__c = 'MNC')
        };
        
        insert accountList;
        System.debug('****Account List: ' + accountList);
        
        countryList = new List<CS_Country__c>{
            new CS_Country__c(Name = 'Croatia'),
            new CS_Country__c(Name = 'Australia'),
            new CS_Country__c(Name = 'India'),
            new CS_Country__c(Name = 'Germany'),
            new CS_Country__c(Name = 'Hong Kong'),
            new CS_Country__c(Name = 'New Zealand')
        };
        
        insert countryList;
        System.debug('****Country List: ' + countryList);
        
        cityList = new List<CS_City__c>{
            new CS_City__c(Name = 'Zagreb', CS_Country__c = countryList[0].Id),
            new CS_City__c(Name = 'Sydney', CS_Country__c = countryList[1].Id)
        };
        
        insert cityList;
        System.debug('****City List: ' + cityList);
        
        bandwidthList = new List<CS_Bandwidth__c>{
            new CS_Bandwidth__c(Name = '64k', Bandwidth_Value_MB__c = 64),
            new CS_Bandwidth__c(Name = '128k', Bandwidth_Value_MB__c = 128)
        };
        
        insert bandwidthList;
        System.debug('****Bandwidth List: ' + bandwidthList);
        
        bandwidthProdTypeList = new List<CS_Bandwidth_Product_Type__c>{
            new CS_Bandwidth_Product_Type__c(Name = 'BAndwidth Product Type 1', CS_Bandwidth__c = bandwidthList[0].Id, Product_Type__c = 'IPVPN'),
            new CS_Bandwidth_Product_Type__c(Name = 'BAndwidth Product Type 2', CS_Bandwidth__c = bandwidthList[1].Id, Product_Type__c = 'IPVPN')
        };
        
        insert bandwidthProdTypeList;
        System.debug('****Bandwidth Product Type List: ' + bandwidthProdTypeList);        
        
        priceItemList = new List<cspmb__Price_Item__c>{
            new cspmb__Price_Item__c(Name = 'Price Item 1', Country__c = countryList[0].Id, City__c = cityList[0].Id, CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[0].Id, cspmb__Account__c = accountList[0].Id, cspmb__Is_Active__c = true, IPVPN_Type__c = 'Standard', Connectivity__c = 'Onnet', Pricing_Segment__c = 'MNC'),
            new cspmb__Price_Item__c(Name = 'Price Item 2', Country__c = countryList[0].Id, City__c = cityList[0].Id, CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[0].Id, cspmb__Account__c = null, cspmb__Is_Active__c = true, IPVPN_Type__c = 'Standard', Connectivity__c = 'Onnet', Pricing_Segment__c = 'MNC')
        };
        
        insert priceItemList;
        System.debug('****Price Item List: ' + priceItemList);        
    }
    
    private static testMethod void doDynamicLookupSearchTest() {
    Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
                             
            initTestData();  
            
            searchFields.put('Account Id', accountList[0].Id);
            searchFields.put('PortPOP City', cityList[0].Id);
            searchFields.put('VLAN Port Country', countryList[0].Id);
            searchFields.put('VLAN speed', bandwidthProdTypeList[0].Id);
            searchFields.put('VLAN type', 'Standard');
            searchFields.put('Account Customer Type', 'MNC');
            searchFields.put('Product Type', 'IPVPN');
            searchFields.put('Onnet or Offnet', 'Onnet');
            
            
            CS_VLANPriceItemLookup vlanPriceItemLookup = new CS_VLANPriceItemLookup();
            String reqAtts = vlanPriceItemLookup.getRequiredAttributes();
            Object[] data = vlanPriceItemLookup.doDynamicLookupSearch(searchFields, productDefinitionId);
        
            System.debug('*******Data: ' + data);
            System.assert(data.size() > 0, '');
            Test.stopTest();
            
        } catch(Exception e){
            ee = e;
        } finally {
           // Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }  
    }

}