/**
* @author Telstra Accenture
* @description This class has the logic to Close the case and update the record type accordingly
*/ 

public class CaseClosure{ 

    @testvisible
    private Case theCase;
    @testvisible
    private ApexPages.StandardController stdController;
    @testvisible
    private List<String> additionalFields = new List<String>{'RecordTypeId', 'Type', 'Product_Basket__c', 'Internet_Product__c', 'Opportunity_Name__c', 'Opportunity_Name__r.Order_Support_AI_completed__c','Parent_Feasibility__c','Status', 'Number_of_Child_Case_Closed__c', 'Number_of_Child_Case__c', 'Enrichment_Status__c', 'Subject', 'Opportunity_Name__r.Number_of_Products__c', 'Product_Basket__r.CountOfProductConfiguration__c','Opportunity_Name__r.No_of_usage_line_item__c','Test_order_owner__c','Order__c','CaseNumber','Service_Name__c','Waiver_Request_Status__c','Account.Name','Requested_Termination_Date__c','Contract_Expiry_Date__c','Account.Customer_Type_New__c','AccountId'};
    @testvisible
    private static Map<String, Schema.RecordTypeInfo> caseRecordTypes = null;
    
    public CaseClosure(ApexPages.StandardController controller){
        if(!test.isrunningtest()){controller.addFields(additionalFields);}
        this.stdController = controller;
        this.theCase = (Case)controller.getRecord();    
    }
   public CaseClosure(){
    
   }

/**
* @author Telstra Accenture
* @description this contains the logic to navigate the detail page after the case closure
*/ 
 
    public PageReference gotoDetailPage(){

    try{
        
        if(theCase.Type == 'Enrichment' && theCase.Enrichment_Status__c == 'Completed'){
         TriggerFlags.NoProductBasketTriggers = True;
         TriggerFlags.NoProductConfigurationTriggers = True;
         TriggerFlags.NoAttributeTriggers =True;
         if(!theCase.Opportunity_Name__r.Order_Support_AI_completed__c){
         
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, label.Order_Support_Error_Msg)); 
            return null;
         }
    List<cscfga__Product_Configuration__c> prodConfigList = [select id,Product_Definition_Name__c,Added_Ports__c,Product_Id__c,Product_Code__c,cscfga__Product_Basket__c,Order_Type__c,EVPLAEndPop__c,csordtelcoa__Replaced_Service__c,csordtelcoa__Replaced_Service__r.Name,csordtelcoa__Replaced_Service__r.Inventory_Status__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c =:theCase.Product_Basket__c];
        //Nidhi 
        String buildServices=validateService(prodConfigList);
      if (buildServices <> '')
      {
      String eRR = 'The Order cannot be generated as the Service ' +buildServices+ ' are still in BUILD & TEST status. Please contact the SD PM to complete the In-Progress order against the same service before generating a new order.';
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, eRR));
        return null;
      }
        theCase.Internet_Product__c = validateProductConfig(prodConfigList,theCase.Opportunity_Name__r.Number_of_Products__c,theCase.Opportunity_Name__r.No_of_usage_line_item__c,theCase.Product_Basket__r.CountOfProductConfiguration__c);    
    }
         if(theCase.Parent_Feasibility__c == null && (theCase.Number_of_Child_Case_Closed__c != theCase.Number_of_Child_Case__c || (theCase.Number_of_Child_Case__c == 0 && theCase.Subject.Contains('Parent request for supplier quote')))){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Parent case cannot be closed without closing child cases'));
                return null;
            }
            else{
                theCase.Status = 'Closed';
     
                if(getRecordTypeId('Default') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Default Close Case');
                }
                else if(getRecordTypeId('FeasibilityStudyType') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('FeasibilityStudyType Close Case');
                }
                else if(getRecordTypeId('Parent Product Supplier Quote') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Parent Product Close Case');
                }
                else if(getRecordTypeId('Local Loop Record Type') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Local Loop Close Case Record Type');
                }
                else if(getRecordTypeId('GCPE Record Type') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('GCPE Close Case Record Type');
                }
                else if(getRecordTypeId('GCPE Renegotiation') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('GCPE Renegotiation Close Case Record Type');
                }
                else if(getRecordTypeId('Approve') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Approve Close Case Record Type');
                }
                else if(getRecordTypeId('FSProductConfiguration Type') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('FSProductConfiguration Close Case Type'); 
                }
                 else if(getRecordTypeId('Reject') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Reject Close Case Record type');
                }
                else if(getRecordTypeId('Approve/reject') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Approve/Reject Close Case Record Type');  
                }
                else if(getRecordTypeId('Bid Management Type') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Bid Management Type Close case');
                }
                else if(getRecordTypeId('Jeopardy') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Jeopardy');
                }
                else if(getRecordTypeId('Connectivity') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Connectivity Closed');
                }
                else if(getRecordTypeId('Cross Connect') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Cross Connect Closed');
                }
                else if(getRecordTypeId('Cage/Private Room') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Cage/Private Room Closed');
                }
                else if(getRecordTypeId('RR Cage or Rack RType') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('RR Cage or Rack Closed');
                }
                else if(getRecordTypeId('RR Connectivity RType') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('RR Connectivity Closed');
                }
                else if(getRecordTypeId('RR Cross Connect RType') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('RR Cross Connect Closed');
                }
                else if(getRecordTypeId('Design, Implement & Test') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Design, Implement & Test');
                }
                else if(getRecordTypeId('BillingStageGateRequest') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('BillingStageGateRequest');
                }
                else if(getRecordTypeId('Internet Product') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Internet Product Closed');
                }
                else if(getRecordTypeId('RR Internet Product') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('RR Internet Product Closed');  
                } 
                else if(getRecordTypeId('IPVPN & VPLS Product') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('IPVPN & VPLS Product Closed');  
                } 
                else if(getRecordTypeId('RR IPVPN & VPLS Product') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('RR IPVPN & VPLS Product Closed');  
                }
                else if(getRecordTypeId('ETC Commercial') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('ETC Closed Or Cancelled');  
                }
                /** Supplier cases **/
                else if(getRecordTypeId('Supplier Quoting VPN Services') == theCase.RecordTypeId || getRecordTypeId('Supplier Quoting VPN Services Renegotiation') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Supplier Quoting VPN Services Close Case');  
                }
                else if(getRecordTypeId('Parent Product Supplier Quote') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Parent Product Close Case');
                }
                else if(getRecordTypeId('Local Loop Record Type') == theCase.RecordTypeId || getRecordTypeId('Local Loop Renegotiation') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Local Loop Close Case Record Type');
                }
                else if(getRecordTypeId('GCPE Record Type') == theCase.RecordTypeId || getRecordTypeId('GCPE Renegotiation') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('GCPE Close Case Record Type');
                }
                else if(getRecordTypeId('Cross Connect Record Type') == theCase.RecordTypeId || getRecordTypeId('Cross Connect Renegotiation') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Local Loop Close Case Renegotiation');
                }
                else if(getRecordTypeId('PTP Supplier MES or OSS') == theCase.RecordTypeId || getRecordTypeId('PTP Supplier MES or OSS Renegotiation') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('PTP Supplier MES or OSS Close Case');
                }
                else if(getRecordTypeId('PTP Supplier PTP') == theCase.RecordTypeId || getRecordTypeId('PTP Supplier PTP Renegotiation') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('PTP Supplier PTP Close Case');
                }
                else if(getRecordTypeId('Escalate')==theCase.RecordTypeId){
                     theCase.RecordTypeId = getRecordTypeId('PTP Supplier PTP Renegotiation');
                }
                else if(getRecordTypeId('Supplier GIE Renegotiation') == theCase.RecordTypeId || getRecordTypeId('Supplier Quoting VPN Services Renegotiation') == theCase.RecordTypeId){
                    theCase.RecordTypeId = getRecordTypeId('Supplier GIE Close');  
                }           
                 else if(getRecordTypeId('Supplier GIE')==theCase.RecordTypeId){
                     theCase.RecordTypeId = getRecordTypeId('Supplier GIE Close');
                }
                else if(getRecordTypeId('Enrichment') == theCase.RecordTypeId){
                    
                    if(theCase.Internet_Product__c != true){
                         SolutionSummary.createSolution(theCase.Id,theCase.AccountId,theCase.Account.Customer_Type_New__c,theCase.Opportunity_Name__c,theCase.Product_Basket__c);//CR019 
                        OpportunityManager oppManager = new OpportunityManager();
                        opportunity opp = oppManager.updateStage(theCase.Opportunity_Name__c, 'Won', 'Closed Sales', 'Closed Won');
                        updateBasketStage(theCase.Product_Basket__c);
                    }
                    
                    if('Partial Completed'.equals(theCase.Enrichment_Status__c)){
                        List<csord__Order__c> opportunityOrders = [select Id from csord__Order__c where csordtelcoa__Opportunity__c = :theCase.Opportunity_Name__c];
                        for(csord__Order__c order :opportunityOrders){
                            order.Enrichment_Incomplete__c = true;
                        }
                        update opportunityOrders;
                    }
                }
                return stdController.save();
            }
          
      
        } catch(Exception e){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, +e.getMessage()));
            return null;
          }     
    }
    
  public String validateService(List<cscfga__Product_Configuration__c> prodConfigList){
     String buildServices = '';
     system.debug('prodConfigList===='+prodConfigList);
     for(cscfga__Product_Configuration__c pc :prodConfigList){
               system.debug('pc==='+pc);
            if(pc.csordtelcoa__Replaced_Service__r.Inventory_Status__c != 'LIVE - AS DESIGNED' && pc.csordtelcoa__Replaced_Service__r.Name != null)
                  {
                    buildServices = pc.csordtelcoa__Replaced_Service__r.Name +','+ buildServices;
                  }
     }
     system.debug('===buildServices'+buildServices);
     return buildServices;
    
  }
    public Boolean validateProductConfig(List<cscfga__Product_Configuration__c> prodConfigList,Decimal noOfProd,Decimal noOfUsageLine,Decimal countOfProdConfig){
          /** Validating Internet product's parent child linkage **/
            Set<Id> IPMSingleHomeProdList = new Set<Id>();
            Set<Id> IPCAddedPortsList = new Set<Id>();
            Set<Id> IPMAddedPortsList = new Set<Id>();
            Set<Id> IPMPCId = new Set<Id>();
            Set<Id> IPTPCId = new Set<Id>();
            boolean flag = true;
            boolean internetProducts = false;
             /** The list 'prodConfigList' will contain all products configured in the basket **/
            
            for(cscfga__Product_Configuration__c pc :prodConfigList){
               system.debug('pc===='+pc);
                /** The list 'IPCAddedPortsList' will contain all IPC product's child PC Ids **/
                if(pc.Product_Code__c == 'IPC' && pc.Added_Ports__c != null){

                    for(String portId :pc.Added_Ports__c.split(',')){
                        IPCAddedPortsList.add(Id.valueOf(portId));                  
                    }
                    internetProducts = true;
                }
                /** The list 'IPMPCId' will contain all IPM product's PC Ids **/
                if(pc.Product_Code__c == 'IPM' && pc.Order_Type__c == 'New Provide'){
                    system.debug('IPM');
                    IPMPCId.add(pc.Id);
                    internetProducts = true;
                }
                /** The list 'IPMAddedPortsList' will contain all IPM product's child PC Ids **/
                if((pc.Product_Id__c == 'IPTM-PLA' || pc.Product_Id__c == 'IPTM-STD' || pc.Product_Id__c == 'TWIM') && pc.Added_Ports__c != null){
                    for(String portId :pc.Added_Ports__c.split(',')){
                        IPMAddedPortsList.add(Id.valueOf(portId));
                    }
                    internetProducts = true;
                }
                /** The list 'IPTPCId' will contain all IPT product's PC Ids **/
                if(((pc.Product_Code__c == 'IPT' && pc.Product_Id__c != 'DOSP') || pc.Product_Code__c == 'IPT-BACKPOP') && pc.EVPLAEndPop__c != 'Singlehome' && pc.Order_Type__c == 'New Provide'){
                    IPTPCId.add(pc.Id);
                    internetProducts = true;
                }
                /** The list will contain all IPT and TWI Singlehome product's PC Ids **/
                if((pc.Product_Definition_Name__c == 'TWI Singlehome' || pc.Product_Definition_Name__c == 'IPT Singlehome') && pc.Order_Type__c == 'New Provide'){
                    IPMSingleHomeProdList.add(pc.Id);
                }
            }
    
            
            /** Validating if IPM PCs are linked to IPC PCs and IPT PCs are linked to IPM PCs **/
            if(IPCAddedPortsList.size() > 0 && IPMPCId.size() > 0){
            
            /** Validating if IPM PCs are linked to IPC PCs **/
            for(Id IPMId :IPMPCId){
                if(!IPCAddedPortsList.Contains(IPMId) && IPCAddedPortsList.size() > 0){
                    flag = false;
                    break;
                }
            }
            /** Setting the flag false if non of the IPM PCs or IPT PCs are linked **/
            } else if(IPCAddedPortsList.isEmpty() && !IPMPCId.isEmpty() && internetProducts == true){
                flag = false;
            }
            
            if(IPMAddedPortsList.size() > 0 && IPTPCId.size() > 0 && flag != false){

            /** Validating if IPT PCs are linked to IPM PCs **/
            for(Id IPTId :IPTPCId){    
                if(!IPMAddedPortsList.Contains(IPTId) && IPMAddedPortsList.size() > 0){
                    flag = false;
                    break;
                }
            }
            /** Setting the flag false if non of the IPM PCs or IPT PCs are linked **/
            } else if(IPMAddedPortsList.isEmpty() && !IPTPCId.isEmpty() && internetProducts == true && flag != false){
                flag = false;
            }
            
            /**
             * If any of the IPM or IPT PCs are not linked to parent PCs, then setting the Internet_Product__c field's value to true.
             * If the Internet_Product__c flag's value is set to true, it will not allow user to close the case without linking the Internet PCs accordingly.
             */
            if((flag == false && prodConfigList.size()>0) || (((noOfProd - noOfUsageLine) / 2) <> countOfProdConfig)){
              return true;
            } else{
              return false;
            }
          
        
                
    }

    /**
     * Author: Sanchit/Pavan
     * Fix for 15355
     * Update basket stage to 'Closed Won' when opportunity is set to 'Close Won'
     * This is done as part of W17 upgrade
     * Logic has been udpated. //Sapan
     */
    
public void updateBasketStage(Id prodBasketId){
        try{        
            if(prodBasketId != null){
                
                cscfga__Product_Basket__c bskt = new cscfga__Product_Basket__c(Id=prodBasketId,csordtelcoa__Basket_Stage__c='Closed Won',Order_Creator__c=userinfo.getuserid());
                
                update bskt;
            }
        } catch(Exception e){
            System.debug('Error: "CaseClosure; updateBasketStage" mehotd update failure - ' +e);
          }
    }

    public PageReference returnToCase(){
        return stdController.view().setRedirect(true);
    }
/**
* @author Telstra Accenture
* @description Loads all Case record types by name and stores in a static map for subsequent calls
*/ 

    @testvisible
    private Id getRecordTypeId(String name){
        return RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, name);
    }
/**
* @author Telstra Accenture
* @description Send email to the order owner upon closure of the ETC Case
*/
    @testvisible
    private void sendEmailToOrderOwner(Case newCase){
        Id groupOwnerId = null;
        if(newCase.Test_order_owner__c != null && newCase.Subject == 'Request to calculate ETC' && newCase.Status == 'Closed'){
            Id objectId = newCase.Test_order_owner__c;
            String sObjName = objectId.getSObjectType().getDescribe().getName();
            if(sObjName == 'Group'){
                groupOwnerId = newCase.Test_order_owner__c;
            } else if(sObjName == 'User'){
               SendEmailToQueueMembers.sendEmail(newCase, new Set<Id>{Id.ValueOf(newCase.Test_order_owner__c)});
            }
        }
        if(groupOwnerId != null){
            SendEmailToQueueMembers bc = new sendEmailToQueueMembers(groupOwnerId, newCase);
            Database.executeBatch(bc, 1);
        }
    }   
}