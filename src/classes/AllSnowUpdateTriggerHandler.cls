public class AllSnowUpdateTriggerHandler extends BaseTriggerHandler{

public override void afterInsert(List<SObject> newSnowUpdates, Map<Id, SObject> newSnowUpdatesMap){

 sendEmailtoSDOnSNOWComplete((List<Integration_Update__c>) newSnowUpdates);

}

 public override void afterUpdate(List<SObject> newSnowUpdates, Map<Id, SObject> newSnowUpdatesMap, Map<Id, SObject> oldSnowUpdatesMap){

 //sendEmailtoSDOnSNOWComplete((List<Integration_Update__c>) newSnowUpdates);

}

public void sendEmailtoSDOnSNOWComplete(List<Integration_Update__c> newSnowUpdates){
Set<String> rootProdSet=new Set<String>();
String rootProd='';

for(Integration_Update__c intUp:newSnowUpdates){

if(intUp.Product_Id__c=='TWI' && intUp.Name=='Design, Implement and Testing has completed in SNOW'){
  system.debug('intUp.Root_Service_Id__c'+intUp.Root_Service_Id__c);
rootProdSet.add(intUp.Root_Service_Id__c);
rootProd=intUp.Root_Service_Id__c!=null?intUp.Root_Service_Id__c:'';


}

}
system.debug('===rootProdSet'+rootProd);
if(rootProdSet.size()>0 || rootProd!=''){
List<csord__Service__c> servList=[select id,Name,Order_Number__c,Region__c from csord__Service__c where Product_Id__c=:'TWI' and Root_Service_ID__c =:rootProd];
system.debug('servList==='+servList);
List<Integration_Update__c> snowUpdateList=[select id,Is_SNOW_DIT_Complete__c,Order_Owner__c,Order_Owner_Name__c,Product_Id__c,Root_Service_Id__c from Integration_Update__c where Is_SNOW_DIT_Complete__c=true and Root_Service_Id__c =:rootProd and Product_Id__c=:'TWI' and Name=:'Design, Implement and Testing has completed in SNOW'];
system.debug('snowUpdateList==='+snowUpdateList);
if(servList.size()==snowUpdateList.size()){
sendEmailNotification(servList,snowUpdateList);
}

}
}

public void sendEmailNotification(List<csord__Service__c> servList,List<Integration_Update__c> snowUpdateList){
        
        //List of all emails that will be sent
        List<Messaging.SingleEmailMessage> mailToOrderOwnerList = new List<Messaging.SingleEmailMessage>();
        //Email to be sent to the Order Owner
        Messaging.SingleEmailMessage mailToOrderOwner = new Messaging.SingleEmailMessage();
        //Setting the recepient address
        List<String> sendTo = new List<String>();
        EmailContentUtility emailutil=new EmailContentUtility();
        String queuename = snowUpdateList[0].Order_Owner_Name__c;
        sendTo=emailutil.getEmailAddess(QueueUtil.getQueueIdByName(csord__Order__c.SObjectType,queuename)).split(':', 0);
       system.debug('=====sendTo===='+sendTo);
       // sendTo.add(UserInfo.getUserEmail());
        mailToOrderOwner.setToAddresses(sendTo);
        //Setting the address from which the email will be triggered
       // mailToOrderOwner.setReplyTo(UserInfo.getUserEmail());
        //Setting the email subject
        mailToOrderOwner.setSubject('Design, Implement and Testing has completed in SNOW');
        //Setting the CSS styling for the table
        String mailBody='<style type="text/css">'+
        '.tg  {border-collapse:collapse;border-spacing:0;border-color:#000000;}'+
        '.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}'+
        '.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}'+
        '</style>';
        //Setting the email body -- initial information
        mailBody+='Hi '+'Service Delivery - '+servList[0].Region__c+','
        +'<br/>'
        +'<br/>'
        +'This is just for your information SNOW RITM Tasks are completed for following Services.'
        +'<br/><br/>';
        //Setting the email body -- table headers
        mailBody+='<table class="tg">'
        +'<tr>'
        +'<th class="tg-031e"><strong>Order Name</strong></th>'
        +'<th class="tg-031e"><strong>Service Name</strong></th>'
        +'</tr>';
        //Setting the email body -- table data
        for(csord__Service__c ai:servList){
            mailBody+='<tr>'
           
            +'<td class="tg-031e">'+ai.Order_Number__c+'</td>'
            +'<td class="tg-031e"><a href='+URL.getSalesforceBaseUrl().toExternalForm()+'/'+ai.Id+'>'+ai.Name+'</a></td>'
            +'</tr>';
        }
        mailBody+='</table>'
        +'<br/>'
            
        +'<br/>'
        +'Thanks'
        +'<br/>'
       
        +'***This is an auto-generated notification, please do not reply to this email***';
        
        mailToOrderOwner.setHtmlBody(mailBody);
        mailToOrderOwnerList.add(mailToOrderOwner);
        Messaging.sendEmail(mailToOrderOwnerList);
 

}
}