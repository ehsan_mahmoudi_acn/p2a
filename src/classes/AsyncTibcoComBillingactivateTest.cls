@istest(seealldata=false)
public class AsyncTibcoComBillingactivateTest{
     
    static testMethod void allOrderTriggertest() {

    AsyncTibcoComBillingactivate.AsyncBillingActivatePortTypeEndpoint1 AsyncBillingActive = new AsyncTibcoComBillingactivate.AsyncBillingActivatePortTypeEndpoint1();
    
    TibcoComSchemasXsdgenerationBill.Service_element[] Service = new List<TibcoComSchemasXsdgenerationBill.Service_element>();
    //System.continuation con = new System.continuation();
    
    Test.startTest();
        Test.setMock(WebServiceMock.class, new MainMockClass.SuspendOrderResponse_BillingActivateResponse());
       AsyncTibcoComSchemasXsdgenerationBill.BillingActivateResponse_elementFuture AsyncXsdgenBillingActresp = new AsyncTibcoComSchemasXsdgenerationBill.BillingActivateResponse_elementFuture();
        //TibcoComSchemasXsdgenerationBill.BillingActivateResponse_element AsyncXsdgenBillingActresp = new TibcoComSchemasXsdgenerationBill.BillingActivateResponse_element();
       AsyncXsdgenBillingActresp  = AsyncBillingActive.beginBillingActivateOperation(new System.continuation(60), Service);
    Test.stopTest();
     system.assertEquals(true,AsyncBillingActive!=null);
     system.assertEquals(true,AsyncXsdgenBillingActresp!=null);
 
    }
 }