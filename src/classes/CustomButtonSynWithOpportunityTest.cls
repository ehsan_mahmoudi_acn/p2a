@isTest(SeeAllData=false)
Public Class CustomButtonSynWithOpportunityTest{

Public static testMethod void customButtonSynchronizeWittoppTest(){


        P2A_TestFactoryCls.sampleTestData();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
           
    
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware');
        insert prod;
        system.assertEquals(true,prod!=null);
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry pbe = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 300, IsActive = true);
            pbe.UseStandardPrice = false;
        insert pbe;
     system.assertEquals(true,pbe!=null);
  
       List<Opportunity> opplist = new List<Opportunity>();
       Opportunity opp = new Opportunity();
       opp.Name = 'Test Opportunity';
       opp.StageName = 'Identify And Define';
       opp.CloseDate = System.today();
       opp.QuoteStatus__c = 'Approved';
        opp.Product_Type__c = 'IPL';
                opp.Estimated_MRC__c = 100.00;
                opp.Estimated_NRC__c = 100.00;
       Opplist.add(opp);
       insert OPplist;
      system.assertEquals(true,OPplist!=null);
   
      
      OpportunityLineItem opplineitem = new OpportunityLineItem();
      List<OpportunityLineItem> opplineitemlist = new List<OpportunityLineItem>(); 
      opplineitem.Opportunityid = OppList[0].id;
      opplineitem.Quantity = 12;
      opplineitem.TotalPrice = 100;
     // opplineitem.PricebookEntryId = pbEntry.id; 
      opplineitem.PricebookEntryId = pbe.id;
      opplineitemlist.add(opplineitem);
      insert opplineitemlist;
      system.assertEquals(true,opplineitemlist!=null);
    
      list<cscfga__Product_Basket__c> basketlist = new list<cscfga__Product_Basket__c>();
      cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c();
        pb.csbb__Account__c = Acclist[0].id;
        pb.cscfga__Opportunity__c = opplist[0].id;
         basketlist.add(Pb);
         Insert basketlist;
         system.assertEquals(true,basketlist!=null); 
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<User> users = P2A_TestFactoryCls.get_Users(1);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,basketlist,prodef,pbundlelist ,Offerlists);
          
         
    list<OpportunityTeamMember> OppTeamList = new list<OpportunityTeamMember>();
     OpportunityTeamMember OppTeam = new OpportunityTeamMember();
     OppTeam.TeamMemberRole = 'Sales special list';
     OppTeam.UserId = users[0].id;
     Oppteam.OpportunityId = Opplist[0].id;
     OppTeamList.add(OppTeam);
     insert OppTeamList;
    system.assertEquals(true,OppTeamList!=null); 
     try{
      test.starttest();
      
     CustomButtonSynchronizeWithOpportunity CBSO = new CustomButtonSynchronizeWithOpportunity();
     CBSO.performAction(basketlist[0].id);
     CustomButtonSynchronizeWithOpportunity.syncWithOpportunity(basketlist[0].id);
        system.assertEquals(true,CBSO!=null); 
      test.stoptest();
     }catch(exception e){
         ErrorHandlerException.ExecutingClassName='CustomButtonSynWithOpportunityTest :CustomButtonSynchronizeWittoppTest';         
        ErrorHandlerException.sendException(e); 
     }
 
}

  }