@isTest(SeeAllData = false)
public class TestContactDelTrg 
{
    static testmethod void testContactDelTrg1()
    {
        Test.startTest();
        Profile profObj = [select Id ,name from Profile  where name ='System Administrator'];
        
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
    
   
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='34987',
                                    Account_ORIG_ID_DM__c = '34987', Customer_Legal_Entity_Name__c = 'test'); 
        insert accObj;
        Contact contObj = new Contact(Country__c=cntryLkupObj.id,AccountId=accObj.Id,LastName='tech',
                                     Email ='mohantesh1256@telstra.com');
        insert contObj;
        
        try
        {
            delete contObj;
           
        } catch (Exception e) {
            System.assert(true, 'Deletion failed appropriately');
        }
        Test.stopTest();
    }
}