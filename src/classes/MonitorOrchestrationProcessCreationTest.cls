@istest (seealldata = false)
public class MonitorOrchestrationProcessCreationTest{
    public static testmethod void monitorOrchestrationProcessCreationTest(){
        P2A_TestFactoryCls.sampleTestData();
        List<Account> account = P2A_TestFactoryCls.getaccounts(1);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1, OrdReqList);    
         
        
           id recordtype = RecordTypeUtil.getRecordTypeIdByName(csord__Order__c.SObjectType, 'MonitorOrchestrationProcessCreation');
        csord__Order__c ord = new csord__Order__c();
                ord.csord__Identification__c = 'Test-JohnSnow-4238362';
                ord.RAG_Order_Status_RED__c = false;
                ord.RAG_Reason_Code__c = '';
                ord.Jeopardy_Case__c = null;
                ord.csord__Order_Type__c = 'terminate';
                ord.recordtypeid = recordtype ;
                ord.csord__Order_Request__c = OrdReqList[0].id;
                insert ord;
                Orders.add(ord);
                
                
           List<csord__service__c> Services = P2A_TestFactoryCls.getservice(1, OrdReqList, subscriptionList);     
                csord__Service__c ser = new csord__Service__c();
                ser.Name = 'Test Service'; 
                ser.csord__Identification__c = 'Test-Catlyne-4238362';
               // ser.csord__Order_Request__c = i<OrdReqList.size() ? OrdReqList[i].id : OrdReqList[0].id;
                ser.csord__Subscription__c = subscriptionList[0].id;
                ser.Billing_Commencement_Date__c = System.Today();
                ser.Stop_Billing_Date__c = System.Today();
                ser.RAG_Status_Red__c = false ; 
                ser.RAG_Reason_Code__c = ''; 
                ser.Estimated_Start_Date__c = null;
                ser.csord__Order__c =  Orders[0].id;                          
                // if(ser.id != null)
                //ser.csordtelcoa__Product_Basket__c = i<ProdBaskList.size()? ProdBaskList[i].id : ProdBaskList[0].id;  
                ser.Product_Id__c = 'IPVPN';     
                ser.Path_Instance_ID__c = 'test';
              
                insert Ser;
                   Services.add(ser);    
                
         
         cscfga__Product_Basket__c p = new cscfga__Product_Basket__c();
            p.Name = 'Test basket ';
            insert  p;
            
           
        


 
        Test.StartTest();
        Solutions__c solObj = new Solutions__c(Batch_Count__c=5,Order_Name__c=Orders[0].Id,Account_Name__c=account[0].Id);
        insert solObj;      
        system.assertEquals(true,solObj!=null); 
        Orders[0].Solutions__c=solObj.Id;
        Services[0].Solutions__c=solObj.Id;
        update Orders;
        update Services;
        system.assertEquals(true,Orders!=null);
        system.assertEquals(true,Services!=null);
        Attachment newAttachement = new Attachment(ParentId=Orders[0].Solutions__c,Name='AsyncApexJobID_OrchestrationProcessCreation-' +Orders[0].Id +'-' +'707O0000011s5VB',body=Blob.valueOf('Testing'));
        insert newAttachement;
        system.assertEquals(true,newAttachement!=null); 
        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(Orders[0]);
        
        
        MonitorOrchestrationProcessCreation monitor = new MonitorOrchestrationProcessCreation(sc);
        ApproveCaseCtrlBatch bc = new ApproveCaseCtrlBatch(p.Id);
       id jobid=Database.executeBatch(bc);
        List<AsyncApexJob>jobList=[select ID,Status from AsyncApexJob limit 10];
        System.assert(jobList.size()>0);
        monitor.jobs=new List<AsyncApexJob>(jobList);
        String batchSt=monitor.getBatchOrderStatus();
        monitor.deleteAttachmentOfSolution();
        monitor.restartProcess(Orders[0]);
        MonitorOrchestrationProcessCreation.deleteAttachment(Orders[0].Solutions__c, Orders[0].Id);
        MonitorOrchestrationProcessCreation.updateRecordTypeId(Orders[0].Id);
        system.assertEquals(true,monitor!=null);
        Test.StopTest();
    }
    @istest
    public static void batchprocess1()
    {
    P2A_TestFactoryCls.sampleTestData();
        List<Account> account = P2A_TestFactoryCls.getaccounts(1);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1, OrdReqList);    
         
        
           id recordtype = RecordTypeUtil.getRecordTypeIdByName(csord__Order__c.SObjectType, 'MonitorOrchestrationProcessCreation');
        csord__Order__c ord = new csord__Order__c();
                ord.csord__Identification__c = 'Test-JohnSnow-4238362';
                ord.RAG_Order_Status_RED__c = false;
                ord.RAG_Reason_Code__c = '';
                ord.Jeopardy_Case__c = null;
                ord.csord__Order_Type__c = 'terminate';
                ord.recordtypeid = recordtype ;
                ord.csord__Order_Request__c = OrdReqList[0].id;
                insert ord;
                Orders.add(ord);
                
                
           List<csord__service__c> Services = P2A_TestFactoryCls.getservice(1, OrdReqList, subscriptionList);     
                csord__Service__c ser = new csord__Service__c();
                ser.Name = 'Test Service'; 
                ser.csord__Identification__c = 'Test-Catlyne-4238362';
               // ser.csord__Order_Request__c = i<OrdReqList.size() ? OrdReqList[i].id : OrdReqList[0].id;
                ser.csord__Subscription__c = subscriptionList[0].id;
                ser.Billing_Commencement_Date__c = System.Today();
                ser.Stop_Billing_Date__c = System.Today();
                ser.RAG_Status_Red__c = false ; 
                ser.RAG_Reason_Code__c = ''; 
                ser.Estimated_Start_Date__c = null;
                ser.csord__Order__c =  Orders[0].id;                          
                // if(ser.id != null)
                //ser.csordtelcoa__Product_Basket__c = i<ProdBaskList.size()? ProdBaskList[i].id : ProdBaskList[0].id;  
                ser.Product_Id__c = 'IPVPN';     
                ser.Path_Instance_ID__c = 'test';
              
                insert Ser;
                   Services.add(ser);    
                
         
         cscfga__Product_Basket__c p = new cscfga__Product_Basket__c();
            p.Name = 'Test basket ';
            insert  p;
            
           
        


 
        Test.StartTest();
        Solutions__c solObj = new Solutions__c(Batch_Count__c=5,Order_Name__c=Orders[0].Id,Account_Name__c=account[0].Id);
        insert solObj;      
        system.assertEquals(true,solObj!=null); 
        Orders[0].Solutions__c=solObj.Id;
        Services[0].Solutions__c=solObj.Id;
        update Orders;
        update Services;
        system.assertEquals(true,Orders!=null);
        system.assertEquals(true,Services!=null);
        Attachment newAttachement = new Attachment(ParentId=Orders[0].Solutions__c,Name='AsyncApexJobID_OrchestrationProcessCreation-' +Orders[0].Id +'-' +'707O0000011s5VB',body=Blob.valueOf('Testing'));
        insert newAttachement;
        system.assertEquals(true,newAttachement!=null); 
   
    ApexPages.StandardController sc = new ApexPages.StandardController(Orders[0]);
    MonitorOrchestrationProcessCreation monitor = new MonitorOrchestrationProcessCreation(sc);
    ApproveCaseCtrlBatch bc = new ApproveCaseCtrlBatch(p.Id);
       id jobid=Database.executeBatch(bc);
        List<AsyncApexJob>jobList=[select ID,Status from AsyncApexJob  where Status = 'Failed' limit 10 ];
     //  System.assert(jobList.size()>0);
    // String batchSt=monitor.getBatchOrderStatus();
        monitor.jobs=new List<AsyncApexJob>(jobList);
        monitor.restartProcess(Orders[0]);
        String batchSt=monitor.getBatchOrderStatus();
        
         
         
   Test.StopTest();
        
    }
}