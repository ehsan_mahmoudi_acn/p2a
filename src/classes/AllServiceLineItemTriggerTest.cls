@isTest(SeeAllData=false) 
    public class AllServiceLineItemTriggerTest
     {
       Public static testMethod void allServiceLineItemTriggertest1() 
        {
        
        P2A_TestFactoryCls.sampleTestData();
        
             AllServiceLineItemTriggerHandler serLineItemTrgHdlr = new AllServiceLineItemTriggerHandler();
             Map<Id,csord__Service_Line_Item__c> serLineItemMap = new Map<Id,csord__Service_Line_Item__c>();
             
             Test.StartTest();
             serLineItemTrgHdlr.isDisabled();
             List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
             List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
             list<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
             List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
             List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1,orderRequestList);
             List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,orderRequestList);
             List<csord__Service__c> serList = P2A_TestFactoryCls.getServiceHdlr(1,orderRequestList, subList,prodBaskList);
             List<csord__Service_Line_Item__c> serLineItemList = P2A_TestFactoryCls.getSerLineItem(1,serList,orderRequestList);
             //serLineItemList[0].csord__Is_Recurring__c = true;
             //serLineItemList[0].Is_Miscellaneous_Credit_Flag__c = true;
             serLineItemList[0].Is_ETC_Line_Item__c = true;
             update serLineItemList;
             
             
             for(csord__Service_Line_Item__c servLineItemObj : serLineItemList) {
                 serLineItemMap.put(servLineItemObj.Id,servLineItemObj);
             }
                    
             serLineItemTrgHdlr.afterInsert(serLineItemList, serLineItemMap);
             serLineItemTrgHdlr.beforeUpdate(serLineItemList, serLineItemMap, serLineItemMap);
             serLineItemTrgHdlr.beforeInsert(serLineItemList);
    
            Test.StopTest(); 
            List<csord__Service_Line_Item__c> servlineitem =[select id ,U2C_Master_Code__c,csord__Service__c,csord__Service__r.U2CMasterCode__c,csord__Service__r.Cost_Centre_Id__c,Cost_Centre__c,MISCService__c,Billing_End_Date__c,Product_Basket__c,Zero_MISC_Charge_Flag__c,CurrencyISOCode,Charging_Frequency__c,NRC_Credit_Bill_Text__c,csord__Is_Recurring__c,Is_Miscellaneous_Credit_Flag__c,Display_line_item_on_Invoice__c,Is_ETC_Line_Item__c,csord__Total_Price__c,MISC_Charge_Amount__c,Display_Line_Item_On_Invoice_Text__c,Bill_Profile__c,Installment_End_Date__c from csord__Service_Line_Item__c];
            prodBaskList = [select Id, CurrencyISOCode, Exchange_Rate__c from cscfga__Product_Basket__c];
            system.assertEquals(prodBaskList[0].id, servlineitem[0].Product_Basket__c); //
            system.assertEquals(prodBaskList[0].CurrencyISOCode, servlineitem[0].CurrencyISOCode);
            serList=[Select Id,csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.cscfga__Default_Frequency__c from csord__Service__c ];        
            system.assertNotEquals(serList[0].csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.cscfga__Default_Frequency__c, servlineitem[0].Charging_Frequency__c );
            system.assert(servlineitem[0].Is_Miscellaneous_Credit_Flag__c);
            system.assert(servlineitem[0].Is_ETC_Line_Item__c);
            system.assertEquals(servlineitem[0].csord__Total_Price__c,servlineitem[0].MISC_Charge_Amount__c);
            system.assertEquals(servlineitem[0].Zero_MISC_Charge_Flag__c,true);
            system.assertEquals(servlineitem[0].NRC_Credit_Bill_Text__c,null);
            Integer numberOfDays = Date.daysInMonth(servlineitem[0].Billing_End_Date__c.year(), servlineitem[0].Billing_End_Date__c.month());
            Date lastDayOfMonth = Date.newInstance(servlineitem[0].Billing_End_Date__c.year(), servlineitem[0].Billing_End_Date__c.month(), numberOfDays);
            system.assertEquals(servlineitem[0].Installment_End_Date__c,lastDayOfMonth);
            //system.assertEquals(servlineitem[0].Bill_Profile__c,servlineitem[0].Bill_Profile__c);
            system.assertEquals(servlineitem[0].Cost_Centre__c,servlineitem[0].csord__Service__r.Cost_Centre_Id__c);
            system.assertEquals(servlineitem[0].csord__Service__c ,servlineitem[0].MISCService__c);
            system.assertEquals(servlineitem[0].U2C_Master_Code__c,servlineitem[0].csord__Service__r.U2CMasterCode__c);
            
        }
         
         
         
            
        Public static testMethod void allServiceLineItemTriggertest2() 
        {
        
        P2A_TestFactoryCls.sampleTestData();
        
             AllServiceLineItemTriggerHandler serLineItemTrgHdlr = new AllServiceLineItemTriggerHandler();
             Map<Id,csord__Service_Line_Item__c> serLineItemMap = new Map<Id,csord__Service_Line_Item__c>();
             
             Test.StartTest();
             serLineItemTrgHdlr.isDisabled();
             List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
             List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
             list<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
             List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
             List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1,orderRequestList);
             List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,orderRequestList);
             List<csord__Service__c> serList = P2A_TestFactoryCls.getServiceHdlr(1,orderRequestList, subList,prodBaskList);
             List<csord__Service_Line_Item__c> serLineItemList = P2A_TestFactoryCls.getSerLineItem(1,serList,orderRequestList);
             
             List<Country_Lookup__c> countryList = P2A_TestFactoryCls.getcountry(1);
             List<Contact> contactList = P2A_TestFactoryCls.getContact(1, accList);
             List<Site__c> SitesList = P2A_TestFactoryCls.getsites(1, accList, countrylist);
             List<BillProfile__c> billProfiles = P2A_TestFactoryCls.getBPs(1, accList, SitesList, contactList);
             
             serList[0].Billing_Commencement_Date__c = System.Today(); 
             serList[0].Stop_Billing_Date__c = System.Today(); 
             update serList;
             
        
         
             //serLineItemList[0].Billing_Commencement_Date__c = System.Today(); 
             //serLineItemList[0].Billing_ETC_End_Date__c = System.Today(); 
             serLineItemList[0].MISCService__c = serList[0].name;
             serLineItemList[0].Display_Line_Item_On_Invoice_Text__c = serList[0].name;
             serLineItemList[0].bill_profile__c = billProfiles[0].id;
             serLineItemList[0].Frequency_flag__c = false;
             serLineItemList[0].Is_ETC_Line_Item__c = true;
             update serLineItemList;
             
              //system.assert(serLineItemList[0].bill_profile__c != null);
             //system.assert(false, serLineItemList[0].Frequency_flag__c);
            //system.assert(serList[0], serLineItemList[0].csord__service__c);
             
             for(csord__Service_Line_Item__c servLineItemObj : serLineItemList) {
                 serLineItemMap.put(servLineItemObj.Id,servLineItemObj);
             }
                    
             serLineItemTrgHdlr.afterInsert(serLineItemList, serLineItemMap);
             serLineItemTrgHdlr.beforeUpdate(serLineItemList, serLineItemMap, serLineItemMap);
             serLineItemTrgHdlr.beforeInsert(serLineItemList);
            
             Test.StopTest(); 
             system.assert(serLineItemList[0].bill_profile__c != null);
             system.assertEquals(false, serLineItemList[0].Frequency_flag__c);
             system.assertEquals(serLineItemList[0].Bill_Profile__c, billProfiles[0].id);
            system.assertEquals(serLineItemList[0].Display_line_item_on_Invoice__c,Boolean.valueOF(serLineItemList[0].Display_Line_Item_On_Invoice_Text__c));

             
        }
        @istest
        public static void testblock()
        {
         AllServiceLineItemTriggerHandler serLineItemTrgHdlr1 = new AllServiceLineItemTriggerHandler();
        // AllServiceLineItemTriggerHandler allline = new AllServiceLineItemTriggerHandler();
        try{
         serLineItemTrgHdlr1.updateDisplayLineItemOnInvoice(null);
        }catch(Exception e){}
        try
        {
          serLineItemTrgHdlr1.updateETCinCaseFromSLI(null);
        }catch(Exception e){}
        try{
        serLineItemTrgHdlr1.updatingcurrencyofserviceslineitems(null);
        }catch(Exception e){}
        system.assertEquals(true,serLineItemTrgHdlr1 !=null);
        
        }
     }