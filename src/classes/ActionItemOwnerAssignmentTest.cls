@isTest(seealldata=false)
public class ActionItemOwnerAssignmentTest{
    
    public static List<Country_Lookup__c> cLookup=P2A_TestFactoryCls.getcountry(1);
    public static List<csord__service__c> servlists = new List<csord__service__c>();
    public static List<Account> AccList = P2A_TestFactoryCls.getAccounts(1);
    public static List<Opportunity> ListOpp = P2A_TestFactoryCls.getOpportunitys(1,AccList); 
    
    static testmethod void testAssignActionItemOwner() {
        
        Test.startTest();
        
        Account acc = AccList[0];
        acc.Account_Status__c = 'Pending ';
        acc.Requested_for_Verification__c =false;
        update acc;
        
        List<User> usrLst =[Select Name, Region__c,Role_Name__c from User where id= :UserInfo.getUserId()];
        usrLst[0].Region__c = 'Australia';
        update usrLst[0];
        
        Id RecordtypId = Schema.SObjectType.Action_Item__c.getRecordTypeInfosByName().get('Adhoc Request').getRecordTypeId();
        
        List<Global_Constant_Data__c> globalConst = new List<Global_Constant_Data__c>{
            new Global_Constant_Data__c(Name='Account Verification Request Subject',Value__c='Account Activation Required'),
            new Global_Constant_Data__c(Name='Account Verification Request Status',Value__c='Assigned'),
            new Global_Constant_Data__c(Name='Account Verification Request Priority',Value__c='Medium')};            
        insert globalConst;
        
        List<Group> groups = [select id, name from group where type = 'Queue' and Name IN  ('Sales Desk Support - Australia GSP')];
        
        List<Action_Item_Owner_Assignment__c> listACT = new List<Action_Item_Owner_Assignment__c>();
        
        Action_Item_Owner_Assignment__c  act =  new Action_Item_Owner_Assignment__c(Name ='Account Activation Australia GSP',Module__c='Account Verification Request',
                    Region__c='Australia',Queue_ID__c=groups[0].id,Region_And_Customer_type__c='Australia GSP');
        listACT.add(act); 
        
        
        Action_Item_Owner_Assignment__c  act1 =  new Action_Item_Owner_Assignment__c(Name ='Account Verification Request Generic',Module__c='Account Verification Request',
                    Region__c='USA',Queue_ID__c=groups[0].id,Region_And_Customer_type__c='Account_Admin_Support');
        
        listACT.add(act1); 
        insert listACT;
        
        Action_Item__c objActionItem = new Action_Item__c();              
        objActionItem.Account__c = AccList[0].id;
        objActionItem.Due_Date__c = System.Today() + 1;
        objActionItem.RecordTypeId = RecordtypId;
        objActionItem.Region__c =  usrLst[0].Region__c;             
        objActionItem.Subject__c = globalConst[0].value__c;
        objActionItem.Status__c =  globalConst[1].value__c;
        objActionItem.Priority__c = globalConst[2].value__c;
        Insert objActionItem;
        system.assert(objActionItem!=null);
                
        Action_Item_Owner_Assignment__c ActItmAssig = new Action_Item_Owner_Assignment__c();
        ActItmAssig.Name = 'Lead Conversion - Asia GSP';
        ActItmAssig.Module__c = 'Lead Conversion Request';
        //ActItmAssig.Queue_ID__c = testQueue.id;
        ActItmAssig.Region__c = 'Asia';
        ActItmAssig.Region_AND_Customer_Type__c = 'Asia GSP';
        ActItmAssig.Region_And_Customer_Type__c ='Account_Admin_Support';  
        //ActItmAssig.OwnerId = userinfo.getuserid();
        insert ActItmAssig;
        
        Account objAccount = [Select Name,Account_Status__c,Requested_for_Verification__c,Region__c,Customer_Type__c 
                                from Account where Id=:acc.id];

        objAccount.Account_Status__c = 'Pending Activation';
        objAccount.Requested_for_Verification__c =true;
        update objAccount;
        
        string accId = AccList[0].id;
        try {
            ActionItemOwnerAssignment.assignActionItemOwner(accId);
        Test.stopTest();
        
        } catch(Exception e){}
             
    }
    
    static testmethod void testassignActionItemOwnertoLead() {
    
        Test.startTest(); 
       
        Id RecordtypId = Schema.SObjectType.Action_Item__c.getRecordTypeInfosByName().get('Adhoc Request').getRecordTypeId();
        
        List<User> userList = P2A_TestFactoryCls.get_Users(1);
        userList[0].Region__c = 'Asia GSP';
        update userList[0];
        
        User usr = userList[0]; 
        
        List<Global_Constant_Data__c> globalConst = new List<Global_Constant_Data__c>{
            new Global_Constant_Data__c(Name ='Lead Conversion Request Priority',value__c = 'Medium'), 
            new Global_Constant_Data__c(Name ='Lead Conversion Request Status',value__c = 'Assigned'), 
            new Global_Constant_Data__c(Name ='Lead Conversion Request Subject',value__c = 'Lead Conversion Required') };             
        insert globalConst;
        
        Lead ld = new Lead();
        ld.Region__c = 'US';
        ld.Company = 'Telstra International12345';
        ld.LastName = 'Adp123';
        ld.Status = 'Marketing Qualified Lead (MQL)';
        ld.LeadSource = 'Direct Mail';
        ld.Email='test@email.com';
        ld.Industry = 'Education';
        ld.Country_Picklist__c = 'AFGHANISTAN';
        ld.Request_for_Conversion__c = true;
        ld.Sales_Type__c = 'GW';
        insert ld; 
      
        
        Action_Item__c objActionItem1 = new Action_Item__c();              
        objActionItem1.Lead__c = ld.id;
        objActionItem1.Due_Date__c = System.Today() + 5;
        objActionItem1.RecordTypeId = RecordtypId;
        objActionItem1.Region__c = 'Asia GSP';
        objActionItem1.Subject__c = globalConst[0].value__c;
        objActionItem1.Status__c = globalConst[1].value__c;
        objActionItem1.Priority__c = globalConst[2].value__c;
        objActionItem1.ownerId = userInfo.getUserId();
        insert objActionItem1; 
        system.assert(objActionItem1!=null);
        
        List<Action_Item_Owner_Assignment__c>ListActItmAssig = new List<Action_Item_Owner_Assignment__c>();
        Action_Item_Owner_Assignment__c ActItmAssig = new Action_Item_Owner_Assignment__c();
        ActItmAssig.Name = 'Lead Conversion - Asia GSP';
        ActItmAssig.Module__c = 'Lead Conversion Request';
        ActItmAssig.Region__c = 'Asia';
        ActItmAssig.Region_AND_Customer_Type__c = 'Asia GSP';
        ActItmAssig.Region_And_Customer_Type__c ='Account_Admin_Support';  
        ListActItmAssig.add(ActItmAssig); 
        
        Action_Item_Owner_Assignment__c ActItmAssig1 = new Action_Item_Owner_Assignment__c();
        ActItmAssig1.Name = 'Lead Conversion - US MNC';
        ActItmAssig1.Module__c = 'Request';
        ActItmAssig1.Region__c = 'US';
        ActItmAssig1.Region_AND_Customer_Type__c = 'Asia GSP';
        ActItmAssig1.Region_And_Customer_Type__c ='Account_Admin_Support';  
        ListActItmAssig.add(ActItmAssig1); 
        
        insert ListActItmAssig;
         
        string leaIds = ld.id; 
        
        try{
            ActionItemOwnerAssignment.assignActionItemOwnertoLead(leaIds);
        Test.stopTest();
        
        }catch(Exception e){} 
        
        
    }
    
}