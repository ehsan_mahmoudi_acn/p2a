/**
    @author - Accenture
    @date - 30-Jul-2012
    @version - 1.0
    @description - This class is used as controller for the Common Mapper Object.
*/
public class CommonMapperController {
    
    @future
    public static void setUserAgentMapperDetails(String sourceApiName, String targetApiName, Set<String> eligibleUserIds){
        List<Agent__c> agent_List = new List<Agent__c>();
         /*
        Map<Id,String> mapProfile = new Map<Id,String>();
               List<Profile> pr = [SELECT Id,Name FROM Profile];
        for(Profile p: pr){
            mapProfile.put(p.Id,p.Name);
        }*/
                
        List<Common_Mapper__c> fCm = [SELECT Source_Field_API_Name__c , Destination_Field_API_Name__c, 
                                            Related_ID__c, Reference_Object_API_Name__c,
                                            Reference_Object_API_Field_Name__c 
                                            FROM Common_Mapper__c 
                                            WHERE Source_Object_API_Name__c =:sourceApiName AND 
                                            Destination_Object_API_Name__c =:targetApiName];
                                            

        String fildLst = '';
        for(Common_Mapper__c cm : fCm){
            fildLst+=', '+cm.Source_Field_API_Name__c;
        }
        
        if(fildLst == ''){
            System.debug('No mapping exists');
        }else {    
            String selct = 'SELECT ';
            String frm = ' FROM User where id in :eligibleUserIds';
            
    
            try{
                String qry = selct + fildLst.substring(1) + frm;

                List<User> u = Database.query(qry); // database query
                
                
                for(User user: u){
                    Agent__c agentObj = new Agent__c();
                    for(Common_Mapper__c cm : fCm){
                        agentObj.put(cm.Destination_Field_API_Name__c,user.get(cm.Source_Field_API_Name__c));
                        system.debug('Agent Value:' +agentObj.get(cm.Destination_Field_API_Name__c));
                    }
                    agent_List.add(agentObj);
                }
                             
                //upsert agent ID__c; //Upsert with external ID
                upsert agent_List ID__c;
            }catch (DmlException e2) {   
                ErrorHandlerException.ExecutingClassName='CommonMapperController:setUserAgentMapperDetails';
                ErrorHandlerException.sendException(e2);
                System.debug('Error while creating a agent record '+ e2.getMessage()); 
                    
            }
         }
    }
   
@future
       public static void createSOVPerformanceTrackingforSIPUser(Set<String> fiscalYrSet,Map<Id,Date> mapUserIdPeriod,Map<Id,Date> mapUserIdPeriodE,Set<Id> userIds){
           List<Performance_Tracking__c> performanceTrackingList=new List<Performance_Tracking__c>();
        Map<Id,Performance_Tracking__c> mapUserIdPerf=new Map<Id,Performance_Tracking__c>(); 
        // Added a where clause to the query to fetch Performance Tracking records for only the concerned Opportunity -- Bhargav
        performanceTrackingList=[select Currency__c,Q1_Individual_Performance_Opp__c,Q1_Rolled_Up_Opp__c,Q2_Individual_Performance_Opp__c,Q2_Rolled_Up_Opp__c,Q3_Individual_Performance_Opp__c,Q3_Rolled_Up_Opp__c,Q4_Individual_Performance_Opp__c,Q4_Rolled_Up_Opp__c,CurrencyIsoCode,OwnerId,Credit_Debit_Date__c,Q1_SOV_Credit__c,Q2_SOV_Credit__c,Q3_SOV_Credit__c,Q4_SOV_Credit__c,Opportunity_Look_Up__c,SOV_Type__c,UserRoleId__c,Employee__c,Employee__r.UserRoleId,Q1_End_date__c,Q1_Start_date__c,Q2_End_date__c,Q2_Start_date__c,Fiscal_year__c,Q3_End_date__c,Q3_Start_date__c,Q4_End_date__c,Q4_Start_date__c,FY_End_Date__c,FY_Start_date__c,Q1_Individual_Performance__c,Q1_Rolled_Up__c,Q1_Target__c,Q2_Individual_Performance__c,Q2_Rolled_Up__c,Q2_Target__c,Q3_Individual_Performance__c,Q3_Rolled_Up__c,Q3_Target__c,Q4_Individual_Performance__c,Q4_Rolled_Up__c,Q4_Target__c,Q1_GAM_Roll_Up__c,Q2_GAM_Roll_Up__c,Q3_GAM_Roll_Up__c,Q4_GAM_Roll_Up__c from Performance_Tracking__c where Fiscal_year__c IN:fiscalyrSet and SOV_Type__c= :'Performance'];
            for(Performance_Tracking__c perfObj:performanceTrackingList){
                mapUserIdPerf.put(perfObj.Employee__c,perfObj);
                
            }
      Performance_Tracking__c  perfobj=new Performance_Tracking__c();
      //List<User> userlist=[select id,SIP_User__c from User where id IN:userIds];
        for(User usrObj:[select id,SIP_User__c from User where id IN:userIds]){
            
    if(mapUserIdPerf!=null && !mapUserIdPerf.containskey(usrObj.Id) && usrObj.SIP_User__c){
                perfobj=new Performance_Tracking__c();
                perfobj.Employee__c=usrObj.Id;
                perfobj.Fiscal_year__c=mapUserIdPeriod.get(usrObj.Id).year()+'-'+mapUserIdPeriodE.get(usrObj.Id).year();
                perfobj.FY_Start_date__c=mapUserIdPeriod.get(usrObj.Id);
                perfobj.FY_End_Date__c=mapUserIdPeriodE.get(usrObj.Id);
                perfobj.Q1_Rolled_Up__c=0;
                perfobj.Q1_Individual_Performance__c=0;
                perfobj.Q2_Rolled_Up__c=0;
                perfobj.Q2_Individual_Performance__c=0;
                perfobj.Q3_Rolled_Up__c=0;
                perfobj.Q3_Individual_Performance__c=0;
                perfobj.Q4_Rolled_Up__c=0;
                perfobj.Q4_Individual_Performance__c=0;
                perfobj.Q1_GAM_Roll_Up__c=0;
                perfobj.Q2_GAM_Roll_Up__c=0;
                perfobj.Q3_GAM_Roll_Up__c=0;
                perfobj.Q4_GAM_Roll_Up__c=0;
                perfobj.Q1_Rolled_Up_Opp__c=0;
                perfobj.Q1_Individual_Performance_Opp__c=0;
                perfobj.Q2_Rolled_Up_Opp__c=0;
                perfobj.Q2_Individual_Performance_Opp__c=0;
                perfobj.Q3_Rolled_Up_Opp__c=0;
                perfobj.Q3_Individual_Performance_Opp__c=0;
                perfobj.Q4_Rolled_Up_Opp__c=0;
                perfobj.Q4_Individual_Performance_Opp__c=0;
                perfobj.OwnerId=perfobj.Employee__c;
                perfobj.SOV_Type__c='Performance';
                perfobj.Currency__c='AUD';
                perfobj.CurrencyIsoCode=perfobj.Currency__c;
                
                performanceTrackingList.add(perfobj);
                
                 perfobj=new Performance_Tracking__c();
                perfobj.Employee__c=usrObj.Id;
                perfobj.Fiscal_year__c=mapUserIdPeriod.get(usrObj.Id).year()+'-'+mapUserIdPeriodE.get(usrObj.Id).year();
                perfobj.FY_Start_date__c=mapUserIdPeriod.get(usrObj.Id);
                perfobj.FY_End_Date__c=mapUserIdPeriodE.get(usrObj.Id);
                perfobj.Q1_Rolled_Up__c=0;
                perfobj.Q1_Individual_Performance__c=0;
                perfobj.Q2_Rolled_Up__c=0;
                perfobj.Q2_Individual_Performance__c=0;
                perfobj.Q3_Rolled_Up__c=0;
                perfobj.Q3_Individual_Performance__c=0;
                perfobj.Q4_Rolled_Up__c=0;
                perfobj.Q4_Individual_Performance__c=0;
                perfobj.SOV_Type__c='Transaction CR/DR';
                perfobj.Opportunity_Look_Up__c=null;
                perfobj.Q1_SOV_Credit__c=0;
                perfobj.Q2_SOV_Credit__c=0;
                perfobj.Q3_SOV_Credit__c=0;
                perfobj.Q4_SOV_Credit__c=0;
                perfobj.Q1_GAM_Roll_Up__c=0;
                perfobj.Q2_GAM_Roll_Up__c=0;
                perfobj.Q3_GAM_Roll_Up__c=0;
                perfobj.Q4_GAM_Roll_Up__c=0;
                 //sov in Opp currency
                perfobj.Q1_Rolled_Up_Opp__c=0;
                perfobj.Q1_Individual_Performance_Opp__c=0;
                perfobj.Q2_Rolled_Up_Opp__c=0;
                perfobj.Q2_Individual_Performance_Opp__c=0;
                perfobj.Q3_Rolled_Up_Opp__c=0;
                perfobj.Q3_Individual_Performance_Opp__c=0;
                perfobj.Q4_Rolled_Up_Opp__c=0;
                perfobj.Q4_Individual_Performance_Opp__c=0;
               
                perfobj.OwnerId=perfobj.Employee__c;
                perfobj.Credit_Debit_Date__c=system.now();
                
               perfobj.Currency__c='AUD';
               perfobj.CurrencyIsoCode=perfobj.Currency__c;
               
            performanceTrackingList.add(perfobj);




        }
        }
     if(performanceTrackingList.size()>0){upsert performanceTrackingList;}   
       }
}
