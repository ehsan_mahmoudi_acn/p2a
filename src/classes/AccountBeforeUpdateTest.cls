/**
    @author - Accenture
    @date - 17- Jul-2012
    @version - 1.0
    @description - This is the test class for AccountBeforeUpdateTrigger
*/
@isTest
private class AccountBeforeUpdateTest {
    static Account a;
    static Country_Lookup__c cl;
    
   
	/*
    static testMethod void myUnitTest() {
        a = getAccount();
        a.Selling_Entity__c = 'Telstra Incorporated';
        update a;
        a =[SELECT Id,Is_updated__c from Account where Id =: a.Id];
        system.assert(a!=null);
      //  system.assertEquals(a.Is_updated__c,true);
    }
	*/
    private static Account getAccount(){
        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Account' LIMIT 1];
        if(a == null){    
            a = new Account();
            Country_Lookup__c country = getCountry();
            a.Name = 'Test Account';
            a.Customer_Type__c = 'MNC';
            a.Country__c = country.Id;
            a.Selling_Entity__c = 'Telstra INC';
            a.Account_Manager__c = 'Albert Thomas';
            a.Activated__c = true;
            a.RecordTypeId = rt.Id;
            a.Account_Status__c ='Active';
            a.HasOpportunityAboveCustom__c =True;
            a.Account_ID__c = '9090';
            a.Customer_Legal_Entity_Name__c = 'Test';
            
            a.Postal_Code__c='123456789';
            insert a;
              list<account> prod = [select id,Name from account where Name = 'Test Account'];
              system.assertEquals(a.Name , prod[0].Name);
        System.assert(a!=null); 
        }
        return a;
    }
    private static Country_Lookup__c getCountry(){
        if(cl == null){ 
            cl = new Country_Lookup__c();
            cl.CCMS_Country_Code__c = 'SG';
            cl.CCMS_Country_Name__c = 'India';
            cl.Country_Code__c = 'US';
            insert cl;
            system.assert(cl!=null);
        }
        return cl;
    } 
}