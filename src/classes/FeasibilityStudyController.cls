/**
    @author - Accenture
    @date - 19-April-2012
    @version - 1.0
    @description - This class is used as controller .
*/

public class FeasibilityStudyController
{ 
    List<OppProductWrapper> accountList = new List<OppProductWrapper>();
   
    List<OppProductWrapper> selaccountList = new List<OppProductWrapper>();
    List<OppProductWrapper> selectedOppProd = new List<OppProductWrapper>();
        String actId;
        Id actOwnerId;
        public FeasibilityTask__c ftask{get;set;}
    public List<OppProductWrapper> getOppProducts()
    {     
        return accountList;
    }
   
    public List<OppProductWrapper> getSelOppProducts()
    {     
        return selaccountList;
    }
    public FeasibilityStudyController()
    {
                actId = System.currentPageReference().getParameters().get('actId');        
                Action_Item__c act = [Select Id,Opportunity__c,OwnerId from Action_Item__c where Id=:actId];        
                   actOwnerId = act.ownerId;
     //acctId =System.currentPageReference().getParameters().get('accountId');
        for(OpportunityLineItem a : [SELECT ProductCode__c,PricebookEntry.Product2.ProductCode,Feasibility_Study_Required__c,PricebookEntry.product2.Name,CPQItem__c 
     FROM OpportunityLineItem where OpportunityId=: act.Opportunity__c  ]){
        accountList.add(new OppProductWrapper(a.PricebookEntry.product2.Name,a.CPQItem__c,  a.Feasibility_Study_Required__c,a.PricebookEntry.Product2.ProductCode ));                 
        }
        ftask = new FeasibilityTask__c();  
    }
    
    public PageReference getSelected()
    {
        selectedOppProd.clear();
        for(OppProductWrapper accwrapper : accountList){
        //System.debug('Going Under Wrapper' + accwrapper.selected);
        if(accwrapper.selected == true)
        selectedOppProd.add(accwrapper);
        }
        return null;
    }
    
    public PageReference createFeasibilityStudy()
    {        
       List<FeasibilityTask__c> allActions = new List<FeasibilityTask__c>();
        for (OppProductWrapper wrap : accountList){
        if (wrap.selected==true){
                //System.debug('inside Action');
               FeasibilityTask__c fbtask = new FeasibilityTask__c();
               fbtask.Action_Item__c = actId;
               fbtask.Due_Date__c = ftask.Due_Date__c;
               fbtask.Product__c = wrap.productName;
               fbtask.ownerId= ftask.ownerid;
               fbtask.status__c = 'Assigned';
               allActions.add(fbtask);
           }
        }        
        insert allActions;
        // Send a Email to the OwnerId for the Entire Product List 
        /*String[] email;
        boolean isUser = false;
        if (((String)ftask.OwnerId).startswith('005')){
            isUser=true;    
        }
        List<User> usrGroupMembers = new List<User>();
        Set<Id> userIds = new Set<Id>();
        Set<Id> usrGrpids = new Set<Id>();
        Set<Id> nxtUsrIds = new Set<Id>();
        if (isUser){
            User u =[Select Id,FirstName,email from User where Id=:ftask.ownerid];
            email = new String[1];
            email[0] = u.email;
        }else{
            List<GroupMember> grpMembers= [Select UserOrGroupId From GroupMember where GroupId =:ftask.ownerid];
            for (GroupMember grpMem : grpMembers){
                if (((String)grpMem.UserOrGroupId).startswith('005')){
                    // For User
                    userIds.add(grpMem.UserOrGroupId);
                }else{
                    // For Group
                    usrGrpids.add(grpMem.UserOrGroupId);
                }
            } 
        }
        if (userIds.size()>0){
            List<User> newUserMem = [Select Id,FirstName,email from User where Id in: userIds];
            for (User u : newUserMem){
                usrGroupMembers.add(u);
            }
        }
        
        if (usrGrpids.size()>0){
            List<GroupMember> grpUserMembers= [Select UserOrGroupId From GroupMember where GroupId in :usrGrpids];
            for (GroupMember grpMem : grpUserMembers){
                if (((String)grpMem.UserOrGroupId).startswith('005')){
                    nxtUsrIds.add(grpMem.UserOrGroupId);
                }
            }
        }
        if (nxtUsrIds.size()>0){
            List<User> newUserMem = [Select Id,FirstName,email from User where Id in: nxtUsrIds];
            for (User u : newUserMem){
                usrGroupMembers.add(u);
            }
        }
        if (usrGroupMembers.size()>0){
            email = new String[usrGroupMembers.size()];
            Integer i=0;
            for (User u : usrGroupMembers){
                email[i] = u.email;
                i++;
            }
        }
        
        String htmlContent ='';
        htmlContent='Dear User <br/> The Following Products have been Assigned to Complete the Feasibility Task.<br/> The Products are as Follows : <br/><table><tr><td>Product Name</td><td>Due Date</td></tr>';
                    
        for (FeasibilityTask__c fbtask : allActions){
            htmlContent +='<tr><td>'+fbtask.Product__c+'</td><td>'+fbtask.Due_Date__c+'</td></tr>';
        }
        htmlContent +='</table><br/>';
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setUseSignature(false);
        mail.setToAddresses(email);
        mail.setSubject('New Feasibility Task has been Assigned to you');
        mail.setHtmlBody(htmlContent);*/
         
        // Send the email
       // Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        return new pageReference('/'+actId);
    }
    
    public pageReference cancelRequest(){
        return new pageReference('/'+actId);
    }
    
    public List<SelectOption> getPriorities()
    {
        List<SelectOption> priorities = new List<SelectOption>();
        priorities.add(new SelectOption('High','High'));
        priorities.add(new SelectOption('Medium','Medium'));
        priorities.add(new SelectOption('Low','Low'));
        return priorities;
    }
    
    public List<OppProductWrapper> getSelectedAccounts()
    {
        if(selectedOppProd.size()>0)
        return selectedOppProd;
        else
        return null;
    }    
    
    public class OppProductWrapper
    {
        public String productName{get; set;}
        public Boolean selected {get; set;}
        Public Boolean preselected{get;set;}
        public String item{get;set;}        
        public String prodCode{get;set;}        
        public OppProductWrapper(String a,String item2,Boolean preselect,String procode)
        {
            productName = a;
            selected = false;
            preselected = preselect;
            /*if (preselected){
                selected=true;
            }*/
            item=item2;            
            prodCode = procode;
        }
    }
}