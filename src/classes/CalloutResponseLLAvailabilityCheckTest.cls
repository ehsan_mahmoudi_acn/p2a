@isTest(SeeAllData = false)
private class CalloutResponseLLAvailabilityCheckTest {

    private static List<cscfga__Product_Category__c> productCategoryList;
    private static Map<String, csbb.CalloutResponse> mapCR;
    private static csbb.ProductCategory productCategory;
    private static csbb.CalloutProduct.ProductResponse productResponse;
    private static Map<String, Object> inputMap;
    private static Map<String, Object> resultMap;
    private static String categoryIndicator;
    private static csbb.Result result;
    
    private static Map<String, String> attMap;
    private static Map<String, String> responseFields;
    
    Set<String> myStrings = new Set<String>{'Test1','Test2'};
    public String trids = JSON.serialize(myStrings);
    
    private static void initTestData(){
        
        productCategoryList = new List<cscfga__Product_Category__c>{
            new cscfga__Product_Category__c(Name = 'Product Category 1')
        };
        
        insert productCategoryList;
        system.assertEquals(true,productCategoryList!=null); 

        mapCR = new Map<String, csbb.CalloutResponse>();
        mapCR.put('LLAvailabilityCheck', new csbb.CalloutResponse());
        
        productCategory = new csbb.ProductCategory(productCategoryList[0].Id);
        
        productResponse = new csbb.CalloutProduct.ProductResponse();
        productResponse.fields = new Map<String, String>();
        
        inputMap = new Map<String, Object>();
        resultMap = new Map<String, Object>();
        
      // categoryIndicator = 'Sample message from LocalLoopAvailabilityCheck';
        result = new csbb.Result();
        
      attMap = new Map<String, String>();
       responseFields = new Map<String, String>();
    }
    
    private static testMethod void test() {
    Exception ee = null;
    Set<String> myStrings = new Set<String>{'Test1','Test2'};
    String trids = JSON.serialize(myStrings);
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
             
            initTestData();
             

            // categoryIndicator = 'Sample message from LocalLoopAvailabilityCheck';
            categoryIndicator = 'categoryIndicator';
            CalloutResponseLLAvailabilityCheck calloutResponseLLAvailabilityCheck = new CalloutResponseLLAvailabilityCheck(mapCR, productCategory, productResponse);
            CalloutResponseLLAvailabilityCheck calloutResponseLLAvailabilityCheck1 = new CalloutResponseLLAvailabilityCheck();
   
            System.debug('***** CR Primary: ' + calloutResponseLLAvailabilityCheck.crPrimary);
            System.debug('***** Product Response fields: ' + calloutResponseLLAvailabilityCheck.productResponse.fields);

            resultMap = calloutResponseLLAvailabilityCheck.processResponseRaw(inputMap);
            resultMap = calloutResponseLLAvailabilityCheck.getDynamicRequestParameters(inputMap);
            result = calloutResponseLLAvailabilityCheck.canOffer(attMap, responseFields, productResponse);
            //calloutResponseLLAvailabilityCheck.runBusinessRules(categoryIndicator);
                                      Try{
            calloutResponseLLAvailabilityCheck.runBusinessRules(categoryIndicator); 
            }catch(exception e){
           ErrorHandlerException.ExecutingClassName='calloutResponseLLAvailabilityCheck :test';         
                ErrorHandlerException.sendException(e); 
}
            Test.stopTest();//added
             system.assertEquals(true,calloutResponseLLAvailabilityCheck!=null); 
        
        } catch(Exception e){
        ErrorHandlerException.ExecutingClassName='CalloutResponseLLAvailabilityCheckTest :test';         
        ErrorHandlerException.sendException(e); 


            ee = e;
        } finally {
            //Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
    }

}