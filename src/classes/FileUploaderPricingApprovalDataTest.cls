@isTest
private class FileUploaderPricingApprovalDataTest{
     
    static testMethod void fileUploaderPricingApprovalData() {
    
    
        P2A_TestFactoryCls.sampleTestData();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> pblist  = P2A_TestFactoryCls.getProductBasket(1);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<cscfga__Product_Definition__c> Pdlist1 = P2A_TestFactoryCls.getProductdef(1);
        Pdlist1[0].name = 'TWI Singlehome';
        upsert  Pdlist1[0];
        system.assertEquals(Pdlist1[0].name , 'TWI Singlehome');
        list<CSPOFA__Orchestration_Process_Template__c> orchprocessTemplist = P2A_TestFactoryCls.getOrchestrationProcess(5);    
        List<CSPOFA__Orchestration_Process__c>processList = P2A_TestFactoryCls.getOrchestrationProcesss(5,orchprocessTemplist);    
        List<CSPOFA__Orchestration_Step__c>stepList=P2A_TestFactoryCls.getOrchestrationStep(5,processList);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(5,OrdReqList); 
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,pblist,Pdlist1,pbundlelist,Offerlists);
        List<case>caseList=P2A_TestFactoryCls.getcase(1,accList); 
        proconfig[0].name ='TWI Singlehome';
        upsert proconfig[0];
        system.assertEquals(proconfig[0].name , 'TWI Singlehome');
        
        List<Pricing_Approval_Request_Data__c> papprovalist = P2A_TestFactoryCls.getProductonfigappreq(1,pblist,proconfig);

        //List<Profile> Profiles_Id = [Select id, Name from Profile Where id =:userinfo.getProfileid() and (Name = 'TI Commercial' Or Name = 'System Administrator') Limit 1];
        //System.assertequals(Profiles_Id[0].Name,'System Administrator');
        
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = Id.valueOf(Label.Profile_TI_Commercial)/*profiles_Id[0].id*/, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser123456@testorg.com',EmployeeNumber='123');
        insert u;
        System.assert(U.id!=null); 
        
        cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c();
        insert Pb; 
        
        case cobj = new case();
        Cobj.Product_Basket__c = Pb.id;
        insert cobj;
        System.assert(Cobj.id != null);
        
        Decimal maxcontractterm = 0; 

        Pricing_Approval_Request_Data__c P_Request = new Pricing_Approval_Request_Data__c();
        P_Request.Approved_NRC__c=0;
        P_Request.Approved_RC__c=0;
        P_Request.Offer_NRC__c=0;
        P_Request.Offer_RC__c =0;
        P_Request.Cost_RC__c = 0;
        P_Request.Cost_NRC__c = 0;  
        P_Request.Contract_Term__c = 0;
        P_Request.Is_offnet__c = false;
        P_Request.Product_Basket__c = cobj.Product_Basket__c;
        insert P_Request ;
        system.assertEquals(P_Request.Cost_NRC__c,0);
        
        Blob b = Blob.valueOf('Test Data');
        
        //StaticResource testdoc = [Select Id,Body from StaticResource where name ='testMethodCSVUpload' limit 1];
       
        ApexPages.StandardController sc = new ApexPages.standardController(cobj);
        FileUploaderPricingApprovalData fp= new FileUploaderPricingApprovalData(sc); 
        //fp.contentFile= testdoc.Body;  
        //fp.nameFile = 'https://telstra--p2aval.cs57.my.salesforce.com/01p0k0000004GK7AAM';
        fp.message = 'abc';
        String str='http,://down,load\n'+P_Request.id+',.think,broad,band.\ncom/,5MB.\nzi,p';
        fp.contentFile= Blob.valueof(str);
        fp.nameFile=FileUploaderPricingApprovalData.blobToString( fp.contentFile,'ISO-8859-1');
        fp.ReadFile(); 
        string blobToString =  FileUploaderPricingApprovalData.blobToString(b, 'UTF-8');          
    } 
   
   
   
   static testMethod void fileUploaderPricingApprovalData1() {

        P2A_TestFactoryCls.sampleTestData();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> pblist  = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Pdlist1 = P2A_TestFactoryCls.getProductdef(1);
        Pdlist1[0].name = 'TWI Singlehome';
        upsert  Pdlist1[0];
        system.assertEquals(Pdlist1[0].name, 'TWI Singlehome');
         list<CSPOFA__Orchestration_Process_Template__c> orchprocessTemplist = P2A_TestFactoryCls.getOrchestrationProcess(5);    
        List<CSPOFA__Orchestration_Process__c>processList = P2A_TestFactoryCls.getOrchestrationProcesss(5,orchprocessTemplist);    
        List<CSPOFA__Orchestration_Step__c>stepList=P2A_TestFactoryCls.getOrchestrationStep(5,processList);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(5,OrdReqList); 
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,pblist,Pdlist1,pbundlelist,Offerlists);
        List<case>caseList=P2A_TestFactoryCls.getcase(1,accList); 
        proconfig[0].name ='TWI Singlehome';
        upsert proconfig[0];
        system.assertEquals(proconfig[0].name, 'TWI Singlehome');
        
        List<Pricing_Approval_Request_Data__c> papprovalist = P2A_TestFactoryCls.getProductonfigappreq(1,pblist,proconfig);
        papprovalist[0].Approved_RC__C = Decimal.valueOf(7);
        update papprovalist ;
        
        List<User> users = P2A_TestFactoryCls.get_Users(1);
   
        //List<Profile> Profiles_Id = [Select id, Name from Profile Where id =:userinfo.getProfileid() and (Name = 'TI Commercial' Or Name = 'System Administrator') Limit 1];
        //System.assertequals(Profiles_Id[0].Name,'System Administrator');
        
        /*User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = profiles_Id[0].id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser123456@testorg.com',EmployeeNumber='123');
        insert u;*/
        //System.assert(users.id!=null); 
        
        //cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c();
        //insert Pb; 
        
        //case cobj = new case();
        caseList[0].Product_Basket__c = Pblist[0].id;
        update caseList;
        System.assert(caseList[0].id != null);
        
        Decimal maxcontractterm = 0; 

           
        
        Blob b = Blob.valueOf('TestData\nTest,Data.test1\ntest23,test1567');
        
        //StaticResource testdoc = [Select Id,Body from StaticResource where name ='testMethodCSVUpload' limit 1];
       
        ApexPages.StandardController sc = new ApexPages.standardController(caselist[0]);
        FileUploaderPricingApprovalData fp= new FileUploaderPricingApprovalData(sc); 
        //fp.contentFile= testdoc.Body;  
        fp.ReadFile(); 
        string blobToString =  FileUploaderPricingApprovalData.blobToString(b, 'UTF-8');  
          
    } 
   
  }