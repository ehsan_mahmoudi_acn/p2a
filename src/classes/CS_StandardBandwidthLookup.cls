global with sharing class CS_StandardBandwidthLookup extends cscfga.ALookupSearch {
    
    public override String getRequiredAttributes(){ 
        return '[ "IPL","EPL","EPLX","ICBS","EVPL","Country Name","Account type","Network" ]';
    }
    
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){
        /*List <String> ProductTypeNames = new List<String>();
        
            for (String key : searchFields.keySet()){
                String attValue = searchFields.get(key);
                if(attValue == 'Yes' || attValue == 'true')
                ProductTypeNames.add(key);
            }

        String searchValue = searchFields.get('searchValue') +'%';*/
        List <CS_Bandwidth__c> data = [SELECT Id, Name, Bandwidth_Value_MB__c FROM CS_Bandwidth__c ORDER BY Bandwidth_Value_MB__c];
        
        return data;
    }
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
    Integer pageOffset, Integer pageLimit){
        
        
        final Integer SELECT_LIST_LOOKUP_PAGE_SIZE = 25;
        final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = 26;
        Integer recordOffset = pageOffset * SELECT_LIST_LOOKUP_PAGE_SIZE;
        
        String country = searchFields.get('Country Name');      
        String accCustomerType = searchFields.get('Account type'); 
        String network = searchFields.get('Network');
        
        /*  String aEndCountry = searchFields.get('A-End Country');
        Set <Id> bwIds = new Set<Id>();
        List<CS_Bandwidth__c> bandlist = new List<CS_Bandwidth__c>();
        
        List <String> ProductTypeNames = new List<String>();
        
        for (String key : searchFields.keySet()){
            String attValue = searchFields.get(key);
            if(attValue == 'Yes' || attValue == 'true')
            ProductTypeNames.add(key);
        }
        
        List<CS_Bandwidth__c> bandlist = [SELECT Id 
                                            FROM CS_Bandwidth__c];
        
        for(CS_Bandwidth__c item : bandlist){
            bwIds.add(item.Id);
        } */
        
        System.Debug('doLookupSearch');
        System.Debug(searchFields);
        String searchValue = '%' + searchFields.get('searchValue') +'%';
        List <CS_Bandwidth__c> data;
        
        Set<String> TIDBandwidths = new Set<String>();
           List<TID_Bandwidths__c> allTIDBandwidths = TID_Bandwidths__c.getall().values();  
           for(TID_Bandwidths__c BW: allTIDBandwidths)
             {
                TIDBandwidths.add(BW.TID_Bandwidth__c);

             }
        
      if ((accCustomerType =='MNC' || accCustomerType =='ISO') && network =='Australia (AS 1221)' && country=='AUSTRALIA'){
          data = [SELECT Id, Name, Bandwidth_Value_MB__c 
            FROM 
                CS_Bandwidth__c 
            WHERE 
                Name IN :TIDBandwidths
            ORDER BY 
                Bandwidth_Value_MB__c 
            LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset];
      }  
      else {
          data = [SELECT Id, Name, Bandwidth_Value_MB__c 
            FROM 
                CS_Bandwidth__c 
            WHERE 
                Name LIKE :searchValue
            ORDER BY 
                Bandwidth_Value_MB__c 
            LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset];
         } 
         
            
        System.Debug(data);
        return data;
        
    }
    
}