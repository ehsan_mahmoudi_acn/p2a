@istest(seealldata = false)
 Public class PricingapprovalEXT_Test{
   /* @istest
    Public static void pricingapprovalextmtd(){
       P2A_TestFactoryCls.sampleTestData();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Proddef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,Products,proddef,pbundlelist,Offerlists);       
         List<cscfga__Configuration_Screen__c> configscreen = P2A_TestFactoryCls.getConfigScreen(1,Proddef);
          List<cscfga__Screen_Section__c> screen = P2A_TestFactoryCls.getScreenSec(1,configscreen);
        List<cscfga__Attribute_Definition__c > Attributesdeflist = P2A_TestFactoryCls.getAttributesdef(1,proconfig ,proddef,configscreen,screen);
        List<cscfga__Attribute__c> Attributeslist = P2A_TestFactoryCls.getAttributes(1, proconfig,Attributesdeflist);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
    
      List<Profile> Profiles_Id = [Select id, Name from Profile Where id =:userinfo.getProfileid() and (Name = 'TI Commercial' Or Name = 'System Administrator') Limit 1];
        System.assertequals(Profiles_Id[0].Name,'System Administrator');
        
        User u = new User(Alias = 'standt', Email='standarduser1233@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = profiles_Id[0].id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser123as4561234789@testorg.com',EmployeeNumber='123');
        insert u;
        System.assert(U.id!=null);
       
       system.runas(u){    
    
        cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c();
        pb.currencyisocode = 'USD';
        insert Pb; 
        
        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c();
        pc.name = 'IPL';
        pc.Cscfga__Product_Basket__c = pb.id;
        pc.currencyisocode = pb.currencyisocode;
        insert Pc; 
        
        case cobj = new case();
        Cobj.Product_Basket__c = Pb.id;
        cobj.currencyisocode = pb.currencyisocode; 
        insert cobj;
        System.assert(Cobj.id != null);
        
        
        Decimal maxcontractterm = 0; 
        
        Pricing_Approval_Request_Data__c P_Request = new Pricing_Approval_Request_Data__c();
        P_Request.Approved_NRC__c=0;
        P_Request.Approved_RC__c=0;
        P_Request.Offer_NRC__c=0;
        P_Request.Offer_RC__c =0;
        P_Request.Cost_RC__c = 0;
        P_Request.Cost_NRC__c = 0;  
        P_Request.Contract_Term__c = 0;
        P_Request.Is_offnet__c = false;
        P_Request.Product_Basket__c = cobj.Product_Basket__c;
        P_Request.currencyisocode = pb.currencyisocode; 
        insert P_Request ;
        System.assert(P_Request.id != null);
        
        ApexPages.StandardController sc = new ApexPages.standardController(Cobj);
      //  PricingFinancialSummaryManishcls PFSM = new PricingFinancialSummaryManishcls(sc);   
       // PFSM.getisvisible(); 
       // PFSM.initlize();
        
        cscfga__Product_Basket__c Cpb = new cscfga__Product_Basket__c();
        Cpb .currencyisocode = 'USD'; 
        insert CPb;
        System.assert( Cpb.id!= null ); 
        
        case csobj = new case();
        Csobj.Product_Basket__c = CPb.id;
        Csobj.currencyisocode = CPb.currencyisocode; 
        insert csobj; 
        System.assert(Csobj.id != null);
 
        maxcontractterm = 0; 
        
        Pricing_Approval_Request_Data__c Pf_Request = new Pricing_Approval_Request_Data__c();
        Pf_Request.Approved_NRC__c=0;
        Pf_Request.Approved_RC__c=0;
        Pf_Request.Offer_NRC__c=0;
        Pf_Request.Offer_RC__c =0;
        Pf_Request.Cost_RC__c = 0;
        Pf_Request.Cost_NRC__c = 0;  
        Pf_Request.Contract_Term__c = 0;
        Pf_Request.Is_offnet__c = true;
        Pf_Request.Product_Basket__c = csobj.Product_Basket__c;
        Pf_Request.CurrencyIsoCode= cobj.Product_Basket__r.CurrencyIsoCode;
        insert Pf_Request ; 
        System.assert(Pf_Request != null);
  
        ApexPages.StandardController scon = new ApexPages.standardController(Csobj);
        PricingFinancialSummaryManishcls PFSMS = new PricingFinancialSummaryManishcls(scon);   
        PFSMS.getisvisible();  
        PFSMS.initlize();
        
        cscfga__Product_Basket__c Cpbs = new cscfga__Product_Basket__c();
        cpbs.Onnet_vs_Offnet__c = 21;
        cpbs.Gross_Margin__c = 10.00;
        cpbs.Max_Contract_Term__c = 24;
        cpbs.Capex__c = 1536.67;
        cpbs.CurrencyIsoCode = 'USD'; 
        insert CPbs;
        System.assert(CPbs != null);
  
        case casobj = new case();
        Casobj.Product_Basket__c = CPbs.id;
        Casobj.Order_type__c='new';
        Casobj.CurrencyIsoCode = CPbs.CurrencyIsoCode;
        insert casobj;
        System.assert(Casobj != null);
 
        maxcontractterm = 24; 
        Decimal cterm = maxcontractterm/12 ;
        
         Pricing_Approval_Request_Data__c Paf_Request1 = new Pricing_Approval_Request_Data__c();
        Paf_Request1 .Approved_NRC__c=0;
        Paf_Request1 .Approved_RC__c=0;
        Paf_Request1 .Offer_NRC__c=0;
        Paf_Request1 .Offer_RC__c =0;
        Paf_Request1 .Cost_RC__c = 0;
        Paf_Request1 .Cost_NRC__c = 0;  
        Paf_Request1 .Contract_Term__c = 24;
        Paf_Request1 .Is_offnet__c = false;
        Paf_Request1 .Product_Configuration__c = pc.id;
        Paf_Request1 .Product_Basket__c = casobj.Product_Basket__c;

        insert Paf_Request1  ;
        Exception ee = null;
    
         test.startTest();
         //Pricing_Approval_Request_Data__c 
         PricingApprovalExt pext = new PricingApprovalExt(new ApexPages.StandardController(casobj));
         
         Pricing_Approval_Request_Data__c Paf_Request = new Pricing_Approval_Request_Data__c();
        Paf_Request.Approved_NRC__c=0;
        Paf_Request.Approved_RC__c=0;
        Paf_Request.Offer_NRC__c=0;
        Paf_Request.Offer_RC__c =0;
        Paf_Request.Cost_RC__c = 0;
        Paf_Request.Cost_NRC__c = 0;  
        Paf_Request.Contract_Term__c = 24;
        Paf_Request.Is_offnet__c = false;
        Paf_Request .Product_Configuration__c = pc.id;
        Paf_Request.Product_Basket__c = casobj.Product_Basket__c;
        insert Paf_Request ;
        System.assert(Paf_Request != null);
        
      
                      
                        Case caseObj = new Case();
                        caseObj.Origin                       = 'Web';
                        caseObj.Is_Supplier_Quote_Request__c = true;
                        caseObj.Status                       = 'New';
                        caseObj.Is_Child_Case__c             = true;
                        caseObj.ParentID                     = casobj.id;
                        caseobj.Product_Configuration__c     = proconfig[0].id;
                        caseObj.Subject                      = 'Supplier quote request for ';
                        caseObj.Product_Basket__c            = Products[0].id;
                        caseObj.Opportunity_Name__c          = Opplist[0].id;
                        caseObj.accountId                    = acclist[0].id;
                        caseObj.CS_Service__c                = servList[0].id;
                        caseObj.Subscription__c              = Sublist[0].id;
                        insert caseobj;
        
         PricingApprovalExt.PricingApprovalWrapper pricAppWrapObj = new PricingApprovalExt.PricingApprovalWrapper(Paf_Request,true);
         pext.pc.add(pricAppWrapObj);
         
        
 
   PricingApprovalExt pext1 = new PricingApprovalExt(new ApexPages.StandardController(caseobj));
         
         
         Pagereference pg = pext.createCase();
         pext.requestSupplierQuote();
         Boolean b =pext.getisVisible();
         pg = pext.updateItems();
        
         }
          
        } */
     }