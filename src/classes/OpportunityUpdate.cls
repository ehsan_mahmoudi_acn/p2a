public Class OpportunityUpdate{
public Static Integer OppCount=0;
Public Static Boolean flag=false;
  
  /*public static  void updateAccountContactSites(){
     Set<Id> oppAccid = new Set<Id>();    
     Set<Id> accountIds = new set<Id>();
    
     Map<Id,Boolean> accHasOppCustomMap = new Map<Id,Boolean>();
        
      for(integer i= 0; i < Trigger.new.size();i++){
           Opportunity opp=(Opportunity)Trigger.new[i];
           accountIds.add(opp.AccountId);
     }
      
     //List<Account> allOppAccount = [Select Id, Name,HasOpportunityAboveCustom__c from Account where Id in :accountIds ];
     for (Account acc: [Select Id, Name,HasOpportunityAboveCustom__c from Account where Id in :accountIds ]){
         accHasOppCustomMap.put(acc.Id,acc.HasOpportunityAboveCustom__c); 
     }
     
      for(integer i= 0; i < Trigger.new.size();i++){
                   Opportunity opp=(Opportunity)Trigger.new[i];
                   if (!accHasOppCustomMap.get(opp.AccountId)){
                        if (opp.probability>=60){
                          oppAccid.add(opp.AccountId);  
                        }                    
                    }
                }*/
               /* List<Account> allAccounts = [Select Id,Name,HasOpportunityAboveCustom__c from Account where id in : oppAccid];
                List<Contact> allContacts = [Select Id,Name,email, Accountid from Contact where AccountId in : oppAccId];
                List<Site__c> allSites = [Select Id,Name, AccountId__c from Site__c where AccountId__c in : oppAccId];
                List<BillProfile__c> allBillprofiles = [Select Id,Name,Account__c from BillProfile__c where Account__c in : oppAccId];
            */
             // Now update all the Accounts, Contacts and Sites of the specific OppId.
          /*   for (Account acc : allAccounts){
             
               acc.HasOpportunityAboveCustom__c = true; 
             }
             update allAccounts;*/
           /*  for (Contact con : allContacts)
             {
                 con.isOppUpdate__c = true;
             }
             update allContacts;*/
           /*  for (Site__c sit : allSites)
             {
                 sit.isOppUpdate__c = true;
             }
             update allSites;*/
           
           /* for (BillProfile__c bil : allBillprofiles)
             {
                 bil.isOppUpdate__c = true;
             }
             update allBillprofiles;*///Now  update all the Bill Profile Associated with the Account             
            // } 

     
            public static void doAdd(){
   
               OppCount++;
      
            }  
            public static Integer getcount(){
                return OppCount;
            }
      //Added as part of CR-271 
       public void  billProfileApproval(Set<Opportunity> OppObj ){
       System.debug('*********Inside Bill profile Approval*************');
       Action_Item__c actionObj = new Action_Item__c();       
       List<Action_Item__c> actionObjList =  new  List<Action_Item__c>();
       User curntUser = [Select Name, Region__c from User where id= :UserInfo.getUserId() limit 1];
       Map<Id,Set<Id>> BillProfileMap = new Map<Id,Set<Id>>();
       Map<Id,Map<Id,String>> BillProfileNumberMap = new Map<Id,Map<Id,String>>();
       Map<Id,Id> oppAccMap = new  Map<Id,Id>();
       Set<Id> tempBPIds = new Set<Id>();
       String BPIds;
       String instanceURL = URL.getSalesforceBaseUrl().toExternalForm() + '/';
       
       for(Opportunity Opp : OppObj){
       
            oppAccMap.put(opp.id,opp.AccountId);
            system.debug('oppAccMap' + oppAccMap);
        }
       
           for(OpportunityLineItem opplineobj : [select Id,OpportunityId,BillProfileId__c,BillProfileId__r.Bill_Profile_ORIG_ID_DM__c from OpportunityLineItem where OpportunityId in : oppAccMap.keySet() and BillProfileId__r.Approved__c = false and BillProfileId__c != null]){
                
                if(BillProfileMap.containskey(opplineobj.OpportunityId)){
                
                    BillProfileMap.get(opplineobj.OpportunityId).add(opplineobj.BillProfileId__c);
                    (BillProfileNumberMap.get(opplineobj.OpportunityId)).put(opplineobj.BillProfileId__c,opplineobj.BillProfileId__r.Bill_Profile_ORIG_ID_DM__c);
                }else{
                
                   Set<Id> bpIdlist = new Set<Id>();
                    Map<Id,String> bpNumberMap = new Map<Id,String>();
                    bpIdlist.add(opplineobj.BillProfileId__c);
                    bpNumberMap.put(opplineobj.BillProfileId__c,opplineobj.BillProfileId__r.Bill_Profile_ORIG_ID_DM__c); 
                    BillProfileMap.put(opplineobj.OpportunityId,bpIdlist);
                    BillProfileNumberMap.put(opplineobj.OpportunityId,bpNumberMap);
                }
                
            }
            
             for(Id Oppid : BillProfileMap.keyset()){
                
                tempBPIds.addAll(BillProfileMap.get(Oppid));
                Map<Id,String> bpNumberMap = BillProfileNumberMap.get(OppId);
                for(Id id : tempBPIds){
                    
                    if(BPIds == null)
                        BPIds = instanceURL + id +' BP Number: '+bpNumberMap.get(id);  
                    else
                        BPIds += '\n' + instanceURL + id+' BP Number: '+bpNumberMap.get(id);
                
                }
                
                
                
                actionObj = Util.createActionItem ('Request Billing Activation',oppAccMap,Oppid,null);
                actionObj.Subject__c = 'Request for Bill Profile Approval';
                
                actionObj.Credit_Check_Comments__c = System.Label.Email_Body11 + '\n'+'\n'+ System.Label.Email_Body12 + '\n' + '\n'+ BPIds;
                Util.assignOwnersToRegionalQueue( curntUser, actionObj, 'Request for Bill Profile Approval');
                /*List<Action_Item__c> allBillProfileApproval = [Select Id,Subject__c,Opportunity__c from Action_Item__c where Opportunity__c =:Oppid and Subject__c='Request for Bill Profile Approval'];
                if(allBillProfileApproval.size()==0)
                    actionObjList.add(actionObj);           
                 */  
            }
              System.debug('*********Bill profile Approval Action Item Size*************'+actionObjList.size());
            if(actionObjList.size()>0)              
                insert actionObjList; 
      }
      
       public void  billProfileActivation(Set<Opportunity> OppObj ){
       
       Action_Item__c actionObj = new Action_Item__c();       
       List<Action_Item__c> actionObjList =  new  List<Action_Item__c>();
       User curntUser = [Select Name, Region__c from User where id= :UserInfo.getUserId() limit 1];
       Map<Id,Set<Id>> BillProfileMap = new Map<Id,Set<Id>>();
       Map<Id,Map<Id,String>> BillProfileNumberMap = new Map<Id,Map<Id,String>>();
       Map<Id,Id> oppAccMap = new  Map<Id,Id>();
       Set<Id> tempBPIds = new Set<Id>();
       List<id> tempAIBPIds = new List<Id>();
       String BPIds;
       String instanceURL = URL.getSalesforceBaseUrl().toExternalForm() + '/';
       
       for(Opportunity Opp : OppObj){
       
            oppAccMap.put(opp.id,opp.AccountId);
            system.debug('oppAccMap' + oppAccMap);
        }
       
           for(OpportunityLineItem opplineobj : [select Id,OpportunityId,BillProfileId__c, BillProfileId__r.Bill_Profile_ORIG_ID_DM__c from OpportunityLineItem where OpportunityId in : oppAccMap.keySet() and BillProfileId__r.Activated__c = false and BillProfileId__c != null]){
                System.debug('BillProfileMap::>><<:'+BillProfileMap+'>>><<<'+opplineobj.OpportunityId);
                if(BillProfileMap.containskey(opplineobj.OpportunityId)){
                
                    BillProfileMap.get(opplineobj.OpportunityId).add(opplineobj.BillProfileId__c);
                    (BillProfileNumberMap.get(opplineobj.OpportunityId)).put(opplineobj.BillProfileId__c,opplineobj.BillProfileId__r.Bill_Profile_ORIG_ID_DM__c);
                }else{
                
                    Set<Id> bpIdlist = new Set<Id>();
                    Map<Id,String> bpNumberMap = new Map<Id,String>();
                    bpIdlist.add(opplineobj.BillProfileId__c);
                    bpNumberMap.put(opplineobj.BillProfileId__c,opplineobj.BillProfileId__r.Bill_Profile_ORIG_ID_DM__c); 
                    BillProfileMap.put(opplineobj.OpportunityId,bpIdlist);
                    BillProfileNumberMap.put(opplineobj.OpportunityId,bpNumberMap);
                }
                
            }
            
            for(Id Oppid : BillProfileMap.keyset()){
                
                tempBPIds.addAll(BillProfileMap.get(Oppid));
                tempAIBPIds.addAll(BillProfileMap.get(Oppid));
                Map<Id,String> bpNumberMap = BillProfileNumberMap.get(OppId);
                for(Id id : tempBPIds){
                    
                    if(BPIds == null)
                        BPIds = instanceURL + id +' BP Number: '+bpNumberMap.get(id);  
                    else
                        BPIds += '\n' + instanceURL + id+' BP Number: '+bpNumberMap.get(id);
                
                }
                
                
                
                actionObj = Util.createActionItem ('Request Billing Activation',oppAccMap,Oppid,null);
                actionObj.Subject__c = 'Request for Bill Profile Activation';
                if(tempAIBPIds.size() > 0)
                actionObj.Bill_Profile__c = tempAIBPIds[0];
                actionObj.Credit_Check_Comments__c = System.Label.Email_Body11 + '\n'+'\n'+ system.label.Email_Body14 +' '+ curntUser.Name+ ' '+ system.label.Email_Body13+'\n' + '\n'+ BPIds;
                Util.assignOwnersToRegionalQueue( curntUser, actionObj, 'Request for Bill Profile Approval');
                /*List<Action_Item__c> allBillProfileActivation = [Select Id,Subject__c,Opportunity__c from Action_Item__c where Opportunity__c =:Oppid and Subject__c='Request for Bill Profile Activation'];
                if (allBillProfileActivation.size()==0)               
                                actionObjList.add(actionObj);           
                  */ 
            }
            // Check if there are any Bill Profile Activation already associated with the Opportunity.

                if(actionObjList.size()>0)              
                    insert actionObjList; 
           
      } //ends here
       /**
        @param - Set<Opportunity> OppObj
        @return - void noReturn
        @description - Method to auto create credit check action Item when Opportunity moving from Approved to Firm or  Budgetary to firm**/
        public void  creditcheckCreation(Set<Opportunity> OppObj ){
         Map<ID,ID> accountOppMapId = new Map<ID,ID>();
      
        // query Opportunity to get Opportunity Object based on updated Opportunity
         if(OppObj != null && flag == false){ 
         
         Opportunity opp2  = [Select Id,Order_Type__c,CurrencyIsoCode,Region__c,AccountId,Account.Account_ID__c,Account.AccountNumber,Account.Customer_Group__c,ContractTerm__c,Total_Contract_Value__c,Total_MRC__c,Total_NRC__c,Account.Credit_Limit__c,Account.credit_limit_expiry_date__c,Account.type,Account.Business_Registration__c from Opportunity where Id=:OppObj limit 1];
                accountOppMapId.put(opp2.id,opp2.AccountId);
                Action_Item__c action = new Action_Item__c();
                action = Util.createActionItem('Request Credit Check', accountOppMapId, opp2.id, null);
                action.Subject__c = 'Request Credit Check';
               //action.Opportunity__c = opp2.id;
               //action.recordtypeid = rt.id;
                action.Region__c = opp2.Region__c;
                //Querrying current user and checking for the right queue
                List<User> userLst = [select id,name,Region__c from user where Id =:UserInfo.getUserId()];
                User userid = userLst[0];
                Util.assignOwnersToRegionalQueue(userid,action,'Request Credit Check');
                action.Feedback__c = '';
         
                // Check Previous Credit check Date
                action.Account_Number__c= opp2.Account.Account_ID__c;
              
                //Setting the values from Opportunity to Action Item
                action.Total_Contract_Value__c = opp2.Total_Contract_Value__c;
                action.Contract_Term__c = opp2.ContractTerm__c;
                action.MRC_Monthly_Recurring_Charges__c = opp2.Total_MRC__c;
                action.NRC_Non_Recurring_Charges__c= opp2.Total_NRC__c;
                action.Business_Registration_Number__c = opp2.Account.Business_Registration__c;
                action.Billing_Currency__c = opp2.CurrencyIsoCode;
                action.CurrencyIsoCode = opp2.CurrencyIsoCode;
                action.creditlimit__c = opp2.Account.credit_limit__c;
                action.credit_limit_expiry_date__c = opp2.Account.credit_limit_expiry_date__c;
                if(opp2.Account.type == 'Prospect')
                    action.new_existing_customer__c = 'New';
                else
                    action.new_existing_customer__c = 'Existing';   
                
                List<Action_Item__c> lastAction=null;
               List<Action_Item__c> previousActionItem = new List<Action_Item__c>();       
                        
                try{
                    
                     lastAction= [Select Id,Name,Account_Number__c,Opportunity__c,Status__c,Feedback__c,Account__r.Account_ID__c,LastModifiedDate,MRC_Monthly_Recurring_Charges__c,NRC_Non_Recurring_Charges__c,Total_Estimated_MRC__c,Total_Estimated_NRC__c from Action_Item__c where Opportunity__c=:opp2.id  and RecordTypeId=:action.RecordTypeId order by LastModifiedDate Desc];
                           system.debug('lastAction===='+lastAction);       
                    }catch(Exception e){
                        lastAction=null;
                    }
                if (lastAction!=null){
                    if (lastAction.size()!=0){
                        action.Previous_Credit_Check_Date__c=Date.newInstance(lastAction.get(0).LastModifiedDate.year(),lastAction.get(0).LastModifiedDate.month(),lastAction.get(0).LastModifiedDate.day());
                        action.Action_Item_ID_Previous_Credit_Check__c=lastAction[0].Name;                    
                        action.Previous_MRC__c=lastAction[0].MRC_Monthly_Recurring_Charges__c;
                        action.Previous_NRC__c=lastAction[0].NRC_Non_Recurring_Charges__c;
                        action.Previous_Estimated_MRC__c=lastAction[0].Total_Estimated_MRC__c;
                        action.Previous_Estimated_NRC__c=lastAction[0].Total_Estimated_NRC__c;
                        if(action.MRC_Monthly_Recurring_Charges__c != null && lastAction[0].MRC_Monthly_Recurring_Charges__c != null){
                            action.Difference_in_MRC__c=action.MRC_Monthly_Recurring_Charges__c-lastAction[0].MRC_Monthly_Recurring_Charges__c;
                        }
                        if(action.NRC_Non_Recurring_Charges__c != null && lastAction[0].NRC_Non_Recurring_Charges__c != null){
                            action.Difference_in_NRC__c=action.NRC_Non_Recurring_Charges__c-lastAction[0].NRC_Non_Recurring_Charges__c;
                        }

                     // action.feedback__c = lastAction[0].Feedback__c;
                        previousActionItem.add(lastAction[0]);
                        //system.debug('++++++++++previousActionItem========'+previousActionItem);
                    }
                }
                  List<Opportunity> openOpportunities = new List<Opportunity>();
                    try{
                        //added Sales_Status__c to the query and updated stage name closed won and closed lost as per Somp requirement
                     openOpportunities =[Select Total_MRC__c,CurrencyIsoCode,Order_Type__c,Accountid,Total_NRC__c,Account_Number__c,StageName,Sales_Status__c from Opportunity where AccountId =:opp2.AccountId and (StageName!='Prove & Close' and Sales_Status__c!='Won') and Sales_Status__c!='Lost'];
                            //system.debug('openOpportunities===if=='+openOpportunities.size());
                    }catch(Exception e){
                            openOpportunities=null;
                            //system.debug('openOpportunities======'+openOpportunities.size());                                         
                    }
                    if (openOpportunities.size()>0){
                        //Here getting all currencies type and form Map key as Currencycode and value as CurrencyType Object
                        //List<CurrencyType> currencyTypeList = new List<CurrencyType>();
                        Map<String,CurrencyType> isoCodeCurrencyTypeObjMap = new Map<String,CurrencyType>();
                        //currencyTypeList = [Select id,ConversionRate,IsCorporate,IsoCode from CurrencyType];
                        
                        for(CurrencyType currencyTypeObj : [Select id,ConversionRate,IsCorporate,IsoCode from CurrencyType]){
                            isoCodeCurrencyTypeObjMap.put(currencyTypeObj.IsoCode,currencyTypeObj);
                        }
                        
                        Double Mrcvalues=0;
                        Double NrcValues=0;
                            for(Opportunity opp1:openOpportunities){
                                    Double currencyExchangeRateValue = isoCodeCurrencyTypeObjMap.get(opp1.CurrencyIsoCode).ConversionRate;
                                   if(opp1.Total_MRC__c != null ){
                                     Mrcvalues += (opp1.Total_MRC__c/currencyExchangeRateValue).setscale(2);
                                     }
                                   if(opp1.Total_NRC__c != null){
                                     NrcValues += (opp1.Total_NRC__c/currencyExchangeRateValue).setscale(2);
                                   }    
                            }
                            action.Total_MRC_for_all_Open_Opportunities__c = Mrcvalues;
                            action.Total_NRC_for_all_Open_Opportunities__c = NrcValues;
                            //system.debug('Total MRC =========' + action.Total_MRC_for_all_Open_Opportunities__c);
                            //system.debug('Total NRC =========' + action.Total_NRC_for_all_Open_Opportunities__c);       
                   }                        
                   System.debug('testttttttttttt'+action);
                                  if( opp2.Order_Type__c != 'Termination' && opp2.Order_Type__c != 'Relocation' && !(opp2.Order_Type__c == 'Renewal' && opp2.By_Pass_Renewal_Credit_Check__c == true) ){  
                if(lastAction.size() == 0){
                     insert action;
                     flag = true;
                }
                else if(lastAction.size() != 0 && lastAction[0].feedback__c == 'Approved' && (opp2.Total_NRC__c > lastAction[0].NRC_Non_Recurring_Charges__c || opp2.Total_MRC__c > lastAction[0].MRC_Monthly_Recurring_Charges__c)){
                     insert action;
                     flag = true;
                 }
               }      
            }              
        }
     public void  previousCreditcheckupdation(Set<Opportunity> OppObj ){
        
        // query Opportunity to get Opportunity Object based on updated Opportunity
         if(OppObj != null){ 
         Opportunity opp2  = [Select Id,Order_Type__c,CurrencyIsoCode,Region__c,AccountId,Account.Account_ID__c,Account.AccountNumber,Account.Customer_Group__c,ContractTerm__c,Total_Contract_Value__c,Total_MRC__c,Total_NRC__c,Account.Business_Registration__c from Opportunity where Id=:OppObj limit 1];
               List<Action_Item__c> lastAction=null; 
               List<Action_Item__c> previousActionItem = new List<Action_Item__c>();  
           RecordType rt = [select Id,Name from RecordType where Name='Request Credit Check']; 
            Action_Item__c action = new Action_Item__c();
            action.recordtypeId = rt.id;            
                        
                try{                    
                     lastAction= [Select Id,Name,Account_Number__c,Opportunity__c,status__c,Feedback__c,Account__r.Account_ID__c,LastModifiedDate,MRC_Monthly_Recurring_Charges__c,NRC_Non_Recurring_Charges__c,Total_Estimated_MRC__c,Total_Estimated_NRC__c from Action_Item__c where Opportunity__c=:opp2.id  and RecordTypeId=:action.RecordTypeId and Status__c != 'Reversed' order by LastModifiedDate Desc];
                           system.debug('lastAction=1111==='+lastAction);       
                    }catch(Exception e){
                        lastAction=null;
                    }
                if(lastAction.size() != 0 && lastAction[0].feedback__c == 'Approved' && flag == false && (opp2.Total_NRC__c > lastAction[0].NRC_Non_Recurring_Charges__c || opp2.Total_MRC__c > lastAction[0].MRC_Monthly_Recurring_Charges__c)){
                    lastAction[0].status__c = 'Reversed';
                    update lastAction[0];
                    system.debug('lastAction=1111==='+lastAction[0]);  
              }

        }
    }              
 }