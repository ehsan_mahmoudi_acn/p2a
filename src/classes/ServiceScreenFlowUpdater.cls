/**
 * Updates the screen flow name on the service to match the one
 * on the corresponding Product Congiuration
 */
public class ServiceScreenFlowUpdater {
    
    /**
     * Updates the service screen flow to the corresponding Product Configuration.
     * 
     * Note: the services are not updated in DB.
     * 
     * @param services the services to update
     * @return the updated services.
     */
    public List<csord__Service__c> setScreenFlowName(List<csord__Service__c> services) {
        Map<Id, cscfga__Product_Configuration__c> pcMap = findRelatedProductConfigurations(services);
        for (csord__Service__c item : services) {
            cscfga__Product_Configuration__c configuration = pcMap.get(item.csordtelcoa__Product_Configuration__c);
            item.Screen_Flow_Name__c = getFlowName(configuration);
        }
        return services;
    }
    
    /**
     * Returns the flow name from the product definition, or null is not found.
     */
    private String getFlowName(cscfga__Product_Configuration__c config) {
        if (config == null) { return null; }
        
        cscfga__Product_Definition__c definition = config.cscfga__Product_Definition__r;
        if (definition == null) {
            return null;
        }
        return definition.Screen_Flow_Name__c;
    }
    
    /**
     * Loads all the product configurations for the given services
     */
    private Map<Id, cscfga__Product_Configuration__c> findRelatedProductConfigurations(List<csord__Service__c> services) {
        Set<Id> pcSet = SObjectsUtil.getIds(services, 'csordtelcoa__Product_Configuration__c');
        
        if (pcSet.isEmpty()) { return new Map<Id, cscfga__Product_Configuration__c>(); }
        
        return new Map<Id, cscfga__Product_Configuration__c>([
            SELECT Id, cscfga__Product_Definition__r.Screen_Flow_Name__c
            FROM cscfga__Product_Configuration__c 
            WHERE Id in :pcSet
        ]);
    }
}