/**
 * Updates the links between a replacement service and the corresponding replaced service.
 *
 */
public class ReplacementServiceUpdater {

    /**
     * Fixes/Sets the links between the replacement and the replaced services.
     *
     * This method does not update the database. Instead it returns an object with 2 lists
     * (@see ReplacementUpdateResult)
     *
     * Note: this method does not update the database.
     *
     * @param services the potential replacement services to update
     * @return the result containing only the modified services.
     */
    public LinkUpdateResult fixLinks(Set<Id> serviceIds) {
        List<csord__Service__c> services = findServicesWithMissingLinksById(serviceIds);
        return updateLinks(services);
    }

    /**
     * Fixes/Sets the links between the replacement and the replaced services.
     *
     * This method is suitable to be called in a before update trigger since it
     * does not update the database. Instead it returns an object with 2 lists
     * (@see ReplacementUpdateResult)
     *
     * Note: this method does not update the database.
     * Note: this method will modify the given services if needed
     *
     * @param services the potential replacement services to update
     * @return the result containing only the modified services.
     */
    public LinkUpdateResult fixLinks(List<csord__Service__c> services) {
        enrichWithReplacedServiceInformation(services);
        return updateLinks(services);
    }

    /**
     * Set the links on replacement and replaced services for the given services.
     *
     * Note: this method does not update the database.
     *
     * @param services a collection of potential replacement services.
     *                 Those services will be modified by this method if needed.
     * @return the result of the update containing only the modified services
     */
	//For @Testvisible ,Modified by Anuradha
	@Testvisible
    private LinkUpdateResult updateLinks(List<csord__Service__c> services) {
        Map<Id, csord__Service__c> replacedToReplacement = setLinksOnReplacementServices(services);
        List<csord__Service__c> replaced = setLinksOnReplacedServices(replacedToReplacement);
        return new LinkUpdateResult(replacedToReplacement.values(), replaced);
    }

    /**
     * Set the link on the replacement service to the replaced service if needed.
     *
     * @param services a collection of service to update. those services must have the 
     *                 necessary informations for this methid to work (@see enrichWithReplacedServiceInformation)
     * @return a map (key: Id of the replaced service, value: the matching replacement service)
     */
	//For @Testvisible ,Modified by Anuradha
	@Testvisible 
    private Map<Id, csord__Service__c> setLinksOnReplacementServices(List<csord__Service__c> services) {
        Map<Id, csord__Service__c> replacedToReplacement = new Map<Id, csord__Service__c>();
        for (csord__Service__c service: services) {
            if (isMissingLink(service)) {
                Id replacedServiceId = service.csordtelcoa__Product_Configuration__r
                    .csordtelcoa__Replaced_Service__c;
                service.csordtelcoa__Replaced_Service__c = replacedServiceId;
                replacedToReplacement.put(replacedServiceId, service);
            }
        }
        return replacedToReplacement;
    }

    /**
     * Checks if the given service is a replacement for another service.
     *
     * @param service the service to check
     * @return true id the given service replaces another one, false otherwise
     */
	//For @Testvisible ,Modified by Anuradha
	@Testvisible
    private Boolean isReplacement(csord__Service__c service) {
        cscfga__Product_Configuration__c config = service.csordtelcoa__Product_Configuration__r;
        return config != null && config.csordtelcoa__Replaced_Service__c != null;
    }

    /**
     * Checks if a given service misses a link to the replaced service.
     *
     * @param service the service to check
     * @return true if the given service is a replacement and is missing a link to
     *         the replaced service, false otherwise.
     */
	//For @Testvisible ,Modified by Anuradha
	@Testvisible 
    private Boolean isMissingLink(csord__Service__c service) {
        return isReplacement(service) && service.csordtelcoa__Replaced_Service__c == null;
    }

    /**
     * Sets the link to the replcement service on the replaced service.
     *
     * @param replacedToReplacement key: replaced service Id, value: matching replacement service
     * @return a list of modified replaced services, can be empty but not null.
     */
	//For @Testvisible ,Modified by Anuradha
	@Testvisible 
    private List<csord__Service__c> setLinksOnReplacedServices(Map<Id, csord__Service__c> replacedToReplacement) {
        if (replacedToReplacement.isEmpty()) { return new List<csord__Service__c>(); }

        List<csord__Service__c> updated = new List<csord__Service__c>();
        for (csord__Service__c replaced: findReplacedServicesById(replacedToReplacement.keySet())) {
            csord__Service__c replacement = replacedToReplacement.get(replaced.Id);
            if (replacement != null && replacement.Id != replaced.csordtelcoa__Replacement_Service__c) {
                // the replacement link needs to be updated
                replaced.csordtelcoa__Replacement_Service__c = replacement.Id;
                updated.add(replaced);
            }
        }

        return updated;
    }

    /**
     * Adds the necessary information on the given services to fix the links
     *
     * This method adds the linked Product Configuration to the given services.
     * Note: the given services are modified by this method
     *
     * @param services a collection of services to enrich.
     */
	//For @Testvisible ,Modified by Anuradha
	@Testvisible 
    private void enrichWithReplacedServiceInformation(List<csord__Service__c> services) {
        Set<Id> productConfigIds = new Set<Id>();
        for (csord__Service__c service: services) {
            productConfigIds.add(service.csordtelcoa__Product_Configuration__c);
        }

        Map<Id, cscfga__Product_Configuration__c> productConfigs = findProductConfigurationsById(productConfigIds);

        for (csord__Service__c service: services) {
            service.csordtelcoa__Product_Configuration__r = productConfigs.get(service.csordtelcoa__Product_Configuration__c);
        }
    }

    /**
     * Returns a map of product configurations matching the given set of ids
     *
     * @param productConfigIds a set of Product Ocnfiguration Id used that
     *                         is to be retrieved.
     * @return a Map of product configuration (key: Id of the product configuration,
     *         value: the matching product configuration)
     */
	//For @Testvisible ,Modified by Anuradha
	@Testvisible 
    private Map<Id, cscfga__Product_Configuration__c> findProductConfigurationsById(Set<Id> productConfigIds) {
        if (productConfigIds.isEmpty()) {
            return new Map<Id, cscfga__Product_Configuration__c>();
        }

        return new Map<Id, cscfga__Product_Configuration__c>([
                SELECT Id, csordtelcoa__Replaced_Service__c
                FROM cscfga__Product_Configuration__c
                WHERE Id IN :productConfigIds]);
    }

    /**
     * Returns a list of replaced services by id
     *
     * @param replacedIds the ids of the services to retrieve
     * @return a List of services matching the given ids. the list can be empty but cannot be null
     */
	//For @Testvisible ,Modified by Anuradha
	@Testvisible 
    private List<csord__Service__c> findReplacedServicesById(Set<Id> replacedIds) {
        return [
            SELECT
                Id,
                csordtelcoa__Replacement_Service__c
             FROM csord__Service__c
             WHERE Id IN :replacedIds
        ];
    }

    /**
     * Returns a list of replacement services which are missing a link to the replaced service
     *
     * @param serviceIds used to limit the query to the specified set of ids
     * @return a list of services that needs an update. the list can be empty, but cannot be null.
     */
	//For @Testvisible ,Modified by Anuradha
	@Testvisible 
    private List<csord__Service__c> findServicesWithMissingLinksById(Set<Id> serviceIds) {
        return [SELECT
                  Id,
                  csordtelcoa__Product_Configuration__r.csordtelcoa__Replaced_Service__c
                FROM csord__Service__c
                WHERE Id IN :serviceIds
                  AND csordtelcoa__Replaced_Service__c = NULL
                  AND csordtelcoa__Product_Configuration__r.csordtelcoa__Replaced_Service__c != NULL];
    }

    /**
     * Holds the result of link updates between replacement and replaced services.
     *
     * This result gives access to the modified services that were modified during 
     * the process.
     */
    public class LinkUpdateResult {
        private List<csord__Service__c> modifiedReplacementServices;
        private List<csord__Service__c> modifiedReplacedServices;

        public LinkUpdateResult(List<csord__Service__c> replacement, List<csord__Service__c> replaced) {
            this.modifiedReplacementServices = (replacement == null ? new List<csord__Service__c>() : replacement);
            this.modifiedReplacedServices = (replaced == null ? new List<csord__Service__c>() : replaced);
        }

        /**
         * Returns the list of modified replacement services
         */
        public List<csord__Service__c> getModifiedReplacementsServices() {
            return this.modifiedReplacementServices;
        }

        /**
         * Returns the list of modified replaced services
         */
        public List<csord__Service__c> getModifiedReplacedServices() {
            return this.modifiedReplacedServices;
        }

        /**
         * Returns a list of all modified services (replacement and replaced)
         */
        public List<csord__Service__c> getAllModifiedServices() {
            List<csord__Service__c> modified  = new List<csord__Service__c>();
            modified.addAll(this.modifiedReplacementServices);
            modified.addAll(this.modifiedReplacedServices);
            return modified;
        }
    }
}