global with sharing class CS_StandardAEndCityLookup extends cscfga.ALookupSearch {
    
    public override String getRequiredAttributes(){ 
        return '[ "A-End Country","IPL","EPL","EPLX","ICBS","EVPL" ]';
    }
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){

        String aEndCountry = searchFields.get('A-End Country');
        Set <Id> popIds = new Set<Id>();
       
        List <String> ProductTypeNames = new List<String>();
        
        for (String key : searchFields.keySet()){
            String attValue = searchFields.get(key);
            if(attValue == 'Yes' || attValue == 'true')
                ProductTypeNames.add(key);
        }

        /*List<CS_Route_Segment__c> routeSegList = [SELECT Id, POP_A__c, POP_A__r.CS_City__c, POP_A__r.CS_Country__c, A_End_City__c
                                                     FROM CS_Route_Segment__c
                                                  WHERE Product_Type__c in :ProductTypeNames 
                                                        AND A_End_City__r.CS_Country__c = :aEndCountry ];*/

        for(CS_Route_Segment__c item : [SELECT Id, POP_A__c, POP_A__r.CS_City__c, POP_A__r.CS_Country__c, A_End_City__c
                                                     FROM CS_Route_Segment__c
                                                  WHERE Product_Type__c in :ProductTypeNames 
                                                        AND A_End_City__r.CS_Country__c = :aEndCountry ]){
            popIds.add(item.A_End_City__c);
        }
        System.Debug('doLookupSearch');
        System.Debug(searchFields);
        String searchValue = searchFields.get('searchValue') +'%';
        List <CS_City__c> data = [SELECT Id, Name FROM CS_City__c WHERE Id IN :popIds AND Name LIKE :searchValue ORDER BY Name];
        System.Debug(data);
       return data;

   }

}