public class FileUploaderPricingApprovalData 
{

    public FileUploaderPricingApprovalData(ApexPages.StandardController controller) {

    }

    public string nameFile{get;set;}
    public string message{get;set;}
    public Blob contentFile{get;set;}
    String[] filelines = new String[]{};
    List<Pricing_Approval_Request_Data__c> pricingDataUpload;
    
    /***This function reads the CSV file and inserts records into the Pricing Approval Data object. ***/
    public Pagereference readFile()
    {
        try{
                //Convert the uploaded file which is in BLOB format into a string
                nameFile =blobToString( contentFile,'ISO-8859-1');
                
                //Now sepatate every row of the excel file
                filelines = nameFile.split('\n');
                
                //Iterate through every line and create a Account record for each row
                pricingDataUpload= new List<Pricing_Approval_Request_Data__c>();
                for (Integer i=1;i<filelines.size();i++)
                {
                    String[] inputvalues = new String[]{};
                    inputvalues = filelines[i].split(',');
                    
                    Pricing_Approval_Request_Data__c a = new Pricing_Approval_Request_Data__c();
                    a.id = inputvalues[0];
                                        
                   try{
                        a.Approved_NRC__c = Decimal.valueof(inputvalues[7]);
                    }catch(exception e){
                        a.Approved_NRC__C = 0;
                    }
                   
                   try{
                        a.Approved_RC__c = Decimal.valueOf(inputvalues[8]);
                    }catch (exception e){
                        a.Approved_RC__C = 0;
                    }
                    
                    try{
                        a.Cost_NRC__c = Decimal.valueof(inputvalues[9]);
                    }catch(exception e){
                        a.Cost_NRC__c = 0;
                    }
        
                    try{
                        a.Cost_RC__c = Decimal.valueOf(inputvalues[10]);
                    }catch (exception e){
                        a.Cost_RC__c = 0;
                    }
                    
                    pricingDataUpload.add(a);
                }
         }
         catch(Exception e){
                 ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured reading the CSV file'+e.getMessage());
                ApexPages.addMessage(errormsg);
         }       
        //Finally, insert the collected records
        try{
            update pricingDataUpload;
        }
        catch (Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured inserting the records'+e.getMessage());
            ApexPages.addMessage(errormsg);
        }
        ApexPages.Message successmsg = new ApexPages.Message(ApexPages.severity.INFO,'File was uploaded successfully');
        ApexPages.addMessage(successmsg);    
        return null;
    }
   
     /**
         This function convers the input CSV file in BLOB format into a string
        @param input    Blob data representing correct string in @inCharset encoding
        @param inCharset    encoding of the Blob data (for example 'ISO 8859-1')
     */
    public static String blobToString(Blob input, String inCharset){
        String hex = EncodingUtil.convertToHex(input);
        System.assertEquals(0, hex.length() & 1);
        final Integer bytesCount = hex.length() >> 1;
        String[] bytes = new String[bytesCount];
        for(Integer i = 0; i < bytesCount; ++i)
            bytes[i] =  hex.mid(i << 1, 2);
        return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
    }         
}