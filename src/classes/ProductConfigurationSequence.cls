/**
* @author Telstra Accenture (Ritesh)
* @description This class has created as part of CR19 to create the hierarchy of product configuration which will appear in Pricing case and Solution summary page
*/ 

public class ProductConfigurationSequence{
	
/**
* @author Telstra Accenture (Ritesh)
* @description It accepts the list of product configuration and populate the "Parent Config for sequence" at product config 
* @param newProductConfigs Product Configuration's List 
*/ 	
 public static void updateParentConfigForSequence(List<cscfga__Product_Configuration__c> newProductConfigs){
        try{

         List<cscfga__Product_Configuration__c>updatePcList=new List<cscfga__Product_Configuration__c>();
         Set<String>AvoidProductsByDefinition=new Set<String>{'IPT Singlehome','TWI Singlehome'};
         Set<String> Productids = new Set<String>{'OFFPOPA','OFFPOPB','VLL-A'};
        
        for(cscfga__Product_Configuration__c pc:newProductConfigs){
        if(pc.cscfga__Parent_Configuration__c!=null && !AvoidProductsByDefinition.contains(pc.Product_Definition_Name__c))
         {pc.Parent_Configuration_For_Sequencing__c=pc.cscfga__Parent_Configuration__c;}
        else if(pc.Master_IPVPN_Configuration__c!=null)
         {
             if(Productids.contains(pc.Product_Id__c) && pc.Added_Ports__c!=null){
                 //do nothing
             }
             else if(!pc.name.contains('ASBR')){
                pc.Parent_Configuration_For_Sequencing__c=pc.Master_IPVPN_Configuration__c; 
             }
             
         }
         //for EVPL products
         else if(pc.EvplId__c!=null && pc.EvplId__c!='')
         {pc.Parent_Configuration_For_Sequencing__c=pc.EvplId__c;}        
                  
        }
        updateASBR(newProductConfigs);  
        }       
        
       catch(Exception ex){
       ErrorHandlerException.ExecutingClassName='ProductConfigurationSequence:updateParentConfigForSequence';
       ErrorHandlerException.objectList = newProductConfigs;
       ErrorHandlerException.sendException(ex);
       }   
    } 

/**
* @author Telstra Accenture (Ritesh)
* @date 18/5/2017
* @description product changes for SingleHomeProducts
It accepts the list of product configuration and populate the "Parent Config for sequence" at product config for Both IPT and TWI singleHome Products
* @param newProductConfigs Product Configuration's List 
*/
  
  public static void updateIPTSinglehome(List<cscfga__Product_Configuration__c> productConfigs){
    
    try{
    
    Boolean runMethod=true;    
    /*for(cscfga__Product_Configuration__c p : productConfigs){
        if(p.Product_Definition_Name__c.contains('Singlehome') && p.Parent_Configuration_For_Sequencing__c==null && p.cscfga__Parent_Configuration__c!=null){
            runMethod=true;
        }
    }
    */
    if(runMethod){    
    List<cscfga__Product_Configuration__c> IPTSinglehome = [Select id,name,Parent_Configuration_For_Sequencing__c ,
    cscfga__Parent_Configuration__c from cscfga__Product_Configuration__c where id in: productConfigs and Product_Definition_Name__c like '%Singlehome%' ];    
    Map<Id,Id>SingleHomeToIpt=new Map<Id,Id>();
    
    //get parent of IPTSingleHome
    for(cscfga__Product_Configuration__c p : IPTSinglehome){
            if(p.cscfga__Parent_Configuration__c != null && !SingleHomeToIpt.containsKey(p.cscfga__Parent_Configuration__c)){                    
                    SingleHomeToIpt.put(p.cscfga__Parent_Configuration__c,p.id);                    
            }           
    }
    
    if(SingleHomeToIpt.size()>0){
    //update IPT with IPTSingleHome as Parent       
    List<cscfga__Product_Configuration__c> IPTList = [Select id,name,Parent_Configuration_For_Sequencing__c,
    cscfga__Parent_Configuration__c from cscfga__Product_Configuration__c where id in:SingleHomeToIpt.keyset()];
        for(cscfga__Product_Configuration__c p : IPTList){
            if(SingleHomeToIpt.containsKey(p.id)){
                p.Parent_Configuration_For_Sequencing__c=SingleHomeToIpt.get(p.id);                
            }
        }
        if(IPTList.size()>0){update IPTList;}
    }
    }
    }catch(Exception e){
        ErrorHandlerException.ExecutingClassName='ProductConfigurationSequence:updateIPTSinglehome';
        ErrorHandlerException.objectList = productConfigs;
        ErrorHandlerException.sendException(e);
    }
    
  }
  
/**
* @author Telstra Accenture (Ritesh)
* @date 18/5/2017
* @description product Specific code for ASBR
It accepts the list of product configuration and populate the "Parent Config for sequence" at product config for ASBR
* @param newProductConfigs Product Configuration's List 
*/ 
  public static void updateASBR(List<cscfga__Product_Configuration__c> productConfigs){
           try{ 
            //List<cscfga__Product_Configuration__c> Asbr = [Select id,name,Parent_Configuration_For_Sequencing__c,Added_Ports__c from cscfga__Product_Configuration__c where id in: productConfigs and name like '%ASBR%' ];
            Boolean runMethod=false; 
                
            for(cscfga__Product_Configuration__c p : productConfigs){
                if(p.Name.contains('ASBR') && p.Added_Ports__c!=null){
                    runMethod=true;
                }
                else if(p.Name.contains('Standalone ASBR') && p.Parent_Configuration_For_Sequencing__c==null && p.Master_IPVPN_Configuration__c!=null){
                    runMethod=true;
                }

            }
            if(runMethod){
            for(cscfga__Product_Configuration__c p : productConfigs){
                            if(p.Added_Ports__c != null){
                                            List<String> portid = p.Added_Ports__c.split(',');
                                            p.Parent_Configuration_For_Sequencing__c = portid[0];
                            }
                            else if(p.Master_IPVPN_Configuration__c != null){
                                            p.Parent_Configuration_For_Sequencing__c = p.Master_IPVPN_Configuration__c;
                            }

            }
            
            
            }
            }catch(Exception ex){
            ErrorHandlerException.ExecutingClassName='ProductConfigurationSequence:updateASBR';
            ErrorHandlerException.objectList = productConfigs;
            ErrorHandlerException.sendException(ex);
            }
        }

/**
* @author Telstra Accenture (Ritesh)
* @date 18/5/2017
* @description product Specific code to relate singleHome and IPC
It accept the Map of new and old Product config and craete the linkage between IPC and SingleHome
* @param newProductConfigs Map of New Product Configuration
* @param newProductConfigs Map of Old Product Configuration
*/ 		
   public static void updateIPCtoSingleHome(Map<Id,String> newProductConfigsMap,Map<Id,String>oldProductConfigsMap){
           try{

            Boolean runMethod=true;    
            
            if(runMethod){          
            System.debug('in updateipctosinglehome');
            List<cscfga__Product_Configuration__c> PCList = [Select id,name,Parent_Configuration_For_Sequencing__c,Added_Ports__c 
            from cscfga__Product_Configuration__c where id in: newProductConfigsMap.keyset() and Product_Id__c IN('IPTS-C')];//,'IPTM-STD','TWIM-STR')];
            Map<Id,Id>IPCtoIPTSingleHomeMap=new Map<Id,Id>();
            for(cscfga__Product_Configuration__c pc : PCList){
                            if(newProductConfigsMap.containsKey(pc.id) && newProductConfigsMap.get(pc.id)!= null){
                                            List<String> portid = newProductConfigsMap.get(pc.id).split(',');
                                            //pc.Parent_Configuration_For_Sequencing__c = portid[0];
                                            for(String tempStr:portid){
                                              if(tempStr!=null)IPCtoIPTSingleHomeMap.put(tempStr,pc.id);
                                            }
                            }
                            //when Any of the IPT singleHome is no more linked to IPC
                            if(oldProductConfigsMap.get(pc.id)!=null && newProductConfigsMap.get(pc.id)!=oldProductConfigsMap.get(pc.id)){
                                   
                                   String newPortid =newProductConfigsMap.get(pc.id);
                                   List<String> oldPortid = oldProductConfigsMap.get(pc.id).split(',');                                   
                                   for(String tempStr:oldPortid ){
                                       if(tempStr!=null && (newPortid==null||(newPortid!=null  && !newPortid.contains(tempStr))))IPCtoIPTSingleHomeMap.put(tempStr,null);
                                   }
                            }
            }
            if(IPCtoIPTSingleHomeMap.size()>0){
            system.debug('Map IPCtoIPTSingleHomeMap---- > ' + IPCtoIPTSingleHomeMap);
            List<cscfga__Product_Configuration__c>IPTSingleHomeList=[Select id,name,Parent_Configuration_For_Sequencing__c,Added_Ports__c 
            from cscfga__Product_Configuration__c where ID IN:IPCtoIPTSingleHomeMap.keyset()];
            for(cscfga__Product_Configuration__c pc:IPTSingleHomeList){
              if(IPCtoIPTSingleHomeMap.containsKey(pc.id)){
                pc.Parent_Configuration_For_Sequencing__c =IPCtoIPTSingleHomeMap.get(pc.id);
              }
            }
            
            if(IPTSingleHomeList.size()>0){
                            update(IPTSingleHomeList);
            }
            }
            
            }
            }catch(Exception ex){
            ErrorHandlerException.ExecutingClassName='ProductConfigurationSequence:updateIPCtoSingleHome';
            ErrorHandlerException.objectIdList= new list<Id>(newProductConfigsMap.keyset());
            ErrorHandlerException.sendException(ex);
            }
        }

    
/**
* @author Telstra Accenture (Ritesh)
* @date 07/06/2017
* @description The below code will update the Hierarchy attributes in PC
That will later be used for populating line item number on servie page
* @param newProductConfigs List of Product Configuration
*/

public static void setPcHierarchy(List<cscfga__Product_configuration__c> NewPCList) {

    
try{

Set<Id> BasketId = new Set<Id>();
Map<Id,cscfga__Product_configuration__c>currentPcMap=new Map<Id,cscfga__Product_configuration__c>();

//getting Basket ID
for(cscfga__Product_configuration__c pc : NewPCList){
    if(pc.cscfga__Product_Basket__c != null ){
        BasketId.add(pc.cscfga__Product_Basket__c);
    }
    currentPcMap.put(pc.id,pc);
}
if(!BasketId.isEmpty()){
List<cscfga__Product_Configuration__c>PCList=[select Id,Name,Product_Definition_Name__c ,Product_Id__c ,Parent_Configuration_For_Sequencing__c,Hierarchy__c,Hierarchy_Product_Sequence__c,Hierarchy_Row_Sequence__c,Product_Counter__c,Row_Sequence__c,Sequenc_Key__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c IN:BasketId order by Sequenc_Key__c];

//try{
//ProductConfigurationSequence.updateIPCtoSingleHome(PCList);//CR019
//ProductConfigurationSequence.updateIPTSinglehome(PCList);
//ProductConfigurationSequence.updateASBR(PCList);//CR019
//}catch(Exception e){System.debug(e);}


Map<Id,Decimal>ProductSequenceMap=new Map<Id,Decimal>();
Map<id,Decimal>RowSequenceMap=new Map<id,Decimal>();
Map<id,Decimal>ChildProductSequenceMap=new Map<id,Decimal>();

Map<Id,String>ProductSequenceTempMap=new Map<Id,String>();
for(cscfga__Product_Configuration__c pc:PCList){
    if(pc.Parent_Configuration_For_Sequencing__c==null && pc.Product_Counter__c!=null){
        ProductSequenceTempMap.put(pc.id,pc.Product_Counter__c);
    }   
}

//for loop ends
List<String>ProductSequenceList=ProductSequenceTempMap.values();
System.debug('ProductSequenceList'+ProductSequenceList);
ProductSequenceList.sort();

//case:1
//updating productSequenceMap
//the below code in for loop will assign sequencing for parent PC
for(id key:ProductSequenceTempMap.keyset()){
    for(integer i=0;i<ProductSequenceList.size();i++){
        if(ProductSequenceTempMap.get(key)==ProductSequenceList[i]){
            ProductSequenceMap.put(key,i+1);
            ProductSequenceList[i]=String.valueOf(1000000);//avoid duplicate values
        }
    }
}

//case:2
//updating rowsequenceMap
//the below code in for loop will assign sequencing for child PC with row sequence value
Map<Id,Decimal>ParentPcTochildWithRowCount=new Map<Id,Decimal>();
for(cscfga__Product_Configuration__c pc:PCList){    
    //if(pc.Parent_Configuration_For_Sequencing__c==null){
        Map<id,Decimal>RowSequenceTempMap=new Map<id,Decimal>();
        for(cscfga__Product_Configuration__c childpc:PCList){
            if(childpc.Parent_Configuration_For_Sequencing__c!=null 
            && childpc.Parent_Configuration_For_Sequencing__c==pc.id 
            && childpc.Row_Sequence__c>0){
                RowSequenceTempMap.put(childpc.id,childpc.Row_Sequence__c);
            }
        }
        if(!RowSequenceTempMap.isempty()){
        List<Decimal>RowSequenceList=RowSequenceTempMap.values();       
        RowSequenceList.sort();     
        for(id key:RowSequenceTempMap.keyset()){
            for(integer j=0;j<RowSequenceList.size();j++){
                if(RowSequenceTempMap.get(key)==RowSequenceList[j]){
                    RowSequenceMap.put(key,j+1);                    
                    break;
                }
            }                       
        }
        
        }
    // }
    if(!RowSequenceTempMap.isempty()){
      ParentPcTochildWithRowCount.put(pc.id,RowSequenceTempMap.size()); 
    }
    else{
        ParentPcTochildWithRowCount.put(pc.id,0);
    }
    
    
}

//case :3
//updating child product sequence(for example IPVPN port inside Master IPVPN)
//the below code in for loop will assign sequencing for child PC with row sequence value
for(cscfga__Product_Configuration__c pc:PCList){    
   // if(pc.Parent_Configuration_For_Sequencing__c==null){
        Map<id,String>ChildProductSequenceTempMap=new Map<id,String>();
        for(cscfga__Product_Configuration__c childpc:PCList){
            if(childpc.Parent_Configuration_For_Sequencing__c!=null
            && childpc.Parent_Configuration_For_Sequencing__c==pc.id 
            && childpc.Row_Sequence__c==0){
                ChildProductSequenceTempMap.put(childpc.id,childpc.Product_Counter__c);
            }
        }
        if(!ChildProductSequenceTempMap.isempty()){
        List<String>ChildProductSequenceList=ChildProductSequenceTempMap.values();
        ChildProductSequenceList.sort();     
        for(id key:ChildProductSequenceTempMap.keyset()){
            for(integer j=0;j<ChildProductSequenceList.size();j++){
                if(ChildProductSequenceTempMap.get(key)==ChildProductSequenceList[j]){
                    ChildProductSequenceMap.put(key,ParentPcTochildWithRowCount.get(pc.id)+j+1);
                }
            }                       
        }
        
        }
    // }
    
}


//updating hierarchy attributes in PC
Map<Id,cscfga__Product_Configuration__c>UpdatePCMap=new Map<Id,cscfga__Product_Configuration__c>();
for(cscfga__Product_Configuration__c pc:PCList){
    if(ProductSequenceMap.containsKey(pc.id)){
        pc.Hierarchy_Product_Sequence__c=ProductSequenceMap.get(pc.id);
    }   
    else if(RowSequenceMap.containsKey(pc.id)){
        pc.Hierarchy_Row_Sequence__c=RowSequenceMap.get(pc.id);
    }
    else if(ChildProductSequenceMap.containsKey(pc.id)){
        pc.Hierarchy_Product_Sequence__c=ChildProductSequenceMap.get(pc.id);
    }
    //to avoid record is read only error
    cscfga__Product_Configuration__c p=new cscfga__Product_Configuration__c();
        p.id=pc.id;
        p.Hierarchy_Product_Sequence__c=pc.Hierarchy_Product_Sequence__c;
        p.Hierarchy_Row_Sequence__c=pc.Hierarchy_Row_Sequence__c;
    UpdatePCMap.put(p.id,p); 
       
}

//updating the PC List
system.debug('Hierarchy UpdatePCMap'+UpdatePCMap);
if(UpdatePCMap.size()>0){
system.debug('before updated successfully');
update UpdatePCMap.values();
system.debug('after updated successfully');
}

}
// if condition ends
}catch(exception e){
ErrorHandlerException.ExecutingClassName='ProductConfigurationSequence:setPcHierarchy';
ErrorHandlerException.objectList = NewPCList;
ErrorHandlerException.sendException(e);

}
}

/**
* @author Telstra Accenture (Ritesh)
* @date 07/06/2017
* @description The below code will update the Hierarchy attributes in PC before the update
That will later be used for populating line item number on servie page
* @param newProductConfigs List of Product Configuration
*/

public static void setPcHierarchyBeforeUpdate(List<cscfga__Product_configuration__c> NewPCList) {

    
try{

Set<Id> BasketId = new Set<Id>();
Map<Id,cscfga__Product_configuration__c>currentPcMap=new Map<Id,cscfga__Product_configuration__c>();

//getting Basket ID
for(cscfga__Product_configuration__c pc : NewPCList){
    if(pc.cscfga__Product_Basket__c != null ){
        BasketId.add(pc.cscfga__Product_Basket__c);
    }
    currentPcMap.put(pc.id,pc);
}
if(!BasketId.isEmpty()){
List<cscfga__Product_Configuration__c>PCList=[select Id,Name,Product_Definition_Name__c ,Product_Id__c ,Parent_Configuration_For_Sequencing__c,Hierarchy__c,Hierarchy_Product_Sequence__c,Hierarchy_Row_Sequence__c,Product_Counter__c,Row_Sequence__c,Sequenc_Key__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c IN:BasketId order by Sequenc_Key__c];

//try{
//ProductConfigurationSequence.updateIPCtoSingleHome(PCList);//CR019
//ProductConfigurationSequence.updateIPTSinglehome(PCList);
//ProductConfigurationSequence.updateASBR(PCList);//CR019
//}catch(Exception e){System.debug(e);}


Map<Id,Decimal>ProductSequenceMap=new Map<Id,Decimal>();
Map<id,Decimal>RowSequenceMap=new Map<id,Decimal>();
Map<id,Decimal>ChildProductSequenceMap=new Map<id,Decimal>();

Map<Id,String>ProductSequenceTempMap=new Map<Id,String>();
for(cscfga__Product_Configuration__c pc:PCList){
    if(pc.Parent_Configuration_For_Sequencing__c==null && pc.Product_Counter__c!=null){
        ProductSequenceTempMap.put(pc.id,pc.Product_Counter__c);
    }   
}

//for loop ends
List<String>ProductSequenceList=ProductSequenceTempMap.values();
System.debug('ProductSequenceList'+ProductSequenceList);
ProductSequenceList.sort();

//case:1
//updating productSequenceMap
//the below code in for loop will assign sequencing for parent PC
for(id key:ProductSequenceTempMap.keyset()){
    for(integer i=0;i<ProductSequenceList.size();i++){
        if(ProductSequenceTempMap.get(key)==ProductSequenceList[i]){
            ProductSequenceMap.put(key,i+1);
            ProductSequenceList[i]=String.valueOf(1000000);//avoid duplicate values
        }
    }
}

//case:2
//updating rowsequenceMap
//the below code in for loop will assign sequencing for child PC with row sequence value
Map<Id,Decimal>ParentPcTochildWithRowCount=new Map<Id,Decimal>();
for(cscfga__Product_Configuration__c pc:PCList){    
    //if(pc.Parent_Configuration_For_Sequencing__c==null){
        Map<id,Decimal>RowSequenceTempMap=new Map<id,Decimal>();
        for(cscfga__Product_Configuration__c childpc:PCList){
            if(childpc.Parent_Configuration_For_Sequencing__c!=null 
            && childpc.Parent_Configuration_For_Sequencing__c==pc.id 
            && childpc.Row_Sequence__c>0){
                RowSequenceTempMap.put(childpc.id,childpc.Row_Sequence__c);
            }
        }
        if(!RowSequenceTempMap.isempty()){
        List<Decimal>RowSequenceList=RowSequenceTempMap.values();       
        RowSequenceList.sort();     
        for(id key:RowSequenceTempMap.keyset()){
            for(integer j=0;j<RowSequenceList.size();j++){
                if(RowSequenceTempMap.get(key)==RowSequenceList[j]){
                    RowSequenceMap.put(key,j+1);                    
                    break;
                }
            }                       
        }
        
        }
    // }
    if(!RowSequenceTempMap.isempty()){
      ParentPcTochildWithRowCount.put(pc.id,RowSequenceTempMap.size()); 
    }
    else{
        ParentPcTochildWithRowCount.put(pc.id,0);
    }
    
    
}

//case :3
//updating child product sequence(for example IPVPN port inside Master IPVPN)
//the below code in for loop will assign sequencing for child PC with row sequence value
for(cscfga__Product_Configuration__c pc:PCList){    
   // if(pc.Parent_Configuration_For_Sequencing__c==null){
        Map<id,String>ChildProductSequenceTempMap=new Map<id,String>();
        for(cscfga__Product_Configuration__c childpc:PCList){
            if(childpc.Parent_Configuration_For_Sequencing__c!=null
            && childpc.Parent_Configuration_For_Sequencing__c==pc.id 
            && childpc.Row_Sequence__c==0){
                ChildProductSequenceTempMap.put(childpc.id,childpc.Product_Counter__c);
            }
        }
        if(!ChildProductSequenceTempMap.isempty()){
        List<String>ChildProductSequenceList=ChildProductSequenceTempMap.values();
        ChildProductSequenceList.sort();     
        for(id key:ChildProductSequenceTempMap.keyset()){
            for(integer j=0;j<ChildProductSequenceList.size();j++){
                if(ChildProductSequenceTempMap.get(key)==ChildProductSequenceList[j]){
                    ChildProductSequenceMap.put(key,ParentPcTochildWithRowCount.get(pc.id)+j+1);
                }
            }                       
        }
        
        }
    // }
    
}


//updating hierarchy attributes in PC
Map<Id,cscfga__Product_Configuration__c>UpdatePCMap=new Map<Id,cscfga__Product_Configuration__c>();
for(cscfga__Product_Configuration__c pc:PCList){
    if(ProductSequenceMap.containsKey(pc.id)){
        pc.Hierarchy_Product_Sequence__c=ProductSequenceMap.get(pc.id);
    }   
    else if(RowSequenceMap.containsKey(pc.id)){
        pc.Hierarchy_Row_Sequence__c=RowSequenceMap.get(pc.id);
    }
    else if(ChildProductSequenceMap.containsKey(pc.id)){
        pc.Hierarchy_Product_Sequence__c=ChildProductSequenceMap.get(pc.id);
    }
    else{}
    
    if(currentPcMap.containsKey(pc.id)){
        if(ProductSequenceMap.containsKey(pc.id)){
        currentPcMap.get(pc.id).Hierarchy_Product_Sequence__c=ProductSequenceMap.get(pc.id);
        }
        else{
        currentPcMap.get(pc.id).Hierarchy_Product_Sequence__c=ChildProductSequenceMap.get(pc.id);
        }
        currentPcMap.get(pc.id).Hierarchy_Row_Sequence__c=pc.Hierarchy_Row_Sequence__c;
    }
    //to avoid record is read only error
    else if(!currentPcMap.containsKey(pc.id)){
      cscfga__Product_Configuration__c p=new cscfga__Product_Configuration__c();
        p.id=pc.id;
        p.Hierarchy_Product_Sequence__c=pc.Hierarchy_Product_Sequence__c;
        p.Hierarchy_Row_Sequence__c=pc.Hierarchy_Row_Sequence__c;
       UpdatePCMap.put(p.id,p); 
    }
       
}

//updating the PC List
system.debug('Hierarchy ChildProductSequenceMap'+ChildProductSequenceMap);
system.debug('Hierarchy ProductSequenceMap'+ProductSequenceMap);
system.debug('Hierarchy currentPcMap'+currentPcMap);
system.debug('NewPCList'+NewPCList);

if(UpdatePCMap.size()>0){
system.debug('before updated successfully');
update UpdatePCMap.values();
system.debug('after updated successfully');
}

}
// if condition ends
}catch(exception e){
ErrorHandlerException.ExecutingClassName='ProductConfigurationSequence:setPcHierarchyBeforeUpdate';
ErrorHandlerException.objectList = NewPCList;
ErrorHandlerException.sendException(e);
}
}  
  }