@isTest
public class TestAllStepTriggerHandler {

   private static cscfga__Product_Basket__c basket = null;
    
    private static cscfga__Product_Configuration__c config = null;
    private static cscfga__Product_Configuration__c masterConfig = null;
    
    private static csord__Service__c service = null;
    private static csord__Service__c masterService = null;
    
    private static List<Offer_Id__c> offIdlist;
    private static List<csordtelcoa__Change_Types__c> changeTypeCSList;
    private static List<Product_Definition_Id__c> pdCSList;
    
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        } else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }

    private static void disableTelcoAll(Id userId) {
        csordtelcoa__Orders_Subscriptions_Options__c telcoOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance(userId);

       if(telcoOptions == null) {
            telcoOptions = new csordtelcoa__Orders_Subscriptions_Options__c ();
            telcoOptions.csordtelcoa__Disable_Triggers__c = true;
            telcoOptions.SetupOwnerId = userId;
        } else {
            telcoOptions.csordtelcoa__Disable_Triggers__c = true;
        }

        upsert telcoOptions;
   
    }
    
    @TestSetup
    static void setupTestData(){
    
        P2A_TestFactoryCls.SetupTestData();
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
                List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
         List<CSPOFA__Orchestration_Process_Template__c> processtemplate = P2A_TestFactoryCls.getOrchestrationProcess(1);
         List<CSPOFA__Orchestration_Process__c>  orch =  P2A_TestFactoryCls.getOrchestrationProcesss(1,processtemplate);
     List<CSPOFA__Orchestration_Step_Template__c> steptemplate = P2A_TestFactoryCls.getOOrchestration_Step_Template(1,processtemplate);
      List<CSPOFA__Orchestration_Step__c> orchstep = P2A_TestFactoryCls.getOrchestrationStep(1,orch);
        //TestDataFactory.createProductDefinitionIdCustomSettings(TestDataFactory.createProductDefinition('Test Prod', 'Test Desc', true));
        /*csord__Order_Request__c ordReq = new csord__Order_Request__c(name='Test Ord Req', csord__Module_Version__c='1.0', 
                                            csord__Module_Name__c = 'Test Module');
        insert ordReq;
        Id ordReqId = [select Id from csord__Order_Request__c ORDER BY CreatedDate DESC LIMIT 1].get(0).Id;
        

        CSPOFA__Orchestration_Process_Template__c thisProcTempl = new CSPOFA__Orchestration_Process_Template__c(name='Test Proc Templ');
        insert thisProcTempl;
        CSPOFA__Orchestration_Step_Template__c thisStepTempl = new CSPOFA__Orchestration_Step_Template__c(name='Test Step Template',
                                                              CSPOFA__Orchestration_Process_Template__c = thisProcTempl.Id, 
                                                              Estimated_Time_To_Complete__c = 2.0, 
                                                            CSPOFA__OLA_Unit__c = 'day', 
                                                            CSPOFA__Milestone_Label__c = '1');
        insert thisStepTempl;
        CSPOFA__Orchestration_Step_Template__c thisStepTempl2 = new CSPOFA__Orchestration_Step_Template__c(name='Test Step Template2',
                                                              CSPOFA__Orchestration_Process_Template__c = thisProcTempl.Id, 
                                                              Estimated_Time_To_Complete__c = 4.0, 
                                                            CSPOFA__OLA_Unit__c = 'day', 
                                                            CSPOFA__Milestone_Label__c = '2');
        insert thisStepTempl2;
        csord__Subscription__c sub = new csord__Subscription__c(/*csord__order__c = ordLst.get(0).Id,
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReq.Id,
                                                    name = 'Some Sub');
        insert sub;

        csord__Order__c order = new csord__Order__c(name='Test Order2', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReq.Id,
                                                    Customer_Required_Date__c = Date.newInstance(2017,01,01));
        insert order;

        sub.csord__Order__c = order.Id;
        update sub;
       
        csord__Service__c someServ = new csord__Service__c(csordtelcoa__Service_Number__c = '21234-', 
                                        Estimated_Start_Date__c = system.now().addDays(10),
                                        name = 'Test Serv', csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                        csord__Order_Request__c = ordReqId, csord__Subscription__c = sub.Id);
        
        
        insert someServ;
         
        CSPOFA__Orchestration_Process__c thisProc = new CSPOFA__Orchestration_Process__c(name='Test Process', 
                                                      CSPOFA__Orchestration_Process_Template__c = thisProcTempl.Id,
                                                      Estimated_Time_To_Complete__c = 30,
                                                      CSPOFA__Status__c = 'Initialising',
                                                      order__c = order.Id, csordtelcoa__Service__c = someServ.Id);
        insert thisProc;

        CSPOFA__Orchestration_Process__c thisNewProc = new CSPOFA__Orchestration_Process__c(name='Test Process2', 
                                                      CSPOFA__Orchestration_Process_Template__c = thisProcTempl.Id,
                                                      Estimated_Time_To_Complete__c = 30, 
                                                      CSPOFA__Status__c = 'In Progress',
                                                      csordtelcoa__Service__c = someServ.Id);
        insert thisNewProc;
        
        
        CSPOFA__Orchestration_Process__c thisNewPr = new CSPOFA__Orchestration_Process__c(name='Test Process2', 
                                                      CSPOFA__Orchestration_Process_Template__c = thisProcTempl.Id,
                                                      Estimated_Time_To_Complete__c = 30, 
                                                      CSPOFA__Status__c = 'Complete',
                                                      csordtelcoa__Service__c = someServ.Id);
        insert thisNewPr;

        cspofa__Orchestration_Step__c thisStep = new cspofa__Orchestration_Step__c(name='Test Step', 
                                                    CSPOFA__Orchestration_Process__c = thisProc.Id,
                                                    CSPOFA__Status__c = 'Initializing',
                                                    CSPOFA__Orchestration_Step_Template__c = thisStepTempl.Id,
                                                    Estimated_Time_To_Complete__c = 2.0);
        insert thisStep;
        
        cspofa__Orchestration_Step__c thisNewStep = new cspofa__Orchestration_Step__c(name='Test Step2', 
                                                    CSPOFA__Orchestration_Process__c = thisProc.Id, 
                                                    CSPOFA__Status__c = 'Initializing',
                                                    CSPOFA__Orchestration_Step_Template__c = thisStepTempl2.Id,
                                                    Estimated_Time_To_Complete__c = 4.0);
        insert thisNewStep;
        */
               
        
    }
    
     
  
    static testmethod void testBeforeUpdate(){
      //  disableAll(UserInfo.getUserId());
     //   disableTelcoAll(UserInfo.getUserId());
        AllStepTriggerHandler ASTH = new AllStepTriggerHandler();
        map<Id, cspofa__Orchestration_Step__c> oldStepsMap = 
                              new map<Id, cspofa__Orchestration_Step__c>([select Id, Name, Start_Date_Time__c,End_Date_Time__c,
                                                              Estimated_Time_To_Complete__c, CSPOFA__Orchestration_Process__c,
                                                              CSPOFA__Status__c, RAG_Status__c, 
                                                              CSPOFA__Orchestration_Process__r.Order__r.RAG_Status__c,
                                                              CSPOFA__Orchestration_Process__r.csordtelcoa__Service__c,
                                                              CSPOFA__Orchestration_Process__r.order__c,
                                                              CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c,
                                                              CSPOFA__Orchestration_Step_Template__r.Estimated_Time_to_Complete__c
                                                              from cspofa__Orchestration_Step__c
                                                              where CSPOFA__Orchestration_Process__c != null]);

        map<Id, cspofa__Orchestration_Step__c> newStepsMap = oldStepsMap.deepClone();//new map<Id, cspofa__Orchestration_Step__c>();
        
        newStepsMap.values().get(0).Start_Date_Time__c = System.now().addDays(10);
        newStepsMap.values().get(0).CSPOFA__Status__c = 'In Progress';
        newStepsMap.values().get(0).End_Date_Time__c = System.now().addDays(100);
        newStepsMap.values().get(1).Start_Date_Time__c = System.now().addDays(10);
        newStepsMap.values().get(1).End_Date_Time__c = System.now().addDays(100);
        newStepsMap.values().get(1).CSPOFA__Status__c = 'Complete';
        Set<Id> tempIds = newstepsMap.keySet(); 
        System.assert(newStepsMap.values().get(0).CSPOFA__Status__c != oldStepsMap.values().get(0).CSPOFA__Status__c);
        System.assert(newStepsMap.values().get(1).CSPOFA__Status__c != oldStepsMap.values().get(1).CSPOFA__Status__c);
        for(Id I : tempIds)
            System.debug('<><> ' + newstepsMap.get(I).CSPOFA__Status__c);

        Test.startTest();
        try{
        ASTH.beforeUpdate(newStepsMap.values(), oldStepsMap, newStepsMap);
        ASTH.afterUpdate(newStepsMap.values(), oldStepsMap, newStepsMap);   
        Test.stopTest();
        }catch (DMLException e) {
        }

    }
}