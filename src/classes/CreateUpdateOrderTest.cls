/**
    @author - Accenture
    @date - 18- Jun-2012
    @version - 1.0
    @description - This is the test class for CreateUpdateOrderController class
*/
@isTest(seeAllData = false)

private class CreateUpdateOrderTest {



   /* static testMethod void generateOrderTest1() {
     // test.StartTest();
        List<String> fmStrLst = new String [] {'Accountid;Account__c;Order', 'CPQItem__c;CPQItem__c;Order Line Item', 'OrderType__c;OrderType__c;Order Line Item',
        'product_id__c;Product__c;Order Line Item', 'id;Opportunity_Line_Item_ID__c;Order Line Item','AccessOption__c;AccessOption__c;Product Configuration',
        'BearerType__c;BearerType__c;Product Configuration', 
        'BearerTypeB__c;BearerTypeB__c;Product Configuration',
        'ApplicationManagement__c;ApplicationManagement__c;Product Configuration', 'CPQItem__c;CPQItem__c;Product Configuration', 'OrderType__c;OrderType__c;Product Configuration',';Product Configuration','Is_GCPE_shared_with_multiple_services__c;Is_GCPE_shared_with_multiple_services__c;Order Line Item'};
        
        /*List<FieldMapper__c> fmLst = new  List<FieldMapper__c>();
        for(String str : fmStrLst ){
            String [] spltStr = str.split(';',0);
            FieldMapper__c fm = new FieldMapper__c(Source_Field__c = spltStr[0], Destination_Field__c = spltStr[1] , Mapping_Type__c= spltStr[2]);
            // FieldMapper__c fm = new FieldMapper__c(Source_Field__c = spltStr[0], Destination_Field__c = spltStr[1] , Mapping_Type__c= 'Product Configuration');
            fmLst.add(fm);
                        
        }
        insert fmLst;*/
        //Test.StartTest();    
        /*FieldMapper__c fieldMapObj = new FieldMapper__c(Source_Field__c = 'Test', Mapping_Type__c= 'Order',
                                                        Destination_Field__c = 'Test');
        insert fieldMapObj;*/
     /*   Product2 product2obj = new Product2(Name = 'GFTS - enepath',ProductCode = 'GFTS-e',Product_ID__c ='IPT',
                                           Installment_NRC_Bill_Text__c='test',IsActive=true,ETC_Bill_Text__c='testing',
                                           MRC_Bill_Text__c='test123',NRC_Bill_Text__c='working',
                                           Root_Product_Bill_Text__c='Dell',Service_Bill_Text__c='service',
                                           RC_Credit_Bill_Text__c='credit',NRC_Credit_Bill_Text__c='NRcredit'); 
        insert product2obj;   
  
        PricebookEntry pricebookobj = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(),Product2Id =  product2obj.Id,
                                                        IsActive = true, CurrencyIsoCode='USD',
                                                        UseStandardPrice=false, UnitPrice=0.01);
        insert pricebookobj; 
        
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
        
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
        insert accObj;
        
        Opportunity oppObj = new Opportunity(name = 'GFTS Ph 2',AccountID =accObj.ID, Opportunity_Type__c='Simple',
                                            CurrencyIsoCode ='USD', CloseDate = Date.today(),StageName ='Identify & Define',Stage__c='Identify & Define',
                                            QuoteStatus__c = 'Order', Sales_Status__c = 'Won', Win_Loss_Reasons__c='Product', 
                                            Order_Type__c ='New',Action_Item_Created_SD__c=true);
        insert oppObj;
        ApexPages.StandardController con = new ApexPages.StandardController(oppObj);
        Apexpages.currentPage().getParameters().put('Id',oppObj.id);  
       // CreateUpdateOrderController controller = new CreateUpdateOrderController(con);
        
        User userObj = [ select Id,name,Region__c from User where Id = :UserInfo.getUserId() 
                         and IsActive=true limit 1];
        OpportunityLineItem   opplineobj = new OpportunityLineItem(OpportunityId = oppobj.Id,IsMainItem__c = 'Yes',
               Quantity = 5,UnitPrice = 10,PricebookEntryId = pricebookobj.Id,
               OffnetRequired__c = 'Yes',Location__c = 'Bangalore',OrderType__c ='New Provide',
               Type_of_Professional_Service__c='other',
               Is_GCPE_shared_with_multiple_services__c = 'NA',CPQItem__c = '1', 
               Installment_NRC_Amount_per_month__c= 12.23);
        
        List<OpportunityLineItem> oppLineItmList = new List<OpportunityLineItem>();
        oppLineItmList.add(opplineobj);
        insert oppLineItmList;
                             
      
       /* Action_Item__c objActionItem = new Action_Item__c();
        objActionItem.Subject__c='Request for Bill Profile Approval';
        objActionItem.Account__c=accObj.Id;
        objActionItem.Feedback__c='Approved';
        insert objActionItem;*/
        
         
       // Opportunity opp = OpportunityHelp.getOpportunity();     
        
        
        
      //  controller.generateOrder();
        
        //Test.StopTest();
  //}
  
   /* static testMethod void generateOrderTest2() 
    {
        Test.startTest();
        Map<Id,Id> opportunityaccountmap = new Map <Id,Id>();
        Map<Id,String> errorStr = new Map<Id,String>();
                
        
        Test.stopTest();
    }
    /*static testMethod void deleteOrder(){
     try{
        Order__c ordObj = new Order__c();
        Opportunity oppObj = OpportunityHelp.getOpportunity();
        ordObj.Opportunity__c = oppObj.id; 
        insert ordObj;
        
        ApexPages.StandardController con1 = null;
        CreateUpdateOrderController controller1 = new CreateUpdateOrderController(con1);
        test.StartTest(); 
        controller1.deleteOrder(ordObj.id);
        Test.StopTest();
        } 
        Catch(Exception e)
        {
        Boolean expectedExceptionThrown =  e.getMessage().contains('My Error Message')? true : false;
        System.AssertEquals(expectedExceptionThrown, true);
        } 
    
    }
    
    static testMethod void updateOrder(){
        
       
        Opportunity oppObj = OpportunityHelp.getOpportunity();
        Order__c ordObj = new Order__c();
        ordObj.Opportunity__c = oppObj.id;
        ordObj.Is_In_Flight__c = false;
        ordObj.Is_Order_Submitted__c = false;
        ordObj.OLIUpdateRequired__c = true;
        test.StartTest(); 
        insert ordObj;
              
        
        Order_Line_Item__c oli1 = new Order_Line_Item__c();
       // oli1.Product__c = getProduct().Id;
        oli1.ParentOrder__c = ordObj.Id;
        system.debug('order****'+ ordObj.id);
        oli1.Opportunity_Line_Item_ID__c = 'Opp Line Item 001';
        oli1.OrderType__c = 'New Provide';
        oli1.CPQItem__c = '1';
        oli1.Is_GCPE_shared_with_multiple_services__c='YES';
        oli1.OrderType__c='Completed';
       oli1.Site_A__c=getSite().id;
       oli1.Product_Code__c='Test';
       oli1.Site_Type__c='';
      //oli1.Site_A_SDPM__c=getSite().id;  
      //  oli1.Site_B__c=getSite1().id;
        insert oli1;
        try{
              oli1.line_item_status__c = 'Complete';
             // oli.Billing_Commencement_Date__c=System.today();
              update oli1;
          }
        catch(DMLException e){
            system.debug('Error' +e.getMessage());
         }   
        
       List<String> fmStrLst = new String [] {'Accountid;Account__c;Order', 'CPQItem__c;CPQItem__c;Order Line Item', 'OrderType__c;OrderType__c;Order Line Item',
        'product_id__c;Product__c;Order Line Item', 'id;Opportunity_Line_Item_ID__c;Order Line Item','AccessOption__c;AccessOption__c;Product Configuration',
        'BearerType__c;BearerType__c;Product Configuration', 
        'BearerTypeB__c;BearerTypeB__c;Product Configuration',
        'ApplicationManagement__c;ApplicationManagement__c;Product Configuration', 'CPQItem__c;CPQItem__c;Product Configuration', 'OrderType__c;OrderType__c;Product Configuration','Product Configuration','Is_GCPE_shared_with_multiple_services__c;Is_GCPE_shared_with_multiple_services__c;Order Line Item'};
        
        List<FieldMapper__c> fmLst = new  List<FieldMapper__c>();
        for(String str : fmStrLst){
            String [] spltStr = str.split(';',0);
            FieldMapper__c fm = new FieldMapper__c(Source_Field__c = spltStr[0], Destination_Field__c = spltStr[1] , Mapping_Type__c= spltStr[2]);
            fmLst.add(fm);
        }
        
        insert fmLst;
        
        ApexPages.StandardController con1 = null;
        CreateUpdateOrderController controller1 = new CreateUpdateOrderController(con1);
        controller1.oppID = oppObj.id;  
           
        controller1.generateOrder();
        
      Test.StopTest();  
            
    }
    // New method...
    static testMethod void SubmitOrderGenericNP(){
    
    Opportunity oppObj = OpportunityHelp.getOpportunity();
        Order__c ordObj = new Order__c();
        ordObj.Opportunity__c = oppObj.id;
        ordObj.Is_In_Flight__c = false;
        ordObj.Is_Order_Submitted__c = false;
        ordObj.OLIUpdateRequired__c = true;
        test.StartTest(); 
        insert ordObj;
        ordobj.clean_order_date__c=system.today();
        update ordobj;
              
        
        Order_Line_Item__c oli1 = new Order_Line_Item__c();
       // oli1.Product__c = getProduct().Id;
        oli1.ParentOrder__c = ordObj.Id;
        system.debug('order****'+ ordObj.id);
        oli1.Opportunity_Line_Item_ID__c = 'Opp Line Item 001';
        oli1.OrderType__c = 'New Provide';
        oli1.CPQItem__c = '1';
        oli1.Is_GCPE_shared_with_multiple_services__c='YES';
        oli1.OrderType__c='Completed';
        
       oli1.Site_A__c=getSite().id;
       oli1.Product_Code__c='Test';
       oli1.Site_Type__c='';
      //oli1.Site_A_SDPM__c=getSite().id;  
      //  oli1.Site_B__c=getSite1().id;
        insert oli1;
        ApexPages.StandardController con1 = null;
        CreateUpdateOrderController controller11 = new CreateUpdateOrderController(con1);
        controller11.oppID = oppObj.id; 
        controller11.SubmitOrderGenericNP(ordObj.ID);
    
    
    
    }
    
     private static Order__c getOrder1(){
        Order__c ord = new Order__c();
        Opportunity o = getOpportunity();
        ord.Opportunity__c = o.Id;
        ord.Account__c=getAccount().id;
        insert ord;
        return ord;
    }
    
    private static Product2 getProduct(){
            
            Product2 prod1 = new Product2();
            prod1.name = 'Test Product';
            prod1.CurrencyIsoCode='EUR';
            prod1.Product_ID__c='IPL';        
            prod1.Create_Resource__c=TRUE;
            insert prod1;
            return prod1;
          
     }  
    private Static PricebookEntry getPriceBook(){
       PricebookEntry pbe = new PricebookEntry();
        pbe.Pricebook2Id ='01s90000000KWfFAAW';
        pbe.Product2Id = getProduct().Id;
        pbe.IsActive = true;
        pbe.UseStandardPrice = false;
        pbe.UnitPrice = 999999;
        insert pbe;
        return pbe;
    }
    
    private static Account getAccount(){
        Account acc = new Account();
      //  Country_Lookup__c cl = getCountry();
        acc.Name = 'Test Account';
        acc.Customer_Type__c = 'MNC';
      //  acc.Country__c = cl.Id;
        acc.Selling_Entity__c = 'Telstra INC';
        acc.Activated__c= true;
        acc.Account_Id__c ='12123';
        acc.Customer_Legal_Entity_Name__c = 'Telstra Incorporation';
        insert acc;
        return acc;
    }
    
    private static Opportunity getOpportunity(){
        Opportunity opp = new Opportunity();
        Account a = getAccount();
        opp.Name = 'Test Opportunity';
        opp.AccountId = a.Id;
        opp.StageName = 'Unqualified Prospect';
        opp.Action_Item_Created_SD__c=TRUE;
        opp.CloseDate = System.today();
        opp.Estimated_MRC__c=800;
        opp.Estimated_NRC__c=1000;
        opp.ContractTerm__c='10';
        insert opp;
        return opp;
        
    }
         
          private static Action_Item__c getActionItem(){
           Action_Item__c actionItem= new Action_Item__c();
           
            actionItem.Opportunity__c = getOpportunity().Id;
            //acc = getAccount();
            actionItem.Account__c = getAccount().Id;
            //actionItem.RecordTypeId = rt1.Id;
            actionItem.Bill_Profile__c=getBillProfile().id;
            actionItem.Status__c = 'Assigned';
            actionItem.Due_Date__c = system.today()+2;
            insert actionItem;
            return actionItem;
            
            }
            

     
     private static Order__c getOrder(Id orderId){
        Order__c ordr = new Order__c();
        //ordr.SD_PM_Contact__c = getUser().Id;
        Opportunity o = OpportunityHelp.getOpportunity();
        ordr.Opportunity__c = o.Id;
        insert ordr;
        return ordr;
    }
   
    // Bill Profile data
    
         private static BillProfile__c getBillProfile() {
        
            BillProfile__c bp = new BillProfile__c();
            bp.Name = 'testbill';
            bp.Account__c = getAccount().Id;
            bp.Activated__c = false;
            bp.Bill_Profile_Site__c = getSite1().Id;
            bp.Billing_Entity__c = 'Telstra Corporation';
            bp.CurrencyIsoCode = 'GBP';
            bp.Payment_Terms__c = '30 Days';
            bp.Start_Date__c = Date.today();
            bp.Invoice_Delivery_Method__c = 'Postal';
            bp.Postal_Delivery_Method__c = 'First Class';
            bp.Invoice_Breakdown__c = 'Summary Page';
            //bp.Billing_Contact__c = getContact().id;
            bp.Billing_Frequency__c = 'Monthly';
            bp.Status__c = 'Approved';
            bp.Payment_Method__c = 'Manual';
            bp.Minimum_Bill_Amount__c = 10.20;
            bp.First_Period_Date__c = date.today();
            bp.Rating_Rounding_Level__c = 2;
            bp.Bill_Decimal_Point__c = 0;
            bp.FX_Group__c = 'Corporate';
            bp.FX_Rule__c = 'FX Rate at end of period';
            insert bp;
            return bp;
    }
    
     
     private static City_Lookup__c getCity(){
        City_Lookup__c city = new City_Lookup__c();
        city.Generic_Site_Code__c = 'X123';
        city.name = 'Mumbai';
        city.City_Code__c='MUM';
        city.OwnerID = userinfo.getuserid();
        insert city;
       return city;
     
    }
    private static City_Lookup__c getCity1(){
        City_Lookup__c city = new City_Lookup__c();
        city.Generic_Site_Code__c = 'N123';
        city.name = 'Delhi';
        city.City_Code__c='DL';
        city.OwnerID = userinfo.getuserid();
        insert city;
       return city;
     
    }
     private static Site__c getSite(){
        Site__c site = new Site__c(); 
        site.Name='Test Site1';  
       site.AccountId__c =getAccount().id;
       site.City_Finder__c=getCity().id;
       site.Country_Finder__c=getCountry().id;
       site.Address1__c='Address11';
       insert site;
       return site;
     }
       private static Site__c getSite1(){
        Site__c site = new Site__c(); 
        site.Name='Test Site11';  
       site.AccountId__c =getAccount().id;
       site.City_Finder__c=getCity1().id;
       site.Country_Finder__c=getCountry1().id;
       site.Address1__c='Address11';
       insert site;
       return site;
     }
          private static Country_Lookup__c getCountry(){
        Country_Lookup__c c2 = new Country_Lookup__c();
       c2.Country_Code__c = 'ENG';
        insert c2;
        return c2;
    } 
    
     private static Country_Lookup__c getCountry1(){
        Country_Lookup__c c2 = new Country_Lookup__c();
       c2.Country_Code__c = 'NZ';
        insert c2;
        return c2;
    } 
    
     static testMethod void generateOrderTest1() {
     try{
     
     
     
    }catch(Exception e){
    }
    }*/
}