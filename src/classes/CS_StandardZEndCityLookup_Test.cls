@isTest
public class CS_StandardZEndCityLookup_Test 
{
    public static List<CS_City__c> cityList;
    public static List<CS_Route_Segment__c> bwList;
    public static List<CS_Country__c> countryList;
    public static List<cscfga__Product_Configuration__c> ProdConfList;
    private static List<cscfga__Product_Category__c> prodCategoryList;
    private static List<cscfga__Product_Definition__c> prodDefList;
   
    
    
    static testMethod void myTest1() 
    {    
        P2A_TestFactoryCls.sampleTestData();
        bwList = P2A_TestFactoryCls.getSegment(1);
        countryList = P2A_TestFactoryCls.getCSCountry(1);
        cityList = P2A_TestFactoryCls.getCSCity(1,countryList);
     //   ProdConfList = P2A_TestFactoryCls.getProductonfig(1);
     
       list<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> ProdConfList = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
        
       
        //system.debug('Country List'+countryList);      
        
        Map<String,String> fieldMap = new Map<String,String>();
        fieldMap.put('aaaaa','ddddddd');
        String str1 = 'Idhj85555';        
        Integer i1 = 1;
        Integer i2 = 2;         
        //ID[] ids = new ID[]{'01pO0000000I4g2IAL','01pO0000000I4g2IAK'};
        List<ID> ids = new List<Id>();
        ids.add(ProdConfList[0].id);    
        Test.startTest();   
        CS_StandardZEndPOPLookup stdPop = new CS_StandardZEndPOPLookup();
        stdPop.getRequiredAttributes();
        stdPop.doLookupSearch(fieldMap, str1, ids, i1, i2); 
        system.assertEquals(true,stdPop!=null);
         
        CS_StandardZEndCountryLookup stdCountry = new CS_StandardZEndCountryLookup();  
        stdCountry.getRequiredAttributes();
        stdCountry.doLookupSearch(fieldMap, str1, ids, i1, i2);
        system.assertEquals(true,stdCountry!=null); 
        
        CS_StandardZEndCityLookup std = new CS_StandardZEndCityLookup();
        std.getRequiredAttributes();
        std.doLookupSearch(fieldMap, str1, ids, i1, i2);
        system.assertEquals(true,std!=null); 
        
        CS_VLANGroupingLookup vLan = new CS_VLANGroupingLookup();
        vLan.doLookupSearch(fieldMap, str1, ids, i1, i2);
        vlan.getRequiredAttributes();
        system.assertEquals(true,vLan!=null); 
        test.stopTest();
    }
}