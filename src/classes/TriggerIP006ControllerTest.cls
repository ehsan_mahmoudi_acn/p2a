@isTest(SeeAllData=false)
private class TriggerIP006ControllerTest{    
    static testMethod void triggerIP006ControllerTestMethod1() 
    {        
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        P2A_TestFactoryCls.sampleTestData();
        
        Product_Definition_Id__c pdI = new Product_Definition_Id__c();
        pdI.Name = 'IPVPN_Product_ID';
        pdI.Product_Id__c = 'IPVPN';
        insert pdI;
        Product_Definition_Id__c pdI1 = new Product_Definition_Id__c();
        pdI1.Name = 'VPLS_Product_ID';
        pdI1.Product_Id__c = 'VLM';
        insert pdI1;
        Product_Definition_Id__c pdI2 = new Product_Definition_Id__c();
        pdI2.Name = 'IPC_Product_ID';
        pdI2.Product_Id__c = 'IPTS-C';
        insert pdI2;
        
        List<csord__Order__c> Orderlist = new list<csord__Order__c>();
        List<csord__Order__c> Orderlists = new list<csord__Order__c>();     
        Id orderid=ApexPages.currentPage().getParameters().get('Id') ;
        List<cscfga__Product_Definition__c> pdlist = new List<cscfga__Product_Definition__c>(); 
        List<csord__Order__c> ordrslists = new List<csord__Order__c>();
        
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<User> users = P2A_TestFactoryCls.get_Users(1);
        List<Country_Lookup__c> countrylist = P2A_TestFactoryCls.getcountry(1);
        List<Site__c> sitelist = P2A_TestFactoryCls.getsites(1,accList,countrylist);
        
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.name = 'TWI Singlehome';
        pd.cscfga__Description__c= 'GCPEbdkjdvbhe;ghwfguwefkjwf';
        pdlist.add(pd);
        insert pdlist;
        System.assertEquals(pdlist[0].name,'TWI Singlehome'); 
        
        Test.starttest();
        
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,Products,pdlist,pbundlelist,Offerlists);
        
        Account acc = new Account();
        acc.name                            = 'Test Accenture';
        acc.BillingCountry                  = 'GB';
        acc.Activated__c                    = True;
        acc.Account_ID__c                   = '3333';
        acc.Account_Status__c               = 'Active';
        acc.Customer_Legal_Entity_Name__c   = 'Test';
        acc.Customer_Type_New__c            = 'MNC';
        acc.Region__c                       = 'US';
        acc.Selling_Entity__c               = 'Telstra Limited';
        List<Account> accList1 = new List<Account>();
        accList1.add(acc);
        
        Account acc1 = new Account();
        acc1.name                           = 'Test Accenture';
        acc1.BillingCountry                 = 'GB';
        acc1.Activated__c                   = True;
        acc1.Account_ID__c                  = '3333';
        acc1.Account_Status__c              = 'Active';
        acc1.Customer_Legal_Entity_Name__c  = 'Test';
        acc1.Customer_Type_New__c           = 'MNC';
        acc1.Region__c                      = 'US';
        acc1.Selling_Entity__c              = 'Telstra Limited';
        accList1.add(acc1);
        insert accList1;
        
        csord__Order__c ord = new csord__Order__c();
        ord.csord__Identification__c = 'Test-JohnSnow-4238362';
        ord.RAG_Order_Status_RED__c = false;
        ord.RAG_Reason_Code__c = '';
        ord.Id = orderid;
        ord.Order_Type__c = 'New';
        ord.Is_Terminate_Order__c = false;
        ord.Is_Order_Submitted__c = true;
        ord.SD_PM_Contact__c = NULL; 
        ord.Reseller_Account_ID__c = NULL ; 
        ord.csord__Order_Request__c = OrdReqList[0].Id;
        Orderlist.add(ord);
        insert orderlist;
        System.assertEquals(orderlist[0].csord__Identification__c ,'Test-JohnSnow-4238362'); 
        
        csord__Service__c csServ = new csord__Service__c();
        csServ.Name = 'Test Service'; 
        csServ.csord__Identification__c = 'Test-Catlyne-4238362';
        csServ.csord__Order_Request__c = OrdReqList[0].Id;
        csServ.csord__Subscription__c = SUBList[0].Id;
        csServ.Billing_Commencement_Date__c = System.Today();
        csServ.Stop_Billing_Date__c = System.Today();
        csServ.RAG_Status_Red__c = false ; 
        csServ.RAG_Reason_Code__c = 'Text Reason';
        csServ.Opportunity__c = oppList[0].Id;
        csServ.csord__Order__c = Orderlist[0].Id;
        csServ.Customer_Required_Date__c = System.today();
        csServ.Estimated_Start_Date__c = System.now().addDays(10);
        csServ.Selling_Entity__c = 'Telstra Limited';
        csServ.Service_Type__c = 'Test service';
        csServ.Product_Code__c = 'VLV';
        csServ.Order_Channel_Type__c = 'Direct Sale';
        csServ.csordtelcoa__Product_Basket__c = Products[0].Id;
        csServ.csordtelcoa__Product_Configuration__c = proconfig[0].Id;
        csServ.Updated__c = true;
        csServ.Product_Id__c = 'VLV';
        csServ.Path_Instance_ID__c = NULL;
        csServ.AccountId__c = accList1[0].Id;
        csServ.Bundle_Label_name__c = 'Test Bundle';
        csServ.Parent_Bundle_Flag__c = true;
        csServ.Inventory_Status__c = System.Label.PROVISIONED;
        insert csServ;
        
        List<csord__Service__c> serviceList = new List<csord__Service__c>();
        
        csord__Service__c csServ1 = new csord__Service__c();
        csServ1.Name = 'Test Service 1234'; 
        csServ1.csord__Identification__c = 'Test-Catlyne-4238362';
        csServ1.csord__Order_Request__c = OrdReqList[0].Id;
        csServ1.csord__Subscription__c = SUBList[0].Id;
        csServ1.Billing_Commencement_Date__c = System.Today();
        csServ1.Stop_Billing_Date__c = System.Today();
        csServ1.RAG_Status_Red__c = false ; 
        csServ1.RAG_Reason_Code__c = 'Text Reason';
        csServ1.Opportunity__c = oppList[0].Id;
        csServ1.csord__Order__c = Orderlist[0].Id;
        csServ1.Customer_Required_Date__c = System.today();
        csServ1.Estimated_Start_Date__c = System.now().addDays(10);
        csServ1.Selling_Entity__c = 'Telstra Limited';
        csServ1.Service_Type__c = 'Test service';
        csServ1.Product_Code__c = 'VLV';
        csServ1.Order_Channel_Type__c = 'Direct Sale';
        csServ1.csordtelcoa__Product_Basket__c = Products[0].Id;
        csServ1.csordtelcoa__Product_Configuration__c = proconfig[0].Id;
        csServ1.Updated__c = true;
        csServ1.Product_Id__c = 'VLV';
        csServ1.Path_Instance_ID__c = NULL;
        csServ1.AccountId__c = accList1[0].Id;
        csServ1.Bundle_Label_name__c = 'Test Bundle';
        csServ1.Parent_Bundle_Flag__c = true;
        csServ1.Inventory_Status__c = System.Label.PROVISIONED;
        serviceList.add(csServ1);
        
        csord__Service__c csServ2 = new csord__Service__c();
        csServ2.Name = 'Test Service 1234'; 
        csServ2.csord__Identification__c = 'Test-Catlyne-4238362';
        csServ2.csord__Order_Request__c = OrdReqList[0].Id;
        csServ2.csord__Subscription__c = SUBList[0].Id;
        csServ2.Billing_Commencement_Date__c = System.Today().addDays(-2);
        csServ2.Stop_Billing_Date__c = System.Today();
        csServ2.RAG_Status_Red__c = false ; 
        csServ2.RAG_Reason_Code__c = 'Text Reason';
        csServ2.Opportunity__c = oppList[0].Id;
        csServ2.csord__Order__c = Orderlist[0].Id;
        csServ2.Customer_Required_Date__c = System.today();
        csServ2.Estimated_Start_Date__c = System.now().addDays(10);
        csServ2.Selling_Entity__c = 'Telstra Limited';
        csServ2.Service_Type__c = 'Test service';
        csServ2.Product_Code__c = 'NID';
        csServ2.Order_Channel_Type__c = 'Direct Sale';
        csServ2.csordtelcoa__Product_Basket__c = Products[0].Id;
        csServ2.csordtelcoa__Product_Configuration__c = proconfig[0].Id;
        csServ2.Updated__c = true;
        csServ2.Product_Id__c = 'CUSTSITEOFFNETPOP123';
        csServ2.Path_Instance_ID__c = NULL;
        csServ2.AccountId__c = accList1[0].Id;
        csServ2.Bundle_Label_name__c = 'Test Bundle';
        csServ2.Parent_Bundle_Flag__c = true;
        serviceList.add(csServ2);
        
        csord__Service__c csServ3 = new csord__Service__c();
        csServ3.Name = 'Test Service 1234'; 
        csServ3.csord__Identification__c = 'Test-Catlyne-4238362';
        csServ3.csord__Order_Request__c = OrdReqList[0].Id;
        csServ3.csord__Subscription__c = SUBList[0].Id;
        //csServ3.Billing_Commencement_Date__c = System.Today().addDays(-2);
        csServ3.Stop_Billing_Date__c = System.Today();
        csServ3.RAG_Status_Red__c = false ; 
        csServ3.RAG_Reason_Code__c = 'Text Reason';
        csServ3.Opportunity__c = oppList[0].Id;
        csServ3.csord__Order__c = Orderlist[0].Id;
        csServ3.Customer_Required_Date__c = System.today();
        csServ3.Estimated_Start_Date__c = System.now().addDays(10);
        csServ3.Selling_Entity__c = 'Telstra Limited';
        csServ3.Service_Type__c = 'Test service';
        csServ3.Product_Code__c = 'NID';
        csServ3.Order_Channel_Type__c = 'Direct Sale';
        csServ3.csordtelcoa__Product_Basket__c = Products[0].Id;
        csServ3.csordtelcoa__Product_Configuration__c = proconfig[0].Id;
        csServ3.Updated__c = true;
        csServ3.Product_Id__c = 'CUSTSITEOFFNETPOP123';
        csServ3.Path_Instance_ID__c = NULL;
        csServ3.AccountId__c = accList1[0].Id;
        csServ3.Bundle_Label_name__c = 'Test Bundle';
        csServ3.Parent_Bundle_Flag__c = true;
        serviceList.add(csServ3);
        
        csord__Service__c csServ4 = new csord__Service__c();
        csServ4.Name = 'Test Service 1234'; 
        csServ4.csord__Identification__c = 'Test-Catlyne-4238362';
        csServ4.csord__Order_Request__c = OrdReqList[0].Id;
        csServ4.csord__Subscription__c = SUBList[0].Id;
        //csServ4.Billing_Commencement_Date__c = System.Today().addDays(-2);
        csServ4.Stop_Billing_Date__c = System.Today();
        csServ4.RAG_Status_Red__c = false ; 
        csServ4.RAG_Reason_Code__c = 'Text Reason';
        csServ4.Opportunity__c = oppList[0].Id;
        csServ4.csord__Order__c = Orderlist[0].Id;
        csServ4.Customer_Required_Date__c = System.today();
        csServ4.Estimated_Start_Date__c = System.now().addDays(10);
        csServ4.Selling_Entity__c = 'Telstra Limited';
        csServ4.Service_Type__c = 'Test service';
        csServ4.Product_Code__c = 'NID';
        csServ4.Order_Channel_Type__c = 'Direct Sale';
        csServ4.csordtelcoa__Product_Basket__c = Products[0].Id;
        csServ4.csordtelcoa__Product_Configuration__c = proconfig[0].Id;
        csServ4.Updated__c = true;
        csServ4.Product_Id__c = 'CUSTSITEOFFNETPOP123';
        csServ4.Path_Instance_ID__c = NULL;
        csServ4.AccountId__c = accList1[0].Id;
        csServ4.Bundle_Label_name__c = 'Test Bundle';
        csServ4.Parent_Bundle_Flag__c = true;
        //csServ4.Inventory_Status__c = System.Label.PROVISIONED;
        serviceList.add(csServ4);
        
        csord__Service__c csServ5 = new csord__Service__c();
        csServ5.Name = 'Test Service 1234'; 
        csServ5.csord__Identification__c = 'Test-Catlyne-4238362';
        csServ5.csord__Order_Request__c = OrdReqList[0].Id;
        csServ5.csord__Subscription__c = SUBList[0].Id;
        //csServ5.Billing_Commencement_Date__c = System.Today().addDays(-2);
        csServ5.Stop_Billing_Date__c = System.Today().addDays(-1);
        csServ5.RAG_Status_Red__c = false ; 
        csServ5.RAG_Reason_Code__c = 'Text Reason';
        csServ5.Opportunity__c = oppList[0].Id;
        csServ5.csord__Order__c = Orderlist[0].Id;
        csServ5.Customer_Required_Date__c = System.today();
        csServ5.Estimated_Start_Date__c = System.now().addDays(10);
        csServ5.Selling_Entity__c = 'Telstra Limited';
        csServ5.Service_Type__c = 'Test service';
        csServ5.Product_Code__c = 'NID';
        csServ5.Order_Channel_Type__c = 'Direct Sale';
        csServ5.csordtelcoa__Product_Basket__c = Products[0].Id;
        csServ5.csordtelcoa__Product_Configuration__c = proconfig[0].Id;
        csServ5.Updated__c = true;
        csServ5.Product_Id__c = 'CUSTSITEOFFNETPOP123';
        csServ5.Path_Instance_ID__c = NULL;
        csServ5.AccountId__c = accList1[0].Id;
        csServ5.Bundle_Label_name__c = 'Test Bundle';
        csServ5.Parent_Bundle_Flag__c = true;
        //csServ5.Inventory_Status__c = System.Label.PROVISIONED;
        csServ5.Bundle_Flag__c = true;
        csServ5.csord__Service__c = csServ.Id;
        serviceList.add(csServ5);
        
        csord__Service__c csServ6 = new csord__Service__c();
        csServ6.Name = 'Test Service 1234'; 
        csServ6.csord__Identification__c = 'Test-Catlyne-4238362';
        csServ6.csord__Order_Request__c = OrdReqList[0].Id;
        csServ6.csord__Subscription__c = SUBList[0].Id;
        csServ6.Billing_Commencement_Date__c = System.Today();
        csServ6.Stop_Billing_Date__c = System.Today();
        csServ6.RAG_Status_Red__c = false ; 
        csServ6.RAG_Reason_Code__c = 'Text Reason';
        csServ6.Opportunity__c = oppList[0].Id;
        csServ6.csord__Order__c = Orderlist[0].Id;
        csServ6.Customer_Required_Date__c = System.today();
        csServ6.Estimated_Start_Date__c = System.now().addDays(10);
        csServ6.Selling_Entity__c = 'Telstra Limited';
        csServ6.Service_Type__c = 'Test service';
        csServ6.Product_Code__c = 'NID';
        csServ6.Order_Channel_Type__c = 'Direct Sale';
        csServ6.csordtelcoa__Product_Basket__c = Products[0].Id;
        csServ6.csordtelcoa__Product_Configuration__c = proconfig[0].Id;
        csServ6.Updated__c = true;
        csServ6.Product_Id__c = 'CUSTSITEOFFNETPOP123';
        csServ6.Path_Instance_ID__c = NULL;
        csServ6.AccountId__c = accList1[0].Id;
        csServ6.Bundle_Label_name__c = 'Test Bundle';
        csServ6.Parent_Bundle_Flag__c = true;
        csServ6.Usage_Flag__c = 'Yes';
        serviceList.add(csServ6);
        
        csord__Service__c csServ7 = new csord__Service__c();
        csServ7.Name = 'Test Service 1234'; 
        csServ7.csord__Identification__c = 'Test-Catlyne-4238362';
        csServ7.csord__Order_Request__c = OrdReqList[0].Id;
        csServ7.csord__Subscription__c = SUBList[0].Id;
        csServ7.Billing_Commencement_Date__c = System.Today();
        csServ7.Stop_Billing_Date__c = System.Today();
        csServ7.RAG_Status_Red__c = false ; 
        csServ7.RAG_Reason_Code__c = 'Text Reason';
        csServ7.Opportunity__c = oppList[0].Id;
        csServ7.csord__Order__c = Orderlist[0].Id;
        csServ7.Customer_Required_Date__c = System.today();
        csServ7.Estimated_Start_Date__c = System.now().addDays(10);
        csServ7.Selling_Entity__c = 'Telstra Limited';
        csServ7.Service_Type__c = 'Test service';
        csServ7.Product_Code__c = 'IPVPN';
        csServ7.Order_Channel_Type__c = 'Direct Sale';
        csServ7.csordtelcoa__Product_Basket__c = Products[0].Id;
        csServ7.csordtelcoa__Product_Configuration__c = proconfig[0].Id;
        csServ7.Updated__c = true;
        csServ7.Path_Instance_ID__c = NULL;
        csServ7.AccountId__c = accList1[0].Id;
        csServ7.Bundle_Label_name__c = 'Test Bundle';
        csServ7.Parent_Bundle_Flag__c = true;
        csServ7.Usage_Flag__c = 'Yes';
        csServ7.Product_Id__c = 'CUSTSITEOFFNETPOP123';
        serviceList.add(csServ7);
        
        insert serviceList;
        
        List<Case> caseList = P2A_TestFactoryCls.getcase(1,accList1);
        caseList[0].order__c = Orderlist[0].Id;
        caseList[0].Subject = 'Request to calculate ETC';
        caseList[0].CS_Service__c = serviceList[0].Id;
        update caseList;
        
        List<csord__Service_Line_Item__c> serLineItemList = P2A_TestFactoryCls.getSerLineItem(1,serviceList,OrdReqList);
        for(csord__Service_Line_Item__c csServiceLineItem : serLineItemList){
            csServiceLineItem.Is_Miscellaneous_Credit_Flag__c = false;
            csServiceLineItem.Name = 'Test Service Line Item';
        }
        update serLineItemList;
 
        PageReference tpageRef1 = Page.TriggerIP006;
        Test.setCurrentPage(tpageRef1);        
        ApexPages.currentPage().getParameters().put('Id', serviceList[0].id);
        TriggerIP006Controller trGenericCon1= new TriggerIP006Controller(); 
        TriggerIP006Controller trcustomCon1= new TriggerIP006Controller(new ApexPages.StandardController(serviceList[0]));
        Pagereference p1 = trcustomCon1.validateService(); 
        Test.stopTest();       
    }
    
    /*static testMethod void TriggerIP006ControllerTestMethod2() 
    {        
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        P2A_TestFactoryCls.sampleTestData();
        
        Product_Definition_Id__c pdI = new Product_Definition_Id__c();
        pdI.Name = 'IPVPN_Product_ID';
        pdI.Product_Id__c = 'IPVPN';
        insert pdI;
        Product_Definition_Id__c pdI1 = new Product_Definition_Id__c();
        pdI1.Name = 'VPLS_Product_ID';
        pdI1.Product_Id__c = 'VLM';
        insert pdI1;
        Product_Definition_Id__c pdI2 = new Product_Definition_Id__c();
        pdI2.Name = 'IPC_Product_ID';
        pdI2.Product_Id__c = 'IPTS-C';
        insert pdI2;
        
        List<csord__Order__c> Orderlist = new list<csord__Order__c>();
        List<csord__Order__c> Orderlists = new list<csord__Order__c>();     
        Id orderid=ApexPages.currentPage().getParameters().get('Id') ;
        List<cscfga__Product_Definition__c> pdlist = new List<cscfga__Product_Definition__c>(); 
        List<csord__Order__c> ordrslists = new List<csord__Order__c>();
        
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<User> users = P2A_TestFactoryCls.get_Users(1);
        List<Country_Lookup__c> countrylist = P2A_TestFactoryCls.getcountry(1);
        List<Site__c> sitelist = P2A_TestFactoryCls.getsites(1,accList,countrylist);
        
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.name = 'TWI Singlehome';
        pd.cscfga__Description__c= 'GCPEbdkjdvbhe;ghwfguwefkjwf';
        pdlist.add(pd);
        insert pdlist;
        System.assertEquals(pdlist[0].name,'TWI Singlehome'); 
        
        Test.starttest();
        
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,Products,pdlist,pbundlelist,Offerlists);
        
        Account acc = new Account();
        acc.name                            = 'Test Accenture';
        acc.BillingCountry                  = 'GB';
        acc.Activated__c                    = True;
        acc.Account_ID__c                   = '3333';
        acc.Account_Status__c               = 'Active';
        acc.Customer_Legal_Entity_Name__c   = 'Test';
        acc.Customer_Type_New__c            = 'MNC';
        acc.Region__c                       = 'US';
        acc.Selling_Entity__c               = 'Telstra Limited';
        List<Account> accList1 = new List<Account>();
        accList1.add(acc);
        
        Account acc1 = new Account();
        acc1.name                           = 'Test Accenture';
        acc1.BillingCountry                 = 'GB';
        acc1.Activated__c                   = True;
        acc1.Account_ID__c                  = '3333';
        acc1.Account_Status__c              = 'Active';
        acc1.Customer_Legal_Entity_Name__c  = 'Test';
        acc1.Customer_Type_New__c           = 'MNC';
        acc1.Region__c                      = 'US';
        acc1.Selling_Entity__c              = 'Telstra Limited';
        accList1.add(acc1);
        insert accList1;
        
        List<Contact> contactList = P2A_TestFactoryCls.getContact(1,accList1);
        
        csord__Order__c ord = new csord__Order__c();
        ord.csord__Identification__c = 'Test-JohnSnow-4238362';
        ord.RAG_Order_Status_RED__c = false;
        ord.RAG_Reason_Code__c = '';
        ord.Id = orderid;
        ord.Order_Type__c = 'New';
        ord.Is_Terminate_Order__c = false;
        ord.Is_Order_Submitted__c = true;
        ord.SD_PM_Contact__c = NULL; 
        ord.Reseller_Account_ID__c = NULL ; 
        ord.csord__Order_Request__c = OrdReqList[0].Id;
        Orderlist.add(ord);
        insert orderlist;
        System.assertEquals(orderlist[0].csord__Identification__c ,'Test-JohnSnow-4238362'); 
        
        Country_Lookup__c country = new Country_Lookup__c();
        country.CCMS_Country_Code__c = 'HK';
        country.CCMS_Country_Name__c = 'India';
        country.Country_Code__c = 'HK';
        insert country;
        
        City_Lookup__c ci = new City_Lookup__c();
        ci.City_Code__c ='MUM';
        ci.Name = 'MUMBAI';
        insert ci;
        
        Site__c site= new Site__c();
        site.Name = 'Test_site';
        site.Address1__c = '43';
        site.Address2__c = 'Bangalore';
        site.Country_Finder__c = country.Id;
        site.City_Finder__c = ci.Id;
        site.AccountId__c =  accList1[0].Id; 
        site.Address_Type__c = 'Billing Address';
        insert site;
        
        BillProfile__c BProfObj = new BillProfile__c();
        BProfObj.Account__c = accList1[0].Id;
        BProfObj.Billing_Entity__c = 'Telstra Corporation Limited';
        BProfObj.Payment_Terms__c = '45 Days';
        BProfObj.Billing_Frequency__c = 'Hong Kong Annual Voice';
        BProfObj.Billing_Contact__c = contactList[0].id;
        BProfObj.Activated__c = true;
        //BProfObj.Status__c = 'Active';
        BProfObj.Approved__c = TRUE;
        BProfObj.Bill_Profile_Site__c = site.Id;
        insert BProfObj;
        
        csord__Service__c csServ = new csord__Service__c();
        csServ.Name = 'Test Service'; 
        csServ.csord__Identification__c = 'Test-Catlyne-4238362';
        csServ.csord__Order_Request__c = OrdReqList[0].Id;
        csServ.csord__Subscription__c = SUBList[0].Id;
        csServ.Billing_Commencement_Date__c = System.Today().addDays(2);
        csServ.Stop_Billing_Date__c = System.Today().addDays(2);
        csServ.RAG_Status_Red__c = false ; 
        csServ.RAG_Reason_Code__c = 'Text Reason';
        csServ.Opportunity__c = oppList[0].Id;
        csServ.csord__Order__c = Orderlist[0].Id;
        csServ.Customer_Required_Date__c = System.today();
        csServ.Estimated_Start_Date__c = System.now().addDays(10);
        csServ.Selling_Entity__c = 'Telstra Limited';
        csServ.Service_Type__c = 'Test service';
        csServ.Product_Code__c = 'VLV';
        csServ.Order_Channel_Type__c = 'Direct Sale';
        csServ.csordtelcoa__Product_Basket__c = Products[0].Id;
        csServ.csordtelcoa__Product_Configuration__c = proconfig[0].Id;
        csServ.Updated__c = true;
        csServ.Product_Id__c = 'VLV';
        csServ.Path_Instance_ID__c = NULL;
        csServ.AccountId__c = accList1[0].Id;
        csServ.Bundle_Label_name__c = 'Test Bundle';
        csServ.Parent_Bundle_Flag__c = true;
        csServ.Inventory_Status__c = System.Label.PROVISIONED;
        csServ.Cease_Service_Flag__c = true;
        insert csServ;
        
        List<csord__Service__c> serviceList = new List<csord__Service__c>();
        
        csord__Service__c csServ1 = new csord__Service__c();
        csServ1.Name = 'Test Service 1234'; 
        csServ1.csord__Identification__c = 'Test-Catlyne-4238362';
        csServ1.csord__Order_Request__c = OrdReqList[0].Id;
        csServ1.csord__Subscription__c = SUBList[0].Id;
        csServ1.Billing_Commencement_Date__c = System.Today();
        csServ1.Stop_Billing_Date__c = System.Today();
        csServ1.RAG_Status_Red__c = false ; 
        csServ1.RAG_Reason_Code__c = 'Text Reason';
        csServ1.Opportunity__c = oppList[0].Id;
        csServ1.csord__Order__c = Orderlist[0].Id;
        csServ1.Customer_Required_Date__c = System.today();
        csServ1.Estimated_Start_Date__c = System.now().addDays(10);
        csServ1.Selling_Entity__c = 'Telstra Limited';
        csServ1.Service_Type__c = 'Test service';
        csServ1.Product_Code__c = 'VLV';
        csServ1.Order_Channel_Type__c = 'Direct Sale';
        csServ1.csordtelcoa__Product_Basket__c = Products[0].Id;
        csServ1.csordtelcoa__Product_Configuration__c = proconfig[0].Id;
        csServ1.Updated__c = true;
        csServ1.Product_Id__c = 'VLV';
        csServ1.Path_Instance_ID__c = NULL;
        csServ1.AccountId__c = accList1[0].Id;
        //csServ1.Bundle_Label_name__c = 'Test Bundle';
        csServ1.Parent_Bundle_Flag__c = false;
        csServ1.Inventory_Status__c = System.Label.PROVISIONED;
        csServ1.Country__c = 'US';
        csServ1.Root_Bill_Text__c = 'Test';
        csServ1.Service_Bill_Text__c = 'Test';
        csServ1.Bill_ProfileId__c = NULL;
        csServ1.csord__Service__c = csServ.Id;
        csServ1.Bundle_Flag__c = true;
        csServ1.Cease_Service_Flag__c = true;
        serviceList.add(csServ1);
        
        csord__Service__c csServ2 = new csord__Service__c();
        csServ2.Name = 'Test Service 1234'; 
        csServ2.csord__Identification__c = 'Test-Catlyne-4238362';
        csServ2.csord__Order_Request__c = OrdReqList[0].Id;
        csServ2.csord__Subscription__c = SUBList[0].Id;
        csServ2.Billing_Commencement_Date__c = NULL;
        csServ2.Stop_Billing_Date__c = System.Today();
        csServ2.RAG_Status_Red__c = false ; 
        csServ2.RAG_Reason_Code__c = 'Text Reason';
        csServ2.Opportunity__c = oppList[0].Id;
        csServ2.csord__Order__c = Orderlist[0].Id;
        csServ2.Customer_Required_Date__c = System.today();
        csServ2.Estimated_Start_Date__c = System.now().addDays(10);
        csServ2.Selling_Entity__c = 'Telstra Limited';
        csServ2.Service_Type__c = 'Test service';
        csServ2.Product_Code__c = 'VLV';
        csServ2.Order_Channel_Type__c = 'Direct Sale';
        csServ2.csordtelcoa__Product_Basket__c = Products[0].Id;
        csServ2.csordtelcoa__Product_Configuration__c = proconfig[0].Id;
        csServ2.Updated__c = true;
        csServ2.Product_Id__c = 'VLV';
        csServ2.Path_Instance_ID__c = NULL;
        csServ2.AccountId__c = accList1[0].Id;
        //csServ2.Bundle_Label_name__c = 'Test Bundle';
        csServ2.Parent_Bundle_Flag__c = false;
        csServ2.Inventory_Status__c = System.Label.PROVISIONED;
        csServ2.Country__c = 'US';
        csServ2.Root_Bill_Text__c = 'Test';
        csServ2.Service_Bill_Text__c = 'Test';
        csServ2.Bill_ProfileId__c = NULL;
        csServ2.csord__Service__c = csServ.Id;
        csServ2.Bundle_Flag__c = true;
        csServ2.Cease_Service_Flag__c = true;
        csServ2.Bill_ProfileId__c = BProfObj.Id;
        serviceList.add(csServ2);
                
        insert serviceList;
        
        List<Case> caseList = P2A_TestFactoryCls.getcase(1,accList1);
        caseList[0].order__c = Orderlist[0].Id;
        caseList[0].Subject = 'Request to calculate ETC';
        caseList[0].CS_Service__c = csServ.Id;
        update caseList;
        
        List<csord__Service_Line_Item__c> serLineItemList = P2A_TestFactoryCls.getSerLineItem(1,serviceList,OrdReqList);
        for(csord__Service_Line_Item__c csServiceLineItem : serLineItemList){
            csServiceLineItem.Is_Miscellaneous_Credit_Flag__c = false;
            csServiceLineItem.Name = 'Test Service Line Item';
        }
        update serLineItemList;
 
        PageReference tpageRef1 = Page.TriggerIP006;
        Test.setCurrentPage(tpageRef1);        
        ApexPages.currentPage().getParameters().put('Id', serviceList[0].id);
        TriggerIP006Controller trGenericCon1= new TriggerIP006Controller(); 
        TriggerIP006Controller trcustomCon1= new TriggerIP006Controller(new ApexPages.StandardController(serviceList[0]));
        Pagereference p1 = trcustomCon1.validateService(); 
        Test.stopTest();       
    }*/
    
    static testMethod void triggerIP006ControllerTestMethod3() 
    {        
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        P2A_TestFactoryCls.sampleTestData();
        
        Product_Definition_Id__c pdI = new Product_Definition_Id__c();
        pdI.Name = 'IPVPN_Product_ID';
        pdI.Product_Id__c = 'IPVPN';
        insert pdI;
        Product_Definition_Id__c pdI1 = new Product_Definition_Id__c();
        pdI1.Name = 'VPLS_Product_ID';
        pdI1.Product_Id__c = 'VLM';
        insert pdI1;
        Product_Definition_Id__c pdI2 = new Product_Definition_Id__c();
        pdI2.Name = 'IPC_Product_ID';
        pdI2.Product_Id__c = 'IPTS-C';
        insert pdI2;
        
        List<csord__Order__c> Orderlist = new list<csord__Order__c>();
        List<csord__Order__c> Orderlists = new list<csord__Order__c>();     
        Id orderid=ApexPages.currentPage().getParameters().get('Id') ;
        List<cscfga__Product_Definition__c> pdlist = new List<cscfga__Product_Definition__c>(); 
        List<csord__Order__c> ordrslists = new List<csord__Order__c>();
        
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<User> users = P2A_TestFactoryCls.get_Users(1);
        List<Country_Lookup__c> countrylist = P2A_TestFactoryCls.getcountry(1);
        List<Site__c> sitelist = P2A_TestFactoryCls.getsites(1,accList,countrylist);
        
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.name = 'TWI Singlehome';
        pd.cscfga__Description__c= 'GCPEbdkjdvbhe;ghwfguwefkjwf';
        pdlist.add(pd);
        insert pdlist;
        System.assertEquals(pdlist[0].name,'TWI Singlehome'); 
        
        Test.starttest();
        
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,Products,pdlist,pbundlelist,Offerlists);
        
        Account acc = new Account();
        acc.name                            = 'Test Accenture';
        acc.BillingCountry                  = 'GB';
        acc.Activated__c                    = True;
        acc.Account_ID__c                   = '3333';
        acc.Account_Status__c               = 'Active';
        acc.Customer_Legal_Entity_Name__c   = 'Test';
        acc.Customer_Type_New__c            = 'MNC';
        acc.Region__c                       = 'US';
        acc.Selling_Entity__c               = 'Telstra Limited';
        List<Account> accList1 = new List<Account>();
        accList1.add(acc);
        
        Account acc1 = new Account();
        acc1.name                           = 'Test Accenture';
        acc1.BillingCountry                 = 'GB';
        acc1.Activated__c                   = True;
        acc1.Account_ID__c                  = '3333';
        acc1.Account_Status__c              = 'Active';
        acc1.Customer_Legal_Entity_Name__c  = 'Test';
        acc1.Customer_Type_New__c           = 'MNC';
        acc1.Region__c                      = 'US';
        acc1.Selling_Entity__c              = 'Telstra Limited';
        accList1.add(acc1);
        insert accList1;
        
        csord__Order__c ord = new csord__Order__c();
        ord.csord__Identification__c = 'Test-JohnSnow-4238362';
        ord.RAG_Order_Status_RED__c = false;
        ord.RAG_Reason_Code__c = '';
        ord.Id = orderid;
        ord.Order_Type__c = 'New';
        ord.Is_Terminate_Order__c = false;
        ord.Is_Order_Submitted__c = true;
        ord.SD_PM_Contact__c = NULL; 
        ord.Reseller_Account_ID__c = NULL ; 
        ord.csord__Order_Request__c = OrdReqList[0].Id;
        Orderlist.add(ord);
        insert orderlist;
        System.assertEquals(orderlist[0].csord__Identification__c ,'Test-JohnSnow-4238362'); 
        
        csord__Service__c csServ = new csord__Service__c();
        csServ.Name = 'Test Service'; 
        csServ.csord__Identification__c = 'Test-Catlyne-4238362';
        csServ.csord__Order_Request__c = OrdReqList[0].Id;
        csServ.csord__Subscription__c = SUBList[0].Id;
        csServ.Billing_Commencement_Date__c = System.Today();
        csServ.Stop_Billing_Date__c = System.Today().addDays(-2);
        csServ.RAG_Status_Red__c = false ; 
        csServ.RAG_Reason_Code__c = 'Text Reason';
        csServ.Opportunity__c = oppList[0].Id;
        csServ.csord__Order__c = Orderlist[0].Id;
        csServ.Customer_Required_Date__c = System.today();
        csServ.Estimated_Start_Date__c = System.now().addDays(10);
        csServ.Selling_Entity__c = 'Telstra Limited';
        csServ.Service_Type__c = 'Test service';
        csServ.Product_Code__c = 'VLV';
        csServ.Order_Channel_Type__c = 'Direct Sale';
        csServ.csordtelcoa__Product_Basket__c = Products[0].Id;
        csServ.csordtelcoa__Product_Configuration__c = proconfig[0].Id;
        csServ.Updated__c = true;
        csServ.Product_Id__c = 'VLV';
        csServ.Path_Instance_ID__c = NULL;
        csServ.AccountId__c = accList1[0].Id;
        csServ.Bundle_Label_name__c = 'Test Bundle';
        csServ.Parent_Bundle_Flag__c = true;
        csServ.Bundle_Flag__c = true;
        csServ.Inventory_Status__c = System.Label.PROVISIONED;
        csServ.Usage_Flag__c = 'Yes';
        //csServ.Primary_Service_ID__c = 'Test';
        insert csServ;
        
        List<csord__Service__c> serviceList = new List<csord__Service__c>();
        
        csord__Service__c csServ1 = new csord__Service__c();
        csServ1.Name = 'Test Service 1234'; 
        csServ1.csord__Identification__c = 'Test-Catlyne-4238362';
        csServ1.csord__Order_Request__c = OrdReqList[0].Id;
        csServ1.csord__Subscription__c = SUBList[0].Id;
        csServ1.Billing_Commencement_Date__c = System.Today();
        csServ1.Stop_Billing_Date__c = System.Today();
        csServ1.RAG_Status_Red__c = false ; 
        csServ1.RAG_Reason_Code__c = 'Text Reason';
        csServ1.Opportunity__c = oppList[0].Id;
        csServ1.csord__Order__c = Orderlist[0].Id;
        csServ1.Customer_Required_Date__c = System.today();
        csServ1.Estimated_Start_Date__c = System.now().addDays(10);
        csServ1.Selling_Entity__c = 'Telstra Limited';
        csServ1.Service_Type__c = 'Test service';
        //csServ1.Product_Code__c = 'VLV';
        csServ1.Order_Channel_Type__c = 'Direct Sale';
        csServ1.csordtelcoa__Product_Basket__c = Products[0].Id;
        csServ1.csordtelcoa__Product_Configuration__c = proconfig[0].Id;
        csServ1.Updated__c = true;
        csServ1.Product_Id__c = 'VLV';
        csServ1.Path_Instance_ID__c = NULL;
        csServ1.AccountId__c = accList1[0].Id;
        csServ1.Bundle_Label_name__c = 'Test Bundle';
        csServ1.Parent_Bundle_Flag__c = true;
        csServ1.Inventory_Status__c = System.Label.PROVISIONED;
        csServ1.csord__Service__c = csServ.Id;
        csServ1.Bundle_Flag__c = true;
        csServ1.Cease_Service_Flag__c = true;
        serviceList.add(csServ1);
        
        insert serviceList;
        
        List<Case> caseList = P2A_TestFactoryCls.getcase(1,accList1);
        caseList[0].order__c = Orderlist[0].Id;
        caseList[0].Subject = 'Request to calculate ETC';
        caseList[0].CS_Service__c = serviceList[0].Id;
        update caseList;
        
        List<csord__Service_Line_Item__c> serLineItemList = P2A_TestFactoryCls.getSerLineItem(1,serviceList,OrdReqList);
        for(csord__Service_Line_Item__c csServiceLineItem : serLineItemList){
            csServiceLineItem.Is_Miscellaneous_Credit_Flag__c = false;
            csServiceLineItem.Name = 'Test Service Line Item';
        }
        update serLineItemList;
 
        PageReference tpageRef1 = Page.TriggerIP006;
        Test.setCurrentPage(tpageRef1);        
        ApexPages.currentPage().getParameters().put('Id', serviceList[0].id);
        TriggerIP006Controller trGenericCon1= new TriggerIP006Controller(); 
        TriggerIP006Controller trcustomCon1= new TriggerIP006Controller(new ApexPages.StandardController(serviceList[0]));
        Pagereference p1 = trcustomCon1.validateService(); 
        Test.stopTest();      
    }
}