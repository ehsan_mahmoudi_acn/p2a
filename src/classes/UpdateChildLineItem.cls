/**
 This class is used to Update child line item status to complete if parent line item status is complete
*/

Public class UpdateChildLineItem{
 /* public static boolean ischildOLIComplete = True;
    
    public void updateChileOLILIst(List<Order_Line_Item__c> childOLILIst){
        System.debug('I am in updateChileOLILIst' + childOLILIst);
        Map<Id,Order_Line_Item__c> mapServResOli=new Map<Id,Order_Line_Item__c>();
        for(Order_Line_Item__c oli:childOLILIst){
            
            mapServResOli.put(oli.Id,oli);
            
            
        }
        system.debug('=====mapServResOli====='+mapServResOli);
        List<Service__c> srvLst = [select id,Billing_Commencement_Date__c,OLI_ID__c from Service__c where OLI_ID__c=:mapServResOli.keySet()];
         List<Resource__c> resLst = [select id,Billing_Commencement_Date__c,OLI_ID__c from Resource__c where OLI_ID__c=:mapServResOli.keySet()  ];
      
       for(Resource__c res:resLst){
        if(mapServResOli.containskey(Id.valueOf(res.OLI_ID__c))){
            res.Billing_Commencement_Date__c=mapServResOli.get(Id.valueOf(res.OLI_ID__c)).Billing_Commencement_Date__c;
            
        }
        
    }
    
     for(Service__c serv:srvLst){
        if(mapServResOli.containskey(Id.valueOf(serv.OLI_ID__c))){
            serv.Billing_Commencement_Date__c=mapServResOli.get(Id.valueOf(serv.OLI_ID__c)).Billing_Commencement_Date__c;
            
        }
        
    }
      
        ischildOLIComplete = false;
        update childOLILIst;
        update srvLst;
        update resLst;
    }

*/
}