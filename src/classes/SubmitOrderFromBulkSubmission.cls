public class SubmitOrderFromBulkSubmission{ 

   /* public  List<Id>OrderIdList{get;set;}
    public  set<Id>MasterOrderSet{get;set;}
    public  Boolean isChildSubmitible{get;set;}
    public  Boolean isBatchComplete{get;set;}
    public  id solutionId{get;set;}
    public  solutions__c  solnObj{get;set;}
    
    
    
    //contructor
    public SubmitOrderFromBulkSubmission(List<Id>OrderIdList,set<Id>MasterOrderSet,Boolean isChildSubmitible) {
        this.OrderIdList=OrderIdList;
        this.MasterOrderSet=MasterOrderSet;
        this.isChildSubmitible=isChildSubmitible;
        id orderId=OrderIdList[0];
        isBatchComplete=isChildSubmitible;
        csord__order__c ordertosolution=[select id,Solutions__c from csord__order__c where Id =:orderId];
        solnObj=[select id,batch_count__c from solutions__c where id=:ordertosolution.Solutions__c];
        solnObj.batch_count__c=OrderIdList.size();
        update solnObj;
        id solutionId=solnObj.id;
        getOrderList(MasterOrderSet,OrderIdList,isChildSubmitible,solnObj,solutionId);        
    }
    
    //start method: will return list of order Sobject
    public static void getOrderList(set<Id>MasterOrderSet,List<Id>OrderIdList,Boolean isBatchComplete,solutions__c solnObj,id solutionId)
    {
        List<csord__order__c> orderList=[select id,Solutions__c from csord__order__c where Id IN:MasterOrderSet];
        SubmitOrderCall(orderList,OrderIdList,isBatchComplete,solnObj,solutionId);
    }
    
    //execute method will process the list of order sobject and submit the order
    public static  void submitOrderCall(List<csord__order__c> orderList,List<Id>OrderIdList,Boolean isBatchComplete,solutions__c solnObj,id solutionId)

    {            
         List<csord__order__c>updateOrderList=new List<csord__order__c>();
            for (csord__order__c ord:orderList){
              try{                    
                       SubmitOrder.submitOrderToFOMFromBatch(ord.id);//submission of Orders
                       ord.Is_Order_Submitted__c = true;
                       ord.Status__c = 'Submitted'; 
                       ord.Clean_Order_Check_Passed__c = true;
                       updateOrderList.add(ord);  
                    
                }
               catch(Exception e){
                  System.debug(e);
               } 
            }            
        
        if(updateOrderList.size()>0){update updateOrderList;}
        if(isBatchComplete){           
           //TriggerFlags.BatchSuccess='Batch Completed SuccessFully';
           //TriggerFlags.BatchFailed='';
           //TriggerFlags.BatchCount=0;//will help in actionPoller in solutionSummary Page
           solnObj=[select id,name,Account_Name__r.name,Opportunity_Name__r.name,batch_count__c from solutions__c where id=:solutionId];
           solnObj.batch_count__c=0;
           update solnObj;
           SolEmailTempCtrl.SendEmail(OrderIdList,solnObj); 
           System.debug('execution finished'); 
           }
    }

   */ 
}