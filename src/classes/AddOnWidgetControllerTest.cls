@isTest(seealldata = false)
public class AddOnWidgetControllerTest {
  
   Public static testmethod void testFeatureMethods1() {
    
       P2A_TestFactoryCls.sampleTestData();
    
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(3);
       for(integer i=0;i<Prodef.size();i++)
       {
           Prodef[i].cscfga__Active__c=true;
       }
       update Prodef;
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(10,Products,prodef,pbundlelist,Offerlists);
        List<cscfga__Configuration_Screen__c> cscreen = P2A_TestFactoryCls.getConfigScreen(1,Prodef);
        List<cscfga__Screen_Section__c>  sslist = P2A_TestFactoryCls.getScreenSec(1,cscreen);

        cspmb__Add_On_Price_Item__c api = new cspmb__Add_On_Price_Item__c(
        Name = 'Addonpriceitem',cspmb__Product_Definition_Name__c = Prodef[0].name,cspmb__Recurring_Charge__c = 123,Image_URL__c = 'testurl'
        );
        insert api;         
        
        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(
            Name = 'Price item'
        );
        insert priceItem;
        system.assertEquals(priceItem.Name,'Price item');
        
         cspmb__Price_Item_Add_On_Price_Item_Association__c piAssoc = new cspmb__Price_Item_Add_On_Price_Item_Association__c(
            cspmb__Price_Item__c = priceItem.Id,
            cspmb__Add_On_Price_Item__c = api.Id
        );
        insert piAssoc;
        integer i= [Select count() from cspmb__Price_Item_Add_On_Price_Item_Association__c];
        system.assert(i>0);

        List<cscfga__Attribute_Definition__c> attdef = P2A_TestFactoryCls.getAttributesdef(1,proconfig , Prodef , cscreen, sslist);
        List<cscfga__Attribute__c> Attributes = P2A_TestFactoryCls.getAttributes(1, proconfig, attdef);

        Add_On_Widget_Settings__c addOnWid = new Add_On_Widget_Settings__c();
        addOnWid.Name = 'Test';
        addOnWid.Add_On_Attribute__c = 'Test';
        addOnWid.Add_On_Definition__c = Prodef[0].name;
        addOnWid.Custom_Object_Fields__c = null;
        addOnWid.Parent_Attribute__c = Attributes[0].name;
        addOnWid.Quantity__c = true;
        addOnWid.Add_On_Fields__c = 'cspmb__Recurring_Charge__c';
        addOnWid.Image__c = '';
        addOnWid.Product_Definition__c = Prodef[0].name;
        addOnWid.Record_Type_Name__c = null;
        //addonwid.Custom_Object__c = 'cspmb__Add_On_Price_Item__c' ;       
        insert addOnWid;
        system.assertEquals(addOnWid.Add_On_Fields__c , 'cspmb__Recurring_Charge__c');
     
        String features = AddOnWidgetController.getAvaialbleAddOns(priceItem.Id, 'Test', proconfig[0].id);
        //String saveResult1 = AddOnWidgetController.saveAddOns(proconfig[0].Id, new List<id>{api.id}, 'Test1');
        String saveResult2 = AddOnWidgetController.saveAddOns(proconfig[0].Id, new List<id>{api.id}, 'Test'); 
     AddOnWidgetController.deleteAddOns(proconfig[0].id, 'Test');
        AddOnWidgetController.deleteAddOns(proconfig[0].id, 'Test1');
       
   }
    
   Public static testmethod void testFeatureMethods2() {
    
       P2A_TestFactoryCls.sampleTestData();
    
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(3);
       for(integer i=0;i<Prodef.size();i++)
       {
           Prodef[i].cscfga__Active__c=true;
       }
       update Prodef;
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(10,Products,prodef,pbundlelist,Offerlists);
        List<cscfga__Configuration_Screen__c> cscreen = P2A_TestFactoryCls.getConfigScreen(1,Prodef);
        List<cscfga__Screen_Section__c>  sslist = P2A_TestFactoryCls.getScreenSec(1,cscreen);

        cspmb__Add_On_Price_Item__c api = new cspmb__Add_On_Price_Item__c(
        Name = 'Addonpriceitem',cspmb__Product_Definition_Name__c = Prodef[0].name,cspmb__Recurring_Charge__c = 123,Image_URL__c = 'testurl'
        );
        insert api;         
        
        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(
            Name = 'Price item'
        );
        insert priceItem;
        system.assertEquals(priceItem.Name,'Price item');
        
         cspmb__Price_Item_Add_On_Price_Item_Association__c piAssoc = new cspmb__Price_Item_Add_On_Price_Item_Association__c(
            cspmb__Price_Item__c = priceItem.Id,
            cspmb__Add_On_Price_Item__c = api.Id
        );
        insert piAssoc;
        integer i= [Select count() from cspmb__Price_Item_Add_On_Price_Item_Association__c];
        system.assert(i>0);

        List<cscfga__Attribute_Definition__c> attdef = P2A_TestFactoryCls.getAttributesdef(1,proconfig , Prodef , cscreen, sslist);
        List<cscfga__Attribute__c> Attributes = P2A_TestFactoryCls.getAttributes(1, proconfig, attdef);

        Add_On_Widget_Settings__c addOnWid = new Add_On_Widget_Settings__c();
        addOnWid.Name = 'Test';
        addOnWid.Add_On_Attribute__c = 'Test';
        addOnWid.Add_On_Definition__c = Prodef[0].name;
        addOnWid.Custom_Object_Fields__c = null;
        addOnWid.Parent_Attribute__c = Attributes[0].name;
        addOnWid.Quantity__c = true;
        addOnWid.Add_On_Fields__c = 'cspmb__Recurring_Charge__c';
        addOnWid.Image__c = '';
        addOnWid.Product_Definition__c = Prodef[0].name;
        addOnWid.Record_Type_Name__c = null;
        addonwid.Custom_Object__c = 'cspmb__Add_On_Price_Item__c' ;       
        insert addOnWid;
        system.assertEquals(addOnWid.Add_On_Fields__c , 'cspmb__Recurring_Charge__c');
     
        String features = AddOnWidgetController.getAvaialbleAddOns(priceItem.Id, 'Test', proconfig[0].id);
        String saveResult1 = AddOnWidgetController.saveAddOns(proconfig[0].Id, new List<id>{api.id}, 'Test1');
        String saveResult2 = AddOnWidgetController.saveAddOns(proconfig[0].Id, new List<id>{api.id}, 'Test'); 
     AddOnWidgetController.deleteAddOns(proconfig[0].id, 'Test');
        AddOnWidgetController.deleteAddOns(proconfig[0].id, 'Test1');
       
   }
    
}