@isTest
Private class GetInventoryDetailsParamsTest{
    static testMethod void myUnitTest(){
        Test.startTest();
        GetInventoryDetailsParams getparam=new GetInventoryDetailsParams();
        GetInventoryDetailsParams.InvDetailsOutputVO invdetails=new GetInventoryDetailsParams.InvDetailsOutputVO();
        GetInventoryDetailsParams.InvDetailsInputVO invdetailsinp=new GetInventoryDetailsParams.InvDetailsInputVO();
        
        GetInventoryDetailsParams.CustomWorkOrderVO customworkorno=new  GetInventoryDetailsParams.CustomWorkOrderVO();
        
        String woName = customworkorno.WoName;
        String projectId = customworkorno.ProjectId;
        String ElementType=customworkorno.ElementType;
        String ElementName=customworkorno.ElementName;
        String Status=customworkorno.Status;
        Integer NumTasks=customworkorno.NumTasks;
        Integer CmpltedTasks=customworkorno.CmpltedTasks;
        String Description=customworkorno.Description;
        String ElementStatus=customworkorno.ElementStatus;
        String ElmComplStatus=customworkorno.ElmComplStatus;
        String ArchiveElement=customworkorno.ArchiveElement;
        String PendingRevisionStatus=customworkorno.PendingRevisionStatus;
        String CrmOrderNum=customworkorno.CrmOrderNum;
        String SharePointURL=customworkorno.SharePointURL;
        GetInventoryDetailsParams.CustomTaskVO[] TaskList=customworkorno.TaskList;
        
        GetInventoryDetailsParams.CustomPathVO custompath=new  GetInventoryDetailsParams.CustomPathVO();
        String aSideCustomer=custompath.aSideCustomer;
        String aSideSiteName=custompath.aSideSiteName;
        Integer NbrChanAssigned=custompath.NbrChanAssigned;
        Integer ChanWarnThreshhold=custompath.ChanWarnThreshhold;
        String Type_x=custompath.Type_x;
        Integer StartChanNbr=custompath.StartChanNbr;
        Integer EndChanNbr=custompath.EndChanNbr;
        String Decommision=custompath.Decommision;
        String Due=custompath.Due;
        String CircPathHumId=custompath.CircPathHumId;
        String InService=custompath.InService;
        String Installed=custompath.Installed;
        String OrderNum=custompath.OrderNum;
        String Ordered=custompath.Ordered;
        String CustomerId=custompath.CustomerId;
        Integer PathRevNbr=custompath.PathRevNbr;
        String ShcedDate=custompath.ShcedDate;
        String Stat=custompath.Status;
        String Topology=custompath.Topology;
        Integer WarnThreshPct=custompath.WarnThreshPct;
        String zSideCustomer=custompath.zSideCustomer;
        String zSideSiteId=custompath.zSideSiteId;
        
        String Bandwidth=custompath.Bandwidth;
        String Role=custompath.Role;
        Integer NbrMembers=custompath.NbrMembers;
        String MemberBw=custompath.MemberBw;
        String Direction=custompath.Direction;
        String BpsAvailable=custompath.BpsAvailable;
        String VirtualChans=custompath.VirtualChans;
        Integer MaxPctOversubscription=custompath.MaxPctOversubscription;
        String PctUtil=custompath.PctUtil;
        String ManagementBw=custompath.ManagementBw;
        String ActualSupplyDate=custompath.ActualSupplyDate;
        String CustomerASNumber=custompath.CustomerASNumber;
        String OrderPlacedWithSupplier=custompath.OrderPlacedWithSupplier;
        String PurchaseOrderNumber=custompath.PurchaseOrderNumber;
        String PurchaseReqNumbers=custompath.PurchaseReqNumbers;
        String SupplierCommitmentDate=custompath.SupplierCommitmentDate;
        String SupplierOrderAccepted=custompath.SupplierOrderAccepted;
        String SupplierTermination=custompath.SupplierTermination;
        
        GetInventoryDetailsParams.CustomTaskVO customtaskout=new GetInventoryDetailsParams.CustomTaskVO();
        Integer Task_Inst_Id =customtaskout.Task_Inst_Id;
        String Action =customtaskout.Action;
        Integer Priority =customtaskout.Priority;
        String Site_hum_id =customtaskout.Site_hum_id;
        String Elm_compl_status =customtaskout.Elm_compl_status;
        String Jeopardy_status =customtaskout.Jeopardy_status;
        String User_x =customtaskout.User_x;
        Long WoInstId=customtaskout.WoInstId;
        String TaskName=customtaskout.TaskName;
        String Status1=customtaskout.Status;
        String TaskOperation=customtaskout.TaskOperation;
        String Elementty=customtaskout.ElementType;
        String LockElemonCo=customtaskout.LockElemonCo;
        String QueueName=customtaskout.QueueName;
        system.assert(customtaskout!=null);
        
        Test.stopTest();
        
      }
}