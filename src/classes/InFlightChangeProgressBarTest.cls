@isTest(SeeAllData = false)
public class InFlightChangeProgressBarTest{

        static testmethod void testInFlightChangeProgressBar1(){
        List<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        
        Test.StartTest();
        Map<Id, csord__Service__c> serviceList = serviceMap();
        CloneSubComponentsOfProductConfig bc = new CloneSubComponentsOfProductConfig(Products[0].Id, Products[0].Id, serviceList);
        Id batchProcessId = Database.executeBatch(bc,1);
 
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        products[0].InFlight_Change_Batch_Job_Id__c = batchProcessId;
        upsert Products;
          System.assert(Products!=null);

        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(Products[0]);
        
        InFlightChangeProgressBar icpb = new InFlightChangeProgressBar(sc);
        icpb.refreshJobStatus();
        icpb.isFinished='Completed';
        icpb.getBatchOrderStatus();
        Test.StopTest();
       }

    private static Map<Id, csord__Service__c> serviceMap(){
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> order = P2A_TestFactoryCls.getorder(1, OrdReqList);

        Map<Id, csord__Service__c> serviceList = new Map<Id, csord__Service__c>();
        List<csord__Service__c> insertService = new List<csord__Service__c>();
        for(integer i=0;i<3;i++){           
            csord__Service__c s = new csord__Service__c();
            s.Name = 'Test Service'; 
            s.csord__Identification__c = 'Test-Catlyne-4238362';
            s.csord__Order_Request__c = OrdReqList[0].Id;
            s.csord__Subscription__c = SUBList[0].Id;
            s.csord__Order__c = order[0].Id;
            s.Billing_Commencement_Date__c = System.Today();
            s.Stop_Billing_Date__c = System.Today();
            s.RAG_Status_Red__c = false ; 
            s.RAG_Reason_Code__c = '';          
            s.Bundle_Flag__c = false;
            s.Product_Id__c = 'ATM';
            s.Product_Code__c = 'ATM';
            s.Inventory_Status__c = Label.PROVISIONED;
            s.Cease_Service_Flag__c = true;          
            insertService.add(s);
        }
        insert insertService;
        System.assert(insertService!=null);
        for(csord__Service__c service :insertService){
            serviceList.put(service.Id, service);           
        }
        return serviceList;
    }      
}