public class CheckRecursive{

    /**
     * This flag is to execute the method 'updateOrderOrchestrationProcess' until the batch job 'BatchCascadDetails' is complete.
     * Method 'updateOrderOrchestrationProcess' is defined in the class 'AllOrderTriggersHandler'.
     */
    public static boolean updateOrderOrchestrationProcessExecute = false;

    /**
     * This flag is to stop execution of the method 'AttributeValueIsChanged' until the batch job 'ApproveCaseCtrlBatch' is complete.
     * Method 'AttributeValueIsChanged' is defined in the class 'AllAttributeTriggerHandler'.
     */
    public static boolean AttributeValueIsChangedDontExecute = false;   

    @TestVisible
    private static Set<String> checkValues = new Set<String>();
    private static Set<Id> checkIdValues = new Set<Id>();

    /** Check if the call has already been made for the record and block if yes **/
    public static Boolean canExecuteUpdate(Set<Id> alreadyExecutedIds){
        if(alreadyExecutedIds.Size() > 0){
            boolean flag = false;
            for(Id value :alreadyExecutedIds){
                if(!checkIdValues.contains(value)){
                    checkIdValues.add(value);
                    flag = true;
                    break;
                }
                else{
                    flag = false;
                }
            }
            for(Id value :alreadyExecutedIds){
                if(!checkIdValues.contains(value) && flag == true){
                    checkIdValues.add(value);
                }
            }
            return flag;
        }
        return true;
    }
    
    /** Check if the call has been made already and block if yes **/
    public static Boolean canExecuteBlocking(String value){
        if(checkValues.contains(value)){
            return false;
        }
        else{
            checkValues.add(value);
            return true;
        }
    }   

    /** Check if the call has been made already and block if yes but then reset to allow on next call **/
    public static Boolean canExecuteToggle(String value){
        if(checkValues.contains(value)){
            checkValues.remove(value);
            return false;
        }
        else{
            checkValues.add(value);
            return true;
        }
    }

    /** Get the current setting for the given value **/
    public static Boolean getCurrentSetting(String value){
        if(checkValues.contains(value)){
            return false;
        }
        else{
            return true;
        }
    }

    /*
     * NOTE: The below methods are retained until full refactoring can be done in the meantime rather than create a new method below for every check you wish to use 
     * you can call the above methods which provide the same functionality. All you need to do is pass a unique string as a parameter which will block based on that value.
     */
    public static Boolean runCopyNniNumberToProductBasket(){
        return canExecuteBlocking('copyNniNumberToProductBasket');
    }
    public static Boolean runMACDupdate(){
        return canExecuteBlocking('MACDupdate');
    }
    public static Boolean runCalculateTotalMargin(){
        return canExecuteBlocking('calculateTotalMargin');
    }
    public static Boolean runPricingAssigntoOpportunityowner(){
        return canExecuteBlocking('pricingAssigntoOpportunityowner');
    }   
    public static Boolean runOverridePortRating(){
        return canExecuteBlocking('overridePortRating');
    }
    public static Boolean runGetCurrencyRatioMap(){
        return canExecuteBlocking('getCurrencyRatioMap');
    }
    public static Boolean runOppCloseWon(){
        return canExecuteBlocking('runOppCloseWon');
    }
    public static Boolean runAllProductBasketTrigger(){
        return canExecuteBlocking('runAllProductBasketTrigger');
    }       
    public static Boolean runAllProductBasketTriggerForAfter(){     
        return canExecuteBlocking('runAllProductBasketTriggerForAfter');        
    }
    public static boolean runValidationOptyBeforeTrigger(){
        return canExecuteBlocking('runValidationOptyBeforeTrigger');
    }
    public static boolean runCaseBeforeUpdate(){
        return canExecuteBlocking('caseBeforeUpdate');
    }
    public static boolean runValidationOptyAfterTrigger(){
        return canExecuteBlocking('runValidationOptyAfterTrigger');
    }
    public static Boolean calQuoteRequest(){
        return canExecuteBlocking('calQuoteRequest');
    }
    public static Boolean updateSite(){
        return canExecuteToggle('updateSite');
    }
    public static Boolean setQuoteStatus(){
        return canExecuteToggle('setQuoteStatus');
    }
    public static Boolean serTrgAccHdlrMthd(){
        return canExecuteBlocking('ServiceTrgAccHdlr');
    }
    public static Boolean setOpportunityOwnerEmailId(){
        return canExecuteBlocking('setOpportunityOwnerEmailId');
    }       
    public static Boolean caseAfterUpdate(){
        return canExecuteBlocking('caseAfterUpdate');
    } 
    public static Boolean opportunityBeforeUpdate(){
        return canExecuteBlocking('opportunityBeforeUpdate');
    }  
    public static Boolean opportunityAfterUpdate(){
        return canExecuteBlocking('opportunityAfterUpdate');
    }
    public static Boolean orderBeforeUpdate(){
        return canExecuteBlocking('orderBeforeUpdate');
    }
    public static Boolean orderAfterUpdate(){
        return canExecuteBlocking('orderAfterUpdate');
    }
    public static Boolean productConfigurationBeforeUpdate(){
        return canExecuteBlocking('productConfigurationBeforeUpdate');
    }
    public static Boolean productConfigurationAfterUpdate(){
        return canExecuteBlocking('productConfigurationAfterUpdate');
    }
    public static Boolean stepsBeforeUpdate(){
        return canExecuteBlocking('stepsBeforeUpdate');
    }
    public static Boolean stepsAfterUpdate(){
        return canExecuteBlocking('stepsAfterUpdate');
    }
    public static Boolean newLogoActionItemHdlr(){
        return canExecuteBlocking('NewLogoActionItemHdlr');
    }
    public static Boolean newLogoActionItemHdlr1(){
        return canExecuteBlocking('NewLogoActionItemHdlr1');
    }
         public static Boolean newLogoActionItemHdlr2(){
        return canExecuteBlocking('NewLogoActionItemHdlr2');
    }
    public static Boolean actionItemBusinessClass(){
        return canExecuteBlocking('ActionItemBusinessClass');
    }
    public static Boolean pricingApprovalExtCr1Class(){
        return canExecuteBlocking('PricingApprovalExtCr1');
    }
    public static Boolean pricingApprovalExtCr1grand(){
        return canExecuteBlocking('Grand');
    }
    public static Boolean pricingApprovalExtCr(){
        return canExecuteBlocking('PricingApprovalExtCr');
    }
    public static Boolean pricingApprovalclass(){
        return canExecuteBlocking('PricingApproval');
    }
    public static Boolean attributeAfterUpdate(){
        return canExecuteBlocking('Attsoql');
    }
    public static Boolean createJeopardyCase(){
        return canExecuteBlocking('createJeopardyCase');
    }
    public static Boolean updateParentPathInventoryInstanceIDCheckRecursive(Set<Id> alreadyExecutedIds){
        return canExecuteUpdate(alreadyExecutedIds);
    }     
}