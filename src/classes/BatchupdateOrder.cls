public class BatchupdateOrder implements Database.Batchable<csord__order__c>, Database.AllowsCallouts
{  
 
   public Id solID;
  
   Public BatchupdateOrder(ID SolutionId){
    solID = SolutionId;    
   }
  
    public Iterable<csord__Order__c> start(Database.BatchableContext BC)
    {
                System.debug('BatchupdateOrder1 :');

        return [Select id, Is_InFlight_Update__c, Status__c, Is_Order_Submitted__c  from csord__Order__c where Solutions__c =: solID];
    }
 
    public void execute(Database.BatchableContext info, List<csord__Order__c> toupdateOrders)
    {
          List<csord__Order__c> UpOrders = new List<csord__Order__c>();
          List<csord__Order__c> UpinflightOrders = new List<csord__Order__c>();
         
        for(csord__Order__c Ord : toupdateOrders){
       
           If(Ord.Is_Order_Submitted__c == True && Ord.Status__c == 'Submitted' && Ord.Is_InFlight_Update__c == false){
               Ord.Is_InFlight_Update__c = true;
               UpinflightOrders .add(Ord);
                }
           else If(Ord.Is_Order_Submitted__c == True && Ord.Status__c == 'Submitted' && Ord.Is_InFlight_Update__c == true){
               Ord.Is_InFlight_Update__c = false;
               UpOrders.add(Ord);
            } 
                    
        }
      If(UpinflightOrders.size()>0){Update UpinflightOrders ;}  
      If(UpOrders.size()>0){Update UpOrders ;}
       
    }
 
    public void finish(Database.BatchableContext info) { }
}