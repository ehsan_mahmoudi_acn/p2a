public class PriceItemQueryIM extends cscfga.ALookupSearch {
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){
        // NOT IMPLEMENTED
        String bandwidhtID = searchFields.get('Bandwidth');
        String accountID = searchFields.get('Account Id');
        String rateCardID = searchFields.get('RateCardIDTemp');
        System.Debug('doDynamicLookupSearch' + searchFields);
        List <cspmb__Price_Item__c> data = [SELECT Id, cspmb__One_Off_Cost__c,
                      cspmb__Recurring_Cost__c,
                      cspmb__One_Off_Charge__c,
                      cspmb__Recurring_Charge__c,
                      cspmb__Account__c
                      FROM cspmb__Price_Item__c 
                          WHERE IPL_Rate_Card_Bandwidth_Join__r.Bandwidth__c=:bandwidhtID 
                          AND IPL_Rate_Card_Bandwidth_Join__r.IPL_Rate_Card_Item__c=:rateCardID
                      AND cspmb__Account__c = :accountID];

    if (data.isEmpty()){
      data = [SELECT Id, cspmb__One_Off_Cost__c,
          cspmb__Recurring_Cost__c,
          cspmb__One_Off_Charge__c,
          cspmb__Recurring_Charge__c,
          cspmb__Account__c
          FROM cspmb__Price_Item__c 
              WHERE IPL_Rate_Card_Bandwidth_Join__r.Bandwidth__c=:bandwidhtID 
              AND IPL_Rate_Card_Bandwidth_Join__r.IPL_Rate_Card_Item__c=:rateCardID
              AND cspmb__Account__c = null];
    }
         System.Debug('PriceItemQuery : '+ data);
        return data;
    }
    public override String getRequiredAttributes(){ 
        return '[ "Account Id","RateCardIDTemp","Bandwidth"]';
    }

}