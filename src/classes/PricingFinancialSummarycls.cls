Public with sharing class PricingFinancialSummarycls
{
    public case caseObj;
    private list<case> casequery;
    public Boolean isVisible;
    public Decimal revenueGrandTotal {get;set;}
    public Decimal opexGrandTotal {get;set;}
    public Decimal cogsrandTotal {get;set;}
    public Decimal CapexrandTotal {get;set;}
    public Decimal grossmarginGrandTotal {get;set;}
    public Decimal grossmarginPerGrandTotal {get;set;}
    public Decimal onneroffnetgrandTotal {get;set;}    
    public List<RevenueWrapper> revLst {get;set;}
    
    Public boolean getisVisible()
    {
        ID pID = UserInfo.getProfileID();
        Boolean checkVisibility = false;
         String nameP = system.label.Profile_Access_For_Pricing_Financial_Summary;
        String[] arrTest = nameP.split(',');
        //list<profile> pList = [Select id from profile where name = :arrTest];
        //list<profile> pList = [Select id from profile where name in ('TI Commercial','System Administrator','TI Billing')];
        for (profile p : [Select id from profile where name = :arrTest]) 
        {
            if (p.id == pID)
            checkVisibility = true;
        }
        return checkVisibility;
    }
    
    public PricingFinancialSummarycls(ApexPages.StandardController controller) 
    {
        revLst  = new List<RevenueWrapper>();
        this.caseObj = (case)controller.getrecord();
        list<case> caseUpdate = new list<case>();
        casequery = [select id, Product_Basket__c, Business_Opex__c, Business_Capex__c, Product_Basket__r.Total_Margin__c, Product_Basket__r.Onnet_vs_Offnet__c, Product_Basket__r.Gross_Margin__c, Product_Basket__r.Max_Contract_Term__c, Product_Basket__r.Capex__c from case where id=: caseobj.id];
        revenueGrandTotal  = 0;
        opexGrandTotal = 0;
        cogsrandTotal = 0;
        CapexrandTotal = 0;
        grossmarginGrandTotal = 0;
        grossmarginPerGrandTotal = 0;
        onneroffnetgrandTotal = 0;
        initlize();
    } 
    
    public class RevenueWrapper 
    {
        public Decimal revenue {get;set;}
        public Decimal opex {get;set;}
        public Decimal cogs {get;set;}
        public Decimal capex {get;set;}
        public Decimal grossmargin {get;set;}
        public Decimal grossmarginPer {get;set;}
        public Decimal onnetofnet {get;set;}
        public String years {get;set;}
    }
    
    public void initlize()
    {   
        decimal maxContractTerm = 1, Cterm =1, pcrevenue=0.0, pcopex=0.0, pcapex=0.0, pccogs=0.0, grossmarginRound, grossmarginPreRound, remainder = 0, OnnetRev = 0, TotalOnnetRev = 0;
        
        List<Pricing_Approval_Request_Data__c> PCList= [Select Id,remaining_contract_term__c,Product_Configuration__r.Change_in_Contract__c,Product_Configuration__r.cscfga__Product_Definition__r.cscfga__Product_Category__r.name, Product_Configuration__r.Onnet_OffnetColo__c, Act_Approved_RC__c, Act_Approved_NRC__c, Product_Configuration__r.Product_Id__c, Approved_NRC__c, Approved_RC__c, Offer_NRC__c, Offer_RC__c, Cost_RC__c, Cost_NRC__c, Contract_Term__c, Is_offnet__c, Product_Configuration__r.cscfga__Parent_Configuration__r.Product_Id__c from Pricing_Approval_Request_Data__c where Product_Basket__c=: casequery[0].Product_Basket__c and Product_Configuration__c != null ];
        system.debug('Pc List=>'+PCList);
        for(Pricing_Approval_Request_Data__c iterator : PCList) 
        {    Decimal contractTerm=getContractTerm(iterator.Contract_Term__c,iterator.remaining_contract_term__c,iterator.Product_Configuration__r.Change_in_Contract__c);
            if(contractTerm> maxContractTerm)
            { 
            	
                maxContractTerm = contractTerm;
                remainder = Math.mod(maxContractTerm.intValue(), 12);
            }
        }
        if(maxContractTerm>0 && maxContractTerm<=12 )
        {
            Cterm=1;
        }
        if(maxContractTerm>0 && maxContractTerm>12)
        {
            Cterm = (maxContractTerm)/12;
        }
        
        if(remainder != 0 && maxContractTerm > 12)
        {
            Cterm+=1;
        }
        
        system.debug('***********'+Cterm);
        integer i=1;
        for(i=1; i <= Cterm; i++)
        {
            RevenueWrapper  s = new RevenueWrapper ();
            if(Cterm==1)
            {
                for(Pricing_Approval_Request_Data__c pc: PCList)
                {
                    Decimal offerNRC = pc.Act_Approved_NRC__c; //pc.Approved_NRC__c == null || pc.Approved_NRC__c == 0 ? pc.Offer_NRC__C : pc.Approved_NRC__c;
                    Decimal offerRC = pc.Act_Approved_RC__c; //pc.Approved_RC__c == null || pc.Approved_RC__c == 0 ? pc.Offer_RC__C : pc.Approved_RC__c;
                    Decimal contractTerm=getContractTerm(pc.Contract_Term__c,pc.remaining_contract_term__c,pc.Product_Configuration__r.Change_in_Contract__c);
                    pcrevenue=pcrevenue + (offerRC*(contractTerm < 12 ? contractTerm : 12)) + offerNRC;
                    pcapex+=pc.Cost_NRC__c==null?0:pc.Cost_NRC__c;
                    
                    if(pc.Product_Configuration__r.Product_Id__c != null)
                    {
                        if(Label.Pricing_Financial_off_net.contains(' '+pc.Product_Configuration__r.Product_Id__c+',') && pc.Product_Configuration__r.Product_Id__c != 'VLV')
                        {system.debug('Rc=>'+pc.Cost_RC__c+' NRC=>'+pc.Cost_NRC__c);
                            pccogs = pccogs + ((pc.Cost_RC__c == null ? 0 : pc.Cost_RC__c)*(contractTerm < 12 ? contractTerm : 12));
                            pccogs = pccogs.setscale(2);
                        }
                        else if((pc.Product_Configuration__r.Product_Id__c == 'VLV') && (pc.Product_Configuration__r.cscfga__Parent_Configuration__r.Product_Id__c == 'BackUp-VLP-Offnet' || pc.Product_Configuration__r.cscfga__Parent_Configuration__r.Product_Id__c == 'VLP-Offnet'))
                        {  
                            pccogs=pccogs + ((pc.Cost_RC__c == null ? 0 : pc.Cost_RC__c)*(contractTerm < 12 ? contractTerm : 12));
                            pccogs=pccogs.setscale(2);
                        }
                      else if(pc.Product_Configuration__r.cscfga__Product_Definition__r.cscfga__Product_Category__r.name == 'Colocation')
                       {
                        
                       if(Label.Colo_Off_Net_Datacenters.contains('#'+pc.Product_Configuration__r.Onnet_OffnetColo__c+'#'))
                       {
                       pccogs=pccogs + ((pc.Cost_RC__c == null ? 0 : pc.Cost_RC__c)*(contractTerm < 12 ? contractTerm : 12));
                       pccogs=pccogs.setscale(2);
                          }
                      
                      else {
                       pcopex = pcopex + ((pc.Cost_RC__c == null ? 0 : pc.Cost_RC__c)*(contractTerm < 12 ? contractTerm : 12));
                       pcopex = pcopex.setscale(2);
                       OnnetRev+=(((pc.Approved_RC__c == null ? 0 : pc.Approved_RC__c)*(contractTerm < 12 ? contractTerm : 12)) + (pc.Approved_NRC__c == null ? 0 : pc.Approved_NRC__c));
                       
                           }
                         }
                       
                        else if(pc.Product_Configuration__r.cscfga__Product_Definition__r.cscfga__Product_Category__r.name != 'Colocation') 
                        {
                            pcopex = pcopex + ((pc.Cost_RC__c == null ? 0 : pc.Cost_RC__c)*(contractTerm < 12 ? contractTerm : 12));
                            pcopex = pcopex.setscale(2);
                            OnnetRev+=(((pc.Approved_RC__c == null ? 0 : pc.Approved_RC__c)*(contractTerm < 12 ? contractTerm : 12)) + (pc.Approved_NRC__c == null ? 0 : pc.Approved_NRC__c));
                        }
                    }
                }
                
                if(casequery[0].Business_Capex__c!=null)
                {
                    s.capex = (casequery[0].Business_Capex__c + pcapex).setscale(2);
                    CapexrandTotal= s.capex;
                }
                else
                {
                    s.capex = (pcapex).setscale(2);
                    CapexrandTotal= s.capex;
                }
                
                s.revenue = pcrevenue.setscale(2);
                pcopex+=(casequery[0].Business_Opex__c == null ? 0 : casequery[0].Business_Opex__c)*maxContractTerm;
                s.opex= pcopex.setscale(2);
                s.cogs= pccogs.setscale(2);
                grossmarginRound = pcrevenue-(pcopex + pccogs + CapexrandTotal);
                s.grossmargin= grossmarginRound.setScale(2);
                if(pcrevenue!=0.0 && pcrevenue !=null)
                {
                    grossmarginPreRound = ((100* (pcrevenue - (pcopex + pccogs + CapexrandTotal)))/pcrevenue).setscale(2);
                }
                else
                {
                    grossmarginPreRound=0.0;
                }
                s.grossmarginPer= grossmarginPreRound.setScale(2);
            }
            if(Cterm>1 && i==1)
            {
                for(Pricing_Approval_Request_Data__c pc0: PCList)
                {
                    Decimal offerNRC = pc0.Act_Approved_NRC__c; //pc0.Approved_NRC__c == null || pc0.Approved_NRC__c == 0 ? pc0.Offer_NRC__C : pc0.Approved_NRC__c;
                    Decimal offerRC = pc0.Act_Approved_RC__c; //pc0.Approved_RC__c == null || pc0.Approved_RC__c == 0 ? pc0.Offer_RC__C : pc0.Approved_RC__c;
                    Decimal contractTerm=getContractTerm(pc0.Contract_Term__c,pc0.remaining_contract_term__c,pc0.Product_Configuration__r.Change_in_Contract__c);
                    pcrevenue=pcrevenue + (offerRC*(contractTerm < 12 ? contractTerm : 12)) + offerNRC;
                    pcapex+=pc0.Cost_NRC__c==null?0:pc0.Cost_NRC__c;
                    
                    if(pc0.Product_Configuration__r.Product_Id__c != null)
                    {
                        if(Label.Pricing_Financial_off_net.contains(' '+pc0.Product_Configuration__r.Product_Id__c+',') && pc0.Product_Configuration__r.Product_Id__c != 'VLV')
                        {system.debug('Rc=>'+pc0.Cost_RC__c+' NRC=>'+pc0.Cost_NRC__c);
                            pccogs = pccogs + ((pc0.Cost_RC__c == null ? 0 : pc0.Cost_RC__c)*(contractTerm < 12 ? contractTerm : 12));
                            pccogs = pccogs.setscale(2);
                        }
                        else if((pc0.Product_Configuration__r.Product_Id__c == 'VLV') && (pc0.Product_Configuration__r.cscfga__Parent_Configuration__r.Product_Id__c == 'BackUp-VLP-Offnet' || pc0.Product_Configuration__r.cscfga__Parent_Configuration__r.Product_Id__c == 'VLP-Offnet'))
                        {
                            pccogs=pccogs + ((pc0.Cost_RC__c == null ? 0 : pc0.Cost_RC__c)*(contractTerm < 12 ? contractTerm : 12));
                            pccogs=pccogs.setscale(2);
                        }
                      else if(pc0.Product_Configuration__r.cscfga__Product_Definition__r.cscfga__Product_Category__r.name == 'Colocation')
                       {
                       
                      
                       if(Label.Colo_Off_Net_Datacenters.contains('#'+pc0.Product_Configuration__r.Onnet_OffnetColo__c+'#')){
                       pccogs=pccogs + ((pc0.Cost_RC__c == null ? 0 : pc0.Cost_RC__c)*(contractTerm < 12 ? contractTerm : 12));
                            pccogs=pccogs.setscale(2);
                          }
                      
                      else {
                       pcopex = pcopex + ((pc0.Cost_RC__c == null ? 0 : pc0.Cost_RC__c)*(contractTerm < 12 ? contractTerm : 12));
                       pcopex = pcopex.setscale(2);
                       OnnetRev+=(((pc0.Approved_RC__c == null ? 0 : pc0.Approved_RC__c)*(contractTerm < 12 ? contractTerm : 12)) + (pc0.Approved_NRC__c == null ? 0 : pc0.Approved_NRC__c));
                       
                           }
                          }
                        
                        else if(pc0.Product_Configuration__r.cscfga__Product_Definition__r.cscfga__Product_Category__r.name != 'Colocation') 
                        {
                            pcopex = pcopex + ((pc0.Cost_RC__c == null ? 0 : pc0.Cost_RC__c)*(contractTerm < 12 ? contractTerm : 12));
                            pcopex = pcopex.setscale(2);
                            OnnetRev+=(((pc0.Approved_RC__c == null ? 0 : pc0.Approved_RC__c)*(contractTerm < 12 ? contractTerm : 12)) + (pc0.Approved_NRC__c == null ? 0 : pc0.Approved_NRC__c));
                        }
                    }
                }
                if(casequery[0].Business_Capex__c != null)
                {
                    s.capex = (casequery[0].Business_Capex__c + pcapex).setscale(2);
                    CapexrandTotal= s.capex.setscale(2);
                }
                else
                {
                    s.capex = (pcapex).setscale(2);
                    CapexrandTotal= s.capex;
                }
                s.revenue = pcrevenue.setscale(2);
                pcopex+=(casequery[0].Business_Opex__c == null ? 0 : casequery[0].Business_Opex__c)*12;
                s.opex= pcopex.setscale(2);
                s.cogs= pccogs.setscale(2);
                grossmarginRound = pcrevenue - (pcopex + pccogs + CapexrandTotal);
                s.grossmargin= grossmarginRound.setScale(2);
                if(pcrevenue!=0.0 && pcrevenue !=null)
                {
                    grossmarginPreRound = ((100* (pcrevenue - (pcopex + pccogs + CapexrandTotal)))/pcrevenue).setscale(2);
                }
                else
                {
                    grossmarginPreRound=0.0;
                }
                s.grossmarginPer= grossmarginPreRound.setScale(2);
            }       
            if(Cterm>1 && i>1)
            {
                for(Pricing_Approval_Request_Data__c pc1: PCList)
                {
                    Decimal offerNRC = pc1.Act_Approved_NRC__c; //pc1.Approved_NRC__c == null || pc1.Approved_NRC__c == 0 ? pc1.Offer_NRC__C : pc1.Approved_NRC__c;
                    Decimal offerRC = pc1.Act_Approved_RC__c; //pc1.Approved_RC__c == null || pc1.Approved_RC__c == 0 ? pc1.Offer_RC__C : pc1.Approved_RC__c;
                    Decimal contractTerm=getContractTerm(pc1.Contract_Term__c,pc1.remaining_contract_term__c,pc1.Product_Configuration__r.Change_in_Contract__c);
                    if((contractTerm/12)>=i)
                    {
                        pcrevenue=pcrevenue + (offerRC*12);
                        
                        if(pc1.Product_Configuration__r.Product_Id__c != null)
                        {
                            if(Label.Pricing_Financial_off_net.contains(' '+pc1.Product_Configuration__r.Product_Id__c+',') && pc1.Product_Configuration__r.Product_Id__c != 'VLV')
                            {
                                pccogs=pccogs + ((pc1.Cost_RC__c == null ? 0 : pc1.Cost_RC__c)*12);
                                pccogs=pccogs.setscale(2);
                            }
                            else if((pc1.Product_Configuration__r.Product_Id__c == 'VLV') && (pc1.Product_Configuration__r.cscfga__Parent_Configuration__r.Product_Id__c == 'BackUp-VLP-Offnet' || pc1.Product_Configuration__r.cscfga__Parent_Configuration__r.Product_Id__c == 'VLP-Offnet'))
                            {
                                pccogs=pccogs + ((pc1.Cost_RC__c == null ? 0 : pc1.Cost_RC__c)*12);
                                pccogs=pccogs.setscale(2);
                            }
                        else if(pc1.Product_Configuration__r.cscfga__Product_Definition__r.cscfga__Product_Category__r.name == 'Colocation')
                       {
                       
                       if(Label.Colo_Off_Net_Datacenters.contains('#'+pc1.Product_Configuration__r.Onnet_OffnetColo__c+'#')){
                       pccogs=pccogs + ((pc1.Cost_RC__c == null ? 0 : pc1.Cost_RC__c)*12);
                       pccogs=pccogs.setscale(2);
                          }
                      
                      else {
                       pcopex = pcopex + ((pc1.Cost_RC__c == null ? 0 : pc1.Cost_RC__c)*12);
                       pcopex = pcopex.setscale(2);
                       OnnetRev+=((pc1.Approved_RC__c == null ? 0 : pc1.Approved_RC__c)*12);
                       
                           }
                         }
                            else if(pc1.Product_Configuration__r.cscfga__Product_Definition__r.cscfga__Product_Category__r.name != 'Colocation') 
                            {
                                pcopex = pcopex + ((pc1.Cost_RC__c == null ? 0 : pc1.Cost_RC__c)*12);
                                pcopex = pcopex.setscale(2);
                                OnnetRev+=((pc1.Approved_RC__c == null ? 0 : pc1.Approved_RC__c)*12);
                            }
                        }
                    }
                   
                    else if ((contractTerm/12)<i && math.mod(contractTerm.intValue(),12)>0 && contractTerm >(12*(i-1)))
                    {
                        pcrevenue=pcrevenue + (offerRC*(math.mod(contractTerm.intValue(),12))) ;
                        if(pc1.Product_Configuration__r.Product_Id__c != null)
                        {
                            if(Label.Pricing_Financial_off_net.contains(' '+pc1.Product_Configuration__r.Product_Id__c+',') && pc1.Product_Configuration__r.Product_Id__c != 'VLV')
                            {
                                pccogs=pccogs + ((pc1.Cost_RC__c == null ? 0 : pc1.Cost_RC__c)*(math.mod(contractTerm.intValue(),12)));
                                pccogs=pccogs.setscale(2);
                            }
                            else if((pc1.Product_Configuration__r.Product_Id__c == 'VLV') && (pc1.Product_Configuration__r.cscfga__Parent_Configuration__r.Product_Id__c == 'BackUp-VLP-Offnet' || pc1.Product_Configuration__r.cscfga__Parent_Configuration__r.Product_Id__c == 'VLP-Offnet'))
                            {
                                pccogs=pccogs + ((pc1.Cost_RC__c == null ? 0 : pc1.Cost_RC__c)*(math.mod(contractTerm.intValue(),12)));
                                pccogs=pccogs.setscale(2);
                            }
                       else if(pc1.Product_Configuration__r.cscfga__Product_Definition__r.cscfga__Product_Category__r.name == 'Colocation')
                       {
                       
                       if(Label.Colo_Off_Net_Datacenters.contains('#'+pc1.Product_Configuration__r.Onnet_OffnetColo__c+'#')){
                       pccogs=pccogs + ((pc1.Cost_RC__c == null ? 0 : pc1.Cost_RC__c)*(math.mod(contractTerm.intValue(),12)));
                       pccogs=pccogs.setscale(2);
                          }
                      
                      else {
                       pcopex=pcopex +  ((pc1.Cost_RC__c == null ? 0 : pc1.Cost_RC__c)*(math.mod(contractTerm.intValue(),12)));
                       pcopex = pcopex.setscale(2);
                       OnnetRev+=((pc1.Approved_RC__c == null ? 0 : pc1.Approved_RC__c)*(math.mod(contractTerm.intValue(),12)));
                       
                           }
                         }
                       
                      else if(pc1.Product_Configuration__r.cscfga__Product_Definition__r.cscfga__Product_Category__r.name != 'Colocation') 
                            {
                                pcopex=pcopex +  ((pc1.Cost_RC__c == null ? 0 : pc1.Cost_RC__c)*(math.mod(contractTerm.intValue(),12)));
                                pcopex = pcopex.setscale(2);
                                OnnetRev+=((pc1.Approved_RC__c == null ? 0 : pc1.Approved_RC__c)*(math.mod(contractTerm.intValue(),12)));
                            }
                            }
                    }
                    else
                    {
                        pcrevenue=pcrevenue +0.0;
                        pccogs=pccogs + 0.0;
                        pcopex=pcopex + 0.0;
                    }
                }
                s.capex = 0.0;
                s.revenue = pcrevenue.setscale(2);
                if(i == Cterm.intValue())
                {
                    pcopex+=(casequery[0].Business_Opex__c == null ? 0 : casequery[0].Business_Opex__c)* (remainder == 0 ? 12 : remainder);
                }
                else
                {
                    pcopex+=(casequery[0].Business_Opex__c == null ? 0 : casequery[0].Business_Opex__c)*12;
                }
                s.opex= pcopex.setscale(2);
                s.cogs= pccogs.setscale(2);
                grossmarginRound = (pcrevenue - (pcopex + pccogs)).setscale(2);
                s.grossmargin= grossmarginRound.setScale(2);
                if(pcrevenue!=0.0 && pcrevenue !=null)
                {
                    grossmarginPreRound = ((100* (pcrevenue - (pcopex + pccogs)))/pcrevenue).setscale(2);
                }
                else
                {
                    grossmarginPreRound=0.0;
                }
                s.grossmarginPer= grossmarginPreRound.setScale(2);
                
            }
            TotalOnnetRev+=OnnetRev;
            s.onnetofnet = 0;
            if(OnnetRev != 0 && pcrevenue != 0){
                s.onnetofnet = ((OnnetRev/pcrevenue)*100).setscale(2);  
            }
            
            s.years = 'Year '+ i; 
            revenueGrandTotal+= s.revenue;
            opexGrandTotal+= s.opex;
            cogsrandTotal+= s.cogs;
            grossmarginGrandTotal += s.grossmargin;
            grossmarginPerGrandTotal += s.grossmarginPer;
            if(TotalOnnetRev != 0 && revenueGrandTotal != 0){
                onneroffnetgrandTotal= ((TotalOnnetRev/revenueGrandTotal)*100).setscale(2);
            }
            pcrevenue=0.00;
            pcopex=0.00;
            pccogs=0.00;
            pcapex= 0.00;
            OnnetRev = 0.00;
            revLst.add(s);
        }
        if(grossmarginGrandTotal != 0 && revenueGrandTotal != 0){
            grossmarginPerGrandTotal = (grossmarginGrandTotal/revenueGrandTotal*100).setScale(2);
        }
    }
    @testvisible
    private decimal getContractTerm(decimal contractTerm,decimal remainingContractTerm,String changeInContarct){
    	
    	return changeInContarct!=null && changeInContarct.equalsignorecase('No')?remainingContractTerm:contractTerm;
    	
    	
    }
}