@isTest(SeeAllData=false)
public class CustomAccountMultipleSubCtrlTest{    
    
    private static List<Account> accList;
    private static List<Opportunity> oppList;
    private static list<cscfga__Product_Category__c> prodCategoryLists;
    private static cscfga__Product_Category__c prodCategoryList; 
    private static List<csbb__Product_Configuration_Request__c> prodConfReq;
    private static List<cscfga__product_basket__c> prodBaskList;
    private static List<csord__Order_Request__c> requestList;
    private static List<csord__Order__c> orderList;
    private static List<cscfga__Product_Definition__c> ProductDeflist;
    private static List<cscfga__Product_Bundle__c> Pbundle;
    private static List<cscfga__Configuration_Offer__c> Offerlists;
    private static List<cscfga__Product_Configuration__c> proconfigs;
    private static List<csord__Subscription__c> subscriptionList;
    private static List<csord__Service__c> serviceList;
    private static List<csord__Service_Line_Item__c> serLineItemList;
    private static List<cscfga__Product_Basket__c> basketList;
    private static list<String> listChoice;
    private static List<csord__Subscription__c> listSubscriptionNumber;
    
    Private static void intiTestData(){
    
    P2A_TestFactoryCls.SampleTestData();
    accList = P2A_TestFactoryCls.getAccounts(1);
    oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
    prodCategoryLists=  P2A_TestFactoryCls.getProductCategory(1); 
    prodConfReq = P2A_TestFactoryCls.getProductonfigreq(1,prodCategoryLists);
    prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
    requestList = P2A_TestFactoryCls.getorderrequest(1);
    orderList = P2A_TestFactoryCls.getorder(1,requestList);
    ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
    Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
    Offerlists = P2A_TestFactoryCls.getOffers(1);   
    proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
    subscriptionList = P2A_TestFactoryCls.getSubscription(1, requestList);
    serviceList = P2A_TestFactoryCls.getService(1, requestList, subscriptionList);
    serLineItemList = P2A_TestFactoryCls.getSerLineItem(1, serviceList,requestList);
    basketList = P2A_TestFactoryCls.getProductBasket(1); 
    system.assertEquals(true,serviceList!=null); 
    }
    
    /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        } else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
        /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableTelcoAll(Id userId) {
        csordtelcoa__Orders_Subscriptions_Options__c telcoOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance(userId);

        if(telcoOptions == null) {
            telcoOptions = new csordtelcoa__Orders_Subscriptions_Options__c ();
            telcoOptions.csordtelcoa__Disable_Triggers__c = true;
            telcoOptions.SetupOwnerId = userId;
        } else {
            telcoOptions.csordtelcoa__Disable_Triggers__c = true;
        }

        upsert telcoOptions;
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        } else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }

    private static void enableTelcoAll(Id userId) {
        csordtelcoa__Orders_Subscriptions_Options__c telcoOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance(userId);
        
        if(telcoOptions == null) {
            telcoOptions = new csordtelcoa__Orders_Subscriptions_Options__c ();
            telcoOptions.csordtelcoa__Disable_Triggers__c = false;
            telcoOptions.SetupOwnerId = userId;
        } else {
            telcoOptions.csordtelcoa__Disable_Triggers__c = false;
        }

        upsert telcoOptions;
    }    
    
    
   /* private static testMethod void CustomAccountMultipleSubCtrlTest_method() {
        //id useId = UserInfo.getUserId();
        //disableAll(useId);
        //disableTelcoAll(useId);       
        //CustomAccountMultipleSubscriptionsCtrl(new ApexPages.StandardController(accList[0]));
        //enableAll(useId);
        //enableTelcoAll(useId);
    }*/
    @istest static void customAccountMultipleSubCtrlTestmethod1() {
    string a,b,c;
        Test.startTest();
        intiTestData();
        id useId = UserInfo.getUserId();
        disableAll(useId);
        disableTelcoAll(useId);
        CustomAccountMultipleSubscriptionsCtrl ctrl = new CustomAccountMultipleSubscriptionsCtrl(new ApexPages.StandardController(accList[0]));
        ctrl.createProcesses();
        ctrl.getOppRecordTypes();        
        ctrl.getNextButton();
        ctrl.getPreviousButton();
        ctrl.previousPage();
        ctrl.getPreviousChoices();
        //ctrl.nextPage();
        ctrl.rerenderButtons();
        ctrl.getInvokeProcess();
        ctrl.getRequestedDateLabel();
        ctrl.getRequestDate();
        ctrl.getChangeTypes();
        ctrl.getList();
        ctrl.nextPage();
        
        ctrl.processName = 'Termination';
        ctrl.dateString = system.today().format();
        
        
        ctrl.addMessageToPage();
        ctrl.previous();
        ctrl.next();
        system.assertEquals(true,ctrl!=null); 
        List<String> queues = CustomAccountMultipleSubscriptionsCtrl.getAvailableRecordTypeNamesForSObject(Case.SObjectType);
        List<String> queuess = CustomAccountMultipleSubscriptionsCtrl.getDefaultRecordTypeNameForSObject(Case.SObjectType);     
        csordtelcoa__Change_Types__c emp = new csordtelcoa__Change_Types__c(Name = 'Test');
        CustomAccountMultipleSubscriptionsCtrl.Change_Types_Wrapper empW = new CustomAccountMultipleSubscriptionsCtrl.Change_Types_Wrapper(emp);  //Covering inner/wrapper class
        empW.compareTo(empW);  
        ctrl.createProcesses();        
        ctrl.dateString = system.today().format();
        ctrl.changeType = 'Renewal';        
       // ctrl.CreateMultipleSubscriptionMacOpportunity();        
        //ctrl.CreateMultipleSubscriptionMacBasket();        
        CustomAccountMultipleSubscriptionsCtrl.getStatusesNotAllowingChange();
        CustomAccountMultipleSubscriptionsCtrl.getOpportunityRecordTypes();        
        ctrl.getOppRecordTypes();
        ctrl.rerenderButtons();  
        List<String> queues1 = CustomAccountMultipleSubscriptionsCtrl.getDefaultRecordTypeNameForSObject(Case.SObjectType);        
        csordtelcoa__Change_Types__c emp1 = new csordtelcoa__Change_Types__c(Name = 'Test');
        CustomAccountMultipleSubscriptionsCtrl.Change_Types_Wrapper empW1 = new CustomAccountMultipleSubscriptionsCtrl.Change_Types_Wrapper(emp1);  //Covering inner/wrapper class
        empW1.compareTo(empW1); 
        system.assertEquals(true,empW1!=null); 
        ctrl.processName = 'Termination';
        ctrl.dateString = system.today().format();
        //Date aDate = date.parse(ctr.dateString);
        ctrl.createProcesses();
        enableAll(useId);
        enableTelcoAll(useId);      
        Test.stopTest();
    }
    
     @istest static  void doLookupSearchTest() {
        Exception ee = null;
        integer userid = 0;
        try{
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assertEquals(true,userid!=null); 
            //CustomAccountMultipleSubCtrlTest_method1();
            
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='CustomAccountMultipleSubCtrlTest :doLookupSearchTest';         
            ErrorHandlerException.sendException(e); 
            ee = e;
        } finally {
           
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
        
    }
    //additional test method
    @istest static void testnegativescenarios()
    {
        test.startTest();
        try{
         List<Country_Lookup__c> cLookup=P2A_TestFactoryCls.getcountry(1);
         List<csord__Order_Request__c> OrdReqList=P2A_TestFactoryCls.getorderrequest(1);
            Account a = new Account();
            a.name                          = 'Test Accenture ';
            a.BillingCountry                = 'GB';
            a.Activated__c                  = True;
            a.Account_ID__c                 = 'test';
            a.Account_Status__c             = 'Active';
            a.Customer_Legal_Entity_Name__c = 'Test';
            a.Customer_Type_New__c = 'MNC' ;
            a.Region__c = 'US';
            a.Website='abc.com';
            a.Selling_Entity__c='Telstra Limited';          
            a.Country__c=cLookup[0].Id;
            a.Industry__c='BPO';
            a.Account_ORIG_ID_DM__c = 'test';
            insert a;
            system.assertEquals(true,a!=null); 
            csord__Subscription__c sub = new csord__Subscription__c();
            sub.Name ='Test'; 
            sub.csord__Identification__c = 'Test-Catlyne-4238362';
            sub.csord__Order_Request__c =OrdReqList[0].id;
            sub.csord__Account__c=a.id;
            sub.csordtelcoa__closed_Replaced__c=false;
            sub.Path_Status__c='abc';
            sub.OrderType__c = null;
            insert sub;
            system.assertEquals(true,sub!=null); 
            List<csord__Subscription__c>subList=new List<csord__Subscription__c>();
            subList.add(sub);
            List<csord__Subscription__c>subscriptionListList=new List<csord__Subscription__c>();
            subscriptionListList.add(sub);
            csordtelcoa__Change_Types__c ch=new csordtelcoa__Change_Types__c();
            ch.name='Termination';
            ch.csordtelcoa__Allow_Multiple_Subscriptions__c=true;
            ch.csordtelcoa__Sort_Order__c=11;
            ch.csordtelcoa__Invoke_Process__c=true;
            ch.csordtelcoa__Process_To_Invoke__c='Terminate Order Creation';
            insert ch;          
            system.assertEquals(true,ch!=null); 
            
            Test.setCurrentPage(page.CustomAccountMultipleSubscriptionsPage);              
            ApexPages.StandardController sc = new ApexPages.standardController(a);
            CustomAccountMultipleSubscriptionsCtrl controller = new CustomAccountMultipleSubscriptionsCtrl(sc);
            
            //System.assertEquals(true,controller.changeTypeMap!=null);
            controller.productBasketTitle='';
            try{controller.CreateMultipleSubscriptionMacBasket();
            }catch(exception e){
                ErrorHandlerException.ExecutingClassName='CustomAccountMultipleSubCtrlTest :testnegativescenarios';         
                ErrorHandlerException.sendException(e); 
                System.debug(e);
                }
            controller.productBasketTitle='abc';
            controller.Subnumber='2';
            controller.changeType='Termination';
            controller.pageNumber=0;
            controller.title='some value';
            controller.dateString='some value';
            controller.processName = 'Termination';
            controller.subscriptions=subList;
            system.assertEquals(true,controller!=null); 
            List<SelectOption>bcc=controller.getList();
            controller.createProcesses();
            controller.CreateMultipleSubscriptionMacOpportunity();
            
            controller.pageNumber=1;
            controller.title=null;
            controller.processName = 'Termination';
            controller.dateString=system.today().format();
            List<SelectOption>bcc1=controller.getList();            
            controller.createProcesses();
            controller.CreateMultipleSubscriptionMacOpportunity();
            controller.previousChoices.add('prev');         
            controller.nextPage();
            controller.previousPage();
            controller.rerenderButtons();
            boolean aa=controller.getInvokeProcess();
            string b=controller.getRequestedDateLabel();
            boolean bb=controller.getRequestDate();
            List<SelectOption> cc=controller.getChangeTypes();
            try{//controller.CreateMultipleSubscriptionMacBasket();
            }catch(Exception e){System.debug(e);}
        }catch(exception e){
            ErrorHandlerException.ExecutingClassName='CustomAccountMultipleSubCtrlTest :testnegativescenarios';         
            ErrorHandlerException.sendException(e); 
            system.debug(e);}
            test.stopTest();
    }
  /*  @istest static void testunit()
    {
     test.startTest();
     List<Country_Lookup__c> cLookup=P2A_TestFactoryCls.getcountry(1);
         List<csord__Order_Request__c> OrdReqList=P2A_TestFactoryCls.getorderrequest(1);
            Account a = new Account();
            a.name                          = 'Test Accenture ';
            a.BillingCountry                = 'GB';
            a.Activated__c                  = True;
            a.Account_ID__c                 = 'test';
            a.Account_Status__c             = 'Active';
            a.Customer_Legal_Entity_Name__c = 'Test';
            a.Customer_Type_New__c = 'MNC' ;
            a.Region__c = 'US';
            a.Website='abc.com';
            a.Selling_Entity__c='Telstra Limited';          
            a.Country__c=cLookup[0].Id;
            a.Industry__c='BPO';
            a.Account_ORIG_ID_DM__c = 'test';
            insert a;
     
           csord__Subscription__c sub = new csord__Subscription__c();
            sub.Name ='Test'; 
            sub.csord__Identification__c = 'Test-Catlyne-4238362';
            sub.csord__Order_Request__c =OrdReqList[0].id;
            sub.csord__Account__c=a.id;
            sub.csordtelcoa__closed_Replaced__c=false;
            sub.Path_Status__c='abc';
            sub.OrderType__c = null;
            sub.CreatedDate = system.today();
            sub.CurrencyIsoCode = 'USD';
            insert sub;
            List<csord__Subscription__c>subList=new List<csord__Subscription__c>();
            subList.add(sub);
            List<csord__Subscription__c>subscriptionListList=new List<csord__Subscription__c>();
            subscriptionListList.add(sub);
            string basketId= 'sub' ;
            string subscriptionIds2 = 'test1';
            String productBasketTitle = 'test2';
            
                // List<csord__Subscription__c> singlesub = [SELECT Id, Name, csord__Account__c, CreatedDate, CurrencyIsoCode FROM csord__Subscription__c WHERE Id = :subscription ];
        csordtelcoa__Change_Types__c changetype = new csordtelcoa__Change_Types__c ();
        changetype.name = 'test1';
        changetype.csordtelcoa__Add_To_Existing_Subscription__c = true;
        changetype.csordtelcoa__Auto_Create_Bundle__c = true;
        changetype.csordtelcoa__Invoke_Process__c=true;
        changetype.csordtelcoa__Allow_Multiple_Subscriptions__c= true;
        changetype.csordtelcoa__Process_To_Invoke__c = 'test';
        changetype.csordtelcoa__Request_Date__c=true;
        changetype.csordtelcoa__Requested_Date_Label__c=  'test';
        changetype.csordtelcoa__Sort_Order__c = 2;
        changetype.csordtelcoa__Subscription_Date_Field_API_Name__c = 'test';
        insert changetype ;
         //  csordtelcoa__Change_Types__c changeType1 = [SELECT Id, Name,csordtelcoa__Add_To_Existing_Subscription__c, csordtelcoa__Auto_Create_Bundle__c FROM csordtelcoa__Change_Types__c WHERE Name = :changeTypeName];
            
     Test.setCurrentPage(page.CustomAccountMultipleSubscriptionsPage);              
            ApexPages.StandardController sc = new ApexPages.standardController(a);
            //CustomAccountMultipleSubscriptionsCtrl controller = new CustomAccountMultipleSubscriptionsCtrl(sc);
           CustomAccountMultipleSubscriptionsCtrl.addSubscriptionsToBasket('sub','test1');
     test.stoptest();  
    }*/
}