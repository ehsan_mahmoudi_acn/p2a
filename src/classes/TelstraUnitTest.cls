/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TelstraUnitTest {

    static testMethod void testQuoteTrigger() {
    try{
        // TO DO: implement unit test
        Opportunity o = UnitTestHelper.getOpportunity();
        Quote__c q = UnitTestHelper.getQuote(o);
        
        // Check the Opportunity has picked up the Quote Id
        o = [SELECT Id, External_Id__c FROM Opportunity WHERE Id = :o.Id];
        System.assertEquals(q.Name, 'UnitTest001'); 
        }catch(Exception e){}
        
    }

    static testMethod void testAccountTrigger() {
        Account a = UnitTestHelper.getAccount();
        System.assertNotEquals(null, a);
        
        // Set to Customer type
        a.Type = 'Customer';
        update a;
        
    }

    static testMethod void testOpportunityTrigger() {
    try{
        // Get opportunity with a line item
        Opportunity o = UnitTestHelper.getOpportunity();

        // Force an update of the Opportunity to test the Bill Profile Complete
        o.BillProfileComplete__c = false;
        update o;

        System.assertNotEquals(null, o);
        }catch(Exception e){}
    }

    static testMethod void testCloneOpportunity() {
        Account a = UnitTestHelper.getAccount();
        
        Opportunity o = new Opportunity();
        o.AccountId = a.Id;
        o.Name = 'UnitTestOpp trg_bi_Opportunity';
        o.CloseDate = System.today();
        o.Amount = 25000;
        o.External_ID__c = 'UT15423542';
        o.Status__c = '1';
        o.StageName = 'Identify & Define';
        o.Stage__c= 'Identify & Define';
        insert o;
        List<Opportunity> op = [Select id,name from Opportunity where name = 'UnitTestOpp trg_bi_Opportunity'];
        system.assertequals(o.name,op[0].name);
        system.assert(o!=null);
        // Check the values have been cleared
        Opportunity o2 = [SELECT Id, External_ID__c, Status__c From Opportunity WHERE Id = :o.Id];
        //System.assertNotEquals('1', o2.Status__c);
        //System.assertNotEquals('UT15423542', o2.External_ID__c);
            
    }
    
}