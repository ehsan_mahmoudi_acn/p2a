public class SObjectFactory {
    
    private FieldValueFactory fieldValueFactory;
    
    public SObject create(DescribeSObjectResult type) {
        return this.create(type, null);
    }
    
    public SObject create(DescribeSObjectResult type, Map<String, Object> defaultValues) {
        SObject obj = type.getSObjectType().newSObject(null, true);
        Map<String, Schema.SObjectField> fieldMap = type.fields.getMap();
        
        for (String fieldName: fieldMap.keySet()) {
            Schema.SObjectField field = fieldMap.get(fieldName);
            Schema.DescribeFieldResult fieldDesc = field.getDescribe();
            if (fieldDesc.isAccessible() && fieldDesc.isCreateable()) {
                Object value = getValueForField(fieldDesc, defaultValues);
                if (value != null) {
                    // override the default value at create time only if the generated value is nor null
                    obj.put(field, getValueForField(fieldDesc, defaultValues));
                }
            }
        }
        
        return obj;
    }
    
    private Object getValueForField(Schema.DescribeFieldResult fieldDesc, Map<String, Object> defaultValues) {
        Object result = null; 
        
        if (defaultValues != null && defaultValues.containsKey(fieldDesc.getName())) {
            result = getGivenValue(fieldDesc, defaultValues);
        } else if (!fieldDesc.isNillable()) {
            result = generateValue(fieldDesc);
        } 
        
        return result;
    }
    
    private Object getGivenValue(Schema.DescribeFieldResult fieldDesc, Map<String, Object> defaultValues) {
        Object value = defaultValues.get(fieldDesc.getName());
        if (value instanceOf FieldValueGenerator) {
            return ((FieldValueGenerator) value).generate(fieldDesc); 
        }
        return value;      
    }
    
    private Object generateValue(Schema.DescribeFieldResult fieldDesc) {
        return getFieldValueFactory().generate(fieldDesc);
    }
    
    public void setFieldValueFactory(FieldValueFactory factory) {
        this.fieldValueFactory = factory;
    }
    
    private FieldValueFactory getFieldValueFactory() {
        if (this.fieldValueFactory == null) {
            this.fieldValueFactory = new FieldValueFactory();
        }
        return this.fieldValueFactory;
    }
}