@isTest(SeeAllData = false)
public class LeadDelTriggerTest 
{
    static testmethod void TestLeadDelTrigger1()
    {
         Test.startTest();
         
        Profile profObj = [select Id ,name from Profile  where name ='System Administrator'];
        
         Lead leadToDelete = new Lead(Company = 'Telstra', Country_Picklist__c = 'HONG KONG',
                               Status = 'Sales Accepted Lead (SAL)', Disqual_Reason__c= 'Bad fit for Telstra',
                               LeadSource= 'Advertising', Industry = 'BPO', Email='testleadDelTrgHdlr@abc.com',
                                    LastName='test' );
        insert leadToDelete; 
        //String uid=profObj.Id;
        //system.assertEquals(uid, profObj.Id);
        try
        {
            delete leadToDelete;
           
        } catch (Exception e) {
            System.assert(true, 'Deletion failed appropriately');
        }
        Test.stopTest();
    }
}