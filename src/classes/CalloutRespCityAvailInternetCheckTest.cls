@isTest(SeeAllData = false)
private class CalloutRespCityAvailInternetCheckTest{

    private static List<cscfga__Product_Category__c> productCategoryList;
    private static Map<String, csbb.CalloutResponse> mapCR;
    private static csbb.ProductCategory productCategory;
    private static csbb.CalloutProduct.ProductResponse productResponse;
    private static Map<String, Object> inputMap;
    private static Map<String, Object> resultMap;
    private static String categoryIndicator;
    private static csbb.Result result;
    
    private static Map<String, String> attMap;
    private static Map<String, String> responseFields;
    
    private static void initTestData(){
        
        productCategoryList = new List<cscfga__Product_Category__c>{
            new cscfga__Product_Category__c(Name = 'Product Category 1')
        };
        
        insert productCategoryList;
        system.assertEquals(true,productCategoryList!=null); 

        mapCR = new Map<String, csbb.CalloutResponse>();
        mapCR.put('CityAvailabilityCheck', new csbb.CalloutResponse());
        
        productCategory = new csbb.ProductCategory(productCategoryList[0].Id);
        
        productResponse = new csbb.CalloutProduct.ProductResponse();
        productResponse.fields = new Map<String, String>();
        
        inputMap = new Map<String, Object>();
        resultMap = new Map<String, Object>();
        
        //categoryIndicator = '';
        result = new csbb.Result();
        
        attMap = new Map<String, String>();
        responseFields = new Map<String, String>();
    }
    
    private static testMethod void test() {
    Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
             
            initTestData();
            categoryIndicator = 'Sample message from LocalLoopAvailabilityCheck';
            CalloutResponseCityAvailInternetCheck calloutResponseCityAvailInternetCheck = new CalloutResponseCityAvailInternetCheck(mapCR, productCategory, productResponse);
            CalloutResponseCityAvailInternetCheck calloutResponseCityAvailInternetCheck1 = new CalloutResponseCityAvailInternetCheck();

            //System.debug('***** CR Primary: ' + calloutResponseCityAvailabilityCheck.crPrimary);
            //System.debug('***** Product Response fields: ' + calloutResponseCityAvailabilityCheck.productResponse.fields);

            resultMap = calloutResponseCityAvailInternetCheck.processResponseRaw(inputMap);
            resultMap = calloutResponseCityAvailInternetCheck.getDynamicRequestParameters(inputMap);
            result = calloutResponseCityAvailInternetCheck.canOffer(attMap, responseFields, productResponse);
             system.assertEquals(true,calloutResponseCityAvailInternetCheck!=null); 
              system.assertEquals(true,result!=null); 
            if(categoryIndicator != null){
            Try{
            calloutResponseCityAvailInternetCheck.runBusinessRules(categoryIndicator); 
            }catch(exception e){
            ErrorHandlerException.ExecutingClassName='CalloutRespCityAvailInternetCheckTest :test';         
            ErrorHandlerException.sendException(e); 
}
            }
        } catch(Exception e){
        ErrorHandlerException.ExecutingClassName='CalloutRespCityAvailInternetCheckTest :test';         
        ErrorHandlerException.sendException(e); 

            ee = e;
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
    }


}