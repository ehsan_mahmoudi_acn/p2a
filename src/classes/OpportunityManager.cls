public with sharing class OpportunityManager {

    set<id> oppid = new set<id>();
    set<id> oppids = new set<id>();
    set<id> Prodconfigs = new set<id>();
    set<id> Prodconfigsipm = new set<id>();
    set<id> Prodconfiguration = new set<id>();
    set<id> Prodconfigurations = new set<id>();
    set<id> Prodconfigaddedports = new set<id>();
    set<id> Prodconfigaddedportss = new set<id>();

   public OpportunityManager() {
        
    }

    public Opportunity updateStage(Id opportunityId, String salesStatus, String currentStage, String nextStage){
        Opportunity opportunity = [select Id, Probability, Win_Loss_Reasons__c, StageName, Internet_Product__c, Sales_Status__c, UIFlag__c, Stage__c from Opportunity where Id = :opportunityId limit 1];
        OpportunityStage oppStage = [Select SortOrder, MasterLabel, DefaultProbability, IsActive From OpportunityStage where MasterLabel = :nextStage];

        if(salesStatus != 'Won' || (currentStage == 'InFlight' || currentStage == 'Closed Sales' || nextStage == 'Closed Sales' || nextStage == 'Prove & Close')){
            opportunity.StageName = nextStage;
            opportunity.Stage__c = nextStage;
        }
        system.debug('@@@@Stage'+currentStage+'####'+nextStage);

        opportunity.Sales_Status__c = salesStatus != null ? salesStatus : opportunity.Sales_Status__c;
        opportunity.Probability = salesStatus == 'Lost' ? opportunity.Probability : salesStatus == 'Won' ? 100 : oppStage.DefaultProbability;
        opportunity.UIFlag__c = opportunity.UIFlag__c != null ? opportunity.UIFlag__c + 1 : 1;

        System.debug('OPP-Sales_Status__c: ' + opportunity.Sales_Status__c);
        System.debug('OPP-StageName: ' + opportunity.StageName);
        System.debug('OPP-Stage__c: ' + opportunity.Stage__c);
if(currentStage!='Propose' && salesStatus!='Lost'){
       update opportunity;
}
        return opportunity;
    }
}
