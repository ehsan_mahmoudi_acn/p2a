global with sharing class LocalLoopInternetCheck {
  

  webservice static String getAddresses (String city, String country, String building, String street, String district) {
    
    system.debug('****** LocalLoopCheck search values ******** ' + city +' - '+country+' - '+building+' - '+street+' - '+district);
    String cityCondition = (city == '') ? '' : 'City__c like \'%' + city +'%\'';
    String countryCondition = (country == '') ? '' : 'Name like \'%' + country +'%\'';
    String buildingCondition = (building == '') ? '' : 'Building__c like \'%' + building +'%\'';
    String streetCondition = (street == '') ? '' : 'Street__c like \'%' + street +'%\'';
    String districtCondition = (district == '') ? '' : 'District__c like \'%' + district +'%\'';
    List <String> conditionList = new List <String> {cityCondition, countryCondition, buildingCondition, streetCondition, districtCondition};
    String conditions ='';
    for(String item : conditionList){
      if(item != '' && conditions == ''){
        conditions += item;
      } else if (item != ''){
        conditions += ' AND '+item;
      }
    }
    String whereConditions = (conditions == '') ? ' LIMIT 150' : ' where ' + conditions+' LIMIT 150';
    String query = 'select Id, Name, CS_Country__c, CS_City__c, Pop__c,Speed__c, CS_POP__c, CS_Bandwidth_Product_Type__c, City__c, Street__c, Building__c, District__c from Local_Access_Rate_Card__c ' + whereConditions;
    List<Local_Access_Rate_Card__c> data = Database.query(query);
    system.debug('****** LocalLoopCheck Local_Access_Rate_Card__c ******** ' + data);
    return JSON.serialize(data);
  }
}