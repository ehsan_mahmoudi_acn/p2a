@isTest
global class CustomerActivationClassHandlerTest {
    
   public static void generateData(){     
        
        string customerDateTimeString = '2017-07-27 15:19:00';
        DateTime customerDateTime = DateTime.valueofGmt(customerDateTimeString);
        string customerTimeZoneSidId = 'Europe/London';
        
        TimeZone customerTimeZone = TimeZone.getTimeZone(customerTimeZoneSidId);
        //System.assertEquals('Greenwich Mean Time',  customerTimeZone.getDisplayName());
                
        integer offsetToCustomersTimeZone = customerTimeZone.getOffset(customerDateTime);
        System.debug('GMT Offset: ' + offsetToCustomersTimeZone + ' (milliseconds) to PST');
        
        // For the given Date I expect PST to be GMT - 8 hours
       // System.assertEquals(1, offsetToCustomersTimeZone / (1000 * 60 *60));
        
        // Here you might like to explicitly use 'Asia/Colombo' to get IST in your code.
        TimeZone tz = UserInfo.getTimeZone();
        // During daylight saving time for the Pacific/Auckland time zone
        integer offsetToUserTimeZone = tz.getOffset(customerDateTime);
        System.debug('GMT Offset: ' + offsetToUserTimeZone + ' (milliseconds) to NZDT');
        
        // Expect NZDT to be GMT + 13 hours
      //  System.assertEquals(8, offsetToUserTimeZone / (1000 * 60 *60));
        
        // Figure out correct to go from Customers DateTime to GMT and then from GMT to Users TimeZone
        integer correction = offsetToUserTimeZone - offsetToCustomersTimeZone;
        System.debug('correction: ' + correction);
        
        // Note: Potential issues for TimeZone differences less than a minute
        DateTime correctedDateTime = customerDateTime.addMinutes(correction / (1000 * 60));
        System.debug('correctedDateTime: ' + correctedDateTime);
        // In the users Pacific/Auckland timezone the time should be moved forward 21 hours
       // System.assertEquals(correctedDateTime, DateTime.valueofGmt('2017-07-27 22:19:00'));  
        
    }

    @testSetup
    private static void setupData() {
        generateData();
        List<Account> Acclist = P2A_TestFactoryCls.getAccounts(1);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Order__c> orderList = P2A_TestFactoryCls.getorder(1,OrdReqList);


        List<String> cityNames = new List<String>{
            'London',
            'Pune',
            'HONG KONG/KUALA LUMPUR',
            'Sydney/Melbourne',
            'Tokyo',
            'Los Angeles',
            'New York'
        };
        List<Customer_Activation_Test__c> catList = new List<Customer_Activation_Test__c>();
        for (String cityName : cityNames) {

            Customer_Activation_Test__c cat = new Customer_Activation_Test__c();
            cat.Account__c = Acclist[0].id;
            cat.Customer_Name__c = 'CATTest';
            cat.Customer_contact_details__c = 'Sample Test Street';
            cat.End_time_for_activation_activity__c =  datetime.newInstance(2017,07,29,15,19,00);
            cat.Estimated_Start_Date__c = datetime.newInstance(2017,07,29,14,48,00);
            cat.Order__c = orderList[0].id;
            cat.Start_time_for_activation_activity__c = datetime.newInstance(2017,07,18,15,19,00);
            cat.START_Time_Tibco__c = datetime.newInstance(2017,07,28,14,48,00);  
            cat.End_Time_Tibco__c =  datetime.newInstance(2017,07,28,16,48,00);  
            cat.Time_zone_for_appointment__c = cityName;
            cat.Parent_Product__c = 'CAT';
            cat.Product_Configuration_Type__c = 'New Provide';
            cat.Region__c = 'US';
            cat.CAT_InFlight_Update__c = 'NO';
            cat.Scope_of_work__c = 'Scope';
            cat.Request_Summary__c = 'Request';
            cat.Work_Order_Status__c =  'New';
            cat.Type_of_Update__c = 'data_update';
            cat.Service_Activation_Queue__c =  'GSDI - Technical Delivery';

            catList.add(cat); 

        }    
        insert(catList);
        system.assertEquals(catList.size(), cityNames.size());       
    }
    
    public Static testMethod void Timezonechanges1(){  
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WebServiceConnectorMock());
        Customer_Activation_Test__c cat = [SELECT Id, Name, Time_zone_for_appointment__c FROM Customer_Activation_Test__c LIMIT 1];
        cat.Time_zone_for_appointment__c = 'New York';
        update(cat);
        Test.StopTest(); 
    }
    
    public Static testMethod void Timezonechanges2(){  
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WebServiceConnectorMock());
        Customer_Activation_Test__c cat = [SELECT Id, Name, Time_zone_for_appointment__c FROM Customer_Activation_Test__c LIMIT 1];
        cat.Time_zone_for_appointment__c = 'Los Angeles';
        update(cat);
        Test.StopTest(); 
    }

    public Static testMethod void Timezonechanges3(){  
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WebServiceConnectorMock());
        Customer_Activation_Test__c cat = [SELECT Id, Name, Time_zone_for_appointment__c FROM Customer_Activation_Test__c LIMIT 1];
        cat.Time_zone_for_appointment__c = 'Tokyo';
        update(cat);
        Test.StopTest(); 
    }

    public Static testMethod void Timezonechanges4(){  
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WebServiceConnectorMock());
        Customer_Activation_Test__c cat = [SELECT Id, Name, Time_zone_for_appointment__c FROM Customer_Activation_Test__c LIMIT 1];
        cat.Time_zone_for_appointment__c = 'Sydney/Melbourne';
        update(cat);
        Test.StopTest(); 
    }

    public Static testMethod void Timezonechanges5(){  
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WebServiceConnectorMock());
        Customer_Activation_Test__c cat = [SELECT Id, Name, Time_zone_for_appointment__c FROM Customer_Activation_Test__c LIMIT 1];
        cat.Time_zone_for_appointment__c = 'HONG KONG/KUALA LUMPUR';
        update(cat);
        Test.StopTest(); 
    }

    public Static testMethod void Timezonechanges6(){  
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WebServiceConnectorMock());
        Customer_Activation_Test__c cat = [SELECT Id, Name, Time_zone_for_appointment__c FROM Customer_Activation_Test__c LIMIT 1];
        cat.Time_zone_for_appointment__c = 'Pune';
        update(cat);
        Test.StopTest(); 
    }
}