@isTest
private class FieldEmailValueGeneratorTest{

static testMethod void unittest() {
        
        CurrencyValue__c cv = new CurrencyValue__c();
        cv.CreatedDate = Datetime.now();
        cv.name = 'Currency';
        cv.Currency_Value__c = 45;
        insert cv;
        System.assertEquals(45,cv.Currency_Value__c );
        
        Blob blobValue = EncodingUtil.convertFromHex('4A4B4C');
        system.debug('--blobValue--'+blobValue);
        System.assertEquals('JKL', blobValue.toString());
        
        
        Schema.DescribeFieldResult SdFre = CurrencyValue__c.Currency_Value__c.getDescribe();
        
        list<String> slist = new list<string>();
        slist.add('sekhar@gmail.com');
        slist.add('sekhar@gmail.com');
        slist.add('sekhar@gmail.com');
        slist.add('sekhar@gmail.com');
        
        Test.startTest();
            FieldEmailValueGenerator fdvg = new FieldEmailValueGenerator();  
            Boolean  b = fdvg.canGenerateValueFor(SdFre);
            //object obj = fdvg.generate(SdFre);
            String s= fdvg.getRandom(slist);
          
        Test.stopTest();
    
    }

}