@isTest(seealldata=false)
private class BatchupdateOrderTest{         
   @isTest
    static void batchTestMethod(){            
       P2A_TestFactoryCls.sampleTestData();
       List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
       List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
       List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
       List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
       List<csord__Order__c>order= P2A_TestFactoryCls.getorder(2, OrdReqList);
       List<solutions__c> sol = P2A_TestFactoryCls.getsolutions(1,acclist);
       List<user>userList=P2A_TestFactoryCls.get_Users(1);
       sol[0].batch_count__c=2;
       update sol;
       system.assert(sol!=null);
       order[0].Is_Order_Submitted__c=true;
       order[0].Status__c='Submitted';
       order[0].Is_InFlight_Update__c=true;
       order[0].solutions__c=sol[0].id;
       
       order[1].Is_Order_Submitted__c=true;
       order[1].Status__c='Submitted';
       order[1].Is_InFlight_Update__c=false;
       order[1].solutions__c=sol[0].id;
       update order;
       system.assert(order!=null);
       //Database.BatchableContext BC;
       // = new Database.BatchableContext();
       
        Test.startTest();
            BatchupdateOrder bc = new BatchupdateOrder(sol[0].id);
            //batchOrd.start(BC);
            Database.executeBatch(bc);
            Database.executeBatch(bc);
           // batchOrd.inflightupdate=false;
           // batchOrd.execute(BC,order);
           // batchOrd.finish(BC);
            
        Test.stopTest();
            
    }
}