/**
 * Collection of utilities to deal with retrieving ids from SObjects
 */
public class SObjectsUtil {

    /**
     * Returns a set of Id from the given collection of SObjects
     * 
     * @param objects a collection of object from which to retrieve ids
     * @return a set of ids. Null values are removed from the Set.
     */
    public static Set<Id> getIds(List<SObject> objects) {
        // using a new map instead of using SObject.get('Id') as it is much faster
        Set<Id> ids = new Map<Id, SObject>(objects).keySet();
        ids.remove(null);
        return ids;
    }
    
    /**
     * Returns a set of Id from the given collection of SObjects
     * 
     * @param objects a collection of object from which to retrieve ids
     * @param fieldName the name of the field to extract ID from (can contain relationship)
     * @return a set of ids. Null values are removed from the Set.
     */
    public static Set<Id> getIds(List<SObject> objects, String fieldName) {
        Set<Id> ids = new Set<Id>();
        for (Object id : getValues(objects, fieldName)) {
            ids.add((Id) id);
        }
        ids.remove(null);
        return ids;
    }
    
    /**
     * Returns a list of values from the given collection of objects.
     * 
     * @param objects a collection of object from which to retrieve values
     * @param fieldName the name of the field to extract ID from (can contain relationship)
     * @return a list of values. Null values are kept in the List
     */
    public static List<Object> getValues(List<SObject> objects, String fieldName) {
        List<Object> values = new List<Object>();
        List<String> fields = fieldName.split('\\.');
        for (SObject obj: objects) {
            values.add(getValue(obj, fields));
        }
        return values;
    }
    
    /**
     * Returns the value for the given field.
     * 
     * Note: returns null if any of the field in the hierachy
     *       returns null (i.e. no NullPointerException)
     * 
     * @param obj the object to retrieve the value from
     * @param fieldNames the hierarchy of fields to traverse
     * @return the value of the field
     */
    private static Object getValue(SObject obj, List<String> fieldNames) {
        SObject current = obj;
        Integer lastIndex = fieldNames.size() - 1;
        for (Integer i = 0; i < lastIndex; i++) {
            current = (SObject) current.getSObject(fieldNames.get(i));
            if (current == null) {
                return null;
            }
        }
        return current.get(fieldNames.get(lastIndex));
    }
    
}