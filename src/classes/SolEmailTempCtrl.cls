public class SolEmailTempCtrl{
     
    public SolEmailTempCtrl(){}

    public static Id templateId = [select Id from EmailTemplate where developername = 'Solution_Summary_Template'].Id;
    public static List<csord__order__c> SusOrders {get;set;}
    public static List<csord__order__c> FailOrders {get;set;}
    public static solutions__c  solnObjName{get;set;}
    
    public static void sendEmail(solutions__c solnObj){
        Map<Id,csord__order__c> ordMap = new Map<Id,csord__order__c>([SELECT Id,name,csord__Order_Number__c,Status__c,Is_Order_Submitted__c FROM csord__order__c WHERE Solutions__c =:solnObj.Id]); 
        SusOrders = new List<csord__order__c>();
        FailOrders = new List<csord__order__c>();       
        solnObjName = solnObj;

        if(ordMap != null){
            for(csord__order__c ord :ordMap.values()){
                if(solnObj.Submitted_Orders__c.contains(ord.Id) != null && solnObj.Submitted_Orders__c.contains(ord.Id)){
                    if(ord.Status__c != 'New' && ord.Is_Order_Submitted__c == true){
                        SusOrders.add(ord);
                    }else if(ord.Status__c == 'New' && ord.Is_Order_Submitted__c == false){
                        FailOrders.add(ord);
                    }
                }
            }
        }
                     
        EmailUtil.EmailData mailData = new EmailUtil.EmailData();
        mailData.message.setSaveAsActivity(false);
        mailData.message.setUseSignature(false);
        mailData.message.setTemplateId(templateId);
        mailData.message.setTargetObjectId(UserInfo.getUserId());

        EmailUtil.sendEmail(mailData);
    }
}