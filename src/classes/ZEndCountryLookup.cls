global class ZEndCountryLookup extends cscfga.ALookupSearch {
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){
        // NOT IMPLEMENTED
        System.Debug('doDynamicLookupSearch');
        List <Additional_Data__c> data = [SELECT Name, Type__c FROM Additional_Data__c WHERE Type__c = 'Country'];
        return data;
    }
    public override String getRequiredAttributes(){ 
        return '[ "A-End Country" ]';
    }
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
            List <String> countryNames = new List<String>();
            String Aend_Country = searchFields.get('A-End Country');
            System.Debug('Search field = ' + Aend_Country);
            System.Debug('Search field 2= ' + searchFields);
            String query = 'SELECT Z_End_Country_Name__c FROM IPL_Rate_Card_Item__c';
            if (Aend_Country != null)
            {
                query = query + ' WHERE Aend_Country__c = :Aend_Country';
            }
            System.Debug('query = '+query);
            for (IPL_Rate_Card_Item__c rateCard : Database.query(query))
            {
                countryNames.add(rateCard.Z_End_Country_Name__c);
            }
            System.Debug('countryNames = '+countryNames);
         String searchValue = searchFields.get('searchValue') +'%';
         List <Additional_Data__c> data = [SELECT Name, Type__c FROM Additional_Data__c WHERE Type__c = 'Country' AND Name in :countryNames AND Name Like :searchValue ORDER BY Name];
            System.Debug('data = ' + data);
       return data;
    }

}