public class ApproveCaseCtrl 
{
    private Case caseObj {get;set;}
    public boolean displayPopup {get; set;} 
    public string callfunc{get;set;}
    public string callfunct{get;set;}
    public Pricing_Delegate__c Delegaterole;
    public String userID;
    public Boolean Approve = false ;
    public ApproveCaseCtrl(ApexPages.StandardController controller) 
    {
        List<String> fieldLst = new List<String>();
        fieldLst.add('Pricing_Approval_Case__c');
         if (!Test.isRunningTest()) {
        controller.addFields(fieldLst);
        }
        caseObj = (Case) controller.getrecord();
    }
    
    public PageReference approveCase() 
    {
        List<case> updateCase = new List<case>();
        Set<ID> caseProductBasketId = new Set<Id>();
        string  basketId;
        List<Pricing_Approval_Request_Data__c> Updateproductconfig = new List<Pricing_Approval_Request_Data__c>();
        Boolean checkactionitem=orderdeskapprove(caseObj);
        system.debug('====checkactionitem=='+checkactionitem);
        if(!checkactionitem)
        {
            system.debug('====!checkactionitem==='+!checkactionitem);
            callfunc='<script> checkactionitemstatus() </script>';
            return null;
        
        }
        if(userinfo.getProfileId() == Label.TI_Commercial || userinfo.getProfileId() == Label.System_Admin)
        {
            userId = userinfo.getuserid();
            system.debug('@@@userId'+userId);
        }
        map<string,decimal> queriedcurrencies = new map<string,decimal>();
        for(CurrencyValue__c c : [SELECT Name , Currency_Value__c FROM CurrencyValue__c])
        {
            queriedcurrencies.put(c.Name,c.Currency_Value__c);
        }
        List<case> appCase = [SELECT id, CurrencyIsoCode, Approval_Status_Reason__c,Approved_Offline__c, Product_Basket__c, Margin__c, TCV__c, Confirmedcheck__c, Approval_Rejection_Date__c FROM case WHERE id=:caseObj.id];
        
        for(case aCase : appCase)
        {
            caseProductBasketId.add(aCase.Product_Basket__c);
            basketId=aCase.Product_Basket__c;
        }
        List<cscfga__Product_Configuration__c> productconfig = [select id, Approved_Offer_Price_MRC__c, Approved_Offer_Price_NRC__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c IN : caseProductBasketId ];
        PricingApproval.setActualConfiguration(productconfig);
        List<Pricing_Approval_Request_Data__c> pricingRequestdata = [select id
            , Product_Configuration__r.cscfga__Product_Family__c
            , Product_Configuration__r.name
            , Product_Basket__c, Approved_NRC__c, Approved_RC__c
            , Product_Configuration__c, Cost_NRC__c, Cost_RC__c
            , Product_Configuration__r.Product_Name__c
            , Product_Configuration__r.cscfga__Product_Basket__c
            , Product_Configuration__r.cscfga__Configuration_Status__c 
            , Product_Configuration__r.cscfga__Root_Configuration__c
            , Act_Approved_NRC__c, Approved_Offer_Price_MRC__c
            , Act_Approved_RC__c, Approved_Offer_Price_NRC__c
            , Act_Approved_Burst_Rate__c, Actual_Configuration__c
            , Act_Approved_China_Burst_Rate__c, Approved_Configuration__c
            , Approved_Burst_Rate__c,Approved_China_Burst_Rate__c
            from 
            Pricing_Approval_Request_Data__c 
            where 
            Product_Basket__c IN : caseProductBasketId
        and Product_Configuration__r.cscfga__Product_Family__c != null ORDER BY Product_Configuration__r.cscfga__Root_Configuration__c DESC NULLS LAST];
        
        string currencycode = String.valueof(appCase[0].get('CurrencyIsoCode'));
        decimal convertedvalue = 0;
        decimal conversionrate = 0;
        for(STRING d : queriedcurrencies.KEYSET()){
            if(currencycode == d){
                conversionrate = queriedcurrencies.get(d);
                convertedvalue = appCase[0].TCV__c / conversionrate ;
            }
        }
          for(Pricing_Approval_Request_Data__c Pa : pricingRequestdata)
        {
             /*if((Pa.Product_Configuration__r.cscfga__Product_Family__c != 'Internet-Onnet' ||
            Pa.Product_Configuration__r.cscfga__Product_Family__c != 'IPT Multihome' || Pa.Product_Configuration__r.cscfga__Product_Family__c != 'IPVPN Port'
            
            || Pa.Product_Configuration__r.cscfga__Product_Family__c != 'TWI Multihome') &&
            (Pa.Approved_NRC__c == null || Pa.Approved_RC__c == null || Pa.Cost_NRC__c == null || Pa.Cost_RC__c == null))
            {
                callfunc='<script> MrcNrcCheck() </script>';
                return null;
            } 
                   
            if((Pa.Product_Configuration__r.cscfga__Product_Family__c == 'Internet-Onnet' ||
            Pa.Product_Configuration__r.cscfga__Product_Family__c == 'IPT Multihome' || Pa.Product_Configuration__r.cscfga__Product_Family__c == 'IPVPN Port'
            
            || Pa.Product_Configuration__r.cscfga__Product_Family__c == 'TWI Multihome')            
            && (pa.Approved_Burst_Rate__c == null || pa.Approved_China_Burst_Rate__c == null || Pa.Approved_NRC__c == null || Pa.Approved_RC__c == null || Pa.Cost_NRC__c == null || Pa.Cost_RC__c == null))
            {
               callfunc='<script> MrcNrcCheck() </script>';
                return null;
            } */
            
            // defect #15248 - Apurva
            if(Pa.Approved_NRC__c == null || Pa.Approved_RC__c == null || Pa.Cost_NRC__c == null || Pa.Cost_RC__c == null)
            {
                callfunc='<script> MrcNrcCheck() </script>';
                return null;
            }
            
            PricingApprovalExtCr1 pcVal = new PricingApprovalExtCr1(new ApexPages.StandardController(caseObj));
            Boolean BurstVisible = pcVal.isVisible1;
            if(BurstVisible!=null) {
            If(!BurstVisible && (pa.Approved_Burst_Rate__c == null || pa.Approved_China_Burst_Rate__c == null))
            {
                callfunc='<script> MrcNrcCheck() </script>';
                return null;
            }
         }
        }
        if(appCase[0].Approved_Offline__c == false)
        {
            //List<Pricing_role__c> pricingRoles = [SELECT Id,Owner.id, Name,Role__c,Margin__c,Revenue_TCV__c,Term__c FROM Pricing_Role__c where owner.id =: userinfo.getuserid()];
            
            for(Pricing_Role__c p : [SELECT Id,Owner.id, Name,Role__c,Margin__c,Revenue_TCV__c,Term__c FROM Pricing_Role__c where owner.id =: userinfo.getuserid()]) 
            {  
                for (Case casequote : appCase)
                {
                    if((casequote.Margin__c >= p.margin__c) && (convertedvalue <= p.Revenue_TCV__c)) 
                    { 
                        Approve = true;
                    }
                }
            }
            if(Approve == false)
            {
                List<Pricing_Delegate__c> Delegates = [select Id,Name,Delegate_Start_Date__c,Pricing_roles__c,Delegates__c,Pricing_Roles__r.margin__c,Pricing_Roles__r.Revenue_TCV__c,Delegate_End_Date__c from Pricing_Delegate__c where Delegates__c =: userinfo.getuserid()];
                for(Pricing_Delegate__c pricingdelegate : Delegates) 
                {  
                    for(Case casequotes : appCase)
                    {
                        if((casequotes.Margin__c >= pricingdelegate.Pricing_Roles__r.margin__c) && (convertedvalue <= pricingdelegate.Pricing_Roles__r.Revenue_TCV__c) && pricingdelegate.Delegate_Start_Date__c<=System.Today() && pricingdelegate.Delegate_End_Date__c>=System.Today()) 
                        { 
                            Approve = true;
                        }
                    }
                    if(pricingdelegate.Delegate_Start_Date__c>System.Today())
                    {
                        callfunc='<script> functdelgate() </script>';
                        return null; 
                    }
                    if(pricingdelegate.Delegate_End_Date__c<System.Today())
                    {
                        callfunc='<script> functdelgateend() </script>';
                        return null; 
                    }
                }
            }
        }
                system.debug('before approve');
        if(Approve == true || appCase[0].Approved_Offline__c == true) 
        {
            for (Case casequoteapp : appCase)
            {
                if(casequoteapp.Approval_Status_Reason__c != null) 
                {
                    Case upcase = new Case();
                    upcase.id = caseobj.id;
                    upcase.status = 'Approved';
					upcase.RecordTypeId = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType,'Price Request Approved');
                    upcase.Approved_Rejected_By__c = userinfo.getUserId();
                    upcase.Approval_Rejection_Date__c = System.today();
                    updateCase.add(upcase);
                    system.debug('before PC');
                    For(cscfga__Product_Configuration__c AppPcdata : productconfig)
                    {   
                        system.debug('before Pdata');
                        system.debug('productconfig.id'+AppPcdata.id);
                        For(Pricing_Approval_Request_Data__c parData : pricingRequestdata)
                        {  
                            system.debug('parData.id'+parData.id);
                            if(AppPcdata.id == parData.Product_Configuration__c)
                            {
                                ParData.Approved_Configuration__c = parData.Actual_Configuration__c;
                                ParData.Approved_Offer_Price_MRC__c = parData.Act_Approved_RC__c;
                                ParData.Approved_Offer_Price_NRC__c = parData.Act_Approved_NRC__c;                                
                                ParData.is_Pricing__c = true;
                                system.debug('this is executing');
                                /*    AppPcdata.Approved_Configuration__c   = AppPcdata.Actual_Configuration__c;
                                    AppPcdata.Approved_Offer_Price_MRC__c = parData.Act_Approved_RC__c;
                                AppPcdata.Approved_Offer_Price_NRC__c = parData.Act_Approved_NRC__c;    */
                                Updateproductconfig.add(parData);
                            }
                        }
                    }
                }
                else
                {
                    callfunc='<script> func() </script>';
                    return null;
                } 
            }
        }
        
        else
        {
            callfunc='<script> funct() </script>';
            return null;
        }       
        update Updateproductconfig;
        ApproveCaseCtrlBatch bc = new ApproveCaseCtrlBatch(AppCase.get(0).Product_Basket__c); 
        Database.executeBatch(bc,1);        
        update updateCase;
        
        return new PageReference('/'+caseobj.id);
    }
    public boolean orderdeskapprove(Case caseobj)
    {
       // list<Action_Item__c> acitem=[select id,Recordtype.name,Status__c,Pricing_Case_Approval__c,Opportunity__r.id from Action_Item__c where Opportunity__c=:caseobj.opportunity_name__c and Recordtype.name='Pricing Validation' and Pricing_Case_Approval__c='Order Desk' and Status__c='Approved'];
         if(((userinfo.getProfileId()).contains(label.Profile_TI_Order_Desk) && caseobj.Pricing_Approval_Case__c =='Order Desk'  )|| (userinfo.getProfileId()).contains(label.Profile_TI_Commercial) || (userinfo.getProfileId()).contains(label.System_Admin))
        {
          return true;      
        }else{
            return false;
        }
}

}