@isTest(seealldata = false)
public class AllOrderTriggerTest {
    
    private static List<csord__Order__c> pb;
    public static List<Product_Definition_Id__c> productDefIdList; 
    
    @testSetUp static  void setup() {
    
    try{
       P2A_TestFactoryCls.sampleTestData();

        
        Map<id,csord__Order__c> mprodo= new Map<id,csord__Order__c>();
        
        List<Account> ListAcc  = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> ListOpp = P2A_TestFactoryCls.getOpportunitys(1,ListAcc);
        List<cscfga__Product_basket__c> Products =  P2A_TestFactoryCls.getProductBasketHdlr(1,ListOpp); 
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        //RecordType rc = [select id from RecordType where DeveloperName='OffNet_Record_Type' limit 1];
        list<CSPOFA__Orchestration_Process_Template__c> orchprocessTemplist = P2A_TestFactoryCls.getOrchestrationProcess(5);    
        List<CSPOFA__Orchestration_Process__c>processList = P2A_TestFactoryCls.getOrchestrationProcesss(5,orchprocessTemplist);    
        List<CSPOFA__Orchestration_Step__c>stepList=P2A_TestFactoryCls.getOrchestrationStep(5,processList);
        List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(5,OrdReqList);    
        List<csord__service__c> servList = P2A_TestFactoryCls.getservice(5,OrdReqList,subscriptionList);    
        List<case>caseList=P2A_TestFactoryCls.getcase(1,ListAcc); 
    
        system.assert(caseList!=null);}      
        catch(exception ex){
          System.debug(Ex);
        }
  }
  //Test method for Order simplification postive behaviour
  @isTest
  static void testIp006Case(){
  	//Start Test
  	test.startTest();
  	//Query the order records
  	list<csord__Order__c> orderLst = [select id,Status__c from csord__order__c limit 1];
  	system.assertequals(orderLst.size(),1);
  	//Update the Case subject to Verify Order and Trigger IP006
  	list<case> caseForUpdate = new list<case>();
  	case echCase = new case();
  	echCase.Order__c = orderLst[0].id;
  	echCase.subject = 'Verify Order and trigger IP006';
  	echCase.Status='New';
  	caseForUpdate.add(echCase);
  	insert caseForUpdate;
  	  
  	if(caseForUpdate!=null && caseForUpdate.size() >0){
  		update caseForUpdate;
  		system.assertequals('Verify Order and trigger IP006',caseForUpdate[0].subject);
  		system.assertequals('New',caseForUpdate[0].Status);
  		system.assertequals(orderLst[0].id,caseForUpdate[0].Order__c);
  	}
  	//Update the Order object to mark status as complete
 	orderLst[0].Status__c='Completed';
 	try{
 		update orderLst;
 	}
 	catch(exception ex){
		system.Assert(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'), Label.Ver_Ord_Completion);
	}
  	//Stop Test
  	test.stoptest();
  
  } 
  //Test method for Order simplification postive behaviour
  @isTest
  static void testhandoverComplTerminate(){
  	//Start Test
  	test.startTest();
  	//Query the order records
  	list<csord__Order__c> orderLst = [select id,Handover_Complete__c,csord__Order_Type__c from csord__order__c limit 1];
  	system.assertequals(orderLst.size(),1);
  	list<csord__Order_Request__c> OrdReqList = new list<csord__Order_Request__c>();
	csord__Order_Request__c ordreq = new csord__Order_Request__c();
	ordreq.Name = 'OrderNametest';
	ordreq.csord__Module_Name__c = 'tes';
	ordreq.csord__Module_Version__c = 'test';
	ordreq.csord__Process_Status__c = 'Requested';
	ordreq.csord__Request_DateTime__c = System.now();
  
	OrdReqList.add(ordreq);
	insert OrdReqList;
	system.assertEquals(OrdReqList[0].Name,'OrderNametest');
	list<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
	list<csord__service__c> serviceLst = P2A_TestFactoryCls.getservice(1,OrdReqList,subscriptionList);
  
	//link the service to order
	system.assertEquals(serviceLst.size(),1);
	serviceLst[0].csord__Order__c =orderLst[0].id;
	serviceLst[0].Stop_Billing_Date__c=null;
	serviceLst[0].Billing_Commencement_Date__c=null;
	serviceLst[0].Cease_Service_Flag__c=true;
	
	//mark csord__Order_Type__c as terminate
	orderLst[0].Handover_Complete__c =true;
	orderLst[0].Order_Type__c	='Terminate';
	try{
        update serviceLst;
        system.assertequals(1,serviceLst.size());
		update orderLst;
	}
	catch(exception ex){
        if(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
		system.Assert(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'), Label.TerminateOrder_Label);
	}
  	//Stop Test
  	test.stoptest();
  
  } 
  //Test method for Order simplification postive behaviour
  @isTest
  static void testhandoverComplNonTerminate(){
  	//Start Test
  	test.startTest();
  	//Query the order records
  	list<csord__Order__c> orderLst = [select id,Handover_Complete__c,Order_Type__c from csord__order__c limit 1];
  	system.assertequals(orderLst.size(),1);
  	list<csord__Order_Request__c> OrdReqList = new list<csord__Order_Request__c>();
	csord__Order_Request__c ordreq = new csord__Order_Request__c();
	ordreq.Name = 'OrderNametest2';
	ordreq.csord__Module_Name__c = 'test';
	ordreq.csord__Module_Version__c = 'test';
	ordreq.csord__Process_Status__c = 'Requested';
	ordreq.csord__Request_DateTime__c = System.now();
	
	OrdReqList.add(ordreq);
	insert OrdReqList;
	system.assertEquals(OrdReqList[0].Name,'OrderNametest2');
	list<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
	list<csord__service__c> serviceLst = P2A_TestFactoryCls.getservice(1,OrdReqList,subscriptionList);
  
	//link the service to order
	system.assertEquals(serviceLst.size(),1);
	serviceLst[0].csord__Order__c =orderLst[0].id;
	serviceLst[0].Stop_Billing_Date__c=null;
	serviceLst[0].Billing_Commencement_Date__c=null;
	
	//mark csord__Order_Type__c as nonterminate
	orderLst[0].Handover_Complete__c =true;
	orderLst[0].Order_Type__c='new';
    try{
        update serviceLst;    
        system.assertequals(1,serviceLst.size());
		update orderLst;
	}
	catch(exception ex){
        if(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
		system.Assert(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'), Label.NonTerminateOrder_Label);
	}
  	//Stop Test
  	test.stoptest();
  }
  //first test method
    @isTest static void testpushStatusToActiveStep(){
    test.startTest();
    
    List<csord__Order__c> Orders = [select id,RAG_Reason_Code__c,RAG_Status__c,RAG_Status_Red__c,RAG_Order_Status_RED__c from csord__order__c where id !=''];
    List<CSPOFA__Orchestration_Process__c>processList=[SELECT Id,Order__c FROM CSPOFA__Orchestration_Process__c where id!=''];
    List<CSPOFA__Orchestration_Step__c>stepList=[select id,CSPOFA__Orchestration_Process__c from CSPOFA__Orchestration_Step__c where id!=''];
    List<csord__Order__c> Orderupdate=new List<csord__Order__c>();
    List<CSPOFA__Orchestration_Process__c> processupdate=new List<CSPOFA__Orchestration_Process__c>();
    List<CSPOFA__Orchestration_Step__c> stepupdate=new List<CSPOFA__Orchestration_Step__c>();
    
    Map<Integer,Id>ordMap=new Map<Integer,Id>();
    Map<Integer,Id>proMap=new Map<Integer,Id>();
    Map<id,csord__Order__c> mprodn= new Map<id,csord__Order__c>();
    
    for(integer i=0;i<Orders.size();i++){
      csord__Order__c ord=Orders[i];
      ordMap.put(i,Orders[i].id);
      mprodn.put(Orders[i].id,Orders[i]);
      Orderupdate.add(ord);
    }
    
    update Orderupdate;
    
    
    for(integer i=0;i<processList.size();i++){
        CSPOFA__Orchestration_Process__c pro=processList[i];      
            if(i<2){
                pro.Order__c=ordMap.get(i);  
            }
                proMap.put(i,processList[i].id);
                processupdate.add(pro);       
            }
        update processupdate;    
    
    for(integer i=0;i<stepList.size();i++){
        CSPOFA__Orchestration_Step__c step=stepList[i];
        if(i<2){
            step.CSPOFA__Status__c='Complete';
            step.RAG_Status__c='Red';
        }
        //stepList[i].CSPOFA__Orchestration_Process__c=proMap.get(i);
        stepupdate.add(step);       
        }
       update stepupdate;
        
    AllOrderTriggersHandler.pushStatusToActiveStep(Orders,mprodn);
    
    test.stopTest();
    
    CSPOFA__Orchestration_Step__c OrchStep = [select id,CSPOFA__Status__c from CSPOFA__Orchestration_Step__c where id =: stepupdate[0].id];

    system.assertEquals(5, [select count() from CSPOFA__Orchestration_Step__c], 'Should contain 1 Orchestration Step.');
    System.assertEquals('Complete',OrchStep.CSPOFA__Status__c,'Should contain orchestration Status = \'Complete\'');
    System.AssertNotEquals(Null,Orderupdate);

    }
	
    //second test method
    @isTest static void testmapOrdersByType(){
      
     List<csord__Order__c> Orders = [select id from csord__order__c where id !=''];
     List<csord__Order__c> Orderupdate=new List<csord__Order__c>();    
     
     for(integer i=0;i<Orders.size();i++){
       csord__Order__c ord=Orders[i];
       if(i<2){
       ord.Order_Type__c = 'Terminate';
       }
       else{
        ord.Order_Type__c = 'Reconfiguration'; 
       }
       Orderupdate.add(ord);
     }
     
     update Orderupdate;
     
     Test.startTest();
        AllOrderTriggersHandler allo = new AllOrderTriggersHandler();
        Map<String, List<Id>> testList=  allo.mapOrdersByType(Orderupdate);
     Test.stopTest();  
     
     csord__Order__c ord = [select id,Order_Type__c  from csord__Order__c limit 1];
     System.AssertEquals('Terminate',ord.Order_Type__c);
     System.AssertNotEquals(Null,ord);
     
    
    }
    
    //third test method
    @isTest static void testcancelEarlyTerminationCalc(){
        setup();
        List<case> caseupdate=new List<case>();
        set<id>orderSet=new set<id>(); 
        List<csord__Order__c> Orders =[select id from csord__order__c where id !=''];
        
        List<case> caseList = [select id from case where id !=''];

        for(csord__Order__c o:Orders){
            if(!orderSet.contains(o.id))orderSet.add(o.id); 
        }
        
        for(integer i=0;i<caseList.size();i++){
            case c=caseList[i]; 
            c.order__c=Orders[0].id;
            caseupdate.add(c);
        }
        update caseupdate;
        
        //Id myId = caseupdate[0].id;
        
         
        Test.startTest();
            AllOrderTriggersHandler allo = new AllOrderTriggersHandler();
            allo.cancelEarlyTerminationCalc(orderSet,'test');  
        Test.stopTest();
        
        system.assertNotEquals(null,allo);
        //system.assertEquals(Schema.case.SObjectType, myId.getSobjectType());
             
    }
    
    //fourth test method
    @isTest static void testsetSharing(){
        
        List<csord__Order__c> Orders =[select id from csord__order__c where id !=''];
        List<Opportunity> ListOpp=[select id from Opportunity where id !=''];
        List<csord__Order__c> Orderupdate=new List<csord__Order__c>();
    
        for(integer i=0;i<Orders.size();i++){
            csord__Order__c ord=Orders[i];
            ord.csordtelcoa__Opportunity__c=ListOpp[0].id;
            Orderupdate.add(ord);
        }    
        update Orderupdate;
          
          Id myId = Orderupdate[0].id;
          
          Test.startTest();          
              AllOrderTriggersHandler.setsharing(Orderupdate);
          Test.stopTest();
          
          system.assert(Orderupdate!=null); 
          
          system.assertEquals(Schema.csord__Order__c.SObjectType, myId.getSobjectType());
          
       }
        
    //fourth test method
    @isTest static void testsetOpportunityAndAccountID(){
     
    
      test.startTest();    
    List<csord__Order__c> Orders =[select id from csord__order__c where id !=''];
    List<Opportunity> ListOpp=[select id from Opportunity where id !=''];
    List<csord__Order_Request__c> serReqList=[select id from csord__Order_Request__c where id !=''];
    List<csord__Subscription__c> subscriptionList=[select id from csord__Subscription__c where id !=''];
    List<csord__Service__c> servlist=[select id from csord__service__c where id !=''];
    List<csord__Order__c> Orderupdate=new List<csord__Order__c>();
    List<csord__Service__c> servupdate=new List<csord__Service__c>(); 

      for(integer i=0;i<Orders.size();i++){
    csord__Order__c ord=Orders[i];
    if(i>1){
    ord.csordtelcoa__Opportunity__c=ListOpp[0].id;
      if(i==2){
        ord.Is_Order_Submitted__c=false;
      }
      else{
        ord.Is_Order_Submitted__c=true;
      }    
    }
    else{
    ord.csordtelcoa__Opportunity__c=null;  
    }
    ord.Partial_Terminate_Order__c=true;
    ord.Order_Submitted_to_SD__c=true;
    Orderupdate.add(ord);
    }    
    update Orderupdate;
    
    for(integer i=0;i<servlist.size();i++){
      
    csord__service__c ser=servlist[i];            
        ser.Opportunity__c=ListOpp[0].id;    
        if(i>0){
    ser.Cease_Service_Flag__c=true;
    }
    else{
    ser.Cease_Service_Flag__c=false;  
    }
    ser.Name = 'ServiceNew'; 
        ser.csord__Identification__c = 'Test-Catlyne-4238362';
        ser.csord__Order_Request__c = serReqList[0].id;
        ser.csord__Subscription__c = subscriptionList[0].id;
        ser.Billing_Commencement_Date__c = System.Today();
        ser.Stop_Billing_Date__c = System.Today();
        ser.RAG_Status_Red__c = false ;
        ser.Product_Id__c = 'GCC';
        ser.Order_Channel_Type__c = 'Distributor';
        ser.csord__Order__c = i<Orders.size()?Orders[i].id:Orders[0].id;
        ser.RAG_Reason_Code__c = '';
        servupdate.add(ser);    
     }
     update servupdate;
     
     AllOrderTriggersHandler allo = new AllOrderTriggersHandler();
       //allo.setOpportunityAndAccountID(Orderupdate);
       for(csord__service__c ser:servupdate){
      ser.Opportunity__c=null; 
     }
       update servupdate; 
       system.assert(servupdate!=null);  
     //allo.setOpportunityAndAccountID(Orderupdate);
    test.stopTest();
    
    csord__Order__c ord =  [select id,Is_Order_Submitted__c,Partial_Terminate_Order__c from csord__Order__c where id =:Orderupdate[0].id limit 1];
    
    System.assertEquals(ord.Is_Order_Submitted__c,false);
    System.assertEquals(true,ord.Partial_Terminate_Order__c);
   
    }
    
    //fifth test method
    @isTest static void testsetSubscriptionID(){
    List<csord__Order__c> Orders =[select id from csord__order__c where id !=''];
        List<csord__Order_Request__c> OrdReqList =[select id from csord__Order_Request__c where id !=''];            
        csord__Subscription__c sub = new csord__Subscription__c();
        sub.Name ='Test'; 
        sub.csord__Identification__c = 'Test-Catlyne-4238362';
        sub.csord__Order_Request__c = OrdReqList[0].id;
        sub.csord__Order__c = Orders[0].id;      
        
        System.assertNotEquals(sub,null,'success');
        
        insert sub;
        
        system.assert(sub!=null);
        AllOrderTriggersHandler allo = new AllOrderTriggersHandler();
        allo.setSubscriptionID(Orders);  
        
        system.assertEquals(2, [select count() from csord__Subscription__c], 'Should contain 1 Subscription.');

        
    }
    
    @isTest static void testAfterupdate(){
      
    test.starttest();  
        List<csord__Order__c> Orders =[select id,customer_required_date__c from csord__order__c where id !=''];
        List<Account> ListAcc  = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> ListOpp = P2A_TestFactoryCls.getOpportunitys(1,ListAcc);
        //List<opportunity> ListOpp =[select id from opportunity where id !=''];  
        // List<Account> ListAcc  = [select id from Account where id !=''];
        //List<Opportunity> ListOpp = P2A_TestFactoryCls.getOpportunitys(1,ListAcc);
        //List<cscfga__Product_basket__c> Products=[select id,cscfga__Opportunity__c from cscfga__Product_basket__c where id !=''];
        List<cscfga__Product_basket__c> Products =  P2A_TestFactoryCls.getProductBasketHdlr(1,ListOpp); 
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<cscfga__Configuration_Offer__c>offer=P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Bundle__c>productbundleList=P2A_TestFactoryCls.getProductBundleHdlr(1,ListOpp);
        List<cscfga__Product_Definition__c>prodDefList=P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Product_Configuration__c>pcList=P2A_TestFactoryCls.getProductonfig(4,Products,prodDefList,productbundleList,offer);    
        //List<cscfga__Product_Configuration__c> pcList=[select id from cscfga__Product_Configuration__c where id !=''];
        
        List<CSPOFA__Orchestration_Process_Template__c>otList=new List<CSPOFA__Orchestration_Process_Template__c>(); 
        List<csord__service__c> servList=[select id from csord__service__c where id !=''];
        List<cscfga__Product_basket__c> pbasket=[select id from cscfga__Product_basket__c where id !=''];
        List<csord__Order__c> Orderupdate=new List<csord__Order__c>();    
        List<cscfga__Product_Configuration__c> pcupdate=new List<cscfga__Product_Configuration__c>();
        List<csord__service__c> servupdate=new List<csord__service__c>();            
    
        for(integer i=0;i<pcList.size();i++){
        cscfga__Product_Configuration__c pc=pcList[i];
             pc.Added_Ports__c=i>2?String.valueOf(pcList[0].id)+','+String.valueOf(pcList[1].id):null;
             pcupdate.add(pc);     
        }
        update pcupdate;
        
            
        id ordId=null;
        
        for(integer i=0;i<servList.size();i++){
          csord__service__c ser=servList[i];
          //ser.Order_Status__c=i==0?'New':'Old';
          if(i==2){
            ordId=Orders[i].id;
            ser.Cease_Service_Flag__c=true;
          }
          ser.csordtelcoa__Product_Basket__c=pbasket[0].id;
          ser.Product_Code__c=i<1?'IPC':'TWIM';
          ser.product_id__c='abc';
          ser.csordtelcoa__Product_Configuration__c=pcList[0].id;
          ser.Master_service__c=i!=0?servList[0].id:null;
          ser.csord__order__c=i<Orders.size()?Orders[i].id:Orders[0].id;
          servupdate.add(ser);
        }
        update servupdate; 
        
        
        List<csord__order__c>newOrd=new List<csord__order__c>();
        Map<id,csord__order__c>newMap=new map<id,csord__order__c>();
        Map<id,csord__order__c>oldMap=new map<id,csord__order__c>();
        
        for(integer i=0;i<Orders.size();i++){
        csord__Order__c ord=Orders[i];
        oldMap.put(ord.id,ord);    
        ord.customer_required_date__c=Date.newInstance(2017,01,01);
        ord.status__c='Cancelled';
        ord.csordtelcoa__opportunity__c=ListOpp[0].id;
        ord.csord__Order_Type__c ='Subscription Creation';    
        if(ord.id==ordId){
            ord.Termination_Reason__c='just like that';
            ord.Is_Terminate_Order__c=true;
            ord.Requested_Termination_Date__c=Date.newInstance(2017,01,01);
            ord.status__c='Rejected';
        }
        newOrd.add(ord);
        newMap.put(ord.id,ord);
        }
        update newOrd;
        
        Id myId = newOrd[0].id;
        
        
        AllOrderTriggersHandler allo = new AllOrderTriggersHandler();
        allo.afterUpdate(newOrd,newMap,oldMap);
    
        test.stopTest(); 
         
        //System.assertEquals(true,servupdate[0].csordtelcoa__Product_Configuration__c!= null);        
        System.assertEquals('Cancelled', newOrd[0].status__c, 'New Order Status = \'Cancelled\'');
        system.assertEquals(Schema.csord__Order__c.SObjectType, myId.getSobjectType());
           
    }

      @isTest static void testbeforeUpdate(){
        test.startTest();
        
        List<csord__Order__c> Orders =[select id from csord__order__c where id !=''];
        List<opportunity> ListOpp =[select id from opportunity where id !=''];
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);     
        List<Account> ListAccount=[select id from Account where id !=''];
        List<csord__service__c> servList=[select id from csord__service__c where id !=''];
        List<csord__service__c> servupdate=new List<csord__service__c>();
        List<Group> groups = [select id, name from group where type = 'Queue' and Name =:'CaseQueueIfJeopardy'];     
              
        
        OpportunityPartner oppPart=new OpportunityPartner();
        //oppPart.Opportunityid=ListOpp[0].id;
        //oppPart.Role ='GCC Resale';
        //oppPart.AccountToID=ListAccount[0].id;
        //insert oppPart;
        id ordnewId=null;
        
        for(integer i=0;i<servList.size();i++){
            csord__service__c ser=servList[i];
            ser.Product_Id__c='GCC';
            ser.Order_Channel_Type__c =i<2?'Distributor':'';
            ser.csord__order__c=i<Orders.size()?Orders[i].id:Orders[0].id;
            if(i==2){
                ser.Inventory_Status__c= 'LIVE - AS DESIGNED' /*label.PROVISIONED*/;  
                ordnewId=Orders[i].id;
            }
            else{
                ser.Inventory_Status__c='LIVE - AS DESIGNED' /*label.PROVISIONED*/;
            }
            servupdate.add(ser);
            }
        update servupdate; 
        
        List<csord__order__c>newOrd=new List<csord__order__c>();
        Map<id,csord__order__c>newMap=new map<id,csord__order__c>();
        Map<id,csord__order__c>oldMap=new map<id,csord__order__c>();
        
        for(integer i=0;i<Orders.size();i++){
            csord__Order__c ord=Orders[i];    
            ord.csordtelcoa__Opportunity__c=ListOpp[0].id;
            oldMap.put(ord.id,ord);    
            if(i==1){      
                ord.csord__Status2__c='Submitted';}
            else if(i==2){ord.csord__Status2__c='Completed';}
            else {ord.csord__Status2__c='Accepted';}
            if(ord.id==ordnewId){ord.Status__c='Completed';}
            ord.Subscription__c=subList[0].id;
            ord.ownerId=groups[0].id;    
            newOrd.add(ord);
            newMap.put(ord.id,ord);
        }
        update newOrd;
            AllOrderTriggersHandler allo = new AllOrderTriggersHandler();
            allo.beforeUpdate(newOrd,newMap,oldMap);
            //allo.validateInventoryStatusOnComplete(newOrd);
            allo.validateInventoryStatusOnComplete(newOrd,newMap);
            allo.cancelOrRejectOrders(newOrd);
            allo.afterInsert(newOrd,newMap);
        test.stopTest(); 
        
        system.assert(newOrd!=null);
        System.assertEquals(true,oldMap.get(newOrd[0].id).csordtelcoa__Opportunity__c!=null);
        //System.assertEquals('LIVE - AS DESIGNED',servupdate[0].inventory_status__c);
    } 
    @istest
    public static void testcatchblock()
    {
       AllOrderTriggersHandler allo1 = new AllOrderTriggersHandler();

      try{
          allo1.beforeInsert(null);
      }catch(Exception e){}
      try{
          allo1.afterInsert(null,null);
      }catch(Exception e){}
      try{
          allo1.beforeUpdate(null,null,null);
      }catch(Exception e){}
      try
      {
       allo1.afterUpdate(null,null,null); 
      }catch(Exception e){}
      try
      {
       allo1.updateCreatedByField(null); 
      }catch(Exception e){} 
      try
      {
       AllOrderTriggersHandler.pushStatusToActiveStep(null,null); 
      }catch(Exception e){}
      try
      {
       allo1.mapOrdersByType(null); 
      }catch(Exception e){}
      try
      {
       allo1.updateServicesAndCases(null,null); 
      }catch(Exception e){}
      try
      {
       allo1.cancelEarlyTerminationCalc(null,null); 
      }catch(Exception e){}
       try
      {
       allo1.updateTerminationDetailsOnOrderAndServices(null,null); 
      }catch(Exception e){}
      try
      {
       allo1.SetCancelledOrderFlagOnOpportunity(null,null); 
      }catch(Exception e){}
      try
      {
       AllOrderTriggersHandler.setsharing(null); 
      }catch(Exception e){}
      try
      {
        allo1.validateInventoryStatusOnComplete(null,null); 
      }catch(Exception e){}
      try
      {
        allo1.setSubscriptionID(null); 
      }catch(Exception e){}
       try
      {
        allo1.amendOrderToFOMExecute(null,null); 
      }catch(Exception e){}
      try
      {
        allo1.setCustomerRequiredDate(null); 
      }catch(Exception e){}
      try
      {
        allo1.cancelOrRejectOrders(null); 
      }catch(Exception e){}
      try
      {
        allo1.setContactDetails(null,null); 
      }catch(Exception e){}
      try
      {
       AllOrderTriggersHandler.updateOrderOrchestrationProcess(null,null); 
      }catch(Exception e){}
      try
      {
        allo1.setResellerAccountName(null); 
      }catch(Exception e){} 
      system.assertEquals(true,allo1!=null);      
    }
   
}