@isTest(SeeAllData = false)
private class CS_TIDZoneCustomLookupTest {

    private static testMethod void doLookupSearchtest() {
        
        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('TIDPostcode','ESACodeName');
         
        CS_TID_PostCode_ESACode_Zone__c postcode = new CS_TID_PostCode_ESACode_Zone__c();
        postcode.Name = 'TestPostCode';
        postcode.CS_TID_ESACode__c = 'TIDPostcode';
        postcode.CS_TID_Zone__c = 'ESACodeName';
        insert postcode;
        
        CS_TIDZoneCustomLookup  lookup = new CS_TIDZoneCustomLookup();
         
        Object[] obj = lookup.doDynamicLookupSearch(searchFields,'productDefinitionID');
        
        String str = lookup.getRequiredAttributes();
       list<CS_TID_PostCode_ESACode_Zone__c> postcode1 = [select id ,CS_TID_ESACode__c , CS_TID_Zone__c  from  CS_TID_PostCode_ESACode_Zone__c];
       system.assertEquals(postcode.CS_TID_ESACode__c  , postcode1[0].CS_TID_ESACode__c ); 
       system.assertEquals(postcode.CS_TID_Zone__c ,postcode1[0].CS_TID_Zone__c );  
    }

}