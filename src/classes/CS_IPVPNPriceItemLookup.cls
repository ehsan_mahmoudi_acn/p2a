global with sharing class CS_IPVPNPriceItemLookup  extends cscfga.ALookupSearch {
    
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){

        String cityID = searchFields.get('Pop City');
        String countryName = searchFields.get('Country Name');
        String accountID = searchFields.get('Account Id');
        String portSpeedID = searchFields.get('Port Speed');
        String IPVPNtype = searchFields.get('Port Type');
        String onnetOffnet = searchFields.get('Onnet or Offnet');
        String offnetProviderName = searchFields.get('Offnet Provider Name');
        System.Debug('CS_IPVPNPriceItemLookup : doDynamicLookupSearch searchFields' + searchFields);
        
        
        List<cspmb__Price_Item__c> cityData = new List<cspmb__Price_Item__c>();
        List<cspmb__Price_Item__c> data;
		System.Debug('CS_IPVPNPriceItemLookup : TransTasman if case:  ' + onnetOffnet +' --- '+countryName.toUpperCase());
        if(onnetOffnet == 'Offnet' && offnetProviderName.toUpperCase() == 'VODAFONE NZ' && (countryName.toUpperCase() == 'NEW ZEALAND' || countryName.toUpperCase() == 'NEWZEALAND')){
        	Map<String,Object> whereMap = new Map<String,Object>();
        	//searchFields.put('Port Type', 'TransTasman');
        	whereMap.put('Trans_Tasman_Type__c', searchFields.get('Type Of NNI'));
        	data = getPriceItems(searchFields, whereMap, true);
			System.Debug('CS_IPVPNPriceItemLookup : TransTasman data' + data);
        	return getAccountPricing(data, accountID);
        }

        if(IPVPNtype == 'BIP'){
        	Map<String,Object> whereMap = new Map<String,Object>();
        	String zoneId = searchFields.get('Zone');
        	if(zoneId != ''){
        		List<CS_BIP_Postcode_Zone__c> postcodezoneList = [SELECT Id, Name, CS_BIP_Zone__c FROM CS_BIP_Postcode_Zone__c WHERE Id = :zoneId];
        		if(!postcodezoneList.isEmpty()){
        			zoneId = postcodezoneList[0].CS_BIP_Zone__c;
        		}
        	}
        	whereMap.put('CS_BIP_Zone__c', zoneId);
        	//whereMap.put('CS_BIP_Postcode_Zone__c', searchFields.get('Zone'));
        	whereMap.put('BIP_Connection_Type__c', searchFields.get('Connection Type'));
        	data = getPriceItems(searchFields, whereMap, false);
			System.Debug('CS_IPVPNPriceItemLookup : BIP data' + data);
        	return getAccountPricing(data, accountID);
        }

        data = getPriceItems(searchFields, new Map<String,Object>(), false);
        
        System.Debug('CS_IPVPNPriceItemLookup : doDynamicLookupSearch data' + data);
        if(cityID != '' && cityID != null){
        	for(cspmb__Price_Item__c item : data){
        		if(item.City__c == cityID){
        			cityData.add(item);
        		}
        	}
        }
        System.Debug('CS_IPVPNPriceItemLookup : doDynamicLookupSearch cityData' + cityData);
        
        if(!cityData.isEmpty()){ data = cityData; }

        return getAccountPricing(data, accountID);
	}

	public override String getRequiredAttributes(){ 
	    return '["Connection Type","Product Type", "Account Id", "Offnet Provider Name","Port Type","Pop City","Port Country","Port Speed", "Account Customer Type", "Type Of NNI", "Onnet or Offnet", "Country Name", "Zone"]';
	}

	public List<cspmb__Price_Item__c> getAccountPricing(List<cspmb__Price_Item__c> data, String accountID){
		List<cspmb__Price_Item__c> returnData = new List<cspmb__Price_Item__c>();
		for(cspmb__Price_Item__c item : data){
			if(item.cspmb__Account__c == accountID){
				returnData.add(item);
				return returnData;
			} else if (item.cspmb__Account__c == null){
				returnData.add(item);
			}
		}
		return returnData;
	}
	public List<cspmb__Price_Item__c> getPriceItems(Map<String, String> attributesValues, Map<String,Object> wherePart, Boolean isTransTasman){
		
		String portType = attributesValues.get('Port Type') == 'Burstable' ? 'Standard' : attributesValues.get('Port Type');
		if(isTransTasman){
			portType = 'TransTasman';
		}
		String countryID = attributesValues.get('Port Country');
		String productType = attributesValues.get('Product Type');
		String portSpeed = attributesValues.get('Port Speed');
		String connectivity = attributesValues.get('Onnet or Offnet');
		String customerType = attributesValues.get('Account Customer Type');
		System.Debug('CS_IPVPNPriceItemLookup : getPriceItems attributesValues' + portType +' - '+ countryID +' - '+ portSpeed +' - '+ connectivity+' - '+ customerType);
		String query = 'SELECT Id, cspmb__One_Off_Cost__c,'+
							'cspmb__Recurring_Cost__c,'+
							'cspmb__One_Off_Charge__c,'+
							'cspmb__Recurring_Charge__c,'+
							'cspmb__Account__c,'+
							'Country__c,'+
							'City__c,'+
							'Critical_NRC__c,'+
							'Critical_MRC__c,'+
							'CS_Bandwidth_Product_Type__c,'+
							'Interactive_MRC__c,'+
							'Interactive_NRC__c,'+
							'IPVPN_Type__c,'+
							'cspmb__Is_Active__c,'+
							'Low_Priority_MRC__c,'+
							'Low_Priority_NRC__c,'+
							'Pricing_Segment__c,'+
							'Standard_MRC__c,'+
							'Standard_NRC__c,'+
							'Video_MRC__c,'+
							'Video_NRC__c,'+
							'Voice_MRC__c,'+
							'Voice_MRC_Cost__c,'+
							'Video_MRC_Cost__c,'+
							'Standard_MRC_Cost__c,'+
							'Low_Priority_MRC_Cost__c,'+
							'Interactive_MRC_Cost__c,'+
							'Critical_MRC_Cost__c,'+
							'Trans_Tasman_Type__c,'+
							'Connectivity__c,'+
							'BIP_Connection_Type__c,'+
							'Voice_NRC__c '+
						'FROM cspmb__Price_Item__c '+
        				'WHERE Country__c =:countryID'+
        					' AND CS_Bandwidth_Product_Type__c=:portSpeed'+
        					' AND cspmb__Is_Active__c= true'+
        					' AND IPVPN_Type__c =:portType'+
        					' AND Connectivity__c =:connectivity'+
        					' AND Pricing_Segment__c = :customerType'+
        					' AND Generic_Product_Type__c = :productType';

     	System.Debug('CS_IPVPNPriceItemLookup : getPriceItems query1: ' + query);
        String whereQuery = '';
        for(String key : wherePart.keySet()){
        	Object value = wherePart.get(key);

        	if(value instanceof String){
        		whereQuery += ' AND '+key+' = \''+value + '\'';
        	} else if (value instanceof Integer || value instanceof Boolean){
        		whereQuery += ' AND '+key+' = '+value;
        	}
        }
        query += whereQuery;
        System.Debug('CS_IPVPNPriceItemLookup : getPriceItems query2: ' + query);
		
        return Database.query(query);
	}
}