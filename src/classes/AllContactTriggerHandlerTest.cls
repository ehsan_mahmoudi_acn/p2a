@isTest
public class AllContactTriggerHandlerTest{
  static testmethod void createTIDocumentsmethod1()
  {
       Map<Id,Contact> newContactsMap = new Map<Id,Contact>();
        Map<Id,Contact> newContactsMaps = new Map<Id,Contact>();
        list<Contact> Conlist =new list<Contact>();
      
      List<Account> accounts = P2A_TestFactoryCls.getAccounts(1);
      List<Contact> Contacts = P2A_TestFactoryCls.getContact(1,accounts);
      List<Country_Lookup__c> countrys = P2A_TestFactoryCls.getcountry(1);
      List<Site__c> siteslist = P2A_TestFactoryCls.getsites(1,accounts,countrys);
      List<BillProfile__c> billProfiles = P2A_TestFactoryCls.getBPs(1,accounts,SitesList,Contacts);          
      list<Contact> Con =new list<Contact>();
      Profile p=[SELECT ID , name From Profile WHERE NAME = 'TI COMMERCIAL' AND (Name !='System Administrator' AND Name !='TI Marketing')  limit 1];  user usr =[Select id, name from User where ProfileId=:p.Id limit 1];
   
      Contact cons = new Contact();
      cons.LastName = 'Test';
      cons.accountid = accounts[0].id; 
      cons.OwnerId = usr.id;
      cons.Contact_Type__c = 'Billing';
      cons.Country__c = countrys[0].id;
      cons.Is_updated__c = true;
      cons.Is_Attached_to_Bill_Profile__c = true;
      cons.Primary_Contact__c = true;
      cons.Postal_Code__c = '12345';
      Con.add(cons);
      insert con;
      
      list<Contact> cont = [select id,LastName from Contact where LastName = 'Test'];
        system.assertEquals(con[0].LastName , cont[0].LastName);
        System.assert(con!=null);
      
      for(contact c :con){
        newContactsMap.put(c.id,c); 
      }
        
        
     try{
      
      AllContactTriggerHandler act = new AllContactTriggerHandler();      
      act.beforeUpdate(Con ,newContactsMap,newContactsMap);
      delete con;
      act.profPermToDelete(con,newContactsMap);
      } catch(Exception e){
      
      } 
    
     /*
      Contact conts = new Contact();
      conts.LastName = 'Test';
      conts.accountid = accounts[0].id; 
      conts.OwnerId = usr.id;
      conts.Contact_Type__c = 'Billing';
      conts.Country__c = countrys[0].id;
      conts.Is_updated__c = true;
      conts.Is_Attached_to_Bill_Profile__c = true;
      conts.Primary_Contact__c = true;
      conts.Postal_Code__c = '';
      Conlist.add(conts);
      insert conlist;
      list<Contact> con1 = [select id,LastName from Contact where LastName = 'Test'];
      //  system.assertEquals(conlist[0].LastName , con1[0].LastName);
        System.assert(conlist!=null);

      
      for(contact c :conlist){
        newContactsMaps.put(c.id,c); 
      }
     */
  
  }
}