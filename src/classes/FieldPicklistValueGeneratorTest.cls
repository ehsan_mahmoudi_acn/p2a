@isTest
private class FieldPicklistValueGeneratorTest {
    
    static testMethod void unittest() {
        
        CurrencyValue__c cv = new CurrencyValue__c();
        cv.CreatedDate = Datetime.now();
        cv.name = 'Currency';
        cv.Currency_Value__c = 45;
        insert cv;
        System.assertEquals(45,cv.Currency_Value__c );
        
        Blob blobValue = EncodingUtil.convertFromHex('4A4B4C');
        system.debug('--blobValue--'+blobValue);
        System.assertEquals('JKL', blobValue.toString());
         
        
        
        Schema.DescribeFieldResult SdFRcuurency = Account.Industry.getDescribe();
        
        Schema.DescribeFieldResult SdFRcuurency1 = Account.Name.getDescribe();
        Test.startTest();
            FieldPicklistValueGenerator fdvg = new FieldPicklistValueGenerator();  
            Boolean  b = fdvg.canGenerateValueFor(SdFRcuurency);
            object obj = fdvg.generate(SdFRcuurency);
            
            FieldPicklistValueGenerator fdvg1 = new FieldPicklistValueGenerator();  
            Boolean  b1 = fdvg1.canGenerateValueFor(SdFRcuurency1);
            object obj1 = fdvg1.generate(SdFRcuurency1);
        Test.stopTest();
    
    }
}