@isTest(seealldata=False)
public class U2CMasterCodePopulationTest {


/*static Account acc;
static Opportunity opp;
static Product2 prod;
static List<OpportunityLineItem> OpportunityLineItemList = new List<OpportunityLineItem> ();
static List<OpportunityLineItem> OpportunityLineItemList1 = new List<OpportunityLineItem> ();
static List<OPPLISortingWrapper> oPPLISortList = new List<OPPLISortingWrapper>();

        
    private static Product2 getProduct1(){
 
            Product2 prod1 = new Product2();
            prod1.name = 'Test Product';
            prod1.CurrencyIsoCode='EUR';
            prod1.Product_ID__c='GIDSECO'; 
            prod1.Is_Service__c=TRUE; 
            prod1.Usage_Based_Charge_Flag__c =FALSE;      
            prod1.Root_Product_Bill_Text__c= 'Global Internet Direct' ;
            
            insert prod1;
            return prod1;
          
     }
    private static Product2 getProduct2(){
 
            Product2 prod2 = new Product2();
            prod2.name = 'Test Product';
            prod2.CurrencyIsoCode='EUR';
            prod2.Product_ID__c='CUSTSITE';        
            prod2.Create_Resource__c=FALSE;
            prod2.Root_Product_Bill_Text__c= 'ROOTPRBT';
            
            insert prod2;
            return prod2;
          
     }       
    private Static PricebookEntry getPriceBook(){
       PricebookEntry pbe = new PricebookEntry();
        pbe.Pricebook2Id ='01s90000000KWfFAAW';
        pbe.Product2Id = getProduct2().Id;
        pbe.IsActive = true;
        pbe.UseStandardPrice = false;
        pbe.UnitPrice = 999999;
        insert pbe;
        return pbe;
    }
    
    private static Account getAccount(){
        acc = new Account();
      //  Country_Lookup__c cl = getCountry();
        acc.Name = 'Test Account';
        acc.Customer_Type__c = 'MNC';
      //  acc.Country__c = cl.Id;
        acc.Selling_Entity__c = 'Telstra INC';
        acc.Activated__c= true;
        acc.Account_Id__c ='12123';
        insert acc;
        return acc;
    }
    
    private static Opportunity getOpportunity(){
        opp = new Opportunity();
        Account a = getAccount();
        opp.Name = 'Test Opportunity';
        opp.AccountId = a.Id;
         //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
        opp.StageName = 'Identify & Define';
        opp.Action_Item_Created_SD__c=TRUE;
        opp.CloseDate = System.today();
        opp.Estimated_MRC__c=800;
        opp.Estimated_NRC__c=1000;
        opp.ContractTerm__c='10';
        insert opp;
        return opp;
        
    }          
    private static List<OpportunityLineItem> getOpportunityLineItem() {
   

        OpportunityLineItem oppli= new OpportunityLineItem();
        opp = getOpportunity();
        BillProfile__c b = getBillProfile();
        oppli.OpportunityId = opp.Id;
        oppli.IsMainItem__c = 'Yes';
        oppli.OrderType__c = 'Terminate';
        Product2 p1 = getProduct1();
        oppli.U2C_Master_Code__c = 'GIDSECO';
        oppli.Usage_Base_Flag__c = TRUE;
        oppli.Quantity = 1;
        oppli.UnitPrice = 10;
        oppli.CPQItem__c = '1.1';
        oppli.ParentCPQItem__c='1';
        oppli.PricebookEntryId = getPriceBook().Id;
        oppli.ContractTerm__c = '12'; 
        oppli.Bundle_Label_Name__c = 'test';
        OpportunityLineItemList.add(oppli);
        insert OpportunityLineItemList;
        return  OpportunityLineItemList;
        
        OpportunityLineItem oppli1= new OpportunityLineItem();
        opp = getOpportunity();
        BillProfile__c b1 = getBillProfile();
        oppli1.OpportunityId = opp.Id;
        oppli1.IsMainItem__c = 'No';
        oppli1.OrderType__c = 'New Provide';
        oppli1.OrderType__c = 'Downgrade - Provide & Cease';
        oppli1.OrderType__c = 'Upgrade - Provide & Cease';
        Product2 p2 = getProduct2();
        oppli1.Usage_Base_Flag__c = TRUE;
        oppli1.U2C_Master_Code__c = 'GIDSECO:CUSTSITE';
        //oppli.Root_Product_Id__c = 'GIDSECO';
        oppli1.Quantity = 1;
        oppli1.UnitPrice = 10;
        oppli1.CPQItem__c = '1';
        oppli1.PricebookEntryId = getPriceBook().Id;
        oppli1.ContractTerm__c = '12'; 
        oppli1.ParentCPQItem__c=null;
        OpportunityLineItemList.add(oppli1);
        insert OpportunityLineItemList1;
        return OpportunityLineItemList1;
       
       
   }
   
    // Bill Profile data
    
         private static BillProfile__c getBillProfile() {
        
            BillProfile__c bp = new BillProfile__c();
            bp.Name = 'testbill';
            bp.Account__c = getAccount().Id;
            bp.Activated__c = false;
            bp.Bill_Profile_Site__c = getSite1().Id;
            bp.Billing_Entity__c = 'Telstra Corporation';
            bp.CurrencyIsoCode = 'GBP';
            bp.Payment_Terms__c = '30 Days';
            bp.Start_Date__c = Date.today();
            bp.Invoice_Delivery_Method__c = 'Postal';
            bp.Postal_Delivery_Method__c = 'First Class';
            bp.Invoice_Breakdown__c = 'Summary Page';
            //bp.Billing_Contact__c = getContact().id;
            bp.Billing_Frequency__c = 'Monthly';
            bp.Status__c = 'Approved';
            bp.Payment_Method__c = 'Manual';
            bp.Minimum_Bill_Amount__c = 10.20;
            bp.First_Period_Date__c = date.today();
            bp.Rating_Rounding_Level__c = 2;
            bp.Bill_Decimal_Point__c = 0;
            bp.FX_Group__c = 'Corporate';
            bp.FX_Rule__c = 'FX Rate at end of period';
            insert bp;
            return bp;
    }
    
     
     private static City_Lookup__c getCity(){
        City_Lookup__c city = new City_Lookup__c();
        city.Generic_Site_Code__c = 'X123';
        city.name = 'Mumbai';
        city.City_Code__c='MUM';
        city.OwnerID = userinfo.getuserid();
        insert city;
       return city;
     
    }
    private static City_Lookup__c getCity1(){
        City_Lookup__c city = new City_Lookup__c();
        city.Generic_Site_Code__c = 'N123';
        city.name = 'Delhi';
        city.City_Code__c='DL';
        city.OwnerID = userinfo.getuserid();
        insert city;
       return city;
     
    }
     private static Site__c getSite(){
        Site__c site = new Site__c(); 
        site.Name='Test Site1';  
       site.AccountId__c =getAccount().id;
       site.City_Finder__c=getCity().id;
       site.Country_Finder__c=getCountry().id;
       site.Address1__c='Address11';
       insert site;
       return site;
     }
       private static Site__c getSite1(){
        Site__c site = new Site__c(); 
        site.Name='Test Site11';  
       site.AccountId__c =getAccount().id;
       site.City_Finder__c=getCity1().id;
       site.Country_Finder__c=getCountry1().id;
       site.Address1__c='Address11';
       insert site;
       return site;
     }
          private static Country_Lookup__c getCountry(){
        Country_Lookup__c c2 = new Country_Lookup__c();
       c2.Country_Code__c = 'ENG';
        insert c2;
        return c2;
    } 
    
     private static Country_Lookup__c getCountry1(){
        Country_Lookup__c c2 = new Country_Lookup__c();
       c2.Country_Code__c = 'NZ';
        insert c2;
        return c2;
    } 
      
 
 static testmethod void Mastercodepopulation(){
 U2CMasterCodePopulation.populateU2CMasterCode(OpportunityLineItemList);
 U2CMasterCodePopulation.populateU2CMasterCode(OpportunityLineItemList1);
 
}*/
}