@isTest(SeeAllData = false)
private class AddressCheckTest {

    private static List<CS_Country__c> countryList;
    private static List<CS_City__c> cityList;
    private static List<CS_POP__c> popList;
    
    private static String city;
    private static String country;
    
    private static void initTestData(){
        countryList = new List<CS_Country__c>{
            new CS_Country__c(Name = 'Croatia'),
            new CS_Country__c(Name = 'Hong Kong')
        };
        
        insert countryList;
        System.assertEquals('Croatia',countryList[0].Name );   
        
        cityList = new List<CS_City__c>{
            new CS_City__c(Name = 'Zagreb', CS_Country__c = countryList[0].Id),
            new CS_City__c(Name = 'Hong Kong', CS_Country__c = countryList[1].Id)
        };
        
        insert cityList;
        System.assertEquals('Zagreb',cityList[0].Name );   
        
        popList = new List<CS_POP__c>{
            new CS_POP__c(Name = 'POP 1', CS_Country__c = countryList[0].Id, CS_City__c = cityList[0].Id, IPVPN_Critical_Data__c = true),
            new CS_POP__c(Name = 'POP 2', CS_Country__c = countryList[1].Id, CS_City__c = cityList[1].Id, IPVPN_Critical_Data__c = true)
        };
        
        insert popList;
         System.assertEquals('POP 1',popList[0].Name ); 
        
        city = cityList[0].Name;
        country = countryList[0].Name;
    }
    
    private static testMethod void getAddressesTest() {
    Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
             
            initTestData();
            
            String addresses = AddressCheck.getAddresses(city, country);

            System.debug('**** Addresses: ' + addresses);
            System.assert(addresses.contains(city), '');
            
        } catch(Exception e){
            ee = e;
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
    }

}