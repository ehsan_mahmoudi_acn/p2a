@isTest(SeeAllData=false)
public class CreditCardCheckActionItemTest{

Static Account acc1 = getAccount();

static opportunity opp1=getOpportunity();

  private static Account getAccount(){                
        Account acc = new Account();            
        Country_Lookup__c c = getCountry();            
        acc.Name = 'Test Account';            
        acc.Customer_Type__c = 'MNC';            
        acc.Country__c = c.Id;            
        acc.Selling_Entity__c = 'Telstra INC';  
        acc.Activated__c= true;  
        acc.Account_ID__c = '9090';  
        acc.Customer_Legal_Entity_Name__c = 'Test' ;     
        insert acc;       
        system.assertEquals(acc.Name , 'Test Account');
        return acc;  
          
   } 
   private static Country_Lookup__c getCountry(){        
           Country_Lookup__c country = new Country_Lookup__c();            
           country.CCMS_Country_Code__c = 'HK';            
           country.CCMS_Country_Name__c = 'India';            
           country.Country_Code__c = 'HK';            
           insert country;        
           system.assert(country!=null);
           return country;    
         } 
           private static Opportunity getOpportunity(){                 
        Opportunity opp = new Opportunity();            
        //Account acc = getAccount();           
        opp.Name = 'Test Opportunity';            
        opp.AccountId = acc1.Id;   
         //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement         
        opp.StageName = 'Identify & Define' ;  
        opp.Stage__c= 'Identify & Define' ;          
        opp.CloseDate = System.today();   
        opp.Opportunity_Type__c = 'Complex';
        opp.Estimated_MRC__c=8;
            opp.Estimated_NRC__c=2;
            opp.ContractTerm__c='1';
            opp.Order_Type__c='New';
        //opp.Stage_Gate_1_Action_Created__c = true;         
        //opp.Approx_Deal_Size__c = 9000;                 
        system.assertEquals(opp.Name , 'Test Opportunity');
        return opp;    
       } 
       private static cscfga__Product_Basket__c  getProductbasket(){
      cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c ();
      pb.cscfga__Opportunity__c =opp1.id;
      pb.csordtelcoa__Synchronised_with_Opportunity__c =true;
      pb.csbb__Synchronised_With_Opportunity__c =true;
     system.assert(pb!=null);
     return pb;  
       }
    
    static testMethod void previousCreditcheckupdationTest() {
       
       List<Global_Constant_Data__c> globalConstantDataObjList = new List<Global_Constant_Data__c>();       
       Global_Constant_Data__c globalConstantDataObj;
       
       // providing the values to custom setting object
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Status';
       globalConstantDataObj.Value__c = 'Assigned';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Priority';
       globalConstantDataObj.Value__c = 'High';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       insert globalConstantDataObjList;
        List<Action_Item__c> previousActionItem = new List<Action_Item__c>();
           Action_Item__c action = new Action_Item__c();
           List<Action_Item__c> lastAction= [Select Id,Name,Previous_MRC__c,Previous_NRC__c,Account_Number__c,Opportunity__c,Status__c,Feedback__c,Account__r.Account_ID__c,LastModifiedDate,MRC_Monthly_Recurring_Charges__c,NRC_Non_Recurring_Charges__c,Total_Estimated_MRC__c,Total_Estimated_NRC__c from Action_Item__c where Id =: globalConstantDataObjList[0].id];
         //action.Previous_Credit_Check_Date__c=Date.newInstance(lastAction.get(0).LastModifiedDate.year(),lastAction.get(0).LastModifiedDate.month(),lastAction.get(0).LastModifiedDate.day());
                       if(lastAction != null && lastAction.size()>0){
                            action.Action_Item_ID_Previous_Credit_Check__c=lastAction[0].Name;                    
                            action.Previous_MRC__c=lastAction[0].MRC_Monthly_Recurring_Charges__c;
                            action.Previous_NRC__c=lastAction[0].NRC_Non_Recurring_Charges__c;
                            action.Previous_Estimated_MRC__c=lastAction[0].Total_Estimated_MRC__c;
                            action.Previous_Estimated_NRC__c=lastAction[0].Total_Estimated_NRC__c;
                            } 
                        previousActionItem.add(action);
                        insert previousActionItem;      
                        system.assert(previousActionItem!=null);
        
       
        Opportunity opportunityObj = getOpportunity();
        insert opportunityObj;
        system.assert(opportunityObj!=null);
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
        system.assert(cntryLkupObj!=null);
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                        Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                        Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                        Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
        insert accObj;
        system.assert(accObj!=null);
        Contact conObj = DataFactoryTest.createContact('Bill','Profile',accobj,'Billprofile',cntryLkupObj,true);
        
         BillProfile__c billProfileObject = getBillProfile(accObj,conObj);
       
        Action_Item__c objActionItem= new Action_Item__c();
        objActionItem.Bill_Profile__c = billProfileObject.Id;
        objActionItem.Account__c =  accObj.Id;
        objActionItem.Due_Date__c = System.Today() + 2;  
        objActionItem.Subject__c = 'Update Contract Documents';
        objActionItem.Comments__c = 'Payment Terms and Billing Frequency has changed, please update the contract documents (if any).';
        objActionItem.Opportunity__c = opportunityObj.Id;
        insert objActionItem;
        system.assert(objActionItem!=null);
        Set<Opportunity> oppSet = new Set<Opportunity>();
        oppSet.add(opportunityObj);
        OpportunityUpdate oppObjUpdate = new OpportunityUpdate();
        oppObjUpdate.BillProfileActivation(oppSet);
        oppObjUpdate.BillProfileApproval(oppSet);
        oppObjUpdate.CreditcheckCreation(oppSet);
        oppObjUpdate.PreviousCreditcheckupdation(oppSet);
        //OpportunityUpdate.doAdd();
        // OpportunityUpdate.getcount();
        // OpportunityUpdate.updateAccountContactSites();
        
         CreditCardCheckActionItem cr= new CreditCardCheckActionItem();
         Test.startTest(); 
         cr.CreditcheckCreation(oppSet);
         cr.PreviousCreditcheckupdation(oppSet);
        
         Test.stopTest();
      
    
  }
    
     private static BillProfile__c getBillProfile(Account accobj,Contact conobj){
          
     City_Lookup__c cityObj = new City_Lookup__c(Name ='Bangalore',City_Code__c='Beijing',
                                                    Generic_Site_Code__c ='HK#');
    insert cityObj;
    
    System.assertEquals('Bangalore',cityObj.Name ); 
     
      Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
    insert cntryLkupObj;
    
    System.assertEquals('HK',cntryLkupObj.Name ); 
     
     Site__c siteObj = new Site__c(Name='Test_site',Address1__c='43',Address2__c='Bangalore',Country_Finder__c=cntryLkupObj.Id,
                             City_Finder__c=cityObj.Id,AccountId__c=accObj.Id,Address_Type__c='Billing Address');
    insert siteObj;
    
    System.assertEquals('Test_site',siteObj.Name);
    
        BillProfile__c BProfObj = new BillProfile__c();
        BProfObj.Account__c = siteObj.AccountId__c;
        BProfObj.Bill_Profile_Site__c = siteObj.Id;
        BProfObj.Billing_Entity__c = 'Telstra International Limited';
        BProfObj.Payment_Terms__c = '45 Days';
        BProfObj.Billing_Frequency__c = 'Hong Kong Annual Voice';
        BProfObj.Billing_Contact__c = conobj.id;
        BProfObj.Activated__c = true;
        BProfObj.Status__c = 'Active';
        BProfObj.Approved__c = FALSE;
        insert BProfObj;
        system.assert(BProfObj!=null);
        return BProfObj;
     }   
     
}