/**
        @author - Accenture
        @date - 4-JULY-2013
        @description - This class contains the sequence of actions for the Solution triggers. 
    */ 
public class SolutionSequenceController{
/**
        @author - Accenture
        @date - 4-JULY-2013
        @description - This method carries out the defined action for the Insert Solution Triggers. 
    */ 
    
        
   public void appendSolutionNameAccountId (List<Solutions__c> lst_solutions){ 
       
       set<Id> Account_id = new set<Id>();
       Map<Id,String> AccountID = new Map<Id,String>();
      
      for(Solutions__c solns: lst_solutions){
          
          //Account_id.add(solns.Account_Name__c);
         
      } 
       
       
      //List<Account> Accobj = [select Id,Account_ID__c from Account where Id in : Account_id ];
       
      for(Account Acc : [select Id,Account_ID__c from Account where Id in : Account_id ]){
          
         AccountID.put(Acc.Id,Acc.Account_ID__c);
          
      
      }
      
      for(Solutions__c solns: lst_solutions){
          
          if(!AccountID.isEmpty()){
              //if(AccountID.get(solns.Account_Name__c)!= null){
                  //solns.Solution_Name__c = AccountID.get(solns.Account_Name__c) + ' '+ solns.Solution_Name__c;
              //}
           }
      
      }
   
   }
    
    
}