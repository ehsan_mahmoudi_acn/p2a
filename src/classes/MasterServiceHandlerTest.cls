@isTest(SeeAllData=false)
public class MasterServiceHandlerTest {
    
    private static Map<Id, cscfga__Product_Configuration__c> configMap = null;
    
    /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
     // System.unexpected exception , the reason behind commenting the lines
     
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        } else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        } else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }

    private static void createTestData(Boolean multipleBaskets) {
        
        cscfga__Product_Category__c productCategory = new cscfga__Product_Category__c(Name = 'Test category');
        insert productCategory;
       
        
        List<String> definitionIds= new List<String>(MasterServiceLogic.productToSolutionTypeMap.keySet());
        
        List<cscfga__Product_Basket__c> baskets = new List<cscfga__Product_Basket__c>();
        //for (Integer i = 0; i < (multipleBaskets ? 2 : 1); i) {
            baskets.add(new cscfga__Product_Basket__c());
       // }
        insert baskets;

        List<cscfga__Product_Configuration__c> configs = new List<cscfga__Product_Configuration__c>();
        //for (Integer i = 0; i < (multipleBaskets ? 2 : 1); i ) {
            configs.add(new cscfga__Product_Configuration__c(Name = 'Test master config ' 
                , cscfga__Product_Definition__c = definitionIds[0]
                , cscfga__Product_Basket__c = baskets[0].Id));
        //}
        insert configs;
        
        
        configMap = new Map<Id, cscfga__Product_Configuration__c>();
        //for (Integer i = 0; i < (multipleBaskets ? 2 : 1); i ) {
            configMap.put(configs[0].Id, configs[0]);
        //}
    }

    private static void tryToCreateTestData(Boolean multipleBaskets) {
        Exception ee = null;
        try {
            disableAll(UserInfo.getUserId());
            createTestData(multipleBaskets);
        } catch(Exception e) {
            ee = e;
        } finally {
            enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
    
    private static testMethod void testOneBasketCall() {
       P2A_TestFactoryCls.sampleTestData();
        tryToCreateTestData(false);
        
        Test.startTest();
        MasterServiceHandler.process(configMap);
        Test.stopTest();
        
        /*List<cscfga__Product_Configuration__c> configs = [select Id
                , Name
                , cscfga__Product_Basket__c
                , Master_IPVPN_Configuration__c 
            from cscfga__Product_Configuration__c 
            where Id in :configMap.keySet() and Master_IPVPN_Configuration__c <> null];*/
            
        for (cscfga__Product_Configuration__c config: [select Id, Name, cscfga__Product_Basket__c, Master_IPVPN_Configuration__c from cscfga__Product_Configuration__c where Id in :configMap.keySet() and Master_IPVPN_Configuration__c <> null]) {
            system.assert(config.Master_IPVPN_Configuration__c != null, 'Master havent been initialized!');
        }
    }
    /*
    private static testMethod void testMultipleBasketCall() {
        P2A_TestFactoryCls.sampleTestData();
        tryToCreateTestData(true);
        
        Test.startTest();
        MasterServiceHandler.process(configMap);
        Test.stopTest();
        
        List<cscfga__Product_Configuration__c> configs = [select Id
                , Name
                , cscfga__Product_Basket__c
                , Master_IPVPN_Configuration__c 
            from cscfga__Product_Configuration__c 
            where Id in :configMap.keySet() and Master_IPVPN_Configuration__c <> null];
        
        for (cscfga__Product_Configuration__c config: [select Id, Name, cscfga__Product_Basket__c, Master_IPVPN_Configuration__c from cscfga__Product_Configuration__c where Id in :configMap.keySet() and Master_IPVPN_Configuration__c <> null]) {
            system.assert(config.Master_IPVPN_Configuration__c != null, 'Master havent been initialized!');
        }
    }
	*/
}