/**
    @author - Accenture
    @date - 26- Jun-2012
    @version - 1.0
    @description - This is the test class for QuoteAfterInsert and QuoteAfterUpdate Trigger.
*/
@isTest(SeeAllData = false)
private class QuoteAfterInsertUpdateTest {
/*commented for test class coverage
    static Account acc;
    static Opportunity opp;
    static Country_Lookup__c cl;
    static Site__c site;
    static City_Lookup__c ci;
    
    static testMethod void getContractDetails(){
        List<OpportunityLineItem> oli = new List<OpportunityLineItem>();
        Quote__c quote = getQuote();
        oli = getOpportunityLineItem();
       
        //insert oli;        
        //insert quote;
        
        quote.Primary__c = true;
        //update quote;
        Test.startTest();
        //opp = [SELECT ContractTerm__c FROM Opportunity WHERE Id =: opp.Id];
       // system.assertEquals(opp.ContractTerm__c, '48');
        
                
        Test.stopTest();
    }
    private static Quote__c getQuote(){
        Quote__c q = new Quote__c();
        opp = getOpportunity();
        q.Opportunity__c = opp.Id;
        q.Name = 'Test Quote';
        return q;
    }
    private static List<OpportunityLineItem> getOpportunityLineItem(){
        
        List<OpportunityLineItem> lst = new List<OpportunityLineItem>();
        
        OpportunityLineItem oli = new OpportunityLineItem();
        opp = getOpportunity();
        BillProfile__c b = getBillProfile();
        oli.OpportunityId = opp.Id;
        oli.IsMainItem__c = 'Yes';
        oli.OrderType__c = 'New Provide';
        oli.Quantity = 5;
        oli.UnitPrice = 10;
        oli.is_GCPE_Shared_with_Multiple_Services__c = 'NA';
        oli.PricebookEntryId = getPriceBookEntry('Global IPVPN').Id;
        oli.ContractTerm__c = '12';
        oli.cpqitem__c='1';
        lst.add(oli);
        
        OpportunityLineItem oppLineItem = new OpportunityLineItem();
        oppLineItem.OpportunityId = opp.Id;
        oppLineiTem.IsMainItem__c = 'No';
        oppLineItem.OrderType__c = 'Renew';
        oppLineItem.Quantity = 5;
        oppLineItem.UnitPrice = 10;
        oppLineItem.BillProfileId__c = null;
        oppLineItem.PricebookEntryId = getPriceBookEntry('EPL').Id;
        oppLineItem.ContractTerm__c = '24';
        
        lst.add(oppLineItem);
        
        OpportunityLineItem oppLineItem1 = new OpportunityLineItem();
        BillProfile__c b1 = getBillProfile();
        oppLineItem1.OpportunityId = opp.Id;
        oppLineiTem1.IsMainItem__c = 'Yes';
        oppLineItem1.OrderType__c = 'New Provide';
        oppLineItem1.Quantity = 5;
        oppLineItem1.UnitPrice = 10;
        oppLineItem1.BillProfileId__c = b1.Id;
        oppLineItem1.PricebookEntryId = getPriceBookEntry('IPL').Id;
        oppLineItem1.ContractTerm__c = '48';
        
        lst.add(oppLineItem1);
        
        return lst;
    }
    private static Opportunity getOpportunity(){
        if(opp == null) {
            opp = new Opportunity();
            acc = getAccount();
            opp.Name = 'Test Opportunity';
            opp.AccountId = acc.Id;
             //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
            opp.StageName = 'Identify & Define';
            opp.Stage__C='Identify & Define';
            opp.CloseDate = System.today();
            opp.Estimated_MRC__c=800;
            opp.Estimated_NRC__c=1000;
            opp.ContractTerm__c='10';
         //   opp.Approx_Deal_Size__c = 9000;
            insert opp;
        }
        return opp;
    }
    private static Pricebook2 getPriceBook(String prodName){
        Pricebook2 p = [SELECT Id FROM Pricebook2 where IsStandard = true LIMIT 1];
        return p;   
    }
    
    private static Product2 getProduct(String prodName){
        Product2 prod = new Product2();
        prod.Name = prodName;
        prod.ProductCode = prodName;
        prod.Product_ID__c ='EPL';
        insert prod;
        return prod;
    }
    
    private static PricebookEntry getPriceBookEntry(String prodName){
        PricebookEntry p = new PricebookEntry();
        p.Pricebook2Id = getPriceBook(prodName).Id;
        p.Product2Id =  getProduct(prodName).Id;
        p.UnitPrice = 2000;
        p.IsActive = true;
        insert p; 
        return p;    
    }
    
    
    
    private static BillProfile__c getBillProfile(){
        BillProfile__c b = new BillProfile__c();
        acc = getAccount();
       // b.Bill_Profile_Number__c = 'Test Bill Profile';
        b.Billing_Entity__c = 'Telstra Limited';
        b.Account__c = acc.Id;
        b.Start_Date__c = date.today();        
        b.Invoice_Breakdown__c = 'Summary Page';        
        b.First_Period_Date__c = date.today();
        site = getSite();
        b.Bill_Profile_Site__c = site.Id;
        insert b;
        return b;
    }
    
    private static Account getAccount(){
        if(acc == null){    
            acc = new Account();
            Country_Lookup__c c = getCountry();
            acc.Name = 'Test Account Test 1';
            acc.Customer_Type__c = 'MNC';
            acc.Country__c = c.Id;
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Activated__c = True;
            acc.Account_Status__c = 'Active';
            acc.Account_ID__c = '9090';
            acc.Customer_Legal_Entity_Name__c='Test';
            insert acc;
        }
        return acc;
    }
    private static Country_Lookup__c getCountry(){
        if(cl == null){ 
            cl = new Country_Lookup__c();
            cl.CCMS_Country_Code__c = 'SG';
            cl.CCMS_Country_Name__c = 'India';
            cl.Country_Code__c = 'SG';
            insert cl;
        }
        return cl;
    } 
private static Site__c getSite(){
    if(site== null){
        site= new Site__c();
        acc = getAccount();
        site.Name = 'Test_site';
        site.Address1__c = '43';
        site.Address2__c = 'Bangalore';
        cl = getCountry();
        site.Country_Finder__c = cl.Id;
        ci = getCity();
        site.City_Finder__c = ci.Id;
        site.AccountId__c =  acc.Id; 
        site.Address_Type__c = 'Billing Address';
        insert site;
    }
    return site;
}  

private static City_Lookup__c getCity(){
    if(ci == null){
        ci = new City_Lookup__c();
        ci.City_Code__c ='MUM';
        ci.Name = 'MUMBAI';
        insert ci;
    }
    return ci;
}
 */
   
}