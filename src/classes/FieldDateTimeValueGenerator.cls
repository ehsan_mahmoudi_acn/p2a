public class FieldDateTimeValueGenerator implements FieldValueGenerator {

    private Integer offsetInHours;

    public FieldDateTimeValueGenerator(Integer offsetInHours) {
        this.offsetInHours = offsetInHours;
    }

    public FieldDateTimeValueGenerator() {
        this.offsetInHours = 0;
    }
    
    public Boolean canGenerateValueFor(Schema.DescribeFieldResult fieldDesc) {
        return Schema.DisplayType.DATETIME == fieldDesc.getType();
    }
    
    public Object generate(Schema.DescribeFieldResult fieldDesc) {
        DateTime result = DateTime.now();
        result.addHours(this.offsetInHours);
        return result;
    }
}