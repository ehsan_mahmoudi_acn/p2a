@isTest(seealldata = false)
public class AddEVPLPortTest{

    @isTest    
    public static void unitTest() {
    try{    
        P2A_TestFactoryCls.sampleTestData();
    }catch(Exception e){
     ErrorHandlerException.ExecutingClassName='AddEVPLPortTest:UnitTest';         
            ErrorHandlerException.sendException(e);
    }
     Set<Id> bsktIdList = new Set<Id>();
     List<Account> accList = P2A_TestFactoryCls.getAccounts(1);  
     Map<ID,cscfga__Product_Configuration__c> pcMap = new Map<ID,cscfga__Product_Configuration__c>();
     Map<Id,Id> aPortMap = new Map<Id,Id>();
     Map<Id,Id> zPortMap = new Map<Id,Id>();
     
    List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
    List<Country_Lookup__c> countrylist =  P2A_TestFactoryCls.getcountry(1);
    List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
    List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
    List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
    List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
    List<CS_Bandwidth__c> ListBandwidth = P2A_TestFactoryCls.getBandwidths(1); 
    List<cscfga__Product_Configuration__c> Listproconfigs = P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);        
        Listproconfigs[0].Name = 'EVPL';
        Listproconfigs[0].cscfga__Description__c = 'EVPL';
        Listproconfigs[0].cscfga__Configuration_Status__c = 'Incomplete';                    
        //Listproconfigs[0].cscfga__Product_Basket__c = prodBaskList[0].id;
        Listproconfigs[0].AddVlan__c = 'Yes';
        Listproconfigs[0].csordtelcoa__Replaced_Product_Configuration__c = null;
        Listproconfigs[0].CurrencyIsoCode = 'USD';
    update Listproconfigs;
    list<Opportunity> opp = [select id,Name from Opportunity where Name = 'EVPL'];
                                //system.assertEquals(Listproconfigs[0].name , opp[0].name);
                                System.assert(Listproconfigs!=null); 
    
     bsktIdList.add(prodBaskList[0].id);
    system.assertEquals(Listproconfigs[0].CurrencyIsoCode,'USD');

    List<cscfga__Configuration_Screen__c> configLst = P2A_TestFactoryCls.getConfigScreen(1,ProductDeflist);
    List<cscfga__Screen_Section__c> ssList1 = P2A_TestFactoryCls.getScreenSec(1,configLst);
    List<cscfga__Attribute_Definition__c> attrdef = P2A_TestFactoryCls.getAttributesdef(3,Listproconfigs,ProductDeflist,ConfigLst,ssList1);
    
    List<cscfga__Attribute_Definition__c> attrdefList = new List<cscfga__Attribute_Definition__c>();
    cscfga__Attribute_Definition__c  Attributesdef1 = new cscfga__Attribute_Definition__c(Name='POP__c',cscfga__Product_Definition__c=ProductDeflist[0].id,cscfga__Configuration_Screen__c=configLst[0].id,cscfga__Screen_Section__c=ssList1[0].id,cscfga__Column__c=2,cscfga__Row__c=4);
        attrdefList.add(Attributesdef1);
    cscfga__Attribute_Definition__c  Attributesdef2 = new cscfga__Attribute_Definition__c(Name='Contract Term',cscfga__Product_Definition__c=ProductDeflist[0].id,cscfga__Configuration_Screen__c=configLst[0].id,cscfga__Screen_Section__c=ssList1[0].id,cscfga__Column__c=2,cscfga__Row__c=4);
        attrdefList.add(Attributesdef2);
    cscfga__Attribute_Definition__c  Attributesdef3 = new cscfga__Attribute_Definition__c(Name='Bandwidth',cscfga__Product_Definition__c=ProductDeflist[0].id,cscfga__Configuration_Screen__c=configLst[0].id,cscfga__Screen_Section__c=ssList1[0].id,cscfga__Column__c=2,cscfga__Row__c=4);
        attrdefList.add(Attributesdef3);
    cscfga__Attribute_Definition__c  Attributesdef4 = new cscfga__Attribute_Definition__c(Name='Add NID',cscfga__Product_Definition__c=ProductDeflist[0].id,cscfga__Configuration_Screen__c=configLst[0].id,cscfga__Screen_Section__c=ssList1[0].id,cscfga__Column__c=2,cscfga__Row__c=4);
        attrdefList.add(Attributesdef4);
    cscfga__Attribute_Definition__c  Attributesdef5 = new cscfga__Attribute_Definition__c(Name='EVP Port Type',cscfga__Product_Definition__c=ProductDeflist[0].id,cscfga__Configuration_Screen__c=configLst[0].id,cscfga__Screen_Section__c=ssList1[0].id,cscfga__Column__c=2,cscfga__Row__c=4);
        attrdefList.add(Attributesdef5);
    cscfga__Attribute_Definition__c  Attributesdef6 = new cscfga__Attribute_Definition__c(Name='EVPL PC Id',cscfga__Product_Definition__c=ProductDeflist[0].id,cscfga__Configuration_Screen__c=configLst[0].id,cscfga__Screen_Section__c=ssList1[0].id,cscfga__Column__c=2,cscfga__Row__c=4);
        attrdefList.add(Attributesdef6);
    cscfga__Attribute_Definition__c  Attributesdef7 = new cscfga__Attribute_Definition__c(Name='NonStandardContractTerm',cscfga__Product_Definition__c=ProductDeflist[0].id,cscfga__Configuration_Screen__c=configLst[0].id,cscfga__Screen_Section__c=ssList1[0].id,cscfga__Column__c=2,cscfga__Row__c=4);
        attrdefList.add(Attributesdef7);
    
    insert attrdefList;
    
    List<cscfga__Product_Configuration__c> Listproconfigs1 = P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
   // Listproconfigs1[0].EvplId__c = 'a4kO00000004reHYAS';
    Listproconfigs1[0].Name = 'Z End Port';
    Listproconfigs1[0].cscfga__Description__c = 'Z End Port';
    Listproconfigs1[0].cscfga__Configuration_Status__c = 'Incomplete';                    
    Listproconfigs1[0].cscfga__Product_Basket__c = prodBaskList[0].id;
    Listproconfigs1[0].cscfga__Quantity__c = 1;
    Listproconfigs1[0].EVPLAEndPop__c = 'HKAT';
    Listproconfigs1[0].CurrencyIsoCode = 'USD';
    upsert Listproconfigs1[0]; 
    
    system.assertEquals(Listproconfigs1[0].EVPLAEndPop__c,'HKAT');
    
    
    Test.startTest(); 
    cscfga__Product_Configuration__c pb = new cscfga__Product_Configuration__c();
    pb.name ='A End Port';    
    pb.EvplId__c = Listproconfigs[0].EvplId__c; 
    pb.cscfga__Product_basket__c = prodBaskList[0].id ;
    pb.CurrencyIsoCode = 'USD';
    pb.EvplVlan_Transparent__c = 'undefined';
    pb.AddVlan__c = 'Yes';
    pb.EVPLAEndPop__c = 'PTHP';
    pb.EVPLZEndPop__c = 'HKAT';
    Pb.csordtelcoa__Replaced_Product_Configuration__c = Listproconfigs[0].id;
    //pb.EvplId__c = Listproconfigs[0].RecordID__c;
    pb.cscfga__Contract_Term__c = 12;
    pb.Customer_Required_Date__c = Date.newInstance(2017,02,04);
    pb.NNI_bandwidth__c = ListBandwidth[0].id;
    pb.Add_NID__c = 'No';
    pb.cscfga__Product_Bundle__c = Pbundle[0].id;
    pb.cscfga__Product_Definition__c = ProductDeflist[0].id ;
    pb.Product_Name__c = 'test product';
    Insert pb;
    
    
    list<cscfga__Product_Configuration__c > prod_con = [select id,Name from cscfga__Product_Configuration__c where Name = 'test product'];
                                //system.assertEquals(pb.name , prod_con[0].name);
                                System.assert(pb!=null); 
    
    cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
    pd.name = 'Master IPVPN Service';
    pd.cscfga__Description__c ='112312331';
    insert pd;
    
    system.assertEquals(pb.EVPLAEndPop__c , 'PTHP');

    cscfga__Product_Configuration__c pb1 = new cscfga__Product_Configuration__c();
    pb1.name ='Z End Port';    
    pb1.EvplId__c = Listproconfigs[0].RecordID__c;
    pb1.cscfga__Product_Definition__c =pd.id;
    pb1.cscfga__Product_basket__c = prodBaskList[0].id ;
    pb1.CurrencyIsoCode = 'USD';
    //RecordID__c = 'a24O0000000jbKI';
    pb1.EvplVlan_Transparent__c = 'undefined';
    pb1.AddVlan__c = 'Yes';
    pb1.EVPLAEndPop__c = 'PTHP';
    pb1.EVPLZEndPop__c = 'HKAT';
    Pb1.csordtelcoa__Replaced_Product_Configuration__c = Listproconfigs1[0].id;
    pb1.EvplId__c = Listproconfigs1[0].id;
    pb1.cscfga__Contract_Term__c = 12;
    pb1.Customer_Required_Date__c = Date.newInstance(2017,02,04);
    pb1.NNI_bandwidth__c = ListBandwidth[0].id;
    pb1.Add_NID__c = 'No';
    pb1.cscfga__Product_Bundle__c = Pbundle[0].id;
    pb1.cscfga__Product_Definition__c = ProductDeflist[0].id ;
    pb1.Product_Name__c = 'test product';
    Insert pb1;
    list<cscfga__Product_Configuration__c > prod_conf = [select id,Name from cscfga__Product_Configuration__c where Name = 'test product'];
                                //system.assertEquals(pb1.name , prod_conf[0].name);
                                System.assert(pb1!=null);  
    
    System.assertEquals(pb1.AddVlan__c , 'Yes');
    List<cscfga__Product_Definition__c> prodDef1 = [SELECT id,Name FROM cscfga__Product_Definition__c where Name='EVP Port' limit 1];
    Set<ID> setStudentId = new Set<ID>();
    for(cscfga__Product_Definition__c stu:prodDef1){
            setStudentId.add(stu.Id);
        }
    
    Id a = Listproconfigs[0].id;
        if(a!= null) {
        a = null;
    }

    cscfga__Product_Configuration__c pb2 = new cscfga__Product_Configuration__c();
    pb2.name ='EVPL';    
    pb2.EvplId__c = Listproconfigs[0].EvplId__c;
    pb2.cscfga__Product_basket__c = prodBaskList[0].id ;
    pb2.CurrencyIsoCode = 'USD';
    //pb2.RecordID__c = 'a24O0000000jbKI';
    pb2.EvplVlan_Transparent__c = 'undefined';
    pb2.AddVlan__c = 'No';
    pb2.EVPLAEndPop__c = 'PTHP';
    pb2.EVPLZEndPop__c = 'HKAT';
    pb2.csordtelcoa__Replaced_Product_Configuration__c = a;
    pb2.EvplId__c = Listproconfigs1[0].id;
    pb2.cscfga__Contract_Term__c = 12;
    pb2.Customer_Required_Date__c = Date.newInstance(2017,02,04);
    pb2.NNI_bandwidth__c = ListBandwidth[0].id;
    pb2.Add_NID__c = 'No';
    pb2.cscfga__Product_Bundle__c = Pbundle[0].id;
    pb2.cscfga__Product_Definition__c = ProductDeflist[0].id ;
    pb2.Product_Name__c = 'test product';
    Insert pb2; 
    
    System.assertEquals(pb2.csordtelcoa__Replaced_Product_Configuration__c , a);
    
    pcMap.put(pb2.id,pb2);
    aPortMap.put(pb.EvplId__c,pb.id);
    zPortMap.put(pb.EvplId__c,pb1.id);

    Id pb2null = pb2.id;
        if(pb2null!= null) {
        pb2null = null;
    }
    
    cscfga__Attribute__c Attributes = new cscfga__Attribute__c(name = 'POP__c',cscfga__Product_Configuration__c=pb.id,cscfga__Value__c = 'Test',cscfga__Attribute_Definition__c =attrdefList[0].id);
            insert Attributes;
    
    
      Map<Id, Id> oldEVPLAEndPort = new Map<Id, Id>();
      Map<Id, Id> oldEVPLZEndPort = new Map<Id, Id>();
      for(cscfga__Product_Configuration__c pcs : Listproconfigs1){
          oldEVPLAEndPort.put(pcs.id,pcs.id);
          oldEVPLZEndPort.put(pcs.id,pcs.id); 
      }
      String AttrDefStr = System.Label.EVPLPortProdLabel;
      Id AttrDefStrId = Id.valueOf(AttrDefStr);
      Date CRD = System.today();
     // Id id1=attId('EVP Port Type', AttrDefStrId);
     //AddEVPLPort.addEVPPorts(bsktIdList);
      try{
         AddEVPLPort.addEVPPorts(bsktIdList);      
       
      }catch(Exception e){
       ErrorHandlerException.ExecutingClassName='AddEVPLPortTest:UnitTest';         
            ErrorHandlerException.sendException(e);
      }
      List<cscfga__Attribute_Definition__c>checkattrDef=[select id from cscfga__Attribute_Definition__c where Name =:attrdefList[0].name and cscfga__Product_Definition__c =:Attributesdef1.cscfga__Product_Definition__c];
      System.assertNotEquals(null,checkattrDef);
      AddEVPLPort.evpPortPOP(pb.id,'test','VPLS',attrdefList[0].cscfga__Product_Definition__c,7,CRD,'11','Test',pb2.id);
      AddEVPLPort.evpPortsAttrbt(pcMap,aPortMap,zPortMap);
      
      
    Test.stopTest();
                         
    }
    public static Id attId(String attName, Id attProdDefId){
        cscfga__Attribute_Definition__c attId = [select id from cscfga__Attribute_Definition__c where Name =:attName and cscfga__Product_Definition__c =:attProdDefId];
        return attId.Id;
    }  
    
    
    public static void rplcOldEVPLPC(List<cscfga__Product_Configuration__c> listPC, Map<Id, Id> oldEVPLAEndPort, Map<Id, Id> oldEVPLZEndPort)
    {

        List<cscfga__Product_Configuration__c> evpPC = new List<cscfga__Product_Configuration__c>();
        Map<Id, Id> rplcdEVPLMapList = new Map<Id, Id>();
            
        for(cscfga__Product_Configuration__c pc :listPC){
            
            if(pc.csordtelcoa__Replaced_Product_Configuration__c != null && pc.Name == 'EVPL'){
                rplcdEVPLMapList.put(oldEVPLAEndPort.get(pc.csordtelcoa__Replaced_Product_Configuration__c), pc.Id);
                rplcdEVPLMapList.put(oldEVPLZEndPort.get(pc.csordtelcoa__Replaced_Product_Configuration__c), pc.Id);
            }
        }

        for(cscfga__Product_Configuration__c pc :listPC){
            
            if(pc.EvplId__c != null && oldEVPLAEndPort.get(pc.EvplId__c) == pc.Id){
                pc.EvplId__c = rplcdEVPLMapList.get(pc.Id);
                evpPC.add(pc);
            }
            
            if(pc.EvplId__c != null && oldEVPLZEndPort.get(pc.EvplId__c) == pc.Id){
                pc.EvplId__c = rplcdEVPLMapList.get(pc.Id);
                evpPC.add(pc);
            }

        }

        if(evpPC.size() > 0){
            update evpPC;
            list<cscfga__Product_Configuration__c > prod_c= [select id,Name from cscfga__Product_Configuration__c where Name = 'EVPL'];
                                system.assertEquals(evpPC[0].name , prod_c[0].name);
                                System.assert(evpPC!=null); 
          
           
        }       
    }
    @ istest
    public static void unittest4()
    {
        Map<ID,cscfga__Product_Configuration__c> pcMap = new Map<ID,cscfga__Product_Configuration__c>();
     Map<Id,Id> aPortMap = new Map<Id,Id>();
     Map<Id,Id> zPortMap = new Map<Id,Id>();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        P2A_TestFactoryCls.sampleTestData();
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
        List<User> users = P2A_TestFactoryCls.get_Users(1);
        List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
    List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        List<CS_Bandwidth__c> ListBandwidth = P2A_TestFactoryCls.getBandwidths(1); 
    List<cscfga__Configuration_Screen__c> configLst = P2A_TestFactoryCls.getConfigScreen(1,ProductDeflist);
    List<cscfga__Screen_Section__c> ssList1 = P2A_TestFactoryCls.getScreenSec(1,configLst);
    List<cscfga__Product_Configuration__c> Listproconfigs = P2A_TestFactoryCls.getProductonfig(1, Products,ProductDeflist,Pbundle,Offerlists);  
    
    List<cscfga__Attribute_Definition__c> attrdefList = new List<cscfga__Attribute_Definition__c>();
    cscfga__Attribute_Definition__c  Attributesdef7 = new cscfga__Attribute_Definition__c(Name='POP__c',cscfga__Product_Definition__c=ProductDeflist[0].id,cscfga__Configuration_Screen__c=configLst[0].id,cscfga__Screen_Section__c=ssList1[0].id,cscfga__Column__c=2,cscfga__Row__c=4);
        attrdefList.add(Attributesdef7);    
    insert attrdefList;
    list<cscfga__Attribute_Definition__c> att = [select id,Name from cscfga__Attribute_Definition__c where Name = 'POP__c'];
                                system.assertEquals(attrdefList[0].name ,att[0].name);
                                System.assert(attrdefList!=null); 
    
    
        cscfga__Product_Configuration__c pb = new cscfga__Product_Configuration__c();
            pb.name ='A End Port';    
            pb.EvplId__c = Listproconfigs[0].EvplId__c;
            pb.cscfga__Product_basket__c = Products[0].id ;
            pb.CurrencyIsoCode = 'USD';
            pb.EvplVlan_Transparent__c = 'undefined';
            pb.AddVlan__c = 'Yes';
            pb.EVPLAEndPop__c = 'PTHP';
            pb.EVPLZEndPop__c = 'HKAT';
            Pb.csordtelcoa__Replaced_Product_Configuration__c = Listproconfigs[0].id;
            pb.EvplId__c = Listproconfigs[0].RecordID__c;
            pb.cscfga__Contract_Term__c = 12;
            pb.Customer_Required_Date__c = Date.newInstance(2017,02,04);
            pb.NNI_bandwidth__c = ListBandwidth[0].id;
            pb.Add_NID__c = 'No';
            pb.cscfga__Product_Bundle__c = Pbundle[0].id;
            pb.cscfga__Product_Definition__c = ProductDeflist[0].id ;
            pb.Product_Name__c = 'test product';
            Insert pb;
            list<cscfga__Product_Configuration__c> prod = [select id,Name from cscfga__Product_Configuration__c where Name = 'test product'];
                                //system.assertEquals(pb.name , prod[0].name);
                                System.assert(pb!=null); 
        
        cscfga__Product_Configuration__c pb2 = new cscfga__Product_Configuration__c();
            pb2.name ='EVPL';    
            pb2.EvplId__c = Listproconfigs[0].EvplId__c;
            pb2.cscfga__Product_basket__c = Products[0].id ;
            pb2.CurrencyIsoCode = 'USD';
            //pb2.RecordID__c = 'a24O0000000jbKI';
            pb2.EvplVlan_Transparent__c = 'undefined';
            pb2.AddVlan__c = 'No';
            pb2.EVPLAEndPop__c = 'PTHP';
            pb2.EVPLZEndPop__c = 'HKAT';
            pb2.csordtelcoa__Replaced_Product_Configuration__c = Listproconfigs[0].id;
            pb2.EvplId__c = Listproconfigs[0].id;
            pb2.cscfga__Contract_Term__c = 12;
            pb2.Customer_Required_Date__c = Date.newInstance(2017,02,04);
            pb2.NNI_bandwidth__c = ListBandwidth[0].id;
            pb2.Add_NID__c = 'No';
            pb2.cscfga__Product_Bundle__c = Pbundle[0].id;
            pb2.cscfga__Product_Definition__c = ProductDeflist[0].id ;
            pb2.Product_Name__c = 'test product';
            Insert pb2; 
            list<cscfga__Product_Configuration__c> proc = [select id,Name from cscfga__Product_Configuration__c where Name = 'test product'];
                                //system.assertEquals(pb2.name , proc[0].name);
                                System.assert(pb2!=null);
        
        cscfga__Attribute__c Attributes = new cscfga__Attribute__c(name = 'POP__c',cscfga__Product_Configuration__c=pb.id,cscfga__Value__c = 'Test',cscfga__Attribute_Definition__c =attrdefList[0].id);
            insert Attributes;
            list<cscfga__Attribute__c > attt = [select id,Name from cscfga__Attribute__c where Name = 'POP__c'];
                                system.assertEquals(Attributes.name , attt[0].name);
                                System.assert(Attributes!=null); 
        
        pcMap.put(pb2.id,pb2);
        aPortMap.put(pb.EvplId__c,pb.id);
        zPortMap.put(pb.EvplId__c,pb.id);       
        
        Test.startTest();
        AddEVPLPort.evpPortsAttrbt(pcMap,aPortMap,zPortMap);
        //AddEVPLPort.evpPortPOP(pcId,'test','VPLS',attProdDefId,6,CRD,'11','Test',EVPLPCId);
        Test.stopTest();
    }       
    
    @istest
    public static void unittest6()
    {
    Test.startTest();
    P2A_TestFactoryCls.sampleTestData();
     List<Account> ListAcc  = P2A_TestFactoryCls.getAccounts(1);
    List<Opportunity> ListOpp = P2A_TestFactoryCls.getOpportunitys(1,ListAcc);       
    List<cscfga__Product_basket__c> Products =  P2A_TestFactoryCls.getProductBasketHdlr(1,ListOpp); 
    List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<cscfga__Configuration_Offer__c> offer= P2A_TestFactoryCls.getOffers(1);        
        List<cscfga__Product_Bundle__c> productbundleList= P2A_TestFactoryCls.getProductBundleHdlr(1,ListOpp);
        List<cscfga__Product_Definition__c> prodDefList= P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Configuration_Screen__c> configLst = P2A_TestFactoryCls.getConfigScreen(1,prodDefList);
        List<cscfga__Screen_Section__c> ssList1 = P2A_TestFactoryCls.getScreenSec(1,configLst);
        List<cscfga__Product_Configuration__c> pcList= P2A_TestFactoryCls.getProductonfig(4,Products,prodDefList,productbundleList,offer); 
    List<cscfga__Attribute_Definition__c> attrdefList = new List<cscfga__Attribute_Definition__c>();
    cscfga__Attribute_Definition__c  Attributesdef1 = new cscfga__Attribute_Definition__c(Name='POP__c',cscfga__Product_Definition__c=prodDefList[0].id,cscfga__Configuration_Screen__c=configLst[0].id,cscfga__Screen_Section__c=ssList1[0].id,cscfga__Column__c=2,cscfga__Row__c=4);
        attrdefList.add(Attributesdef1);
    insert attrdefList;
       system.assertEquals(true,pcList!=null);
       Map<Id, cscfga__Product_Configuration__c> pcMap1 = new Map<Id, cscfga__Product_Configuration__c>();
        Map<Id, cscfga__Product_Configuration__c> pcMap2 = new Map<Id, cscfga__Product_Configuration__c>();
       Map<Id, Id> oldEVPLAEndPort = new  Map<Id, Id> ();
       Map<Id, Id> oldEVPLZEndPort = new Map<Id, Id> ();
        pcMap1.put(pcList[0].id,pcList[0]);
        oldEVPLAEndPort.put(pcList[0].id,pcList[0].id);
        oldEVPLZEndPort.put(pcList[0].id,pcList[0].id);
        AddEVPLPort.rplcOldEVPLPC(pcList,oldEVPLAEndPort,oldEVPLZEndPort);
                
        pcList[0].EvplId__c=pcList[1].id;
        pcList[0].Name='A End Port';        
        pcList[0].EVPLAEndPop__c=pcList[1].id;
        pcList[1].Customer_Required_Date__c=System.today();
        pcList[1].Name='EVPL';
        pcList[1].csordtelcoa__Replaced_Product_Configuration__c=pcList[2].id;
        update pcList;
        pcMap2.put(pcList[1].Id,pcList[1]);
        System.assertEquals('EVPL',pcList[1].Name);
        
        System.assertNotEquals(null,pcList[1].Customer_Required_Date__c);
        List<cscfga__Attribute__c> attList = new List<cscfga__Attribute__c>();        
        cscfga__Attribute__c Attributes6 = new cscfga__Attribute__c(name = 'Customer_Required_Date_c',cscfga__Product_Configuration__c=pcList[0].id,cscfga__Value__c = 'Test',cscfga__Display_Value__c='Test',cscfga__Attribute_Definition__c =attrdefList[0].id);
        attList.add(Attributes6);
        cscfga__Attribute__c Attributes1 = new cscfga__Attribute__c(name = 'POP__c',cscfga__Product_Configuration__c=pcList[1].id,cscfga__Value__c = 'Test',cscfga__Attribute_Definition__c =attrdefList[0].id);
        attList.add(Attributes1);
        cscfga__Attribute__c Attributes2 = new cscfga__Attribute__c(name = 'Bandwidth',cscfga__Product_Configuration__c=pcList[2].id,cscfga__Value__c = 'Test',cscfga__Attribute_Definition__c =attrdefList[0].id);
        attList.add(Attributes2);
        cscfga__Attribute__c Attributes3 = new cscfga__Attribute__c(name = 'Add NID',cscfga__Product_Configuration__c=pcList[3].id,cscfga__Value__c = 'Test',cscfga__Attribute_Definition__c =attrdefList[0].id);
        attList.add(Attributes3);
        cscfga__Attribute__c Attributes4 = new cscfga__Attribute__c(name = 'Contract Term',cscfga__Product_Configuration__c=pcList[0].id,cscfga__Value__c = 'Test',cscfga__Attribute_Definition__c =attrdefList[0].id);
        attList.add(Attributes4);
        cscfga__Attribute__c Attributes5 = new cscfga__Attribute__c(name = 'NonStandardContractTerm',cscfga__Product_Configuration__c=pcList[0].id,cscfga__Value__c = 'Test',cscfga__Attribute_Definition__c =attrdefList[0].id);
        attList.add(Attributes5);
                
        insert attList;
        
        AddEVPLPort.evpPortsAttrbt(pcMap1,oldEVPLAEndPort,oldEVPLZEndPort);
        AddEVPLPort.rplcOldEVPLPC(pcList,oldEVPLAEndPort,oldEVPLZEndPort);
        
        System.assertNotEquals(null,pcList[0].EvplId__c);
        System.assertNotEquals(null,pcList[0].Name);
        
        try{AddEVPLPort.rplcOldEVPLPC(null,null,null);}catch(Exception e){}
        try{AddEVPLPort.evpPortsAttrbt(null,null,null);}catch(Exception e){}
        
        Test.stopTest();
    }      
    
}