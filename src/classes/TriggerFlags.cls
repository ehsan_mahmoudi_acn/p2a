public class TriggerFlags {
    public static Boolean NoProductConfigurationTriggers = false;
    public static Boolean NoProductBasketTriggers = false;
    public static Boolean NoOpportunityTriggers = false;
    public static Boolean NoOrderTriggers = false;
    public static Boolean NoServiceTriggers = false;
    public static Boolean NoSubscriptionTriggers = false;
    public static Boolean NoAttributeTriggers= false;
    public static Boolean NobillProfileUPdate = false;
    public static Boolean NoOpportunitySplitTriggers = false;
	public static Boolean NoConfigurationRequestTriggerHandler = false;
	public static Boolean NoOpportunitySplitValError = false;
	public static Boolean NoSPT = true;
}