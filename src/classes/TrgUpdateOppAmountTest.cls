@isTest
private class TrgUpdateOppAmountTest {

         private static final String contractTerm = '12';
         private static final Decimal nrcPrice = 10.00;
         private static final Decimal mrcPrice = 10.00;

  /*  static testMethod void myUnitTest() {
         
       // List<Opportunity> oppList = new List<Opportunity>();
       // Opportunity o= getOpportunity();
       getOpportunity();
       // oppList.add(o);
        //insert oppList;
    }*/
    
    private static Opportunity getOpportunity() {
        Opportunity o = new Opportunity();
        o.Name = 'UnitTest Opp';
        o.AccountId = UnitTestHelper.getAccount().Id;
         //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
        o.StageName = 'Identify & Define';
        o.Stage__c='Identify & Define';
        o.CloseDate = System.today();
        o.Total_MRC__c = mrcPrice;
        o.Total_NRC__c = nrcPrice;
        o.ContractTerm__c = contractTerm;
        o.QuoteStatus__c = 'Budgetary Quote';
        insert o;
        system.assert(o!=null);
        system.assertEquals(o.Name , 'UnitTest Opp');
        return o;
    }
    
    private static PricebookEntry getProduct() {
        
        Product2 p = new Product2();
        p.Name = 'IP VPN';
        p.ProductCode = 'G-VPN';
        p.IsActive = true;

        insert p;
        
        Pricebook2 pb = [SELECT Id FROM Pricebook2 WHERE IsStandard = true][0];
        
        PricebookEntry pbe = new PricebookEntry();
        pbe.Pricebook2Id = pb.Id;
        pbe.Product2Id = p.Id;
        pbe.IsActive = true;
        pbe.UseStandardPrice = false;
        pbe.UnitPrice = 999999;
        
        insert pbe;
        system.assert(pbe!=null);
        
        return pbe;
    }   
    
    private static OpportunityLineItem getOppLineItem(Opportunity o, PricebookEntry pbe) {
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = o.Id;
        oli.Quantity = 1;
        oli.TotalPrice = 100;
        oli.PricebookEntryId = pbe.Id;
        oli.Description = 'Unit Test 001';
        oli.BillProfileId__c = null;
        oli.ProductLeadTime__c = 0;
        oli.OrderType__c = 'New Provide';
        oli.IsMainItem__c = 'Yes';
        oli.CPQItem__c = '1';
        oli.ContractTerm__c = contractTerm;
        oli.NetNRCPrice__c = nrcPrice;
       // oli.NetMRCPrice__c = mrcPrice;
        insert oli;
        system.assert(oli!=null);
        return oli;
    }
}