/**
    This class used for Sorting
*/
global class OPPLITreeSorting{
    public string Position_Edit; // it equals to CPQItem;
    //Define other attributes
    public OpportunityLineItem oPPli;
    public OPPLITreeSorting(String iCPQItem,OpportunityLineItem ioli){
        Position_Edit= iCPQItem;
        oPPli = ioli;
    }
}