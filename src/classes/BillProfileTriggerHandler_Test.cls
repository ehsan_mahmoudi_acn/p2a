@isTest
private with Sharing class BillProfileTriggerHandler_Test{
    
    @testSetup
    private static void setupTestData() {
        list<PACNET_Entities__c> pacnetEntities = new List<PACNET_Entities__c>{
                        new PACNET_Entities__c(Name = 'Entity123', PACNET_Entity__c = 'Asia Netcom Pacnet (Ireland) Limited')
        };
        insert(pacnetEntities);

        List<Country_Lookup__c> countryList =  P2A_TestFactoryCls.getcountry(1); 
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Contact> contList = P2A_TestFactoryCls.getContact(1, accList);
        List<Site__c> siteList = P2A_TestFactoryCls.getsites(1, accList, countryList);
    }

    // Note by Hengky 2017-11-17: This unit test was not written properly
    // To Do: 
    // 1. Bulkify the code, 
    // 2. Remove @testVisible annotation from the main class
    // 3. Remove direct call to trigger event method.

    @isTest
    private static void BillProfileTriggerHandlrMethod() {

        List<Global_Constant_Data__c> gclist = [select id,name from Global_Constant_Data__c where name = 'Action Item Priority'];
        System.assert(gclist != null);

        List<Account> accList = [SELECT Name, Account_ID__c FROM Account];
        List<Contact> contList = [SELECT FirstName, LastName, Email FROM Contact];
        List<Site__c> siteList = [SELECT Name FROM Site__c];

        List<BillProfile__c>  billProfList = P2A_TestFactoryCls.getBPs(1, accList ,siteList,contList);
        List<Action_Item__c> inActIteList = P2A_TestFactoryCls.getActIteList(1);
        Map<Id,BillProfile__c> billProfMap = new Map<Id,BillProfile__c>();
        Map<Id,BillProfile__c> billProfMap1 = new Map<Id,BillProfile__c>();
        Map<Id,BillProfile__c> billProfMap2 = new Map<Id,BillProfile__c>();
        
        inActIteList[0].Bill_Profile__c = billProfList[0].id;
        //inActIteList[0].status__c = 'Assigned';
        update inActIteList;
        
        accList[0].Customer_Type__c='ISO';
        update accList;
        
        System.assert(inActIteList!=null);
        System.assertEquals('ISO',accList[0].Customer_Type__c);     
        
        BillProfile__c bp = new BillProfile__c();
        bp.id = billProfList[0].id;
        bp.Payment_Terms__c = billProfList[0].Payment_Terms__c;
        
        billProfMap2.put(bp.id, bp);
        
        billProfList[0].Payment_Terms__c = '15 Days';
        billProfList[0].Activated__c = true;
        billProfList[0].Display_Data_Itemisation__c=false;      
        billProfList[0].Bill_Profile_ORIG_ID_DM__c='dfsdfs';
        RecordType rt = [select Id,Name from RecordType where Name='ISO Bill Profile RT'];
        billProfList[0].RecordTypeId=rt.id;
        billProfList[0].E_bill_Email_Billing_Address_CC__c='fsdfsd@acc.com';
        billProfList[0].CDR_Email_Address__c='fsdfsd@acc.com';
        billProfList[0].CDR_Email_Address_CC__c='fsdfsd@acc.com';
        billProfList[0].E_bill_Email_Billing_Address__c='fsdfsd@acc.com';
        update billProfList;
        System.assert(billProfList[0].Itemisation_Display_Level__c == null);
        System.assert(billProfList!=null);
        System.assert(billProfList[0].Bill_Profile_ORIG_ID_DM__c!=null);
        
        
        for(BillProfile__c billProf : billProfList)
        {
            billProfMap.put(billProf.Id,billProf);          
        }
        BillProfileTriggerHandler  billProfTrgHdlr = new BillProfileTriggerHandler();
        Test.startTest();
        billProfTrgHdlr.beforeUpdate(billProfList, billProfMap, billProfMap);
        billProfTrgHdlr.updateContractDocuments(billProfList, billProfMap2);
        //billProfTrgHdlr.updateItemisationDisplayLevel(billProfList, billProfMap);
        try{
            billProfTrgHdlr.updateItemisationDisplayLevel(billProfList, billProfMap1);
        }catch(Exception e){}
        
        Test.stopTest();        
    }

    // Test inserting Bill Profile for Account which owned by a user with Region__c = 'EMEA'.
    // Expected result: An Action Item record is created with its OwnerId set to a queue 'Billing - EMEA'
    @isTest
    private static void createActionItemOnBillProfileInsert() {
        List<Global_Constant_Data__c> gclist = [SELECT id,name from Global_Constant_Data__c where name = 'Action Item Priority'];
        System.assert(gclist!=null);

        List<Account> accList = [SELECT Name, Account_ID__c FROM Account];
        List<Contact> contList = [SELECT FirstName, LastName, Email FROM Contact];
        List<Site__c> siteList = [SELECT Name FROM Site__c];

        List<User> users = P2A_TestFactoryCls.get_Users(1);
        for (User u : users) {
            u.Region__c = 'EMEA';
        }
        update(users);

        // change the account owner as the above users
        for (Account acc : accList) {
            acc.OwnerId = users[0].Id;
        }
        update(accList);

        Test.startTest();
        List<BillProfile__c> billProfList = P2A_TestFactoryCls.getBPs(1, accList, siteList, contList);
        Test.stopTest();

        List<Action_Item__c> actItems = [SELECT Id, OwnerId FROM Action_Item__c WHERE Bill_Profile__c IN :billProfList];
        System.assertEquals(actItems.size(), billProfList.size(), 'Error: Action item is not created when inserting Bill profile.');

        Map<String, QueueSobject> queueMap = QueueUtil.getQueuesMapForObject(Action_Item__c.SObjectType);
        for (Action_Item__c ai : actItems) {
            System.assertEquals(queueMap.get('Billing - EMEA').QueueId, ai.OwnerId, 'ERROR: Action item owner id is not set to Billing - EMEA.');
        }
    }

    // Test inserting Bill Profile which has ex-Pacnet Billing Entity.
    // Expected result: An Action Item record is created with its OwnerId set to a queue 'Billing - PACNET'
    @isTest
    private static void createActionItemOnBillProfileInsertPacnetEntity() {
        List<Global_Constant_Data__c> gclist = [SELECT id,name from Global_Constant_Data__c where name = 'Action Item Priority'];
        System.assert(gclist!=null);

        List<PACNET_Entities__c> pacnetEntities = PACNET_Entities__c.getall().values();
        System.assert(pacnetEntities.size() == 1);

        List<Account> accList = [SELECT Name, Account_ID__c FROM Account];
        List<Contact> contList = [SELECT FirstName, LastName, Email FROM Contact];
        List<Site__c> siteList = [SELECT Name FROM Site__c];

        List<BillProfile__c> billProfList = P2A_TestFactoryCls.getBPs(1, accList, siteList, contList);

        List<BillProfile__c> billProfListTest = billProfList.clone();
        for (BillProfile__c bp : billProfListTest) {
            bp.Id = null;
            bp.Billing_Entity__c = pacnetEntities[0].PACNET_Entity__c;
        }

        Test.startTest();
        insert(billProfListTest);
        Test.stopTest();

        List<Action_Item__c> actItems = [SELECT Id, OwnerId FROM Action_Item__c WHERE Bill_Profile__c IN :billProfList];
        System.assertEquals(actItems.size(), billProfListTest.size(), 'Error: Action item is not created when inserting Bill profile.');

        Map<String, QueueSobject> queueMap = QueueUtil.getQueuesMapForObject(Action_Item__c.SObjectType);
        for (Action_Item__c ai : actItems) {
            System.assertEquals(queueMap.get('Billing - PACNET').QueueId, ai.OwnerId, 'ERROR: Action item owner id is not set to Billing - PACNET.');
        }
    } 

    // Test inserting Bill Profile for Account which owned by a user with invalid Region__c value.
    // Expected result: An Action Item record is created with its OwnerId set to queue 'Billing - Australia'
    @isTest
    private static void createActionItemOnBillProfileInsertAustralia() {
        List<Global_Constant_Data__c> gclist = [SELECT id,name from Global_Constant_Data__c where name = 'Action Item Priority'];
        System.assert(gclist!=null);

        List<Account> accList = [SELECT Name, Account_ID__c FROM Account];
        List<Contact> contList = [SELECT FirstName, LastName, Email FROM Contact];
        List<Site__c> siteList = [SELECT Name FROM Site__c];

        List<User> users = P2A_TestFactoryCls.get_Users(1);
        for (User u : users) {
            u.Region__c = 'Random Region';
        }
        update(users);

        // change the account owner as the above users
        for (Account acc : accList) {
            acc.OwnerId = users[0].Id;
        }
        update(accList);

        Test.startTest();
        List<BillProfile__c> billProfList = P2A_TestFactoryCls.getBPs(1, accList ,siteList,contList);
        Test.stopTest();

        List<Action_Item__c> actItems = [SELECT Id, OwnerId FROM Action_Item__c WHERE Bill_Profile__c IN :billProfList];
        System.assertEquals(actItems.size(), billProfList.size(), 'Error: Action item is not created when inserting Bill profile.');

        Map<String, QueueSobject> queueMap = QueueUtil.getQueuesMapForObject(Action_Item__c.SObjectType);
        for (Action_Item__c ai : actItems) {
            System.assertEquals(queueMap.get('Billing - Australia').QueueId, ai.OwnerId, 'ERROR: Action item owner id is not set to Billing - Australia.');
        }


    }   

}