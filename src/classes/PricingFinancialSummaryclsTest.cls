@istest(seealldata = false)
Public class PricingFinancialSummaryclsTest
{
    public static List<User> usrlist=P2A_TestFactoryCls.get_Users(1);
    public static List<Account> acclist = P2A_TestFactoryCls.getAccounts(1);
    public static List<Opportunity> opplist = P2A_TestFactoryCls.getOpportunitys(1,acclist);
    public static List<cscfga__Product_Basket__c> prodbasktlist = P2A_TestFactoryCls.getProductBasketHdlr(1,opplist);
    public static List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
    public static List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
    public static List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
    public static List<case> caselist=  P2A_TestFactoryCls.getcase(1,acclist);
    
    public static testmethod void pricingFinancialSummaryclsmtd()
    {
        P2A_TestFactoryCls.sampleTestData();
        
        system.runas(usrlist[0])
        {   
            List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1,prodbasktlist,ProductDeflist,Pbundle,Offerlists);
            proconfigs[0].Product_Id__c='VLV';
            update proconfigs[0];
            system.assert(proconfigs[0].Product_Id__c!=null);
            
            system.assert(proconfigs[0].id!=null);
            case cobj = new case();
            Cobj.Product_Basket__c =prodbasktlist[0].id;
            insert cobj;
            System.assert(Cobj.id != null);    
            
            Decimal maxcontractterm = 0; 
            
            Pricing_Approval_Request_Data__c P_Request = new Pricing_Approval_Request_Data__c();
            P_Request.Approved_NRC__c=0;
            P_Request.Approved_RC__c=0;
            P_Request.Offer_NRC__c=0;
            P_Request.Offer_RC__c =0;
            P_Request.Cost_RC__c = 0;
            P_Request.Cost_NRC__c = 0;  
            P_Request.Contract_Term__c = 10;
            P_Request.Is_offnet__c = false;
            P_Request.Rate_RC__c = 0;
            P_Request.Product_Basket__c = cobj.Product_Basket__c;
            P_Request.Product_Configuration__c = proconfigs[0].id;
            insert P_Request ;
            System.assert(P_Request.id != null);
            
            test.startTest();
            PricingFinancialSummarycls prSumObj = new PricingFinancialSummarycls(new ApexPages.StandardController(Cobj));
            Boolean b = prSumObj.getisVisible();
            prSumObj.initlize();
            test.stoptest();
        }
    }
    public static testmethod void pricingFinancialSummaryclsmtdGcpe()
    {
        P2A_TestFactoryCls.sampleTestData();
        
        system.runas(usrlist[0])
        {   
            List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1,prodbasktlist,ProductDeflist,Pbundle,Offerlists);
            proconfigs[0].Product_Id__c='GCPE';
            update proconfigs[0];
            system.assert(proconfigs[0].Product_Id__c!=null);
            
            system.assert(proconfigs[0].id!=null);
            case cobj = new case();
            Cobj.Product_Basket__c =prodbasktlist[0].id;
            insert cobj;
            System.assert(Cobj.id != null);    
            
            Decimal maxcontractterm = 0; 
            
            Pricing_Approval_Request_Data__c P_Request = new Pricing_Approval_Request_Data__c();
            P_Request.Approved_NRC__c=0;
            P_Request.Approved_RC__c=0;
            P_Request.Offer_NRC__c=0;
            P_Request.Offer_RC__c =0;
            P_Request.Cost_RC__c = 0;
            P_Request.Cost_NRC__c = 0;  
            P_Request.Contract_Term__c = 10;
            P_Request.Is_offnet__c = false;
            P_Request.Rate_RC__c = 0;
            P_Request.Product_Basket__c = cobj.Product_Basket__c;
            P_Request.Product_Configuration__c = proconfigs[0].id;
            insert P_Request ;
            System.assert(P_Request.id != null);
            
            test.startTest();
            PricingFinancialSummarycls prSumObj = new PricingFinancialSummarycls(new ApexPages.StandardController(Cobj));
            Boolean b = prSumObj.getisVisible();
            prSumObj.initlize();
            test.stoptest();
        }
    }
    Public static testmethod void pricingFinancialSummaryclsmtd1(){
        P2A_TestFactoryCls.sampleTestData();
        
        system.runas(usrlist[0])
        {   
            List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1,prodbasktlist,ProductDeflist,Pbundle,Offerlists);
            proconfigs[0].Product_Id__c='GCPE';
            update proconfigs[0];
            system.assert(proconfigs[0].Product_Id__c!=null);
            
            prodbasktlist[0].cscfga__Shared_Context_Configuration__c = proconfigs[0].id;
            update  prodbasktlist[0];
            
            
            case cobj = new case();
            Cobj.Product_Basket__c =prodbasktlist[0].id;
            insert cobj;
            System.assert(Cobj.id != null);    
            
            Decimal maxcontractterm = 0; 
            
            Pricing_Approval_Request_Data__c P_Request = new Pricing_Approval_Request_Data__c();
            P_Request.Approved_NRC__c=0;
            P_Request.Approved_RC__c=0;
            P_Request.Offer_NRC__c=0;
            P_Request.Offer_RC__c =0;
            P_Request.Cost_RC__c = 0;
            P_Request.Cost_NRC__c = 0;  
            P_Request.Contract_Term__c = 12;
            P_Request.Is_offnet__c = false;
            P_Request.Rate_RC__c = 0;
            P_Request.Product_Basket__c = cobj.Product_Basket__c;
            P_Request.Product_Configuration__c = proconfigs[0].id;
            insert P_Request ;
            System.assert(P_Request.id != null);
            
            
            proconfigs[0].Product_Id__c='BackUp-VLP-Offnet';
            update proconfigs[0];
            
            cscfga__Product_Configuration__c prodcon = new cscfga__Product_Configuration__c();
           /* prodcon.cscfga__Product_Family__c = 'VLV';
            prodcon.cscfga__Parent_Configuration__c = proconfigs[0].id;
            prodcon.Product_Id__c = 'VLV';*/
                            prodcon.cscfga__Product_basket__c = prodbasktlist[0].id ;
                prodcon.cscfga__Product_Definition__c = ProductDeflist[0].id ;
                prodcon.name ='IPL';
                prodcon.cscfga__contract_term_period__c = 12;
                prodcon.Is_Offnet__c = 'yes';
                prodcon.cscfga_Offer_Price_MRC__c = 200;
                prodcon.cscfga_Offer_Price_NRC__c = 300;
                prodcon.cscfga__Product_Bundle__c =  Pbundle[0].id;
                prodcon.Rate_Card_NRC__c = 200;
                prodcon.Rate_Card_RC__c = 300;
                prodcon.cscfga__total_contract_value__c = 1000;
                prodcon.cscfga__Contract_Term__c = 12;
                prodcon.CurrencyIsoCode = 'USD';
                prodcon.cscfga_Offer_Price_MRC__c = 100;
                prodcon.cscfga_Offer_Price_NRC__c = 100;
                prodcon.Child_COst__c = 100;
                prodcon.Cost_NRC__c = 100;
                prodcon.Cost_MRC__c = 100;
                prodcon.Product_Name__c = 'test product';
                prodcon.Added_Ports__c = null ;
                prodcon.cscfga__Product_Family__c = 'Point to Point';
              //  prodcon.csordtelcoa__Replaced_Product_Configuration__c = Pb.csordtelcoa__Replaced_Product_Configuration__c;
            insert prodcon;
            
            
            Pricing_Approval_Request_Data__c P_Request1 = new Pricing_Approval_Request_Data__c();
            P_Request1.Approved_NRC__c=0;
            P_Request1.Approved_RC__c=0;
            P_Request1.Offer_NRC__c=0;
            P_Request1.Offer_RC__c =0;
            P_Request1.Cost_RC__c = 0;
            P_Request1.Cost_NRC__c = 0;  
            P_Request1.Contract_Term__c = 25;
            P_Request1.Is_offnet__c = false;
            P_Request1.Rate_RC__c = 0;
            P_Request1.Product_Basket__c = cobj.Product_Basket__c;
            P_Request1.Product_Configuration__c = prodcon.id;
            insert P_Request1 ;
            System.assert(P_Request1.id != null);
            
            Case cs = new case();
            cs.Business_Capex__c = 10;
            insert cs;
            
            test.startTest();
            PricingFinancialSummarycls prSumObj = new PricingFinancialSummarycls(new ApexPages.StandardController(Cobj));
            Boolean b = prSumObj.getisVisible();
            prSumObj.initlize();
            PricingFinancialSummarycls prSumObj1 = new PricingFinancialSummarycls(new ApexPages.StandardController(Cobj));
            Boolean b1 = prSumObj1.getisVisible();
            prSumObj1.initlize();
            PricingFinancialSummarycls prSumObj2 = new PricingFinancialSummarycls(new ApexPages.StandardController(cs));
            Boolean b2 = prSumObj2.getisVisible();
            prSumObj2.initlize();
            test.stoptest();            
        }
    }
    
    Public static testmethod void pricingFinancialSummaryclsmtd2(){
        P2A_TestFactoryCls.sampleTestData();
        
        system.runas(usrlist[0])
        {   
            List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1,prodbasktlist,ProductDeflist,Pbundle,Offerlists);
            proconfigs[0].Product_Id__c = 'VLV';
            update proconfigs[0];
            
            system.assert(proconfigs[0].id!=null);
            
            prodbasktlist[0].cscfga__Shared_Context_Configuration__c = proconfigs[0].id;
            update  prodbasktlist[0];
            
            case cobj = new case();
            Cobj.Product_Basket__c =prodbasktlist[0].id;
            insert cobj;
            System.assert(Cobj.id != null);    
            
            Decimal maxcontractterm = 0; 
            
            Pricing_Approval_Request_Data__c P_Request = new Pricing_Approval_Request_Data__c();
            P_Request.Approved_NRC__c=110;
            P_Request.Approved_RC__c=120;
            P_Request.Offer_NRC__c=10;
            P_Request.Offer_RC__c =10;
            P_Request.Cost_RC__c = 1230;
            P_Request.Cost_NRC__c = 2220;  
            P_Request.Contract_Term__c = 36;
            P_Request.Is_offnet__c = false;
            P_Request.Rate_RC__c = 10;
            P_Request.Product_Basket__c = cobj.Product_Basket__c;
            P_Request.Product_Configuration__c = proconfigs[0].id;
            insert P_Request ;
            System.assert(P_Request.id != null);
            
            test.startTest();
            PricingFinancialSummarycls prSumObj = new PricingFinancialSummarycls(new ApexPages.StandardController(Cobj));
            Boolean b = prSumObj.getisVisible();
            prSumObj.initlize();
            
            test.stoptest();
        }
    }
}