/**
    @author - Accenture
    @date - 25- Jun-2012
    @version - 1.0
    @description - This is the test class for OpportunityAfterUpdate  Trigger
*/
@isTest
private class OpportunityAfterUpdateTest {
  /* Static Account acc1 = getAccount();

   
   static testMethod void Opp_stage1() {
       
       List<Global_Constant_Data__c> globalConstantDataObjList = new List<Global_Constant_Data__c>();       
       Global_Constant_Data__c globalConstantDataObj;
       
       // providing the values to custom setting object
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Status';
       globalConstantDataObj.Value__c = 'Assigned';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Priority';
       globalConstantDataObj.Value__c = 'High';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       insert globalConstantDataObjList;
        List<Action_Item__c> previousActionItem = new List<Action_Item__c>();
           Action_Item__c action = new Action_Item__c();
           List<Action_Item__c> lastAction= [Select Id,Name,Previous_MRC__c,Previous_NRC__c,Account_Number__c,Opportunity__c,Status__c,Feedback__c,Account__r.Account_ID__c,LastModifiedDate,MRC_Monthly_Recurring_Charges__c,NRC_Non_Recurring_Charges__c,Total_Estimated_MRC__c,Total_Estimated_NRC__c from Action_Item__c where Id =: globalConstantDataObjList[0].id];
         //action.Previous_Credit_Check_Date__c=Date.newInstance(lastAction.get(0).LastModifiedDate.year(),lastAction.get(0).LastModifiedDate.month(),lastAction.get(0).LastModifiedDate.day());
                       if(lastAction != null && lastAction.size()>0){
                            action.Action_Item_ID_Previous_Credit_Check__c=lastAction[0].Name;                    
                            action.Previous_MRC__c=lastAction[0].MRC_Monthly_Recurring_Charges__c;
                            action.Previous_NRC__c=lastAction[0].NRC_Non_Recurring_Charges__c;
                            action.Previous_Estimated_MRC__c=lastAction[0].Total_Estimated_MRC__c;
                            action.Previous_Estimated_NRC__c=lastAction[0].Total_Estimated_NRC__c;
                            } 
                        previousActionItem.add(action);
                        insert previousActionItem;
System.assert(previousActionItem!=null);                        
       
       Opportunity opportunityObj = getOpportunity();
       insert opportunityObj;
       System.assert(opportunityObj!=null); 
        
       Set<Opportunity> oppSet = new Set<Opportunity>();
       oppSet.add(opportunityObj);
       OpportunityUpdate oppObjUpdate = new OpportunityUpdate();
       oppObjUpdate.BillProfileActivation(oppSet);
       oppObjUpdate.BillProfileApproval(oppSet);
       oppObjUpdate.CreditcheckCreation(oppSet);
       oppObjUpdate.PreviousCreditcheckupdation(oppSet);
      // OpportunityUpdate.updateAccountContactSites();
      
      
    
  }
     static testMethod void Opp_stage2() {
        List<Global_Constant_Data__c> globalConstantDataObjList = new List<Global_Constant_Data__c>();
       
       Global_Constant_Data__c globalConstantDataObj;
       
       // providing the values to custom setting object
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Status';
       globalConstantDataObj.Value__c = 'Assigned';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Priority';
       globalConstantDataObj.Value__c = 'High';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       insert globalConstantDataObjList;
       System.assert(globalConstantDataObjList!=null);
       
       Opportunity opportunityObj = getOpportunity1();
       insert opportunityObj;
       System.assert(opportunityObj!=null);
        
     }
   static testMethod void Opp_stage3() {
        List<Global_Constant_Data__c> globalConstantDataObjList = new List<Global_Constant_Data__c>();
       
       Global_Constant_Data__c globalConstantDataObj;
       
       // providing the values to custom setting object
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Status';
       globalConstantDataObj.Value__c = 'Assigned';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Priority';
       globalConstantDataObj.Value__c = 'High';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       insert globalConstantDataObjList;
       System.assert(globalConstantDataObjList!=null);
       
       Opportunity opportunityObj = getOpportunity2();
       insert opportunityObj;
       System.assert(opportunityObj!=null);
        
     }
  static testMethod void Opp_stage4() {
        List<Global_Constant_Data__c> globalConstantDataObjList = new List<Global_Constant_Data__c>();
       
       Global_Constant_Data__c globalConstantDataObj;
       
       // providing the values to custom setting object
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Status';
       globalConstantDataObj.Value__c = 'Assigned';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Priority';
       globalConstantDataObj.Value__c = 'High';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       insert globalConstantDataObjList;
       System.assert(globalConstantDataObjList!=null);
       
       Opportunity opportunityObj = getOpportunity3();
       insert opportunityObj;
       System.assert(opportunityObj!=null);
     }
     static testMethod void Opp_stage5() {
        List<Global_Constant_Data__c> globalConstantDataObjList = new List<Global_Constant_Data__c>();
       
       Global_Constant_Data__c globalConstantDataObj;
       
       // providing the values to custom setting object
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Status';
       globalConstantDataObj.Value__c = 'Assigned';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Priority';
       globalConstantDataObj.Value__c = 'High';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       insert globalConstantDataObjList;
       System.assert(globalConstantDataObjList!=null);
       
       Opportunity opportunityObj = getOpportunity4();
       insert opportunityObj;
       System.assert(opportunityObj!=null);
        
     }
      static testMethod void deleteOpp(){
        List<Global_Constant_Data__c> globalConstantDataObjList = new List<Global_Constant_Data__c>();
       
       Global_Constant_Data__c globalConstantDataObj;
       
       // providing the values to custom setting object
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Status';
       globalConstantDataObj.Value__c = 'Assigned';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Priority';
       globalConstantDataObj.Value__c = 'High';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       insert globalConstantDataObjList;
       System.assert(globalConstantDataObjList!=null);
        
        Opportunity opportunityObj = getOpportunity4();
        insert opportunityObj;
        System.assert(opportunityObj!=null);
        
        delete opportunityObj;
        
      }
       
     
    // Creating the Test Data
    private static Opportunity getOpportunity(){                 
        Opportunity opp = new Opportunity();            
        //Account acc = getAccount();           
        opp.Name = 'Test Opportunity';            
        opp.AccountId = acc1.Id;   
         //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement         
        opp.StageName = 'Identify & Define' ;  
        opp.Stage__c= 'Identify & Define' ;          
        opp.CloseDate = System.today();   
        opp.Opportunity_Type__c = 'Complex';
        opp.Estimated_MRC__c=8;
            opp.Estimated_NRC__c=2;
            opp.ContractTerm__c='1';
        //opp.Stage_Gate_1_Action_Created__c = true;         
        //opp.Approx_Deal_Size__c = 9000;                 
        return opp;    
       }
       private static Opportunity getOpportunity1(){                 
        Opportunity opp = new Opportunity();            
        //Account acc = getAccount();           
        opp.Name = 'Test Opportunity';            
        opp.AccountId = acc1.Id;   
        //updated Stage name Qualified prospect  as Qualify as per SOMP requirement         
        opp.StageName = 'Identify & Define' ;   
        opp.Stage__c=  'Identify & Define' ;         
        opp.CloseDate = System.today();   
        opp.Opportunity_Type__c = 'Complex';
        opp.Estimated_MRC__c=8;
            opp.Estimated_NRC__c=2;
            opp.ContractTerm__c='1';
        //opp.Stage_Gate_1_Action_Created__c = true;         
       // opp.Approx_Deal_Size__c = 9000;                 
        return opp;    
       }
       private static Opportunity getOpportunity2(){                 
        Opportunity opp = new Opportunity();            
       // Account acc = getAccount();           
        opp.Name = 'Test Opportunity';            
        opp.AccountId = acc1.Id;  
        //updated stage name Proposal Issued as Develop as per SOMP requirement          
        opp.StageName = 'Identify & Define' ; 
        opp.Stage__c=  'Identify & Define' ;         
        opp.CloseDate = System.today();  
        opp.Estimated_MRC__c=8;
            opp.Estimated_NRC__c=2;
            opp.ContractTerm__c='1'; 
        opp.Opportunity_Type__c = 'Complex';
        //opp.Stage_Gate_1_Action_Created__c = true;         
       // opp.Approx_Deal_Size__c = 9000;                 
        return opp;    
       }
        private static Opportunity getOpportunity3(){                 
        Opportunity opp = new Opportunity();            
        //Account acc = getAccount();           
        opp.Name = 'Test Opportunity';            
        opp.AccountId = acc1.Id;    
        //updated Stage name Individual & Final Negotiations as Propose as per SOMP requirement     
        opp.StageName = 'Identify & Define' ;
        opp.Stage__c= 'Identify & Define' ;           
        opp.CloseDate = System.today();   
        opp.Opportunity_Type__c = 'Complex';
        opp.Estimated_MRC__c=8;
            opp.Estimated_NRC__c=2;
            opp.ContractTerm__c='1';
        //opp.Stage_Gate_1_Action_Created__c = true;         
      //  opp.Approx_Deal_Size__c = 9000;                 
        return opp;    
       }
       private static Opportunity getOpportunity4(){                 
        Opportunity opp = new Opportunity();            
        //Account acc = getAccount();           
        opp.Name = 'Test Opportunity';            
        opp.AccountId = acc1.Id;  
        //updated Stage name verbal as Propose as per SOMP requirement          
        opp.StageName = 'Identify & Define' ; 
        opp.Stage__c= 'Identify & Define' ;          
        opp.CloseDate = System.today();   
        opp.Opportunity_Type__c = 'Complex';
        opp.Estimated_MRC__c=8;
            opp.Estimated_NRC__c=2;
            opp.ContractTerm__c='1';
        //opp.Stage_Gate_1_Action_Created__c = true;         
      //  opp.Approx_Deal_Size__c = 9000;                 
        return opp;    
       }
       private static Opportunity getOpportunity5(){                 
        Opportunity opp = new Opportunity();            
        //Account acc = getAccount();           
        opp.Name = 'Test Opportunity';            
        opp.AccountId = acc1.Id; 
        //updated stage name closed won as per SOMP requirement           
        opp.StageName = 'Identify & Define';
        opp.Stage__c='Identify & Define';
        opp.Sales_Status__c = 'Won';            
        opp.CloseDate = System.today();   
        opp.Opportunity_Type__c = 'Complex';
        opp.Estimated_MRC__c=8;
            opp.Estimated_NRC__c=2;
            opp.ContractTerm__c='1';
        //opp.Stage_Gate_1_Action_Created__c = true;         
      //  opp.Approx_Deal_Size__c = 9000;                 
        return opp;    
       }
       private static Account getAccount(){                
        Account acc = new Account();            
        Country_Lookup__c c = getCountry();            
        acc.Name = 'Test Account';            
        acc.Customer_Type__c = 'MNC';            
        acc.Country__c = c.Id;            
        acc.Selling_Entity__c = 'Telstra INC';  
        acc.Activated__c= true;  
        acc.Account_ID__c = '9090';  
        acc.Customer_Legal_Entity_Name__c = 'Test' ;     
        insert acc;       
        return acc;    
      }        
      
      private static Country_Lookup__c getCountry(){        
           Country_Lookup__c country = new Country_Lookup__c();            
           country.CCMS_Country_Code__c = 'HK';            
           country.CCMS_Country_Name__c = 'India';            
           country.Country_Code__c = 'HK';            
           insert country;        
           return country;    
         }
  */
}