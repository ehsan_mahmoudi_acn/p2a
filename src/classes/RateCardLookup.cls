public class RateCardLookup extends cscfga.ALookupSearch {
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){
        // NOT IMPLEMENTED
        System.Debug('doDynamicLookupSearch');
        List <Additional_Data__c> data = [SELECT Name, Type__c FROM Additional_Data__c WHERE Type__c = 'Country'];
        return data;
    }
    public override String getRequiredAttributes(){ 
        return '[ "Aend_Country__c", "Zend_Country__c" ]';
    }
    public string buildWhereClause(Map<String, String> searchFields)
    {
        List<String> subclauses = new List<String>();
        for (String key : searchFields.keySet())
        {
            if (searchFields.get(key) != null)
            {
                subclauses.add(key + ' = ' + searchFields.get(key));
            }
        }
        if (subClauses.isEmpty())
        {
            return '';
        }
        else
        {
            return String.Join(subClauses, ' AND ');
        }
    }
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
            try {
            System.Debug('RateCardLookup.apxc');
            List <String> countryNames = new List<String>();
            String Aend_Country = searchFields.get('A End Country');
            String Zend_Country = searchFields.get('Z End Country');
            System.Debug('Search field = ' + Aend_Country);
            String query = 'SELECT A_End_Country_Name__c, '+
                'Z_End_Country_Name__c, '+
                'Cable_Path_Name__c, '+
                'Aend_Country__c, '+
                'Zend_Country__c, '+
                'Cable_Path__c, '+
                'Zend_POP__c, '+
                'Aend_POP__c, '+
                'A_End_POP__c, '+
                'Z_End_POP__c '+
                ' FROM IPL_Rate_Card_Item__c';
            System.Debug('query = ' + query);
			List<IPL_Rate_Card_Item__c> rateCards = Database.query(query);
            System.Debug('rateCards = ' + rateCards);
			return rateCards;
            }
            catch (Exception e)
            {
                System.Debug('Exception: '+e);
                return null;
            }
    }

}