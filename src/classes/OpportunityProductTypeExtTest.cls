/*********************************************************************************
Name        : TestOpportunityProductTypeExt Class
Created By  : Mohantesh Kanike
Created Date: 25-JUN-2015    
Project     : Telstra P2O SFDC Implementation
Usage       : Code Coverage for OpportunityProductTypeExt Controller 
*********************************************************************************/
@isTest(SeeAllData = false)
public class OpportunityProductTypeExtTest
{
    static testmethod void testOpportunityProductTypeExt1()
    
    {
    
     P2A_TestFactoryCls.sampleTestData();    
        // Country_Lookup__c Object instance and Mandatory attributes with values
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
        system.assertEquals(cntryLkupObj.Name,'HK');
        
        // Account Object instance and Mandatory attributes with values
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
        insert accObj;
        system.assertEquals(accObj.Industry,'BPO');
        
        // Opportunity Object instance and Mandatory attributes with values
        Opportunity oppObj = new Opportunity(name = 'GFTS Ph 2',AccountID =accObj.ID, Opportunity_Type__c='Simple',
                                            CurrencyIsoCode ='HKD', CloseDate = Date.today(),StageName ='Identify & Define',Stage__c ='Identify & Define',
                                            Sales_Status__c = 'Won', QuoteStatus__c = 'Accepted',Win_Loss_Reasons__c='Product', 
                                            Order_Type__c ='New',Quote_Simplification__c = true, ContractTerm__c = '24');
        insert oppObj;
        system.assertEquals(oppObj.Sales_Status__c,'Won');
        
        string str = 'Application Assured Network';
        
        List<user>userObj=[select name from user where name!='' limit 10];
        
        //   Opp_Product_Type__c Object instance and Mandatory attributes with values
        
        Boolean UpdateSOV = true;
        
        Opp_Product_Type__c oppProdTypeObj = new Opp_Product_Type__c(Name='GFTS Ph 2',CurrencyIsoCode='USD',MRC__C=0.00,NRC__C=2.00,Product_Type_Name__c = str,
                                                                      Existing_MRC__c=0.00,Sales_Specialist__c=userObj[0].name,Opportunity__c =oppObj.Id);
        insert oppProdTypeObj;
        system.assertEquals(oppProdTypeObj.MRC__C,0.00);        

        
        
        // Declared the methods inside StartTest and StopTest
        Test.startTest();
            ApexPAges.StandardController sc = new ApexPages.StandardController(oppObj);
            opportunityProductTypeExt oppProdExtObj = new opportunityProductTypeExt(sc);
            oppProdExtObj.sSelectedProductType='dummy value';
            oppProdExtObj.selectProducts();
            oppProdExtObj.deleteProduct();
            oppProdExtObj.getProductTypeVals();
            oppProdExtObj.saveSelectedType();
            //oppProdExtObj.refreshMainItem(UpdateSOV);
            oppProdExtObj.updateItems();
            oppProdExtObj.deleteItem();
            oppProdExtObj.cancel();
            opportunityProductTypeExt.wrapperItem wrarpObj = new opportunityProductTypeExt.wrapperItem();
            wrarpObj.setItem(oppProdTypeObj);       
            
        Test.stopTest();
    }
    
     static testmethod void testOpportunityProductTypeExt2(){
        
        P2A_TestFactoryCls.sampleTestData();    
        
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
        system.assertEquals(cntryLkupObj.Name,'HK');
        
        Id UserId = userinfo.getUserId();
        List<user>userObj=[select name from user where id=:UserId];
        
        string str = 'Application Assured Network';
        
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
        insert accObj;
        system.assertEquals(accObj.Industry,'BPO');
        
        
        opportunity oppObj1 = new opportunity();
        oppObj1.name = 'GFTS Ph 2';
        oppObj1.AccountID =accObj.ID;
        oppObj1.Opportunity_Type__c='Simple';
        oppObj1.CurrencyIsoCode ='HKD';
        oppObj1.CloseDate = Date.today();
        oppObj1.StageName ='Identify & Define';
        oppObj1.Stage__c ='Identify & Define';
        oppObj1.Sales_Status__c = 'Won';
        oppObj1.Estimated_MRC__c = 0.00;
        oppObj1.Estimated_NRC__c =0.00;              
        oppObj1.Existing_MRC__c=0.00;
        oppObj1.QuoteStatus__c = 'draft';
        oppObj1.Calculated_New_SOV__c=0.00;
        oppObj1.Calculated_Renewal_SOV__c=0.00;
        oppObj1.Reported_New_SOV__c=0.00;
        oppObj1.Reported_Renewal_SOV__c=0.00;
        oppObj1.Total_Incremental_MRC__c =0.00;
        oppObj1.Estimated_TCV__c =0.00;
        oppObj1.Reported_MRC__c =0.00;
        oppObj1.Reported_NRC__c =0.00;
        oppObj1.Existing_MRC__c=0.00;
        oppObj1.SOV_Contract_Type__c = 'Duplicate';
        oppObj1.Win_Loss_Reasons__c='Product';
        oppObj1.Order_Type__c ='New';
        oppObj1.Quote_Simplification__c = true;
        oppObj1.ContractTerm__c = '24';
        insert oppObj1;
        
        Boolean UpdateSOV = true;
        
        Opp_Product_Type__c oppProdTypeObj = new Opp_Product_Type__c(Name='GFTS Ph 2',CurrencyIsoCode='USD',MRC__C=0.00,NRC__C=2.00,Product_Type_Name__c = str,
                                                                      Existing_MRC__c=0.00,Sales_Specialist__c=userObj[0].name,Opportunity__c =oppObj1.Id);
        insert oppProdTypeObj;
        system.assertEquals(oppProdTypeObj.MRC__C,0.00); 
        
        
        ApexPAges.StandardController sc = new ApexPages.StandardController(oppObj1);
        opportunityProductTypeExt oppProdExtObj = new opportunityProductTypeExt(sc);
            
        oppProdExtObj.refreshMainItem(UpdateSOV);    
        
       }

}