public with sharing class CS_DynamicRateCardLookup extends cscfga.ALookupSearch {
    
    public override String getRequiredAttributes(){ 
        return '[ "A-End POPCLS 1","Z-End POPCLS 1","A End Country","Z-End Country","A End City","Z End City","Bandwidth","IPL","EPL","EPLX","ICBS","EVPL","Show Unprotected Routes","Show Restored Routes","Show Protected Routes","Account Id" ]';
    }
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
        
        Id aEndCountry = searchFields.get('A End Country');
        Id zEndCountry = searchFields.get('Z-End Country');
        Id aEndCity = searchFields.get('A End City');
        Id zEndCity = searchFields.get('Z End City');
        Id aEndPop = searchFields.get('A-End POPCLS 1');
        Id zEndPop = searchFields.get('Z-End POPCLS 1');
        
        String bandwidth = searchFields.get('Bandwidth');
        Set <Id> bwIds = new Set<Id>();
        Set <Id> bwgrpIds = new Set<Id>();
        String Flag = searchFields.get('EVPL');
        String protected1 = searchFields.get('Show Protected Routes');
        String Unprotected = searchFields.get('Show Unprotected Routes');
        String Restored = searchFields.get('Show Restored Routes');
        Id accnt= searchFields.get('Account Id');        
       
       List <String> Resiliencefilter = new List<String>();
        //List <String> ProductTypeNames = new List<String>();
        Set<String> ProductTypeNames = new Set<String>();
        //Set<String> BwTypeNames = new Set<String>();
        List<String> BwTypeNames = new List<String>();        
        //List <CS_Bandwidth_Product_Type__c > BandwidthGroup = new List<CS_Bandwidth_Product_Type__c >();
        
        for (String key : searchFields.keySet()){
            String attValue = searchFields.get(key);
            if(attValue == 'Yes' || attValue == 'true')
                ProductTypeNames.add(key);
        }
        if(protected1 == 'Yes' || protected1 == 'true'){
            Resiliencefilter.add('Protected');
        }
        if(Unprotected == 'Yes' || Unprotected == 'true'){
            Resiliencefilter.add('Unprotected');
        }
        if(Restored == 'Yes' || Restored == 'true'){
            Resiliencefilter.add('Restored');
        }
        
        if(Resiliencefilter.size()==0){
            Resiliencefilter.add(''); 
        }
        System.debug('ProductTypeNames--------------++++++++++++++++++'+ProductTypeNames);        
        
        //BandwidthGroup = [SELECT Id,name,CS_Bandwidth__c, Bandwidth_Group__c from CS_Bandwidth_Product_Type__c where CS_Bandwidth__c = :bandwidth];
         //System.debug('BWgroup--------------++++++++++++++++++'+BandwidthGroup );
         for (CS_Bandwidth_Product_Type__c key : [SELECT Id,name,CS_Bandwidth__c, Bandwidth_Group__c from CS_Bandwidth_Product_Type__c where CS_Bandwidth__c = :bandwidth] ){
       //  System.debug('FOR' );
                 if(key.Bandwidth_Group__c != null){
                // System.debug('IF' );
                 //List <string> l = key.Bandwidth_Group__c.split(',');
                 //System.debug('Split--------------++++++++++++++++++'+l );
                 //for(String l1 : l)
                System.debug('++++++++++++++++++'+key.Bandwidth_Group__c);
                BwTypeNames.add(key.Bandwidth_Group__c);
                BwTypeNames.add(key.name);
                }
        }
        System.debug('BWTypename++++++++++++++++++'+BwTypeNames);
      
        List<CS_Bandwidth__c > bwgrpId = [SELECT id from CS_Bandwidth__c where name IN :BwTypeNames];      
       // bwgrpId.add(bwgrpIds);
        System.debug('BWTypename++++++++++++++++++'+bwgrpId);
        if(bwgrpId.isEmpty()){
            bwgrpId = [SELECT id from CS_Bandwidth__c where id = :bandwidth];
        }
        
        System.Debug('ProductTypeNames +++++++++++' + ProductTypeNames);
        System.Debug('aEndCountry +++++++++++' + aEndCountry);
        System.Debug('zEndCountry +++++++++++' + zEndCountry);
        System.Debug('aEndCity +++++++++++' + aEndCity);
        System.Debug('zEndCity +++++++++++' + zEndCity); 
        System.Debug('aEndPop +++++++++++' + aEndPop);
        System.Debug('zEndPop +++++++++++' + zEndPop);
        System.Debug('BwTypeNames +++++++++++' + BwTypeNames);
        System.Debug('accnt +++++++++++' + accnt);        
                                                                       
        List<CS_PTP_Rate_Card_Dummy__c> data = ReturnRateCardsForP2PDev.returnRateCards(ProductTypeNames, aEndCountry, zEndCountry, aEndCity, zEndCity, aEndPop, zEndPop, BwTypeNames, accnt);
        System.Debug('PTPRateCard:'+data);
       return data;

   }

}