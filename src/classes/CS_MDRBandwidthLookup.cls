global with sharing class CS_MDRBandwidthLookup extends cscfga.ALookupSearch {
    
    public override String getRequiredAttributes(){ 
        return '[ "Committed Bandwidth excluding direct China","CDR name" ]';
    }
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
    Integer pageOffset, Integer pageLimit){
        
        
        final Integer SELECT_LIST_LOOKUP_PAGE_SIZE = 25;
        final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = 26;
        Integer recordOffset = pageOffset * SELECT_LIST_LOOKUP_PAGE_SIZE;
        
        String cdr = searchFields.get('CDR name');
        
       // System.Debug('doLookupSearch');
        //System.Debug(searchFields);
        String searchValue = '%' + searchFields.get('searchValue') +'%';
        List <CS_Bandwidth__c> cdrdata = [SELECT Id, Name, Bandwidth_Value_MB__c 
            FROM 
                CS_Bandwidth__c 
            WHERE 
                name =: cdr
               
            ];
        Decimal cdrbw = cdrdata[0].Bandwidth_Value_MB__c;
        List <CS_Bandwidth__c> mdrdata = [SELECT Id, Name, Bandwidth_Value_MB__c 
            FROM 
                CS_Bandwidth__c 
            WHERE 
                Bandwidth_Value_MB__c >: cdrbw
                 AND
                Name LIKE :searchValue
            ORDER BY 
                Bandwidth_Value_MB__c 
            LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset];
            
        System.Debug(mdrdata);
        return mdrdata;
        
    }
    
}