@isTest(SeeAllData = false)
public class GenerateQuotePDFTest  {

    private static testMethod void generateQuotePDFMethod(){
        P2A_TestFactoryCls.sampleTestData();
        List<Account> acc = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> opp = P2A_TestFactoryCls.getOpportunitys(1,acc);
        List<cscfga__Product_Basket__c> pblist =  P2A_TestFactoryCls.getProductBasketHdlr(1,opp);
        List<cscfga__Product_Definition__c> pd = P2A_TestFactoryCls.getProductdef(1); 
        List<cscfga__Product_Bundle__c> bundle = P2A_TestFactoryCls.getProductBundleHdlr(1,opp);
        List<cscfga__Configuration_Offer__c> offr = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> pclist =  P2A_TestFactoryCls.getProductonfig(1,pblist,pd,bundle,offr);
        List<csord__Order_Request__c > OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Order__c> Orderlist = P2A_TestFactoryCls.getorder(1, OrdReqList);
        List<Supplier__c> supplierlist = P2A_TestFactoryCls.getsuppliers(1);
         
        List<cscfga__Configuration_Screen__c> configscreenlist = P2A_TestFactoryCls.getConfigScreen(1,pd);
         List<cscfga__Screen_Section__c> SSList =  P2A_TestFactoryCls.getScreenSec(1,configscreenlist);
        List<cscfga__Attribute_Definition__c > attdefinationList = P2A_TestFactoryCls.getAttributesdef(1,pclist,pd,configscreenlist,SSList);
        List<cscfga__Attribute__c> attList = P2A_TestFactoryCls.getAttributes(1,pclist,attdefinationList);
         attList[0].name = 'FastQuoteResult';
         attList[0].cscfga__value__c = 'FastQuoteResult';
         update  attList ;
         System.assertEquals(attList[0].name,'FastQuoteResult');
         System.assert(attList.Size()>0);
         
         Test.startTest();
         
         ApexPages.StandardController con = new ApexPages.StandardController(pclist[0]);
         PageReference pageRef = Page.GenerateQuotePDF;
         ApexPages.currentPage().getParameters().put('configId',pclist[0].id);
         Test.setCurrentPage(pageRef);  
         
         acc[0].Name = 'Test Account';
         update acc;
         
         opp[0].Account = acc[0];
         update opp;
         
         pd[0].Name = 'Point To Point-POC';
         update pd;
         
         pblist[0].cscfga__Opportunity__c = opp[0].id;
         update pblist;
         
         attList[0].Name = 'Contract Term';
         attList[0].cscfga__Display_Value__c = 'test';
         attList[0].cscfga__value__c = 'testing';
         update attList;
         
         pclist[0].cscfga__Product_Basket__c = pblist[0].id;
         pclist[0].cscfga__package_slot__c = attList[0].id;
         pclist[0].cscfga__Product_Definition__c = pd[0].id;
         update pclist;
         
         system.assert(pclist[0].id!=null);
         ApexPages.currentPage().getParameters().put('accountName',acc[0].Name);
        
        GenerateQuotePDF GQPDF = new GenerateQuotePDF(con);
       // list<fastQuoteStringList> fqsl = new list<fastQuoteStringList>();
       //FastQuoteStringWrapper fastQuoteStringList = new FastQuoteStringWrapper();
        //list<account> acc2 = new list<account>();
         //fastQuoteStringList = (List<FastQuoteStringWrapper>)System.JSON.deserialize(fastQuoteString,List<FastQuoteStringWrapper>.class);  
        try
        {
        GQPDF.processFastQuoteString();
        
        }catch (exception e){
                ErrorHandlerException.ExecutingClassName='GenerateQuotePDFTest:processFastQuoteString';         
                ErrorHandlerException.sendException(e); }
       GenerateQuotePDF.FastQuoteStringWrapper s = new GenerateQuotePDF.FastQuoteStringWrapper();
       s.Contract_Term_0 = 'fdssdf';
       s.Bandwidth_0 = 'fsdf';
       s.Rate_Card_MRC_0 = 56.2;
       s.Rate_Card_NRC_0 = 55.3;
        Test.stopTest();
    }
}