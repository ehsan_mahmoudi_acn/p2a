global class ProcessStepTimeManager {

    public static boolean SetJeopardyStatusRed = false;
    public static void setInitialETCForOrders(Set<Id> orderIds, boolean overrideStartTimeCheck){
        List<OrderAndServices> orderAndServices = OrderAndServiceFactory.makeOrderAndServicesFromOrders(orderIds);
        for (OrderAndServices oands : OrderAndServices) {
            SetInitialETCForOrdersAndService(oands, overrideStartTimeCheck);
        }       
    }

    webservice static String setInitialETCForOrder(Id orderId){
        SetInitialETCForOrders(new Set<Id> {orderId}, false);
        return 'Success';
    }

    webservice static String forceSetInitialETCForOrder(Id orderId){
        SetInitialETCForOrders(new Set<Id> {orderId}, true);
        return 'Success';
    }
    
    public static void setInitialETCForOrdersAndService(OrderAndServices oands){
        SetInitialETCForOrdersAndService(oands, false);
    }

    public static void setInitialETCForOrdersAndService(OrderAndServices oands, boolean overrideStartTimeCheck){
        ///
        /// Master function for setting coordinated critical path times across related orders and services
        ///
        System.Debug('SetInitialETCForOrdersAndService '+oands);
        system.debug('ritesh1 ProcessStepTimeManager.SetInitialETCForOrdersAndService');
        
        csord__Order__c order = oands.m_order;
        Date crd = order.Customer_Required_Date__c;
        List<csord__Service__c> services = oands.m_services;
        cspofa__Orchestration_Process__c orderProcess = oands.m_orderProcess;
        List<cspofa__Orchestration_Process__c> serviceProcesses = oands.m_serviceProcesses;
        if (serviceProcesses != null){
            for (cspofa__Orchestration_Process__c serviceProcess : serviceProcesses){
                if (oands.m_serviceProcessSteps.containsKey(serviceProcess.Id)){
                    for (CSPOFA__Orchestration_Step__c serviceStep : oands.m_serviceProcessSteps.get(serviceProcess.Id)){
                        if (serviceStep.OLA__c == null){
                            if (IncludeInPathView(serviceStep)){
                                serviceStep.Estimated_Time_To_Complete__c = StepOLAInMinutes(serviceStep);
                            }
                        }
                        else{
                            serviceStep.Estimated_Time_To_Complete__c = Integer.Valueof(serviceStep.OLA__c * 60 * 24); //OLA specified in days
                        }
                    }
                }
                else{
                    System.Debug(System.LoggingLevel.ERROR, 'Service process '+serviceProcess+' has no milestone steps for critical path calculation');  
                }
            }
        }
        else{
            System.Debug(System.LoggingLevel.ERROR, 'No service processes found for order '+order.Id);
            return;
        }
        
        SetDatesForProcesses(serviceProcesses, oands.m_serviceProcessSteps, null, false, overrideStartTimeCheck);
        Integer longestProcess = GetLongestProcessLength(serviceProcesses);
        CSPOFA__Orchestration_Step__c diStep = null;
        if (oands.m_orderProcess!=null && oands.m_orderProcessSteps.containsKey(oands.m_orderProcess.Id)){
            for (CSPOFA__Orchestration_Step__c orderstep : oands.m_orderProcessSteps.get(oands.m_orderProcess.Id)){
                if (IsServiceDependentStep(orderstep)){
                    diStep = orderstep;
                    orderstep.Estimated_Time_To_Complete__c = longestProcess;
                }
                else{
                    orderstep.Estimated_Time_To_Complete__c = StepOLAInMinutes(orderstep);
                }
            }
        }
        else{
            System.Debug(System.LoggingLevel.ERROR, 'order process ' + oands.m_orderProcess + ' has no milestone steps for critical path calculation');  
        }
        SetDatesForProcesses(new List<CSPOFA__Orchestration_Process__c> {orderProcess}, oands.m_orderProcessSteps, crd, crd == Null, overrideStartTimeCheck);
        if (diStep != null){
            DateTime diCompleteDate = diStep.End_Date_Time__c;
            SetDatesForProcesses(serviceProcesses, oands.m_serviceProcessSteps, diCompleteDate, false, overrideStartTimeCheck);
        }

    }
    
    public static void setDatesForProcesses(List<CSPOFA__Orchestration_Process__c> processes, Map<Id, List<CSPOFA__Orchestration_Step__c>> processToStepList, DateTime startOrEndDate, boolean dateIsStart){
        SetDatesForProcesses(processes, processToStepList, startOrEndDate, dateIsStart, false);
    }

    public static void setDatesForProcesses(List<CSPOFA__Orchestration_Process__c> processes, Map<Id, List<CSPOFA__Orchestration_Step__c>> processToStepList, DateTime startOrEndDate, boolean dateIsStart, boolean overrideStartTimeCheck){
        ///
        /// Sets the critical path data for a set of processes assuming no interdependencies
        ///
          system.debug('ritesh2 class:ProcessStepTimeManager method:SetDatesForProcesses');
        List<CSPOFA__Orchestration_Step__c> updatedSteps = new List<CSPOFA__Orchestration_Step__c>();
        for (CSPOFA__Orchestration_Process__c process : processes){
            List<CSPOFA__Orchestration_Step__c> stepsForProcess = process!=null?processToStepList.get(process.Id):null;
            if (stepsForProcess == null){
                return;
            }
            Set<CSPOFA__Orchestration_Step__c> steps = SetDatesForProcessAndSteps(process, stepsForProcess, startorEndDate, dateIsStart, overrideStartTimeCheck);
            for (CSPOFA__Orchestration_Step__c step : steps){
                updatedSteps.add(step);
            }
        }
        update processes;
        update updatedSteps;
    }
    
    public static Set<CSPOFA__Orchestration_Step__c> setDatesForProcessAndSteps(CSPOFA__Orchestration_Process__c process, List<CSPOFA__Orchestration_Step__c> stepsForProcess, DateTime startOrEndDate, boolean dateIsStart){
        return SetDatesForProcessAndSteps(process, stepsForProcess, startOrEndDate, dateIsStart, false);
    }
    
    public static Set<CSPOFA__Orchestration_Step__c> setDatesForProcessAndSteps(CSPOFA__Orchestration_Process__c process, List<CSPOFA__Orchestration_Step__c> stepsForProcess, DateTime startOrEndDate, boolean dateIsStart, boolean overrideStartTimeCheck){
        ///
        /// Sets critical path dates for a process and set of associated steps based on a given start or end date.
        /// process = process to update
        /// stepsForProcess = steps of the process that will (potentially) be updated
        /// startOrEndDate = the specified start (or end) date for the process
        /// dateIsStart = true if startOrEndDate is to be interpreted as a start date; false if startOrEndDate is to be interpreted as an end date
        ///
        Set<CSPOFA__Orchestration_Step__c> updatedSteps = new Set<CSPOFA__Orchestration_Step__c>();
        if (startOrEndDate == NULL){
            startOrEndDate = process.CreatedDate;
            dateIsStart = true;
        }

        Integer estimated_Time_To_Complete = 0;
        for (CSPOFA__Orchestration_Step__c step : stepsForProcess){
            if (step.cspofa__status__c != 'Complete')
                estimated_Time_To_Complete = estimated_Time_To_Complete + Integer.valueOf(step.Estimated_Time_To_Complete__c);
        } 
        
        if (!dateIsStart && !overrideStartTimeCheck){
            // When setting an end date, ensure that the start time is not in the past. If it is (or would be), set the start time to now
            DateTime expectedStart = CSCalendarDateCalculation.calculateStartDate(startOrEndDate, estimated_Time_To_Complete);
            if (expectedStart < DateTime.now()){
                startOrEndDate = DateTime.now();
                dateIsStart = true;
            }
        }
        if (dateIsStart){
            List<CSPOFA__Orchestration_Step__c> sortedSteps = SortSteps(stepsForProcess, False);
            DateTime dt = startOrEndDate;
            process.start_date_time__c = dt;
            for (CSPOFA__Orchestration_Step__c step : sortedSteps){
                if (step.cspofa__status__c != 'Complete' && step.Estimated_Time_To_Complete__c != NULL){
                    step.Start_Date_Time__c = dt;
                    dt = CSCalendarDateCalculation.calculateEndDate(dt,Integer.valueOf(step.Estimated_Time_To_Complete__c));
                    SetStepEndWithJeopardy(step, dt);
                    updatedSteps.add(step);
                }
            }
            SetProcessEndAndCheckCRD(process, dt);

        }
        else{
            List<CSPOFA__Orchestration_Step__c> sortedSteps = SortSteps(stepsForProcess, True);
            DateTime dt = startOrEndDate;
            SetProcessEndAndCheckCRD(process, dt);
            for (CSPOFA__Orchestration_Step__c step : sortedSteps)
            {
                if (step.cspofa__status__c != 'Complete' && step.Estimated_Time_To_Complete__c != NULL)
                {
                    SetStepEndWithJeopardy(step, dt);
                    dt = CSCalendarDateCalculation.calculateStartDate(dt, Integer.valueOf(step.Estimated_Time_To_Complete__c));
                    step.Start_Date_Time__c = dt;
                    updatedSteps.add(step);
                }
                if (step.Start_Date_Time__c < dt)
                    dt = step.Start_Date_Time__c;
            }
            if (dt != process.Start_Date_Time__c)
                process.Start_Date_Time__c = dt;
        }
        process.Estimated_Time_To_Complete__c = estimated_Time_To_Complete;
        if (process.csordtelcoa__Service__c != null)
        {
            TriggerFlags.NoSubscriptionTriggers = true;
            TriggerFlags.NoServiceTriggers = true;
            TriggerFlags.NoOrderTriggers = true;
            system.debug('ritesh method:SetDatesForProcessAndSteps  estimated estimated=process.Start_Date_Time__c '+process.Start_Date_Time__c);
            process.csordtelcoa__Service__r.Estimated_Start_Date__c = process.Start_Date_Time__c;
            update process.csordtelcoa__Service__r;
        }
        return updatedSteps;
    }

    public static void setStepEndWithJeopardy(CSPOFA__Orchestration_Step__c step, DateTime endDateTime)
    {
        step.End_Date_Time__c = endDateTime;
        step.CSPOFA__Target_Date_Time__c = endDateTime;
        step.CSPOFA__target_date__c = endDateTime.date();
    }
    public static void updateEnd(List<CSPOFA__Orchestration_Process__c> serviceProcesses, List<CSPOFA__Orchestration_Step__c> serviceSteps, DateTime newEnd)
    {
        //
        //Update service processes and steps based on a change in start date
        for (CSPOFA__Orchestration_Process__c serviceProcess : serviceProcesses)
        {
            UpdateProcessEnd(serviceProcess, newEnd);
        }
        for (CSPOFA__Orchestration_Step__c serviceStep : serviceSteps)
        {
            if (IncludeInPathView(serviceStep))
                UpdateStepEnd(serviceStep, newEnd);
            
        }
    }
    public static void updateStart(List<CSPOFA__Orchestration_Process__c> serviceProcesses, List<CSPOFA__Orchestration_Step__c> serviceSteps, DateTime newStart)
    {
        for (CSPOFA__Orchestration_Process__c serviceprocess : serviceProcesses)
        {
            UpdateProcessStart(serviceprocess, newStart);
        }
        for (CSPOFA__Orchestration_Step__c serviceStep : serviceSteps)
        {
            if (IncludeInPathView(serviceStep))
                UpdateStepStart(serviceStep, newStart);
        }
    }
    public static void updateProcessStart(CSPOFA__Orchestration_Process__c serviceProcess, DateTime newStart)
    {
        if (serviceprocess.Start_Date_Time__c != newStart)
        {
            TriggerFlags.NoSubscriptionTriggers = true;
            TriggerFlags.NoServiceTriggers = true;
            TriggerFlags.NoOrderTriggers = true;            
            serviceprocess.Start_Date_Time__c = newStart;
            serviceprocess.csordtelcoa__Service__r.Estimated_Start_Date__c = newStart;
            serviceprocess.End_Date_Time__c = CSCalendarDateCalculation.calculateEndDate(newStart, Integer.valueOf(serviceProcess.Estimated_Time_To_Complete__c));
            update serviceprocess;
            update serviceProcess.csordtelcoa__Service__r;
        }
    }

    public static void updateProcessEnd(CSPOFA__Orchestration_Process__c serviceProcess, DateTime newEnd)
    {
        if (serviceProcess.End_Date_Time__c != newEnd)
        {
            TriggerFlags.NoSubscriptionTriggers = true;
            TriggerFlags.NoServiceTriggers = true;
            TriggerFlags.NoOrderTriggers = true;            
            serviceProcess.End_Date_Time__c = newEnd;
            DateTime newStart = CSCalendarDateCalculation.calculateStartDate(newEnd, Integer.valueof(serviceProcess.Estimated_Time_To_Complete__c));
            serviceProcess.Start_Date_Time__c = newStart;
            serviceprocess.csordtelcoa__Service__r.Estimated_Start_Date__c = newStart;

            update serviceProcess;
            update serviceprocess.csordtelcoa__Service__r;
        }
    }
    public static void updateStepStart(CSPOFA__Orchestration_Step__c serviceStep, DateTime newStart)
    {
        if (serviceStep.Start_Date_Time__c != newStart)
        {
            servicestep.Start_Date_Time__c = newStart;
            SetStepEndWithJeopardy(serviceStep, CSCalendarDateCalculation.calculateEndDate(newStart, Integer.valueOf(serviceStep.Estimated_Time_To_Complete__c)));
            update servicestep;
        }
    }

    public static void updateStepEnd(CSPOFA__Orchestration_Step__c stepToUpdate, DateTime newEnd)
    {
        if (stepToUpdate.End_Date_Time__c != newEnd)
        {
            SetStepEndWithJeopardy(stepToUpdate, newEnd);
            stepToUpdate.Start_Date_Time__c = CSCalendarDateCalculation.calculateStartDate(newEnd, Integer.valueOf(stepToUpdate.Estimated_Time_To_Complete__c));
            update stepToUpdate;
        }
    }
    public static void setProcessEndAndCheckCRD(CSPOFA__Orchestration_Process__c process, DateTime endDateTime)
    {
        //updates the process end time, and if the process is running against an order, checks whether the
        //CRD will be violated.  If this is the case, all services on that order are set to Red.
        System.Debug('SetEndAndCheckCRD '+process);
        System.Debug('Order '+process.order__c);
        process.End_Date_Time__c = endDateTime;
        if (process.order__c != null)
        {
            Id orderId = process.order__r.Id;
            System.Debug('Setting process end to '+endDateTime);
            System.Debug('CRD = '+process.order__r.Customer_Required_Date__c);
            if (endDateTime > process.order__r.Customer_Required_Date__c/* && firstCheck*/) // to prevent creation of two cases for the first time -Mamta- #14090
            {
                System.Debug('Passed CRD');
                //firstCheck = false; // to prevent creation of two cases for the first time -Mamta- #14090

                csord__Order__c order = [SELECT Id, RAG_Status__c, RAG_Order_Status_RED__c, RAG_Reason_Code__c, OwnerId FROM csord__Order__c where Id = :orderId ];
                order.RAG_Order_Status_RED__c = true;
                order.RAG_Reason_Code__c = 'Expected delivery date = '+String.valueof(endDateTime);
                //update order;  //TODO bulkify //Nikhil -- 14/7 commented out
                System.Debug('order in red state'); 
                List<Case> caseList = new List<Case>();
                //UpdateOrchStepsForOrder(order);
                if(RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Jeopardy') != null){
                    
                    Case c = new Case(Status='New', 
                                      Order__c = order.Id, 
                                      Subject ='Order will miss delivery date '+String.valueof(process.order__r.Customer_Required_Date__c));
                    c.RecordTypeId = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Jeopardy');
                    c.OwnerId = order.OwnerId;
                    caseList.add(c);
                    System.Debug('Case owner = '+c.OwnerId + ' | ' + order.Owner.Id);
                }
                
                if(!caseList.isEmpty()){ insert caseList; order.Jeopardy_Case__c = caseList.get(0).Id; }
                update order; //Nikhil - 14/7 Moved this line further down after case creation.

                if(!System.IsBatch() && System.IsFuture() && !SetJeopardyStatusRed){
                    order.RAG_Status_Red__c = false;
                    Map<Id, csord__Order__c> newOrderMap = new Map<Id, csord__Order__c>();
                    newOrderMap.put(order.Id, order);
                    AllOrderTriggersHandler.pushStatusToActiveStep(newOrderMap.values(), newOrderMap);
                    SetJeopardyStatusRed = true;
                }               
            }
        }
    }
    @testvisible
    private static void updateOrchStepsForOrder(csord__Order__c order){
        List<CSPOFA__Orchestration_Step__c> allSteps = new List<CSPOFA__Orchestration_Step__c>();
            Map<Id, List<CSPOFA__Orchestration_Step__c>> process2StepList = new MAp<Id, List<CSPOFA__Orchestration_Step__c>>();
            for (CSPOFA__Orchestration_Step__c step : [SELECT 
                                                       Id, 
                                                       CSPOFA__Orchestration_Process__c, 
                                                       CSPOFA__Orchestration_Process__r.csordtelcoa__Service__r.Id, 
                                                       CSPOFA__Orchestration_Process__r.Order__r.Id, 
                                                       CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c,
                                                       CSPOFA__Orchestration_Step_Template__r.Estimated_Time_to_Complete__c,
                                                       CSPOFA__Status__c, 
                                                       RAG_Status__c, 
                                                       RAG_Status_Message__c 
                                                       FROM CSPOFA__Orchestration_Step__c 
                                                       WHERE CSPOFA__Orchestration_Process__r.Order__c =:order.Id])       
            {
                if (process2StepList.containsKey(step.CSPOFA__Orchestration_Process__c)){
                    process2StepList.get(step.CSPOFA__Orchestration_Process__c).add(step);
                }
                else{
                    process2StepList.put(step.CSPOFA__Orchestration_Process__c, new List<CSPOFA__Orchestration_Step__c> {step});
                }
            }
            for (Id processId : process2StepList.keySet())
            {
                List<CSPOFA__Orchestration_Step__c> steps = process2StepList.get(processId);
                for(CSPOFA__Orchestration_Step__c step : steps){
                    if(step.CSPOFA__Status__c == 'In Progress' || step.CSPOFA__Status__c == 'Initializing'){
                        allSteps.add(step);
                    }
                }
            }
            if(!allSteps.isEmpty()){
                ////System.Debug('All Steps updating '+allSteps);
                for (CSPOFA__Orchestration_Step__c step : allSteps){
                    if(step.CSPOFA__Orchestration_Process__r.Order__c != null){
                        System.Debug('Updating RAG state of order step'+step);
                        step.RAG_Status__c = order.RAG_Status__c;
                        step.RAG_Status_Message__c = order.RAG_Reason_Code__c;
                    } 
                } 
                 //System.Debug('Updating steps '+allSteps);
                update allSteps;
            }
    }
    
    // KK - optimized the code to reduce number of SOQL queries
    public static void updatePathWithTimeBulkified(map<Id, CSPOFA__Orchestration_Process__c> processesToUpdateMap
        , map<Id, cspofa__Orchestration_Step__c> stepsToUpdateMap
        , CSPOFA__Orchestration_Step__c stepToUpdate
        , DateTime dt
        , boolean isStart
        , CSPOFA__Orchestration_Process__c process
        , List <CSPOFA__Orchestration_Process__c> serviceProcesses
        , List <CSPOFA__Orchestration_Step__c> serviceSteps
        , List<cspofa__Orchestration_Step__c> processSteps) {

            Id processId = stepToUpdate.CSPOFA__Orchestration_Process__c;
           
            //update process bounds if step outside range
            boolean processUpdated = false;
            if (dt < process.Start_Date_Time__c)
            {
                process.Start_Date_Time__c = dt;
                processUpdated = true;
            }
            
            // Find all of the steps for this process, in order
            processSteps = SortSteps(processSteps, false);
            boolean updating = false;
            DateTime previousEndDateTime = null;
            for (cspofa__Orchestration_Step__c step : processSteps)
            {
                stepsToUpdateMap.put(step.Id, step);
                if (IncludeInPathView(step))
                {
                    
                    if (step.Id == stepToUpdate.Id)
                    {
                        updating = true;
                        if (isStart)
                        {
                            step.Start_Date_Time__c = dt;
                            if (IsServiceDependentStep(step)){UpdateStart(serviceProcesses, serviceSteps, step.Start_Date_Time__c);}
                            if (step.Estimated_Time_To_Complete__c != null){dt = CSCalendarDateCalculation.calculateEndDate(dt, Integer.valueOf(step.Estimated_Time_To_Complete__c));}
                        }
                        step.End_Date_Time__c = dt;
                        //update start time to equal previous step end
                        if (previousEndDateTime != null)
                        {
                            step.Start_Date_Time__c = previousEndDateTime;
                        }
                        if (step.End_Date_Time__c < step.Start_Date_Time__c)
                        {
                            step.Start_Date_Time__c = step.End_Date_Time__c;
                        }
                    }
                    previousEndDateTime = step.End_Date_Time__c;
                }
            }
            if (dt > process.End_Date_Time__c)
            {
                SetProcessEndAndCheckCRD(process, dt);
                processUpdated = true;
            }
            //update processSteps;
            if (updating && processUpdated) {
                //update process;
                processesToUpdateMap.put(process.Id, process);
            }
            

    
        }

    
    public static void updatePathWithTimeBulkified(map<Id, CSPOFA__Orchestration_Process__c> processesToUpdateMap
        , map<Id, cspofa__Orchestration_Step__c> stepsToUpdateMap
        , CSPOFA__Orchestration_Step__c stepToUpdate
        , DateTime dt
        , boolean isStart) {

            Id processId = stepToUpdate.CSPOFA__Orchestration_Process__c;
            CSPOFA__Orchestration_Process__c process = [SELECT Id, 
                                                        Start_Date_Time__c, 
                                                        End_Date_Time__c, 
                                                        Estimated_Time_To_Complete__c,
                                                        Order__c, 
                                                        Order__r.Customer_Required_Date__c 
                                                        FROM 
                                                        CSPOFA__Orchestration_Process__c 
                                                        WHERE 
                                                        Id = :processId];
            //update process bounds if step outside range
            boolean processUpdated = false;
            if (dt < process.Start_Date_Time__c)
            {
                process.Start_Date_Time__c = dt;
                processUpdated = true;
            }
            List <CSPOFA__Orchestration_Process__c> serviceProcesses = null;
            List <CSPOFA__Orchestration_Step__c> serviceSteps = null;
            if (process.Order__c!=null)
            {
                //If we're updating the order process and the Design & Implement step starts late, then related services may start late
                //Start by finding the services and processes and steps
                Id orderId = process.Order__c;
                List<csord__Service__c> services = [SELECT 
                                                    Id, 
                                                    csord__Subscription__r.csord__order__c 
                                                    FROM 
                                                    csord__Service__c 
                                                    WHERE 
                                                    csord__Subscription__r.csord__Order__c = :orderId];
                List<Id> serviceIds = new List<Id> ();
                for (csord__Service__c service : services)
                {
                    serviceIds.add(service.Id);
                }
                serviceProcesses = OrderAndServiceFactory.GetServiceProcessesFromIds(serviceIds);
                serviceSteps = OrderAndServiceFactory.GetProcessStepsFromProcesses(serviceProcesses);   
            }
            
            // Find all of the steps for this process, in order
            List<cspofa__Orchestration_Step__c> processSteps = OrderAndServiceFactory.GetProcessStepsFromProcesses(new List<CSPOFA__Orchestration_Process__c> {process});
            processSteps = SortSteps(processSteps, false);
            boolean updating = false;
            DateTime previousEndDateTime = null;
            for (cspofa__Orchestration_Step__c step : processSteps)
            {
                stepsToUpdateMap.put(step.Id, step);
                if (IncludeInPathView(step))
                {
                    
                    if (step.Id == stepToUpdate.Id)
                    {
                        updating = true;
                        if (isStart)
                        {
                            step.Start_Date_Time__c = dt;
                            if (IsServiceDependentStep(step)){UpdateStart(serviceProcesses, serviceSteps, step.Start_Date_Time__c);}
                            if (step.Estimated_Time_To_Complete__c != null){dt = CSCalendarDateCalculation.calculateEndDate(dt, Integer.valueOf(step.Estimated_Time_To_Complete__c));}
                        }
                        step.End_Date_Time__c = dt;
                        //update start time to equal previous step end
                        if (previousEndDateTime != null)
                        {
                            step.Start_Date_Time__c = previousEndDateTime;
                        }
                        if (step.End_Date_Time__c < step.Start_Date_Time__c)
                        {
                            step.Start_Date_Time__c = step.End_Date_Time__c;
                        }
                    }
                    previousEndDateTime = step.End_Date_Time__c;
                }
            }
            if (dt > process.End_Date_Time__c)
            {
                SetProcessEndAndCheckCRD(process, dt);
                processUpdated = true;
            }
            //update processSteps;
            if (updating && processUpdated) {
                //update process;
                processesToUpdateMap.put(process.Id, process);
            }
            

    
        }

    //
    //UTILITY FUNCTIONS & HELPERS
    //---------------------------
    //
    public static Integer  getLongestProcessLength(List<cspofa__Orchestration_Process__c> serviceProcesses)
    {
        System.Debug('GetLongestProcessLength' + serviceProcesses);
        Integer LongestProcessLength = 0;
        for (CSPOFA__Orchestration_Process__c process : serviceProcesses)
        {
            System.Debug('GetLongestProcessLength: process = ' + process);
            System.Debug('GetLongestProcessLength: ETC = ' + process.Estimated_Time_To_Complete__c);
            if (process.Estimated_Time_To_Complete__c!=null)
            {
                if (process.Estimated_Time_To_Complete__c > LongestProcessLength)
                {
                    LongestProcessLength = Integer.valueOf(process.Estimated_Time_To_Complete__c);
                }
            }
            System.Debug('GetLongestProcessLength: length = ' + LongestProcessLength);
        }
        return LongestProcessLength;

    }
    public static boolean includeInPathView(CSPOFA__Orchestration_Step__c step)
    {
        if (step.CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c != null && step.CSPOFA__Orchestration_Step_Template__r.Estimated_Time_To_Complete__c!=null)
        {
            return true;
        }
        return false;
    }
    public static Integer stepOLAInMinutes(CSPOFA__Orchestration_Step__c step)
    {
        if (step.CSPOFA__Orchestration_Step_Template__r.Estimated_Time_To_Complete__c == NULL)
        {
            return 0;
        }
        else 
        {
            Decimal olaTime = step.CSPOFA__Orchestration_Step_Template__r.Estimated_Time_To_Complete__c;
            return Integer.valueOf(olaTime * 24 * 60);
        }
    }
    public static List<CSPOFA__Orchestration_Step__c> sortSteps(List<CSPOFA__Orchestration_Step__c> stepsToSort, boolean reverse)
    {
        List<StepWrapper> sortableSteps = new List<StepWrapper>();
        for (CSPOFA__Orchestration_Step__c step : stepsToSort)
        {
            sortableSteps.add(new StepWrapper(step));
        }

        sortableSteps.sort();
        List<CSPOFA__Orchestration_Step__c> stepsToReturn = new List<CSPOFA__Orchestration_Step__c>();
        if (reverse)
        {
            Integer N = sortableSteps.size() - 1;
            for (Integer i = N; i>=0 ;i--)
            {
                stepsToReturn.add(sortableSteps[i].m_step);
            }
        }
        else
        {

            for (StepWrapper stepw : sortableSteps)
            {
                stepsToReturn.add(stepw.m_step);
            }
        }
        return stepsToReturn;
    }
    public static boolean isServiceDependentStep(CSPOFA__Orchestration_Step__c step)
    {
        boolean retVal = step.CSPOFA__Orchestration_Step_Template__r.Name == 'Design, Implement & Test Complete';
        System.Debug('IsServiceDependentStep ' + step.CSPOFA__Orchestration_Step_Template__r.Name + '? ' + retVal);
        return retVal;
    }
}