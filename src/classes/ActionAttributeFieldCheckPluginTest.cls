@isTest(SeeAllData=false) 
public class ActionAttributeFieldCheckPluginTest{
    
    private static List<cscfga__Action__c> actionlist;
    
     @istest
     private static void actionattributetest(){
     P2A_TestFactoryCls.sampletestdata();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<cscfga__Product_Basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasket(9);
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1,prodBaskList,ProductDeflist,Pbundle,Offerlists);
        List<cscfga__Configuration_Screen__c> screen = P2A_TestFactoryCls.getConfigScreen(1,ProductDeflist);
        List<cscfga__Screen_Section__c> screensection = P2A_TestFactoryCls.getScreenSec(1,screen);
        List<cscfga__Attribute_Definition__c> attributedeflist = P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist,screen,screensection);
        system.assertEquals(true,attributedeflist!=null); 
        Exception ee = null;
        String[] str1 = new List<string>();
       
        cscfga__Attribute_Field_Definition__c attrFielddef = new cscfga__Attribute_Field_Definition__c();
        attrFielddef.name = 'Attribute';
        attrFielddef.csordtelcoa__Asset_Name__c = 'AssetName'; 
        attrFielddef.cscfga__Attribute_Definition__c = attributedeflist[0].id;
        Insert attrFielddef;
        list<cscfga__Attribute_Field_Definition__c> ATT = [select id,name from cscfga__Attribute_Field_Definition__c where name = 'Attribute'];
        system.assertEquals(attrFielddef.name , ATT[0].name);
        System.assert(attrFielddef!=null);
         
        actionlist = new List<cscfga__Action__c>{
            new cscfga__Action__c(cscfga__Attribute_Field_Name__c = 'Action 19636',cscfga__Target_Reference__c = 'adv'
                ,cscfga__Target__c = attributedeflist[0].id,csexpimp1__guid__c = '96150aed-3b77-4f67-b3a2-6f57a740e0b6'
                ,cscfga__Product_Definition__c = ProductDeflist[0].id,cscfga__Attribute_Field__c = attrFielddef.id)
                
        };
        insert actionlist;
        list<cscfga__Action__c> attdef = [select id,cscfga__Attribute_Field_Name__c from cscfga__Action__c where cscfga__Attribute_Field_Name__c = 'Action 19636'];
        system.assertEquals(actionlist[0].cscfga__Attribute_Field_Name__c , attdef[0].cscfga__Attribute_Field_Name__c);
        System.assert(actionlist!=null);
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
             string str4 = actionlist[0].cscfga__Attribute_Field_Name__c;
            
            Map<String, Object> Mapsobj = new Map<String, Object>();
            Mapsobj.put('Attribute',actionlist);

            List<Map<String, Object>> ListMapsobj = new List<Map<String, Object>>();
            ListMapsobj.add(Mapsobj);
                          
            ActionAttributeFieldCheckPlugin action = new ActionAttributeFieldCheckPlugin();
            String  str = action.getSObjectName();
            str1 = action.getSObjectFields();
            String  str3 = action.getCustomFilters();
            try{
            action.runCheckOnObject(actionlist[0],ListMapsobj);
            }catch(Exception e){
                ErrorHandlerException.ExecutingClassName='ActionAttributeFieldCheckPluginTest :actionattributetest';         
                ErrorHandlerException.sendException(e); 
            }
          //  Test.stopTest();
                  
            }catch(Exception e){
                ErrorHandlerException.ExecutingClassName='ActionAttributeFieldCheckPluginTest :actionattributetest';         
                ErrorHandlerException.sendException(e); 
                ee = e;
        } finally {
            
                CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
    
    }
    
}