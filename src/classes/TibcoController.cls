/*
usage example: 

TibcoServiceOrder tso = new TibcoServiceOrder();
tso.header = new TibcoOrderHeader('ORDER_REF_0014', 'TIBCO', 'PSG', 'notes', 'SLAID_01', new UDF('MOPD','no'));
tso.lines.add(new TibcoOrderLine('1', 'IPLWC', '1', 'UOM', 'PROVIDE', '1', '1234567', 'NOTES', 'SLAID_01', new UDF('IPLWC_Attribute', 'blabla')));
tso.lines.add(new TibcoOrderLine('2', 'SRV', '1', 'UOM', 'PROVIDE', '1', '', 'NOTES', 'SLAID_01', new UDF('SRV_Attribute', 'blabla')));
tso.lines.add(new TibcoOrderLine('3', 'LL', '1', 'UOM', 'PROVIDE', '1', '', 'NOTES', 'SLAID_01', new UDF('LL_Attribute01','blabla')));
tso.lines.add(new TibcoOrderLine('4', 'LL', '1', 'UOM', 'PROVIDE', '1',  '', 'NOTES', 'SLAID_01', new UDF('LL_Attribute02', 'lalalal')));

TibcoController controller = new TibcoController();
String result = controller.SubmitServiceOrder(tso, 'http://54.74.194.100:18080/omsServer/api/orderService');
System.Debug(result);
//controller.SubmitServiceOrder('http://requestb.in/1fz77ch1');
 * 
 * */



public class TibcoController extends WebServiceConnector {
    
    //Define xml namespaces here:
    String ord = 'http://www.tibco.com/aff/orderservice';
    String ord1 = 'http://www.tibco.com/aff/order';
    String com = 'http://www.tibco.com/aff/commontypes';
    
    public String submitServiceOrder(TibcoServiceOrder tso, String endPoint, String responseName, String requestName, String soapAction)
    {
        DOM.Document doc = CreateServiceOrderDocument(tso, requestName);
         System.Debug('Request = '+doc);
        System.Debug('Request = '+doc.ToXmlString());       
        DOM.Document response = this.Request(doc, endPoint, soapAction);
        DOM.XmlNode root = response.getRootElement();
        DOM.XmlNode body = root.getChildElement('Body',soapNS);
        DOM.XmlNode submitOrderResponse = body.getChildElement(responseName,ord);
        String orderId = submitOrderResponse.getChildElement('orderId',ord).getText();
        String orderRef = submitOrderResponse.getChildElement('orderRef',ord).getText();        
        List<String> data = new String[]{orderId, orderRef};
        System.Debug(String.format('orderId = {0}, orderRef = {1}', data));
        //email part        
        sendMail(doc,orderId,response);//added to send logs via mail
        //ends here
        return orderId;
    }
    public DOM.Document createServiceOrderDocument(TibcoServiceOrder tso, String requestName) {
        DOM.Document doc = new DOM.Document();
        Map<String, String> nameSpaces = new Map<String, String>();
        namespaces.put('ord',ord);
        namespaces.put('ord1',ord1);
        namespaces.put('com',com);
        Dom.XmlNode body = CreateDocumentBody(doc, namespaces);
        Dom.XmlNode submitOrderRequest = body.addChildElement(requestName,ord,null);
        submitOrderRequest.setAttribute('ExternalBusinessTransactionID', '12312341');
        Dom.XmlNode orderRequest = submitOrderRequest.addChildElement('orderRequest',ord, null);
        orderRequest.addChildElement('orderRef',ord1,null).addTextNode(tso.orderRef);
        
        CreateHeaderChild(orderRequest, tso.header);
        for (TibcoOrderLine line : tso.lines)
        {
            if(null!=line){
                CreateLineChild(orderRequest, line, requestName, tso.orderType);
            }
        }
                       
        return doc;
    }
    public void createAddressChild(Dom.XmlNode parent, String nodeName){
        Dom.XmlNode address = parent.addChildElement(nodeName, ord1, null);
        address.addChildElement('line1',com,null).addTextNode('line1');
        address.addChildElement('line2',com,null).addTextNode('line2');
        address.addChildElement('line3',com,null).addTextNode('line3');
        address.addChildElement('locality',com,null).addTextNode('locality');
        address.addChildElement('region',com,null).addTextNode('region');
        address.addChildElement('country',com,null).addTextNode('US');
        address.addChildElement('postCode',com,null).addTextNode('11111');
        
    }
    public void createHeaderChild(Dom.XmlNode parent,
                                 TibcoOrderHeader header)
    {
        Dom.XmlNode headerNode = parent.addChildElement('header',ord1,null);
        if(null!=header.description)
        headerNode.addChildElement('description',ord1,null).addTextNode(header.description);
        if(null!=header.customerID)
        headerNode.addChildElement('customerID',ord1,null).addTextNode(header.customerID);
        if(null!=header.subscriberID)
        headerNode.addChildElement('subscriberID',ord1,null).addTextNode(header.subscriberID);
       // CreateAddressChild(headerNode,'invoiceAddress');
        //CreateAddressChild(headerNode,'deliveryAddress');
        if(null!=header.notes)
        headerNode.addChildElement('notes',ord1,null).addTextNode(header.notes);
        if(null!=header.slaID)
        headerNode.addChildElement('slaID',ord1,null).addTextNode(header.slaID);
        CreateUDFChild(headerNode,header.udf);
        //headerNode.addChildElement('extension',ord1,null).addChildElement('fulfilment',null,null).addTextNode('AFO');

    }
    public void createLineChild(Dom.XmlNode parent, TibcoOrderLine line, String requestName, String orderType)
    {
    system.debug('$$$Line'+line);
        Dom.XmlNode line1 = parent.addChildElement('line',ord1,null);
        if(null!=line.lineNumber)
       line1.addChildElement('lineNumber',ord1,null).addTextNode(line.lineNumber);
        if(null!=line.productID)
        line1.addChildElement('productID',ord1,null).addTextNode(line.productID);
        //if(null!=line.quantity)
        line1.addChildElement('quantity',ord1,null).addTextNode('1');
        if(null!=line.uom)
        line1.addChildElement('uom',ord1,null).addTextNode('UOM');
        //if(null!=line.action)
        if(requestName=='SubmitOrderRequest'){
        system.debug('<===CheckOrderTypeAshish==SubmitOrderRequest==>'+OrderType+'@@@@line.action'+line.action);
       /*/     if(orderType=='New Provide' || orderType=='' || orderType=='New' || orderType=='Subscription Creation'){
                line1.addChildElement('action',ord1,null).addTextNode('PROVIDE');
                }else{
                line1.addChildElement('action',ord1,null).addTextNode('UPDATE');
                }    /*/
       if(null!=line.action)
        line1.addChildElement('action',ord1,null).addTextNode(line.action);             
        }else if(requestName=='AmendOrderRequest'){
        system.debug('<===CheckOrderTypeAshish==AmendOrderRequest==>'+OrderType);
            if(orderType=='Terminate'){
                line1.addChildElement('action',ord1,null).addTextNode('CEASE');
            //}else if(orderType=='Suspend'){
            //    line1.addChildElement('action',ord1,null).addTextNode('CANCEL');
            }else{
                line1.addChildElement('action',ord1,null).addTextNode(line.action);
            }
        }
        if(null!=line.linkID)
        line1.addChildElement('linkID',ord1,null).addTextNode(line.linkID);
        if(null!=line.inventoryId)
        line1.addChildElement('inventoryID',ord1,null).addTextNode(line.inventoryID);
        if(null!=line.notes)
        line1.addChildElement('notes',ord1,null).addTextNode(line.notes);
        //line1.addChildElement('slaID',ord1,null).addTextNode(line.slaID);
       CreateUDFChild(line1,line.udf);
    
    }
    
     public void createUDFChild(Dom.XmlNode parent, UDF[] udfList)
    {
        if(null!=udfList){
            for (UDF udf : udfList) {
                if(null!=udf && null!=udf.name && null!=udf.value){
                    Dom.XmlNode udfNode = parent.addChildElement('udf',ord1,null);
                    udfNode.addChildElement('name',ord1,null).addTextNode(udf.name);
                    udfNode.addChildElement('value',ord1,null).addTextNode(udf.value); 
                }
            }
        }
    }
    
    public void sendMail(DOM.Document request,String orderId,DOM.Document response){
        //sending logs via mail
        List<messaging.singleEmailMessage>mails=new List<messaging.singleEmailMessage>();
        messaging.singleEmailMessage mail=new messaging.SingleEmailMessage();
        List<String>sendTo=new List<String>();
        List<String>getsenders=label.OrderXmlEmailReceiver.split(',');
        for(String email:getSenders){
        system.debug('email'+email);
        sendTo.add(email);
        }              
        mail.setToAddresses(sendTo);
        
        String inputXml='input'+
        '************'+
        request.ToXmlString()+
        'output'+
        '*********'+
        response.ToXmlString();
        
        String subjectMsg='P2A-DEV'+String.valueOf(orderId)+' :orderlogs';
        mail.setsubject(subjectMsg);
        mail.setPlainTextBody(inputXml);
        mails.add(mail);
        messaging.sendemail(mails);
        //ends here 
    }
        
}