/**
* @author - Accenture
* @date - 08-Aug-2016
* @description - This Class is used to Override IPT/TWI Multihome port rating type in individual port level for all the added port in the table.
* @Product configurations required this class : IPT Multihome and TWI Multihome.
*/

public with sharing class OverridePortRating{
    public static void afterUpdate(List<cscfga__Product_Configuration__c> productConfigs){
        String multiHomePortType;
        List<String> multihomeAddedPorts;
        String accountId;
        String accountCusTyp;
        Boolean runflag=false;
        List<cscfga__Attribute__c> atrbtList1 = new List<cscfga__Attribute__c>();
            
            
           /*
            * The method should not run after opportunity is closed won 
            */          
        for(cscfga__Product_Configuration__c pcID: productConfigs){
            if(pcID.name == 'IPT Multihome' || pcID.name == 'TWI Multihome'){
                if(pcID.Internet_Onnet_Port_Rating__c != null && pcID.Internet_Onnet_Port_Rating__c != 'None'){
                    multiHomePortType = pcID.Internet_Onnet_Port_Rating__c;
                }
                if(pcID.Added_Ports__c != null){
                    multihomeAddedPorts = pcID.Added_Ports__c.split(','); 
                }
            }
            if(pcID.AccountId__c != null || pcID.AccountCustomerType__c != null){
                accountId = pcID.AccountId__c;
                accountCusTyp = pcID.AccountCustomerType__c;
            }
            if(pcID.OpportunityStage__c!='Closed Won'){
                runflag=true;
            }
        }
        
        if(runflag){
            if(multiHomePortType != null && multihomeAddedPorts != null){
                List<cscfga__Attribute__c> attList = [SELECT Id,Name,cscfga__Value__c,cscfga__Display_Value__c,cscfga__Product_Configuration__c 
                                                        From cscfga__Attribute__c
                                                        Where Name = 'Port Rating Type' and cscfga__Product_Configuration__c IN :multihomeAddedPorts];

                if(attList.size() > 0){
                    for(cscfga__Attribute__c atrbtList : attList){
                        if(atrbtList.Name == 'Port Rating Type'){
                            atrbtList.cscfga__Value__c = multiHomePortType;
                            atrbtList.cscfga__Display_Value__c = multiHomePortType;
                            atrbtList1.add(atrbtList);
                        }
                    }
                }
            }
        
            if(accountId != null){
                List<cscfga__Attribute__c> attList = [SELECT Id,Name,cscfga__Value__c,cscfga__Display_Value__c,cscfga__Product_Configuration__c        From cscfga__Attribute__c
                                                        Where (Name = 'Account Id' or Name = 'AccountId' or Name = 'account_id' or Name = 'Account_Id__c' or Name = 'AccountType') and cscfga__Product_Configuration__c IN :productConfigs];
                system.debug('attListsap' +attList);
                if(attList.size() > 0){
                    for(cscfga__Attribute__c atrbtList : attList){
                        if(atrbtList.cscfga__Value__c == null && !atrbtList.Name.Contains('AccountType')){
                            atrbtList.cscfga__Value__c = AccountId;
                            atrbtList.cscfga__Display_Value__c = AccountId;
                            atrbtList1.add(atrbtList);
                        }
                        if(atrbtList.cscfga__Value__c == null && atrbtList.Name.Contains('AccountType')){
                            atrbtList.cscfga__Value__c = accountCusTyp;
                            atrbtList.cscfga__Display_Value__c = accountCusTyp;
                            atrbtList1.add(atrbtList);
                        }
                        
                    }
                }
            }
            if(atrbtList1.size() > 0){
                update atrbtList1;
            }            
        }
    }   
}