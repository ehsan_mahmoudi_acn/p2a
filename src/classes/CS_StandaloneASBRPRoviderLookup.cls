global with sharing class CS_StandaloneASBRPRoviderLookup extends cscfga.ALookupSearch {
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){

        String scenario = searchFields.get('Scenario');
        String nniType = searchFields.get('Type Of NNI');
        String offnetProvider ='%' + searchFields.get('Offnet Provider Calculated') +'%';
       	String searchValue = searchFields.get('searchValue') +'%';


        List<CS_Provider__c> returnList = new  List<CS_Provider__c> ();
        Set<Id> provIds = new Set<Id>();
        List<CS_Provider_NNI_Service_Join__c> providerList;
        if(scenario == 'NAS'){
            providerList = [SELECT CS_Provider__c, CS_NNI_Service__r.NNI_Type__c 
                            FROM CS_Provider_NNI_Service_Join__c
                            WHERE CS_NNI_Service__r.NNI_Type__c = :nniType
                            AND NAS__c = true];
        }else{
            providerList = [SELECT CS_Provider__c, CS_NNI_Service__r.NNI_Type__c 
                            FROM CS_Provider_NNI_Service_Join__c
                            WHERE CS_NNI_Service__r.NNI_Type__c = :nniType
                            AND Customer_NNI__c = true];//AND Standalone_ASBR__c = true
        }
        
        
        for(CS_Provider_NNI_Service_Join__c item : providerList){
            provIds.add(item.CS_Provider__c);
        }

        if(scenario == 'Customer NNI'){
        	returnList = [SELECT Id, Name FROM CS_Provider__c WHERE Id in :provIds and Carrier_Customer__c = true and Name Like :searchValue];

        } else if (scenario == 'NAS'){
        	returnList = [SELECT Id, Name FROM CS_Provider__c WHERE Id in :provIds and NAS_Product__c = true and Name Like :searchValue];

        }else if (scenario == 'Ex-Pacnet'){
            returnList = [SELECT Id, Name FROM CS_Provider__c WHERE Id in :provIds and Ex_Pacnet__c = true and Name Like :searchValue];
        }else {
        	returnList = [SELECT Id, Name FROM CS_Provider__c WHERE Offnet_Provider__c = true and Name Like :offnetProvider];
        }

        return returnList;
	}

	public override String getRequiredAttributes(){ 
	    return '["Scenario", "Offnet Provider Calculated", "Type Of NNI"]';
	}

	
	
}