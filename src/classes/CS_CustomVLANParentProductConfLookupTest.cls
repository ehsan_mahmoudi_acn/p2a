@isTest(SeeAllData = false)
private class CS_CustomVLANParentProductConfLookupTest {

    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    private static Object[] data = new List<Object>();
    
    private static List<cscfga__Product_Configuration__c> vlanPortList;
    private static List<cscfga__Product_Configuration__c> vlanList;
    private static List<csord__Service__c> parentServiceList;
    private static List<csord__Service__c> serviceList;
    private static List<csord__Order_Request__c> orderRequestList;
    private static List<csord__Subscription__c> subscriptionList;
    
    private static void initTestData(){
        
        //Product Configuration part
       /* vlanPortList = new List<cscfga__Product_Configuration__c>{
            new cscfga__Product_Configuration__c(Name = 'VPLS VLAN PORT 1'),
            new cscfga__Product_Configuration__c(Name = 'VPLS VLAN PORT 2')
        };
        
        insert vlanPortList;
        System.debug('**** VLAN PORTs: ' + vlanPortList);
          System.assertEquals('VPLS VLAN PORT 1',vlanPortList[0].Name ); 
        
        vlanList = new List<cscfga__Product_Configuration__c>{
            new cscfga__Product_Configuration__c(Name = 'VLAN 1', cscfga__Parent_Configuration__c = vlanPortList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'VLAN 2', cscfga__Parent_Configuration__c = vlanPortList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'VLAN 3', cscfga__Parent_Configuration__c = vlanPortList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'VLAN 4', cscfga__Parent_Configuration__c = vlanPortList[1].Id),
            new cscfga__Product_Configuration__c(Name = 'VLAN 5', cscfga__Parent_Configuration__c = vlanPortList[1].Id)
        };
        
        insert vlanList;
        System.debug('****VLANs: ' + vlanList);
        System.assertEquals('VLAN 1',vlanList[0].Name ); */
        
         list<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
      //  List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        //List<cscfga__Product_Configuration__c> vlanPortList = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
        List<cscfga__Product_Configuration__c> vlanList = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
        
        String vlanData = '';
        for (cscfga__Product_Configuration__c item : vlanList){
            if(vlanData == ''){
                vlanData += item.Id;
            } else {
                vlanData += ',' + item.Id;
            }
        }
        
        searchFields.put('VLANs', vlanData);
        
        //Order request part
        orderRequestList = new List<csord__Order_Request__c>{
            new csord__Order_Request__c(Name = 'Order Request 1', csord__Module_Name__c = 'Module name', csord__Module_Version__c = 'Module version')
        };
        
        insert orderRequestList;
           System.assertEquals('Order Request 1',orderRequestList[0].Name ); 
        
        //Subscritpion part
        subscriptionList = new List<csord__Subscription__c>{
            new csord__Subscription__c(Name = 'Subscription 1', csord__Identification__c = 'Identification string',csord__Order_Request__c = orderRequestList[0].Id)
        };
        insert subscriptionList;
        System.assertEquals('Subscription 1',subscriptionList[0].Name ); 
        
        //Service Part
        parentServiceList = new List<csord__Service__c>{
            new csord__Service__c(Name = 'VLAN Parent Service 1', csord__Identification__c = 'Parent Service 1', csord__Subscription__c = subscriptionList[0].Id, csord__Order_Request__c = orderRequestList[0].Id),
            new csord__Service__c(Name = 'VLAN Parent Service 2', csord__Identification__c = 'PArent Service 2', csord__Subscription__c = subscriptionList[0].Id, csord__Order_Request__c = orderRequestList[0].Id)
        };
        
        insert parentServiceList;
        System.debug('**** Parent Service List: ' + parentServiceList);
        System.assertEquals('VLAN Parent Service 1',parentServiceList[0].Name ); 
        
        serviceList = new List<csord__Service__c>{
            new csord__Service__c(Name = 'VLAN Service 1', csord__Service__c = parentServiceList[0].Id, csord__Identification__c = 'Service 1', csord__Subscription__c = subscriptionList[0].Id, csord__Order_Request__c = orderRequestList[0].Id),
            new csord__Service__c(Name = 'VLAN Service 2', csord__Service__c = parentServiceList[0].Id, csord__Identification__c = 'Service 2', csord__Subscription__c = subscriptionList[0].Id, csord__Order_Request__c = orderRequestList[0].Id),
            new csord__Service__c(Name = 'VLAN Service 3', csord__Service__c = parentServiceList[1].Id, csord__Identification__c = 'Service 3', csord__Subscription__c = subscriptionList[0].Id, csord__Order_Request__c = orderRequestList[0].Id )
        };
        
        insert serviceList;
        System.debug('****Service List: ' + serviceList);
        System.assertEquals('VLAN Service 1',serviceList[0].Name ); 
        
        String serviceData = '';
        for (csord__Service__c item : serviceList){
            if(serviceData == ''){
                serviceData += item.Id;
            } else {
                serviceData += ','+item.Id;
            }
        }
        
        searchFields.put('VLANs From Service', serviceData);
        
        System.debug('***SearchFields: ' + searchFields);
    }
    
    private static testMethod void doDynamicLookupSearchTest() {

    Exception ee = null;

        try{
            disableAll(UserInfo.getUserId());
            Test.startTest();
            initTestData();
            
            CS_CustomVLANParentProductConfLookup customVLANParentPCLookup = new CS_CustomVLANParentProductConfLookup(); 
            String requiredAtts = customVLANParentPCLookup.getRequiredAttributes();
            data = customVLANParentPCLookup.doDynamicLookupSearch(searchFields, productDefinitionId);
            
            System.debug('*******Data: ' + data);
            System.assert(data.size() > 0, '');
             Test.stopTest();
        } catch(Exception e){
            ee = e;
        } finally {
           // Test.stopTest();
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }
    }
    
    /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }

}