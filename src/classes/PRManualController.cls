/**
    @author - Accenture
    @date - 2-Aug-2012
    @version - 1.0
    @description - This class is used as controller .
*/

public class PRManualController
{ 
    List<OppProductWrapper> oppList = new List<OppProductWrapper>();
   
    List<OppProductWrapper> seloppList = new List<OppProductWrapper>();
    List<OppProductWrapper> selectedOppProd = new List<OppProductWrapper>();
        String oppId;
    Private String ErrorMessage = '';
    Public Boolean isError{get;set;}
    
    public List<OppProductWrapper> getOppProducts()
    {     
        return oppList;
    }
   
    public List<OppProductWrapper> getSelOppProducts()
    {     
        return seloppList;
    }
    public PRManualController(ApexPages.StandardController controller){
       
        oppId = System.currentPageReference().getParameters().get('id');      
         isError = false;
        Opportunity o = [SELECT Id, Is_Offnet_Product_in_Deal__c,QuoteStatus__c from Opportunity where Id =: oppId]; 
        if(o.Is_Offnet_Product_in_Deal__c <= 0){
            setErrorMessage('To enter the purchase request details, opportunity must have offline products listed');
        }else if(o.QuoteStatus__c == 'Budgetary Quote'){
            setErrorMessage('You can not enter the purchase request details, because your opportunity quote is in the budgetary stage');
        }else{  
            for(OpportunityLineItem a : [SELECT Id,ProductCode__c,PricebookEntry.Product2.ProductCode,PricebookEntry.product2.Name,CPQItem__c,Purchase_Requisition_Number__c 
         FROM OpportunityLineItem where OpportunityId=: oppId and OffnetRequired__c =: 'Yes']){
            oppList.add(new OppProductWrapper(a.Id,a.PricebookEntry.product2.Name,a.CPQItem__c,a.PricebookEntry.Product2.ProductCode,a.Purchase_Requisition_Number__c,false ));                 
            }
        }
    }
    
    public PRManualController()
    {
         this.isError = false;
    }
    
    public void setErrorMessage(String error) {
          this.isError  = true;
          this.ErrorMessage=error;
          
    }
    
    public String getErrorMessage() {
         return this.ErrorMessage;
    }
    
    public PageReference getSelected()
    {
        selectedOppProd.clear();
        for(OppProductWrapper accwrapper : oppList){
        //System.debug('Going Under Wrapper' + accwrapper.selected);
        if(accwrapper.selected == true)
        selectedOppProd.add(accwrapper);
        }
        return null;
    }
    
    public PageReference updatePRInfo()
    {        
       List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
        for (OppProductWrapper wrap : oppList){
        if (wrap.selected==true){
                //System.debug('inside Action');
               OpportunityLineItem oli = new OpportunityLineItem(Id = wrap.oppLineId, Purchase_Requisition_Number__c = wrap.prNumber);
               oliList.add(oli);
           }
        }        
        update oliList;
        return new pageReference('/'+oppId);
    }
    
    public pageReference cancelRequest(){
        return new pageReference('/'+oppId);
    }
    
    public List<OppProductWrapper> getSelectedAccounts()
    {
        if(selectedOppProd.size()>0)
        return selectedOppProd;
        else
        return null;
    }    
    
    public class OppProductWrapper
    {
        public Id oppLineId{get; set;}
        public String productName{get; set;}
        public Boolean selected {get; set;}
        Public Boolean preselected{get;set;}
        public String item{get;set;}        
        public String prodCode{get;set;}     
        public String prNumber{get;set;}        
        public OppProductWrapper(Id oliid, String a,String item2,String procode,String prnum,Boolean preselect)
        {
            oppLineId = oliid;
            productName = a;
            selected = false;
            preselected = preselect;
            /*if (preselected){
                selected=true;
            }*/
            item=item2;            
            prodCode = procode;
            prNumber = prnum;
        }
    }
}