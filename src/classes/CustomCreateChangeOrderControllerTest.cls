@isTest(SeeAllData=false)
public class CustomCreateChangeOrderControllerTest {
    
    private static cscfga__Product_Basket__c basket = null;
    private static cscfga__Product_Configuration__c config = null;
    private static csord__Service__c service = null;
    private static csord__Subscription__c subscription = null;
        
    /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        } else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
        system.assert(globalMute!=null);
    }
    
        /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableTelcoAll(Id userId) {
        csordtelcoa__Orders_Subscriptions_Options__c telcoOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance(userId);

        if(telcoOptions == null) {
            telcoOptions = new csordtelcoa__Orders_Subscriptions_Options__c ();
            telcoOptions.csordtelcoa__Disable_Triggers__c = true;
            telcoOptions.SetupOwnerId = userId;
        } else {
            telcoOptions.csordtelcoa__Disable_Triggers__c = true;
        }

        upsert telcoOptions;
        system.assert(telcoOptions!=null);
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        } else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
        system.assert(globalMute!=null);
    }

    private static void enableTelcoAll(Id userId) {
        csordtelcoa__Orders_Subscriptions_Options__c telcoOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance(userId);
        
        if(telcoOptions == null) {
            telcoOptions = new csordtelcoa__Orders_Subscriptions_Options__c ();
            telcoOptions.csordtelcoa__Disable_Triggers__c = false;
            telcoOptions.SetupOwnerId = userId;
        } else {
            telcoOptions.csordtelcoa__Disable_Triggers__c = false;
        }

        upsert telcoOptions;
        system.assert(telcoOptions!=null);
    }

    private static void createTestData() {
       
        cscfga__Product_Category__c productCategory = new cscfga__Product_Category__c(Name = 'Test category');
        insert productCategory;
        
        cscfga__Product_Definition__c prodDefintion = new cscfga__Product_Definition__c (Name = 'Test definition 1'
                , cscfga__Product_Category__c = productCategory.Id
                , cscfga__Description__c = 'Test definition 1');
        insert prodDefintion;
        system.assert(prodDefintion!=null);
        basket = new cscfga__Product_Basket__c();
        insert basket;
       
        config = new cscfga__Product_Configuration__c(Name = 'Test config '
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Product_Basket__c = basket.Id);  
        insert config;

        csord__Order_Request__c orderRequest = new csord__Order_Request__c(Name = 'Test request'
            , csord__Module_Version__c = 'dummy'
            , csord__Module_Name__c = 'dummy');
        insert orderRequest;
        system.assert(orderRequest!=null);
        subscription = new csord__Subscription__c(Name = 'Test subscription'
            , csord__Identification__c = 'dummy'
            , csord__Order_Request__c = orderRequest.Id);
        insert subscription; 
        
        service = new csord__Service__c(Name = 'Test service'
            , csordtelcoa__Product_Configuration__c = config.Id
            , csordtelcoa__Product_Basket__c = basket.Id
            , csord__Identification__c = 'Service_' + config.Id
            , csord__Subscription__c = subscription.Id
            , csord__Order_Request__c = orderRequest.Id);
        insert service;
        system.assert(service!=null);
    }

    private static void tryToCreateTestData() {
    
      P2A_TestFactoryCls.sampleTestData();
      
        Exception ee = null;
        Integer userid = 0;
        try {
            disableAll(UserInfo.getUserId());
            system.assert(userid!=null);
            disableTelcoAll(UserInfo.getUserId());
            createTestData();
        } catch(Exception e) {
            ee = e;
             ErrorHandlerException.ExecutingClassName='CustomCreateChangeOrderControllerTest :testProcessCall';         
             ErrorHandlerException.sendException(e);
        } finally {
            enableAll(UserInfo.getUserId());
            enableTelcoAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
    
    private static testMethod void testProcessCall() {
    try{
        P2A_TestFactoryCls.sampleTestData();
   
        tryToCreateTestData();
        CustomCreateChangeOrderController ctr = new CustomCreateChangeOrderController(new ApexPages.StandardController(subscription));
        system.assert(ctr!=null);
        Test.startTest();
       // ctr.Change_Types_Wrapper();
        ctr.getChangeTypes();
        ctr.getRequestDate();
        ctr.getRequestedDateLabel();
        ctr.getInvokeProcess();
        CustomCreateChangeOrderController.getStatusesNotAllowingChange();
        ctr.getAllowChange();
        CustomCreateChangeOrderController.getOpportunityRecordTypes();
        ctr.getOppRecordTypes();
        ctr.rerenderButtons();  
        List<String> queues = CustomCreateChangeOrderController.getDefaultRecordTypeNameForSObject(Case.SObjectType);
       
        
        csordtelcoa__Change_Types__c emp = new csordtelcoa__Change_Types__c(Name = 'Test');
        CustomCreateChangeOrderController.Change_Types_Wrapper empW = new CustomCreateChangeOrderController.Change_Types_Wrapper(emp);  //Covering inner/wrapper class
        empW.compareTo(empW);  
        ctr.createProcess();
        ctr.processName = 'Termination';
        ctr.dateString = system.today().format();
        //Date aDate = date.parse(ctr.dateString);
        ctr.createProcess();
        ctr.dateString = 'test';
        ctr.createProcess();
        Test.stopTest();
        }catch(Exception e){
         ErrorHandlerException.ExecutingClassName='CustomCreateChangeOrderControllerTest :testProcessCall';         
         ErrorHandlerException.sendException(e);
        }
    }
    
    private static testMethod void testMacOpportunityFromSubscription() {
    
    
       P2A_TestFactoryCls.sampleTestData();
    try{
        tryToCreateTestData();
        CustomCreateChangeOrderController ctr = new CustomCreateChangeOrderController(new ApexPages.StandardController(subscription));
        system.assert(ctr!=null);
        Test.startTest();
        ctr.dateString = system.today().format();
        ctr.changeType = 'Renewal';
        ctr.CreateMacOpportuntiyFromSubscription();
        Test.stopTest();
        }catch(Exception e){
        ErrorHandlerException.ExecutingClassName='CustomCreateChangeOrderControllerTest :testMacOpportunityFromSubscription';         
        ErrorHandlerException.sendException(e); 
        }
    }
    
    private static testMethod void testMacBasketFromSubscription() {
    
      P2A_TestFactoryCls.sampleTestData();
      try{
         tryToCreateTestData();
        CustomCreateChangeOrderController ctr = new CustomCreateChangeOrderController(new ApexPages.StandardController(subscription));
        system.assert(ctr!=null);
        Test.startTest();
        ctr.dateString = system.today().format();
        ctr.changeType = 'Renewal';
        ctr.CreateMacBasketFromSubscription();
        Test.stopTest();
        }catch(Exception e){
        ErrorHandlerException.ExecutingClassName='CustomCreateChangeOrderControllerTest :testMacBasketFromSubscription';         
        ErrorHandlerException.sendException(e); 
        }

    }
}