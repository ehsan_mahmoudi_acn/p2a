public class FieldTimeValueGenerator implements FieldValueGenerator {
    
    private Integer offsetInHours;
    
    public FieldTimeValueGenerator() {
        this.offsetInHours = 0;
    }
    
    public FieldTimeValueGenerator(Integer offsetInHours) {
        this.offsetInHours = offsetInHours;
    }
    
    public Boolean canGenerateValueFor(Schema.DescribeFieldResult fieldDesc) {
        return Schema.DisplayType.TIME == fieldDesc.getType();
    }
    
    public Object generate(Schema.DescribeFieldResult fieldDesc) {
        Time result = DateTime.now().time();
        result.addHours(this.offsetInHours);
        return result;
    }
}