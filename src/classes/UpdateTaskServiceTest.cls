//Author: Atul
//Created Date - 10/04/2017
//Comments - Test class for SNCAccountTeamInputSchema because owner did not create one :)

 @isTest 
public class UpdateTaskServiceTest {

  
  static testmethod void updateTaskmethod(){
    
    String Task_name;
    Integer Priority;
    String Task_operation;
    String Site_hum_id;
    String Elm_compl_status;
    String Lock_element_on_co;
    String Jeopardy_status;
    String Task_description;
    String Action;
    Long Workorder_Inst_Id = 1234;
    Integer Task_Inst_Id;
    String Status_code;
    String User_x;
    
    
    UpdateTaskService.UpdateTaskServicePortSoapPort task1= new UpdateTaskService.UpdateTaskServicePortSoapPort();
    UpdateTaskParams.WOUpdateTaskOutputVO updateTask = new UpdateTaskParams.WOUpdateTaskOutputVO();
    
    Test.startTest();
     Test.setMock(WebServiceMock.class, new SoapServiceMockImpl());
     updateTask = task1.updateTask('Task_name',8,'Task_operation','Site_hum_id','Elm_compl_status','Lock_element_on_co','Jeopardy_status','Task_description','Action',Workorder_Inst_Id,5,'Status_code','User_x');
     system.assertEquals(true,task1!=null);
   Test.stopTest();    
  }
  
}