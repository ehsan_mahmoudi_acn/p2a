/*
*	Utility class for sending email send functionality
*/
public with sharing class EmailUtil {
	
	/*
	*	Send a single email message to one or more recipients
	*/
	public static List<Messaging.SendEmailResult> sendEmail(EmailData mailData){
        List<Messaging.SendEmailResult> sendEmailResults = Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mailData.message});
        return sendEmailResults;
	}

	/*
	*	Contains all data required when sending an emails
	*/
	public class EmailData{
		public Messaging.SingleEmailMessage message;
		public List<String> toAddresses;
		public List<String> ccAddresses;

		public EmailData(){
			message = new Messaging.SingleEmailMessage();
			toAddresses = new List<String>();
			ccAddresses = new List<String>();
		}

		public void addToAddress(String emailAddress){
			toAddresses.add(emailAddress);
			message.setToAddresses(toAddresses);
		}

		public void addToAddresses(List<String> emailAddresses){
			toAddresses.addAll(emailAddresses);
			message.setToAddresses(toAddresses);
		}

		public void addCCAddress(String emailAddress){
			ccAddresses.add(emailAddress);
			message.setCcAddresses(ccAddresses);
		}

		public void addCCAddresses(List<String> emailAddresses){
			ccAddresses.addAll(emailAddresses);
			message.setCcAddresses(ccAddresses);
		}
	}
}