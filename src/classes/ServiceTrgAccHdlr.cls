public with sharing class ServiceTrgAccHdlr 
{
    public void serTrgAccHdlrMthd(List<csord__Service__c> newServices,Map<Id,csord__Service__c>oldServiceMap)
    {
      Set<Id> accountId = new Set<Id>();
        Integer Count = 0;
        //List<Account> accObjList = new List<Account>();
        Map<String,NewLogo__c> newLogoCustMap = NewLogo__c.getAll();
        for(csord__Service__c servObj : newServices)
        {
            if(servObj != null && servObj.Name != null && servObj.Path_Instance_ID__c != null
            && servObj.Primary_Service_ID__c != null && servObj.Inventory_Status__c != null && servObj.AccountId__c != '001N000000jLgNR' // add a bypass for the UAT enviornment on the Data migration account
            && servObj.Inventory_Status__c!=oldServiceMap.get(servObj.id).Inventory_Status__c) 
            {
                accountId.add(servObj.AccountId__c);   
            }
        }
        if(accountId != null && accountId.size()>0)
        {    
            /*accObjList = [Select Id, Name,Type,Account_Status__c from Account where ID IN : accountId
                     AND Account_Status__c= :newLogoCustMap.get('Active').Action_Item__c]; */
                     
            List<csord__Service__c> serviceList = [Select Id, Name, AccountId__c, Inventory_Status__c,Order_Type__c from csord__Service__c Where 
                                 AccountId__c IN : accountId] ;
        
            for(Integer i = 0; i<serviceList.size() ; i++)
            {
                if(serviceList.get(i).Inventory_Status__c == newLogoCustMap.get('Decommissioned').Action_Item__c
                  ||(serviceList.get(i).Inventory_Status__c == newLogoCustMap.get('Non-Operational').Action_Item__c && serviceList.get(i).Order_Type__c == 'Terminate')
                 ||serviceList.get(i).Inventory_Status__c == newLogoCustMap.get('CANCELLED').Action_Item__c)
                Count++;
            }
            
            if(Count == serviceList.size())
                {
                    List<Account> accObjList1 = new List<Account>();
					for(Account  accobj : [Select Id, Name,Type,Account_Status__c from Account where ID IN : accountId
                             AND Account_Status__c= :newLogoCustMap.get('Active').Action_Item__c])
                    {
                        
                            accobj.Type = newLogoCustMap.get('Logo_Former_Customer').Action_Item__c;
                            accObj.Account_Type__c = newLogoCustMap.get('Logo_Former_Customer').Action_Item__c;
							accObjList1.add(accobj);
                            //update accobj;
                        
                    }
					update accObjList1;
               }
        
        }    
    }
    
}