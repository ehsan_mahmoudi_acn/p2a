global class ContactDefaultAddressPopulate implements Database.Batchable<sObject>{

    public String query;
    public String email;

    global Database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);}

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Contact> contacts = new List<Contact>();
                Set<Id> accountId = new Set<Id>();
                
                for(sObject s : scope){
                                Contact c = (Contact)s;
                              accountId.add(c.AccountId);
                }
                                
                List<contact> contactList = [SELECT Id,MailingStreet,MailingState,MailingPostalCode,MailingCountry,MailingCity,Account.BillingStreet,Account.BillingState, 
                                                account.BillingPostalCode, account.BillingCountry, account.BillingCity FROM contact WHERE Id IN:accountId and MailingCity = null and MailingStreet = null];
                                
                For(Contact Cont1:ContactList){
                                cont1.MailingStreet = cont1.Account.BillingStreet;
                                cont1.MailingState = cont1.Account.BillingState;
                                cont1.MailingPostalCode = cont1.Account.BillingPostalCode;
                                cont1.MailingCountry = cont1.Account.BillingCountry;
                                cont1.MailingCity = cont1.Account.BillingCity;
                                contacts.add(cont1);
                }
                if(contacts != null){
                                update contacts;
                }   
    
    }
    
    global void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.setToAddresses(new String[] {email});
        mail.setReplyTo('ross.tailby@teamuk.telstra.com');
        mail.setSenderDisplayName('SFDC Batch Processing');
        mail.setSubject('Contact Address Populator Batch Process Completed');
        mail.setPlainTextBody('Batch Process has completed and all Contacts should have mailing addresses');

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}