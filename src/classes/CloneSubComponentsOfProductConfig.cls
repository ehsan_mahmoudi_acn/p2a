public class CloneSubComponentsOfProductConfig implements Database.Batchable<SObject>, Database.Stateful{

    public Map<Id, cscfga__Product_Configuration__c> oldProdConfNewProdConf = new Map<Id, cscfga__Product_Configuration__c>();
    public List<cscfga__Attribute__c> relatedProductAttributes = new List<cscfga__Attribute__c>();
    public Set<Id> PCsOfSelectedOrders = new Set<Id>();
    public Id bc_NewbasketId;
    public Id bc_OldbasketId;
    
    public CloneSubComponentsOfProductConfig(Id OldbasketId, Id NewbasketId, Map<Id, csord__Service__c> ServicesOfSelectedOrders){
        for(csord__Service__c srvc :ServicesOfSelectedOrders.Values()){
            if(srvc.csordtelcoa__Product_Configuration__c != null){
                PCsOfSelectedOrders.add(srvc.csordtelcoa__Product_Configuration__c);
            }
        }

        if(OldbasketId == null || NewbasketId == null){
            throw new cloneSubComponentsOfProductConfigException('Basket Id should not be Empty!');
        }
        this.bc_NewbasketId = NewbasketId;
        this.bc_OldbasketId = OldbasketId;
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        
        /** Initialize custom object to product configuration*/
        Set<Id> bc_OldbasketIds = new Set<Id>();
        bc_OldbasketIds.add(this.bc_OldbasketId);

        /** Return the iterable data, that will be processed by the steps of the batch.*/
        return Database.getQueryLocator(getAllFieldQuery('cscfga__Product_Configuration__c', bc_OldbasketIds));  
    }
    
    public void execute(Database.BatchableContext bc, List<cscfga__Product_Configuration__c> OldProdconfgObjList){
        TriggerFlags.NoConfigurationRequestTriggerHandler = true;
        TriggerFlags.NoProductConfigurationTriggers = true;
        TriggerFlags.NoProductBasketTriggers = true;
        TriggerFlags.NoAttributeTriggers = true;        
        List<csbb__Product_Configuration_Request__c> prodConfigRequests = new List<csbb__Product_Configuration_Request__c>();
        List<Pricing_Approval_Request_Data__c> newpricingApprovalList=new List<Pricing_Approval_Request_Data__c>();
        List<cscfga__Product_Configuration__c> NewProdconfgObjList = new List<cscfga__Product_Configuration__c>();
        List<cscfga__Product_Configuration__c> prodConfigs = new List<cscfga__Product_Configuration__c>();
        List<cscfga__Attribute__c> NewAttributeList = new List<cscfga__Attribute__c>();
        List<Attachment> NewAttachmentList = new List<Attachment>();
        csbb__Product_Configuration_Request__c prodConfigRequest = null;
        Pricing_Approval_Request_Data__c PARDNew = null;
        cscfga__Product_Configuration__c newPC = null;
        cscfga__Attribute__c newAtt = null;
        Attachment newAttachment = null;
        Set<Id> replacedServiceIds = new Set<Id>();
        Set<Id> productConfigIds = new Set<Id>();
        Set<Id> attachmentIds= new Set<Id>();
        Set<Id> PCIdSet = new Set<Id>();
        
        /** Check the number of items, throw if more than 1, due to heavy workload*/
        if(OldProdconfgObjList.Size() > 1){
            throw new cloneSubComponentsOfProductConfigException('Set the step size on 1 when executing this batch!');
        }

        /** Cloning of the product configuration started **/
        for(cscfga__Product_Configuration__c prod :OldProdconfgObjList){
            newPC = new cscfga__Product_Configuration__c();
            newPC = prod.clone(false,false,false,false);
            newPC.Name = prod.Name;
            newPC.cscfga__Total_Price__c = prod.cscfga__Total_Price__c;
            newPC.cscfga__Configuration_Status__c = 'Valid';
            newPC.cscfga__Description__c = prod.cscfga__Description__c; 
            newPC.cscfga__Quantity__c = prod.cscfga__Quantity__c;  
            newPC.csordtelcoa__Hierarchy_Level__c = prod.csordtelcoa__Hierarchy_Level__c;
            newPC.cscfga__Unit_Price__c = prod.cscfga__Unit_Price__c;  
            newPC.cscfga__Product_Basket__c = bc_NewbasketId;   
            newPC.cscfga__Recurrence_Frequency__c = prod.cscfga__Recurrence_Frequency__c; 
            newPC.cscfga__Contract_Term__c = prod.cscfga__Contract_Term__c;  
            newPC.cscfga__Contract_Term_Period__c = prod.cscfga__Contract_Term_Period__c; 
            newPC.cscfga__Product_Family__c = prod.cscfga__Product_Family__c;           
            newPC.Hierarchy__c = prod.Hierarchy__c;
            newPC.Hierarchy_Product_Sequence__c = prod.Hierarchy_Product_Sequence__c;   
            newPC.cscfga__Product_Definition__c = prod.cscfga__Product_Definition__c;
            newPC.CommaSepProdDetails__c = prod.CommaSepProdDetails__c;
            newPC.Product_Id__c =  prod.Product_Id__c;
            newPC.Inflight_Cloned_From_Id__c = prod.Id;/** Set the cloned from Id from which the new PC is cloned **/
            newPC.cscfga__Screen_Flow__c = null;
            newPC.Inflight_Clone__c = true;
            newPC.cscfga__Key__c = CreateTerminateOrderP2A.getUniqueExternalId(prod.Id, String.ValueOf(system.now()));
            newPC.csordtelcoa__config_key__c = CreateTerminateOrderP2A.getUniqueExternalId(bc_NewbasketId, String.ValueOf(system.now()));
            NewProdconfgObjList.add(newPC);
            productConfigIds.add(prod.Id);
        }
        if(NewProdconfgObjList.Size()>0){insert NewProdconfgObjList;}
        /** Cloning of the product configuration finish **/

        /** Set flag of the PCs to skip order creation for the non-selected orders **/
        for(cscfga__Product_Configuration__c pc :NewProdconfgObjList){
            oldProdConfNewProdConf.put(pc.Inflight_Cloned_From_Id__c, pc);/** Map to capture Old PC Id with the New PC **/
            pc.csordtelcoa__Ignore_For_Order_Decomposition__c = PCsOfSelectedOrders.contains(pc.Inflight_Cloned_From_Id__c)? false: true;
            pc.Inflight_Cloning_Id__c = pc.Id;/** Set PC Id of the cloned PC **/
            prodConfigs.add(pc);
            if(pc.csordtelcoa__Replaced_Service__c != null){
                replacedServiceIds.add(pc.csordtelcoa__Replaced_Service__c);
            }           
        }
        
        /** Replace Old PC with the New PC for the non selected orders **/
        replaceOldPCWithNewPC(oldProdConfNewProdConf, PCsOfSelectedOrders, replacedServiceIds);

        /** Cloning of the product configuration request started **/
        String prodConfRqstQuery = getAllFieldQuery('csbb__Product_Configuration_Request__c', productConfigIds);
        List<csbb__Product_Configuration_Request__c> OldProdConfRequestList = database.query(prodConfRqstQuery.replace('XXX','productConfigIds'));

        for(csbb__Product_Configuration_Request__c PCR :OldProdConfRequestList){
            prodConfigRequest = new csbb__Product_Configuration_Request__c();
            prodConfigRequest = PCR.clone(false,false,false,false);
            
            /** Replacing Old PC Id with the New PC Id **/
            if(PCR.csbb__Product_Configuration__c != null){
                prodConfigRequest.csbb__Product_Configuration__c = oldProdConfNewProdConf.get(PCR.csbb__Product_Configuration__c).Id;
            }
            prodConfigRequest.csbb__Product_Basket__c = bc_NewbasketId;
            prodConfigRequests.add(prodConfigRequest);
        }
        if(prodConfigRequests.Size()>0){insert prodConfigRequests;}
        /** Cloning of the product configuration request finish **/

        /** Cloning of the Attachment started **/
        String attachmentQuery = getAllFieldQuery('Attachment', productConfigIds);
        List<Attachment> OldAttachmentList = database.query(attachmentQuery.replace('XXX','productConfigIds'));

        for(Attachment attachmnt :OldAttachmentList){
            newAttachment = new Attachment();
            newAttachment = attachmnt.clone(false,false,false,false);
            newAttachment.ParentId = oldProdConfNewProdConf.get(attachmnt.ParentId).Id;
            NewAttachmentList.add(newAttachment);
        }
        if(NewAttachmentList.Size()>0){insert NewAttachmentList;}
        /** Cloning of the Attachment finish **/
        
        /** Cloning of the attributes started **/           
        String attributeQuery = getAllFieldQuery('cscfga__Attribute__c', productConfigIds);
        List<cscfga__Attribute__c> OldAttributeList = database.query(attributeQuery.replace('XXX','productConfigIds'));

        for(cscfga__Attribute__c att :OldAttributeList){
            newAtt = new cscfga__Attribute__c();           
            newAtt = att.clone(false,false,false,false);
            newAtt.cscfga__Key__c = CreateTerminateOrderP2A.getUniqueExternalId(att.Id, String.ValueOf(system.now()));
            
            if(att.Attribute_Screen_Config__c == 1){
                newAtt.Attribute_Value_IsChanged__c = true;
            }

            /** Replacing Old PC Id with the New PC Id **/
            if(att.cscfga__Product_Configuration__c != null){
                newAtt.cscfga__Product_Configuration__c = oldProdConfNewProdConf.get(att.cscfga__Product_Configuration__c).Id;
            }
            NewAttributeList.add(newAtt);
        }
        if(NewAttributeList.Size()>0){insert NewAttributeList;}
        /** Cloning of the attributes finish **/
        
        /** Capturing all the relted attributes to replace its Old PC Id with the New PC Id **/
        for(cscfga__Attribute__c att :NewAttributeList){
            if(att.Attribute_Type__c == 'Related Product'){
                relatedProductAttributes.add(att);
            }
        }
        
        /** Removing reference Id from oldbasket*/
        for(cscfga__Product_Configuration__c pc :OldProdconfgObjList){
           pc.Inflight_Cloning_Id__c = '';
           pc.Inflight_Cloned_From_Id__c = '';
           pc.Inflight_Clone__c = false;
           prodConfigs.add(pc);
        }
        if(prodConfigs.Size()>0){update prodConfigs;}
        
        /** Cloning of the pricing approval request data start **/
        String PARDQuery = getAllFieldQuery('Pricing_Approval_Request_Data__c', productConfigIds);
        List<Pricing_Approval_Request_Data__c> OldpricingApprovalList = database.query(PARDQuery.replace('XXX','productConfigIds'));
        
        for(Pricing_Approval_Request_Data__c PARD :OldpricingApprovalList){
            PARDNew = new Pricing_Approval_Request_Data__c();
            PARDNew = PARD.clone(false,false,false,false);
            
            /** Replacing Old PC Id with the New PC Id **/
            if(PARD.Product_Configuration__c != null){
                PARDNew.Product_Configuration__c = oldProdConfNewProdConf.get(PARD.Product_Configuration__c).Id;
            }
            PARDNew.Product_Basket__c = bc_NewbasketId;
            newpricingApprovalList.add(PARDNew);
        }
        if(newpricingApprovalList.Size()>0){insert newpricingApprovalList;}
        /** Cloning of the pricing approval request data finish **/
    }      
             
    public void finish(Database.BatchableContext bc){
        
        /** Replacing Old PC Id with the New PC Id for the newly cloned PCs**/
        List<cscfga__Product_Configuration__c> prodConfigs = new List<cscfga__Product_Configuration__c>();
        for(cscfga__Product_Configuration__c pc :oldProdConfNewProdConf.Values()){
            try{
                if(pc.cscfga__Parent_Configuration__c != null || pc.cscfga__Root_Configuration__c != null || pc.Master_IPVPN_Configuration__c != null){
                    pc.cscfga__Parent_Configuration__c = pc.cscfga__Parent_Configuration__c!=null? oldProdConfNewProdConf.get(pc.cscfga__Parent_Configuration__c).Id: null;
                    pc.cscfga__Root_Configuration__c = pc.cscfga__Root_Configuration__c!=null? oldProdConfNewProdConf.get(pc.cscfga__Root_Configuration__c).Id: null;
                    pc.Master_IPVPN_Configuration__c = pc.Master_IPVPN_Configuration__c!=null? oldProdConfNewProdConf.get(pc.Master_IPVPN_Configuration__c).Id: null;
                    prodConfigs.add(pc);
                }
            } catch(Exception e){
            
                ErrorHandlerException.ExecutingClassName='CloneSubComponentsOfProductConfig:finish';
            ErrorHandlerException.sendException(e);
            
            }
        }
        if(prodConfigs.Size()>0){update prodConfigs;}

        /** Replacing Old PC Id with the New PC Id for the newly clonned Related Product attributes **/
        for(cscfga__Attribute__c att :relatedProductAttributes){
            try{
                String relatedProduct = '';
                for(String attRL :att.cscfga__Value__c.split(',')){
                    relatedProduct = relatedProduct + oldProdConfNewProdConf.get(Id.ValueOf(attRL)).Id +',';
                }
                if(relatedProduct.subString(relatedProduct.Length()-1,relatedProduct.Length()) == ','){
                    relatedProduct = relatedProduct.subString(0,relatedProduct.Length()-1);
                }
                att.cscfga__Value__c = relatedProduct;
            } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='CloneSubComponentsOfProductConfig:finish';
            ErrorHandlerException.sendException(e);
            }
        }
        if(relatedProductAttributes.Size()>0){update relatedProductAttributes;}     

    }
    
    public class cloneSubComponentsOfProductConfigException extends Exception {}
    
    public void replaceOldPCWithNewPC(Map<Id, cscfga__Product_Configuration__c> oldProdConfNewProdConfig, Set<Id> PCIdsOfSelectedOrders, Set<Id> replacedServiceIds){
        Set<Id> PCIdsOfNonSelectedOrders = new Set<Id>();
        for(Id pc :oldProdConfNewProdConfig.KeySet()){
            if(!PCIdsOfSelectedOrders.contains(pc)){
                PCIdsOfNonSelectedOrders.add(pc);
            }
        }
        
        if(PCIdsOfNonSelectedOrders.Size() > 0 || replacedServiceIds.Size() > 0){
            List<csord__Service__c> serviceList = [Select Id, csord__Identification__c, csordtelcoa__Product_Configuration__c, csordtelcoa__Product_Basket__c, csordtelcoa__Replacement_Product_Configuration__c from csord__Service__c where csordtelcoa__Product_Configuration__c IN :PCIdsOfNonSelectedOrders or Id IN :replacedServiceIds];
            
            if(serviceList.Size() > 0){
                for(csord__Service__c service :serviceList){
                    if(service.csordtelcoa__Replacement_Product_Configuration__c != null 
                        && !replacedServiceIds.IsEmpty() 
                        && replacedServiceIds.contains(service.Id) 
                        && oldProdConfNewProdConf.containsKey(service.csordtelcoa__Replacement_Product_Configuration__c)){
                            service.csordtelcoa__Replacement_Product_Configuration__c = oldProdConfNewProdConf.get(service.csordtelcoa__Replacement_Product_Configuration__c).Id;
                    }
                    else if(!PCIdsOfNonSelectedOrders.IsEmpty() 
                        && PCIdsOfNonSelectedOrders.contains(service.csordtelcoa__Product_Configuration__c)){
                            service.csord__Identification__c = 'Service_' +oldProdConfNewProdConfig.get(service.csordtelcoa__Product_Configuration__c).Id +'_0';
                            service.csordtelcoa__Product_Configuration__c = oldProdConfNewProdConfig.get(service.csordtelcoa__Product_Configuration__c).Id;
                            service.csordtelcoa__Product_Basket__c = bc_NewbasketId;
                    }
                }
                update serviceList;
            }
        }
    }

    public String getAllFieldQuery(String ObjectName, Set<Id> IdSet){
        /** Initialize setup variables **/
        String query = 'SELECT';
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(ObjectName).getDescribe().fields.getMap();

        /** Grab the fields from the describe method and append them to the queryString one by one **/
        for(String s : objectFields.keySet()) {
            query += ' ' + s + ',';
        }

        /** Manually add related object's fields that are needed **/
        
        /** Strip off the last comma if it exists **/
        if(query.subString(query.Length()-1,query.Length()) == ','){
            query = query.subString(0,query.Length()-1);
        }

        /** Add FROM statement **/
        query += ' FROM ' + ObjectName;

        /** Add on a WHERE/ORDER/LIMIT statement as needed **/
        if(ObjectName == 'cscfga__Product_Configuration__c'){
            query += ' WHERE cscfga__Product_Basket__c IN :bc_OldbasketIds';
        } 
        else if(ObjectName == 'cscfga__Attribute__c'){
            query += ' WHERE cscfga__Product_Configuration__c In :XXX';
        }
        else if(ObjectName == 'Pricing_Approval_Request_Data__c'){
            query += ' WHERE Product_Configuration__c IN :XXX';    
        }
        else if(ObjectName == 'csbb__Product_Configuration_Request__c'){
            query += ' WHERE csbb__Product_Configuration__c IN :XXX';    
        }       
        else if(ObjectName == 'Attachment'){
            query += ' WHERE ParentId IN :XXX';
        }
        /** Modify as needed **/
        return query;
    }   
    
}