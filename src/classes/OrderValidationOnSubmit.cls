/*  
    @description - This class is used as controller to validate the order submission to Service Delivery.
*/
public class OrderValidationOnSubmit{
    public Id orderId {get;set;}
    public csord__Order__c ordr {get;set;}    
    
    public OrderValidationOnSubmit(ApexPages.StandardController controller){
        this.orderId = Apexpages.currentPage().getParameters().get('id');
        List<String> fieldLst = new List<String>();
        fieldLst.add('csord__Reference__c');
        fieldLst.add('csord__Account__r.region__C');
        fieldLst.add('Is_Order_Submitted__c');
        fieldLst.add('SD_PM_Contact__c');
        fieldLst.add('Clean_Order_Date__c');
        
        if(!Test.isRunningTest()){ //Added by Soham
            controller.addFields(fieldLst);
        }
        ordr = (csord__Order__c)controller.getRecord();
        orderId = ordr.id;
    }

    public PageReference validateOrdertoSubmit(){
        if(Ordr.Is_Order_Submitted__c == true){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Order Was already submited'));
        }
        else if(Ordr.SD_PM_Contact__c ==null || Ordr.Clean_Order_Date__c==null ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please Fill SD PM Name and Clean Order Date.'));
        }
        else{
            Ordr.Is_Order_Submitted__c = true;
            Ordr.Status__c = 'Submitted';
            if(Ordr.Is_Terminate_Order__c == true){
                Ordr.RecordTypeid = RecordTypeUtil.getRecordTypeIdByName(csord__Order__c.SObjectType, 'Terminate');
            }
            else{
                Ordr.RecordTypeid  = RecordTypeUtil.getRecordTypeIdByName(csord__Order__c.SObjectType, 'Inflight Order');
            }
            update ordr;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Order submitted successfully!!!'));
        }       
        return null;
    }

    private class OrderSubmitValidatorException extends Exception {}
}