global class ColoUtilityModel extends cscfga.ALookupSearch{
       
   public override String getRequiredAttributes()
        { 
            return '["ConfigId","BasketId","LinkedChilds"]';
        }
    

        public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID)
        {   
            List<cscfga__Attribute__c> ReturnList = new List<cscfga__Attribute__c>();
            List<cscfga__Attribute__c> DummyList = new List<cscfga__Attribute__c>();
            //List<cscfga__Attribute__c> UtilityModelList = new List<cscfga__Attribute__c>();
            try{
            string BasketId = searchFields.get('BasketId');
            string ConfigId = searchFields.get('ConfigId');
            string LinkedChilds = searchFields.get('LinkedChilds');
            
            List<String> LinkedChildsList = new List<String>();
            LinkedChildsList = LinkedChilds.split(',');
            //List<cscfga__Attribute__c> UtilityModelList = new List<cscfga__Attribute__c>();
            
            
            //UtilityModelList = [select name,cscfga__Product_Configuration__c,cscfga__Value__c,cscfga__Attribute_Definition__r.Name,cscfga__Product_Configuration__r.cscfga__Product_Basket__c from cscfga__Attribute__c where (cscfga__Attribute_Definition__r.Name='Utility_Model' AND cscfga__Product_Configuration__r.cscfga__Product_Basket__c=:BasketId AND cscfga__Product_Configuration__r.Name ='COLO-RACK' AND cscfga__Product_Configuration__c IN: LinkedChildsList) OR (cscfga__Attribute_Definition__r.Name='Utility_Model' AND cscfga__Product_Configuration__r.cscfga__Product_Basket__c=:BasketId AND cscfga__Product_Configuration__r.Name ='COLO-CAGE-RACK' AND cscfga__Product_Configuration__r.Master_IPVPN_Configuration__c IN: LinkedChildsList)];

            for(cscfga__Attribute__c s : [select name,cscfga__Product_Configuration__c,cscfga__Value__c,cscfga__Attribute_Definition__r.Name,cscfga__Product_Configuration__r.cscfga__Product_Basket__c from cscfga__Attribute__c where (cscfga__Attribute_Definition__r.Name='Utility_Model' AND cscfga__Product_Configuration__r.cscfga__Product_Basket__c=:BasketId AND cscfga__Product_Configuration__r.Name ='COLO-RACK' AND cscfga__Product_Configuration__c IN: LinkedChildsList) OR (cscfga__Attribute_Definition__r.Name='Utility_Model' AND cscfga__Product_Configuration__r.cscfga__Product_Basket__c=:BasketId AND cscfga__Product_Configuration__r.Name ='COLO-CAGE-RACK' AND cscfga__Product_Configuration__r.Master_IPVPN_Configuration__c IN: LinkedChildsList)]){
                if(s.cscfga__Value__c =='Utilities unbundled'){
                    ReturnList.add(s);
                }
            }
            if(ReturnList.size() == 0){
                DummyList = [select id,name,cscfga__Value__c from cscfga__Attribute__c where id=:ConfigId AND cscfga__Attribute_Definition__r.Name='StageName'];
            }
            system.debug('+++ReturnList+++'+ReturnList);
            }
            catch(Exception e){
                ErrorHandlerException.ExecutingClassName='ColoUtilityModel:doDynamicLookupSearch';
                ErrorHandlerException.sendException(e);
                system.debug(e);
                
            }
            if(ReturnList.size()>0){
                return ReturnList;
            }
            else{
                return DummyList;
            }
        }
        
}
