global class CS_VPLS_ASBR_As_ProductConfiguration  extends cscfga.ALookupSearch 
{
    public override String getRequiredAttributes()
    { 
        return '["Type Of NNI","Offnet CS Provider Id","BasketId","ConfigId","Master VPLS Configuration","ASBR Connection Type"]';
    }
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit)          
    {
        //has to return ASBR Product Configurations 
        //parameters are attributes from IPVPN Port
        
        string TypeOfNNI = searchFields.get('Type Of NNI');
        string OffnetCSProviderId = searchFields.get('Offnet CS Provider Id');
        string BasketId=searchFields.get('BasketId');
        string ConfigId=searchFields.get('ConfigId');
        string MasterServiceId=searchFields.get('Master VPLS Configuration');
        string ASBRConnectionType=searchFields.get('ASBR Connection Type');
        
        
        
        string searchValue = searchFields.get('searchValue') +'%';
            
        system.debug('***OffnetCSProviderId=' + OffnetCSProviderId);
        system.debug('***BasketId=' + BasketId);
        system.debug('***ConfigId=' + ConfigId);
        system.debug('***MasterServiceId=' + MasterServiceId);
        system.debug('***ASBRConnectionType=' + ASBRConnectionType);
        
        
        system.debug('***searchValue=' + searchValue);
        
        
        list<cscfga__Product_Configuration__c> lstASBRPC;
        
    
        if (excludeIds != null) 
        {
            lstASBRPC = [select Id,Name 
                from cscfga__Product_Configuration__c
                where cscfga__Product_Basket__c=:BasketId AND ASBR_NNI_Type__c=:TypeOfNNI
                AND cscfga__Product_Definition__r.Name='ASBR'
                AND Type_of_ASBR__c = 'Standard'
                AND Id not in :excludeIds];         
        }
        else
        {
            lstASBRPC = [select Id,Name 
                from cscfga__Product_Configuration__c
                where cscfga__Product_Basket__c=:BasketId AND ASBR_NNI_Type__c=:TypeOfNNI
                AND Type_of_ASBR__c = 'Standard'
                AND cscfga__Product_Definition__r.Name='ASBR'];
        }
    
        
        system.debug('***lstASBRPC=' + lstASBRPC);
        
        set<Id> setPCASBRId = new set<Id>();
        
        for(cscfga__Product_Configuration__c tmpPC : lstASBRPC)
        {
            setPCASBRId.add(tmpPC.Id);
        }
        
        system.debug('***setPCASBRId=' + setPCASBRId);
        
        //now we have a set of ASBR Product Configuration Ids - these are potential for returns but...
        //so we will check if there are IPVPN Ports in the same Product Basket that have different OffnetProvider
        //those who have - have to be excluded
        
        list<cscfga__Product_Configuration__c> lstIPVPNPOrtPC;
        
        if (setPCASBRId.size()>0)
        {
            if (ConfigId!='')
            {
                lstIPVPNPOrtPC = [select Id,ASBR_Product_Configuration__c, Master_IPVPN_Configuration__c, ASBR_Connection_Type__c, CS_Provider__c 
                    from cscfga__Product_Configuration__c
                    where cscfga__Product_Basket__c=:BasketId AND ASBR_Product_Configuration__c != null AND Id<>:ConfigId];
            }
            else
            {
                lstIPVPNPOrtPC = [select Id,ASBR_Product_Configuration__c, Master_IPVPN_Configuration__c, ASBR_Connection_Type__c, CS_Provider__c 
                    from cscfga__Product_Configuration__c
                    where cscfga__Product_Basket__c=:BasketId AND ASBR_Product_Configuration__c != null];              
            }   
            
            system.debug('***lstIPVPNPOrtPC=' + lstIPVPNPOrtPC);
            //now we have a list of ports which have some ASBR assigned... 
            //as ASBR are assigned it meant that they must have same Master Service, same ASBR Connection Type, same Provider
            
            if ((OffnetCSProviderId=='') || (OffnetCSProviderId==null)) 
            {
                setPCASBRId.clear();
            }
            else
            {
                for (cscfga__Product_Configuration__c tmpPC : lstIPVPNPOrtPC)
                {
                    
                    
                    
                    system.debug('3***OffnetCSProviderId=' + OffnetCSProviderId);
                    system.debug('3***tmpPC.CS_Provider__c=' + tmpPC.CS_Provider__c);
                    system.debug('3***MasterServiceId=' + MasterServiceId);
                    system.debug('3***tmpPC.Master_IPVPN_Configuration__c=' + tmpPC.Master_IPVPN_Configuration__c);
                    system.debug('3***ASBRConnectionType=' + ASBRConnectionType);
                    system.debug('3***tmpPC.ASBR_Connection_Type__c=' + tmpPC.ASBR_Connection_Type__c);
                    
                    
                    
                    
                    if ((OffnetCSProviderId!=tmpPC.CS_Provider__c) || (MasterServiceId!=tmpPC.Master_IPVPN_Configuration__c) || (ASBRConnectionType!=tmpPC.ASBR_Connection_Type__c))
                    {
                        String[] ASBRIDs = tmpPC.ASBR_Product_Configuration__c.split(',');

                        for (Id item : ASBRIDs){
                            if(setPCASBRId.contains(item)){  
                                setPCASBRId.remove(item);
                            }  
                        }
                    }
                }
            }
        }
        
        lstASBRPC = new list<cscfga__Product_Configuration__c>();
        
        system.debug('***2***setPCASBRId=' + setPCASBRId);
        
        if (setPCASBRId.size()>0)
        {
            lstASBRPC=[select Id,Name, CS_Provider__r.Name, ASBR_NNI_Type__c, Service_Bill_Text__c from cscfga__Product_Configuration__c 
                where Id in: setPCASBRId AND Name LIKE :searchValue ORDER BY Name];
        }
        
        system.debug('***lstASBRPC=' + lstASBRPC);
        
        return lstASBRPC;
        
    }    
    
}