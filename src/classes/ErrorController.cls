public with sharing class ErrorController{

public List<Error__c> ErrorList {get;set;}
public List<ErrorWrapper>ErrorWrapList {get;set;}
public List<ErrorWrapper>paginatedErrorWrapList {get;set;}
public String queryString{get;set;}
public String logsStatus{get;set;}
public Date StartDate{get;set;}
public Date EndDate{get;set;}
public List<Error__c> deleteList{get;set;}
public List<sobject>objectList{ get; set; }
public List<List<String>>StringList { get; set; }
public List<String>varList {get;set;}
public List<String>querySplitFinal {get;set;}
public boolean Mailer {get;set;}
public boolean Logger {get;set;}
public boolean showFirstTable{get;set;}
public boolean showSecondTable {get;set;}
public boolean isRecordObtained{get;set;}
public String EmailRecipients {get;set;}//email id with comma separated value
public String LastModifiedBy{get;set;}
public String LastModifiedTime{get;set;}
public static Integer paginationSize=20;
public Integer currentPage{get;set;}
public Integer totalPage{get;set;}
public Integer totalRecords{get;set;}
public set<String>queryFieldSet{get;set;}
public Boolean isEditable{get;set;}

//fields declaration
public Boolean Field_Name{get;set;}
public Boolean Field_Log_Time{get;set;}
public Boolean Field_Log_Date{get;set;}
public Boolean Field_Class_Name{get;set;}
public Boolean Field_Object_Id{get;set;}
public Boolean Field_Object_Name{get;set;}
public Boolean Field_Object_Type{get;set;}
public Boolean Field_Stack_Trace{get;set;}

//sorting declaration
public String Sort_Name{get;set;}
public String Sort_Log_Time{get;set;}
public String Sort_Log_Date{get;set;}
public String Sort_Class_Name{get;set;}
public String Sort_Object_Id{get;set;}
public String Sort_Object_Name{get;set;}
public String Sort_Object_Type{get;set;}
public String Sort_Stack_Trace{get;set;}

public String SobjName {get;set;}
public String AvailableString{get;set;}
public Map<String,String>SobjectMap{get;set;}
public Map<String,String>SobjectFieldValMap{get;set;}
public Map<String,String>SelectedFieldMap{get;set;}
public List<String>AvailableFieldList{get;set;}
public List<String>SelectedFieldList{get;set;}
public List<String>removableFieldList{get;set;}
public String defaultObject='Error__c';


public ErrorController(){
StartDate=system.today();
EndDate=system.today();
ErrorHandlerMechanism__c TriageList=ErrorHandlerMechanism__c.getvalues('Logger');
Mailer=TriageList.EnableLog__c;
Logger=TriageList.EnableMailer__c;
EmailRecipients=TriageList.EmailRecipient__c;
LastModifiedBy=TriageList.Last_Modified_By__c;
LastModifiedTime=string.valueOf(TriageList.Last_Modified_Time__c);
SobjName=defaultObject;//default value
SobjectMap=new Map<String,String>();
SobjectFieldValMap=new Map<String,String>();
SelectedFieldMap=new Map<String,String>();
SelectedFieldList=new List<String>();
removableFieldList=new List<String>();
AvailableFieldList=new List<String>();
AvailableString='';
getSobjectList();
getSobjectfieldList(SobjName);
initialise();
queryResult();
}

public pagereference getQueryFromSelectedFields(){

try{
String tempQuerystring='';

if(!SelectedFieldMap.keyset().isEmpty()){
for(String str:SelectedFieldMap.keyset()){
tempQuerystring=tempQuerystring+str+',';
}
tempQuerystring=tempQuerystring.removeEnd(',');
tempQuerystring='select '+tempQuerystring+ ' from '+ SobjName;
if(StartDate!=null && EndDate!=null){
tempQuerystring=tempQuerystring+' '+'where created_date__c > '+String.valueOf(StartDate)+' '+' and '+'created_date__c < ' +String.valueOf(EndDate);
}
}
queryString=tempQuerystring;
return null;
}catch(Exception e){
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
return null;
}
}


public pagereference getQueryString(){

String tempQuerystring='';
queryFieldSet=new Set<String>();

if(Field_Name)queryFieldSet.add('Name');
if(Field_Log_Time)queryFieldSet.add('Log_Time__c');
if(Field_Log_Date)queryFieldSet.add('Log_Date__c');
if(Field_Class_Name)queryFieldSet.add('Class_Name__c');
if(Field_Object_Id)queryFieldSet.add('Name');
if(Field_Object_Name)queryFieldSet.add('Object_Id__c');
if(Field_Object_Type)queryFieldSet.add('Object_Type__c');
if(Field_Stack_Trace)queryFieldSet.add('Stack_Trace__c');

if(!queryFieldSet.isEmpty()){
for(String str:queryFieldSet){
tempQuerystring=str+',';
}
tempQuerystring=tempQuerystring.removeEnd(',');
tempQuerystring='select '+tempQuerystring+ 'from Error__c';
}
queryString=tempQuerystring;
return null;
}

public pagereference queryResult(){
queryString='select name,Log_Time__c,Log_Date__c,Class_Name__c,Object_ID__c,Object_Name__c,Object_Type__c,Stack_Trace__c from error__c';
if(StartDate!=null && EndDate!=null){
queryString=queryString+' '+'where created_date__c > '+String.valueOf(StartDate)+' '+' and '+'created_date__c < ' +String.valueOf(EndDate);
}
System.debug('queryString'+queryString);
//ErrorList = database.query(queryString);

return null;
}

public pagereference getRecords(){

try{
queryString=queryString.trim();
objectList =database.query(queryString);
integer startIndex=queryString.indexOf('select ');//size is 8
integer lastIndex=queryString.indexOf('from ');
String temp=queryString.substring(7,lastIndex);
List<String>querySplit=temp.split(',');
querySplitFinal=new List<String>();
for(String q:querySplit){
    String demo=q.trim();
    querySplitFinal.add(demo);
    
}
Map<integer,sobject>idMap=new Map<integer,sobject>();
StringList=new List<List<String>>();
ErrorWrapList=new List<ErrorWrapper>();
integer k=0;
 for(sobject obj:objectList){ 
    if(obj!=null){   
    ErrorWrapper er=new ErrorWrapper(false,obj,k);  
    ErrorWrapList.add(er); 
    }   
   k++;
  }
isRecordObtained=ErrorWrapList.size()>0?true:false;
showFirstTable=true; 
currentPage=1;
totalRecords=ErrorWrapList.size();
getPaginationRecords(currentPage);
return null;
}catch(Exception e){
isEditable=true;
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
return null;
}
}

public void getPaginationRecords(Integer currentPageVal){
//integer startVal=((currentPageVal-1)*paginationSize)+1;
integer startVal=((currentPageVal-1)*paginationSize);
integer endVal=startVal+paginationSize;
integer counter=0;
totalPage=Math.mod(ErrorWrapList.size(),paginationSize)==0?ErrorWrapList.size()/paginationSize:(ErrorWrapList.size()/paginationSize)+1;
paginatedErrorWrapList=new List<ErrorWrapper>();

for(ErrorWrapper eList:ErrorWrapList){
if(counter>=startVal && counter<=endval){
paginatedErrorWrapList.add(eList);
}
counter++;
}
}
public void firstPage(){
currentPage=1;
getPaginationRecords(currentPage);
}
public void lastPage(){
currentPage=totalPage;
getPaginationRecords(currentPage);
}
public void nextPage(){
currentPage=currentPage<totalPage?currentPage+1:currentPage;
getPaginationRecords(currentPage);
}
public void previousPage(){
currentPage=currentPage>1?currentPage-1:currentPage;
getPaginationRecords(currentPage);
}

//method exportData starts
public pagereference exportData(){
PageReference pageRef = new PageReference('/apex/TableExport');
pageRef.setRedirect(false);
return pageRef;
}
//method exportData ends

public pagereference deleteSelectedRecords(){
List<sobject>deleteList=new List<sobject>();

for(ErrorWrapper ew:ErrorWrapList){
if(ew.selectedProduct && ew.obj!=null){
    deleteList.add(ew.obj);    
}    
}

if(deleteList.size()>0){delete deleteList;}
getRecords();
return null;    
}


public pagereference editRecords(){
showFirstTable=false;
showSecondTable=true;
isEditable=true;
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'The table is now Editable !'));
return null;
}


public pagereference updateRecords(){

List<sobject>updateList=new List<sobject>();

for(ErrorWrapper ew:paginatedErrorWrapList){
if(ew.obj!=null){
    updateList.add(ew.obj);    
}
     
}

//ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error Occured: '+updateList));
//return null;

if(updateList.size()>0){update updateList;}
showFirstTable=true;
showSecondTable=false;
isEditable=false;
getRecords();
return null;    
}

public pagereference updateLoggingConfig(){
ErrorHandlerMechanism__c TriageList=[select EnableLog__c,Last_Modified_By__c,Last_Modified_Time__c,EnableMailer__c,EmailRecipient__c from ErrorHandlerMechanism__c where name='logger' limit 1];
TriageList.EnableLog__c=Mailer;
TriageList.EnableMailer__c=Logger;
TriageList.EmailRecipient__c=EmailRecipients;
TriageList.Last_Modified_By__c=UserInfo.getName();
TriageList.Last_Modified_Time__c=system.now();
LastModifiedBy=UserInfo.getName();
LastModifiedTime=String.valueOf(system.now());
update TriageList;

return null;    
}

public class ErrorWrapper{
    public boolean selectedProduct {get;set;}    
    public sobject obj {get;set;}
    public integer index {get;set;}
    
public  ErrorWrapper(Boolean selectedProduct,sobject obj,integer index){
this.selectedProduct=selectedProduct;
this.obj=obj;  
this.index=index; 
}

}


public void initialise(){ 
String defaultStr='Name,Log_Time__c,Log_Date__c,Executing_Class_Name__c,Class_Name__c,Object_ID__c,Object_Name__c,Object_Type__c,Stack_Trace__c';
AvailableFieldList=defaultStr.split(',');
for(string st:AvailableFieldList){
    if(SobjectFieldValMap.containsKey(st)){
     SobjectFieldValMap.remove(st);
    }
    SelectedFieldMap.put(st,st);
}

}
 public void getSobjectList(){      
      Map<String,schema.SObjectType>sg=Schema.getGlobalDescribe();
        for(String str:sg.keyset()){
           schema.DescribeSObjectResult sr=sg.get(str).getDescribe();
            System.debug('sr.getLabel()'+sr.getLabel());
            System.debug('sr.getName()'+sr.getName());
            SobjectMap.put(sr.getLabel(),sr.getName());  
        }
    }
    public pagereference getfieldsFromSobject(){
      SelectedFieldList=new List<String>();
      AvailableFieldList=new List<String>();
      SelectedFieldMap=new Map<String,String>();
      getSobjectfieldList(SobjName);
      if(SobjName==defaultObject){
      initialise();
      } 
      return null;
    }
    
    
    
    public void getSobjectfieldList(String str){
        //String str='csord__service__c';//api name of sobject
        
        SobjectFieldValMap=new Map<String,String>();
        set<String>AvailableStringSet=new Set<String>(AvailableFieldList);
        Map<String,schema.SObjectType>sg=Schema.getGlobalDescribe();
        schema.DescribeSObjectResult sr=sg.get(str).getDescribe();
        Map<String, Schema.SObjectField> sf = sr.fields.getMap();
        for(Schema.SObjectField s1:sf.values()){  
                Schema.DescribeFieldResult  sf1= s1.getDescribe(); 
                String FLabel=sf1.getLabel();
                String FName=sf1.getName();
                system.debug('FLabel'+FLabel);
                system.debug('FName'+FName);               
                SobjectFieldValMap.put(FName,FName);
            }            
    }

     public List<SelectOption> getitems() {
            
            List<SelectOption> options = new List<SelectOption>();
            List<String>SortedTempList=new List<String>(SobjectMap.keyset());
            SortedTempList.sort();
            integer counter=0;
            for(String st:SortedTempList){
             options.add(new SelectOption(SobjectMap.get(St),st));
             counter++;
             //if(counter==5)break;
             
            }           
            return options;
        }
        
        
    public pagereference test(){
    return null;
    }
    
    public pagereference addFields(){
    System.debug('Add fields AvailableFieldList'+AvailableFieldList);
    for(string st:AvailableFieldList){
        if(SobjectFieldValMap.containsKey(St)){
           SobjectFieldValMap.remove(st);
        }
        SelectedFieldMap.put(st,st);
    }
    return null;
    }
    
    public pagereference removeFields(){
    System.debug('Add fields AvailableFieldList'+AvailableFieldList);
    for(string st:removableFieldList){
        SobjectFieldValMap.put(st,st);
        if(SelectedFieldMap.containsKey(St)){
        SelectedFieldMap.remove(st);
        }
    }
    return null;
    }
    
    
    public List<SelectOption> getinputitems() {
                       
            List<SelectOption> options = new List<SelectOption>();
            integer counter=0;
            List<String>SortedTempList=new List<String>(SobjectFieldValMap.keyset());
            SortedTempList.sort();
            if(SobjectFieldValMap.size()>0){
            for(String st:SortedTempList){
             options.add(new SelectOption(SobjectFieldValMap.get(St),SobjectFieldValMap.get(St)));
             counter++;
             //if(counter==5)break;             
            }
            }
            else{
            options.add(new SelectOption('blank','blank'));
            }           
            return options;
        }
        
         public List<SelectOption> getoutputitems() {
            
            List<SelectOption> options = new List<SelectOption>();       
             System.debug('ritzz AvailableFieldList'+AvailableFieldList);
                         
            for(String st:SelectedFieldMap.values()){
             options.add(new SelectOption(st,st));                       
            }
                       
            return options;
        }
}