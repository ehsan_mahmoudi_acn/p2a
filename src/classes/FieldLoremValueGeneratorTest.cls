@isTest
private class FieldLoremValueGeneratorTest{

static testMethod void unittest() {
        
        CurrencyValue__c cv = new CurrencyValue__c();
        cv.CreatedDate = Datetime.now();
        cv.name = 'Currency';
        cv.Currency_Value__c = 45;
        insert cv;
        System.assertEquals(45,cv.Currency_Value__c );
        
        Blob blobValue = EncodingUtil.convertFromHex('4A4B4C');
        system.debug('--blobValue--'+blobValue);
        System.assertEquals('JKL', blobValue.toString());
        
        
        Schema.DescribeFieldResult SdFre = CurrencyValue__c.Currency_Value__c.getDescribe();
        
        Test.startTest();
            FieldLoremValueGenerator fdvg = new FieldLoremValueGenerator();  
            Boolean  b = fdvg.canGenerateValueFor(SdFre);
            //object obj = fdvg.generate(SdFre);
            String s= fdvg.randomString(5);
          
        Test.stopTest();
    
    }

}