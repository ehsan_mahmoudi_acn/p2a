@isTest(SeeAllData = false)
private class GCCPriceItemQueryTest{

    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    
    private static List<Account> accountList;
    private static List<IPL_Rate_Card_Bandwidth_Join__c> iplRateCardBandwidthJoinList;
    private static List<cspmb__Price_Item__c> priceItemList;
    private static List<CloudSense_Bandwidth__c> bandwidthList;
    private static List<IPL_Rate_Card_Item__c> iplRateCardItemList;
    
    
    private static void initTestData(){
        
        accountList = new List<Account>{
            new Account(Name = 'Account 1', Customer_Type_New__c = 'MNC'),
            new Account(Name = 'Account 2', Customer_Type_New__c = 'MNC')
        };
        
        insert accountList;  
list<Account> prod = [select id,Name from Account where Name = 'Account 1'];
        system.assertEquals(accountList[0].Name , prod[0].Name);
        System.assert(accountList[0]!=null);
        
        
        bandwidthList = new List<CloudSense_Bandwidth__c>{
            new CloudSense_Bandwidth__c(Name = '64k'),
            new CloudSense_Bandwidth__c(Name = '128k')
        };
        
        insert bandwidthList;
        list<CloudSense_Bandwidth__c> prod1 = [select id,Name from CloudSense_Bandwidth__c where Name = '64k'];
        system.assertEquals(bandwidthList[0].Name , prod1[0].Name);
        System.assert(bandwidthList[0]!=null);

        
        iplRateCardItemList = new List<IPL_Rate_Card_Item__c>{
            new IPL_Rate_Card_Item__c(Name = 'Rate Card 1', Product_Type__c = 'IPL'),
            new IPL_Rate_Card_Item__c(Name = 'Rate Card 2', Product_Type__c = 'IPL')
        };
        
        insert iplRateCardItemList;
        list<IPL_Rate_Card_Item__c> prod2 = [select id,Name from IPL_Rate_Card_Item__c where Name = 'Rate Card 1'];
        system.assertEquals(iplRateCardItemList[0].Name , prod2[0].Name);
        System.assert(iplRateCardItemList[0]!=null);

        
        iplRateCardBandwidthJoinList = new List<IPL_Rate_Card_Bandwidth_Join__c>{
            new IPL_Rate_Card_Bandwidth_Join__c(Bandwidth__c = bandwidthList[0].Id, IPL_Rate_Card_Item__c = iplRateCardItemList[0].Id),
            new IPL_Rate_Card_Bandwidth_Join__c(Bandwidth__c = bandwidthList[1].Id, IPL_Rate_Card_Item__c = iplRateCardItemList[1].Id)
        };
        
        insert iplRateCardBandwidthJoinList;
       
        system.assertEquals(iplRateCardBandwidthJoinList[0].Bandwidth__c , bandwidthList[0].Id);
        System.assert(iplRateCardBandwidthJoinList[0]!=null);

        
        priceItemList = new List<cspmb__Price_Item__c>{
            new cspmb__Price_Item__c(Name = 'Price Item 1', IPL_Rate_Card_Bandwidth_Join__c = iplRateCardBandwidthJoinList[0].Id, cspmb__Account__c = accountList[0].Id),
            new cspmb__Price_Item__c(Name = 'Price Item 2', IPL_Rate_Card_Bandwidth_Join__c = iplRateCardBandwidthJoinList[1].Id, cspmb__Account__c = null)
        };
        
        insert priceItemList;
        list<cspmb__Price_Item__c> prod4 = [select id,Name from cspmb__Price_Item__c where Name = 'Price Item 1'];
        system.assertEquals(priceItemList[0].Name , prod4[0].Name);
        System.assert(priceItemList[0]!=null);

                
    }
    
  private static testMethod void doDynamicLookupSearchTest() {
        Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
            
            initTestData();
            
            searchFields.put('Contract Term', bandwidthList[0].Id);
            searchFields.put('Account Id', accountList[0].Id);
            searchFields.put('RateCardIDTemp', iplRateCardItemList[0].Id);
            
            GCCPriceItemQuery gccpriceItemQuery = new GCCPriceItemQuery();
            Object[] data = gccpriceItemQuery.doDynamicLookupSearch(searchFields, productDefinitionId);
            String reqAtts = gccpriceItemQuery.getRequiredAttributes();
            
            System.debug('*******Data: ' + data);
          system.assertEquals(true,data!=null);
           
         
           
            
           
            
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='GCCPriceItemQueryTest :After doDynamicLookupSearchTest';    
           ErrorHandlerException.sendException(e);

        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
  }

}