@isTest(SeeAllData = false)
private class BandwidthLookupTest {

    private static Map<String, String> searchFields = new Map<String, String>();
    private static List<IPL_Rate_Card_Bandwidth_Join__c> iplRateCardBandwidthJoinList;
    private static List<cspmb__Price_Item__c> priceItemList;
    private static List<CloudSense_Bandwidth__c> bandwidthList;
    private static List<IPL_Rate_Card_Item__c> iplRateCardItemList;
    
    private static void initTestData(){
        bandwidthList = new List<CloudSense_Bandwidth__c>{
            new CloudSense_Bandwidth__c(Name = '64k'),
            new CloudSense_Bandwidth__c(Name = '128k')
        };
        
        insert bandwidthList;
        list<CloudSense_Bandwidth__c> band = [select id,Name from CloudSense_Bandwidth__c where Name = '64k'];
        system.assertEquals(bandwidthList[0].Name , band[0].Name);
        System.assert(bandwidthList!=null); 
        
        iplRateCardItemList = new List<IPL_Rate_Card_Item__c>{
            new IPL_Rate_Card_Item__c(Name = 'Rate Card 1', Product_Type__c = 'IPL'),
            new IPL_Rate_Card_Item__c(Name = 'Rate Card 2', Product_Type__c = 'IPL')
        };
        
        insert iplRateCardItemList;
        list<IPL_Rate_Card_Item__c> iplrate = [select id,Name from IPL_Rate_Card_Item__c where Name = 'Rate Card 1'];
        system.assertEquals(iplRateCardItemList[0].Name , iplrate[0].Name);
        System.assert(iplRateCardItemList!=null); 
        
        iplRateCardBandwidthJoinList = new List<IPL_Rate_Card_Bandwidth_Join__c>{
            new IPL_Rate_Card_Bandwidth_Join__c(Bandwidth__c = bandwidthList[0].Id, IPL_Rate_Card_Item__c = iplRateCardItemList[0].Id),
            new IPL_Rate_Card_Bandwidth_Join__c(Bandwidth__c = bandwidthList[1].Id, IPL_Rate_Card_Item__c = iplRateCardItemList[1].Id)
        };
        
        insert iplRateCardBandwidthJoinList;
        system.assertEquals(true,iplRateCardBandwidthJoinList!=null); 
    }
    
    
    private static testMethod void doLookupSearchTest() {
        Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
            
            initTestData();
            
            searchFields.put('RateCardIDTemp', iplRateCardItemList[0].Id);
            searchFields.put('searchValue', '');
            Id[] excludeIds; 
            Integer pageOffset, pageLimit;
            String productDefinitionId;

            BandwidthLookup bndLookup = new BandwidthLookup();
          // Object[] data = bndLookup.doDynamicLookupSearch(searchFields, productDefinitionId);
            Object[] data = bndLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            String reqAtts = bndLookup.getRequiredAttributes();
            
              data = bndLookup.doDynamicLookupSearch(searchFields, productDefinitionId);
            System.debug('*******Data: ' + data);
            system.assertEquals(true,data!=null);   
             Test.stopTest();
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='BandwidthLookupTest :doLookupSearchTest';         
            ErrorHandlerException.sendException(e); 
            ee = e;
        } finally {
           
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
    }

}