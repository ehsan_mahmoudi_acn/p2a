@isTest(SeeAllData=false) 
public class RemoveCloneReferenceTest
{
    static testMethod void runCloneTestMethod()
    {
         //P2A_TestFactoryCls.disableAll(UserInfo.getUserId()); 
         P2A_TestFactoryCls.sampleTestData();
         List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
         List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1 , accList);
         list<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
         List<cscfga__Product_Definition__c>ListProductdef =  P2A_TestFactoryCls.getProductdef(1);
         
         //List<cscfga__Product_Configuration__c> Listconfig = P2A_TestFactoryCls.getProductonfig(1,prodBaskList,ListProductdef ,pbundlelist,Listconfigoffer);
         
         //test.startTest();
         cscfga__Product_Configuration__c pc=new cscfga__Product_Configuration__c();
         pc.cscfga__Product_basket__c = prodBaskList[0].id ;
         pc.cscfga__Product_Definition__c =ListProductdef[0].id ;
         insert pc;
         system.assert(pc!=null);
         
         List<cscfga__Product_Configuration__c> Listconfig=new List<cscfga__Product_Configuration__c>();
         cscfga__Product_Configuration__c pc1=new cscfga__Product_Configuration__c();
         pc1.cscfga__Product_basket__c = prodBaskList[0].id ;
         pc1.cscfga__Product_Definition__c =ListProductdef[0].id ;
         pc1.csordtelcoa__Replaced_Product_Configuration__c=pc.id;
         pc1.Inflight_Primary_Service_Id__c='abcd';
         insert pc1;
         List<cscfga__Product_Configuration__c> pclist = [select cscfga__Product_basket__c,cscfga__Product_Definition__c,csordtelcoa__Replaced_Product_Configuration__c,
                                                          Inflight_Primary_Service_Id__c from cscfga__Product_Configuration__c where Inflight_Primary_Service_Id__c='abcd'];
         system.assert(pclist!=null);
         system.assertEquals(pclist[0].Inflight_Primary_Service_Id__c, 'abcd');
         cscfga__Product_Configuration__c pc2=new cscfga__Product_Configuration__c();
         pc2.cscfga__Product_Basket__c = prodBaskList[0].id ;
         pc2.cscfga__Product_Definition__c =ListProductdef[0].id ;
         pc2.csordtelcoa__Replaced_Product_Configuration__c=pc.id;
         pc2.Inflight_Primary_Service_Id__c='xyz';
         pc2.product_Id__c=null;

         insert pc2;
         system.assert(pc2!=null);
         system.assertEquals(pc2.Inflight_Primary_Service_Id__c, 'xyz');
             
         pc2.product_Id__c=null;
         update pc2;
         system.assert(pc2!=null);
         test.startTest();
         AllProductConfigurationTriggerHandler allProdConf = new AllProductConfigurationTriggerHandler();
         Map<Id,cscfga__Product_Configuration__c>oldMap=new Map<Id,cscfga__Product_Configuration__c>();
         oldMap.put(pc2.id,pc2);
         
         pc2.product_Id__c='IPL';
         update pc2;
         system.assert(pc2!=null);
         
         List<cscfga__Product_Configuration__c>newList=new List<cscfga__Product_Configuration__c>();
         newList.add(pc2);         
         
         RemoveCloneReference.setCloneReferenceForChangeOrder(newList);
         RemoveCloneReference.removeCloneReferenceForChangeOrder(newList);
         
         test.stopTest();
}}