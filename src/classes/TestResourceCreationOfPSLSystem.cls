@isTest

public class TestResourceCreationOfPSLSystem{
    static Testmethod void resourceCreationOfPSLSystem(){
    
             Resource__c res=new Resource__c();
             res.name='res1';
             res.AccountId__c=getAccount().id;
             res.Path_Instance_ID__c='222';
             res.Parent_Path_Instance_ID__c='111';
             res.Product__c  = getProduct().id;
             insert res;
             system.assert(res!=null);
         list<Resource__c> lstres=new list<Resource__c>();
         lstres.add(res);
         
         //ResourceCreationOfPSLSystem test=new ResourceCreationOfPSLSystem();
         //test.resourceCreation(lstres);          
    
    
    }
    
   private static Account getAccount(){
        Account acc = new Account();
        Country_Lookup__c cl = getCountry();
        acc.Name = 'Test Account';
        acc.Customer_Type__c = 'MNC';
        acc.Country__c = cl.Id;
        acc.Selling_Entity__c = 'Telstra INC';
        acc.Activated__c= true;
        acc.Account_Id__c ='12123';
        acc.Customer_Legal_Entity_Name__c='test';
        insert acc;
        system.assert(acc!=null);
        return acc;
    }
   private static Country_Lookup__c getCountry(){
        Country_Lookup__c c2 = new Country_Lookup__c();
        //c2.CCMS_Country_Code__c = 'IND';
        //c2.CCMS_Country_Name__c = 'India';
        c2.Country_Code__c = 'xxx';
        insert c2;
        system.assert(c2!=null);
        return c2;
    }            
    private static Product2 getProduct(){
            
            Product2 prod1 = new Product2();
            prod1.name = 'Test Product';
            prod1.CurrencyIsoCode='EUR';
            prod1.Product_ID__c='IPL';        
            prod1.Create_Resource__c=TRUE;
            insert prod1;
            system.assert(prod1!=null);
            return prod1;
          
     }       


}