global with sharing class CS_BandwidthValueProductTypeLookup extends cscfga.ALookupSearch {
	
    public override String getRequiredAttributes(){ 
        return '[ "Product Type", "Bandwidth Value MB" ]';
    }
	
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){
        
        /*List <String> ProductTypeNames = new List<String>();
        for (String key : searchFields.keySet()){
                String attValue = searchFields.get(key);
                if(attValue == 'Yes' || attValue == 'true')
                ProductTypeNames.add(key);
        }
		String searchValue = searchFields.get('searchValue') +'%';*/
        List <CS_Bandwidth__c> data = [SELECT Id, Name, Bandwidth_Value_MB__c FROM CS_Bandwidth__c ORDER BY Bandwidth_Value_MB__c];
        
        return data;
    }
    
    public override Object[] doLookupSearch(Map<String, String> searchFields
    	, String productDefinitionId
    	, Id[] excludeIds
    	, Integer pageOffset
    	, Integer pageLimit) {
        
        final Integer SELECT_LIST_LOOKUP_PAGE_SIZE = 25;
        final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = 26;
        Integer recordOffset = pageOffset * SELECT_LIST_LOOKUP_PAGE_SIZE;
        
        System.Debug('doLookupSearch');
        System.Debug(searchFields);
        String productType = searchFields.get('Product Type');
        Decimal bandwidthValueMB = searchFields.get('Bandwidth Value MB') != null 
        	? Decimal.valueOf(searchFields.get('Bandwidth Value MB')) 
        	: null;
        String searchValue = searchFields.get('searchValue') != null 
        	? ('%' + searchFields.get('searchValue') +'%') 
        	: null;
        
        System.Debug('doLookupSearch - productType: ' + productType);
        System.Debug('doLookupSearch - bandwidthValueMB: ' + bandwidthValueMB);
        System.Debug('doLookupSearch - searchValue: ' + searchValue);
    	System.Debug('doLookupSearch - pageOffset: ' + pageOffset);
        System.Debug('doLookupSearch - recordOffset: ' + recordOffset);
        System.Debug('doLookupSearch - pageLimit: ' + pageLimit);
        
        String query = 'select Id ' + 
	        	', Name ' +
	        	', Bandwidth_Name__c ' +
	        	', Product_Type__c ' +
        		', CS_Bandwidth__c ' +
        		', Bandwidth_Value_MB__c ' +
        	'from CS_Bandwidth_Product_Type__c ' +        	
        	'where ' +
        	'	Product_Type__c = :productType ' +
        	(bandwidthValueMB 	!= null ? 
        		'and Bandwidth_Value_MB__c >= :bandwidthValueMB ' : '') +
        	(searchValue 		!= null ? 
        		'and Name LIKE :searchValue ' : '') +
        	'order by ' +
        	'	Bandwidth_Value_MB__c ASC ' +
            'LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset ';
            
        List<CS_Bandwidth_Product_Type__c> data= Database.query(query); 
            
        System.Debug(data);
        return data;
    }
}