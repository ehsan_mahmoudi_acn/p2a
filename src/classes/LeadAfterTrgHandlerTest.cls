@isTest(SeeAllData = false)
public class LeadAfterTrgHandlerTest 
{
    static testmethod void testLeadAfterTrgHandler1()
    {
        Test.startTest();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(2);
        List<Contact> conList = P2A_TestFactoryCls.getContact(1,accList);
        List<Lead> leadList = new List<Lead>();
        Lead lead1 = new Lead(LastName='Test2', Company='Testing2', Status='Marketing Qualified Lead (MQL)',
                               Disqual_Reason__c='NA', LeadSource = 'Electronic Direct Mail (eDM)', Industry='BPO',
                               Country_Picklist__c= 'BELGIUM', Email='test1@test1234.com',Lead_Account_Id__c='test1');
        Lead lead2 = new Lead(LastName='Test3', Company='Testing3', Status='Marketing Qualified Lead (MQL)',
                               Disqual_Reason__c='NA', LeadSource = 'Electronic Direct Mail (eDM)', Industry='BPO',
                               Country_Picklist__c= 'BELGIUM', Email='test2@test1234.com',Lead_Account_Id__c='test2');
        leadList.add(lead1);
        leadList.add(lead2);
        insert leadList;
        List<Lead> ll = [Select id,lastname from Lead where lastname ='Test2'];
        system.assertequals(leadlist[0].lastname ,ll[0].lastname);
        system.assert(leadlist!=null);
        LeadAfterTrgHandler leadObj = new LeadAfterTrgHandler();
        leadObj.refLeadOwnerId=leadList[0].Id;
        leadObj.accObjList=accList;
        Test.stopTest();
        
    }
}