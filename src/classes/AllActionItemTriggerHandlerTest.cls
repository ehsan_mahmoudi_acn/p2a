@isTest
 private with Sharing class AllActionItemTriggerHandlerTest{
    private static List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
    private static List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
    private static List<Country_Lookup__c> countryList =  P2A_TestFactoryCls.getcountry(1);    
    private static List<Site__c> cityList = P2A_TestFactoryCls.getsites(1, accList,  countrylist);
    private static List<Contact> contactList = P2A_TestFactoryCls.getContact(1, accList);
    private static Account acc = accList[0];
    private static Opportunity opp = oppList[0];
    private static Country_Lookup__c cl = countryList[0];    
    private static Site__c s = cityList[0];
    private static Contact con = contactList[0];
    private static list<Action_Item__c> acList;
    private static Map<ID,Opportunity> objOppMap = new Map<ID,Opportunity>();   
    private static List<User> users = P2A_TestFactoryCls.get_Users(1);
    
    private static Id getRecordTypeRPP(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Request Pre Provisioning').getRecordTypeId();
        system.assert(cfrSchema !=null);
        return rtId;
    }
    
    private static Id getRecordTypeRCC(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo.get('Request Credit Check').getRecordTypeId();
        system.assert(cfrSchema !=null);
        return rtId;
    }
    
    private static Id getRecordTypeROS(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Request Order Support').getRecordTypeId();
        system.assert(cfrSchema !=null);
        return rtId;
    }
    
    private static Id getRecordTypeRFS(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Request Feasibility Study').getRecordTypeId();
        system.assert(cfrSchema !=null);
        return rtId;
    }
    
    private static Id getRecordTypeRPA(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Request Pricing Approval').getRecordTypeId();
        system.assert(cfrSchema !=null);
        return rtId;
    }
    
    private static Id getRecordTypeRC(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Request Contract').getRecordTypeId();
        system.assert(cfrSchema !=null);
        return rtId;
    }
    
    private static Id getRecordTypeRSD(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Request Service Details').getRecordTypeId();
        system.assert(cfrSchema !=null);
        return rtId;
    }
    
    private static Id getRecordTypeSGRR(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Stage Gate Review Request').getRecordTypeId();
        system.assert(cfrSchema !=null);
        return rtId;
    }
    
    private static Id getRecordTypeAdoc(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Adhoc Request').getRecordTypeId();
        system.assert(cfrSchema !=null);
        return rtId;
    }
    
    
    
     private static void initTestData(){        
        
        P2A_TestFactoryCls.sampleTestData(); 
        string UserId = userinfo.getuserid();        
        Map<Id, Action_Item__c> newActionItemsMap = new map<Id, Action_Item__c>();
        Map<Id, Action_Item__c> oldActionItemsMap = new map<Id, Action_Item__c>();
        
        Map<ID, Action_Item__c> modificationsplits = new Map<ID,Action_Item__c>();
        Set<ID> modifyset = new Set<ID>();
        
        
        acList = new List<Action_Item__c>();
        
        Action_Item__c ai1 = new Action_Item__c();
        Action_Item__c ai2 = new Action_Item__c();
        Action_Item__c ai3 = new Action_Item__c();
        Action_Item__c ai4 = new Action_Item__c();
        Action_Item__c ai5 = new Action_Item__c();        
        Action_Item__c ai6 = new Action_Item__c();
        Action_Item__c ai7 = new Action_Item__c();
        Action_Item__c ai8 = new Action_Item__c();
        Action_Item__c ai9 = new Action_Item__c();
        Action_Item__c ai10 = new Action_Item__c();        
        Action_Item__c ai11 = new Action_Item__c();
        Action_Item__c ai12 = new Action_Item__c();
        Action_Item__c ai13 = new Action_Item__c();
        Action_Item__c ai14 = new Action_Item__c();
        Action_Item__c ai15 = new Action_Item__c();
        Action_Item__c ai16 = new Action_Item__c();
        Action_Item__c ai17 = new Action_Item__c();
        Action_Item__c ai18 = new Action_Item__c();
        Action_Item__c ai19 = new Action_Item__c();
        Action_Item__c ai20 = new Action_Item__c();
        Action_Item__c ai21 = new Action_Item__c();
        Action_Item__c ai22 = new Action_Item__c();
        Action_Item__c ai23 = new Action_Item__c();
        Action_Item__c ai24 = new Action_Item__c();
        Action_Item__c ai25 = new Action_Item__c();
        Action_Item__c ai26 = new Action_Item__c();
        Action_Item__c ai5_1 = new Action_Item__c();
        Action_Item__c ai10_1 = new Action_Item__c();
        Action_Item__c ai19_1 = new Action_Item__c();
        Action_Item__c ai20_1 = new Action_Item__c();
        Action_Item__c ai21_1 = new Action_Item__c();
        Action_Item__c ai22_1 = new Action_Item__c();
        Action_Item__c ai23_1 = new Action_Item__c();
        Action_Item__c ai24_1 = new Action_Item__c();
        Action_Item__c ai25_1 = new Action_Item__c();
        Action_Item__c ai26_1 = new Action_Item__c();
        
        // data inputs
        ai1.RecordTypeId = getRecordTypeRPP();
        //System.debug('sumit Id'+getOpportunity().Id);
        ai1.Opportunity__c = opp.id;
        //ai1.Service_Delivery_Approver__c=UserInfo.getUserId()
        ai1.Completed_By_Service_delivery_date__c = System.today();
        ai1.Is_senior_sales_approved__c =true;
        ai1.Is_senior_delivery_approved__c = false;
        ai1.Feedback__c='Approved';
        acList.add(ai1);
        
        ai2.RecordTypeId = getRecordTypeRPP();
        ai2.Opportunity__c = opp.Id;
        //ai2.Completed_By_Service_delivery__c = ;
        ai2.Completion_Date__c = System.today();
        //ai2.Email_Approval_Uploaded__c=true
        //ai1.Is_senior_sales_approved__c =true;
        ai2.Is_senior_delivery_approved__c = true;
        //ai2.Feedback__c='Approved';
        acList.add(ai2);
        
        ai3.RecordTypeId = getRecordTypeRPP();
        ai3.Opportunity__c = opp.Id;
        //ai3.Email_Approval_Uploaded__c=false
        //ai3.Pre_Contract_Provisioning_Required__c='No'
        ai3.Feedback__c='Rejected';
        acList.add(ai3);
        
        ai4.RecordTypeId = getRecordTypeRCC();
        ai4.Opportunity__c = opp.Id;
        //ai4.Credit_Check_Requested__c = true4
        ai4.Status__c = 'Assigned';
        ai4.Feedback__c='Rejected';
        acList.add(ai4);
        
        ai5_1.Opportunity__c = opp.Id;
        ai5_1.RecordTypeId = getRecordTypeAdoc();
        ai5_1.Status__c = 'Completed';
        ai5_1.Feedback__c='Approved';
        ai5_1.Subject__c = 'Request Cross Country Application Approval';
        ai5_1.CreditLimitValidity__c=System.today();
        ai5_1.SecurityDeposit__c= 100.00;
        ai5_1.SecurityDepositHoldingPeriod__c=System.today();
        ai5_1.Completion_Date__c=null;
        ai5_1.Completed_By_Service_delivery_date__c=System.today();
        acList.add(ai5_1);
        
        ai6.RecordTypeId = getRecordTypeAdoc();
        ai6.Opportunity__c = opp.Id;
        ai6.Subject__c = 'Request to Modify Opportunity Split';
        ai6.Status__c = 'Completed';
        ai6.Feedback_Order__c = 'Approved';
        ai6.CreditLimitValidity__c = System.today();
        ai6.SecurityDeposit__c = 100.00;
        ai6.SecurityDepositHoldingPeriod__c =System.today();
        ai6.Completion_Date__c = System.today();
        acList.add(ai6);
        
        Id oppId_ai7 = opp.id;
        ai7.Opportunity__c = oppId_ai7;
        ai7.RecordTypeId = getRecordTypeROS();
        ai7.Status__c = 'Assigned';
        ai7.Feedback_Order__c = 'Approved';
        ai7.CreditLimitValidity__c = System.today();
        ai7.SecurityDeposit__c = 100.00;
        ai7.SecurityDepositHoldingPeriod__c =System.today();
        ai7.Completion_Date__c = System.today();
        acList.add(ai7);
        Opportunity opp_ai7 = new Opportunity(Id = oppId_ai7);
        objOppMap.put(oppId_ai7, opp_ai7);
        
        ai8.RecordTypeId = getRecordTypeRFS();
        ai8.Opportunity__c = opp.Id;
        //ai8.Feasibility_Study_Requested__c=true
        ai8.Status__c = 'New';
        acList.add(ai8);
        
        ai9.Opportunity__c = opp.Id;
        ai9.RecordTypeId = getRecordTypeRFS();
        ai9.Status__c = 'Assigned';
        //ai9.Completed_By__c = getOpportunity().CreditCheckDoneBy__c;
        ai9.Completion_Date__c = System.today();
        ai9.F_Expiration_Date__c = System.today();
        ai9.FeasibilityResult__c = 'Feasible';
        ai9.FeasibilityResultDetails__c = 'Feasible';
        //objOppMap.remove(ai9.Opportunity__c);
        acList.add(ai9);
        
        ai10.Opportunity__c = opp.Id;
        ai10.RecordTypeId = getRecordTypeRFS();
        ai10.Status__c = 'Assigned';
        ai10.Completed_By__c = 'kutta';
        ai10.Completion_Date__c = System.today();
        ai10.F_Expiration_Date__c = System.today();
        ai10.FeasibilityResult__c = 'Feasible';
        ai10.FeasibilityResultDetails__c = 'Feasible';
        acList.add(ai10);
        
        ai10_1.Opportunity__c = opp.Id;
        ai10_1.RecordTypeId = getRecordTypeRFS();
        ai10_1.Status__c = 'Assigned';
        //ai10_1.Completed_By__c = getOpportunity().CreditCheckDoneBy__c;
        ai10_1.Completion_Date__c = System.today();
        ai10_1.F_Expiration_Date__c = System.today();
        ai10_1.FeasibilityResult__c = 'Feasible';
        ai10_1.FeasibilityResultDetails__c = 'Feasible';
        acList.add(ai10_1);
        
        ai11.Opportunity__c = opp.Id;
        ai11.RecordTypeId = getRecordTypeRPA();
        ai11.Status__c = 'New';
        acList.add(ai11);
        
        ai12.Opportunity__c = opp.Id;
        ai12.RecordTypeId = getRecordTypeRPA();
        ai12.Status__c = 'New';
        //objOppMap.remove(ai12.Opportunity__c);
        acList.add(ai12);   
        
        ai13.Opportunity__c = opp.Id;
        ai13.RecordTypeId = getRecordTypeRPA();
        ai13.Status__c = 'Assigned';
        //ai13.Completed_By__c = getOpportunity().CreditCheckDoneBy__c;
        ai13.Completion_Date__c = System.today();
        ai13.Pricing_Feedback__c = 'Approved';
        ai13.Pricing_Approval_Comments__c = 'Approved';
        ai13.Additional_information_notes__c = 'Approved';
        acList.add(ai13);
        
        ai14.Opportunity__c = opp.Id;
        ai14.RecordTypeId = getRecordTypeRPA();
        ai14.Status__c = 'Assigned';
        //ai14.Completed_By__c = getOpportunity().CreditCheckDoneBy__c;
        ai14.Completion_Date__c = System.today();
        ai14.Pricing_Feedback__c = 'Approved';
        ai14.Pricing_Approval_Comments__c = 'Approved';
        ai14.Additional_information_notes__c = 'Approved';
        acList.add(ai14);           
        
        ai15.Opportunity__c = opp.Id;
        ai15.RecordTypeId = getRecordTypeRC();
        ai15.Status__c ='Assigned';
        ai15.Special_Terms_and_Conditions__c = 'xxxx';
        ai15.LegalComments__c = 'xxxx';
        acList.add(ai15);
        
        ai16.Opportunity__c = opp.Id;
        ai16.Status__c ='Assigned';
        ai16.RecordTypeId = getRecordTypeRC();
        ai16.Special_Terms_and_Conditions__c = 'xxxx';
        ai16.LegalComments__c = 'xxxx';
        acList.add(ai16);           
        
        ai17.Opportunity__c = opp.Id;
        ai17.RecordTypeId = getRecordTypeRSD();
        ai17.Status__c ='Assigned';
        acList.add(ai17);
        
        ai18.Opportunity__c = opp.Id;
        ai18.RecordTypeId = getRecordTypeRSD();
        ai18.Status__c ='Assigned';
        acList.add(ai18);
        
        ai19.Opportunity__c = opp.Id;
        ai19.RecordTypeId = getRecordTypeSGRR();
        ai19.Status__c ='Assigned';
        ai19.Stage_Gate_Number__c = 1;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList.add(ai19);
        
        ai19_1.Opportunity__c = opp.Id;
        ai19_1.RecordTypeId = getRecordTypeSGRR();
        ai19_1.Status__c ='Assigned';
        ai19_1.Stage_Gate_Number__c = 1;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList.add(ai19_1);
        
        ai20.Opportunity__c = opp.Id;
        ai20.RecordTypeId = getRecordTypeSGRR();
        ai20.Status__c ='Assigned';
        ai20.Stage_Gate_Number__c = 2;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList.add(ai20);
        
        ai20_1.Opportunity__c = opp.Id;
        ai20_1.RecordTypeId = getRecordTypeSGRR();
        ai20_1.Status__c ='Assigned';
        ai20_1.Stage_Gate_Number__c = 2;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList.add(ai20_1);
        
        ai21.Opportunity__c = opp.Id;
        ai21.RecordTypeId = getRecordTypeSGRR();
        ai21.Status__c ='Assigned';
        ai21.Stage_Gate_Number__c = 3;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList.add(ai21);
        
        ai21_1.Opportunity__c = opp.Id;
        ai21_1.RecordTypeId = getRecordTypeSGRR();
        ai21_1.Status__c ='Assigned';
        ai21_1.Stage_Gate_Number__c = 3;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList.add(ai21_1);
        
        ai22.Opportunity__c = opp.Id;
        ai22.RecordTypeId = getRecordTypeSGRR();
        ai22.Status__c ='Assigned';
        ai22.Stage_Gate_Number__c = 4;
        //ai16.Stage_Gate_4_Review_Completed__c = true
        acList.add(ai22);
        
        ai22_1.Opportunity__c = opp.Id;
        ai22_1.RecordTypeId = getRecordTypeSGRR();
        ai22_1.Status__c ='Assigned';
        ai22_1.Stage_Gate_Number__c = 4;
        //ai16.Stage_Gate_4_Review_Completed__c = true;
        acList.add(ai22_1);
     
        
        ai23.Opportunity__c = opp.Id;
        ai23.RecordTypeId = getRecordTypeSGRR();
        ai23.Status__c ='Assigned';
        ai23.Stage_Gate_Number__c = 1;
        //ai17.Stage_Gate_1_Action_Created__c = true
        acList.add(ai23);
        
        ai23_1.Opportunity__c = opp.Id;
        ai23_1.RecordTypeId = getRecordTypeSGRR();
        ai23_1.Status__c ='Assigned';
        ai23_1.Stage_Gate_Number__c = 1;
        //ai17.Stage_Gate_1_Action_Created__c = true
        acList.add(ai23_1);
        
        ai24.Opportunity__c = opp.Id;
        ai24.RecordTypeId = getRecordTypeSGRR();
        ai24.Status__c ='Assigned';
        ai24.Stage_Gate_Number__c = 2;
        //ai18.Stage_Gate_2_Action_Created__c = true
        acList.add(ai24);
        
        ai24_1.Opportunity__c = opp.Id;
        ai24_1.RecordTypeId = getRecordTypeSGRR();
        ai24_1.Status__c ='Assigned';
        ai24_1.Stage_Gate_Number__c = 2;
        //ai18.Stage_Gate_2_Action_Created__c = true
        acList.add(ai24_1);
        
        ai25.Opportunity__c = opp.Id;
        ai25.RecordTypeId = getRecordTypeSGRR();
        ai25.Status__c ='Assigned';
        ai25.Stage_Gate_Number__c = 3;
        //ai19.Stage_Gate_3_Action_Created__c = true
        acList.add(ai25);
        
        ai25_1.Opportunity__c = opp.Id;
        ai25_1.RecordTypeId = getRecordTypeSGRR();
        ai25_1.Status__c ='Assigned';
        ai25_1.Stage_Gate_Number__c = 3;
        //ai19.Stage_Gate_3_Action_Created__c = true
        acList.add(ai25_1);
        
        
        
        ai26_1.Opportunity__c = opp.Id;
        ai26_1.RecordTypeId = getRecordTypeSGRR();
        ai26_1.Status__c ='Assigned';
        ai26_1.Stage_Gate_Number__c = 4;
       // ai26_1.Assigned_To__c=UserInfo.getUserId();
       // ai26_1.Assigned_To_OD__c=UserInfo.getUserId();
        //ai21.Stage_Gate_4_Action_Created__c = true
        acList.add(ai26_1);
                
            insert acList; 
            system.assert(acList!=null);
            
            
        opportunity oppgate = [select id, Stage_Gate_1_Review_Completed__c from opportunity where id =:opp.Id];
        oppgate.Stage_Gate_1_Review_Completed__c = true;
        upsert oppgate;      
        
        opportunity oppgate1 = [select id, Stage_Gate_2_Review_Completed__c from opportunity where id =:opp.Id];
        oppgate1.Stage_Gate_2_Review_Completed__c = true;
        upsert oppgate1;      
        
        opportunity oppgate2 = [select id, Stage_Gate_3_Review_Completed__c from opportunity where id =:opp.Id];
        oppgate2.Stage_Gate_3_Review_Completed__c = true;
        upsert oppgate2;      
        
        opportunity oppgate3 = [select id, Stage_Gate_4_Review_Completed__c from opportunity where id =:opp.Id];
        oppgate3.Stage_Gate_4_Review_Completed__c = true;
        upsert oppgate3;      
        
        List<Action_item__c> OldActionItem = new List<Action_item__c>();
        Action_item__c ai = new Action_item__c();
        ai.Opportunity__c = oppList[0].id;
        ai.Opportunity_Split_Owner__c = UserId;
        ai.Status__c = 'Completed';
        ai.Subject__c = 'Request Cross Country Application Approval';
        ai.Completed_By_Order__c = userinfo.getUserId();
        ai.Modification_of_existing_Split__c = true;
        ai.recordtypeid = getRecordTypeSGRR();
        ai.Opportunity_Email__c = 'test@gmail.com';
        ai.Quote_Subject__c = 'Pricing Validation';
        ai.Assigned_User__c = 'test@gmail.com';
        ai.Approved_by_Email__c = 'test@gmail.com';
        ai.Commercial_Impact__c = 'yes';
        ai.Due_Date__c = system.today()+ 5;
        ai.Order_Approval_Comments__c = 'Testtest';
        ai.CreditLimitValidity__c = system.today()+ 12;
        ai.SecurityDeposit__c = 45;
        ai.Completed_By__c = 'User';
        ai.Completion_Date__c = system.today();
        ai.Feedback__c = 'Approved';
        ai.Is_senior_sales_approved__c = true;
        ai.Is_senior_delivery_approved__c =false;
        OldActionItem.add(ai); 
        insert OldActionItem;
        
        List<Action_item__c> ActionItemList = new List<Action_item__c>();
        Action_item__c aim1 = new Action_item__c();
        aim1.Opportunity__c = oppList[0].id;
        aim1.Opportunity_Split_Owner__c = UserId;
        // aim1.New_Product_SOV__c = 25;
        aim1.Status__c = 'Completed';
        aim1.Subject__c = 'Request to Modify Opportunity Split';
        aim1.Completed_By_Order__c = userinfo.getuserid();
        aim1.Modification_of_existing_Split__c = true;
        aim1.recordtypeid = getRecordTypeRPA();
        aim1.Opportunity_Email__c = 'test@gmail.com';
        aim1.Quote_Subject__c = 'Pricing Validation';
        aim1.Assigned_User__c = 'test@gmail.com';
        aim1.Approved_by_Email__c = 'test@gmail.com';
        aim1.Commercial_Impact__c = 'yes';
        aim1.Due_Date__c = system.today()+ 5;
        aim1.Order_Approval_Comments__c = 'Testtest';
        aim1.CreditLimitValidity__c = system.today()+ 12;
        aim1.SecurityDeposit__c = 45;
        aim1.Completed_By__c = 'User';
        aim1.Completion_Date__c = system.today();
        aim1.Feedback__c = 'Approved';
        aim1.Is_senior_sales_approved__c = true;
        aim1.Is_senior_delivery_approved__c =false;
        ActionItemList.add(aim1); 
        insert ActionItemList;
        
        for(Action_item__c a:ActionItemList) {
            modificationsplits.put(a.Opportunity_Split_Owner__c,a);
            newActionItemsMap.put(a.id,a);
            modifyset.add(a.Opportunity__c);
        }

        for(Action_item__c a1:OldActionItem) {
            oldActionItemsMap.put(a1.id,a1); 
       }
        
        
        
        List<OpportunitySplit> ModificationOppSplits = [select id, Total_Product_SOV__c, SplitPercentage_Product_SOV__c, 
                        Team_Role__c, SplitAmount, SplitNote, SplitPercentage, SplitOwnerID, SplitOwner.UserRole.Name, OpportunityID 
                            from OpportunitySplit where OppSplitType__c=:true and SplitOwnerID IN:modificationsplits.keyset() and OpportunityID IN:modifyset];
        
        system.debug('-----ModificationOppSplits ----'+ModificationOppSplits); 
        
        List<Opportunity> opplist = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opportunity';
        opp.sales_status__c = 'Won';
        opp.StageName = 'Identify And Define';
        opp.CloseDate = System.today();
        opp.Win_Loss_Reasons__c =   'Quality';
        opp.QuoteStatus__c = 'Approved';
        opp.Product_Type__c = 'IPL';
        opp.Estimated_MRC__c = 100.00;
        opp.Estimated_NRC__c = 100.00;
        Opplist.add(opp);
        insert opplist;
        system.assert(opplist!=null);
       
        ID splittypeidactive = [select DeveloperName,isactive,id from OpportunitySplitType where isactive=:true][0].id;
        
        List<OpportunityTeamMember> OppTeamList = new List<OpportunityTeamMember>();
        OpportunityTeamMember OppTeam = new OpportunityTeamMember();
        OppTeam.TeamMemberRole = 'Sales special list';
        OppTeam.UserId = users[0].id;
        Oppteam.OpportunityId = Opplist[0].id;
        OppTeamList.add(OppTeam);
        
        OpportunityTeamMember OppTeam1 = new OpportunityTeamMember();
        OppTeam1.TeamMemberRole = 'Opportunity Owner';
        OppTeam1.UserId = users[0].id;
        Oppteam1.OpportunityId = Opplist[0].id;
        OppTeamList.add(OppTeam1);
        insert OppTeamList;
        system.assert(OppTeamList!=null);
        List<OpportunitySplit>oppsList = new List<OpportunitySplit>();
        OpportunitySplit o = new OpportunitySplit();
        o.SplitPercentage = 100;
        o.OpportunityID = opplist[0].id;
        o.Team_Role__c= OppTeamList[0].TeamMemberRole;
        o.SplitNote= OppTeamList[0].TeamMemberRole;
        o.SplitOwnerID = OppTeamList[0].UserID;
        o.SplitTypeid=splittypeidactive;
        oppsList.add(o);
        
        
        
        
    }    
     
    
    private static Opportunity getOpportunity(){
        //if(opp == null) {
        opp = new Opportunity();
        //Account acc = new Account()
        //acc =getAccount();
        //opp.Id= getRecordType().id;
        opp.Name = 'test';
        opp.Opportunity_Type__c = 'Simple';
        opp.Sales_Status__c = 'Open';
        opp.AccountId = getAccount().Id;
        opp.Order_Type__c = 'New';
        opp.StageName = 'Identify & Define';
        opp.Stage__c='Identify & Define';
        opp.CloseDate = System.today();
        opp.Estimated_MRC__c=800;
        opp.Estimated_NRC__c=1000;
        opp.ContractTerm__c='10';
        opp.Approved_By__c=UserInfo.getUserId();
        opp.CreditCheckDoneBy__c='Sumit Suman';

            insert opp;
            system.assert(opp!=null);
        //}
        return opp;
    }
    
    private static Account getAccount(){
        if(acc == null){    
            acc = new Account();
            cl = getCountry();
            acc.Name = 'Test Account';
            acc.Customer_Type__c = 'MNC';
            acc.Country__c = cl.Id;
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Activated__c = True;
            acc.Account_Status__c = 'Active';
            acc.Billing_Status__c = 'Active';
            acc.Account_ID__c = '9090';
            acc.Customer_Legal_Entity_Name__c='Test';
            insert acc;
            system.assert(acc!=null);
        }
        return acc;
    }
    
    private static Country_Lookup__c getCountry(){
        if(cl == null){ 
            cl = new Country_Lookup__c();
            cl.CCMS_Country_Code__c = 'SG';
            cl.CCMS_Country_Name__c = 'India';
            cl.Country_Code__c = 'SG';
            insert cl;
            system.assert(cl!=null);
        }
        return cl;
    }   
   
   
    public static void AllActionItemTriggerHandlerMethod() {
        //initTestData();
        
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
         string UserId = userinfo.getuserid();
         
        /*
        List<User> usrLst =[Select Name, Region__c from User where id= :UserInfo.getUserId()];
        User usr = usrLst[0];
        */
        
        Profile p=[SELECT Id From Profile WHERE Name='System Administrator'];
        User u2 =new User( Alias = 'newUser1' ,
                            Email ='newuser113523@testorg.com',
                            EmailEncodingKey = 'UTF-8',
                            EmployeeNumber = '132',
                            LastName = 'Testing',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', // changed for to avoid: INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST, Locale: bad value for restricted picklist field: America/Los_Angeles
                            UserName='newuser0123456123@testorg.com',
                            ProfileId=p.Id,
                            TimeZoneSidKey    = 'America/Los_Angeles');
        insert u2;
       
        System.runAs(u2) {
        
        
        
        Account accobj= accList[0];
        accobj.Account_ID__c = '1234';
        upsert accList;
          
        List<Contact> con =  P2A_TestFactoryCls.getContact(1,accList);
        
        Contact conobj = con[0];
        List<Country_Lookup__c> countrys = P2A_TestFactoryCls.getcountry(1);
        List<Site__c> siteslist = P2A_TestFactoryCls.getsites(1,accList,countrys);
        List<Global_Constant_Data__c> listGCD = new List<Global_Constant_Data__c>();
        
        Global_Constant_Data__c  authTokenSetting =  new Global_Constant_Data__c(name ='Action Item Status',value__c = 'Assigned');
        listGCD.add(authTokenSetting); 
        
        Global_Constant_Data__c  authTokenSetting1 =  new Global_Constant_Data__c(name ='Action Item Priority',value__c = 'High');
        listGCD.add(authTokenSetting1);
        insert listGCD;
        
        List<BillProfile__c> BillProfList = new List<BillProfile__c>();
        BillProfile__c BProfObj = new BillProfile__c();
        BProfObj.Account__c = accobj.Id;
        BProfObj.Billing_Entity__c = 'Austrila';
        BProfObj.Payment_Terms__c = '45 Days';
        BProfObj.Billing_Frequency__c = 'Hong Kong Annual Voice';
        BProfObj.Billing_Contact__c = conobj.id;
        BProfObj.Activated__c = true;
        BprofObj.Bill_Profile_Site__c= siteslist[0].id;
        BillProfList.add(BProfObj);
        insert BillProfList; 
        
        
       
      id recodTypeid1 = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.sObjectType, 'Adhoc Request'); 
       
      list<BillProfile__c> BilProfile = [select id, name,Account__c,Status__c,Payment_Terms__c,Activated__c,Billing_Contact__c,Billing_Entity__c 
                                             from BillProfile__c where id=:BillProfList[0].id limit 1];    
        
        
        Action_Item__c objActionItem1= new Action_Item__c();
        //objActionItem9.OwnerID = BillProfList[0].OwnerID;
        objActionItem1.Opportunity__c = oppList[0].id; 
        objActionItem1.Bill_Profile__c = BillProfList[0].Id;
        objActionItem1.Account__c =  BillProfList[0].Account__c;
        objActionItem1.Due_Date__c = System.Today() + 2;  
        objActionItem1.RecordTypeId = recodTypeid1; 
        objActionItem1.Commercial_Impact__c = 'Opp';
        //objActionItem1.Pricing_Case_Approval__c = 'Order Desk';
        objActionItem1.Quote_Subject__c = 'Pricing Validation';
        objActionItem1.Status__c =  'High';
        //objActionItem9.Priority__c = listGCD[1].value__c;
        // Hard Coded Comments , need to Put them in Herirachial Custom Settings
        objActionItem1.Comments__c = 'Payment Terms and Billing Frequency has changed, please update the contract documents (if any).';
        Insert objActionItem1;
        
        Action_Item__c pv =  [Select Commercial_Impact__c, Status__c, Pricing_Case_Approval__c from Action_Item__c where 
                                 id =: objActionItem1.id];
        pv.Commercial_Impact__c = 'Opp Split';
        pv.Status__c = 'Closed - Non Commercial';
        //pv.Pricing_Case_Approval__c = 'Order Desk';
        update pv; 
        
        List<Action_item__c> OldActionItem = new List<Action_item__c>();
        Action_item__c ai = new Action_item__c();
        ai.Opportunity__c = oppList[0].id;
        ai.Opportunity_Split_Owner__c = UserId;
        ai.Status__c = 'Completed';
        ai.Subject__c = 'Request to Modify Opportunity Split';
        ai.Completed_By_Order__c = userinfo.getuserid();
        ai.Modification_of_existing_Split__c = true;
        ai.recordtypeid = getRecordTypeSGRR();
        ai.Opportunity_Email__c = 'test@gmail.com';
        ai.Quote_Subject__c = 'Pricing Validation';
        ai.Assigned_User__c = 'test@gmail.com';
        ai.Approved_by_Email__c = 'test@gmail.com';
        ai.Commercial_Impact__c = 'yes';
        ai.Due_Date__c = system.today()+ 5;
        ai.Order_Approval_Comments__c = 'Testtest';
        ai.CreditLimitValidity__c = system.today()+ 12;
        ai.SecurityDeposit__c = 45;
        ai.Completed_By__c = 'User';
        ai.Completion_Date__c = system.today();
        ai.Feedback__c = 'Rejected';
        ai.Is_senior_sales_approved__c = true;
        ai.Is_senior_delivery_approved__c =false;
        OldActionItem.add(ai); 
        insert OldActionItem;
        
        List<Action_item__c> ActionItemList = new List<Action_item__c>();
        Action_item__c aim1 = new Action_item__c();
        aim1.Opportunity__c = oppList[0].id;
        aim1.Opportunity_Split_Owner__c = UserId;
        aim1.New_Product_SOV__c = 25;
        aim1.Status__c = 'Completed';
        aim1.Completed_By_Order__c = userinfo.getUserId();
        aim1.Subject__c = 'Request Cross Country Application Approval';
        aim1.Modification_of_existing_Split__c = true;
        aim1.recordtypeid = getRecordTypeRPA();
        aim1.Opportunity_Email__c = 'test@gmail.com';
        aim1.Quote_Subject__c = 'Pricing Validation';
        aim1.Assigned_User__c = 'test@gmail.com';
        aim1.Approved_by_Email__c = 'test@gmail.com';
        aim1.Commercial_Impact__c = 'yes';
        aim1.Due_Date__c = system.today()+ 5;
        aim1.Order_Approval_Comments__c = 'Testtest';
        aim1.CreditLimitValidity__c = system.today()+ 12;
        aim1.SecurityDeposit__c = 45;
        aim1.Completed_By__c = 'User';
        aim1.Completion_Date__c = system.today();
        aim1.Feedback__c = 'Approved';
        aim1.Is_senior_sales_approved__c = true;
        aim1.Is_senior_delivery_approved__c =false;
        ActionItemList.add(aim1); 
        insert ActionItemList;
        system.assert(ActionItemList!=null);        
       list<Action_Item__c> al = new list<Action_Item__c> ();  
       al.add(objActionItem1); 
       
       list<Action_Item__c> al1 = new list<Action_Item__c> ();  
       al.add(objActionItem1); 
        
      Map<Id,BillProfile__c> BillProfNewMap = new Map<Id,BillProfile__c>();
        if(BillProfNewMap.isEmpty()) {
        BillProfNewMap.put(BillProfList[0].id,BillProfList[0]);
        }
        
      Map<Id,Action_Item__c> actMap = new Map<Id,Action_Item__c>();
        if(actMap.isEmpty()) { 
            for(Action_Item__c atitem:ActionItemList) {
                actMap.put(atitem.id,atitem);
            }
        }
    
      Map<Id,Action_Item__c> oldMap = new Map<Id,Action_Item__c>();
        if(oldMap .isEmpty()) {
            for(Action_Item__c oldatitem:OldActionItem) {
                oldMap.put(oldatitem.id,oldatitem);
            }
        }
    
         
        Test.startTest();
        //Util.assignOwnersToRegionalQueue(usr,objActionItem1,'Bill Profile');
        AllActionItemTriggerHandler  allAction = new AllActionItemTriggerHandler(); 
        allAction.runManagedSharing(al);        
        
        System.debug('al--------'+al);
        System.debug('actMap--------'+actMap);
        System.debug('actMap--------'+actMap);
        
        allAction.updatingcommercialimpactinpricingvalidationAI(ActionItemList);
        //allAction.beforeUpdate(al,actMap,actMap);
        //allAction.afterUpdate(al,actMap,actMap);
        //allAction.updateCrossCountryAppAI(al,actMap,actMap);
        //ActionItemBusinessClass.updateOpportunity(); 
        allAction.updateCompletedByAndDate(ActionItemList,actMap);
        //allAction.updateCrossCountryAppAI(ActionItemList,actMap,oldMap);        
        
        allAction.requesttomodifySplitAI(ActionItemList);
 
        Test.stopTest();     
      } 
     }
   
    private static testMethod void doDynamicLookupSearchTest() {
       Exception ee = null;
       Integer userid = 0; 
        try{
        
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        system.assert(userid!=null);
        
        AllActionItemTriggerHandlerMethod();                          
        
       } catch(Exception e){
            ee = e;
            ErrorHandlerException.ExecutingClassName='AllActionItemTriggerHandlerTest :doDynamicLookupSearchTest';         
            ErrorHandlerException.sendException(e); 
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
    } 
   
   private static testMethod void unitTest() {
   
       Test.startTest();
       Integer userid = 0;
        initTestData();
        system.assert(userid!=null);
       Test.stopTest();
   }
   
   

    
     @isTest static void testExceptions(){
         AllActionItemTriggerHandler alls=new AllActionItemTriggerHandler();        
         try{alls.UpdateOpportunityFromActionItemMethod(null);}catch(Exception e){}
         try{alls.updatingcommercialimpactinpricingvalidationAI(null);}catch(Exception e){}
         try{alls.updateCrossCountryAppAI(null,null,null);}catch(Exception e){}
         system.assert(alls!=null);
          
     }
     
}