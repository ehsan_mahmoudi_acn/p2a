/**    @author - Accenture    
       @date - 11-11-2013    
       @version - 1.0    
       @description - This class is used as controller to trigger IP031/Email notification.  
**/

public class PRSubmissionController {
/*private class OrderSubmitValidatorException extends Exception {}
    public ID oppId {get;set;}
    Opportunity opp {get;set;}
    EmailContentVO emailContentVo=null;
    Boolean bFlag=false;
    Boolean emailFlag=false;
    OpportunityLineItem oppliObj=null;
    Site__c siteAObj=null;
    Site__c siteBObj=null;
    Resource__c resource=null;
    Service__c service=null;
    Boolean eFlag=false;
    List<EmailContentVO> emailList=new List<EmailContentVO>();
    public PRSubmissionController(ApexPages.StandardController controller){
        this.oppId = Apexpages.currentPage().getParameters().get('id');
        List<String> fieldLst = new List<String>();
        fieldLst.add('Name');
        fieldLst.add('Id');
        fieldLst.add('QuoteStatus__c');
        fieldLst.add('Sent_to_TIGFIN__c');
        fieldLst.add('Is_Offnet_Product_in_Deal__c');
        fieldLst.add('PR_Requested__c');
        fieldLst.add('Stagename');
        //added Sales_Status__c as  part of SOMP
        fieldLst.add('Sales_Status__c');
        fieldLst.add('Sent_to_TIGFIN_for_Cancel__c');
        fieldLst.add('Opportunity_ID__c');
        fieldLst.add('User_Name_sent_to_TIGFIN__c');
        fieldLst.add('Selling_Entity__c');
        controller.addFields(fieldLst);
        opp = (Opportunity)controller.getRecord();
        oppId = opp.id;
    }
    public PRSubmissionController(){}
    public PageReference validateOpportunity(){
            
    try{
        if(this.oppId==null){
            this.oppId = Apexpages.currentPage().getParameters().get('id');
            //added  Sales_Status__c in query as part of SOMP
            this.opp = [select Id,User_Name_sent_to_TIGFIN__c,Name,QuoteStatus__c,Sent_to_TIGFIN_for_Cancel__c,Opportunity_ID__c,Is_Offnet_Product_in_Deal__c,Sent_to_TIGFIN__c,PR_Requested__c,StageName,Selling_Entity__c,Sales_Status__c from Opportunity where id =:this.oppId];
        }
        //U2C master code population for OpportunityLineItems
       List<OpportunityLineItem> opplistItems = [select id,Product_id2__c,PricebookEntry.product2.Usage_Based_Charge_Flag__c,ParentCPQItem__c,ProductCode__c,CPQItem__c,Root_Product_Bill_Text__c,Root_Product_Id__c,Product_code__c,Bundle_Label_Name__c,OrderType__c from OpportunityLineItem where opportunityId =:oppid];
       U2CMasterCodePopulation.populateU2CMasterCode(opplistItems);
       
       Map<Id,OpportunityLineItem> oppliOffnet = new Map<Id,OpportunityLineItem>();
       PREmailController con= new PREmailController();
        List<OpportunityLineItem> oliList=new List<OpportunityLineItem>();
        List<OpportunityLineItem> oppliList=[select id,PricebookEntry.product2.Id,CartItemGuid__c,CPQItem__c,ServiceId__c,Resource__c,Email_Sent_To_TIGFIN__c
                                                ,GCPE_Type__c,Installation_after_business_hours_only__c,MTU_Size__c,GIE_router_model__c,Type_of_space__c,Unit_of_Power__c,Utility_model__c,Power_per_rack__c,Specify_the_additional_items_required__c,Media_type__c,Sub_Product_Type__c,Video_device_financing_model__c,Video_hardware_name_and_model__c,Overall_Video_Service_Management__c,Concierge_Service__c,Level_of_Supplier_management__c
                                                ,GCPE_Hardware_Manufacturer__c,GCPE_Hardware_Model__c,Trigger_Required__c
                                                ,Line_Description__c,Supplier_Flag__c,Site_A_Country__c,Site_B_country__c
                                                ,offnetRequired__c,PR_Created__c,PricebookEntry.Product2.Product_ID__c,Purchase_Order_Number__c,Line_Description_for_TIFGIN__c,Operating_Unit_TIGFIN__c
                                                ,GMNS_PS_Type__c,Financing_Model__c,Upstream_speed__c,Downstream_speed__c,Internet_Access_Technology__c,Product_id2__c,Type__c,COSVoice__c
                                                ,Other_NNI_Location__c,COS_Low_Priority__c,Site__c,Site_B__c,Site_B_State__c,Type_of_circuit__c,EMC_Type__c,Number_of_E1_T1_Cables_Required__c
                                                ,NID_Port_Speed__c,Model__c,NID_Rack_Mount_Size__c,NID_WAN_Interface_SFP_Type__c,Type_of_Installation__c,GCPE_type_associated_with_GMNS__c
                                                ,Hardware_Size__c,DCoS_required__c,Maintenance_MRC__c,Local_Loop_Speed__c,COSStandardData__c,COSInteractiveData__c
                                                ,COSVideo__c,COSCriticalData__c,NNI_Provider__c,Interface_type__c
                                                ,Port_speed__c,POP_A_Code__c,POP_Z_Code__c,Video_Hardware_Maintenance_Level__c
                                                ,Site_B_City__c,Customer_Site_A_Address_2__c,Site_A_City__c,Site_A_State__c,Customer_Site_B_Address_1__c,Service_Type__c,ProductCode__c
                                                ,Existing_PR_Number__c,Existing_PO_Number__c,OrderType__c,PricebookEntry.product2.Name,Purchase_Requisition_Number__c
                                                ,Contract_term__c,Need_By_Date__c,Specific_Quote_Requirements__c,CurrencyIsoCode,Customer_Site_A_Address_1__c
                                                ,Customer_Site_B_Address_2__c,Type_of_NNI_Required__c,Type_A_NNI_Port_CoS_mix__c
                                        from OpportunityLineItem where OpportunityId = :this.oppId and offnetRequired__c = 'Yes'];
       //updated stagename closed lost as per SOMP requirement
       if(opp.Sales_Status__c == 'Lost' && opp.Sent_to_TIGFIN_for_Cancel__c==false){
           opp.Sent_to_TIGFIN__C =true;
           opp.Sent_to_TIGFIN_for_Cancel__c=true;
           opp.PR_Requested__c=false;
           emailList=new List<EmailContentVO>();
           for(OpportunityLineItem oliitem:oppliList){
           oliitem.Trigger_Required__c=true;
           emailList=getEmailDetails(oliitem,opp);
           oliList.add(oliitem);
           }
       }else if(opp.Sales_Status__c != 'Lost'){
        //updated stagename closed lost as per SOMP requirement
        emailList=new List<EmailContentVO>();
       for(OpportunityLineItem oliitem:oppliList ){
        
        oppliOffnet.put(oliitem.id,oliitem);
        if(oliitem.Site__c!=null){
        siteAObj=[Select Id,Country_Finder__c,Site_Code__c from Site__c where Id=:oliitem.Site__c limit 1];
        }
        if(oliitem.Site_B__c!=null){
        siteBObj=[Select Id,Country_Finder__c,Site_Code__c from Site__c where Id=:oliitem.Site_B__c limit 1];
        }
       System.debug('oppliOffnet-------oliitem'+oliitem+'----------------'+oppliOffnet); 
       System.debug('SiteAObj  SiteBObj'+siteAObj +' '+siteBObj);
       if(oliitem.PR_Created__c==false && oliitem.OrderType__c=='New Provide' && (oliitem.Purchase_Requisition_Number__c=='' || oliitem.Purchase_Requisition_Number__c==null)){
            //ip031
            opp.Sent_to_TIGFIN__C =true;
            oliitem.PR_Created__c=true;
            opp.PR_Requested__c=true;
            oliitem.Trigger_Required__c=true;
       }
       else if(oliitem.PR_Created__c==true && oliitem.Supplier_flag__c == true && oliitem.OrderType__c=='New Provide'){
            //email
            emailContentVo=new EmailContentVo();
            oppliObj=new OpportunityLineItem(); 
            oppliObj=getLineDetails(oliitem,opp); 
            emailContentVo.CartLineItemId=oliitem.CartItemGuid__c;
            emailContentVo.OpportunityId=opp.Opportunity_ID__c;
            emailContentVo.OpportunityLineItemNumber=oliitem.CPQItem__c;
            emailContentVo.OpportunityStage=opp.StageName;
            emailContentVo.OrderType=oliitem.OrderType__c;
            emailContentVo.LineDescription=oppliObj.Line_Description_for_TIFGIN__c;
            emailContentVo.PONumber=oliitem.Purchase_Order_Number__c;
            emailContentVo.PRNumber=oliitem.Purchase_Requisition_Number__c;
            emailContentVo.PurchasingOperatingUnit=oppliObj.Operating_Unit_TIGFIN__c;
            if(siteAObj.Id!=null){
            emailContentVo.SiteName=[select Id,Name from  Country_Lookup__c  where  Id=:siteAObj.Country_Finder__c].Name;
            emailContentVo.SiteCode=siteAObj.Site_Code__c;
             }else{
            emailContentVo.SiteName=[select Id,Name from  Country_Lookup__c  where  Id=:siteBObj.Country_Finder__c].Name;
             emailContentVo.SiteCode=siteBObj.Site_Code__c; 
           }
           emailList.add(emailContentVo);
           oliitem.Supplier_flag__c = false;
           oliitem.Email_Sent_To_TIGFIN__c=true;
           emailFlag=true;
           }else if(oliitem.OrderType__c=='Price Change' || oliitem.OrderType__c=='Reconfiguration'){
            
            bFlag=true;
            
        }else if(oliitem.Email_Sent_To_TIGFIN__c==false && oliitem.Supplier_flag__c == true && (oliitem.OrderType__c=='Downgrade - Provide & Cease' || oliitem.OrderType__c=='Upgrade - Provide & Cease')){
            emailList=getEmailDetails(oliitem,opp);
             
       }else if(oliitem.Email_Sent_To_TIGFIN__c==false && oliitem.PR_Created__c==false && opp.StageName!='InFlight' && oliitem.Supplier_flag__c == false && (oliitem.OrderType__c=='Downgrade - Provide & Cease' || oliitem.OrderType__c=='Upgrade - Provide & Cease')){
             opp.Sent_to_TIGFIN__C =true;
             oliitem.PR_Created__c=true;
             opp.PR_Requested__c=true;
             oliitem.Trigger_Required__c=true;
             emailList=getEmailDetails(oliitem,opp);
             
        }else if(oliitem.Email_Sent_To_TIGFIN__c==false && oliitem.Supplier_flag__c == true && (oliitem.OrderType__c=='Relocation' || oliitem.OrderType__c=='Renewal' || oliitem.OrderType__c=='Upgrade Hot' ||oliitem.OrderType__c=='Downgrade Hot' || oliitem.OrderType__c=='Change Order')){
          emailList=getEmailDetails(oliitem,opp);
       }
         else if(oliitem.Email_Sent_To_TIGFIN__c==false && oliitem.PR_Created__c==false && opp.StageName!='InFlight' && oliitem.Supplier_flag__c == false && (oliitem.OrderType__c=='Relocation' || oliitem.OrderType__c=='Renewal' || oliitem.OrderType__c=='Upgrade Hot' ||oliitem.OrderType__c=='Downgrade Hot' || oliitem.OrderType__c=='Change Order')){
           opp.Sent_to_TIGFIN__C =true;
           oliitem.PR_Created__c=true;
           opp.PR_Requested__c=true;
           oliitem.Trigger_Required__c=true;
           emailList=getEmailDetails(oliitem,opp);
        }else if(oliitem.Email_Sent_To_TIGFIN__c==false && (oliitem.OrderType__c=='Cancel' || oliitem.OrderType__c=='Terminate')){
           opp.Sent_to_TIGFIN__C =true;
           opp.Sent_to_TIGFIN_for_Cancel__c=true;
           opp.PR_Requested__c=false;
           oliitem.Trigger_Required__c=true;
           emailList=getEmailDetails(oliitem,opp);
             
             }
       oliList.add(oliitem);  
         }
 
       } 
      //opp.Sent_to_TIGFIN__C =true;//need to remove
       
      system.debug('=======oliList===='+oliList);
      system.debug('===emailList==='+emailList);
      if(emailList.size()>0){
        eFlag=true;
      con.getEmailList(emailList,opp.Opportunity_ID__c,opp.User_Name_sent_to_TIGFIN__c);
      }
       
     if(opp.Is_Offnet_Product_in_Deal__c == 0){
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'This opportunity does not contain components that require external procurement.'));
      }else{
       if(opp.Sent_to_TIGFIN__C && opp.PR_Requested__c ||eFlag){
        update opp;
        update oliList;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Opportunity is submitted successfully!!!'));
        }else if(opp.Sent_to_TIGFIN__C && opp.Sent_to_TIGFIN_for_Cancel__c){
        update opp;
       update oliList;
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Opportunity Closed Lost is submitted successfully!!!'));  
        }else if(bFlag){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Request Supplier Quote is not available for Price Change or Reconfiguration'));    
            
        }
        else{
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Opportunity is already submitted for external procurement.'));   
                    
        }
     }
    }
   catch(DMLException dx){
            system.debug('DMLException inside PR Submission - '+dx.getMessage());
            CreateApexErrorLog.InsertHandleException('ApexClass','PRSubmissionController',dx.getMessage(),'Opportunity',Userinfo.getName());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'DMLException'));
        }catch(Exception ex){
            system.debug('DMLException inside PR Submission - '+ex.getMessage());
            CreateApexErrorLog.InsertHandleException('ApexClass','PRSubmissionController',ex.getMessage(),'opportunity',Userinfo.getName());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception'));

        }
   return null;
       
 }
    public List<EmailContentVO> getEmailDetails(OpportunityLineItem oliitem,Opportunity opp){
            siteAObj=new Site__c();
            siteBObj=new Site__c();
            resource=new Resource__c();
            service=new Service__c();
            if(oliitem.Site__c!=null){
            siteAObj=[Select Id,Country_Finder__c,Site_Code__c from Site__c where Id=:oliitem.Site__c limit 1];
             }
             if(oliitem.Site_B__c!=null){
             siteBObj=[Select Id,Country_Finder__c,Site_Code__c from Site__c where Id=:oliitem.Site_B__c limit 1];
             }
             //updated stagename closed lost as per the SOMP requirement
            if(opp.StageName=='InFlight' || opp.Sales_Status__c == 'Lost'){
            oppliObj=new OpportunityLineItem(); 
            oppliObj=getLineDetails(oliitem,opp);    
            emailContentVo=new EmailContentVo();
            emailContentVo.CartLineItemId=oliitem.CartItemGuid__c;
            emailContentVo.OpportunityId=opp.Opportunity_ID__c;
            emailContentVo.OpportunityLineItemNumber=oliitem.CPQItem__c;
            emailContentVo.OpportunityStage=opp.StageName;
            emailContentVo.OrderType=oliitem.OrderType__c;
            //emailContentVo.LineDescription=oliitem.Line_Description_for_TIFGIN__c;
            emailContentVo.LineDescription=oppliObj.Line_Description_for_TIFGIN__c;
            emailContentVo.PONumber=oliitem.Purchase_Order_Number__c;
            emailContentVo.PRNumber=oliitem.Purchase_Requisition_Number__c;
            emailContentVo.PurchasingOperatingUnit=oppliObj.Operating_Unit_TIGFIN__c;
            if(siteAObj.Id!=null){
            emailContentVo.SiteName=[select Id,Name from  Country_Lookup__c  where  Id=:siteAObj.Country_Finder__c].Name;
            emailContentVo.SiteCode=siteAObj.Site_Code__c;
             }else{
            emailContentVo.SiteName=[select Id,Name from  Country_Lookup__c  where  Id=:siteBObj.Country_Finder__c].Name;
             emailContentVo.SiteCode=siteBObj.Site_Code__c; 
           }
           emailList.add(emailContentVo);
           oliitem.Supplier_flag__c = false;
           oliitem.Email_Sent_To_TIGFIN__c=true;
           emailFlag=true;  

    }else{
         if(oliitem.Resource__c!=null){
            resource=[select Id,Purchase_Order_Number__c,Purchase_Requisition_Number__c,Line_Description_for_TIFGIN__c,Operating_Unit_TIGFIN__c from Resource__c where Id=:oliitem.Resource__c];
            }
            if(oliitem.ServiceId__c!=null){
            service=[select Id,Purchase_Order_Number__c,Purchase_Requisition_Number__c,Line_Description_for_TIFGIN__c,Operating_Unit_TIGFIN__c from Service__c where Id=:oliitem.ServiceId__c];
            }
            emailContentVo=new EmailContentVo();
            emailContentVo.CartLineItemId=oliitem.CartItemGuid__c;
            emailContentVo.OpportunityId=opp.Opportunity_ID__c;
            emailContentVo.OpportunityLineItemNumber=oliitem.CPQItem__c;
            emailContentVo.OpportunityStage=opp.StageName;
            emailContentVo.OrderType=oliitem.OrderType__c;
            
            emailContentVo.LineDescription=resource.Line_Description_for_TIFGIN__c!=null?resource.Line_Description_for_TIFGIN__c:service.Line_Description_for_TIFGIN__c;
            emailContentVo.PONumber=resource.Purchase_Order_Number__c!=null?resource.Purchase_Order_Number__c:service.Purchase_Order_Number__c;
            emailContentVo.PRNumber=resource.Purchase_Requisition_Number__c!=null?resource.Purchase_Requisition_Number__c:service.Purchase_Requisition_Number__c;
            emailContentVo.PurchasingOperatingUnit=resource.Operating_Unit_TIGFIN__c!=null?resource.Operating_Unit_TIGFIN__c:service.Operating_Unit_TIGFIN__c;
            if(siteAObj.Id!=null){
            emailContentVo.SiteName=[select Id,Name from  Country_Lookup__c  where  Id=:siteAObj.Country_Finder__c].Name;
           emailContentVo.SiteCode=siteAObj.Site_Code__c;
           }else{
            emailContentVo.SiteName=[select Id,Name from  Country_Lookup__c  where  Id=:siteBObj.Country_Finder__c].Name;
            emailContentVo.SiteCode=siteBObj.Site_Code__c;  
            }
           emailList.add(emailContentVo);
           oliitem.Supplier_flag__c = false;
           oliitem.Email_Sent_To_TIGFIN__c=true;
           emailFlag=true;
           
    }
     return  emailList;   
           
     }
     
     public OpportunityLineItem getLineDetails(OpportunityLineItem oli,Opportunity opp){
    String ClassofService;
    String LineDescription;
    String country_purcahsing_Unit;
     Site__c siteAObj = new Site__c();
                    Site__c siteBObj = new site__c();
                    siteAObj=[Select Id,Name,Country_Finder__c from Site__c where Id=:oli.Site__c limit 1];
                    if(siteAObj.Id == null){
                    siteBObj =[Select Id,Name,Country_Finder__c from Site__c where Id=:oli.Site_B__c limit 1];
                    }
                    
                    system.debug('siteAObj===siteBObj.Id'+siteAObj+'==='+siteBObj.Id);
                    country_purcahsing_Unit=siteAObj.Country_Finder__c!=null?siteAObj.Country_Finder__c:siteBObj.Country_Finder__c;
                    system.debug('===Product_ID__c=='+oli.PricebookEntry.Product2.Product_ID__c);
                    PurchasingEntityMapper__c CPEObj=[SELECT Id,Country_Name__c,Direct_Purchasing_PU__c,Purchasing_Entity__c,Other__c from PurchasingEntityMapper__c where Country_Name__c=:country_purcahsing_Unit limit 1];
                    SellingEntityPurchasingEntityMapper__c SEPEObj=[select Id,Country__c,Direct_Purchasing_PU__c,Purchasing_Entity__c,Region__c,Selling_Entity__c from SellingEntityPurchasingEntityMapper__c where Selling_Entity__c=:opp.Selling_Entity__c limit 1];
    ClassofService=(oli.COSVoice__c!=null?'Voice:'+oli.COSVoice__c+'%'+',':'')
                    +(oli.COSVideo__c!=null?'Video:'+oli.COSVideo__c+'%'+',':'')
                    +(oli.COSCriticalData__c!=null?'Critical Data:'+oli.COSCriticalData__c+'%'+',':'')
                    +(oli.COSInteractiveData__c!=null?'Interactive Data:'+oli.COSInteractiveData__c+'%'+',':'')
                    +(oli.COSStandardData__c!=null?'Standard Data:'+oli.COSStandardData__c+'%'+',':'')
                    +(oli.COS_Low_Priority__c!=null?'Low Priority Data:'+oli.COS_Low_Priority__c+'%'+',':'');
                    ClassofService=ClassofService.substring(0,ClassofService.length()-1);
                    if(oli.PricebookEntry.Product2.Product_ID__c=='LLOOP'){
                   LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                               +(oli.Service_Type__c!=null?'Service Type:'+ oli.Service_Type__c+',':'')
                               +(oli.Local_Loop_Speed__c!=null?'Speed:'+ oli.Local_Loop_Speed__c+',':'')
                               +(oli.Interface_type__c!=null?'Interface Type:'+ oli.Interface_type__c+',':'')
                               +(oli.MTU_Size__c!=null?'MTU:'+ oli.MTU_Size__c+',':'')
                               +(oli.Installation_after_business_hours_only__c!=null?'Installation After Business Hours only:'+ oli.Installation_after_business_hours_only__c+',':'');
                                           
                    }else if(oli.PricebookEntry.Product2.Product_ID__c=='GCPE'){
                        
                    LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                               +(oli.GCPE_Type__c!=null?'TYPE:'+ oli.GCPE_Type__c+',':'')
                               +(oli.Financing_Model__c!=null?'Financing Model:'+ oli.Financing_Model__c+',':'')
                               +(oli.Maintenance_MRC__c!=null?'Maintenance:'+ oli.Maintenance_MRC__c+',':'')                              
                               +(oli.Installation_after_business_hours_only__c!=null?'Installation after Business Hours only:'+ oli.Installation_after_business_hours_only__c+',':'');
   
                    }else if(oli.PricebookEntry.Product2.Product_ID__c=='OFFPOP'){
                        
                     LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                               +(oli.Type_of_NNI_Required__c!=null?'NNI required:'+ oli.Type_of_NNI_Required__c+',':'')
                               +(oli.Port_speed__c!=null?'Port speed:'+ oli.Port_speed__c+',':'')
                               +(oli.Other_NNI_Location__c!=null?'NNI location:'+ oli.Other_NNI_Location__c+',':'')
                               +(ClassofService!=null?'CoS mix:'+ClassofService+',':'')
                               +(oli.DCoS_required__c!=null?',DCos required:'+ oli.DCoS_required__c+',':'');

                    }else if(oli.PricebookEntry.Product2.Product_ID__c=='G-MNS-PS-SS' || oli.PricebookEntry.Product2.Product_ID__c=='G-MNS-PS-PM' || oli.PricebookEntry.Product2.Product_ID__c=='G-MNS-PS-POC'){
                        
                     LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                               +(oli.GMNS_PS_Type__c!=null?oli.PricebookEntry.Product2.Product_ID__c+''+'Type:'+ oli.GMNS_PS_Type__c+',':'')
                               +(oli.Hardware_Size__c!=null?'Hardware size:'+ oli.Hardware_Size__c+',':'');

                    }else if(oli.PricebookEntry.Product2.Product_ID__c=='NIDS'){
                        
                    LineDescription=
                    (oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                               +(oli.Model__c!=null?'Model:'+ oli.Model__c+',':'')
                               +(oli.NID_Port_Speed__c!=null?'Port Speed:'+ oli.NID_Port_Speed__c+',':'')
                               +(oli.NID_Rack_Mount_Size__c!=null?'Rack Mount Size:'+ oli.NID_Rack_Mount_Size__c+',':'')
                               +(oli.Interface_type__c!=null?'Interface type:'+ oli.Interface_type__c+',':'')
                               +(oli.NID_WAN_Interface_SFP_Type__c!=null?'NID WAN Interface SFP Type:'+ oli.NID_WAN_Interface_SFP_Type__c+',':'')
                               +(oli.Type_of_Installation__c!=null?'Type of installation?:'+ oli.Type_of_Installation__c+',':'');

                        
                    }else if(oli.PricebookEntry.Product2.Product_ID__c=='EMCS'){
                        
                    LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                               +(oli.EMC_Type__c!=null?'Type:'+ oli.EMC_Type__c+',':'')
                               +(oli.Type_of_Installation__c!=null?'Type of installation:'+ oli.Type_of_Installation__c+',':'')
                               +(oli.Number_of_E1_T1_Cables_Required__c!=null?'Number of E1/T1 Cables:'+ oli.Number_of_E1_T1_Cables_Required__c+',':'');
                        
                    }else if(oli.PricebookEntry.Product2.Product_ID__c=='OFFNETPROVIDER-AEND' || oli.PricebookEntry.Product2.Product_ID__c=='OFFNETPROVIDER-ZEND'){
                    LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                               +(oli.Type_of_circuit__c!=null?'Type of circuit:'+ oli.Type_of_circuit__c+',':'');
                    
                     }else if(oli.PricebookEntry.Product2.Product_ID__c=='GIEG' || oli.PricebookEntry.Product2.Product_ID__c=='GIE_ROUTER' || oli.PricebookEntry.Product2.Product_ID__c=='GIER-BACKPOP'){
                    LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                               +(oli.Internet_Access_Technology__c!=null?'Internet Access Technology:'+ oli.Internet_Access_Technology__c+',':'')
                               +(oli.Downstream_speed__c!=null?'Downstream speed:'+ oli.Downstream_speed__c+',':'')
                               +(oli.Upstream_speed__c!=null?'Upstream speed:'+ oli.Upstream_speed__c+',':'')
                               +(oli.GIE_router_model__c!=null?'GIE Router Model:'+ oli.GIE_router_model__c+',':'');

                     }else if(oli.PricebookEntry.Product2.Product_ID__c=='COLO-RACK' || oli.PricebookEntry.Product2.Product_ID__c=='COLO-CAGE' || oli.PricebookEntry.Product2.Product_ID__c=='COLO-CAGE-RACK' || oli.PricebookEntry.Product2.Product_ID__c=='COLO-XC' || oli.PricebookEntry.Product2.Product_ID__c=='COLO-MISC'){
                     LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                                +(oli.Power_per_rack__c!=null?'Power Per Rack:'+ oli.Power_per_rack__c+',':'')
                                +(oli.Unit_of_Power__c!=null?'Unit Of Power:'+ oli.Unit_of_Power__c+',':'')
                                +(oli.Utility_model__c!=null?'Utility Model:'+ oli.Utility_model__c+',':'')
                                +(oli.Type_of_space__c!=null?'Type Of Space:'+ oli.Type_of_space__c+',':'')
                                +(oli.Specify_the_additional_items_required__c!=null?'Specify Additional Items Required:'+oli.Specify_the_additional_items_required__c+',':'')
                                +(oli.Media_type__c!=null?'Media Type:'+oli.Media_type__c+',':'');

                     }else if(oli.PricebookEntry.Product2.Product_ID__c=='GCF-WBC' || oli.PricebookEntry.Product2.Product_ID__c=='GCF-IPA' || oli.PricebookEntry.Product2.Product_ID__c=='GCF-PMC'){
                     LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                                    +(oli.Type__c!=null?'Type:'+ oli.Type__c+',':'');

                     }else if(oli.PricebookEntry.Product2.Product_ID__c=='GTPS' || oli.PricebookEntry.Product2.Product_ID__c=='GTPR' || oli.PricebookEntry.Product2.Product_ID__c=='GTPM' ){
                                        LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                                                   +(oli.Type__c!=null?'Type:'+ oli.Type__c+',':'')
                                                   +(oli.Sub_Product_Type__c!=null?'Sub Product Type:'+ oli.Sub_Product_Type__c+',':'')
                                                   +(oli.Video_device_financing_model__c!=null?'Video Device Financing Model:'+ oli.Video_device_financing_model__c+',':'')
                                                   +(oli.Video_Hardware_Maintenance_Level__c!=null?'Video Hardware Maintenance Level:'+ oli.Video_Hardware_Maintenance_Level__c+',':'')
                                                   +(oli.Overall_Video_Service_Management__c!=null?'Overall Video Service Management:'+ oli.Overall_Video_Service_Management__c+',':'')
                                                   +(oli.Concierge_Service__c!=null?'Concierge Service:'+ oli.Concierge_Service__c+',':'')
                                                   +(oli.Level_of_Supplier_management__c!=null?'Level of supplier management:'+ oli.Level_of_Supplier_management__c+',':'');
                     }
                     
                     else{
                     LineDescription=',';
                    }
                   LineDescription=LineDescription.substring(0,LineDescription.length()-1);
                   
                   if(oli.PricebookEntry.Product2.Product_ID__c=='GCPE' ||oli.PricebookEntry.Product2.Product_ID__c=='NIDS' || oli.PricebookEntry.Product2.Product_ID__c=='EMCS'){
                    oli.Operating_Unit_TIGFIN__c=CPEObj.Purchasing_Entity__c;
                    }else if(oli.PricebookEntry.Product2.Product_ID__c=='G-MNS-PS-SS' || oli.PricebookEntry.Product2.Product_ID__c=='G-MNS-PS-PM' || oli.PricebookEntry.Product2.Product_ID__c=='G-MNS-PS-POC'){
                    oli.Operating_Unit_TIGFIN__c=SEPEObj.Purchasing_Entity__c;
                    }else if(oli.PricebookEntry.Product2.Product_ID__c=='COLO-RACK' || oli.PricebookEntry.Product2.Product_ID__c=='COLO-CAGE' || oli.PricebookEntry.Product2.Product_ID__c=='COLO-CAGE-RACK' || oli.PricebookEntry.Product2.Product_ID__c=='COLO-XC' || oli.PricebookEntry.Product2.Product_ID__c=='COLO-MISC' || oli.PricebookEntry.Product2.Product_ID__c=='GCF-WBC' || oli.PricebookEntry.Product2.Product_ID__c=='GCF-IPA' || oli.PricebookEntry.Product2.Product_ID__c=='GCF-PMC' || oli.PricebookEntry.Product2.Product_ID__c=='GTPS')
                    {
                    oli.Operating_Unit_TIGFIN__c=SEPEObj.Direct_Purchasing_PU__c;
                    }
                    else{
                    oli.Operating_Unit_TIGFIN__c=CPEObj.Direct_Purchasing_PU__c;
                    }
                   oli.Line_Description_for_TIFGIN__c=LineDescription;
                   
    
    return oli;
} */
    
}