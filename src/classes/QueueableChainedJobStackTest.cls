@isTest(SeeAllData=false)
public class QueueableChainedJobStackTest {
    
    static Integer jobCounter = 0;
    
    private static testMethod void testJobExecution() {    
        IWorker worker = new TestWorker();
        QueueableChainedJobStack.add(worker, null);
        QueueableChainedJobStack.add(null, null);
        system.assert(worker!=null);
        Test.startTest();
        QueueableChainedJobStack.enqueue();        
        Test.stopTest();
    }
    
    private class TestWorker implements IWorker {
        public void work(Object params, Object context) {
            QueueableChainedJobStackTest.jobCounter++;
            
        }
    }
}