/**
    @author - Accenture
    @date - 08- Dec-2014
    @version - 1.0
    @description - This is the test class for GlobalAccountCURDonAccountCURDOperationTrigger
*/
@isTest(SeeAllData = false)
private class GlobalAccountCURDTriggerTest {
    static Account acc;
    static Country_Lookup__c cl;
    static Global_Account__c ga;
    
    static testMethod void getAccountDetails() {
    test.starttest();
        String accountOwner = Userinfo.getFirstName() + ' ' +Userinfo.getLastName();
         acc = new Account();
            acc.Name = 'Test Account Test 1';
            acc.Customer_Type__c = 'MNC';
            cl = getCountry();
            acc.Country__c = cl.Id;
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Account_Manager__c = null;
            acc.Customer_Legal_Entity_Name__c='Test';
            insert acc;
            system.assert(acc!=null);
            ga = new Global_Account__c();
            ga.Account__c = acc.id;
            ga.Name = 'test1243';
            ga.Country__c =cl.id;
            insert ga;    
            acc.Name = 'test2234';
            ga.name ='test2234';
            update ga;
            system.assert(ga!=null);
            ga=[select id,Account__c from Global_Account__c where account__c =: acc.id limit 1];   
            delete ga; 
           // ga=[select Account__c from Global_Account__c where id =: ga.id and account__c =: acc.id limit 1];   
            Undelete ga;    
            update acc;
            delete acc;
            undelete acc;
            test.stopTest();        
    }
    
    private static Country_Lookup__c getCountry(){
        if(cl == null){ 
            cl = new Country_Lookup__c();
            cl.CCMS_Country_Code__c = 'SG';
            cl.CCMS_Country_Name__c = 'India';
            cl.Country_Code__c = 'SG';
            insert cl;
            system.assert(cl!=null);
        }
        return cl;
    } 
  }