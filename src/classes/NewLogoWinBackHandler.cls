/*As part of production Ticket# TGCTASK0209037, commented //with sharing */
//public with sharing class NewLogoWinBackHandler {
public class NewLogoWinBackHandler {
   
    Set<Id> accIdforNewLogo = new Set<Id>();
    Set<Id> accIdforWinBack = new Set<Id>();
    Map<String,NewLogo__c> newLogoCustMap = NewLogo__c.getAll();
      
    public void newLogoWinBackHdlrMthd(List<Account> trigNew,boolean isAfter,boolean isUpdate,map<id,Account> oldMap,map<id,Account> newMap) 
    {
       
        // Condition if Account first Opportunity stage have closed won and account type is Prospect 
        // or Former Customer for only profile System Admin or Sales Admin then Account type will update to Customer
        for(Account accObj : trigNew)
        {
           if(accObj.Account_Status__c == newLogoCustMap.get('Active').Action_Item__c
           && oldMap.get(accObj.Id).Type == newLogoCustMap.get('Logo_Prospect').Action_Item__c && newMap.get(accObj.Id).Type == newLogoCustMap.get('Logo_Customer').Action_Item__c 
           && oldMap.get(accObj.Id).Account_Type__c == newLogoCustMap.get('Logo_Prospect').Action_Item__c && newMap.get(accObj.Id).Account_Type__c == newLogoCustMap.get('Logo_Customer').Action_Item__c)
           {
                accIdforNewLogo.add(accObj.id);
           }
            
            
          if(accObj.Account_Status__c == newLogoCustMap.get('Active').Action_Item__c
             && oldMap.get(accObj.Id).Type == newLogoCustMap.get('Logo_Former_Customer').Action_Item__c && newMap.get(accObj.Id).Type ==  newLogoCustMap.get('Logo_Customer').Action_Item__c
           && oldMap.get(accObj.Id).Account_Type__c == newLogoCustMap.get('Logo_Former_Customer').Action_Item__c
             && newMap.get(accObj.Id).Account_Type__c == newLogoCustMap.get('Logo_Customer').Action_Item__c)    
           {
                accIdforWinBack.add(accObj.id);
                System.debug('Testing for WInback');
            } 
           
            
        }
        if( accIdforNewLogo.size()>0 || accIdforWinBack.size()>0)
        {
      newLogoCommonMthdHdlr(trigNew,isAfter,isUpdate,oldMap,newMap);
    }
    } 
   
   public void newLogoCommonMthdHdlr(List<Account> trigNew,boolean isAfter,boolean isUpdate,map<id,Account> oldMap,map<id,Account> newMap)
   {    
       
    Action_Item__c ai = new Action_Item__c();                             
    //Condition to check Action Item count for Record Type New Logo   
     
    List<Action_Item__c> actItNewLogoList = new List<Action_Item__c>();   
    List<Action_Item__c> actItWinBackList = new List<Action_Item__c>();
    
    QueueSobject actItmQueues =[Select Id, SobjectType, QueueId, Queue.Name  From QueueSobject where SobjectType='Action_Item__c'
                                     AND Queue.Name = 'NewWinBackCustomer' LIMIT 1];
       
    List<Opportunity> oppListForLogo = [Select Id,StageName from Opportunity where AccountId In : accIdforNewLogo
                                   AND  StageName ='Closed Won' ORDER BY LastModifiedDate desc LIMIT 1 ];
    List<Opportunity> oppListForWinBack = [Select Id,StageName from Opportunity where AccountId In : accIdforWinBack
                                     AND  StageName ='Closed Won' ORDER BY LastModifiedDate desc LIMIT 1 ];
        
    for(Account accObj : trigNew) 
    {
       if(accIdforNewLogo != null && oldMap.get(accObj.Id).Type != newMap.get(accObj.Id).Type
          && oldMap.get(accObj.Id).Type == newLogoCustMap.get('Logo_Prospect').Action_Item__c  && newMap.get(accObj.Id).Type == newLogoCustMap.get('Logo_Customer').Action_Item__c
          &&  accObj.Type == newLogoCustMap.get('Logo_Customer').Action_Item__c && oppListForLogo != null )
        {
            for(Integer i = 0; i < oppListForLogo.size(); i++) 
            {  
                ai.Account__c =  accObj.Id; 
                ai.Opportunity__c = oppListForLogo.get(i).Id; 
                ai.RecordTypeId = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType, 'New Logo');
                ai.Subject__c = newLogoCustMap.get('Logo_New_Customer').Action_Item__c;
                ai.OwnerId = actItmQueues.Queue.Id;
                actItNewLogoList.add(ai);
               
            }// End of Integer New Logo For Loop
            //insert actItNewLogoList;
        }
        
        //accObj.Requested_By__c = usrMap.get(accObj.OwnerId).name;
        // End of New Logo If Statement
      
       
       // IF condition for Win Back Former Customer
        if(accIdforWinBack != null 
           //&& (profName == Label.Logo_System_Administrator || profName == Label.Logo_TI_Sales_Admin)
          && oldMap.get(accObj.Id).Type != newMap.get(accObj.Id).Type
          && oldMap.get(accObj.Id).Type == newLogoCustMap.get('Logo_Former_Customer').Action_Item__c && newMap.get(accObj.Id).Type == newLogoCustMap.get('Logo_Customer').Action_Item__c
          &&  accObj.Type == newLogoCustMap.get('Logo_Customer').Action_Item__c  && oppListForWinBack != null  )
         {
            for(Integer i = 0; i < oppListForWinBack.size(); i++) 
            {  
                ai.Account__c = accObj.Id; 
                ai.Opportunity__c = oppListForWinBack.get(i).Id; 
                ai.RecordTypeId = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType, 'Win Back');
                ai.Subject__c = newLogoCustMap.get('Logo_Win_Back_Customer').Action_Item__c;
                ai.OwnerId = actItmQueues.Queue.Id;
                actItWinBackList.add(ai);
                
            }// End of Integer Win Back For Loop
            
           //insert actItWinBackList;
             
        } // End of Win Back If Statement
      
      }
      insert actItNewLogoList;
	  insert actItWinBackList;
	  Util.newLogoFlag = false; 
    }  
  
}