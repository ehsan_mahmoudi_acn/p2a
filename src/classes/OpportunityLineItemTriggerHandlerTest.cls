@isTest(SeeAllData = false)
public class OpportunityLineItemTriggerHandlerTest {
    static testMethod void beforeInsertMethod1()
    {
              
        P2A_TestFactoryCls.sampleTestData();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> opportunitys = P2A_TestFactoryCls.getOpportunitys(1,accList);
        
        Product2 prod = new Product2(Name = 'Laptop X200', 
        Family = 'Hardware');
        insert prod;
        system.assert(prod!=null);
        
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = prod.Id,
        UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        system.assert(standardPrice!=null);

        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        system.assertEquals(customPB.Name, 'Custom Pricebook');
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPricebookEntry = new PricebookEntry(
        Pricebook2Id = customPB.Id, Product2Id = prod.Id,
        UnitPrice = 12000, IsActive = true);
        insert customPricebookEntry;
        system.assert(customPricebookEntry!=null);
        
        List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>();
        OpportunityLineItem opplineitem = new OpportunityLineItem();
        opplineitem.pricebookentryid=customPricebookEntry.Id;
        opplineitem.TotalPrice=2000;
        opplineitem.OpportunityID = opportunitys.get(0).Id;
        opplineitem.Quantity = 12;
        oppLineItemList.add(opplineitem); 
        insert oppLineItemList;
        system.assert(oppLineItemList!=null);
        
        OpportunityLineItemTriggerHandler oppLineTrgObj = new OpportunityLineItemTriggerHandler();
        Map<Id,OpportunityLineItem> OppLineMap = new Map<Id,OpportunityLineItem>();
        
        for(OpportunityLineItem oppLineObj : oppLineItemList)
        {
            OppLineMap.put(oppLineObj.Id,oppLineObj);
        } 
        
        oppLineTrgObj.beforeInsert(oppLineItemList);
        oppLineTrgObj.beforeUpdate(oppLineItemList,OppLineMap,OppLineMap);
        oppLineTrgObj.updateReportedSOV(oppLineItemList,OppLineMap,OppLineMap);
    }
    
    
    
    
}