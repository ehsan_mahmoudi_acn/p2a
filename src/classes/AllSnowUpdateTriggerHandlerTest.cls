@isTest(SeeAllData=false)
public class AllSnowUpdateTriggerHandlerTest
{
@istest 
public static void testmethod1()
{
        
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        List<Country_Lookup__c> countryList =  P2A_TestFactoryCls.getcountry(1); 
        List<Contact> contList = P2A_TestFactoryCls.getContact(1, accList);
        List<Site__c> siteList = P2A_TestFactoryCls.getsites(1, accList, countryList);
        //List<BillProfile__c>  billProfList = P2A_TestFactoryCls.getBPs(1, accList ,siteList,contList);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Order__c>  order1=  P2A_TestFactoryCls.getorder(1,OrdReqList);
        order1[0].ownerid=[select id from group where developername='Enrichment_US' and type='queue' limit 1].id;
        update order1;
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
       /* List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        servList[0].Product_Id__c = 'TWI';
        update servList ;*/
        
        
                        
       
       
       List<csord__Service__c> serlist = new List<csord__Service__c>();
                        csord__Service__c selist = new csord__Service__c();
                        selist.Name = 'Test Service'; 
                        selist.AccountId__c = accList[0].id;
                        
                        selist.csord__Identification__c = 'Test-Catlyne-4238362';
                        selist.csord__Order_Request__c = OrdReqList[0].id;
                        selist.csord__Subscription__c = SUBList[0].id;
                        selist.Billing_Commencement_Date__c = System.Today();
                        selist.Stop_Billing_Date__c = System.Today();
                        selist.Customer_Required_Date__c = System.Today()+5;
                        selist.RAG_Status_Red__c = false ; 
                        selist.csord__Order__c = order1[0].id;
                        selist.Root_Service_ID__c  = 'Test1' ;                        
                        selist.csordtelcoa__Product_Basket__c = prodBaskList [0].id;  
                        selist.Product_Id__c = 'TWI';     
                        selist.Path_Instance_ID__c = 'test';
                        serlist.add(selist);
                        insert serlist  ;
                        
                    
              
      Test.startTest();
        
        
        AllSnowUpdateTriggerHandler allsnowTriggetHand = new AllSnowUpdateTriggerHandler ();
       // List<csord__Service__c> servicelist = [ select id,Name,Order_Number__c,Region__c from csord__Service__c where id=:serlist];
        try{
        List<Integration_Update__c> newSnowUpdates = new List<Integration_Update__c>();
                    Integration_Update__c inteupdate = new Integration_Update__c();
                    inteupdate.name = 'Design, Implement and Testing has completed in SNOW';
                    //  inteupdate.Is_SNOW_DIT_Complete__c = true;
                    inteupdate.Milestone__c = true ;
                    inteupdate.OLA_Breached__c = true ;
                    inteupdate.Service_ID__c = serlist[0].id;
                    newSnowUpdates.add(inteupdate);
                    insert newSnowUpdates ;
                    
        allsnowTriggetHand.sendEmailtoSDOnSNOWComplete(newSnowUpdates);
        allsnowTriggetHand.sendEmailNotification(serlist,newSnowUpdates);
         system.assertEquals(true,newSnowUpdates[0].Is_SNOW_DIT_Complete__c);
        }catch(Exception e){}
       
       
        Test.StopTest();
        }

}