public with sharing class AllOrderSubmissionFailuresTriggerHandler extends BaseTriggerHandler {
    @TestVisible
    private Boolean triggerDisabled = false;

    public override Boolean isDisabled(){
        if(Util.muteAllTriggers()){
            return true;
        }
        return triggerDisabled;
    }

    public override void afterInsert(List<SObject> newOrderSub, Map<Id, SObject> newOrderSubMap)
    {
         OrderSubFailureMethod(newOrderSub) ; 
    }

   
    public override void afterUpdate(List<SObject> newOrderSub, Map<Id, SObject> newOrderSubMap, Map<Id, SObject>  oldOrderSubMap) {
        OrderSubFailureMethod(newOrderSub) ; 
    }
    
    //for @Testvisible modified by Anuradha on 2/20/2017
    @Testvisible
    private void orderSubFailureMethod(List<Order_Submission_Failures__c> newOrderSub)
    {
    
    try{
    String ordSubErrDes;
    Map<id,String>orderToErrorMap=new Map<id,String>();
    Map<id,String>serviceToErrorMap=new Map<id,String>();
    List<csord__Order__c> csOrderList = new List<csord__Order__c>();    
    set<id>failedServices=new set<id>();
    for(Order_Submission_Failures__c ordSub : newOrderSub)
    {
      if(ordSub != null && ordSub.Order__c!= null)
      {      
            String errorString=ordSub.Error_Description__c!=null?ordSub.Error_Description__c:''; 
        if(!orderToErrorMap.containsKey(ordSub.Order__c)){
            orderToErrorMap.put(ordSub.Order__c,errorString);
        }
      } 
      //this piece of code will get the service Id for failed service
      
      if(ordSub.Failed_Order_Line_Item__c!=null){
          if(!failedServices.contains(ordSub.Failed_Order_Line_Item__c)){failedServices.add(ordSub.Failed_Order_Line_Item__c);}
      }
      //code for retrieving service id ends
      //creating a map of error linked to service id
      if(ordSub.Failed_Order_Line_Item__c!=null){
         if(!serviceToErrorMap.containsKey(ordSub.Failed_Order_Line_Item__c)){
       String errorString=ordSub.Error_Description__c!=null?ordSub.Error_Description__c:'';
             serviceToErrorMap.put(ordSub.Failed_Order_Line_Item__c,errorString);
             } 
      }
      //code for map ends     
    }
    //retrieving list of failed service details
     List<csord__service__c>failedServList=[select id,csord__order__c,Error_Description__c,RAG_Status_Red__c from csord__service__c where id in:failedServices];
     for(csord__service__c serv:failedServList){
        
        serv.RAG_Status_Red__c=true;//this will enable creation of jeopardy case     
        serv.Error_Description__c=serviceToErrorMap.containsKey(serv.id)?serviceToErrorMap.get(serv.id):serv.Error_Description__c;
      
        //mapping error with order by transfering from service map
        //will be required when order__c is not populated by tibco
        if(serviceToErrorMap.containsKey(serv.id) && serv.csord__order__c!=null){
            
            if(!orderToErrorMap.containsKey(serv.csord__order__c)){
                orderToErrorMap.put(serv.csord__order__c,serviceToErrorMap.get(serv.id));
            }
        }
        //code for error transfer ends
     } 
     //updating RAG flag for failed service
     if(failedServList.size()>0){
        update failedServList; 
     }

    //updating status of failed order    
    if(orderToErrorMap.size()>0)
    {  
      csOrderList = [Select ID, Name, Error_Description__c from csord__Order__c where ID IN : orderToErrorMap.keyset()];    
      for(csord__Order__c csOrdObj : csOrderList)
      {
        csOrdObj.Error_Description__c ='The Service IDs have not been created properly after the order is submitted. Please submit a ticket to Globalserve to resolve the issue.';
        csOrdObj.Status__c = 'Failed';        
      }
      if(csOrderList.size()>0){update csOrderList;}
    } 
       
       //update the created case's description
       String casesub='Service is in RAG Red state.';
       String caseStatus='new';
       List<case>caselist=[select description,CS_Service__c from case where subject=:casesub and status=:caseStatus];
       if(caselist.size()>0){
           for(case c:caselist){
             if(c.CS_Service__c!=null){
                c.description=serviceToErrorMap.get(c.CS_Service__c);
             }  
           }
           update caselist;
       }
    
    }
catch(Exception ex)
{
ErrorHandlerException.ExecutingClassName='AllOrderSubmissionFailuresTriggerHandler:OrderSubFailureMethod';
ErrorHandlerException.objectList=newOrderSub;
ErrorHandlerException.sendException(ex);
System.debug(ex);
}
}
}