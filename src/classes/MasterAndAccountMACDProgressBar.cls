public class MasterAndAccountMACDProgressBar{

    public List<AsyncApexJob> jobs{get; set;}
    public String isFinished{get; set;}
    public Id basketId{get; set;}
    cscfga__Product_Basket__c basket = null;
    boolean batchFailed = false;
    integer waitTimer = 0;
    Set<Id> AsyncApexJobIds = new Set<Id>();

    public MasterAndAccountMACDProgressBar(ApexPages.StandardController ctrl){
        basketId = ctrl.getId();
        refreshJobStatus();
    }

    public void refreshJobStatus(){
        try{
            if(basketId != null){
                basket = [
                    Select Id, cscfga__Basket_Status__c
                    From cscfga__Product_Basket__c
                    Where Id =:basketId Limit 1
                ];
                
                List<Attachment> attachmentList = [
                            Select Id, Name 
                            From Attachment
                            Where ParentId =:basketId
                ];
                if(!attachmentList.IsEmpty()){
                    for(Attachment attachmentName :attachmentList){
                        try{AsyncApexJobIds.add(Id.ValueOf(attachmentName.Name.split('-')[3]));} catch(Exception e) {}
                        system.debug('sap ' +attachmentName.Name);
                        batchFailed = attachmentName.Name.contains('ERRORS');
                    }
                }
            }

            jobs = [
                Select Id, JobItemsProcessed, NumberOfErrors, Status, TotalJobItems
                From AsyncApexJob
                Where Id IN :AsyncApexJobIds
            ];
            
            if(basket != null && basket.cscfga__Basket_Status__c != null && basket.cscfga__Basket_Status__c != ''){
                getBatchOrderStatus();
            }
        } catch(Exception e){
            System.debug('Error: "MasterAndAccountMACDProgressBar; refreshJobStatus" method update failure - ' +e);
          }     
    }
    
    public String getBatchOrderStatus(){

        if(!jobs.IsEmpty()){
            for(AsyncApexJob job :jobs){
                if(job.Status == 'Preparing' || job.Status == 'Holding' || job.Status == 'Queued' || job.Status == 'Processing'){
                    waitTimer = 0;
                    isFinished = '';
                    return 'MACD Basket creation is in progress...';
                }
                else if(job.Status == 'Failed' || job.Status == 'Aborted' || job.NumberOfErrors > 0 || batchFailed){
                    isFinished = 'Error';
                    return 'Error';
                }       
                else if(job.Status == 'Completed'){
                    isFinished = 'Completed';
                }
            }
        }
        if(basket != null && basket.cscfga__Basket_Status__c == 'Processing'){
            return 'MACD Basket creation is in progress...';
        }
        else if(basket != null && basket.cscfga__Basket_Status__c == 'Incomplete'){
            waitTimer++;
            if(Math.mod(waitTimer,24) == 0){
                return 'MACD Basket creation is in progress...' ;
            }
            return 'Completed';
        }       
        return 'MACD Basket creation is in progress...';
    }
}