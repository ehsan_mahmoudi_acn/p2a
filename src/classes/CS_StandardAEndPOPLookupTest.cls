@isTest(SeeAllData = false)
private class CS_StandardAEndPOPLookupTest {
    
    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    private static Id[] excludeIds = new List<Id>();
    private static Integer pageOffset, pageLimit;

    private static List<CS_Country__c> countryList;
    private static List<CS_City__c> cityList;
    private static List<CS_POP__c> popList;
    private static List<CS_Route_Segment__c> routeSegmentList;
    
    private static void initTestData(){
        countryList = new List<CS_Country__c>{
            new CS_Country__c(Name = 'Croatia'),
            new CS_Country__c(Name = 'Australia'),
            new CS_Country__c(Name = 'India'),
            new CS_Country__c(Name = 'Germany'),
            new CS_Country__c(Name = 'Hong Kong')
        };
        
        insert countryList;
        System.debug('****Country List: ' + countryList);
        System.assertEquals('Croatia',countryList[0].Name );
        
        cityList = new List<CS_City__c>{
            new CS_City__c(Name = 'Zagreb', CS_Country__c = countryList[0].Id),
            new CS_City__c(Name = 'New York', CS_Country__c = countryList[1].Id),
            new CS_City__c(Name = 'Bangalore', CS_Country__c = countryList[2].Id),
            new CS_City__c(Name = 'Hong Kong', CS_Country__c = countryList[3].Id),
            new CS_City__c(Name = 'Sydney', CS_Country__c = countryList[4].Id)
        };
        
        insert cityList;
        System.debug('***** City List ' + cityList);
        System.assertEquals('Zagreb',cityList[0].Name );
        
        
        popList = new List<CS_POP__c>{
            new CS_POP__c(Name = 'POP 1', CS_Country__c = countryList[0].Id, CS_City__c = cityList[0].Id, EVPL__c = 'Y'),
            new CS_POP__c(Name = 'POP 2', CS_Country__c = countryList[0].Id, CS_City__c = cityList[0].Id, EVPL__c = 'N'),
            new CS_POP__c(Name = 'POP 3', CS_Country__c = countryList[2].Id, CS_City__c = cityList[2].Id),
            new CS_POP__c(Name = 'POP 4', CS_Country__c = countryList[3].Id, CS_City__c = cityList[3].Id),
            new CS_POP__c(Name = 'POP 5', CS_Country__c = countryList[4].Id, CS_City__c = cityList[4].Id)
        };
        
        insert popList;
        System.debug('****POP List: ' + popList);
         System.assertEquals('POP 1',popList[0].Name ); 
        
        routeSegmentList = new List<CS_Route_Segment__c>{
            new CS_Route_Segment__c(Name = 'Route Segment 1', Product_Type__c = 'IPL', POP_A__c = popList[0].Id, POP_Z__c = popList[4].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 2', Product_Type__c = 'IPL', POP_A__c = popList[1].Id, POP_Z__c = popList[3].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 3', Product_Type__c = 'IPL', POP_A__c = popList[2].Id, POP_Z__c = popList[2].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 4', Product_Type__c = 'EVPL', POP_A__c = popList[3].Id, POP_Z__c = popList[1].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 5', Product_Type__c = 'EPLX', POP_A__c = popList[4].Id, POP_Z__c = popList[0].Id)
        };
        
        insert routeSegmentList;
        System.debug('****Route Segment List: ' + routeSegmentList);
        System.assertEquals('Route Segment 1',routeSegmentList[0].Name ); 
    }
    
    private static testMethod void doLookupSearchTest() {
        Exception ee = null;

        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
            initTestData();
            
            searchFields.put('A-End Country', countryList[0].Id);
            searchFields.put('A End City', cityList[0].Id);
            searchFields.put('IPL', 'true');
            searchFields.put('EPL', 'false');
            searchFields.put('EPLX', 'true');
            searchFields.put('ICBS', 'false');
            searchFields.put('EVPL', 'false');
            searchFields.put('searchValue','');
            
            CS_StandardAEndPOPLookup standardAEndPopLookup = new CS_StandardAEndPOPLookup(); 
            String requiredAtts = standardAEndPopLookup.getRequiredAttributes();
            Object[] data = standardAEndPopLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            
            System.debug('*******Data: ' + data);
            System.assert(data.size() > 0, '');
            
            data.clear();
            searchFields.put('EVPL', 'true');
            
            data = standardAEndPopLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            
            System.debug('*******Data: ' + data);
            System.assert(data.size() > 0, '');
            
            
        } catch(Exception e){
            ee = e;
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }
    }

}