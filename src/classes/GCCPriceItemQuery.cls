global with sharing class GCCPriceItemQuery extends cscfga.ALookupSearch {
    
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){

        String contractterm = searchFields.get('Contract Term');
        String countrygroup = searchFields.get('Country Group');
        String tier = searchFields.get('Tier');
        String workertype = searchFields.get('What worker type is this?');
        //System.Debug('doDynamicLookupSearch' + searchFields);
         List <cspmb__Price_Item__c> data;
        
       data = [SELECT Id, cspmb__One_Off_Cost__c,
                                            cspmb__Recurring_Cost__c,
                                            cspmb__One_Off_Charge__c,
                                            cspmb__Recurring_Charge__c
                                            FROM cspmb__Price_Item__c 
                                            WHERE cspmb__Contract_Term__c =: contractterm 
                                            AND GCC_Group__c=:countrygroup
                                            AND GCC_Tier__c =: tier
                                            AND GCC_Worker_Type__c=:workertype];

        
        System.Debug('PriceItemQuery : '+ data);
    
     
        return data;
    }
    public override String getRequiredAttributes(){ 
        return '[ "cspmb__One_Off_Cost__c","cspmb__Recurring_Cost__c","cspmb__Recurring_Charge__c","cspmb__One_Off_Charge__c"]';
    }
}