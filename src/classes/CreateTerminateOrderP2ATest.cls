/**
@author - Accenture
*/
@isTest(seealldata=false)

public  class CreateTerminateOrderP2ATest{

@istest static void testProcess(){
    
    test.startTest();
    P2A_TestFactoryCls.sampleTestData();
    
     List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);          
     OrdReqList[0].name=String.valueOf(OrdReqList[0].id);
     update OrdReqList;
     
     List<csord__Subscription__c> SUBList = new List<csord__Subscription__c>();//P2A_TestFactoryCls.getSubscription(1, OrdReqList);
     List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
     List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
        
        csord__Subscription__c sub = new csord__Subscription__c();
        sub.Name ='Test'; 
        sub.csord__Account__c = accList[0].id;
        sub.csord__Identification__c = 'Test-Catlyne-4238362';
        sub.csord__Order_Request__c = OrdReqList[0].id;
        sub.OrderType__c = null;
        sub.csord__Order__c=Orders[0].id;
        SUBList.add(sub);
        insert SUBList;
        system.assert(SUBList!=null);
        
        List<csord__Subscription__c> SUBList1 = new List<csord__Subscription__c>();
        csord__Subscription__c sub1 = new csord__Subscription__c();
        sub1.Name ='Test'; 
        sub1.csord__Identification__c = 'Test-Catlyne-4238362';
        sub1.csord__Order_Request__c = OrdReqList[0].id;
        sub1.OrderType__c = null;
        sub1.csord__Order__c=null;
        sub1.csord__Account__c = accList[0].id;
        
        /*sub1.csord__Order_Request__c=OrdReqList[0].csord__Module_Name__c;
        sub1.csord__Order_Request__c=OrdReqList[0].csord__Module_Version__c;
        sub1.csord__Order_Request__c=OrdReqList[0].csord__Processing_Mode__c;
        sub1.csord__Order_Request__c=OrdReqList[0].csord__Processing_Mode__c;
        sub1.csord__Order_Request__c=OrdReqList[0].csord__Process_Status__c;
        sub1.csord__Order_Request__c=OrdReqList[0].Name;
        sub1.csord__Order__c = 'IPVPN';*/
        
        insert sub1;
        SUBList1.add(sub1);
        update SUBList1 ;
        system.assert(SUBList1 !=null);        
     
        List<csord__service__c> servList = P2A_TestFactoryCls.getservice(1,OrdReqList,SUBList); 
        List<csord__service__c> servList1 = P2A_TestFactoryCls.getservice(1,OrdReqList,SUBList1);       
        List<CSPOFA__Orchestration_Process_Template__c>proTemp=P2A_TestFactoryCls.getOrchestrationProcess(1);
        
        
        
        List<CSPOFA__Orchestration_Process__c> orcPro=new List<CSPOFA__Orchestration_Process__c>();//P2A_TestFactoryCls.getOrchestrationProcesss(1,proTemp);

        CSPOFA__Orchestration_Process__c pb = new CSPOFA__Orchestration_Process__c();
        pb.CSPOFA__Orchestration_Process_Template__c= proTemp[0].id;
        pb.csordtelcoa__Subscription__c=SUBList[0].id;
        orcPro.add(pb);
        insert orcPro;
        
        List<CSPOFA__Orchestration_Process__c> orcPro1=new List<CSPOFA__Orchestration_Process__c>();
        CSPOFA__Orchestration_Process__c pb1 = new CSPOFA__Orchestration_Process__c();
        pb1.CSPOFA__Orchestration_Process_Template__c= proTemp[0].id;
        pb1.csordtelcoa__Subscription__c=sub1.id;
        orcPro1.add(pb1);
        insert orcPro1;
        system.assert(orcPro1!=null);
     
    List<CSPOFA__Orchestration_Step__c> stepList=P2A_TestFactoryCls.getOrchestrationStep(1,orcPro);
      Id terminateRecType = RecordTypeUtil.getRecordTypeIdByName(csord__Order__c.SObjectType, 'Cease Order');
    List<CSPOFA__Orchestration_Step__c> stepList1=P2A_TestFactoryCls.getOrchestrationStep(1,orcPro1);
      List<solutions__c> sollist = P2A_TestFactoryCls.getsolutions(1,acclist);
      Map<String, Id>  OdrIdentificationWithOldSubsMap = new Map<String, Id>();
      Map<Id, Id> OldSubWithSolution = new Map<Id, Id>();
      Solutions__c soln = new Solutions__c();
      soln.Account_Name__c = accList[0].id;   //sub1.csord__Account__c; 
      soln.Description__c = sub1.Id;
      soln.Batch_Count__c = 1;
      insert soln ;
      sollist.add(soln);
      update sollist ;
      system.assert(sollist!=null);
      OldSubWithSolution.put(soln.id,soln.id);
    
     csord__Order__c TerminateOrderRec = new csord__Order__c();
     // TerminateOrderRec.recordtypeID =TerminateRecType;
     TerminateOrderRec.Is_Terminate_Order__c = True;
     TerminateOrderRec.csord__Order_Type__c = 'Terminate';
     TerminateOrderRec.Order_Type__c = 'Terminate';
     TerminateOrderRec.Status__c = 'New';
     TerminateOrderRec.Full_Termination__c = true;
     TerminateOrderRec.Is_Order_Submitted__c = false;
     TerminateOrderRec.Order_Submitted__c = false;
     TerminateOrderRec.Solutions__c = OldSubWithSolution.containsKey(sub1.Id)? OldSubWithSolution.get(sub1.Id): null;
     TerminateOrderRec.OldodrId__c = sub1.csord__Order__c!=null? sub1.csord__Order__c: null;
     TerminateOrderRec.Name = sub1.csord__Order__c!=null? 'Terminate-'+sub1.csord__Order__r.Name: 'Terminate-Migrated';
     TerminateOrderRec.csord__Identification__c = sub1.csord__Order__c!=null? sub1.csord__Order__r.csord__Identification__c: sub1.csord__Identification__c;
     OdrIdentificationWithOldSubsMap.put(TerminateOrderRec.csord__Identification__c,sub1.id);
     
    CreateTerminateOrderP2A obj=new CreateTerminateOrderP2A();
    List<Id> odrIdset=new List<Id>();
    String uniqueValue = 'Test';
    String usernameTime = 'TEst1';
    odrIdset.add(Orders[0].id);
    List<sObject>abc= obj.process(stepList);
    List<sObject>abc1= obj.process(stepList1);
    CreateTerminateOrderP2A.sendOrderEmailtoCreator(odrIdset);
    CreateTerminateOrderP2A.getUniqueExternalId('Test','TEst1');
    CreateTerminateOrderP2A.sendExceptionMail('check exception method');
    test.stopTest();
}   

}