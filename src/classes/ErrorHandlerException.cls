public class ErrorHandlerException extends Exception{

    public static id objectId=null;
    public static List<sobject>objectList=null; 
    public static List<Id>objectIdList=null;    
    public static String ExecutingClassName='';
        
    public static void sendException(Exception ex){
             //ErrorHandlerException ex=new ErrorHandlerException();
        try{     
            ErrorHandlerMechanism__c ehm=ErrorHandlerMechanism__c.getInstance('Logger');
            if(ehm.EnableMailer__c){
            String errormsg=
            '\t\t'+'Type of Exception:'+'\t\t'+String.valueOF(ex.getTypeName())+'\n\n'+
            '\t\t'+'Error message:'+'\t\t'+String.valueOF(ex.getMessage())+'\n\n'+
            '\t\t'+'StackTrace: '+'\t\t'+String.valueOf(Ex.getStackTraceString())+'\n\n';
           /* +
            '\t\t'+'Number of Queries:' +'\t\t'+String.valueOf(Limits.getQueries())+'\n\n'+
            '\t\t'+'Number of rows queried: '+'\t\t'+String.valueOf(Limits.getDmlRows())+'\n\n'+
            '\t\t'+'Number of script statements used so far: '+'\t\t'+String.valueOf(Limits.getDmlStatements())+'\n\n';*/
            
            System.debug('error ' + errormsg);
            System.debug('1.Number of Queries used in this apex code so far: ' + Limits.getQueries());
            System.debug('2.Number of rows queried in this apex code so far: ' + Limits.getDmlRows());
            System.debug('3. Number of script statements used so far : ' + Limits.getDmlStatements());
            System.debug('4.Number of Queries used in this apex code so far: ' + Limits.getQueries());
            System.debug('5.Number of rows queried in this apex code so far: ' + Limits.getDmlRows());          
            //if(label.Enable_Triage=='True'){
                //sendExceptionMail(errormsg);
              List<Id>FinalObjectIdList=new List<Id>();
              if(objectList!=null && objectList.size()>0){
               for(sobject so:objectList){
                  FinalObjectIdList.add(so.id);
               }
              }
              else if(objectIdList!=null && objectIdList.size()>0){
              FinalObjectIdList.addAll(objectIdList);
              } 
               
              if(!System.isBatch() && !system.isFuture() && !System.isScheduled()){
                logExceptionAsync(String.valueOF(ex.getTypeName()),errormsg,objectId,FinalObjectIdList,ExecutingClassName);              
                              
              }
                else{
                   logException(String.valueOF(ex.getTypeName()),errormsg,objectId,FinalObjectIdList,ExecutingClassName);
                    
                }
                objectId=null;
                objectList=null;
                objectIdList=null;
                ExecutingClassName='';
            }
                  
            }catch(Exception e){System.debug(e);}
            

    }

  //@future   
 public static void sendExceptionMail(String errormsg){
            //sending logs via mail
            List<messaging.singleEmailMessage>mails=new List<messaging.singleEmailMessage>();
            messaging.singleEmailMessage mail=new messaging.SingleEmailMessage();
            List<String>sendTo=new List<String>();
            List<String>getsenders=label.ExceptionReceiver.split(',');
            for(String email:getSenders){
                system.debug('email'+email);
                sendTo.add(email);
            }               
            mail.setToAddresses(sendTo);     
        
            String subjectMsg='Service Failed in After update';
            mail.setsubject(subjectMsg);
            mail.setPlainTextBody(errormsg);
            mails.add(mail);
            messaging.sendemail(mails);
            system.debug('mail sent');
            //ends here 
        }
    
    //@future 
    public static void logException(String name,String errormsg,id tempid,List<Id>objectIdList,String classMethodString){
        
       String Datetimeval=String.valueOf(DateTime.now());
        String[]dtarr=Datetimeval.split(' ');
        String[]tarr=dtarr[1].split(':');
        String timeval='';
        integer hourval=Integer.valueOf(tarr[0]);   
        if(hourval>12){timeval= string.valueOf(hourval-12)+':'+tarr[1]+':'+tarr[2]+' '+'PM';}
        else{timeval=dtarr[1]+' '+'AM';}
        String className='';
        String MethodName ='';       
        if(classMethodString!='' && classMethodString!=null){
         List<String>tempStrList=classMethodString.split(':');
        className=tempStrList.size()>0?tempStrList[0]:'';
        MethodName=tempStrList.size()>1?tempStrList[1]:'';
        }
        
        
        List<Error__c>InsertErrorList=new List<Error__c>();
        if(objectIdList!=null && objectIdList.size()>0){
        for(id keyId:objectIdList){
        
        Error__c err = new Error__c();      
        err.name=name;
        err.Log_Date__c=dtarr[0];
        err.Log_Time__c=timeval;
        err.created_date__c=system.today();
        String []classArray1=errormsg.split(':');
        String []classArray2=classArray1[0].split(' ');
        err.Class_Name__c=classArray2[classArray2.size()-1];
        err.Executing_Class_Name__c=className;
        err.Method_Name__c=MethodName;
        err.Submitted_By__c=UserInfo.getUserName();
        if(keyId!=null){                               
                String sObjName = keyId.getSObjectType().getDescribe().getName();
                //SObject record = Database.query('Select Name From ' + sObjName + ' Where Id = :tempid');
                //String RecordName=String.valueOf(record.get('name'));
                //System.debug('id objectname name '+tempid+' '+sObjName+' '+String.valueOf(record.get('name')));
        err.Object_ID__c=keyId;
       // err.Object_Name__c=RecordName;
        err.Object_Type__c=sObjName;
            }        
        err.Stack_Trace__c=errormsg;
        InsertErrorList.add(err);
        }
        if(InsertErrorList.size()>0)insert InsertErrorList;
        }
        
        
        
        else{
        
        Error__c err = new Error__c();      
        err.name=name;
        err.Log_Date__c=dtarr[0];
        err.Log_Time__c=timeval;
        err.created_date__c=system.today();
        String []classArray1=errormsg.split(':');
        String []classArray2=classArray1[0].split(' ');
        err.Class_Name__c=classArray2[classArray2.size()-1];
        err.Executing_Class_Name__c=className;
        err.Method_Name__c=MethodName;
        err.Submitted_By__c=UserInfo.getUserName();
        if(tempid!=null){                               
                String sObjName = tempid.getSObjectType().getDescribe().getName();
                //SObject record = Database.query('Select Name From ' + sObjName + ' Where Id = :tempid');
                //String RecordName=String.valueOf(record.get('name'));
                //System.debug('id objectname name '+tempid+' '+sObjName+' '+String.valueOf(record.get('name')));
        err.Object_ID__c=tempid;
       // err.Object_Name__c=RecordName;
        err.Object_Type__c=sObjName;
            }        
        err.Stack_Trace__c=errormsg;
        insert err;      

        
        }     
    }
    
    @future
    public static void logExceptionAsync(String name,String errormsg,id tempid,List<Id>objectIdList,String classMethodString){
        String Datetimeval=String.valueOf(DateTime.now());
        String[]dtarr=Datetimeval.split(' ');
        String[]tarr=dtarr[1].split(':');
        String timeval='';
        integer hourval=Integer.valueOf(tarr[0]);   
        if(hourval>12){timeval= string.valueOf(hourval-12)+':'+tarr[1]+':'+tarr[2]+' '+'PM';}
        else{timeval=dtarr[1]+' '+'AM';}
        String className='';
        String MethodName ='';       
        if(classMethodString!='' && classMethodString!=null){
         List<String>tempStrList=classMethodString.split(':');
        className=tempStrList.size()>0?tempStrList[0]:'';
        MethodName=tempStrList.size()>1?tempStrList[1]:'';
        }
        
        
        List<Error__c>InsertErrorList=new List<Error__c>();
        if(objectIdList!=null && objectIdList.size()>0){
        for(id keyId:objectIdList){
        
        Error__c err = new Error__c();      
        err.name=name;
        err.Log_Date__c=dtarr[0];
        err.Log_Time__c=timeval;
        err.created_date__c=system.today();
        String []classArray1=errormsg.split(':');
        String []classArray2=classArray1[0].split(' ');
        err.Class_Name__c=classArray2[classArray2.size()-1];
        err.Executing_Class_Name__c=className;
        err.Method_Name__c=MethodName;
        err.Submitted_By__c=UserInfo.getUserName();
        if(keyId!=null){                               
                String sObjName = keyId.getSObjectType().getDescribe().getName();
                //SObject record = Database.query('Select Name From ' + sObjName + ' Where Id = :tempid');
                //String RecordName=String.valueOf(record.get('name'));
                //System.debug('id objectname name '+tempid+' '+sObjName+' '+String.valueOf(record.get('name')));
        err.Object_ID__c=keyId;
       // err.Object_Name__c=RecordName;
        err.Object_Type__c=sObjName;
            }        
        err.Stack_Trace__c=errormsg;
        InsertErrorList.add(err);
        }
        if(InsertErrorList.size()>0)insert InsertErrorList;
        }
        
        
        
        else{
        
        Error__c err = new Error__c();      
        err.name=name;
        err.Log_Date__c=dtarr[0];
        err.Log_Time__c=timeval;
        err.created_date__c=system.today();
        String []classArray1=errormsg.split(':');
        String []classArray2=classArray1[0].split(' ');
        err.Class_Name__c=classArray2[classArray2.size()-1];
        err.Executing_Class_Name__c=className;
        err.Method_Name__c=MethodName;
        err.Submitted_By__c=UserInfo.getUserName();
        if(tempid!=null){                               
                String sObjName = tempid.getSObjectType().getDescribe().getName();
                //SObject record = Database.query('Select Name From ' + sObjName + ' Where Id = :tempid');
                //String RecordName=String.valueOf(record.get('name'));
                //System.debug('id objectname name '+tempid+' '+sObjName+' '+String.valueOf(record.get('name')));
        err.Object_ID__c=tempid;
       // err.Object_Name__c=RecordName;
        err.Object_Type__c=sObjName;
            }        
        err.Stack_Trace__c=errormsg;
        insert err;  
        
        }     
        
            
    }

}