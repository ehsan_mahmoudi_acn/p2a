global class DynamicLLOlaLookup extends cscfga.ALookupSearch {


    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields,
String productDefinitionID){
        List<OLA__c> olas = [SELECT Id, Name, OLAField__c FROM OLA__c
            WHERE Name = :searchFields.get('OLALookupString')];
        IF (olas.isEmpty() ) {
            List<OLA__c> olas2 = [SELECT Id, Name, OLAField__c FROM OLA__c
                WHERE Name = :searchFields.get('OLALookupString2')];
            IF (olas2.isEmpty() ) {
                 List<OLA__c> olas3 = [SELECT Id, Name, OLAField__c FROM OLA__c
                    WHERE Name = :searchFields.get('OLALookupString3')];
                 IF (olas3.isEmpty() ) {
                    return null;
                    }
            Else{
                return olas3;
                }
                }
            Else{
                return olas2;
                }
            }
        Else {
            return olas;
            }
 }

 
     public override String getRequiredAttributes(){
         return '["OLALookupString", "OLALookupString2", "OLALookupString3"]';
 }
 
 
}