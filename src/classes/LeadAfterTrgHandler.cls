public with sharing class LeadAfterTrgHandler 
{
     public Id refLeadOwnerId{get;set;} 
      public List<Account> accObjList{get;set;} 
    //public Account accObj{get;set;} 
     //Method for handling if lead is interested in exisiting Account
     public void existLeadTrigHandler(List<Lead> trigNew,boolean isInsert, boolean isAfter)
     {
        List<String> accid=new List<String>();
        map<Id,User> usrMap = new  map<Id,User>();
        List<Id> accOwnerId = new List<Id>();
   
        Account accObj = new Account();
        List<String> emailStr= new List<String>();
        Set<Id> recId = new Set<Id>();
        List<Id> ownerId=new List<Id>();
        set<id> targetUserId= new set<Id>();
        Set<String> ctrStrList = new Set<String>(); 
    
        
        List<Account> accObjList= new List<Account>();
        Map<Id,Account> accMap = new Map<Id,Account>();
    
        
        for(Lead leadObj : trigNew)
        {   
            if(leadObj.getSObjectType().getDescribe().getName()== 'Lead' &&  leadObj.Lead_Account_Id__c != null )
            {
                accid.add(leadObj.Lead_Account_Id__c);
                recId.add(leadObj.Id);
            }
        } 
        
        //Checking size and existing Account ID is exist and Active
        if(accid.size() > 0 )
        {
        
            accObjList= [Select Id,Name,Account_Id__c,OwnerId,Account_Status__c from account where Account_Id__c IN : accid
                  and Account_Status__c='Active'];
            // refLeadOwnerId = accObj.OwnerId; 
             //accObjList.add(accObj);
        
        }
       
        if(accObjList.size() > 0 )
        {           
            for(Account accObjOwner : accObjList)  
            {
                accOwnerId.add(accObjOwner.OwnerId);
            }                 
        }
       
        if(accOwnerId.size() > 0 ) 
        usrMap = new map<Id,User>([Select Id,Name,email From User Where Id IN : accOwnerId]);
    
        if(accObjList.size()> 0 ) 
        {
            
            for(Account accObjId :accObjList)
            {    
                emailStr.add(usrMap.get(accObjId.OwnerId).email);
                
            }    
        }          
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();          
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
        EmailTemplate etforexstLogo=[Select id,Name,subject,body from EmailTemplate where DeveloperName=:'Lead_Object_task_for_existing_account_assigned_to_Sales_Account_Owner'];
        
        for(Lead leadObj : trigNew) 
        {
            if(isInsert && isAfter && accObjList.size() > 0 && leadObj.Lead_Account_Id__c != null) 
            {   
                email.setTemplateId(etforexstLogo.id);
                email.setTargetObjectId(leadObj.Id);
                email.setUseSignature(false);           
                email.setSaveAsActivity(false);  
                emailList.add(email); 
               
            }  
        }
        
        if(!emailList.isEmpty()){
        try{
                system.debug('Email Sent :::');
                Messaging.sendEmail(emailList);
            } catch(Exception e){
                ErrorHandlerException.ExecutingClassName='LeadAfterTrgHandler :existLeadTrigHandler';   
                ErrorHandlerException.objectList=trigNew;
                ErrorHandlerException.sendException(e); 
                 System.debug('The following exception has occurred: ' + e.getMessage());
                
            }    
        } 
     }
     // End of Method for handling if lead is interested in exisiting Account
      //commented the below Method as part of fix TGI0253386 -- Lead Assignment issue
     //Method for LeadSequence Controller
     /*public void leadSequenceController(List<Lead> trigNew,boolean isupdate, boolean isAfter)
     {
         //Commented for loop due to SOQL query.
        //for(Lead leadObj : trigNew)
        //{
           // if(isupdate && isAfter)
           // {
                //LeadSequenceController Lead = new LeadSequenceController();
                //Lead.afterUpdate();
           // }    
        //}        
     }
    
    // End of Method for Lead Sequence Controller*/
    
}