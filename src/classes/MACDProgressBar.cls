public class MACDProgressBar{

    public List<AsyncApexJob> jobs{get; set;}
    public Id subscriptionId{get; set;}
    public String isFinished{get; set;}
    public Id basketId{get; set;}
    Map<Id, String> batchProgress = new Map<Id, String>();
    csord__Subscription__c subscription = null;
    cscfga__Product_Basket__c basket = null;
    boolean batchFailed = false;
    Integer waitTimer = 0;
    Set<Id> AsyncApexJobIds = new Set<Id>();

    public MACDProgressBar(ApexPages.StandardController ctrl){
        subscriptionId = ctrl.getId();
        refreshJobStatus();
        getBatchOrderStatus();
    }

    public void refreshJobStatus(){
        try{        
            subscription = [
                        Select Id, MACD_Batch_Job_Id__c, MACD_Basket_Id__c
                        From csord__Subscription__c
                        Where Id =:subscriptionId Limit 1
                ];
                                    
            if(subscription != null && subscription.MACD_Batch_Job_Id__c != null && subscription.MACD_Batch_Job_Id__c != ''){
                basketId = subscription.MACD_Basket_Id__c!=null? subscription.MACD_Basket_Id__c: null;
                
                if(basketId != null){
                    basket = [
                        Select Id, cscfga__Basket_Status__c
                        From cscfga__Product_Basket__c
                        Where Id =:basketId Limit 1
                    ];
                    
                    List<Attachment> attachmentList = [
                                Select Id, Name 
                                From Attachment
                                Where ParentId =:basketId
                    ];
                    if(!attachmentList.IsEmpty()){
                        for(Attachment attachmentName :attachmentList){
                            try{AsyncApexJobIds.add(Id.ValueOf(attachmentName.Name.split('-')[3]));} catch(Exception e) {}
                            batchFailed = attachmentName.Name.contains('ERRORS');
                        }
                    }
                }

                jobs = [
                    Select Id, JobItemsProcessed, NumberOfErrors, Status, TotalJobItems
                    From AsyncApexJob
                    Where Id =:subscription.MACD_Batch_Job_Id__c or Id IN :AsyncApexJobIds
                ];
            }
        } catch(Exception e){
            System.debug('Error: "MACDProgressBar; refreshJobStatus" method update failure - ' +e);
          }     
    }
    
    public String getBatchOrderStatus(){
        if(!jobs.IsEmpty()){
            for(AsyncApexJob job :jobs){
                if(job.Status == 'Preparing' || job.Status == 'Holding' || job.Status == 'Queued' || job.Status == 'Processing'){
                    waitTimer = 0;
                    return 'MACD Basket creation is in progress...';
                }
                else if(job.Status == 'Failed' || job.Status == 'Aborted' || job.NumberOfErrors > 0 || batchFailed){
                    isFinished = 'Error';
                    return 'Error';
                }       
                else if(job.Status == 'Completed' && (batchProgress.get(job.Id) == null || batchProgress.KeySet().Size() == jobs.Size())){
                    batchProgress.put(job.Id, job.Status);
                }
            }

            if(basket != null && batchProgress.KeySet().Size() == jobs.Size()){
                if(basket.cscfga__Basket_Status__c == 'Processing'){
                    return 'MACD Basket creation is in progress...';
                }                   
                else if(basket.cscfga__Basket_Status__c == 'Incomplete'){
                    waitTimer++;
                    if(Math.mod(waitTimer,12) == 0){
                        return 'Waiting for all the products to be added to the basket...' ;
                    }                       
                    isFinished = 'Completed';
                    return 'Completed';
                }           
            }           
        }       
        return 'MACD Basket creation is in progress...';
    }   
}