public class SiteInstallerAddressExtController {
    
    public site__c sites {get;set;}
    public string accountid {get;set;}
    public string accountids {get;set;}
    
    public SiteInstallerAddressExtController(ApexPages.StandardController controller) { 
    accountid = ApexPages.currentPage().getParameters().get('attributeValues'); 
    String BasketId = ApexPages.currentPage().getParameters().get('BasketId');
    System.debug('<><> Basket ID == ' + BasketId);
    system.debug('@@@@'+accountid);
   
    sites = (Site__c)controller.getRecord(); 
  //String accountIdlength=(system.label.SSSS).length();
   //  Id accountIdfromStr=accountid.contains('Account Id=') && accountid.length()>Integer.valueof(accountid.indexOf('Account Id=',0)+11) ?Id.valueof(accountid.substring(accountid.indexOf('Account Id=',0)+11, accountid.indexOf('Account Id=',0)+11+18)):null;
   
  
     //      sites.AccountId__c=accountIdfromStr;
    }
    
    
   /* public String getAccID(){
    
        System.debug('In the function');        
        return 'abc';
    }*/
    
    public PageReference save(){
     insert sites;
       //PageReference ReturnPage = new PageReference('/' + sites.id);   
      //Start- Suresh: 20/09/2017 changes for tech_debt to avoid hardcoded URL
     PageReference SiteInstallerAddressReplicate= Page.SiteInstallerAddressReplicate;
               SiteInstallerAddressReplicate.getParameters().put('id', sites.id); 
                //End- Suresh
     return SiteInstallerAddressReplicate;
    }
    
    public PageReference saveAndNew() {
     insert sites;
         //PageReference page = new PageReference('/apex/SiteInstallerAddressReplicate?accountid='+ sites.AccountId__c);
     //Start- Suresh: 20/09/2017 changes for tech_debt to avoid hardcoded URL
     PageReference SiteInstallerAddressReplicate= Page.SiteInstallerAddressReplicate;
               SiteInstallerAddressReplicate.getParameters().put('id', sites.AccountId__c);
                    //End- Suresh
    //page.setredirect(true);
     return SiteInstallerAddressReplicate;
    }
    
 }