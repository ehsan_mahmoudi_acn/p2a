global with sharing class  FeasibilityStudy {

//For Test class coverage commented by Anuradha in the GIT 3/14/2017 and this class is in scope for R1.1 or later release
   /* webservice static void Feasibilitycase(list<opportunity> Optylist){
        Set<ID> OptyIDs = new SEt<id>();
        for(Opportunity O :Optylist){
            OptyIDs.add(O.id);
        }
        List<Opportunity> OptyFeasibleSync = new List<Opportunity>();
        List<case> NewFeasiblitycase = new List<case>();
        List<cscfga__Product_Basket__c> SyncOppotyPB = new List<cscfga__Product_Basket__c>();
        List<case> ParentCaseLst = new List<case>();
        List<case> Childcase = new List<case>();
        List<case>Nosync = new List<case>();
        Map<id,id>PctoParentCaseId = new map<id,id>();

        SyncOppotyPB =[Select Id,cscfga__Opportunity__r.Order_Type__c,Name,cscfga__Opportunity__r.name,cscfga__Opportunity__c,cscfga__Opportunity__r.OwnerID,cscfga__Opportunity__r.accountId,cscfga__Opportunity__r.account.Region__c,cscfga__Opportunity__r.account.Customer_Type_New__c from cscfga__Product_Basket__c where cscfga__Opportunity__c in :OptyIDs and csordtelcoa__Synchronised_with_Opportunity__c =true and csbb__Synchronised_With_Opportunity__c =true];

        // query to get all queues corresponding to Bill profile Object
        List<QueueSobject> que = QueueUtil.getQueuesListForObject(Case.SObjectType);    
        Map<Id,Id> queueids = new Map<ID,id>();
        Map<String, Id> queMap= new Map<String, Id> ();
        for(opportunity opty :Optylist){
            for (QueueSObject q:que){
                if(opty.account.Region__c =='EMEA' && ((q.Queue.Name).contains(opty.account.Region__c) &&( (q.Queue.Name).contains('Tech')))){
                    queueids.put(opty.id,q.QueueId);
                }
                else if(((q.Queue.Name).contains(opty.account.Region__c) && ((q.Queue.Name).contains(opty.account.Customer_Type_New__c)) &&( (q.Queue.Name).contains('Tech')))){
                    queueids.put(opty.id,q.QueueId);
                }
            }
        }

        List<Case> OpenFeasibilityCaseList = [select id,status from Case where Opportunity_Name__c =:Optylist[0].Id and status NOT IN ('Complete','Cancelled') and Is_Feasibility_Request__c=True];
        List<Opportunity> oppscase = [select id,OwnerID,accountId,csordtelcoa__Change_Type__c from opportunity where id in:Optylist];

        for(opportunity pb: oppscase){
            if(OpenFeasibilityCaseList.size()==0){
                Case nonsynccase= new Case();  
                nonsynccase.Origin = 'Web';  
                nonsynccase.Status = 'Unassigned';
                nonsynccase.Origin = 'Web';
                nonsynccase.Subject ='Request Feasibility Study'; 
                nonsynccase.Opportunity_Name__c=pb.id;
                nonsynccase.Opportunity_Owner__c =  pb.OwnerID; 
                nonsynccase.recordtypeID = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'FeasibilityOpportunityLayout');  
                nonsynccase.accountId = pb.accountId; 
                nonsynccase.ownerid =queueids.get(pb.id); 
                nonsynccase.Is_Feasibility_Request__c=true;
                system.debug('####'+pb);
                nonsynccase.Order_Type__c = pb.csordtelcoa__Change_Type__c;
                if(SyncOppotyPB.size()>0){
                    nonsynccase.Product_Basket__c=SyncOppotyPB[0].id ;
                }

                Nosync.add(nonsynccase);
                System.debug('@@@@Nosync'+Nosync);
            }
            insert Nosync; 
        }

        if(Nosync.size()>0){
            String queuename;
            for(QueueSobject caseQueue : que){
                if(caseQueue.Id == queueids.get(Optylist[0].Id)){
                    queuename = caseQueue.Queue.Name;
                    break;
                }
            }
            
            
            List<GroupMember> gm = [select UserOrGroupId From GroupMember where groupId =:queueids.get(Optylist[0].id) ];
            List<id> uid = new list<id>();
            
            for( GroupMember g : gm) {
                uid.add(g.UserOrGroupId);   
            }
            
            List<user> uemail = [select email from user where id =: uid];

            List<String> email = new list<String>();
            String emailadress ;
            for( user u : uemail) {
                email.add(u.email);  
                emailadress =u.email+':';
            }
            
            List<String> toAddresses = new List<String>();
            for(String a : email){
                toAddresses.add(a);
            }        

            List<user> usernameinfo = [select Firstname,Lastname from user where id=:Optylist[0].ownerid];
            if(email.size()>0){
                for(opportunity oppty : Optylist){             
                    String instanceURLofopty = URL.getSalesforceBaseUrl().toExternalForm() + '/'+oppty.id ;
                    String instanceuRlcase = URL.getSalesforceBaseUrl().toExternalForm() + '/'+Nosync[0].id;
                    String usernameemail;
                    
                    if(usernameinfo .size() > 0){
                        usernameemail = usernameinfo[0].firstname +'' +usernameinfo[0].lastname;
                    }

                    String body = 'Hi Team,\nAn new Feasibility study request has been requested for opportunity  ' + oppty.name + '\n\n';
                    body += 'Please visit the opportunity and take necessary actions for the Feasibility study request assigned to you.\n\n';
                    body += 'URL for the Opportunity : ' + instanceURLofopty + '\n';                
                    body += 'URL for the Case: ' + instanceuRlcase +'\n\n';
                    body += 'Thanks,'+'\n';
                    body += usernameemail ;

                    EmailUtil.EmailData mailData = new EmailUtil.EmailData();
                    mailData.addToAddresses(toAddresses); 
                    mailData.message.setSubject('Feasibilty Study');
                    mailData.message.setPlainTextBody(body);
                    mailData.message.setSaveAsActivity(false);
                    
                    EmailUtil.sendEmail(mailData);
                }
            }           
        }
    } */
}