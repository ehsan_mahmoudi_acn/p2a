global class SchedulerPlugins {
    global class SecurityProvider implements CSSX.SecuritySignature {
        public String sign(Blob input) {
            return CSSEC.API_V1.sign(input);
        }
        public Boolean verify(Blob input, String signature) {
            return CSSEC.API_V1.verifyMAC(input, signature);
        }
        public String getIdentifier() {
            return 'HOLIDAYS_SERVICE_SECURITY';
        }
    }
    global class SecurityProviderFactory implements CSSX.PluginFactory {
        public Object create() {
            return new SecurityProvider();
        }
    }
}