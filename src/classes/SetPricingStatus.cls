/**
* @author Telstra Accenture
* @description This class sets the quote status in the basket based on the pricing Approval Case status
*/

public class SetPricingStatus{

/**
* @author Telstra Accenture
* @description The below method is ment to calculate the basket's quote status based on Attribute and Product Configuration values
* @param Map of New and old product basket 
* @param List of all product confifurations added in the basket
* @return Map of product basket
*/
  
    public static Map<Id, cscfga__Product_Basket__c> getpricingStatus(Map<Id, cscfga__Product_Basket__c> newBasketsMap, Map<Id, cscfga__Product_Basket__c> oldBasketsMap, List<cscfga__Product_Configuration__c> allProdConfigs, boolean setQuoteStatus){
        
        /**
         * Get the valid basket ID i.e those which are not in closed won stage
         * Only these basket id's will be processed throughout this method
         */
        
        /** Initialise map for return statement*/
        Map<Id, cscfga__Product_Basket__c> ProductBasketStatusMap = new Map<Id, cscfga__Product_Basket__c>();
        Set<Id> openOpportunityBaskets = new Set<Id>();
		List<cscfga__Product_Configuration__c> allBasketProdConfigs = new List<cscfga__Product_Configuration__c>();
		
        for(cscfga__Product_Basket__c basket :newBasketsMap.values()){
			
			if(setQuoteStatus && (oldBasketsMap.get(basket.Id).CountOfProductConfiguration__c != basket.CountOfProductConfiguration__c || oldBasketsMap.get(basket.Id).Quote_Status__c != basket.Quote_Status__c) && basket.csordtelcoa__Basket_Stage__c != 'Closed Won'){
				
				if(basket.Account_Customer_Type__c != 'ISO' && basket.Opportunity_Stage__c != 'Closed Sales'){
					openOpportunityBaskets.add(basket.Id); /** Set of valid baskets*/
				}
				else if(basket.Account_Customer_Type__c == 'ISO'){
					openOpportunityBaskets.add(basket.Id); /** Set of valid baskets*/
				}
			}			
        }
        
        /** Below query gets all the product configs associated with the valid basket(s)*/
        if(!openOpportunityBaskets.isEmpty() && setQuoteStatus){

            String productConfigQuery = 'Select Id, '+ 
				'cscfga__Configuration_Status__c, cscfga__Product_Basket__c, cscfga__Product_Family__c, CountOfAttributeValueChange__c '+
				'From cscfga__Product_Configuration__c '+
				'Where cscfga__Product_Basket__c =:openOpportunityBaskets';
            
            allBasketProdConfigs = database.query(productConfigQuery);
            /** End of query and obtained all Product Configurations*/
		} else if(!setQuoteStatus){
			allBasketProdConfigs = allProdConfigs;
		}
		
		if(allBasketProdConfigs.Size() > 0){

            /** Now we need to organise each product basket and it's associated PC through map like this Map<basket Id,list<associated PC>>*/
            /** Association code starts. Initialising the map*/
            Map<Id, List<cscfga__Product_Configuration__c>> productConfigByBasketMap = new Map<Id, List<cscfga__Product_Configuration__c>>();

            for(cscfga__Product_Configuration__c PC :allBasketProdConfigs){

				/** When basket is already present in map*/
                if(PC != null && productConfigByBasketMap.keySet().contains(PC.cscfga__Product_Basket__c)){
                    /** Initialising the list and added the PC*/
                    productConfigByBasketMap.get(PC.cscfga__Product_Basket__c).add(PC);
                }   
                /** When a new basket has to be added to the map*/
                else{
                    productConfigByBasketMap.put(PC.cscfga__Product_Basket__c,new List<cscfga__Product_Configuration__c>());
                    productConfigByBasketMap.get(PC.cscfga__Product_Basket__c).add(PC);
                }
                
            }
            /** Association code for productConfigByBasketMap ends*/
            
            for(cscfga__Product_Basket__c prodbasket :newBasketsMap.values()){

                /** Status change on Basket when created from Account - ISO Orders*/
                if(setQuoteStatus && (prodbasket.csordtelcoa__Account__c != null || prodbasket.csbb__Account__c != null) && prodbasket.cscfga__Opportunity__c == null && (prodbasket.Account_Customer_Type__c == 'ISO')){
                    prodbasket.cfgoffline__Account__c = prodbasket.csordtelcoa__Account__c;  
                }
                if(setQuoteStatus && (prodbasket.csordtelcoa__Account__c != null || prodbasket.csbb__Account__c != null) && (oldBasketsMap.get(prodbasket.Id).Quote_Status__c != 'Accepted' && prodbasket.Quote_Status__c == 'Accepted') && prodbasket.cscfga__Opportunity__c == null && (prodbasket.Account_Customer_Type__c == 'ISO')){
                    prodbasket.csordtelcoa__Basket_Stage__c = 'Closed Won';
                    prodbasket.csordtelcoa__Process_Order_Generation_In_Batch__c = true;
                    prodbasket.Order_Creator__c = prodbasket.P2O_SFDC_id__c;
                }
                
                /**
                 * Condition to check if opportunity is not Closed Won or Closed Sales. If Yes, the basket is already approved.
                 */                
                Boolean opportunityFlag = false;
                Boolean closedsalesFlag = false; 
                if(prodbasket.Opportunity_Stage__c != null && prodbasket.Opportunity_Stage__c == 'Closed Won'){
                    opportunityFlag = true;
                } else if(prodbasket.Opportunity_Stage__c != null && prodbasket.Opportunity_Stage__c == 'Closed Sales'){
                    closedsalesFlag = true;
                }
                
                if(prodbasket.Account_Customer_Type__c != 'ISO' && !opportunityFlag){

                    /** Setting the baskets quote status*/
                    if(productConfigByBasketMap.containsKey(prodbasket.Id) && productConfigByBasketMap.get(prodbasket.Id) != null){
                        if(isIncomplete(productConfigByBasketMap, prodbasket)){
                            prodbasket.Quote_Status__c = 'Draft'; 
                        }
                        else if(!closedsalesFlag && isQuoteRejected(productConfigByBasketMap, prodbasket, newBasketsMap)){
                            prodbasket.Quote_Status__c = 'Rejected';
                        }
                        else if(!closedsalesFlag && isQuoteAccepted(productConfigByBasketMap, prodbasket, newBasketsMap, oldBasketsMap)){
                            prodbasket.Quote_Status__c = 'Accepted';
                        }
                        else if(!closedsalesFlag && isPendingPricingRequired(productConfigByBasketMap, prodbasket)){
                            prodbasket.Quote_Status__c = 'Pending Pricing Approval';    
                        }
                        else{
                            prodbasket.Quote_Status__c = 'Approved';
                        }    
                    }                      
                }
                else if(prodbasket.Account_Customer_Type__c == 'ISO' && prodbasket.csordtelcoa__Basket_Stage__c != 'Closed Won' && prodbasket.csordtelcoa__Order_Generation_Batch_Job_Id__c == null){
                    /** Incase no configuration added, the basket status should be draft (QC#15414)*/
                    if(productConfigByBasketMap.Size() == 0){
                        prodbasket.Quote_Status__c='Draft';
                    } 
                    else{
                        /** Incase of ISO accounts quote status will be directly approved*/
                        if(isIncomplete(productConfigByBasketMap, prodbasket)){
                            prodbasket.Quote_Status__c = 'Draft'; 
                        }
                        else if(isQuoteRejected(productConfigByBasketMap, prodbasket, newBasketsMap)){
                            prodbasket.Quote_Status__c = 'Rejected';
                        }   
                        else if(isQuoteAcceptedISO(productConfigByBasketMap, prodbasket, newBasketsMap, oldBasketsMap)){
                            prodbasket.Quote_Status__c = 'Accepted';
                        }   
                        else{
                            prodbasket.Quote_Status__c = 'Approved'; 
                            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                            req1.setComments('Automatic submit.');
                            req1.setObjectId(prodbasket.id);
                            Approval.ProcessResult result = Approval.process(req1);
                        } 
                    }
                }
                ProductBasketStatusMap.put(prodbasket.Id, prodbasket);
            }
            /** End of if condition*/
        }
        return ProductBasketStatusMap;
    }
    /** Method ends here*/
    
/**
* @author Telstra Accenture
* @description Method to check if basket is incomplete for Non-ISO accounts
* @param Id of product basket 
* @param List of all product confifurations added in the basket
* @return Boolean
*/
 
    public static Boolean isIncomplete(Map<Id, List<cscfga__Product_Configuration__c>> productConfigByBasketMap, cscfga__Product_Basket__c prodbasket){
        Boolean isIncomplete = false;
        if(!productConfigByBasketMap.IsEmpty() && prodbasket.Id != null && productConfigByBasketMap.containsKey(prodbasket.Id)){
            for(cscfga__Product_Configuration__c pc :productConfigByBasketMap.get(prodbasket.Id)){
                if(pc.cscfga__Configuration_Status__c != 'Valid'){
                    isIncomplete = true;
                    break;      
                }
            }
        }
        return isIncomplete;
    }
/**
* @author Telstra Accenture
* @description Method to check if basket is rejected for Non-ISO accounts
* @param Id and Map of product basket 
* @param List of all product confifurations added in the basket
* @return Boolean
*/
	
    public static  Boolean isQuoteRejected(Map<Id, List<cscfga__Product_Configuration__c>> productConfigByBasketMap, cscfga__Product_Basket__c prodbasket, Map<Id,cscfga__Product_Basket__c> newBasketsMap){
        Boolean isQuoteRejected = false;
        if(!productConfigByBasketMap.IsEmpty() && prodbasket.Id != null && productConfigByBasketMap.containsKey(prodbasket.Id)){
            for(cscfga__Product_Configuration__c pc :productConfigByBasketMap.get(prodbasket.Id)){
                String newRejected = newBasketsMap.get(pc.cscfga__Product_Basket__c).Quote_Status__c;
                if(newRejected == 'Rejected'){
                    isQuoteRejected = true;
                    break;
                }
            }
        }
        return isQuoteRejected;
    }
/**
* @author Telstra Accenture
* @description Method to check if basket is accepted for Non-ISO accounts
* @param Id and Map of product basket 
* @param List of all product confifurations added in the basket
* @return Boolean
*/
    
    public static Boolean isQuoteAccepted(Map<Id, List<cscfga__Product_Configuration__c>> productConfigByBasketMap, cscfga__Product_Basket__c prodbasket, Map<Id,cscfga__Product_Basket__c> newBasketsMap, Map<Id, cscfga__Product_Basket__c> oldBasketsMap){
        Boolean isQuoteAccepted = false;
        if(!productConfigByBasketMap.IsEmpty() && prodbasket.Id != null && productConfigByBasketMap.containsKey(prodbasket.Id)){
            for(cscfga__Product_Configuration__c pc :productConfigByBasketMap.get(prodbasket.Id)){
                String newAccepted = newBasketsMap.get(pc.cscfga__Product_Basket__c).Quote_Status__c;
                String oldAccepted = oldBasketsMap==null? newAccepted: oldBasketsMap.get(pc.cscfga__Product_Basket__c).Quote_Status__c;
                if(newAccepted == 'Accepted' && oldAccepted == 'Approved'){
                    isQuoteAccepted = true;
                    break;
                }
            }
        }
        return isQuoteAccepted;
    }
/**
* @author Telstra Accenture
* @description Method to check if basket is accepted for ISO accounts
* @param Id and Map of product basket 
* @param List of all product confifurations added in the basket
* @return Boolean
*/
    
    public static Boolean isQuoteAcceptedISO(Map<Id, List<cscfga__Product_Configuration__c>> productConfigByBasketMap, cscfga__Product_Basket__c prodbasket, Map<Id,cscfga__Product_Basket__c> newBasketsMap, Map<Id, cscfga__Product_Basket__c> oldBasketsMap){
        Boolean isQuoteAccepted = false;
        if(!productConfigByBasketMap.IsEmpty() && prodbasket.Id != null && productConfigByBasketMap.containsKey(prodbasket.Id)){
            for(cscfga__Product_Configuration__c pc :productConfigByBasketMap.get(prodbasket.Id)){
                String newAccepted = newBasketsMap.get(pc.cscfga__Product_Basket__c).Quote_Status__c;
                String oldAccepted = oldBasketsMap==null? newAccepted: oldBasketsMap.get(pc.cscfga__Product_Basket__c).Quote_Status__c;
                if(newAccepted == 'Accepted' && (oldAccepted == 'Approved' || oldAccepted == 'Accepted')){
                    isQuoteAccepted = true;
                    break;
                }
            }
        }
        return isQuoteAccepted;
    }
/**
* @author Telstra Accenture
* @description Method to check if pricing is required. Logic for pricing approval starts here
* @param Id of product basket 
* @param List of all product confifurations added in the basket
* @return Boolean
*/
    
    public static Boolean isPendingPricingRequired(Map<Id, List<cscfga__Product_Configuration__c>> productConfigByBasketMap, cscfga__Product_Basket__c prodbasket){
        for(cscfga__Product_Configuration__c pc :productConfigByBasketMap.get(prodbasket.Id)){
            /** Return true if price impacted attribute value has been changed*/
            if(pc.CountOfAttributeValueChange__c > 0){
                return true;
            }
        }
        return false;
    }
}