public with sharing class AllServiceLineItemTriggerHandler extends BaseTriggerHandler{
	
	/**
	 * Before insert trigger handler
	 */
    public override void beforeInsert(List<SObject> newServiceLines){
        accountUpdateTriggerCode((List<csord__Service_Line_Item__c>)newServiceLines, true);
        updateTotalPriceforETCandMISC((list<csord__Service_Line_Item__c>) newServiceLines);
        updateDisplayLineItemOnInvoice((list<csord__Service_Line_Item__c>) newServiceLines);
    }

	/**
	 * After insert trigger handler
	 */
    public override void afterInsert(List<SObject> newServiceLines, Map<Id, SObject> newServiceLinesMap){
	
	}

	/**
	 * Before update trigger handler
	 */	
    public override void beforeUpdate(List<SObject> newServiceLines, Map<Id, SObject> newServiceLinesMap, Map<Id, SObject>  oldServiceLinesMap){
		  
        if(!TriggerContext.IsMethodAlreadyExecuted('updatingcurrencyofserviceslineitems')){updatingcurrencyofserviceslineitems((list<csord__Service_Line_Item__c>) newServiceLines);}
        if(!TriggerContext.IsMethodAlreadyExecuted('updatingChargFrequency')){updatingChargFrequency((list<csord__Service_Line_Item__c>) newServiceLines);}
        if(!TriggerContext.IsMethodAlreadyExecuted('updateTotalPriceforETCandMISC')){updateTotalPriceforETCandMISC((list<csord__Service_Line_Item__c>) newServiceLines);}
        if(!TriggerContext.IsMethodAlreadyExecuted('billProfileUPdate.uPdateBillProfileServiceLineItem')){billProfileUPdate.uPdateBillProfileServiceLineItem((list<csord__Service_Line_Item__c>) newServiceLines);}
        if(!TriggerContext.IsMethodAlreadyExecuted('updateETCinCaseFromSLI')){updateETCinCaseFromSLI((list<csord__Service_Line_Item__c>) newServiceLines);}
    }

    /**
	 * Update Total Price in the SLI for ETC and MISC
	 */    
    @testvisible
    private void updateTotalPriceforETCandMISC(list<csord__Service_Line_Item__c> newServiceLines){
        
        for(csord__Service_Line_Item__c servLine:newServiceLines){
            if(servLine.Is_Miscellaneous_Credit_Flag__c || servLine.Is_ETC_Line_Item__c){
                servLine.csord__Total_Price__c=servLine.MISC_Charge_Amount__c;
            }
        }
    }

    /**
	 * Defect fix for #12862 - Allu
	 */
    @testvisible
    private void updatingChargFrequency(List<csord__Service_Line_Item__c> newServiceLines){
		try{		
			Set<Id> srvcIdList = new Set<Id>();
			
			for(csord__Service_Line_Item__c sliList :newServiceLines){
				if(sliList.csord__Service__c != null){
					srvcIdList.add(sliList.csord__Service__c);
				}
			}
					
			if(srvcIdList.Size() > 0){
				Map<Id, csord__Service__c > srvcListMap = new Map<Id, csord__Service__c >([Select Id,csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.cscfga__Default_Frequency__c from csord__Service__c where Id IN :srvcIdList]);

				for(csord__Service_Line_Item__c sliList :newServiceLines){
					if(sliList.Bill_Profile__c != null && sliList.Frequency_flag__c == False && srvcListMap.get(sliList.csord__Service__c) != null){             
						sliList.Charging_Frequency__c = srvcListMap.get(sliList.csord__Service__c).csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.cscfga__Default_Frequency__c;
						sliList.Frequency__c = srvcListMap.get(sliList.csord__Service__c).csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.cscfga__Default_Frequency__c;
						sliList.Frequency_flag__c = True;
					}           
				}
			}
    } catch(exception e){
      ErrorHandlerException.ExecutingClassName='AllServiceLineItemTriggerHandler:updatingChargFrequency';
      ErrorHandlerException.objectList=newServiceLines;
      ErrorHandlerException.sendException(e);
      System.debug('Error: "AllServiceLineItemTriggerHandler; updatingChargFrequency" method update failure - ' +e);
      }    
    }
    
    /**
     * Following method is added to update Service:ineItems' CurrencyISOCode from Basket as part of MultiCurrency
     * Author: Anuradha
	 * Defect fix for #12903
     */
	@testvisible
	Private void updatingcurrencyofserviceslineitems(list<csord__Service_Line_Item__c> newServiceLines){
		try{
			List<Id> slpbids = new List<Id>();

			for(csord__Service_Line_Item__c serviceLine :newServiceLines){    
				if(serviceLine.Product_Basket__c != null)
					slpbids.add(serviceLine.Product_Basket__c);
			}

			if(slpbids.Size() > 0){
				Map<Id, cscfga__Product_Basket__c> pbMap = new Map<Id, cscfga__Product_Basket__c>([select Id, CurrencyISOCode, Exchange_Rate__c from cscfga__Product_Basket__c where Id in: slpbids]);
				
				for(csord__Service_Line_Item__c serviceLine :newServiceLines){
					 if(pbMap.size() > 0 && pbMap.containskey(serviceLine.Product_Basket__c) && (serviceLine.P2O_SFDC_id__c == null || serviceLine.P2O_SFDC_id__c == '')){
						serviceLine.CurrencyISOCode = pbMap.get(serviceLine.Product_Basket__c).CurrencyISOCode;
					}
				}
			}
    } catch(exception e){
      
      ErrorHandlerException.ExecutingClassName='AllServiceLineItemTriggerHandler:updatingcurrencyofserviceslineitems';
      ErrorHandlerException.objectList=newServiceLines;
      ErrorHandlerException.sendException(e);
      System.debug('Error: "AllServiceLineItemTriggerHandler; updatingcurrencyofserviceslineitems" method update failure - ' +e);
      }    
  }

    /**
	 * Update ETC in Case from Service Line Item
	 */	
	@testvisible
	private void updateETCinCaseFromSLI(List<csord__Service_Line_Item__c> newServiceLines){
		try{
			Set<Id> serviceId = new Set<Id>();
			Map<Id, Decimal> serviceETCCaseMap = new Map<Id, Decimal>();
            
			for(csord__Service_Line_Item__c sli :newServiceLines){
                if(sli.csord__service__c != null && !serviceId.contains(sli.csord__service__c) && sli.Is_ETC_Line_Item__c){
                    serviceId.add(sli.csord__service__c);
                    serviceETCCaseMap.put(sli.csord__service__c,sli.MISC_Charge_Amount__c);
				}

				if(sli.Is_ETC_Line_Item__c){
					sli.Frequency__c = 'One-Off';
				}
			}

			if(serviceId.Size() > 0){
				List<case> ETCCaseList = [select id,CS_Service__c,ETC__c from case where subject='Request to calculate ETC' and CS_Service__c in:serviceId and status != 'Closed'];
				
                for(case c :ETCCaseList){
					if(c.CS_Service__c!=null && serviceETCCaseMap.containsKey(c.CS_Service__c)){
						c.ETC__c = serviceETCCaseMap.get(c.CS_Service__c);
					}
				}
				
				if(ETCCaseList.Size() > 0){update ETCCaseList;}                
			}
    } catch(exception e){
      
      ErrorHandlerException.ExecutingClassName='AllServiceLineItemTriggerHandler:updateETCinCaseFromSLI';
      ErrorHandlerException.objectList=newServiceLines;
      ErrorHandlerException.sendException(e);
      System.debug('Error: "AllServiceLineItemTriggerHandler; updateETCinCaseFromSLI" method update failure - ' +e);
      }
  }


	/**
	 * Code moved from the AccountUpdate trigger which needs to be refactored into a method with correct name and purpose
     * Original Description: Trigger to update Account Field on ServiceLine Item used in Bill Profile for lookup filter
     */
    @testvisible
    private void accountUpdateTriggerCode(List<csord__Service_Line_Item__c> newServiceLines, Boolean isInsert){
		try{
			Map<ID, csord__Service__c> parentServices = new Map<ID, csord__Service__c>();
			Set<Id> mrcServiceIds = new Set<Id>();
			List<Id> listIds = new List<Id>();
			List<csord__Service_Line_Item__c> replacedmrc = new List<csord__Service_Line_Item__c>();
			
			for (csord__Service_Line_Item__c childObj : newServiceLines) {
				listIds.add(childObj.csord__Service__c);
			}
			
			List<csord__Service__c> parentServiceList = [Select Id,Product_Configuration_Type__c,ROC_Line_Item_Status__c, AccountId__c, Bill_ProfileId__c, csordtelcoa__Replaced_Service__c, Cost_Centre_Id__c, U2CMasterCode__c from csord__Service__c where Id IN: listIds];
			System.debug('ParentServiceList: ' + parentServiceList);
			
			for(csord__Service__c serviceList : parentServiceList){
				parentServices.put(serviceList.Id, serviceList);
				for(csord__Service_Line_Item__c sli : newServiceLines) {
					if(sli.csord__Is_Recurring__c == true && sli.Is_Miscellaneous_Credit_Flag__c == false && serviceList.csordtelcoa__Replaced_Service__c != null){
						mrcServiceIds.add(serviceList.csordtelcoa__Replaced_Service__c);
					}
				}  
			} 
			
			if(!mrcServiceIds.isEmpty()){
				replacedmrc = [select Id, Charge_ID__c from csord__Service_Line_Item__c where csord__Is_Recurring__c = true and Is_Miscellaneous_Credit_Flag__c =false and csord__Service__c in : mrcServiceIds];
			}  

			List<csord__Service_Line_Item__c> servLineItemList = [select Id, csord__Service__c , csord__Is_Recurring__c, Is_Miscellaneous_Credit_Flag__c, NRC_Credit_Bill_Text__c, RC_Credit_Bill_Text__c from csord__Service_Line_Item__c where csord__Service__c = :parentServices.keyset() and Is_Miscellaneous_Credit_Flag__c = false];
			Map<Id,MiscellaneousCharge> mapMiscellaneousCharge = new Map<Id,MiscellaneousCharge>();
			MiscellaneousCharge miscObj = null;
			for(Id id : parentServices.keyset()){
				miscObj = new MiscellaneousCharge();
				for(csord__Service_Line_Item__c servLineItemObj : servLineItemList){
					if(parentServices != null && parentServices.containskey(servLineItemObj.csord__Service__c) && !servLineItemObj.csord__Is_Recurring__c){
						miscObj.NRCCreditBillText = servLineItemObj.NRC_Credit_Bill_Text__c;
					}
					else if(parentServices != null && parentServices.containskey(servLineItemObj.csord__Service__c) && servLineItemObj.csord__Is_Recurring__c){
						miscObj.RCCreditBillText=servLineItemObj.RC_Credit_Bill_Text__c;
					}
				}
				mapMiscellaneousCharge.put(Id,MiscObj);
			}   
			
			for(csord__Service_Line_Item__c lineItem : newServiceLines){
				for(csord__Service_Line_Item__c lineItemReplacedMRC : replacedmrc){
					if(lineItem.csord__Is_Recurring__c == true && lineItem.Is_Miscellaneous_Credit_Flag__c == false && lineItemReplacedMRC.Id == lineItem.Id){
						lineItem.Charge_ID__c = lineItemReplacedMRC.Charge_ID__c;
					}
				}

				if(lineItem.Account_ID__c == null || lineItem.Bill_Profile__c == null || lineItem.Cost_Centre__c==null){
					if(parentServices.get(lineItem.csord__Service__c).AccountId__c != null && lineItem.Account_ID__c == null){
						lineItem.Account_ID__c = parentServices.get(lineItem.csord__Service__c).AccountId__c;
					}
					if(parentServices.get(lineItem.csord__Service__c).Bill_ProfileId__c != null && lineItem.Bill_Profile__c == null){
						lineItem.Bill_Profile__c= parentServices.get(lineItem.csord__Service__c).Bill_ProfileId__c;
					}
					if(parentServices.get(lineItem.csord__Service__c).Cost_Centre_Id__c != null && lineItem.Cost_Centre__c == null){
						lineItem.Cost_Centre__c= parentServices.get(lineItem.csord__Service__c).Cost_Centre_Id__c;
					}
				}
				if(parentServices.containskey(lineItem.csord__Service__c) && parentServices.get(lineItem.csord__Service__c).Product_Configuration_Type__c=='Terminate' && lineItem.csord__Is_Recurring__c == true){
					lineItem.Charge_Action__c='Terminate';
				}

				if(lineItem.recordtypeid == RecordTypeUtil.getRecordTypeIdByName(csord__Service_Line_Item__c.SObjectType, 'New Miscellaneous Service')){
					lineItem.Is_Miscellaneous_Credit_Flag__c = true;
				}
				else if(lineItem.recordtypeid == RecordTypeUtil.getRecordTypeIdByName(csord__Service_Line_Item__c.SObjectType, 'New ETC Line Item')){
					lineItem.Is_ETC_Line_Item__c=true;
				}

				if(lineItem.Is_Miscellaneous_Credit_Flag__c && lineItem.Transaction_Status__c == null){
					lineItem.Frequency__c = lineItem.Type_of_Charge__c == 'RCR - Recurring credit' ? 'Monthly' : 'One-Off';
					lineItem.Charge_Action__c = 'CREATE';
					lineItem.Zero_MISC_Charge_Flag__c = true;
					lineItem.Display_line_item_on_Invoice__c = true;
					lineItem.NRC_Credit_Bill_Text__c = MapmiscellaneousCharge.containskey(lineItem.csord__Service__c) ? mapMiscellaneousCharge.get(lineItem.csord__Service__c).NRCCreditBillText : null;
					lineItem.RC_Credit_Bill_Text__c = MapmiscellaneousCharge.containskey(lineItem.csord__Service__c) ? mapMiscellaneousCharge.get(lineItem.csord__Service__c).RCCreditBillText : null;
					lineItem.Bill_Profile__c = lineItem.Bill_Profile__c == null ? parentServices.get(lineItem.csord__Service__c).Bill_ProfileId__c : lineItem.Bill_Profile__c;
					lineItem.Cost_Centre__c = lineItem.Cost_Centre__c == null ? parentServices.get(lineItem.csord__Service__c).Cost_Centre_Id__c : lineItem.Cost_Centre__c;
					lineItem.U2C_Master_Code__c = parentServices.get(lineItem.csord__Service__c).U2CMasterCode__c;

					if(lineItem.Type_of_Charge__c == 'RCR - Recurring credit'){
						if(lineItem.Billing_MISC_Commencement_Date__c!= null){
							lineItem.Installment_Start_Date__c = lineItem.Billing_MISC_Commencement_Date__c.toStartOfMonth();
						}
						if(lineItem.Billing_End_Date__c != null){
							Integer numberOfDays = Date.daysInMonth(lineItem.Billing_End_Date__c.year(), lineItem.Billing_End_Date__c.month());
							Date lastDayOfMonth = Date.newInstance(lineItem.Billing_End_Date__c.year(), lineItem.Billing_End_Date__c.month(), numberOfDays);
							lineItem.Installment_End_Date__c=lastDayOfMonth;
						}
					}
					else{
						lineItem.Installment_Start_Date__c=null;
						lineItem.Installment_End_Date__c=null;
					}           
					
					if(lineItem.MISCService__c != null){
						lineItem.csord__Service__c = Id.valueof(lineItem.MISCService__c);
					}
				}

				//ETC Charge:IP006-Suparna 
				if(lineItem.Is_ETC_Line_Item__c){       
					if(lineItem.Billing_Commencement_Date__c != null){
						lineItem.Installment_Start_Date__c = lineItem.Billing_Commencement_Date__c.toStartOfMonth();
					}
					if(lineItem.Billing_ETC_End_Date__c != null){
						Integer numberOfDays = Date.daysInMonth(lineItem.Billing_ETC_End_Date__c.year(), lineItem.Billing_ETC_End_Date__c.month());
						Date lastDayOfMonth = Date.newInstance(lineItem.Billing_ETC_End_Date__c.year(), lineItem.Billing_ETC_End_Date__c.month(), numberOfDays);
						lineItem.Installment_End_Date__c=lastDayOfMonth;
					}
					lineItem.Bill_Profile__c = parentServices.get(lineItem.csord__Service__c).Bill_ProfileId__c;
					lineItem.Cost_Centre__c = parentServices.get(lineItem.csord__Service__c).Cost_Centre_Id__c;
					lineItem.U2C_Master_Code__c = parentServices.get(lineItem.csord__Service__c).U2CMasterCode__c;
								
					if(lineItem.MISCService__c != null){
						lineItem.csord__Service__c = Id.valueof(lineItem.MISCService__c);
					}
					
					lineItem.name=isInsert?lineItem.name+' '+'ETC':lineItem.name;
					lineItem.recordtypeId = RecordTypeUtil.getRecordTypeIdByName(csord__Service_Line_Item__c.SObjectType, 'New ETC Line Item');
				}
			}
    } catch(exception e){
      
      ErrorHandlerException.ExecutingClassName='AllServiceLineItemTriggerHandler:accountUpdateTriggerCode';
      ErrorHandlerException.objectList=newServiceLines;
      ErrorHandlerException.sendException(e);
      System.debug('Error: "AllServiceLineItemTriggerHandler; accountUpdateTriggerCode" method update failure - ' +e);
      }      
    }

	class MiscellaneousCharge{
        public Id serviceId;
        public String NRCCreditBillText;
        public String RCCreditBillText;
    }
    
	/**
     * Following method is added to update Display Line Item On Invoice field.
     * CR: IR265GMNS
     * Defect fix for #15529
     * Developer: KD
     * Date:19-07-2017
     */
	@testvisible
	private void updateDisplayLineItemOnInvoice(list<csord__Service_Line_Item__c> newServiceLines){
		try{
            for(csord__Service_Line_Item__c serviceLine :newServiceLines){

				if(serviceLine.Display_Line_Item_On_Invoice_Text__c != null){
					serviceLine.Display_line_item_on_Invoice__c = Boolean.valueOF(serviceLine.Display_Line_Item_On_Invoice_Text__c);
				}   
            }
        } catch(Exception e){
            
            ErrorHandlerException.ExecutingClassName='AllServiceLineItemTriggerHandler:updateDisplayLineItemOnInvoice';
            ErrorHandlerException.objectList=newServiceLines;
            ErrorHandlerException.sendException(e);
            System.debug('Error: "AllServiceLineItemTriggerHandler; updateDisplayLineItemOnInvoice" method update failure - ' +e);
        }
     }
}