@isTest
public class AllStepTriggerHandlerTest {
    
    
   static testmethod void testUpdateStepTimes(){
   
    
    P2A_TestFactoryCls.SetupTestData();
        
        csord__Order_Request__c ordReq = new csord__Order_Request__c(name='Test Ord Req', csord__Module_Version__c='1.0', 
        csord__Module_Name__c = 'Test Module');
        insert ordReq;
        
        csord__Order__c order = new csord__Order__c(name='Test Order2', 
        csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
        csord__Order_Request__c = ordReq.Id,
        Customer_Required_Date__c = Date.newInstance(2017,01,01),
        RAG_Status_Red__c = true,
        RAG_Order_Status_RED__c = true);
        insert order;
        
        system.assertEquals(order.csord__Order_Request__c,ordReq.Id);
        
        csord__Subscription__c sub = new csord__Subscription__c(Name = 'Service from to on - SN-000004106', csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
        csord__Order_Request__c = ordReq.Id,csord__Order__c = order.Id);
        insert sub;
        
        CSPOFA__Orchestration_Process_Template__c thisProcTempl = [select Id, Name 
                                                                  from CSPOFA__Orchestration_Process_Template__c
                                                                  where name = 'Test Proc Templ'].get(0);
        
        CSPOFA__Orchestration_Step_Template__c thisStepTempl = [select Id, Name, CSPOFA__Orchestration_Process_Template__c,
                                                                Estimated_Time_To_Complete__c, CSPOFA__OLA_Unit__c, 
                                                                CSPOFA__Milestone_Label__c from CSPOFA__Orchestration_Step_Template__c
                                                                where name = 'Test Step Template'].get(0);
        
        CSPOFA__Orchestration_Process__c thisProc = [select Id, name, CSPOFA__Orchestration_Process_Template__c, order__c, order__r.Customer_Required_Date__c,
                                                      Estimated_Time_To_Complete__c,csordtelcoa__Service__c from CSPOFA__Orchestration_Process__c
                                                      where name = 'Test Process'].get(0);
        thisProc.order__c = order.id;
        update thisProc;
        
        Id ServiceId = thisProc.csordtelcoa__Service__c;
        
        csord__Service__c servs = [select id, Name from csord__Service__c where id = :ServiceId].get(0);
        servs.RAG_Status_Amber__c = true;
        update servs;
         
        cspofa__Orchestration_Step__c stepToUpdate = [SELECT Id, CSPOFA__Orchestration_Process__r.Id FROM 
                                                        CSPOFA__Orchestration_Step__c 
                                                        WHERE CSPOFA__Orchestration_Process__r.Id = :thisProc.Id].get(0);
        stepToUpdate.Start_Date_Time__c = system.now();
        stepToUpdate.End_Date_Time__c = system.now().addDays(30);
        stepToUpdate.CSPOFA__Status__c = 'Inprogress';
        update stepToUpdate;

        DateTime dt = DateTime.newInstance(2016,11,1,12,0,0);
        Id processId = stepToUpdate.CSPOFA__Orchestration_Process__r.Id;
        
        List<cspofa__Orchestration_Step__c> orderSteps1 = [SELECT 
                                                              Id, 
                                                              CSPOFA__Orchestration_Process__r.Id,
                                                              CSPOFA__Orchestration_Step_Template__r.Name,
                                                              CSPOFA__Orchestration_Step_Template__r.Estimated_Time_To_Complete__c,
                                                              CSPOFA__Orchestration_Step_Template__r.CSPOFA__OLA_Unit__c,
                                                              CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c,
                                                              Start_Date_Time__c,
                                                              End_Date_Time__c,
                                                              Estimated_Time_To_Complete__c
                                                          FROM 
                                                              cspofa__Orchestration_Step__c 
                                                          WHERE 
                                                              CSPOFA__Orchestration_Process__r.Id = :processId];
        
        List<cspofa__Orchestration_Step__c> orderSteps = [SELECT 
                                                              Id, 
                                                              CSPOFA__Orchestration_Process__r.Id,
                                                              CSPOFA__Orchestration_Step_Template__r.Name,
                                                              CSPOFA__Orchestration_Step_Template__r.Estimated_Time_To_Complete__c,
                                                              CSPOFA__Orchestration_Step_Template__r.CSPOFA__OLA_Unit__c,
                                                              CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c,
                                                              Start_Date_Time__c,
                                                              End_Date_Time__c,
                                                              Estimated_Time_To_Complete__c
                                                          FROM 
                                                              cspofa__Orchestration_Step__c 
                                                          WHERE 
                                                              CSPOFA__Orchestration_Process__r.Id = :processId];
        
        system.assert(orderSteps.size() > 0);
        
        Map<Id,cspofa__Orchestration_Step__c> oldStepsMap = new Map<Id,cspofa__Orchestration_Step__c>();
        Map<Id, cspofa__Orchestration_Step__c> step2ProcessMap = new Map<Id, cspofa__Orchestration_Step__c>();
        
         for (CSPOFA__Orchestration_Step__c step1 : orderSteps){
             
             step2ProcessMap.put(step1.Id, step1);
             oldStepsMap.put(step1.id,step1);
        }
        
        orderSteps = ProcessStepTimeManager.SortSteps(orderSteps, false);
        Test.startTest();
        for (cspofa__Orchestration_Step__c step : orderSteps)
        {
            if (ProcessStepTimeManager.IncludeInPathView(step))
            {
                System.Debug('Step name: ' + 
                             step.CSPOFA__Orchestration_Step_Template__r.Name.rightPad(40) + 
                             ' starts at ' + 
                             step.Start_Date_Time__c.day() + '-' + step.Start_Date_Time__c.month()
                            );
            }
        }
        
        csord__Order__c orders = [select id,RAG_Status__c from csord__Order__c where id = :order.id];
        orderSteps[0].RAG_Status__c = orders.RAG_Status__c;
        update orderSteps[0];
        
        csord__Service__c servs1 = [select id,Name,RAG_Status_Amber__c,RAG_Status__c from csord__Service__c where id = :servs.id];
        orderSteps1[0].RAG_Status__c = servs1.RAG_Status__c;
        update orderSteps1[0];
        
        List<CSPOFA__Orchestration_Process__c>processList = new List<CSPOFA__Orchestration_Process__c>{thisProc};    
        
        Test.stopTest();

        system.assertEquals(orderSteps[0].RAG_Status__c, 'Red');
        system.assertEquals(orderSteps1[0].RAG_Status__c, 'Amber');
        System.assert(ProcessStepTimeManager.IncludeInPathView(orderSteps.get(0)) != null);
        
     }
                  
  }