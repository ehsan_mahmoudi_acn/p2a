public class MasterServiceBatch implements Database.Batchable<Id>, Database.Stateful {
    //For @Testvisible , modified by Anuradha
	@Testvisible
    private Integer m_counter;
	@Testvisible
    private List<Id> m_basketIds;
    
    public MasterServiceBatch (Set<Id> basketIds) {
        this.m_counter = 0;
        this.m_basketIds = new List<Id>(basketIds);
    }
    
    public Iterable<Id> start(Database.BatchableContext ctx) {
        return this.m_basketIds;
    }
    
    public void execute(Database.BatchableContext BC, List<Id> basketIds) {
        m_counter++;
        //MasterServiceLogicOld.process((new Set<Id>(basketIds)));
        MasterServiceLogic.process(basketIds[0]);
    }
    
    public void finish(Database.BatchableContext BC) {
        system.debug('MasterServiceJob - finished, num fo batches: ' + m_counter);
    }
    
}