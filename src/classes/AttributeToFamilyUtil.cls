/*
*   Utility class which contains logic for automatic loading of attribute values onto salesforce objects
*/
public with sharing class AttributeToFamilyUtil{
    private Map<String, AttributeToCaseMapping> allMappings  = new Map<String, AttributeToCaseMapping>();
    private Map<String, Map<String, String>> familyMappings = new Map<String, Map<String, String>>();

    public AttributeToFamilyUtil(){
    	createAllCaseAttributeMappings();
    }

    /*
    *   Loads a mapping into the library of mappings as well as separating them into a map for each family
    *   @param mapping - a new mapping which is to be added into the library of mappings
    */
    public void addMapping(AttributeToCaseMapping mapping){
        allMappings.put(mapping.attributeName, mapping);

        for(String familyKey : mapping.familyToFieldMap.keyset()){
            if(!familyMappings.containsKey(familyKey)){
                familyMappings.put(familyKey, new Map<String, String>());
            }
            familyMappings.get(familyKey).put(mapping.familyToFieldMap.get(familyKey), mapping.attributeName);
        }   
    }

    /*
    *   Loads a list of mappings
    *   @param list of mapping
    */
    public void addMappings(List<AttributeToCaseMapping> mappings){
        for(AttributeToCaseMapping mapping : mappings){
            addMapping(mapping);
        }
    }

    /*
    * Create the mapping from CloudSense attribute to Case object field for each product family
    */
    private void createAllCaseAttributeMappings(){
        List<AttributeToCaseMapping> allCaseAttributeMappings = new List<AttributeToCaseMapping>();

        // Get all custom settings 
        // sort them by attribute name
        // Add each family and case field to the map

        AttributeToCaseMapping finalModel = new AttributeToCaseMapping('Final_Model__c');
        finalModel.familyToFieldMap.put('GCPE', 'Model_Number__c');
        addMapping(finalModel);
        
        AttributeToCaseMapping quantity = new AttributeToCaseMapping('Quantity__c');
        finalModel.familyToFieldMap.put('GCPE', 'Quantity__c');
        addMapping(quantity);

        AttributeToCaseMapping financialModel = new AttributeToCaseMapping('Financial_Model_radio__c');
        finalModel.familyToFieldMap.put('GCPE', 'Rental_Purchase__c');
        addMapping(financialModel);

        AttributeToCaseMapping finalMainGrade = new AttributeToCaseMapping('Final_Main_grade__c');
        finalModel.familyToFieldMap.put('GCPE', 'Maintanence_Level__c');
        addMapping(finalMainGrade);

        AttributeToCaseMapping completeAddress = new AttributeToCaseMapping('CompleteAddress__c');
        finalModel.familyToFieldMap.put('GCPE', 'Site_Address__c'); 
        addMapping(completeAddress);             
    }

    /*
    *   Gets all the CloudSense attribute names for mappings loaded into this class
    */
    public Set<String> getAllAttributeNames(){
        return allMappings.keyset();
    }

    /*
    *   Get all the attribute field mappings for a given family
    */
    public Map<String, String> getFamilyMappings(String familyKey){
        return familyMappings.get(familyKey);
    }

    /*
    *   Get the value of an attribute checking for nulls
    */
    public String getAttributeValue(Map<String, cscfga__Attribute__c> attributes, String attributeName){
        if(attributes.containsKey(attributeName) && attributes.get(attributeName).cscfga__Value__c != null){
            return attributes.get(attributeName).cscfga__Value__c;
        }
        return '';
    }

    /*
    *   Set the mapped attribute fields on the given case object
    *
    *   @param theCase - The Case object to be updated
    *   @param familyKey - The key which is used to reference the set of mappings (can be any string)
    *   @param attributes - The loaded attributes from which to retrieve the mapped field values
    */ 
    public Case setCaseFields(Case theCase, String familyKey, Map<String, cscfga__Attribute__c> attributes){
        if(getFamilyMappings(familyKey) != null){
            Map<String, String> familyFieldMap = getFamilyMappings(familyKey);
            
            for(String key : familyFieldMap.keySet()){
                theCase.put(key, getAttributeValue(attributes, familyFieldMap.get(key)));
            }
        }
        return theCase;
    }

        /*
    *   Contains the CloudSense attribute name and the mapping of that attribute to the corresponding case field for each 
    *   product family.
    */
    class AttributeToCaseMapping{
        String attributeName;
        Map<String, String> familyToFieldMap = new Map<String, String>();

        public AttributeToCaseMapping(String name){
            attributeName = name;
        }
    }
}