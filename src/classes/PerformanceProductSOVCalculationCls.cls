public without sharing class PerformanceProductSOVCalculationCls{

public static Map<String,Decimal> getMapOnSSCatProductSov(Set<Id> OpportunityID ){
//new change    
	system.debug('===heap size1==='+Limits.getHeapSize());
    Map<String,String> MapOppType_Role=new Map<String,String>();
    Set<String> CategorySet=new Set<String>();
    Map<String,Decimal> MapCat_ProductSov=new Map<String,Decimal>();
    Map<String,Decimal> MapCat_OliProductSov=new Map<String,Decimal>();
    Map<String,Decimal> MapCat_OppProdProductSov=new Map<String,Decimal>();
    system.debug('===heap size2==='+Limits.getHeapSize());
    system.debug('==heap size limit===='+Limits.getLimitHeapSize());
    
   /* for(Product_Type_with_Role__c prodRoleObj:ListRoleMappingObj){
        
        MapOppType_Role.put(prodRoleObj.Product_Type__c,prodRoleObj.Role__c);
       CategorySet.add(prodRoleObj.Role__c); 
    }*/
    
    MapOppType_Role=getRoleMapping();
    CategorySet.addall(MapOppType_Role.values());
    List<Opp_Product_Type__c> oppProductTypeLst=[Select Product_Type_Name__c,Opportunity__r.QuoteStatus__c,Product_SOV__c,Sales_Specialist_Category__c,Opportunity__c from Opp_Product_Type__c where Opportunity__c IN:OpportunityID];
    for(Opp_Product_Type__c OppProdObj:oppProductTypeLst){
        OppProdObj.Sales_Specialist_Category__c=MapOppType_Role.containsKey(OppProdObj.Product_Type_Name__c)?MapOppType_Role.get(OppProdObj.Product_Type_Name__c):null;
        
        
    }
    system.debug('===heap size3==='+Limits.getHeapSize());
    system.debug('==heap size limit===='+Limits.getLimitHeapSize());
    
  /*  Decimal productAmount=0;
   for(String cat:CategorySet) {
    productAmount=0;
  
    for(Opp_Product_Type__c OppProdObj:oppProductTypeLst){
     if(OppProdObj.Product_SOV__c==null){OppProdObj.Product_SOV__c=0;}
        if(OppProdObj.Sales_Specialist_Category__c!=null && OppProdObj.Sales_Specialist_Category__c.equalsignorecase(cat) && ((OppProdObj.Product_Type_Name__c == 'Conferencing and Collaboration (UC)' || OppProdObj.Product_Type_Name__c == 'GVoIP'|| OppProdObj.Product_Type_Name__c == 'Global Cloud Collaboration' || OppProdObj.Product_Type_Name__c == 'Global Hosting Services - DRaaS' || OppProdObj.Product_Type_Name__c == 'Global Hosting Services - IaaS' || OppProdObj.Product_Type_Name__c == 'Global Hosting Services - SaaS' || OppProdObj.Product_Type_Name__c == 'IP Telephony' || OppProdObj.Product_Type_Name__c == 'Virtual Contact Centre'|| OppProdObj.Product_Type_Name__c == 'Global Hosting Services - SaaS' || OppProdObj.Product_Type_Name__c == 'Contact Centre Solutions (UC)' || OppProdObj.Product_Type_Name__c == 'Cloud Collaboration' || OppProdObj.Product_Type_Name__c == 'PEN' || OppProdObj.Product_Type_Name__c == 'Global Hosting Services – Prof Svc'|| OppProdObj.Product_Type_Name__c =='Global Hosting Services-On-Premise Cloud') || OppProdObj.Opportunity__r.QuoteStatus__c!='Order') && OppProdObj.Product_SOV__c!=null){
         productAmount=productAmount+OppProdObj.Product_SOV__c;
    }
    
    
   }
    system.debug('===heap size4==='+Limits.getHeapSize());
    system.debug('==heap size limit===='+Limits.getLimitHeapSize());
   
   if(MapCat_OppProdProductSov.containskey(cat)){
   MapCat_OppProdProductSov.put(cat,(MapCat_OppProdProductSov.get(cat)+productAmount));
   }else{
    MapCat_OppProdProductSov.put(cat,productAmount);
    
   }
   }*/
   
     system.debug('===getCpuTime() start====='+Limits.getCpuTime());          
   
  
    for(Opp_Product_Type__c OppProdObj:oppProductTypeLst){
     if(OppProdObj.Product_SOV__c==null){OppProdObj.Product_SOV__c=0;}
     if(((OppProdObj.Product_Type_Name__c == 'Conferencing and Collaboration (UC)' || OppProdObj.Product_Type_Name__c == 'GVoIP'|| OppProdObj.Product_Type_Name__c == 'Global Cloud Collaboration' || OppProdObj.Product_Type_Name__c == 'Global Hosting Services - DRaaS' || OppProdObj.Product_Type_Name__c == 'Global Hosting Services - IaaS' || OppProdObj.Product_Type_Name__c == 'Global Hosting Services - SaaS' || OppProdObj.Product_Type_Name__c == 'IP Telephony' || OppProdObj.Product_Type_Name__c == 'Virtual Contact Centre'|| OppProdObj.Product_Type_Name__c == 'Global Hosting Services - SaaS' || OppProdObj.Product_Type_Name__c == 'Contact Centre Solutions (UC)' || OppProdObj.Product_Type_Name__c == 'Cloud Collaboration' || OppProdObj.Product_Type_Name__c == 'PEN' || OppProdObj.Product_Type_Name__c == 'Global Hosting Services – Prof Svc'|| OppProdObj.Product_Type_Name__c =='Global Hosting Services-On-Premise Cloud') || (OppProdObj.Opportunity__r.QuoteStatus__c!='Approved' && OppProdObj.Opportunity__r.QuoteStatus__c!='Accepted'))  && OppProdObj.Product_SOV__c!=null){
        if(OppProdObj.Sales_Specialist_Category__c!=null && MapCat_OppProdProductSov.containskey(OppProdObj.Sales_Specialist_Category__c)){
         Decimal productAmount=MapCat_OppProdProductSov.get(OppProdObj.Sales_Specialist_Category__c)+OppProdObj.Product_SOV__c;
         MapCat_OppProdProductSov.remove(OppProdObj.Sales_Specialist_Category__c);
         MapCat_OppProdProductSov.put(OppProdObj.Sales_Specialist_Category__c,productAmount);
         
    }else {
    	 MapCat_OppProdProductSov.put(OppProdObj.Sales_Specialist_Category__c,OppProdObj.Product_SOV__c);
    	
    }
    
    }
    } 
    system.debug('===heap size4==='+Limits.getHeapSize());
    system.debug('==heap size limit===='+Limits.getLimitHeapSize());
   
  system.debug('=====MapCat_OppProdProductSov===='+MapCat_OppProdProductSov);
    system.debug('===getCpuTime() end====='+Limits.getCpuTime());          
  
     List<OpportunityLineItem> OppltmLst = [select id,Opportunity.QuoteStatus__c, cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Sales_Specialist_Category__c,cscfga__Attribute__r.cscfga__Product_Configuration__r.sov_usage__c,PricebookEntry.product2.sov_usage__c,Reported_Total_SOV__c,Product_id2__c,PricebookEntry.product2.Sales_Specialist_Category__c,PricebookEntry.product2.Usage_Based_Charge_Flag__c,Root_Product_Id__c,opportunityId from OpportunityLineItem where opportunityId IN:OpportunityID];
   
 for(OpportunityLineItem oppLineItem:OppltmLst){
 if(oppLineItem.Root_Product_Id__c=='IPVPN' ){oppLineItem.cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Sales_Specialist_Category__c ='IPVPN';}
if(oppLineItem.Root_Product_Id__c=='GVOIP'){oppLineItem.cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Sales_Specialist_Category__c ='Collaboration';}
if(oppLineItem.Root_Product_Id__c=='VLM' &&( oppLineItem.Product_id2__c=='NIDS' || oppLineItem.Product_id2__c=='GCPE')){oppLineItem.cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Sales_Specialist_Category__c ='GMNS';}
if(oppLineItem.Root_Product_Id__c=='IPVPN' && oppLineItem.Product_id2__c=='GCPE'){oppLineItem.cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Sales_Specialist_Category__c='GMNS';}
if(oppLineItem.Root_Product_Id__c=='GCC'){oppLineItem.cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Sales_Specialist_Category__c='Collaboration';}
 }
// Decimal productSov=0;
  system.debug('===getCpuTime() start====='+Limits.getCpuTime());          
  /*  for(String cat:CategorySet) {
        productSov=0;    
    for(OpportunityLineItem oppLineItem:OppltmLst){
        
        if(oppLineItem.cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Sales_Specialist_Category__c.equalsignorecase(cat) && oppLineItem.cscfga__Attribute__r.cscfga__Product_Configuration__r.sov_usage__c.equalsignorecase('No') && oppLineItem.Opportunity.QuoteStatus__c=='Accepted'){
        
            
            productSov=productSov+oppLineItem.Reported_Total_SOV__c;
            
        
        }
    
    
    
   }
   MapCat_OliProductSov.put(cat,productSov);
}*/
  system.debug('===getCpuTime() end====='+Limits.getCpuTime()); 
  system.debug('===getCpuTime() end====='+Limits.getLimitCpuTime());  
          

    for(OpportunityLineItem oppLineItem:OppltmLst){
       if(oppLineItem.cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Sales_Specialist_Category__c!='' && (oppLineItem.Opportunity.QuoteStatus__c=='Approved' || oppLineItem.Opportunity.QuoteStatus__c=='Accepted')){ 
        if(MapCat_OliProductSov.containsKey(oppLineItem.cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Sales_Specialist_Category__c) && oppLineItem.cscfga__Attribute__r.cscfga__Product_Configuration__r.sov_usage__c.equalsignorecase('No')){
        
            Decimal productSov= MapCat_OliProductSov.get(oppLineItem.cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Sales_Specialist_Category__c);
            MapCat_OliProductSov.remove(oppLineItem.cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Sales_Specialist_Category__c);
            MapCat_OliProductSov.put(oppLineItem.cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Sales_Specialist_Category__c,productSov+oppLineItem.Reported_Total_SOV__c);
         
          }else{
        	MapCat_OliProductSov.put(oppLineItem.cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Sales_Specialist_Category__c,oppLineItem.Reported_Total_SOV__c);
        	
        }
    
       }
    
   }



system.debug('===heap size5==='+Limits.getHeapSize());
system.debug('==heap size limit===='+Limits.getLimitHeapSize());
            
system.debug('=====MapCat_OliProductSov===='+MapCat_OliProductSov);        
for(String SScat:CategorySet){
MapCat_ProductSov.put(SScat,(MapCat_OliProductSov.containskey(SScat)?MapCat_OliProductSov.get(SScat):0)+(MapCat_OppProdProductSov.containskey(SScat)?MapCat_OppProdProductSov.get(SScat):0)); 

} 
system.debug('=====MapCat_ProductSov===='+MapCat_ProductSov);

    system.debug('===heap size6==='+Limits.getHeapSize());
    system.debug('==heap size limit===='+Limits.getLimitHeapSize());

return MapCat_ProductSov;        
}
public static Map<String,String> getRoleMapping(){
Map<String,String> MapOppType_Role=new Map<String,String>();	
List<Product_Type_with_Role__c> ListRoleMappingObj = Product_Type_with_Role__c.getAll().values();
   
    for(Product_Type_with_Role__c prodRoleObj:ListRoleMappingObj){
        
        MapOppType_Role.put(prodRoleObj.Product_Type__c,prodRoleObj.Role__c);
    }

return MapOppType_Role;
}


}