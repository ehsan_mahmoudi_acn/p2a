global class OrderSubmissionValidation {
	
	webservice static Boolean isOrderSubmissionAllowed(String OrderId){
		Boolean SubmissionAllowedFlag = true;
		List<csord__Service__c> lstServ = [select Id, csord__Order__c, csord__Order__r.Is_Order_Submitted__c, 
											Master_Service__c, Path_Instance_ID__c 
											FROM csord__Service__c
											WHERE
												csord__Order__c =:OrderId
												AND Master_Service__c != null
												/*AND csord__Order__r.Is_Order_Submitted__c = false*/
												AND Master_Service__r.Path_Instance_ID__c = null];

		if(!lstServ.isEmpty()){
			SubmissionAllowedFlag = false;
		}
		system.debug('SubmissionAllowedFlag = ' + SubmissionAllowedFlag);
		return SubmissionAllowedFlag;
	}  
}