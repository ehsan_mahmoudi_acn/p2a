global class ParallelOrderBatch 
{
/*implements Database.Batchable<csord__order__c>,Database.AllowsCallouts{ 

    public List<Id>OrderIdList{get;set;}
    public set<Id>MasterOrderSet{get;set;}
    public set<Id>ParallelOrderSet{get;set;}
    public Boolean isChildSubmitible{get;set;}
    public Boolean isBatchComplete{get;set;}
    public id solutionId{get;set;}
    public solutions__c  solnObj{get;set;}
    
    
    
    //contructor
    global ParallelOrderBatch(List<Id>OrderIdList,set<Id>MasterOrderSet,Boolean isChildSubmitible) {
        this.OrderIdList=OrderIdList;
        this.MasterOrderSet=MasterOrderSet;        
        this.isChildSubmitible=isChildSubmitible;
        id orderId=OrderIdList[0];
        isBatchComplete=isChildSubmitible;
        csord__order__c ordertosolution=[select id,Solutions__c from csord__order__c where Id =:orderId];
        solnObj=[select id,batch_count__c from solutions__c where id=:ordertosolution.Solutions__c];
        solnObj.batch_count__c=OrderIdList.size();
        update solnObj;
        solutionId=solnObj.id;      
    }
    
    //start method: will return list of order Sobject
    global List<csord__order__c> start(Database.BatchableContext BC){
        
        return null;//[select id,Solutions__c from csord__order__c where Id IN:MasterOrderSet];
    }
    
    //execute method will process the list of order sobject and submit the order
    global void execute(Database.BatchableContext BC,List<csord__order__c> orderSet){   
        
        Map<Id,csord__service__c>serviceMap=new Map<Id,csord__service__c>([select id from csord__service__c where csord__order__c in:OrderIdList and primary_service_id__c IN ('','NULL')]);  
        List<String>serviceList=new List<String>();
        for(id servId:serviceMap.keyset()){
           serviceList.add(String.valueof(servId));
        }
       
         TriggerGenerateID.SendServicedata(serviceList);//generation if Id's
        
        //disable triggers
        //TriggerFlags.NoSubscriptionTriggers =true;
        //TriggerFlags.NoOrderTriggers =true;
        //TriggerFlags.NoServiceTriggers =true;
         List<csord__order__c>updateOrderList=new List<csord__order__c>();
            for (csord__order__c ord:orderSet){
              try{
                    if(MasterOrderSet.contains(ord.id)){
                       SubmitOrder.submitOrderToFOMFromBatch(ord.id);//submission of Orders
                       ord.Is_Order_Submitted__c = true;
                       ord.Status__c = 'Submitted'; 
                       ord.Clean_Order_Check_Passed__c = true;
                       updateOrderList.add(ord);  
                    }
                }
               catch(Exception e){
                  ErrorHandlerException.ExecutingClassName='ParallelOrderBatch:execute';
                  ErrorHandlerException.objectList= orderSet;
                  ErrorHandlerException.sendException(e);
               } 
            }            
        
        if(updateOrderList.size()>0){update updateOrderList;}
    }
    

    //final method :will be executed when batch is finished
   global void finish(Database.BatchableContext BC){
        if(isBatchComplete){           
           //TriggerFlags.BatchSuccess='Batch Completed SuccessFully';
           //TriggerFlags.BatchFailed='';
           //TriggerFlags.BatchCount=0;//will help in actionPoller in solutionSummary Page
           solnObj=[select id,name,Account_Name__r.name,Opportunity_Name__r.name,batch_count__c from solutions__c where id=:solutionId];
           solnObj.batch_count__c=0;
           update solnObj;
           SolEmailTempCtrl.SendEmail(OrderIdList,solnObj); 
           System.debug('execution finished'); 
           }
    } 
    
    
    private class SubmitOrderBatchException extends Exception {}
    */
}