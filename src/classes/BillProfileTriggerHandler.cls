/*As part of production Ticket# TGCTASK0209037, commented //with sharing */
//public with sharing class BillProfileTriggerHandler extends BaseTriggerHandler{
public class BillProfileTriggerHandler extends BaseTriggerHandler{

    public override void beforeInsert(List<SObject> newBillProfiles){
        updateItemisationDisplayLevel(newBillProfiles,null);
    }

    public override void afterInsert(list<SObject> newBillProfile,map<Id, SObject> newBillProfileMap){

        // Variables decalration
        map<string,string> pacnetEntityMap = new map<string,string>();
        set<id> acctIds= new set<id>();
        list<BillProfile__c> newBillProfiles= new list<BillProfile__c>();
        newBillProfiles = (List<BillProfile__c>) newBillprofile ;
        map<id, BillProfile__c> billingEntityMap = new map<id, BillProfile__c> ();
        string actionItemRecordType ='Request for Bill Profile Approval';

        // Select only the Bill Profiles witih security_bill_for_activation__c == false
        for(BillProfile__c eachBillProfile: newBillProfiles){
            if(eachBillProfile.security_bill_for_activation__c == false){
                billingEntityMap.put(eachBillProfile.Id, eachBillProfile);
                acctIds.add(eachBillProfile.Account__c);
            }
        } 

        //Fetching Account owner ids
        Map<Id, Account> accMap = new Map<Id, Account>([SELECT ownerId FROM Account where id in :acctIds]);
        Set<Id> ownerIds = new Set<Id>();
        for (Account acc: accMap.values()) {
            ownerIds.add(acc.ownerId);
        }

        // Get a map of users and region
        Map<Id, User> ownerRegion = new map<Id, User>([SELECT Name, Region__c, Role_Name__c FROM User WHERE id in :ownerIds]);

        // Add profile after checking with Sean (Sales user)
        // and IsActive = true

        // Get the Bill Profile Status & Priority
        id recortypeId = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType, actionItemRecordType);

        // Set the due date to 2 days after the today date
        date dueDate = System.Today() + 2;

        // Get the pacnet entities (to be able to compare it with the owner regions)
        //list<PACNET_Entities__c> allPacnetEntities = PACNET_Entities__c.getall().values();
        for(PACNET_Entities__c eachEntity :PACNET_Entities__c.getall().values() ){
            pacnetEntityMap.put(eachEntity.PACNET_Entity__c,eachEntity.PACNET_Entity__c);
        }  

        // Create the list of Action Item and the queue map
        list<Action_Item__c> lstActionItems = new list<Action_Item__c>();
        Map<String, QueueSobject> queueMap = QueueUtil.getQueuesMapForObject(Action_Item__c.SObjectType);

        // For each bill profile, create a new action item
        for (BillProfile__c eachBillProfile: billingEntityMap.values()){
            Action_Item__c eachActionItem = new Action_Item__c();
            eachActionItem.RecordTypeId = recortypeId;

            // Assigning account and bill profile Ids to the action item
            if(billingEntityMap.get(eachBillProfile.id).account__c != null){
                eachActionItem.Account__c = billingEntityMap.get(eachBillProfile.id).account__c;
            }

            eachActionItem.Bill_Profile__c = eachBillProfile.id;

            //Assigning static values 
            eachActionItem.Status__c = 'High';
            eachActionItem.Priority__c = 'Assigned';
            eachActionItem.Due_Date__c = dueDate;
            eachActionItem.Subject__c = actionItemRecordType;
            eachActionItem.Feedback__c = '';

            //assignOwnersToRegionalQueue
            // Has record configured in packet entity object
            string pacnetKey = billingEntityMap.get(eachBillProfile.id).Billing_Entity__c!=null ? billingEntityMap.get(eachBillProfile.id).Billing_Entity__c : '';

            Id ownerID = accMap.get(billingEntityMap.get(eachBillProfile.Id).Account__c).OwnerId;

            string region = ownerRegion.get(ownerID).Region__c;

            System.Debug('@@@@ pacnetKey: ' + pacnetKey);
            System.Debug('@@@@ pacnetEntityMap: ' + pacnetEntityMap);

            // Queue naming convention is 'Billing - ' + <user region>
            String queueNameKey = 'Billing - ' + region;
            if (pacnetEntityMap != null && pacnetEntityMap.containsKey(pacnetKey)) {
                eachActionItem.OwnerId = queueMap.get('Billing - PACNET').QueueId;
            }
            else if (queueMap.get(queueNameKey) != null) {
                eachActionItem.OwnerId = queueMap.get(queueNameKey).QueueId;
            }
            // if user's region is not valid/found, then default to Billing - Australia
            else {
                eachActionItem.OwnerId = queueMap.get('Billing - Australia').QueueId;
            }

            lstActionItems.add(eachActionItem);
        }

        if(lstActionItems!=null && lstActionItems.size() >0 ){
            insert lstActionItems;
        }
    }

    public override void beforeUpdate(List<SObject> newBillProfiles, Map<Id, SObject> newBillProfilesMap, Map<Id, SObject> oldBillProfilesMap){
        updateItemisationDisplayLevel(newBillProfiles,(Map<Id, BillProfile__c>)oldBillProfilesMap);
        // Commented the below line as part of TGME0021240
        //updateContractDocuments((List<BillProfile__c>)newBillProfiles, (Map<Id, BillProfile__c>)oldBillProfilesMap);
    }

    //This menthod has been created as a replacement of BillProfileInsertUpdate on BillProfile__c trigger
    @testvisible
    public void updateItemisationDisplayLevel(List<BillProfile__c> newBillProfiles, Map<Id, BillProfile__c> oldBillProfilesMap){
        for(BillProfile__c bill : newBillProfiles){
            if(bill.Display_Data_Itemisation__c==false && bill.Itemisation_Display_Level__c==null){
                bill.Itemisation_Display_Level__c=0;
            }
        }

        Map<String, BillProfile__c> BillMap = new Map<String, BillProfile__c>();
        Map<String,BillProfile__c> accMap = new Map<String,BillProfile__c>();
        //Iterating through all the newly created bill profiles

        id recortypeId = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType, 'ISO Bill Profile RT');
        //RecordType rt = [select Id,Name from RecordType where Name='ISO Bill Profile RT'];

        for (BillProfile__c bill : newBillProfiles) {
            accMap.put(bill.Account__c,bill);
        }
        List<Account> acc = [select Id,Name,Customer_Type__c,OwnerId from Account where Id in :accMap.keyset()];
        for (BillProfile__c bill : newBillProfiles) {
            if ((bill.Bill_Profile_ORIG_ID_DM__c != null) &&
                    (oldBillProfilesMap == null ||
                    (bill.Bill_Profile_ORIG_ID_DM__c !=
                    oldBillProfilesMap.get(bill.Id).Bill_Profile_ORIG_ID_DM__c))) {
                //To check,if new bill profiles have duplicates.
                if (BillMap.containsKey(bill.Bill_Profile_ORIG_ID_DM__c)) {
                    bill.Bill_Profile_ORIG_ID_DM__c.addError('Duplicate value of bill profile number are present within this new Bill Profile list.');
                } else {
                    BillMap.put(bill.Bill_Profile_ORIG_ID_DM__c, bill);
                }
            }

            for(Account ac : acc){
                if (oldBillProfilesMap == null && bill.RecordTypeId == recortypeId && ac.Customer_Type__c == 'ISO'){

                    bill.Status__c = 'Active';
                    bill.Activated__c = True;
                    bill.Approved__c = True;
                    bill.Approved_By__c = ac.ownerId;
                }
            }
        }

        // Using a single query, to fetch all BillProfiles in the database that have the same bill profile number as any of them being inserted or updated.
        List<BillProfile__c> bpn= [SELECT Bill_Profile_ORIG_ID_DM__c,E_bill_Email_Billing_Address__c,CDR_Email_Address__c,CDR_Email_Address_CC__c,E_bill_Email_Billing_Address_CC__c FROM BillProfile__c WHERE Bill_Profile_ORIG_ID_DM__c IN :BillMap.KeySet()];
        //System.debug('---Before the For Loop ----'+bpn.size());
        if(bpn.size()>0){
            for (BillProfile__c bill :bpn ) {
                //System.debug('---Inside For Loop ----');

                BillProfile__c newBill = BillMap.get(bill.Bill_Profile_ORIG_ID_DM__c);
                //System.debug('Sent to Tibco'+newBill.Sent_To_Tibco__c);
                if(BillProfileUpdateCount.getcount()==0)
                {
                    newBill.addError('The bill profile number is already consumed by another bill profile. Could you please try other combination?');
                }
            }
            // System.debug('---End of For Loop ----');
        }

        string[] input = new string[]{};
        String PatternStr = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@' + '[A-Za-z0-9-]+(\\.[A-Za-z0-9-A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';

        for(BillProfile__c BP:newBillProfiles){
            If(BP.E_bill_Email_Billing_Address_CC__c!=null){
                input = BP.E_bill_Email_Billing_Address_CC__c.split('\\,');
                //Validating email address format
                for(integer i=0; i<input.size();i++){
                    if(!(pattern.Matches(PatternStr,input[i]))){
                        BP.E_bill_Email_Billing_Address_CC__c.addError('Email addresses entered in this field are not valid');
                    }
                }
            }
            If(BP.CDR_Email_Address__c!=null){
                input = BP.CDR_Email_Address__c.split('\\,');

                for(integer i=0; i<input.size();i++){
                    if(!(pattern.Matches(PatternStr,input[i]))){
                        BP.CDR_Email_Address__c.addError('Email addresses entered in this field are not valid');
                    }
                }
            }
            If(BP.CDR_Email_Address_CC__c!=null){
                input = BP.CDR_Email_Address_CC__c.split('\\,');

                for(integer i=0; i<input.size();i++){
                    if(!(pattern.Matches(PatternStr,input[i]))){
                        BP.CDR_Email_Address_CC__c.addError('Email addresses entered in this field are not valid');
                    }
                }
            }
            If(BP.E_bill_Email_Billing_Address__c!=null){
                input = BP.E_bill_Email_Billing_Address__c.split('\\,');

                for(integer i=0; i<input.size();i++){
                    if(!(pattern.Matches(PatternStr,input[i]))){
                        BP.E_bill_Email_Billing_Address__c.addError('Email addresses entered in this field are not valid');
                    }
                }
            }
        }
        BillProfileUpdateCount.doAdd();

        //CR-271 Validation rule on US postal code it shuould be before insert,before update
        //Iterating and checking postal code pattern and throwing error message in case patttern doesnot match
        for(BillProfile__c b : newBillProfiles){
            if(b.Country__c == 'US' && b.Billing_Entity__c == 'Telstra Incorporated' && b.Postal_Code__c!=null){
                //system.debug('billing_entity' + b.Billing_Entity__c + '' + b.Country__c + b.Postal_Code__c);
                if(!(Pattern.matches('[0-9]{5} [0-9]{4}',b.Postal_Code__c)||Pattern.matches('[0-9]{5}-[0-9]{4}',b.Postal_Code__c)||   Pattern.matches('[0-9]{5}[0-9]{4}',b.Postal_Code__c))){
                    b.addError('The postal code entered for country United States of America is not in the appropriate format.  The Postal Code format should be XXXXXYYYY(E.g. 123456789) OR XXXXX-XXXX(E.g. 12345-6789) OR XXXXX XXXX(e.g. 12345 6789). If have filled in the appropriate data and you are still getting this error message then please contact support team');
                }

            }else if(b.Postal_Code__c==null && b.Country__c == 'US' && b.Billing_Entity__c == 'Telstra Incorporated'){
                //system.debug('Inside the billing_entity for null values' + b.Billing_Entity__c + '' + b.Country__c + b.Postal_Code__c);
                b.addError('The postal code entered for country United States of America is not in the appropriate format.  The Postal Code format should be XXXXXYYYY(E.g. 123456789) OR XXXXX-XXXX(E.g. 12345-6789) OR XXXXX XXXX(e.g. 12345 6789). If have filled in the appropriate data and you are still getting this error message then please contact support team');
            }
        }
        //IR116 change for Auto population of profile type based on Billing entity
        if(oldBillProfilesMap == null){
            for(BillProfile__c b : newBillProfiles){
                if(b.Billing_Entity__c != null && b.Billing_Entity__c == 'Telstra International (AUS) Limited - Taiwan Branch'){
                    b.Profile_type__c = 'Statement';
                }
                else{
                    b.profile_type__c = 'Invoice';
                }
            }
        }

    }

    /*
     *   This trigger creates Action Item on updating  "Payment Terms" and "Billing Frequnecy" to send a email notification to the "TI legal/Contract Mgt"
     */
    //for @testvisible, modified by Anuradha
    @testvisible
    private void updateContractDocuments(List<BillProfile__c> newBillProfiles, Map<Id, BillProfile__c> oldBillProfilesMap){
        List<Action_Item__c> actionItemtoInsert = new List<Action_Item__c>(); 

        // Fetch the Current User Region
        List<User> usrLst =[Select Name, Region__c from User where id= :UserInfo.getUserId()];
        User usr = usrLst[0];

        for (BillProfile__c objBillProfile : newBillProfiles){
            Action_Item__c objActionItem = new Action_Item__c();
            // Check If Payment Terms is Updated and if Billing Frequnecy is Updated
            if ((oldBillProfilesMap.get(objBillProfile.Id).Payment_Terms__c != objBillProfile.Payment_Terms__c || oldBillProfilesMap.get(objBillProfile.Id).Billing_Frequency__c != objBillProfile.Billing_Frequency__c) && objBillProfile.Activated__c== true){                                    
                objActionItem.Bill_Profile__c = objBillProfile.Id;
                objActionItem.Account__c = objBillProfile.Account__c;
                objActionItem.Due_Date__c = System.Today() + 2;  
                objActionItem.RecordTypeId = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.sObjectType, 'Adhoc Request');
                objActionItem.Subject__c = 'Update Contract Documents';
                objActionItem.Status__c = 'Assigned';
                objActionItem.Priority__c = 'High';
                // Hard Coded Comments , need to Put them in Herirachial Custom Settings
                objActionItem.Comments__c = 'Payment Terms and Billing Frequency has changed, please update the contract documents (if any).';

                // Putting into Proper Regional Queue
                Util.assignOwnersToRegionalQueue(usr,objActionItem, 'Adhoc Request');
                actionItemtoInsert.add(objActionItem);
            }
            //This trigger updates the field "Approved By" who checked the "Approved" Checkbox

            if (oldBillProfilesMap.get(objBillProfile.Id).Approved__c != objBillProfile.Approved__c && objBillProfile.Approved__c == TRUE) {
                objBillProfile.Approved_By__c = UserInfo.getUserId();

            }
        }
        insert actionItemtoInsert; 
    }
}