public class EmailGenerationCls{

 public void insertDynDataInBody(List<Case> caseList,String body){
        try{
        EMailVO prEMailVO= new EMailVO();
       
        prEMailVO.to=new String[]{'suparna.sikdar@accenture.com'};
        prEMailVO.subject=caseList[0].Subject +' has been assigned to '+caseList[0].Owner.Name;
        String body1 = 'Hi'+' '+caseList[0].Owner.Name+','+'</br>'+'</br>';
                
        String body2 = 'The following Action Item(s) '+'has been created against a Service/Resource and has been assigned to your Queue. This Service is missing the Billing Commencement Date and/or Contract Duration and is required to be added to the service.'+'</br>';
       // String body3=URL.getSalesforceBaseUrl().toExternalForm() + '/'+trigger.new[0].Id+'</br></br>';
        prEMailVO.body=body1+body2+'Details as follows.'+'</br>'+'</br>'+body;
        
        User CaseCreatedBy = [Select Id,name,Email,Region__c from User where id = :caseList[0].CreatedById];
        
        sendMail(prEMailVO,caseList[0].Account.region__c, CaseCreatedBy.name);
        }catch (Exception ex) {
            ErrorHandlerException.ExecutingClassName='EmailGenerationCls:insertDynDataInBody';
            ErrorHandlerException.objectList=caseList;
            ErrorHandlerException.sendException(ex);
            System.Debug('Error in inserting Dynamic data in EmailBody ..'+ex);
        }
    }
        public  Boolean sendMail(EMailVO prEMailVO,String Accountregion,String CaseCreatedBy){
        Boolean mailSent=false;
        try{
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = getEmailAddess(Accountregion).split(':', 0);
            
            String ccAddresses = getEmailCCAddess(CaseCreatedBy);
            String[] ListCCAddresses = new String[]{ccAddresses};

            system.debug('to addresses '+toAddresses );
            system.debug('CC addresses '+ccAddresses );
            mail.setToAddresses(toAddresses);
            mail.setccAddresses(ListCCAddresses);
            mail.setSubject(prEMailVO.subject);
            mail.setHtmlBody(prEMailVO.body);
            List<Messaging.SendEmailResult> results= Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            mailSent=results.get(0).isSuccess();
            if(!mailSent){
                System.Debug('Errors while sending Mail ..'+results.get(0).getErrors()[0] );
            }
        }catch(Exception ex){
            ErrorHandlerException.ExecutingClassName='EmailGenerationCls:sendMail';
            ErrorHandlerException.sendException(ex);
            System.Debug('Errors while sending Mail ..'+ex );
        }
        return mailSent;
    }
 
      public void getEmailList(List<Case> caseList){
        
        List<String> dynDatas = new List<String>();
     
        //get list of all the ordered items
        string htmlOrderedList='';
        string htmlOrderedList1='';
        string htmlOrderedList2='';
        string htmlOrderedList3='';
        string htmlOrderedList4='';
         htmlOrderedList1='<style type="text/css">'+
        '.tg  {border-collapse:collapse;border-spacing:0;border-color:#000000;}'+
        '.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}'+
        '.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}'+
        '</style>';
        
        htmlOrderedList2='<table class="tg">'+
        '<tr>'+
        '<th class="tg-031e"><strong>Case Number</strong></th>'+
        '<th class="tg-031e"><strong>Service ID</strong></th>'+
        '<th class="tg-031e"><strong>Billing Commencement Date</strong></th>'+
        '<th class="tg-031e"><strong>Contract Duration</strong></th>'+
        '</tr>';
        
        for(Case caseObj:caseList)
        {
         htmlOrderedList4+='<tr>'+
         '<td class="tg-031e">'+'<a href='+URL.getSalesforceBaseUrl().toExternalForm() + '/'+caseObj.Id+'>'+caseObj.CaseNumber+'</a></td>'
         +'<td class="tg-031e">'+(caseObj.CS_Service__c==null?'N/A':'<a href='+URL.getSalesforceBaseUrl().toExternalForm() + '/'+caseObj.CS_Service__c+'>'+caseObj.CS_Service__r.Name+'</a>')+'</td>'
         +'<td class="tg-031e">'+(caseObj.Billing_Commencement_Date__c==null?'N/A':caseObj.Billing_Commencement_Date__c.format())+'</td>'
         +'<td class="tg-031e">'+(caseObj.Contract_Term__c==null?'N/A':caseObj.Contract_Term__c)+'</td>'
         +'</tr>';
        
        }
        
        String htmlOrderedList5='</table>';
        
        
    
    
                 
        //insert dynamic data in email html body
        insertDynDataInBody(caseList,htmlOrderedList1+htmlOrderedList+htmlOrderedList2+'</br>'+htmlOrderedList3+htmlOrderedList4+htmlOrderedList5+'Thanks,'+'</br>'+'***This is an auto-generated notification, please do not reply to this email***.');
        //send email
        
        
     }
        public String getEmailAddess(String accountRegion){
 // query to get all queues corresponding to Bill profile Object
          Map<String, QueueSobject> QueueMap = QueueUtil.getQueuesMapForObject(Case.SObjectType);
            
            //Query the User object  to get sales User who created the Order
            List<User> usrLst =[Select Name, Region__c from User where id= :UserInfo.getUserId()];
            User usr = usrLst[0];
             Id queid;
            String billTeam;
            String emailAddresses = '';
            String USerRegion='';
            //Setting for email attributes
            Map<String, Id> queMap= new Map<String, Id> ();
            
            //Get Billing Team Queue id corresponding to sales user region.
            for (QueueSObject q:QueueMap.values()){
                if((q.Queue.Name).contains(accountRegion)&& (q.Queue.Name).contains('Billing')){
                    queMap.put(q.Queue.Name,q.QueueId);
                    queid = q.QueueId;
                    billTeam = q.Queue.Name;
                   // System.debug('queue name ..'+q.Queue.Name);
                }
            }
            String groupMemberQuery = 'SELECT GroupId, Id, SystemModstamp, UserOrGroupId FROM GroupMember where GroupId =\''+queid+'\'';
            
            //Getting the all Users id's  corresponding to queue
            List<GroupMember> list_GM = Database.query(groupMemberQuery);
            
            List<Id> lst_Ids = new List<ID>();
            for(GroupMember gmObj : list_GM){
                lst_Ids.add(gmObj.UserOrGroupId);
            }
            
            //Query the Billing Team Users 
            //List<User> lst_UserObj = [Select Id,name,Email,Region__c from User where id in :lst_Ids];
            
            //Here concanating all email addresses of Users belong to Billing Queue
            for(User usrObj : [Select Id,name,Email,Region__c from User where id in :lst_Ids]){
                if(emailAddresses == ''){
                    emailAddresses = usrObj.Email;
                     USerRegion=usrObj.Region__c;
                }else{
                    emailAddresses += ':'+usrObj.Email; 
                     USerRegion=usrObj.Region__c;
                }
            }
            

           System.debug('emailadresses..'+emailAddresses);
           return emailAddresses;
    
    }
    
    public String getEmailCCAddess(String CaseCreatedBy){
 
            //Query the User object  to get sales User who created the Case
            User usr =[Select Name, Region__c, Email from User where name= :CaseCreatedBy limit 1];
            String emailAddresses = '';
            try{
                if(emailAddresses == '' && usr.Email != ''){
                        emailAddresses = usr.Email;
                }else{
                        emailAddresses += ':'+usr.Email; 
                }
                System.debug('emailadresses..'+emailAddresses);
            }catch (Exception ex) {
            ErrorHandlerException.ExecutingClassName='EmailGenerationCls:getEmailCCAddess';
            ErrorHandlerException.sendException(ex);
            System.Debug('Error in getEmailCCAddess function ..'+ex);
            }
           
           return emailAddresses;
    
    }
    
      public class EmailVO{
    public String sender{get;set;}
    public String senderDispName{get;set;}
    public String[] to{get;set;}
    public String[] cc{get;set;}
    public String[] bcc{get;set;}
    public String subject{get;set;}
    public String body{get;set;}
    public String header{get;set;}
    public String footer{get;set;}
    public String module{get;set;}
    public String originator{get;set;}
    public String replaceCharStart{get;set;}
    public String replaceCharEnd{get;set;}
    public Boolean isCIC{get;set;}
    }


}