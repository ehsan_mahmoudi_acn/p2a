/**
    @author - Accenture
    @date - 19-july-2012
    @version - 1.0
    @description - This is the test class for MultiSelectComponentController class
*/
@isTest
private class MultiSelectComponentControllerTest {
/* 
    Static Account a;
    Static Contact c; 
    Static Site__c s1;
    Static Country_Lookup__c cl;
    Static City_Lookup__c city;
        
    static testMethod void mySelectingTest() {
            
        Contact cont= getContact();       
       
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(cont); 
        MultiSelectComponentController mSC = new MultiSelectComponentController();
        
        List<String> selected = new List<String>();
        selected.add(cont.id);        
        List<SelectOption> opt = new List<SelectOption>();
        Contact conta = [Select Id,Name from Contact where id=:cont.id];
        opt.add(new SelectOption(conta.id,conta.name));        
        mSC.selected = selected;
        mSC.Options = opt;
        mSC.selectedOptions = null;
        mSC.desiredObject = 'Contact';
        mSC.selecting();
        
        List<String> deselected = new List<String>();
        deselected.add(cont.id);        
        List<SelectOption> opt1 = new List<SelectOption>();
        Contact conta1 = [Select Id,Name from Contact where id=:cont.id];
        system.assert(conta1!=null);
        opt1.add(new SelectOption(conta1.id,conta1.name));        
        mSC.deselected = deselected;
        mSC.selectedOptions = opt1;
        mSC.desiredObject = 'Contact';
        mSC.deselecting();
        
    }
    
     static testMethod void myDeSelectingTest() {
        
        Site__c site1  = getSite();
        
        ApexPages.StandardController sc2 = new ApexPages.StandardController(site1);
        MultiSelectComponentController mSC2 = new MultiSelectComponentController();
        
        List<String> selected = new List<String>();
        selected.add(site1.id);        
        List<SelectOption> opt = new List<SelectOption>();
        Site__c site12 = [Select Id,Name from Site__c where id=:site1.id];
        system.assert(site12 !=null);
        opt.add(new SelectOption(site12.id,site12.name));        
        mSC2.selected = selected;
        mSC2.Options = opt;
        mSC2.selectedOptions = null;
        mSC2.desiredObject = 'Site';
        mSC2.selecting();
        
        List<String> deselected = new List<String>();
        deselected.add(site1.id);        
        List<SelectOption> opt1 = new List<SelectOption>();
        Site__c site13 = [Select Id,Name from Site__c where id=:site1.id];
        opt1.add(new SelectOption(site13.id,site13.name));        
        mSC2.deselected = deselected;
        mSC2.selectedOptions = opt1;
        mSC2.desiredObject = 'Site';
        mSC2.deselecting();
     }
    
    private static Account getAccount()
        {
           if(a == null)
           {    
            a = new Account();
            Country_Lookup__c country = getCountry();
            a.Name = 'Test Account';
            a.Customer_Type__c = 'MNC';
            a.Country__c = country.Id;
            //a.Selling_Entity__c = 'Telstra INC';
            a.Account_Manager__c = 'Albert Thomas';
            a.Activated__c = true;
            a.Account_ID__c = '9090';
           // a.RecordTypeId = rt.Id;
            a.Account_Status__c ='Active';
            a.Customer_Legal_Entity_Name__c='Tets';
            insert a;
            List<Account> ac = [Select id,name from Account where name ='Test Account'];
            system.assertequals(a.name,ac[0].name);
            system.assert(a!=null);
           }
        return a;
    }
    
    private static Contact getContact()
        {
              if( c== Null)
              {
                  c = new Contact();
                  Account acc = getAccount();
                  c.Description='testContact';
                  c.AccountId=acc.Id; 
                  c.LastName='testName';
                  c.Contact_Type__c='Sales';                  
                  insert c;
                  system.assert(c!=null);
              }
              return c;
        }  
        
        private static Site__c getSite(){
        s1 = new Site__c();
        s1.name = 'Acc';
        s1.Country_Finder__c = getCountry().id ;
        s1.City_Finder__c = getCity().id;
        s1.Address_Type__c = 'Site Address';
        s1.Address1__c = 'abc';
        s1.Address2__c = 'xyz';
        s1.AccountId__c = getAccount().id;
        insert s1;
        system.assert(s1!=null);
        return s1;
    }  
    
    private static Country_Lookup__c getCountry()
    {
        if(cl == null)
       { 
        cl = new Country_Lookup__c();
        cl.CCMS_Country_Code__c = 'SG';
        cl.CCMS_Country_Name__c = 'India';
        cl.Country_Code__c = 'SG';
        insert cl;
        system.assert(cl!=null);
        }
        return cl;
    } 
    
    private static City_Lookup__c getCity(){
    if(city == null){
    city = new City_Lookup__c();
     city.Generic_Site_Code__c = 'U12';
     city.name = 'Magu';
     city.City_Code__c='xyz';
     city.OwnerID = userinfo.getuserid();
          insert city;
          system.assert(city!=null);
          }
     return city;
     
    }  */    
}