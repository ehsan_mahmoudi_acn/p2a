/**
    @author - Accenture
    @date - 19- Jul-2012
    @version - 1.0
    @description - This is the test class for PRCreateWebService class.
*/
@isTest(seeallData = false)
private class PRCreateWebServiceTest {
/*
static Account acc;
static Country_Lookup__c cl;
static Opportunity opp;
static BillProfile__c b;
static Site__c s;
static City_Lookup__c ci;
static OpportunityLineItem oli;

static testMethod void testUpdatePRFeilds(){
    acc=getAccount();
    oli = getOpportunityLineItem();
    oli.Location__c = 'Mumbai';
    update oli;
    
    PRCreateWebService.fetchLineItemDetails();         
    
}
private static Account getAccount(){
    if(acc == null){
        acc = new Account();
        cl = getCountry();
        acc.Name = 'Test Account';
        acc.Customer_Type__c = 'MNC';
        acc.Country__c = cl.Id;
        acc.Selling_Entity__c = 'Telstra INC';
        acc.Activated__c = True;
        acc.Account_ID__c = '9090';
        acc.Account_Status__c = 'Active';
        acc.Customer_Legal_Entity_Name__c='test';
        insert acc;
    }
    return acc;
}
private static Country_Lookup__c getCountry(){
    if(cl == null){
        cl = new Country_Lookup__c();
        cl.CCMS_Country_Name__c = 'India';
        cl.Country_Code__c = 'IND';
        insert cl;
    }
    return cl;
} 
private static City_Lookup__c getCity(){
    if(ci == null){
        ci = new City_Lookup__c();
        ci.City_Code__c ='MUM';
        ci.Name = 'MUMBAI';
        insert ci;
    }
    return ci;
}
private static Opportunity getOpportunity(){
        if(opp == null) {
            opp = new Opportunity();
           
            opp.Name = 'Test Opportunity';
            opp.AccountId = acc.Id;
             //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
            opp.StageName = 'Identify & Define';
            opp.CloseDate = System.today();
            opp.Estimated_MRC__c=800;
            opp.Estimated_NRC__c=1000;
            opp.ContractTerm__c='10';
          //  opp.Approx_Deal_Size__c = 9000;
            opp.TigFin_PR_Actions__c = 'Processed';
            insert opp;
        }
        return opp;
    }
    
    
private static OpportunityLineItem getOpportunityLineItem(){ 
    if(oli == null){
        oli = new OpportunityLineItem();
        opp = getOpportunity();
        b = getBillProfile();
        oli.OpportunityId = opp.Id;
        oli.IsMainItem__c = 'Yes';
        oli.OrderType__c = 'New Provide';
        oli.Quantity = 5;
        oli.UnitPrice = 10;
        oli.PricebookEntryId = getPriceBookEntry('GCPE').Id;
        oli.ContractTerm__c = '12';
        oli.OffnetRequired__c = 'Yes';
        oli.Location__c = 'Bengaluru';
        oli.BillProfileId__c = b.Id;
        insert oli;
    }
    return oli;
}


private static Pricebook2 getPriceBook(String prodName){
        Pricebook2 p = [SELECT Id FROM Pricebook2 where IsStandard = true LIMIT 1];
        return p;   
    }
    
private static Product2 getProduct(String prodName){
        Product2 prod = new Product2();
        prod.Name = prodName;
        prod.ProductCode = prodName;
        prod.Product_ID__c ='GCPE';
        insert prod;
        return prod;
    }
    
private static PricebookEntry getPriceBookEntry(String prodName){
        PricebookEntry p = new PricebookEntry();
        p.Pricebook2Id = getPriceBook(prodName).Id;
        p.Product2Id =  getProduct(prodName).Id;
        p.UnitPrice = 2000;
        p.IsActive = true;
        insert p; 
        return p;    
    }
    
    
private static BillProfile__c getBillProfile(){
    if(b == null) {
        b = new BillProfile__c();
        s = getSite();
        //acc = getAccount();
        //b.Bill_Profile_Number__c = 'Test Bill Profile';
        b.Billing_Entity__c = 'Telstra Limited';
        b.Account__c = s.AccountId__c;
        b.Bill_Profile_Site__c = s.Id;
        b.Invoice_Breakdown__c = 'Summary Page';
         b.First_Period_Date__c = date.today();
         b.Start_Date__c = date.today();
        //b.Operating_Unit__c = 'TI_HK_I52S';
        insert b;
    }
        return b;
}
private static Site__c getSite(){
    if(s == null){
        s = new Site__c();
        acc = getAccount();
        s.Name = 'Test_site';
        s.Address1__c = '43';
        s.Address2__c = 'Bangalore';
        cl = getCountry();
        s.Country_Finder__c = cl.Id;
        ci = getCity();
        s.City_Finder__c = ci.Id;
        s.AccountId__c =  acc.Id; 
        s.Address_Type__c = 'Billing Address';
        insert s;
    }
    return s;
} */ 
}