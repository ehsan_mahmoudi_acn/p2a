@isTest
public class MasterAndAccountMACDProgressBartest{
       private static List<Product_Definition_Id__c> PdIdlist;
   private static List<cscfga__Product_Definition__c> Pdlist;
   private static List<Offer_Id__c>offIdlist;
    static testmethod void masterAndAccountMACDProgressBartest() {
    
 P2A_TestFactoryCls.sampleTestData();
    string basketid;
         
        cscfga__Configuration_Offer__c csfa = new cscfga__Configuration_Offer__c(name = 'Master  VPLS VLAN Service', cscfga__Active__c = true, cscfga__Template__c = false);
        insert csfa;
            System.assert(csfa!=null);

    
    offIdlist = new List<Offer_Id__c>{
        
        new Offer_Id__c(name ='IPVPN_Port_withoutLL_Offer_Id',Offer_Id__c = csfa.id), 
        new Offer_Id__c(name ='VPLS_VLAN_withoutLL_Offer_Id',Offer_Id__c = csfa.id), 
        new Offer_Id__c(name ='VPLS_Transparent_withoutLL_Offer_Id',Offer_Id__c = csfa.id), 
        new Offer_Id__c(name ='ASBR_TypeA_Offer_Id',Offer_Id__c = csfa.id), 
        new Offer_Id__c(name ='SMA_Gateway_Offer_Id',Offer_Id__c = csfa.id),  
        new Offer_Id__c(name ='VLAN_Group_Offer_Id',Offer_Id__c = csfa.id)
    };
    insert offIdlist; 
    system.assertEquals(offIdlist[0].name,'IPVPN_Port_withoutLL_Offer_Id' );
    
    Pdlist = new List<cscfga__Product_Definition__c>{
              new cscfga__Product_Definition__c(name  = 'ASBR_Definition_Id',cscfga__Description__c = 'ASBR_Definition_Id'), 
              new cscfga__Product_Definition__c(name  = 'IPVPN_Port_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'SMA_Gateway_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'VLANGroup_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'VPLS_Transparent_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'VPLS_VLAN_Port_Definition_Id',cscfga__Description__c = 'Test master VPLS')  
            };
     insert Pdlist;      
     System.assertEquals('ASBR_Definition_Id',Pdlist[0].Name);
     
     PdIdlist = new List<Product_Definition_Id__c>{
                new Product_Definition_Id__c(name = 'ASBR_Definition_Id',Product_Id__c = Pdlist[0].Id),
                new Product_Definition_Id__c(name = 'IPVPN_Port_Definition_Id',Product_Id__c = Pdlist[1].id),
                new Product_Definition_Id__c(name = 'SMA_Gateway_Definition_Id',Product_Id__c = Pdlist[2].id),
                new Product_Definition_Id__c(name = 'VLANGroup_Definition_Id',Product_Id__c = Pdlist[3].id),                
                new Product_Definition_Id__c(name = 'VPLS_Transparent_Definition_Id',Product_Id__c = Pdlist[4].id),
                new Product_Definition_Id__c(name = 'VPLS_VLAN_Port_Definition_Id',Product_Id__c = Pdlist[5].id)
     };
     insert PdIdlist;
     system.assertEquals(PdIdlist[0].name,'ASBR_Definition_Id' ) ; 
       List<cscfga__Product_Category__c> prodCategoryList = new List<cscfga__Product_Category__c>{
            new cscfga__Product_Category__c(Name = 'Test Produc Category')
        };
        insert prodCategoryList;
         System.assert(prodCategoryList!=null);
   List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        List<cscfga__Product_Definition__c> ProductDeflist= new List<cscfga__Product_Definition__c>();
        cscfga__Product_Definition__c pb = new cscfga__Product_Definition__c();
        pb.name = 'point to Point'; 
        pb.cscfga__Description__c= 'GCPEbdkjdvbhe;ghwfguwefkjwf';
        pb.cscfga__Product_Category__c = prodCategoryList[0].id;
        ProductDeflist.add(pb);
        insert ProductDeflist;
        Integer i = [select count() from cscfga__Product_Definition__c ];
        system.assertNotEquals(i,1);
        
        List<cscfga__Configuration_Screen__c> configLst = P2A_TestFactoryCls.getConfigScreen(1, ProductDeflist);
        List<cscfga__Screen_Section__c> ScreenSec = P2A_TestFactoryCls.getScreenSec(1, configLst);
        List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfigs = new List<cscfga__Product_Configuration__c>();
        //proconfigs[0].cscfga__Root_Configuration__c = null;
        //upsert proconfigs[0];
       
       cscfga__Product_Configuration__c pb2 = new cscfga__Product_Configuration__c();
                pb2.cscfga__Product_basket__c = prodBaskList[0].id ;
                pb2.cscfga__Product_Definition__c = Pdlist[0].id ;
                pb2.name ='IPL';
                pb2.cscfga__originating_offer__c = Offerlists[0].id;
                pb2.cscfga__Root_Configuration__c = null;
                pb2.cscfga__contract_term_period__c = 12;
                pb2.Is_Offnet__c = 'yes';
                pb2.cscfga_Offer_Price_MRC__c = 200;
                pb2.cscfga_Offer_Price_NRC__c = 300;
                pb2.cscfga__Product_Bundle__c = Pbundle[0].id;
                pb2.Rate_Card_NRC__c = 200;
                pb2.Rate_Card_RC__c = 300;
                pb2.cscfga__total_contract_value__c = 1000;
                pb2.cscfga__Contract_Term__c = 12;
                pb2.CurrencyIsoCode = 'USD';
                pb2.cscfga_Offer_Price_MRC__c = 100;
                pb2.cscfga_Offer_Price_NRC__c = 100;
                pb2.Child_COst__c = 100;
                pb2.Cost_NRC__c = 100;
                pb2.Cost_MRC__c = 100;
                pb2.Product_Name__c = 'test product';
                pb2.Added_Ports__c = null ;
                Pb2.cscfga__Product_Family__c = 'Point to Point';
       proconfigs.add(pb2);         
       Insert proconfigs;
       Integer j = [select count() from cscfga__Product_Configuration__c ];
       system.assertEquals(j,1);
       
        List<csbb__Product_Configuration_Request__c> ProductConfigreqlist = new List<csbb__Product_Configuration_Request__c>();
         csbb__Product_Configuration_Request__c pb1 = new csbb__Product_Configuration_Request__c();
                pb1.csbb__Product_Category__c= prodCategoryList[0].id;
                pb1.csbb__Product_Configuration__c = proconfigs[0].id;
                ProductConfigreqlist.add(pb1);
        Insert ProductConfigreqlist;
        Integer k = [select count() from csbb__Product_Configuration_Request__c ];
        system.assertEquals(k,1);
        
     Attachment attach=new Attachment();    
     attach.Name='Unit Test Attachment';
     Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
     attach.body=bodyBlob;
     attach.parentId=ProductConfigreqlist[0].id;
    // insert attach;
      List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
            List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(5,OrdReqList);    
  //  List<csord__service__c> servList = P2A_TestFactoryCls.getservice(5,OrdReqList,subscriptionList);  
        
    ApexPages.StandardController sc = new ApexPages.StandardController(prodBaskList[0]);
    MasterAndAccountMACDProgressBar master = new MasterAndAccountMACDProgressBar(sc);
    master.refreshJobStatus();
    master.getBatchOrderStatus();
    }
    
    }