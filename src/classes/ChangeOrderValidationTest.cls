/**
    @author - Accenture
    @date - 25-Nov-2014
    @version - 1.0
    @description - This is the test class for ChangeOrderValidation Class
*/
@isTest(SeeAllData = false)
//Start: Suresh 28/09/2017- Test class name changed as part of tech_debt
private class ChangeOrderValidationTest
//End
{

/*static Account acc;
static Opportunity opp;
static Country_Lookup__c cl;
static City_Lookup__c ci;
static Site__c s;
static Contact con;

static testMethod void TestserviceValidation() {

Set<Id> OppIdSet =  new Set<Id>();
string mesg;

        opp = getOpportunity();
        OppIdSet.add(opp.Id);
             
        Test.startTest();
        getOpportunityDetails();
        ChangeOrderValidation changeOrder= new ChangeOrderValidation();
        Database.SaveResult srList = changeOrder.serviceValidation(OppIdSet);
        Test.stopTest();
        
       }

static testMethod void getOpportunityDetails() {
        
        opp = getOpportunity();
        opp.QuoteStatus__c = 'Order';
        opp.StageName = 'Prove & Close';
        opp.Stage__c='Prove & Close';
        opp.Sales_Status__c = 'Won';
        try
        {
            update opp;
        }catch (QueryException e)
       {}
       catch(DMLException e1){}

       OpportunityLineItem oppLineItem = new OpportunityLineItem();
        oppLineItem.OpportunityId = getOpportunity().id;
        oppLineiTem.IsMainItem__c = 'No';
        oppLineItem.OrderType__c = 'New Provide';
        oppLineItem.CPQItem__c='1';
        oppLineItem.Quantity = 5;
        oppLineItem.UnitPrice = 10;
        oppLineItem.BillProfileId__c = null;
        oppLineItem.PricebookEntryId = getPriceBookEntry('IPT').Id;
        oppLineItem.ContractTerm__c = '24';
        oppLineItem.OffnetRequired__c = 'Yes';
        oppLineItem.is_bundle__c = true;
        oppLineItem.Parent_Bundle_Flag__c = false;
        oppLineItem.Purchase_Requisition_Number__c = '111';
        oppLineItem.Parent_Charge_ID__c = null;
        oppLineItem.Site__c = getSite().Id;
        oppLineItem.Site_B__c = getSite().Id;
        insert oppLineItem;

    }
    private static List<OpportunityLineItem> getOpportunityLineItem(Id oppId){
        
        List<OpportunityLineItem> lst = new List<OpportunityLineItem>();
        
        OpportunityLineItem oli = new OpportunityLineItem();
        //opp = getOpportunity();
        BillProfile__c b = getBillProfile();
        s = getSite();
        oli.OpportunityId = oppId;
        oli.IsMainItem__c = 'Yes';
        oli.OrderType__c = 'New Provide';
        oli.Quantity = 5;
        oli.UnitPrice = 10;
        oli.PricebookEntryId = getPriceBookEntry('Global IPVPN').Id;
        oli.ContractTerm__c = '12';
        oli.OffnetRequired__c = 'Yes';
        oli.Purchase_Requisition_Number__c = '';
        oli.Site__c = s.Id;
        oli.Site_B__c = s.Id;
        
        lst.add(oli);
        
        OpportunityLineItem oppLineItem = new OpportunityLineItem();
        oppLineItem.OpportunityId = oppId;
        oppLineiTem.IsMainItem__c = 'No';
        oppLineItem.OrderType__c = 'New Provide';
        oppLineItem.CPQItem__c='1';
        oppLineItem.Quantity = 5;
        oppLineItem.UnitPrice = 10;
        oppLineItem.BillProfileId__c = null;
        oppLineItem.PricebookEntryId = getPriceBookEntry('IPT').Id;
        oppLineItem.ContractTerm__c = '24';
        oppLineItem.OffnetRequired__c = 'Yes';
        oppLineItem.is_bundle__c = true;
        oppLineItem.Parent_Bundle_Flag__c = true;
        oppLineItem.Purchase_Requisition_Number__c = '111';
        oppLineItem.Site__c = s.Id;
        oppLineItem.Site_B__c = s.Id;
        lst.add(oppLineItem);
        
        OpportunityLineItem oppLineItem1 = new OpportunityLineItem();
        BillProfile__c b1 = getBillProfile();
        oppLineItem1.OpportunityId = oppId;
        oppLineiTem1.IsMainItem__c = 'Yes';
        oppLineItem1.OrderType__c = 'New Provide';
        oppLineItem1.Quantity = 5;
        oppLineItem1.UnitPrice = 10;
        oppLineItem1.BillProfileId__c = b1.Id;
        oppLineItem1.PricebookEntryId = getPriceBookEntry('IPL').Id;
        oppLineItem1.ContractTerm__c = '48';
        oppLineItem1.OffnetRequired__c = 'Yes';
        oppLineItem1.Purchase_Requisition_Number__c = '111';
        oppLineItem1.Site__c = null;
        oppLineItem1.Site_B__c = null;
        oppLineItem1.POP_A_Code__c = null;
        oppLineItem1.POP_Z_Code__c = null;
        lst.add(oppLineItem1);
 
        OpportunityLineItem oppLineItem2 = new OpportunityLineItem();
        BillProfile__c b2 = getBillProfile();
        oppLineItem2.OpportunityId = oppId;
        oppLineiTem2.IsMainItem__c = 'Yes';
        oppLineItem2.OrderType__c = 'New Provide';
        oppLineItem2.Quantity = 5;
        oppLineItem2.UnitPrice = 10;
        oppLineItem2.BillProfileId__c = b2.Id;
        oppLineItem2.PricebookEntryId = getPriceBookEntry('EPL').Id;
        oppLineItem2.ContractTerm__c = '48';
        oppLineItem2.OffnetRequired__c = 'Yes';
        oppLineItem2.Purchase_Requisition_Number__c = '111';
        oppLineItem2.Site__c = null;
        oppLineItem2.Site_B__c = null;
        oppLineItem2.POP_A_Code__c = 'OTHER';
        oppLineItem2.POP_Z_Code__c = 'OTHER';
        lst.add(oppLineItem2);
            insert lst;
        
        return lst;
    }
    private static Opportunity getOpportunity(){
        if(opp == null) {
            opp = new Opportunity();
            acc = getAccount();
            opp.Name = 'Test Opportunity';
            opp.AccountId = acc.Id;
             //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
            opp.StageName = 'Identify & Define';
            opp.Stage__c='Identify & Define';
            opp.CloseDate = System.today();
            opp.Estimated_MRC__c=800;
            opp.Estimated_NRC__c=1000;
            opp.ContractTerm__c='10';
            
          //  opp.Approx_Deal_Size__c = 9000;
            //opp.Signed_Contract_Upload_Date__c = System.today();
            
           
           // getOpportunityLineItem(opp.Id);
                        
            insert opp;
            
           
        }
        return opp;
    }
    private static Pricebook2 getPriceBook(String prodName){
        Pricebook2 p = [SELECT Id FROM Pricebook2 where IsStandard = true LIMIT 1];
        return p;   
    }
    
    private static Product2 getProduct(String prodName){
        Product2 prod = new Product2();
        prod.Name = prodName;
        prod.ProductCode = prodName;
        prod.Product_Id__c = 'EPLS';
        prod.Create_Service__c = 'ME';
        prod.Create_Master_Service__c = false;
        prod.Create_Path__c = true;
        insert prod;
        return prod;
    }
    
    private static PricebookEntry getPriceBookEntry(String prodName){
        PricebookEntry p = new PricebookEntry();
        p.Pricebook2Id = getPriceBook(prodName).Id;
        p.Product2Id =  getProduct(prodName).Id;
        p.UnitPrice = 2000;
        p.IsActive = true;
        p.UseStandardPrice = false;
        insert p; 
        return p;    
    }
    private static Site__c getSite(){      
        if(s == null){
            s = new Site__c();
            s.Name = 'TestSite';
            acc = getAccount();
            s.Address1__c ='door no.10';
            s.Address2__c='bangalore,Testdata';
            s.Address_Type__c='Billing Address';
            cl = getCountry();
            s.Country_Finder__c = cl.Id;
            s.AccountId__c = acc.Id;
            ci = getCity();
            s.City_Finder__c = ci.Id;
            insert s;
        }
        return s;                  
    }            
    
    private static BillProfile__c getBillProfile(){
        BillProfile__c bp = new BillProfile__c();
        if(acc == null)
            acc = getAccount();
        
        if(s == null)
            s = getSite();
        
       // b.Bill_Profile_Number__c = 'Test Bill Profile';
        bp.Billing_Entity__c = 'Telstra Limited';
        bp.Account__c = acc.Id;
        bp.Bill_Profile_Site__c = s.id;
        
        if(con == null)
            con = getContact();
        bp.Billing_Entity__c = 'Telstra Corporation';
        bp.CurrencyIsoCode = 'GBP';
        bp.Payment_Terms__c = '30 Days';
        bp.Start_Date__c = Date.today();
        bp.Invoice_Delivery_Method__c = 'Postal';
        bp.Postal_Delivery_Method__c = 'First Class';
        bp.Invoice_Breakdown__c = 'Summary Page';
        bp.Billing_Contact__c = con.id;
        bp.Status__c = 'Approved';
        bp.Payment_Method__c = 'Manual';
        bp.Minimum_Bill_Amount__c = 10.20;
        bp.First_Period_Date__c = date.today();
        bp.Rating_Rounding_Level__c = 2;
        bp.Bill_Decimal_Point__c = 0;
        bp.FX_Group__c = 'Corporate';
        bp.FX_Rule__c = 'FX Rate at end of period';
        
        
        
        insert bp;
        return bp;
    }
    
    private static Account getAccount(){
        if(acc == null){    
            acc = new Account();
            cl = getCountry();
            acc.Name = 'Test Account Test 1';
            acc.Customer_Type__c = 'MNC';
            acc.Country__c = cl.Id;
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Activated__c = True;
            acc.Account_Status__c = 'Active';
            acc.Billing_Status__c = 'Active';
            acc.Account_ID__c = '9090';
            acc.Customer_Legal_Entity_Name__c='Test';
            insert acc;
        }
        return acc;
    }
    private static Country_Lookup__c getCountry(){
        if(cl == null){ 
            cl = new Country_Lookup__c();
            cl.CCMS_Country_Code__c = 'SG';
            cl.CCMS_Country_Name__c = 'India';
            cl.Country_Code__c = 'SG';
            insert cl;
        }
        return cl;
    }
    private static City_Lookup__c getCity(){
        if(ci == null){
            ci = new City_Lookup__c();
            ci.Name = 'AtS';
            ci.City_Code__c ='AT';
            
           // ci.Generic_Site_Code__c = 'CHE&';
            insert ci;
        }
        return ci;
    }
    
    //contact creation
    private static Contact getContact(){
      if(con == null){
        con = new Contact();
        if(acc == null)
        acc = getAccount();
        if(cl == null)
        cl = getCountry();
        
        con.LastName = 'Talreja';
        con.FirstName = 'Dinesh';
        
        con.AccountId = acc.Id;
        con.Contact_Type__c = 'Technical';
        con.Country__c = cl.Id;
        con.Primary_Contact__c = true;
        con.MobilePhone = '123333';
        con.Phone = '1223';
        insert con;         
      }
      return con;
    }*/
      
}