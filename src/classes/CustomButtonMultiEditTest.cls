@isTest(SeeAllData = false)
Public class CustomButtonMultiEditTest{
/**
* Disables triggers, validations and workflows for the given user
* @param userId Id
*/
     
    private static voId disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
        system.assert(globalMute!=null);
    }
    
/**
* Enables triggers, valIdations and workflows 
* @param userId Id
*/
    private static voId enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_ValIdations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_ValIdations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
        system.assert(globalMute!=null);
    }
//Initializing 
    private static Account act;
    private static Opportunity oppObj;
    private static cscfga__Product_Basket__c pbasket;
//creating test data
    static void createTestData()
    {       
        //Creating Account object records
        act = new Account(Name='Test112233M',Customer_Type__c='MNC',Selling_Entity__c ='Telstra Incorporated',cscfga__Active__c='Yes',
                Activated__c= True,
                Account_ID__c = '3333',
                Account_Status__c = 'Active',
                Customer_Legal_Entity_Name__c = 'Test');
        insert act;
        //Creating Opportunity object records
         oppObj = new Opportunity(Name='GFTS Ph 2',AccountID=act.Id,Opportunity_Type__c='Simple',
                                           CurrencyIsoCode = 'USD', CloseDate = Date.today(),StageName='Identify & Define',Stage__c='Identify & Define',
                                            QuoteStatus__c ='Approved',Sales_Status__c= 'Won',Win_Loss_Reasons__c ='Product',
                                            Order_Type__c= 'New', ContractTerm__c = '24', Product_Type__c = 'Test', Estimated_MRC__c = 100.00 , Estimated_NRC__c = 100.00
                                            );
        //insert oppObj; 
        //insert contact object records
         Contact contObj = new Contact(AccountId=act.Id,LastName='tech',
                                  email='test@gmail.com');
        insert contObj; 
        //insert Product Basket object record
        pbasket = new cscfga__Product_Basket__c();
        pbasket.csbb__Account__c = act.id;
        pbasket.csordtelcoa__Account__c = act.id;
        pbasket.cscfga__Opportunity__c = oppObj.id;
        insert pbasket;
        system.assert(pbasket!=null);
    }
    
    private static testmethod void  customMultiEditPositiveTestMethod()
    {
        Profile profObj = [select Id from Profile  where name ='Standard User'];
        User userObj = new User(profileId = profObj.id, username = 'test@telstra.com',
            email = 'test@telstra.com', emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
            alias='nuser1',  lastname='lastname1',EmployeeNumber='123456789',Region__c='Australia');
            insert userObj;
            system.assert(userObj!=null);
        System.RunAs(userObj)
        {
            disableAll(userObj.Id);
            createTestData();
            enableAll(userObj.Id);
            Test.startTest();
            CustomButtonMultiEdit cme = new CustomButtonMultiEdit();
            cme.performAction(pbasket.Id);
            Test.stopTest();
        }   
    }
  /*   private static testmethod void  customMultiEditNegativeTestMethod()
    {
        Profile profObj = [select Id from Profile  where name ='Standard User'];
        Profile profObj1 = [select Id from Profile  where name ='System Administrator'];
        User userObj = new User(profileId = profObj.id, username = 'test@telstra.com',
            email = 'test@telstra.com', emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
            alias='nuser1',  lastname='lastname1',EmployeeNumber='123456789',Region__c='Australia');
            insert userObj;
        User userObj1 = new User(profileId = profObj1.id, username = 'test1@telstra.com',
            email = 'test1@telstra.com', emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
            alias='nuser11',  lastname='lastname1',EmployeeNumber='1234567892',Region__c='Australia');
            insert userObj1;
        System.RunAs(userObj)
        {
            disableAll(userObj.Id);
            createTestData();
            act.OwnerId=userObj1.Id;
            update act;
            oppObj.OwnerId=userObj1.Id;
            update oppObj;
            enableAll(userObj.Id);
            CustomButtonMultiEdit cme = new CustomButtonMultiEdit();
            cme.performAction(pbasket.Id);
        }   
    }*/ // commenting negative test scenario as it cannot be covered because of the validation on  Account not to change owner id
}