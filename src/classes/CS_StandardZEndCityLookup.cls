global with sharing class CS_StandardZEndCityLookup extends cscfga.ALookupSearch {
    
    public override String getRequiredAttributes(){ 
        return '[ "A-End Country", "Z-End Country","IPL","EPL","EPLX","ICBS","EVPL" ]';
    }
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){

        Set <Id> popIds = new Set<Id>();
        String aEndCountry = searchFields.get('A-End Country');
        String zEndCountry = searchFields.get('Z-End Country');
        List <String> ProductTypeNames = new List<String>();
        
        for (String key : searchFields.keySet()){
            String attValue = searchFields.get(key);
            if(attValue == 'Yes' || attValue == 'true')
                ProductTypeNames.add(key);
        }
        /*List<CS_Route_Segment__c> routeSegList = [SELECT Id, A_End_City__c,A_End_City__r.CS_Country__c,Z_End_City__c,Z_End_City__r.CS_Country__c
                                                  FROM CS_Route_Segment__c
                                                  WHERE Product_Type__c in :ProductTypeNames 
                                                        AND A_End_City__r.CS_Country__c = :aEndCountry 
                                                        AND Z_End_City__r.CS_Country__c = :zEndCountry
                                                      ];*/

        for(CS_Route_Segment__c item : [SELECT Id, A_End_City__c,A_End_City__r.CS_Country__c,Z_End_City__c,Z_End_City__r.CS_Country__c
                                                  FROM CS_Route_Segment__c
                                                  WHERE Product_Type__c in :ProductTypeNames 
                                                        AND A_End_City__r.CS_Country__c = :aEndCountry 
                                                        AND Z_End_City__r.CS_Country__c = :zEndCountry
                                                      ]){
            popIds.add(item.Z_End_City__c);
        }
        System.Debug('doLookupSearch');
        System.Debug(searchFields);
        String searchValue = searchFields.get('searchValue') +'%';
        List <CS_City__c> data = [SELECT Id, Name FROM CS_City__c WHERE  Id IN :popIds AND Name LIKE :searchValue ORDER BY Name];
        System.Debug(data);
        return data;   
    }

}