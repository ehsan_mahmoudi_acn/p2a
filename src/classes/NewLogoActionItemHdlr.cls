public with sharing class NewLogoActionItemHdlr 
{
   
    String profName=[Select Id,Name from Profile where Id=:userinfo.getProfileId()].Name;
    public  map<Id,User> usrMap{get;set;} 
        
    /*RecordType rtNewLogo = [select Id,Name from RecordType where Name=: Label.Logo_New_Logo and 
                                         SobjectType = : Label.Action_Item_c limit 1];
    RecordType rtWinBack = [select Id,Name from RecordType where Name=:Label.Logo_Win_Back and 
                                        SobjectType = : Label.Action_Item_c limit 1]; 
    
    RecordType rtNewLogoForMP = [select Id,Name from RecordType where Name=:Label.New_Logo_for_Manual_Process and 
                                         SobjectType = : Label.Action_Item_c limit 1];
    RecordType rtWinBackForMP = [select Id,Name from RecordType where Name=:Label.Win_Back_for_Manual_Process and 
                                         SobjectType = : Label.Action_Item_c limit 1];*/
    
    Id rtNewLogoId = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'New Logo');
    Id rtWinBackId = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Win Back');
    Id rtNewLogoForMPId = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'New Logo for Manual Process');
    Id rtWinBackForMPId  = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType, 'Win Back for Manual Process');                                        
    
    String rtNewLogoName =  RecordTypeUtil.getRecordTypeNameById(Action_Item__c.SObjectType, rtNewLogoId );
    String rtWinBackName =  RecordTypeUtil.getRecordTypeNameById(Action_Item__c.SObjectType, rtWinBackId );
    String rtNewLogoForMPName =  RecordTypeUtil.getRecordTypeNameById(Action_Item__c.SObjectType, rtNewLogoForMPId );
    String rtWinBackForMPName =  RecordTypeUtil.getRecordTypeNameById(Action_Item__c.SObjectType, rtWinBackForMPId );
    
    public Account accObjForAut;
    public Account accObjForMP;
    public QueueSobject actItmQueues;
	
    Map<String,NewLogo__c> newLogoCustMap = NewLogo__c.getAll();
    
    /*QueueSobject actItmQueues =[Select Id, SobjectType, QueueId, Queue.Name  From QueueSobject where SobjectType='Action_Item__c'
                                                AND Queue.Name = 'NewWinBackCustomer' LIMIT 1];*/
												
    
    public void befInstActItmHdlrMthd(List<Action_Item__c> trigNew,boolean isBefore,boolean isInsert)
    {
        Set<Id> accountId = new Set<Id>(); 
        Set<Id> accIdForMP = new Set<Id>();
        Set<Id> createdIDs = new Set<Id>(); 
        
		if(CheckRecursive.NewLogoActionItemHdlr()) 
        {
            actItmQueues =[Select Id, SobjectType, QueueId, Queue.Name  From QueueSobject where SobjectType='Action_Item__c'
                                                AND Queue.Name = 'NewWinBackCustomer' LIMIT 1];
        }
		
        for(Action_Item__c aiObj : trigNew)
        {
            if(aiObj.Account__c != null    
            && (aiObj.RecordTypeId == rtNewLogoForMPId   || aiObj.RecordTypeId == rtWinBackForMPId )
             && (profName == newLogoCustMap.get('Logo_System_Administrator').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Admin').Action_Item__c
                || profName == newLogoCustMap.get('Logo_TI_Account_Manager').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Manager').Action_Item__c))
            {
                accIdForMP.add(aiObj.Account__c);
                createdIDs.add(aiObj.CreatedById);
                
            } 
            else if (aiObj.Account__c != null && aiObj.Opportunity__c != null 
                 && (aiObj.RecordTypeId == rtNewLogoId || aiObj.RecordTypeId == rtWinBackId))
            {
                accountId.add(aiObj.Account__c);
                createdIDs.add(aiObj.CreatedById);
            }   
          
        }
        
        if(createdIDs!=null) 
         usrMap = new map<Id,User>([Select Id,Name,email From User Where Id In : createdIDs]); 
      
       // QueueSobject actItmQueues =[Select Id, SobjectType, QueueId, Queue.Name  From QueueSobject where SobjectType='Action_Item__c'
           //                                     AND Queue.Name = 'NewWinBackCustomer' LIMIT 1];
        
        if(accIdForMP.size() > 0) 
        {   
             accObjForMP =[Select Id,Name,Type,New_Logo__c,Created_By_For_New_Customer__c,OwnerId,Created_By_For_Win_Back_Customer__c,
                                    Account_ID__c, Last_Billed_Date__c from Account where ID IN : accIdForMP] ;
        }
        
        else if(accountId.size() > 0 )
        {   
             accObjForAut =[Select Id,Name,Type,New_Logo__c,Created_By_For_New_Customer__c,OwnerId,Created_By_For_Win_Back_Customer__c,
                                    Account_ID__c, Last_Billed_Date__c from Account where ID IN : accountId] ;
        }
        
        if(accountId.size() > 0)
        {
            for(Action_Item__c ai : trigNew)
            {
                if(ai.Id == trigNew[0].Id && ai.Status__c == newLogoCustMap.get('Logo_Assigned').Action_Item__c
                  && (ai.Subject__c == newLogoCustMap.get('Logo_New_Customer').Action_Item__c || ai.Subject__c == newLogoCustMap.get('Logo_Win_Back_Customer').Action_Item__c))
                {
                    ai.Acc_Owner_Created_By__c = accObjForAut.OwnerId;
                }  
            }
        }
         if(accIdForMP.size() > 0  
          && ( rtNewLogoForMPName == newLogoCustMap.get('New_Logo_for_Manual_Process').Action_Item__c || rtWinBackForMPName == newLogoCustMap.get('Win_Back_for_Manual_Process').Action_Item__c))
         
          {
            for(Action_Item__c ai : trigNew) 
            {    
                if(ai.Id == trigNew[0].Id && ai.Status__c == newLogoCustMap.get('Logo_Assigned').Action_Item__c
                   && ai.Subject_For_NewLogo__c == newLogoCustMap.get('Logo_New_Customer').Action_Item__c
                   && (profName == newLogoCustMap.get('Logo_System_Administrator').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Admin').Action_Item__c
                       || profName == newLogoCustMap.get('Logo_TI_Account_Manager').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Manager').Action_Item__c))  
                {
                   ai.OwnerId = actItmQueues.Queue.Id;
                   ai.Subject__c=ai.Subject_For_NewLogo__c;
                   ai.RecordTypeId = rtNewLogoForMPId;
                   ai.Acc_Owner_Created_By__c = accObjForMP.OwnerId;
                }
                else  if(ai.Id == trigNew[0].Id && ai.Status__c == newLogoCustMap.get('Logo_Assigned').Action_Item__c  && ai.Subject_For_NewLogo__c == newLogoCustMap.get('Logo_Win_Back_Customer').Action_Item__c
                 && (profName == newLogoCustMap.get('Logo_System_Administrator').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Admin').Action_Item__c 
                     || profName == newLogoCustMap.get('Logo_TI_Account_Manager').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Manager').Action_Item__c))  
                {
                   ai.OwnerId = actItmQueues.Queue.Id;
                   ai.Subject__c=ai.Subject_For_NewLogo__c;
                   ai.RecordTypeId = rtWinBackForMPId;
                   ai.Acc_Owner_Created_By__c = accObjForMP.OwnerId;
                }
                
                else if (ai.Id == trigNew[0].Id && ai.Status__c == newLogoCustMap.get('Logo_Approved').Action_Item__c
                  && ai.Subject_For_NewLogo__c == newLogoCustMap.get('Logo_New_Customer').Action_Item__c
                 && (profName == newLogoCustMap.get('Logo_System_Administrator').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Admin').Action_Item__c  
                     || profName == newLogoCustMap.get('Logo_TI_Sales_Manager').Action_Item__c))
                {
                    ai.OwnerId = actItmQueues.Queue.Id;
                    ai.RecordTypeId = rtNewLogoForMPId;
                    ai.Subject__c=ai.Subject_For_NewLogo__c;
                   ai.Acc_Owner_Created_By__c = accObjForMP.OwnerId;
                   
                }
                else if (ai.Id == trigNew[0].Id && ai.Status__c == newLogoCustMap.get('Logo_Approved').Action_Item__c
                  && ai.Subject_For_NewLogo__c == newLogoCustMap.get('Logo_Win_Back_Customer').Action_Item__c
                  && (profName == newLogoCustMap.get('Logo_System_Administrator').Action_Item__c  || profName == newLogoCustMap.get('Logo_TI_Sales_Admin').Action_Item__c  
                  || profName == newLogoCustMap.get('Logo_TI_Sales_Manager').Action_Item__c ))  
                {
                    ai.OwnerId = actItmQueues.Queue.Id;
                    ai.RecordTypeId = rtWinBackForMPId;
                    ai.Subject__c=ai.Subject_For_NewLogo__c;
                   ai.Acc_Owner_Created_By__c = accObjForMP.OwnerId;
                   
                }
                
            }   
          } 
        
    }
    
    public void aftInstActItmHdlrMthd(List<Action_Item__c> trigNew,boolean isBefore,boolean isUpdate)
    {
        Set<Id> accountId = new Set<Id>(); 
        Set<Id> createdIDs = new Set<Id>(); 
        Set<Id> accIdForApp = new Set<Id>();
        
        for(Action_Item__c aiObj : trigNew)
        {
            if(aiObj.Account__c != null    
            && (aiObj.RecordTypeId == rtNewLogoForMPId   || aiObj.RecordTypeId == rtWinBackForMPId
               || aiObj.RecordTypeId == rtNewLogoId  || aiObj.RecordTypeId == rtWinBackId)
             && (profName == newLogoCustMap.get('Logo_System_Administrator').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Admin').Action_Item__c
                || profName == newLogoCustMap.get('Logo_TI_Account_Manager').Action_Item__c|| profName == newLogoCustMap.get('Logo_TI_Sales_Manager').Action_Item__c) && aiObj.Status__c == newLogoCustMap.get('Logo_Assigned').Action_Item__c)
            {
                accountId.add(aiObj.Account__c);
                createdIDs.add(aiObj.CreatedById);
                
            }    
            else if(aiObj.Account__c != null    
            && (aiObj.RecordTypeId == rtNewLogoForMPId   || aiObj.RecordTypeId == rtWinBackForMPId)
                && (profName == newLogoCustMap.get('Logo_System_Administrator').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Admin').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Manager').Action_Item__c)
                 && aiObj.Status__c == newLogoCustMap.get('Logo_Approved').Action_Item__c)
            {
                accIdForApp.add(aiObj.Account__c);
                createdIDs.add(aiObj.CreatedById);
                
            }      
        }
        
         if(createdIDs!=null) 
         {    
            usrMap = new map<Id,User>([Select Id,Name,email From User Where Id In : createdIDs]); 
            
         } 
         // QueueSobject actItmQueues =[Select Id, SobjectType, QueueId, Queue.Name  From QueueSobject where SobjectType='Action_Item__c'
           //                          AND Queue.Name = 'NewWinBackCustomer' LIMIT 1];
           
         if(accountId.size() > 0)
         {
			List<Account> accObjlist = new List<Account>(); 
            Account accObj =[Select Id,Name,Type,New_Logo__c,Created_By_For_New_Customer__c, OwnerId,
                                    Account_ID__c, Last_Billed_Date__c from Account where ID IN : accountId] ;  
            for(Action_Item__c ai : trigNew)
            {    
                 if(ai.Id == trigNew[0].Id && ai.Status__c == 'Assigned' && !usrMap.isEmpty()
                    && (ai.Subject__c == newLogoCustMap.get('Logo_New_Customer').Action_Item__c || ai.Subject_For_NewLogo__c == newLogoCustMap.get('Logo_New_Customer').Action_Item__c))
                 {
                     //accObj.Created_By_For_New_Customer__c= accObj.OwnerId;
                     Util.notAIForNewLogo = false; 
					 accObjlist.add(accObj);
                     //update accObj;
                 }
                else if(ai.Id == trigNew[0].Id && ai.Status__c == newLogoCustMap.get('Logo_Assigned').Action_Item__c && !usrMap.isEmpty()
                    && (ai.Subject__c == newLogoCustMap.get('Logo_Win_Back_Customer').Action_Item__c || ai.Subject_For_NewLogo__c == newLogoCustMap.get('Logo_Win_Back_Customer').Action_Item__c))
                 {
                     
                    // accObj.Created_By_For_Win_Back_Customer__c= accObj.OwnerId;
                     Util.notAIForNewLogo = false; 
					 accObjlist.add(accObj);
                     //update accObj;
                 }
            } 
			update accObjlist;
         }   
         else if(accIdForApp.size() > 0)
         {
            List<Account> accObjlist = new List<Account>(); 
			Account accObj =[Select Id,Name,Type,New_Logo__c,Created_By_For_New_Customer__c,OwnerId,
                                    Account_ID__c, Last_Billed_Date__c from Account where ID IN : accIdForApp] ; 
            for(Action_Item__c ai : trigNew)
            {    
                             
                 if(ai.Id == trigNew[0].Id && ai.Status__c == newLogoCustMap.get('Logo_Approved').Action_Item__c && !usrMap.isEmpty()
                    && ai.Subject_For_NewLogo__c == newLogoCustMap.get('Logo_New_Customer').Action_Item__c) 
                 {
                     accObj.Created_By_For_New_Customer__c= accObj.OwnerId;
                     Util.notAIForNewLogo = false;
					 accObjlist.add(accObj);
                     //update accObj;
                 }
                
                else if(ai.Id == trigNew[0].Id && ai.Status__c == newLogoCustMap.get('Logo_Approved').Action_Item__c && !usrMap.isEmpty()
                    && ai.Subject_For_NewLogo__c == newLogoCustMap.get('Logo_Win_Back_Customer').Action_Item__c) 
                 {
                     accObj.Created_By_For_Win_Back_Customer__c= accObj.OwnerId;
                     Util.notAIForNewLogo = false;
					 accObjlist.add(accObj);
                     //update accObj;
                 }
            }
            update accObjlist; 
         }
        
         
        
    }   
    
     //Before Update Method for New Logo / Win Back Action Item Handler Method
    public void befUpdActItmHdlrMthd(List<Action_Item__c> trigNew,boolean isBefore,boolean isUpdate)
    {
        Set<Id> accountId = new Set<Id>(); 
        Set<Id> accIdForMP = new Set<Id>();
        Set<Id> lastModifiedIDs = new Set<Id>(); 
        
        
        for(Action_Item__c aiObj : trigNew)
        {       
            if(aiObj.Account__c != null && aiObj.Opportunity__c != null  
            && (aiObj.RecordTypeId == rtNewLogoId || aiObj.RecordTypeId == rtWinBackId)
             && (profName == newLogoCustMap.get('Logo_System_Administrator').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Admin').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_HR').Action_Item__c 
             || profName == newLogoCustMap.get('Logo_TI_Sales_Manager').Action_Item__c)
            &&(aiObj.Status__c == newLogoCustMap.get('Logo_Approved').Action_Item__c || aiObj.Status__c == newLogoCustMap.get('Logo_Rejected').Action_Item__c) 
            && (aiObj.Subject__c == newLogoCustMap.get('Logo_New_Customer').Action_Item__c || aiObj.Subject__c == newLogoCustMap.get('Logo_Win_Back_Customer').Action_Item__c) )
            {
                accountId.add(aiObj.Account__c);
                lastModifiedIDs.add(aiObj.LastModifiedById); 
            }  
            else if(aiObj.Account__c != null     
            && (aiObj.RecordTypeId == rtNewLogoForMPId  || aiObj.RecordTypeId == rtWinBackForMPId)
               &&  (profName == newLogoCustMap.get('Logo_System_Administrator').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Admin').Action_Item__c
                    || profName == newLogoCustMap.get('Logo_TI_HR').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Manager').Action_Item__c )
                &&(aiObj.Status__c == newLogoCustMap.get('Logo_Approved').Action_Item__c || aiObj.Status__c == newLogoCustMap.get('Logo_Rejected').Action_Item__c
                  || aiObj.Status__c == newLogoCustMap.get('Logo_Assigned').Action_Item__c))
            {
                
                accIdForMP.add(aiObj.Account__c);
                lastModifiedIDs.add(aiObj.LastModifiedById); 
            }  
            
       }
          
       
       if(lastModifiedIDs !=null) 
         usrMap = new map<Id,User>([Select Id,Name,email From User Where Id In : lastModifiedIDs]); 
        
      if(accIdForMP.size() > 0 
         && (rtNewLogoForMPName == newLogoCustMap.get('New_Logo_for_Manual_Process').Action_Item__c || rtWinBackForMPName == newLogoCustMap.get('Win_Back_for_Manual_Process').Action_Item__c)
         &&(profName == newLogoCustMap.get('Logo_System_Administrator').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Admin').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_HR').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Manager').Action_Item__c))
      {
            for(Action_Item__c ai : trigNew) 
            {    
                
                if(ai.Id == trigNew[0].Id && ai.Subject_For_NewLogo__c == newLogoCustMap.get('Logo_New_Customer').Action_Item__c
                  &&( ai.Status__c == newLogoCustMap.get('Logo_Approved').Action_Item__c ||ai.Status__c == newLogoCustMap.get('Logo_Rejected').Action_Item__c  ))  
                {
                    ai.Subject__c = ai.Subject_For_NewLogo__c;
                    ai.RecordTypeId = rtNewLogoForMPId;
                    ai.Completion_Date__c = System.today();
                    ai.Completed_By__c = usrMap.get(ai.LastModifiedById).name; 
                    
                    
                }
                else if(ai.Id == trigNew[0].Id && ai.Subject_For_NewLogo__c == newLogoCustMap.get('Logo_Win_Back_Customer').Action_Item__c
                      &&( ai.Status__c == newLogoCustMap.get('Logo_Approved').Action_Item__c ||ai.Status__c == newLogoCustMap.get('Logo_Rejected').Action_Item__c  ))  
                {
                    ai.Subject__c = ai.Subject_For_NewLogo__c;
                    ai.RecordTypeId = rtWinBackForMPId;
                    ai.Completion_Date__c = System.today();
                    ai.Completed_By__c = usrMap.get(ai.LastModifiedById).name; 
                    
                }
                else if(ai.Id == trigNew[0].Id && ai.Subject_For_NewLogo__c == newLogoCustMap.get('Logo_New_Customer').Action_Item__c
                  && ai.Status__c == newLogoCustMap.get('Logo_Assigned').Action_Item__c )  
                {
                    ai.Subject__c = ai.Subject_For_NewLogo__c;
                    ai.RecordTypeId = rtNewLogoForMPId;
                }
                else if(ai.Id == trigNew[0].Id && ai.Subject_For_NewLogo__c == newLogoCustMap.get('Logo_Win_Back_Customer').Action_Item__c
                  && ai.Status__c == newLogoCustMap.get('Logo_Assigned').Action_Item__c)  
                {
                    ai.Subject__c = ai.Subject_For_NewLogo__c;
                    ai.RecordTypeId = rtWinBackForMPId;
                }
            }   
      }   
        
            
      else if(accountId.size() > 0 
          && (rtNewLogoName == newLogoCustMap.get('Logo_New_Logo').Action_Item__c || rtWinBackName == newLogoCustMap.get('Logo_Win_Back').Action_Item__c)
         &&  (profName == newLogoCustMap.get('Logo_System_Administrator').Action_Item__c  || profName == newLogoCustMap.get('Logo_TI_Sales_Admin').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_HR').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Manager').Action_Item__c )) 
      {    
         
        for(Action_Item__c ai : trigNew) 
        { 
            if(ai.Id == trigNew[0].Id && ai.Status__c == newLogoCustMap.get('Logo_Approved').Action_Item__c
              && (ai.Subject__c == newLogoCustMap.get('Logo_New_Customer').Action_Item__c || ai.Subject__c == newLogoCustMap.get('Logo_Win_Back_Customer').Action_Item__c) )
            {    
        
                ai.Completion_Date__c = System.today();
                ai.Completed_By__c = usrMap.get(ai.LastModifiedById).name; 
                
            } 
           else if(ai.Id == trigNew[0].Id && ai.Status__c == newLogoCustMap.get('Logo_Rejected').Action_Item__c && ai.Comments__c  != null
                   && (ai.Subject__c == newLogoCustMap.get('Logo_New_Customer').Action_Item__c || ai.Subject__c == newLogoCustMap.get('Logo_Win_Back_Customer').Action_Item__c))
            {    
                ai.Completion_Date__c = System.today();
                ai.Completed_By__c = usrMap.get(ai.LastModifiedById).name; 
              
                
            }
            
        }
      }  
    }
    // End of befUpdActItmHdlrMthd for 
    
    // After Update Method for New Logo / Win Back Action Item Handler Method
    public void aftUpdActItmHdlrMthd(List<Action_Item__c> trigNew,boolean isAfter,boolean isUpdate) 
    {
        Set<Id> accountId = new Set<Id>(); 
        Set<Id> lastModifiedIDs = new Set<Id>(); 
        
        
        for(Action_Item__c aiObj : trigNew)
        {       
            if(aiObj.Account__c != null 
             && (aiObj.RecordTypeId == rtNewLogoId || aiObj.RecordTypeId == rtNewLogoForMPId
                || aiObj.RecordTypeId == rtWinBackId || aiObj.RecordTypeId == rtWinBackForMPId)
              && (profName == newLogoCustMap.get('Logo_System_Administrator').Action_Item__c  || profName == newLogoCustMap.get('Logo_TI_Sales_Admin').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_HR').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Manager').Action_Item__c))
                //&& (aiObj.Status__c ==Label.Logo_Approved || aiObj.Status__c ==Label.Logo_Rejected)) 
            {
                accountId.add(aiObj.Account__c);
                lastModifiedIDs.add(aiObj.LastModifiedById); 
            }
            
       }
        
            if(lastModifiedIDs !=null && CheckRecursive.NewLogoActionItemHdlr2()) 
         usrMap = new map<Id,User>([Select Id,Name,email From User Where Id In : lastModifiedIDs LIMIT 1]);  

       if(accountId.size() > 0 && accountId != null && CheckRecursive.NewLogoActionItemHdlr1() 
      && (rtNewLogoName == newLogoCustMap.get('Logo_New_Logo').Action_Item__c || rtNewLogoForMPName  == newLogoCustMap.get('New_Logo_for_Manual_Process').Action_Item__c
        || rtWinBackName == newLogoCustMap.get('Logo_Win_Back').Action_Item__c || rtWinBackForMPName  == newLogoCustMap.get('Win_Back_for_Manual_Process').Action_Item__c )
           && (profName == newLogoCustMap.get('Logo_System_Administrator').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Admin').Action_Item__c || 
           profName == newLogoCustMap.get('Logo_TI_HR').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Sales_Manager').Action_Item__c)) 
      {    
        List<Account> accObjlist = new List<Account>(); 
		Account accObj =[Select Id,Name,New_Logo__c,Win_Back__c,Completion_Date_For_New_Customer__c,Completion_Date_For_WinBack_Customer__c,
                                 Account_ID__c, Last_Billed_Date__c,OwnerId from Account where ID IN : accountId] ; 
          
       // 'IF Condition' to check if Action Item status is Approved and record type is New Logo 
       for(Action_Item__c ai : trigNew)
       {    
            
           if(ai.Id == trigNew[0].Id && ai.Status__c == newLogoCustMap.get('Logo_Assigned').Action_Item__c && !usrMap.isEmpty()
             && (ai.RecordTypeId == rtWinBackId || ai.RecordTypeId == rtWinBackForMPId) 
              && (ai.Subject__c == newLogoCustMap.get('Logo_Win_Back_Customer').Action_Item__c || ai.Subject_For_NewLogo__c ==newLogoCustMap.get('Logo_Win_Back_Customer').Action_Item__c))
            {     
              //  accObj.Last_Billed_Date__c = ai.Last_Billed_Date__c;
                Util.notAIForNewLogo = false;
				accObjlist.add(accObj);
                //update accObj;
            }
            
           else if(ai.Id == trigNew[0].Id && ai.Status__c == newLogoCustMap.get('Logo_Approved').Action_Item__c
                   && ai.Subject__c == newLogoCustMap.get('Logo_New_Customer').Action_Item__c && !usrMap.isEmpty())
            {   
                accObj.New_Logo__c = true;
                accObj.Completion_Date_For_New_Customer__c = ai.Completion_Date__c;
                accObj.Created_By_For_New_Customer__c = ai.Acc_Owner_Created_By__c;
                Util.notAIForNewLogo = false;
				accObjlist.add(accObj);
                //update accObj;
            }
      
            else if(ai.Id == trigNew[0].Id && ai.Status__c == newLogoCustMap.get('Logo_Approved').Action_Item__c &&  ai.Subject__c == newLogoCustMap.get('Logo_Win_Back_Customer').Action_Item__c
            && ai.Last_Billed_Date__c != null) 
            {    
                accObj.Win_Back__c = true;
                accObj.Created_By_For_Win_Back_Customer__c = ai.Acc_Owner_Created_By__c;
                accObj.Completion_Date_For_WinBack_Customer__c = ai.Completion_Date__c;
                accObj.Last_Billed_Date__c = ai.Last_Billed_Date__c;
                Util.notAIForNewLogo = false;
				accObjlist.add(accObj);
                //update accObj;
            }
            else if(ai.Id == trigNew[0].Id && ai.Status__c == newLogoCustMap.get('Logo_Approved').Action_Item__c &&  ai.Subject__c == newLogoCustMap.get('Logo_Win_Back_Customer').Action_Item__c)
            {    
                accObj.Win_Back__c = true;
                accObj.Created_By_For_Win_Back_Customer__c = ai.Acc_Owner_Created_By__c;
                accObj.Completion_Date_For_WinBack_Customer__c = ai.Completion_Date__c;
                Util.notAIForNewLogo = false;
				accObjlist.add(accObj);
                //update accObj;
            }      
      
        // 'IF Condition'to check if Action Item status is Rejected and record type is New Logo  
            
            else if(ai.Id == trigNew[0].Id && ai.Status__c ==newLogoCustMap.get('Logo_Rejected').Action_Item__c &&  ai.Subject__c == newLogoCustMap.get('Logo_New_Customer').Action_Item__c)
            {    
                accObj.New_Logo__c = false;
                accObj.Created_By_For_New_Customer__c = null;
                accObj.Completion_Date_For_New_Customer__c = null;
                Util.notAIForNewLogo = false;
				accObjlist.add(accObj);
                //update accObj;
                
            }  
            else if(ai.Id == trigNew[0].Id && ai.Status__c == newLogoCustMap.get('Logo_Rejected').Action_Item__c &&  ai.Subject__c == newLogoCustMap.get('Logo_Win_Back_Customer').Action_Item__c)
            {    
                accObj.Win_Back__c = false;
                accObj.Completion_Date_For_WinBack_Customer__c = null;
                accObj.Created_By_For_Win_Back_Customer__c = null;
                Util.notAIForNewLogo = false;
				accObjlist.add(accObj);
                //update accObj;
            }  
                   
      }
	 Update accObjlist; 
    } 
   }
}