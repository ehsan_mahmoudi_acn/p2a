public class EmailUtility {

       private Messaging.SingleEmailMessage singleEmailMessage;

       private final List<String> toAddresses;

        

       //optional parameters set to default       

       private String subject = '';

       private String htmlBody = '';
       
       private List<String> CcAddresses;

       private Boolean useSignature = false;

       private List<Messaging.EmailFileAttachment> fileAttachments = null;

       //defaults to current user's first name + last name

       private String senderDisplayName = UserInfo.getFirstName()+' '+UserInfo.getLastName();

       //get the current user in context

       User currentUser = [Select email from User where username = :UserInfo.getUserName() limit 1];       

       //replyTo defaults to current user's email

       private String replyTo = currentUser.email;

       private String plainTextBody = '';

        

       public EmailUtility(List<String> addresses) {

           this.toAddresses = addresses;

       }

        

       public EmailUtility ccAddresses(List<String> valList) {

           CcAddresses = valList;

           return this;

       }
       public EmailUtility senderDisplayName(String val) {

           senderDisplayName = val;

           return this;

       }
    
        

       public EmailUtility subject(String val) {

           subject = val;

           return this;

       }

        

       public EmailUtility htmlBody(String val) {

           htmlBody = val;

           return this;

       }

        

       public EmailUtility useSignature(Boolean bool) {

           useSignature = bool;

           return this;

       }

        

       public EmailUtility replyTo(String val) {

           replyTo = val;

           return this;

       }

        

       public EmailUtility plainTextBody(String val) {

           plainTextBody = val;

           return this;

       }

        

       public EmailUtility fileAttachments(List<Messaging.Emailfileattachment> val) {

           fileAttachments = val;

           return this;

       }

        

       //where it all comes together

       //this method is private and is called from sendEmail()

       private EmailUtility build() {

           singleEmailMessage = new Messaging.SingleEmailMessage();

           singleEmailMessage.setToAddresses(this.toAddresses);

           singleEmailMessage.setSenderDisplayName(this.senderDisplayName);

           singleEmailMessage.setSubject(this.subject);

           singleEmailMessage.setHtmlBody(this.htmlBody);
           if(this.CcAddresses!=null){
           singleEmailMessage.setCcAddresses(this.CcAddresses);
           }
           singleEmailMessage.setUseSignature(this.useSignature);

           singleEmailMessage.setReplyTo(this.replyTo);

           singleEmailMessage.setPlainTextBody(this.plainTextBody);

           singleEmailMessage.setFileAttachments(this.fileAttachments);

           return this;

       }

        

       //send the email message

       public void sendEmail() {

              try {

                  //call build first to create the email message object

                  build();

                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { singleEmailMessage });

              } catch (Exception ex) {

               ErrorHandlerException.ExecutingClassName='EmailUtility:sendEmail';
               ErrorHandlerException.sendException(ex);
               // throw new GenericException('There was a problem while calling Messaging.sendEmail()');

              }               

       }   

   public class GenericException extends Exception{
        

}
    public String getEmailAddess(String accountRegion){
 // query to get all queues corresponding to Bill profile Object
          Map<String, QueueSobject> QueueMap = QueueUtil.getQueuesMapForObject(Action_Item__c.SObjectType);
            
            //Query the User object  to get sales User who created the Order
           
             Id queid;
            String emailAddresses = '';
            String USerRegion='';
            //Setting for email attributes
            Map<String, Id> queMap= new Map<String, Id> ();
            
            //Get Billing Team Queue id corresponding to sales user region.
            for (QueueSObject q:QueueMap.values()){
                if((q.Queue.Name).contains(accountRegion)){
                    queMap.put(q.Queue.Name,q.QueueId);
                    queid = q.QueueId;
                   // System.debug('queue name ..'+q.Queue.Name);
                }
            }
            String groupMemberQuery = 'SELECT GroupId, Id, SystemModstamp, UserOrGroupId FROM GroupMember where GroupId =\''+queid+'\'';
            
            //Getting the all Users id's  corresponding to queue
            List<GroupMember> list_GM = Database.query(groupMemberQuery);
            
            List<Id> lst_Ids = new List<ID>();
            for(GroupMember gmObj : list_GM){
                lst_Ids.add(gmObj.UserOrGroupId);
            }
            
            //Query the Billing Team Users 
            //List<User> lst_UserObj = [Select Id,name,Email,Region__c from User where id in :lst_Ids];
            
            //Here concanating all email addresses of Users belong to Billing Queue
            for(User usrObj : [Select Id,name,Email,Region__c from User where id in :lst_Ids]){
                if(emailAddresses == ''){
                    emailAddresses = usrObj.Email;
                     USerRegion=usrObj.Region__c;
                }else{
                    emailAddresses += ':'+usrObj.Email; 
                     USerRegion=usrObj.Region__c;
                }
            }
            

           System.debug('emailadresses..'+emailAddresses);
           return emailAddresses;
    
    }
         

   }