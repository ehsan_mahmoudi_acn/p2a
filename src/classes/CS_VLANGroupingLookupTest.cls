@isTest(SeeAllData = false)
private class CS_VLANGroupingLookupTest {

    private static Map<String, String> searchFields = new Map<String, String>();
    private static List<cscfga__Attribute__c> searchFieldAttributes;
    private static String productDefinitionId;
    private static Id[] excludeIds;
    private static Integer pageOffset, pageLimit;
    
    private static List<cscfga__Product_Basket__c> basketList;
    private static List<cscfga__Product_Definition__c> productDefinitionList;
    private static List<cscfga__Product_Configuration__c> vlansList;
    private static List<cscfga__Product_Configuration__c> parentConfigList;
    private static List<cscfga__Product_Configuration__c> masterServiceList;
    private static List<cscfga__Attribute__c> vlanGroupList;
        
    private static void initTestData(){
        
        basketList = new List<cscfga__Product_Basket__c>{
            new cscfga__Product_Basket__c(Name = 'Basket 1')
        };
        
        insert basketList;
        System.debug('**** Basket List: ' + basketList);
        List<cscfga__Product_Basket__c> basList = [select name from cscfga__Product_Basket__c where name = 'Basket 1'];
        system.assert(basList!=null);
        system.assertEquals(basList [0].name,basketList[0].name);
          
        
        productDefinitionList = new List<cscfga__Product_Definition__c>{
                  new cscfga__Product_Definition__c(Name = 'VLAN', cscfga__Description__c = 'VLAN'),
                  new cscfga__Product_Definition__c(Name = 'VPLS VLAN Port', cscfga__Description__c = 'VPLS VLAN Port'),
                  new cscfga__Product_Definition__c(Name = 'Master VPLS Service', cscfga__Description__c = 'Master VPLS Service')
            };   
            
        insert productDefinitionList;
        System.debug('**** Product Definitions: ' + productDefinitionList);
        List<cscfga__Product_Definition__c> prodList = [select name,cscfga__Description__c  from cscfga__Product_Definition__c where name = 'VLAN'];
        system.assert(prodList!=null);
        system.assertEquals(prodList[0].name,productDefinitionList[0].name);
          
        
        masterServiceList = new List<cscfga__Product_Configuration__c>{
            new cscfga__Product_Configuration__c(Name = 'Master VPLS Service', cscfga__Product_Definition__c = productDefinitionList[2].Id, cscfga__Product_Basket__c = basketList[0].Id)
        };
        
        insert masterServiceList;
        System.debug('**** Master Service List: ' + masterServiceList);
        List<cscfga__Product_Configuration__c> msList = [select name,cscfga__Product_Definition__c from cscfga__Product_Configuration__c where name = 'Master VPLS Service'];
        system.assert(msList!=null);
        system.assertEquals(msList[0].name,masterServiceList[0].name);
        
        parentConfigList = new List<cscfga__Product_Configuration__c>{
            new cscfga__Product_Configuration__c(Name = 'VPLS VLAN Port 1', cscfga__Product_Definition__c = productDefinitionList[1].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id, cscfga__Product_Basket__c = basketList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'VPLS VLAN Port 2', cscfga__Product_Definition__c = productDefinitionList[1].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id, cscfga__Product_Basket__c = basketList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'VPLS VLAN Port 3', cscfga__Product_Definition__c = productDefinitionList[1].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id, cscfga__Product_Basket__c = basketList[0].Id)
        };
        
        insert parentConfigList;
        System.debug('**** Parent Config List: ' + parentConfigList);
        List<cscfga__Product_Configuration__c> pcList = [select name,cscfga__Product_Definition__c from cscfga__Product_Configuration__c where name = 'VPLS VLAN Port 1'];
        system.assert(pcList!=null);
        system.assertEquals(pcList[0].name,parentConfigList[0].name);
        
        vlansList = new List<cscfga__Product_Configuration__c>{
            new cscfga__Product_Configuration__c(Name = 'VLAN 1', cscfga__Product_Definition__c = productDefinitionList[0].Id, cscfga__Parent_Configuration__c = parentConfigList[0].Id, cscfga__Product_Basket__c = basketList[0].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'VLAN 2', cscfga__Product_Definition__c = productDefinitionList[0].Id, cscfga__Parent_Configuration__c = parentConfigList[0].Id, cscfga__Product_Basket__c = basketList[0].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'VLAN 3', cscfga__Product_Definition__c = productDefinitionList[0].Id, cscfga__Parent_Configuration__c = parentConfigList[1].Id, cscfga__Product_Basket__c = basketList[0].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'VLAN 4', cscfga__Product_Definition__c = productDefinitionList[0].Id, cscfga__Parent_Configuration__c = parentConfigList[1].Id, cscfga__Product_Basket__c = basketList[0].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'VLAN 5', cscfga__Product_Definition__c = productDefinitionList[0].Id, cscfga__Parent_Configuration__c = parentConfigList[2].Id, cscfga__Product_Basket__c = basketList[0].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'VLAN 6', cscfga__Product_Definition__c = productDefinitionList[0].Id, cscfga__Parent_Configuration__c = parentConfigList[2].Id, cscfga__Product_Basket__c = basketList[0].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id)
        };
        
        insert vlansList;
        System.debug('**** VLAN List: ' + vlansList);
        List<cscfga__Product_Configuration__c> vlanList = [select name,cscfga__Product_Definition__c from cscfga__Product_Configuration__c where name = 'VLAN 1'];
        system.assert(vlanList!=null);
        system.assertEquals(vlanList[0].name,vlansList[0].name);
        
        vlanGroupList = new List<cscfga__Attribute__c>{
            new cscfga__Attribute__c(Name = 'VLANs', cscfga__Product_Configuration__c = vlansList[0].Id, cscfga__Value__c = vlansList[1].Id),
            new cscfga__Attribute__c(Name = 'VLANs', cscfga__Product_Configuration__c = vlansList[1].Id, cscfga__Value__c = ''),
            new cscfga__Attribute__c(Name = 'VLANs', cscfga__Product_Configuration__c = vlansList[2].Id, cscfga__Value__c = vlansList[3].Id),
            new cscfga__Attribute__c(Name = 'VLANs', cscfga__Product_Configuration__c = vlansList[3].Id, cscfga__Value__c = '')
        };
        
        insert vlanGroupList;
        System.debug('**** VLAN Group List: ' + vlanGroupList);
        List<cscfga__Attribute__c> vlangList = [select name,cscfga__Product_Configuration__c,cscfga__Value__c  from cscfga__Attribute__c where name = 'VLANs'];
        system.assert(vlangList!=null);
        system.assertEquals(vlangList[0].name,vlanGroupList[0].name);
        
    }    
        
    private static testMethod void doLookupSearchTest() {
    Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
                             
            initTestData();  
            
            productDefinitionId = productDefinitionList[0].Id;
            
            searchFields.put('BasketId', basketList[0].Id);
            searchFields.put('VLAN Ports Ids', vlansList[4].Id + ',' + vlansList[5].Id );
            searchFields.put('Product ID', 'true');
            searchFields.put('Max MAC Address', 'true');
            searchFields.put('Master Service Calculated', masterServiceList[0].Id);
            searchFields.put('ConfigId', vlansList[4].Id);
            
                        
            CS_VLANGroupingLookup vlanGroupingLookup = new CS_VLANGroupingLookup();
            String reqAtts = vlanGroupingLookup.getRequiredAttributes();
            Object[] data = vlanGroupingLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            system.assert(vlanGroupingLookup!=null);
            System.debug('*******Data: ' + data);
            //System.assert(data.size() > 0, '');
            
            searchFields.put('Master Service Calculated', '');
            data.clear();
            data = vlanGroupingLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            
            System.debug('*******Data: ' + data);
            //System.assert(data.size() > 0, '');
            
        } catch(Exception e){
            ee = e;
            ErrorHandlerException.ExecutingClassName='CS_VLANGroupingLookupTest:initTestData';      
            ErrorHandlerException.sendException(e);
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }  
    }

}