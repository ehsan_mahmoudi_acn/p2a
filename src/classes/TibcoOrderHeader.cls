public class TibcoOrderHeader {

    public String description = '';
    public String customerID = '';
    public String subscriberID = '';
    public Date requiredByDate = null;
    public Date requiredOnDate = null;
    public Integer orderPriority= null;
    public TibcoOrderAddress invoiceAddress=null;
    public TibcoOrderAddress deliveryAddress=null;
    public String notes = '';
    public String slaID = '';
    public UDF[] udf = null;
    public TibcoOrderHeader()
    {
        
    }
    public TibcoOrderHeader(String description,
                            String customerID,
                            String subscriberID,
                            Date requiredByDate,
                            Date requiredOnDate,
                            Integer orderPriority,
                            TibcoOrderAddress invoiceAddress,
                            TibcoOrderAddress deliveryAddress,
                            String notes,
                            String slaID,
                            UDF[] udf)
    {
        this.description = description;
        this.customerID = customerID;
        this.subscriberID = subscriberID;
        this.requiredByDate = requiredByDate;
        this.requiredOnDate=requiredOnDate;
        this.orderPriority=orderPriority;
        this.invoiceAddress=invoiceAddress;
        this.deliveryAddress=deliveryAddress;
        this.notes = notes;
        this.slaID = slaID;
        this.udf = udf;
    }
         
}