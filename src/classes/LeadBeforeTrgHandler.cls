public with sharing class LeadBeforeTrgHandler 
{
    
    public Id refLeadOwnerId{get;set;}
    public Map<String,Country_Lookup__c> ctryLookupMap = new Map<String,Country_Lookup__c>();
    //List<Country_Lookup__c> ctryLkpList = new List<Country_Lookup__c>();
    public Map<Id,Lead> leadMap = new Map<Id,Lead>();
    Set<String> ctrStrList = new Set<String>();
    //public List<Account> accObjList{get;set;} 
    //public Account accObj{get;set;}   
            
    //Method for handling based on Country Picklist , Country Lookup will update
    public void ctryPickTrgHandler(List<Lead> trigNew,boolean isInsert,boolean isUpdate, boolean isBefore)
    {
        List<Country_Lookup__c> ctryObj = new  List<Country_Lookup__c> ();
        for(Lead leadObj : trigNew)
        {
            if(leadObj.Country_Picklist__c != null)
            {
                ctrStrList.add(leadObj.Country_Picklist__c);
            }
        }
        
        //Checking size value for Country Picklist
        if(ctrStrList.size() >0 )
        {
          //ctryLkpList = [Select Id, Name,Region__c from Country_Lookup__c where Name IN :ctrStrList];
           for(Country_Lookup__c ctryLKPObj : [Select Id, Name,Region__c from Country_Lookup__c where Name IN :ctrStrList])
        {
            ctryLookupMap.put(ctryLKPObj.Name,ctryLKPObj); 
        } 
        }
        
        
        for(Lead leadObj: trigNew)
        {
            if(isInsert && isBefore && ctryLookupMap.containsKey(leadObj.Country_Picklist__c) )
            {
            
                 leadObj.Country_Lookup_Region__c=  ctryLookupMap.get(leadObj.Country_Picklist__c).Region__c;
                 leadObj.Region__c = leadObj.Country_Lookup_Region__c; 
                 leadObj.Country_Lookup__c = ctryLookupMap.get(leadObj.Country_Picklist__c).Id;
            }
                      
            if(isUpdate && isBefore &&  ctryLookupMap.containsKey(leadObj.Country_Picklist__c) )
            {
            
                 leadObj.Country_Lookup_Region__c=  ctryLookupMap.get(leadObj.Country_Picklist__c).Region__c;
                 leadObj.Region__c = leadObj.Country_Lookup_Region__c; 
                 leadObj.Country_Lookup__c = ctryLookupMap.get(leadObj.Country_Picklist__c).Id;
            }
         }   
        
    }
    // End of Method handling based on Country Picklist , Country Lookup will update
    
    // Method for checking Country Validation 
    // 1. if Country is China then City is Mandatory
    // 2. if Country is USA or India or Australia then City or State is Mandatory
         
    public void ctryValidTrgHandler(List<Lead> trigNew,boolean isInsert,boolean isUpdate, boolean isBefore)
    {
        for(Lead leadObj : trigNew)
        {
            if(isInsert && isBefore)
            {
                // Validation rule if Country Picklist  is China
                if(leadObj.Country_Picklist__c=='CHINA' && leadObj.City == Null) 
                {
                    leadObj.addError('If country is China, city is mandatory field');
                }    
                
                // Validation rule if Country Picklist is India or USA or Australia
                if((leadObj.Country_Picklist__c== 'INDIA' || leadObj.Country_Picklist__c== 'UNITED STATES OF AMERICA'  || leadObj.Country_Picklist__c== 'AUSTRALIA' ) 
                && leadObj.City == Null  && leadObj.State == Null) 
                {
                    leadObj.addError('If country is India or USA or Austalia, city or state is mandatory field');
                }  
            }
            if(isUpdate && isBefore)
            {
                // Validation rule if Country Picklist  is China
                if(leadObj.Country_Picklist__c=='CHINA' && leadObj.City == Null) 
                {
                    leadObj.addError('If country is China, city is mandatory field');
                }    
                
                // Validation rule if Country Picklist is India or USA or Australia
                if((leadObj.Country_Picklist__c== 'INDIA' || leadObj.Country_Picklist__c== 'UNITED STATES OF AMERICA'  || leadObj.Country_Picklist__c== 'AUSTRALIA' ) 
                && leadObj.City == Null  && leadObj.State == Null) 
                {
                    leadObj.addError('If country is India or USA or Austalia, city or state is mandatory field');
                }  
                
            }
            
        }
    }  
    
    // End of Method for checking Country Validation 
    
    //Method for checking Postal code format if Country is USA 
     // If country is USA then format for Postal Code
     
    public void postcodeFrmtforUSATrgHandler(List<Lead> trigNew,boolean isInsert,boolean isUpdate, boolean isBefore)
    {
        for(Lead leadObj : trigNew)
        {
            if(isInsert && isBefore)
            {
                // Postal code format for USA   
                if(leadObj.Country_Picklist__c == 'UNITED STATES OF AMERICA' && leadObj.PostalCode != null) 
                {  
                    if(!(Pattern.matches('[0-9]{5} [0-9]{4}',leadObj.PostalCode)|| 
                    Pattern.matches('[0-9]{5}-[0-9]{4}',leadObj.PostalCode)|| 
                    Pattern.matches('[0-9]{5}[0-9]{4}',leadObj.PostalCode)))
                    {     
                        leadObj.PostalCode.addError('The postal code entered for country United States of America is not in the appropriate format.  The Postal Code format should be XXXXXYYYY(E.g. 123456789) OR XXXXX-XXXX(E.g. 12345-6789) OR XXXXX XXXX(e.g. 12345 6789). If have filled in the appropriate data and you are still getting this error message then please contact support team');  
                    }
                 }    
                        
                // if country is USA then postal code will map to contact postal code  
                if(leadObj.Country_Picklist__c == 'UNITED STATES OF AMERICA' && leadObj.PostalCode != null)
                {
                    leadObj.Lead_PostalCode__c = leadObj.PostalCode;
                }
                
                // This is to restrict data coming from data import wizard which has Request_for_Conversion__c 
                // set to true mistakenly
                if(leadObj.Request_for_Conversion__c){
                    leadObj.Request_for_Conversion__c=false;
                }  
            }
            if(isUpdate && isBefore)
            {
                // Postal code format for USA   
                if(leadObj.Country_Picklist__c == 'UNITED STATES OF AMERICA' && leadObj.PostalCode != null) 
                {  
                    if(!(Pattern.matches('[0-9]{5} [0-9]{4}',leadObj.PostalCode)|| 
                    Pattern.matches('[0-9]{5}-[0-9]{4}',leadObj.PostalCode)|| 
                    Pattern.matches('[0-9]{5}[0-9]{4}',leadObj.PostalCode)))
                    {     
                        leadObj.PostalCode.addError('The postal code entered for country United States of America is not in the appropriate format.  The Postal Code format should be XXXXXYYYY(E.g. 123456789) OR XXXXX-XXXX(E.g. 12345-6789) OR XXXXX XXXX(e.g. 12345 6789). If have filled in the appropriate data and you are still getting this error message then please contact support team');  
                    }
                 }    
                        
                // if country is USA then postal code will map to contact postal code  
                if(leadObj.Country_Picklist__c == 'UNITED STATES OF AMERICA' && leadObj.PostalCode != null)
                {
                    leadObj.Lead_PostalCode__c = leadObj.PostalCode;
                } 
                
              //leadQueueChangeAction(trigger.new, trigger.isUpdate, trigger.isBefore, trigger.oldMap);   
            }    
        }
    } 
    
    // End of Method for  checking Postal code format if Country is USA 
    
    //Method for handling if lead is interested in exisiting Account
    public void existLeadTrigHandler(List<Lead> trigNew,boolean isInsert, boolean isBefore)
    {
        List<String> emailStr= new List<String>();
        Set<Id> recId = new Set<Id>();
        List<Id> ownerId=new List<Id>();
        set<id> targetUserId= new set<Id>();
        List<String> accid=new List<String>();
        map<Id,User> usrMap = new  map<Id,User>();
        List<Id> accOwnerId = new List<Id>();
        //Set<Id> refLeadOwnerId = new Set<Id>();
        Account accObj = new Account();
        List<Account> accObjList= new List<Account>();
        
        Map<Id,Account> accMap = new Map<Id,Account>();
        
        for(Lead leadObj : trigNew)
        {
            if(leadObj.getSObjectType().getDescribe().getName()== 'Lead' &&  leadObj.Lead_Account_Id__c != null)
            {
                accid.add(leadObj.Lead_Account_Id__c);
                recId.add(leadObj.Id);
            }
            
        }
      
        if(accid.size() > 0 )
        {
        
        accObjList = [Select Id,Name,Account_Id__c,OwnerId,Account_Status__c from account where Account_Id__c IN :accid 
                 AND Account_Status__c='Active'];
            refLeadOwnerId = accObj.OwnerId;  
            //accObjList.add(accObj);
        }
      
        if(accObjList.size() >0)
        {    
            for(Account accObjOwn : accObjList)
            {
                accOwnerId.add(accObjOwn.OwnerId);
                
            }
        }     
      
        if(accOwnerId.size() > 0)
        {
            usrMap = new map<Id,User>([Select Id,Name,email From User Where Id IN : accOwnerId]);
        }
        
        if(accObjList.size()> 0 ) 
        {
            for(Account accObjOwnerId :accObjList )
            {
              emailStr.add(usrMap.get(accObjOwnerId.OwnerId).email);
            }    
        }
        
        
       
        // Initializing Email templates for email notification
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();          
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
        EmailTemplate etforexstLogo=[Select id,Name,subject,body from EmailTemplate where DeveloperName=:'Lead_Object_task_for_existing_account_assigned_to_Sales_Account_Owner'];
    
        for(Lead leadObj : trigNew)
        {
            if(isInsert && isBefore && accObjList.size() > 0 && leadObj.Lead_Account_Id__c != null )
            {
               for(Integer i = 0; i < accObjList.size(); i++) 
                {    
                    leadObj.OwnerId = accObjList.get(i).OwnerId;
                    leadObj.Company = accObjList.get(i).Name; 
                    leadObj.LeadEmail__c = leadObj.email;
                    leadObj.email = usrMap.get(accObjList.get(i).OwnerId).email; 
                }
            }
            
        }
        /*if(!emailList.isEmpty()){
        try{
                system.debug('Email Sent :::');
                Messaging.sendEmail(emailList);
            } catch(Exception e){
                 System.debug('The following exception has occurred: ' + e.getMessage());
            }    
        }*/
        
    }  
    // End of Method for handling if lead is interested in exisiting Account
   
    //Method for LeadQueueChangeAction
    public void leadQueueChangeAction(List<Lead> trigNew,boolean isUpdate, boolean isBefore, List<Lead> trigOld)
    {
        List<Action_Item__c> action = new List<Action_Item__c>();
        //Fetches the queues with the lead object
        //List<QueueSobject> leadQueues =[Select Id, SobjectType, QueueId, Queue.Name  From QueueSobject where SobjectType='Lead'];
        Map<String,QueueSobject> queueMap = new Map<String,QueueSobject>();
    
        //Seperating the queues based on their names
        for (QueueSobject leadQueue : [Select Id, SobjectType, QueueId, Queue.Name  From QueueSobject where SobjectType='Lead'])
        {
            queueMap.put(leadQueue.Queue.Name, leadQueue);
        }
    
        //Gets the queue based on the name of the queue
        QueueSobject marketingQueue = queueMap.get('Marketing');
        QueueSobject gspLeadQueue = queueMap.get('GSP Lead Queue');
        QueueSobject entSalesLeads = queueMap.get('Enterprise Sales Leads');
    
        //fetches the adhoc request record type 
        RecordType rt1 = [select Id,Name from RecordType where Name='Adhoc Request' and SobjectType = 'Action_Item__c' limit 1];
        
        Map<Id,Lead> oldMap = new Map<Id,Lead>();
        for(Lead leadObj : trigOld) {
            oldMap.put(leadObj.Id,leadObj);
        }
        
        for(Lead ld : trigNew)
        {
            if(isUpdate && isBefore)
            {
                // If Trigger Old Owner is a Queue and New Owner is also a Queue        
                if(((String)(ld.OwnerId)).startswith('00G')&& ((String)(oldMap.get(ld.Id).OwnerId)).startswith('00G')){
                //checks whether the queue owner is sales queue 
                if ((ld.OwnerId==gspLeadQueue.QueueId ||ld.OwnerId==entSalesLeads.QueueId)&& oldMap.get(ld.Id).OwnerId==marketingQueue.QueueId){
                //Creates an action item
                Action_Item__c ai = new Action_Item__c();               
                ai.Lead__c = ld.Id;
                ai.Due_Date__c = System.Today();  //Expected this SLA to be defined
                ai.RecordTypeId = rt1.Id;
                ai.Region__c = ld.Region__c;
                ai.Subject__c = 'Lead is Moved From Marketing to Sales';
                ai.Status__c = 'Assigned';
                ai.Priority__c = 'High';
                action.add(ai);   
                }
               }
            }
        }  
        insert action;
        
    }
     ///End of LeadQueueChangeAction for Before Update    
}