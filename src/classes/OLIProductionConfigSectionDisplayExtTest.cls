/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=false)
private class OLIProductionConfigSectionDisplayExtTest {
 /*   public static Order__c ord;
    public static City_Lookup__c city;
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Order_Line_Item__c oli = getOrderLineItem();
        Product_Configuration__c pc = getPC();
        PageReference pageRef = Page.OLIProductionConfigSectionDisplay;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(pc);
       
        ApexPages.currentPage().getParameters().put('id', oli.id);
       
       Test.startTest();
      OLIProductionConfigSectionDisplayExt controller = new OLIProductionConfigSectionDisplayExt(sc);
      Test.stopTest();
      // cntrl.objId = pc.id;
        
    }


  private static Order__c getOrder(){
        if(ord == null){
        ord = new Order__c();
        Opportunity o = getOpportunity();
         ord.Opportunity__c = o.Id;
        //ord.name = 'TestODR-99999';
        insert ord;
        //System.assertEquals('TestODR-99999',ord.name);
        }
        return ord;
    }
    
    private static Order_Line_Item__c getOrderLineItem(){     
        ord = getOrder();
        Product2 prod1 =getProduct('GCC', 'GCC');
        
        Order_Line_Item__c oli1 = new Order_Line_Item__c();
        oli1.Product__c = prod1.id;
        oli1.Product_Id2__c = prod1.Product_Id__c;
        oli1.ParentOrder__c = ord.Id;
        oli1.CPQItem__c = '1';
        oli1.Line_Item_Status__c = 'New';
        oli1.OrderType__c = 'New Provide';
        oli1.Master_Service_ID__c = 'System Of A Down';
        oli1.Primary_Service_ID__c = 'Chop Suey';
        oli1.Path_ID__c = '';
        oli1.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli1.Path_Status__c = 'new';
        oli1.Net_NRC_Price__c = 100;
        oli1.Net_MRC_Price__c = 20;
        oli1.Is_GCPE_shared_with_multiple_services__c = 'NA';         
       insert oli1;
        return oli1;
    }
    private static Opportunity getOpportunity(){
        Opportunity opp = new Opportunity();
        Account a = getAccount();
        opp.Name = 'Test Opportunity CR611';
        opp.AccountId = a.Id;
         //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
        opp.StageName = 'Identify & Define';
        opp.Stage__c='Identify & Define';
        opp.CloseDate = System.today();
        opp.Estimated_MRC__c=800;
        opp.Estimated_NRC__c=1000;
        opp.ContractTerm__c='10';
        insert opp;
        return opp;
    }
    private static Account getAccount(){
        Account acc = new Account();
        Country_Lookup__c cl = getCountry();
        acc.Name = 'Test Account CR611';
        acc.Customer_Type__c = 'MNC';
        acc.Country__c = cl.Id;
        acc.Selling_Entity__c = 'Telstra INC';
        acc.Activated__c= true;
        acc.Account_Id__c ='12123';
        acc.Customer_Legal_Entity_Name__c='Test';
        acc.Activated__c=true;
        acc.Account_Status__c='Active';
        insert acc;
        return acc;
    }
        private static Pricebook2 getPriceBook(String prodName){
        Pricebook2 p = [SELECT Id FROM Pricebook2 LIMIT 1];
        return p;   
    }
    
    private static Product2 getProduct(String prodName,String serTyp){
        Product2 prod = new Product2();
        prod.Name = prodName;
        prod.ProductCode = prodName;
        prod.Product_ID__c = prodName;
        prod.Create_Service__c =serTyp;
        prod.Create_Path__c =TRUE;
        insert prod;
        return prod;
    }
    
    private static PricebookEntry getPriceBookEntry(String prodName){
        PricebookEntry p = new PricebookEntry();
        p.Pricebook2Id = getPriceBook(prodName).Id;
        p.Product2Id =  getProduct(prodName,'GCC').Id;
        p.UnitPrice = 2000;
        p.IsActive = true;
        insert p; 
        return p;    
    }   

    private static Country_Lookup__c getCountry(){
        Country_Lookup__c c2 = new Country_Lookup__c();
        //c2.CCMS_Country_Code__c = 'IND';
        //c2.CCMS_Country_Name__c = 'India';
        c2.Country_Code__c = 'xxx';
        insert c2;
        return c2;
    } 
  
    private static City_Lookup__c getCity(){
        if(city == null){
        city = new City_Lookup__c();
        city.Generic_Site_Code__c = 'U12';
        city.name = 'Magu';
        city.City_Code__c='xyz';
        city.OwnerID = userinfo.getuserid();
        insert city;
     }
     return city;
     
    }
     private static Product_Configuration__c getPC(){ 
        Product_Configuration__c pc = new Product_Configuration__c();
        pc.Order_Line_Item__c = getOrderLineItem().id;
        Product2 prod1 =getProduct('GCC', 'M');
        pc.Product_Id__c = prod1.Product_Id__c;
        //pc.Name = 'test';
        insert pc;
        return pc;
     
    }
  */
}