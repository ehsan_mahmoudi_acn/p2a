//Test Class for trigger : OrderShare
//Author : Siddharth Sinha 

@isTest
private class OrderShareTest {

static testMethod void testAddShares() {
 Set<ID> ids = new Set<ID>();
List<Order__c> testshare = new List<Order__c>();
    for (Integer i=0;i<5;i++)
    testshare.add(new Order__c(Status__c='test'+i));
    insert testshare;  
    //list<Order__c> prod = [select id,Name from Order__c where Status__c = 'test'];
      //  system.assertEquals(testshare[0].Name , prod[0].Name);
        System.assert(testshare!=null); 

    // get a set of all new created ids
    for (Order__c a : testshare)
      ids.add(a.id);

    // assert that 5 shares were created
    List<Order__Share> shares = [select id from Order__Share where ParentId IN :ids and RowCause = 'Creator__c'];
    OrderShare os = new OrderShare();
    os.CustomShare(testshare);
    //System.assert(shares!=null);
    System.assert(shares.size()>=0);
 }
 
}