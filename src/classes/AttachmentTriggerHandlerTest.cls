@isTest
public class AttachmentTriggerHandlerTest {
  static testmethod void createTIDocumentsmethod1()
    {
        List<Account> accounts = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> opportunitys = P2A_TestFactoryCls.getOpportunitys(1,accounts);
        
        Contract__c contr = new Contract__c(Account__c=accounts[0].id,Initial_Period__c='15',Variation_Number__c=2);
        insert contr;
        
        ObjectPrefixIdentifiers__c op=new ObjectPrefixIdentifiers__c();
        op.ObjectIdPrefix__c='a07';
        op.name='Contract';
        insert op;
        system.assertEquals(true,op!=null); 
         TI_Document__c tidoc = new TI_Document__c(Name= 'a07');
         insert tidoc;  
        system.assertEquals(true,tidoc!=null); 
        Attachment attachment = new Attachment();
        attachment.Body = Blob.valueOf('Test Data');
        attachment.Name = 'Test Attachment for Parent';        
        attachment.ParentId = contr.id;
        attachment.ParentId = tidoc.id;       
                
        Profile p=[SELECT Id,name From Profile WHERE Name='System Administrator' limit 1];
        List<User> usr = P2A_TestFactoryCls.get_Users(1);
        attachment.OwnerId = usr[0].id;        
        list<Attachment> attach=new list<Attachment>();
        attach.add(attachment);
        insert attach;
        
        Map<Id,SObject> newAttachmentsMap = new Map<Id,SObject>();
        system.assertEquals(true,attach!=null); 
        AttachmentTriggerHandler ath = new AttachmentTriggerHandler();
        ath.beforeInsert(attach);
        ath.beforeUpdate(attach,newAttachmentsMap,newAttachmentsMap);
        //ath.updateVariationNoOnContracts(attach);
        system.assertEquals(true,ath!=null); 
  } 
}