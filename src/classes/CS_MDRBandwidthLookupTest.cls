@isTest(SeeAllData = false)
private class CS_MDRBandwidthLookupTest {

    private static Map<String, String> searchFields = new Map<String, String>();
    private static List<IPL_Rate_Card_Bandwidth_Join__c> iplRateCardBandwidthJoinList;
    private static List<cspmb__Price_Item__c> priceItemList;
    private static List<CS_Bandwidth__c> bandwidthList;
    private static List<IPL_Rate_Card_Item__c> iplRateCardItemList;
    
    private static testMethod void doLookupSearchTest() {
        Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
           
           bandwidthList = new List<CS_Bandwidth__c>{           
           new CS_Bandwidth__c(Name = '64', Bandwidth_Value_MB__c = 128),
           new CS_Bandwidth__c(Name = '64', Bandwidth_Value_MB__c = 64),             
           new CS_Bandwidth__c(Name = '64', Bandwidth_Value_MB__c = 256)     
        };
        insert bandwidthList; 
        List<CS_Bandwidth__c> bw = [select id,name from CS_Bandwidth__c where name = '64'];
        system.assert(bw!=null);
        system.assertEquals(bandwidthList[0].name,bw[0].name);
            searchFields.put('CDR name', bandwidthList[0].name);            
            Id[] excludeIds;            
            String productDefinitionId;
            Integer pageOffset=24;
            Integer pageLimit=25 ;           

            CS_MDRBandwidthLookup bndLookup = new CS_MDRBandwidthLookup();
            String reqAtts = bndLookup.getRequiredAttributes();
            Object[] data = bndLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
                      
            data.clear();
            searchFields.put('CDR name', bandwidthList[0].name); 
            data = bndLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);            
            
        } catch(Exception e){
            ee = e;
            ErrorHandlerException.ExecutingClassName='CS_MDRBandwidthLookupTest:doLookupSearchTest';         
            ErrorHandlerException.sendException(e); 
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
           }
        } 
    }
    
       
}