@isTest(SeeAllData = false)
public class Telstra_Test { 
 private static List<Account> accList;
 private static HttpRequest req = new HttpRequest();  
    

    private static void initTestData(){        
          accList = P2A_TestFactoryCls.getAccounts(1);
          req.setEndpoint('http://134.159.212.68:8080/omsServer/api/orderService');
          req.setMethod('POST');
          system.assert(accList!=null);

    }

    private static testMethod void telstra()
    {
            initTestData();
            Test.startTest();
            try{
            Telstra hndl1 = new Telstra();
            Telstra.intfwsHandleSNCAccountTeamRequestsEndpoint0  hndl = new Telstra.intfwsHandleSNCAccountTeamRequestsEndpoint0();
            system.assert(hndl!=null);
            //Test.setMock(WebServiceMock.class, new MockHttpResponseGenerator());
            MockHttpResponseGenerator ht = new MockHttpResponseGenerator();            
            HTTPResponse res = ht.respond(req);            
            hndl.HandleSNCAccountTeamRequestsOp(accList[0].id);
            }catch(Exception e){
            ErrorHandlerException.ExecutingClassName='Telstra_Test:Telstra';         
            ErrorHandlerException.sendException(e); 
            }
            Test.stopTest();
                
            
        }
}