@isTest(SeeAllData=false)
private class ApproveCaseCtrlBatchTest {
    
    private static cscfga__Product_Basket__c basket = null;
    private static List<cscfga__Product_Configuration__c> prodConfigs = null;
    /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
        system.assert(globalMute!=null);
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
        system.assert(globalMute!=null);
    }
    @istest
    public static void prepareTestData()  
    {
            P2A_TestFactoryCls.sampleTestData();  
            List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
            List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList); 
            list<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
               
            basket = new cscfga__Product_Basket__c();
          //  basket.Currency = 'AUD';
            basket.Quote_Status__c = 'Draft';
            basket.csbb__Account__c = accList[0].id;
            basket.cscfga__Opportunity__c = oppList[0].id;
            prodBaskList.add(basket);
            insert basket;
            system.assertEquals(true,basket!=null); 
            cscfga__Product_Category__c productCategory = new cscfga__Product_Category__c(Name = 'Test category');
            insert productCategory;
            
            List<cscfga__Product_Definition__c> prodDefintions = new List<cscfga__Product_Definition__c> {
            new cscfga__Product_Definition__c (Name = 'Test root definition'
            , cscfga__Product_Category__c = productCategory.Id
            , cscfga__Description__c = 'Test root definition'), 
            new cscfga__Product_Definition__c (Name = 'Test child definition'
            , cscfga__Product_Category__c = productCategory.Id
            , cscfga__Description__c = 'Test child definition')};
            insert prodDefintions;
            
            List<cscfga__Attribute_Definition__c> attrDefinitions = new List<cscfga__Attribute_Definition__c> { 
            new cscfga__Attribute_Definition__c (Name = 'Test root attr definition'
            , cscfga__Product_Definition__c = prodDefintions[0].Id
            , cscfga__Data_Type__c = 'Decimal'
            , cscfga__Type__c = 'Display Value'),
            new cscfga__Attribute_Definition__c (Name = 'Test child attr definition'
            , cscfga__Product_Definition__c = prodDefintions[1].Id
            , cscfga__Data_Type__c = 'Decimal'
            , cscfga__Type__c = 'Display Value')
            }; 
            insert attrDefinitions;
            
            prodConfigs = new List<cscfga__Product_Configuration__c> {
            new cscfga__Product_Configuration__c( Name = 'Test root config'
            , cscfga__Product_Definition__c = prodDefintions[0].Id
            , cscfga__Product_Family__c = 'Test family'
            , cscfga__Product_Basket__c = basket.Id), 
            new cscfga__Product_Configuration__c( Name = 'Test root config'
            , cscfga__Product_Definition__c = prodDefintions[1].Id
            , cscfga__Product_Family__c = 'Test family'
            , cscfga__Product_Basket__c = basket.Id) 
            
            };
            insert prodConfigs;
            
            cscfga__Product_Configuration__c rootConfig = prodConfigs[0];
            cscfga__Product_Configuration__c childConfig = prodConfigs[1];
            childConfig.cscfga__Root_Configuration__c = rootConfig.Id;
            update childConfig;
            
            List<cscfga__Attribute__c> attrs = new List<cscfga__Attribute__c>{
            new cscfga__Attribute__c(Name = 'Test attribute'
            , cscfga__Product_Configuration__c = prodConfigs[0].Id
            , cscfga__Line_Item_Description__c = 'LI description'
            , cscfga__Value__c = '0.0'
            , cscfga__Display_Value__c = '0.0'
            , cscfga__Price__c = 0.0
            , cscfga__Is_Required__c = false
            , cscfga__Attribute_Definition__c = attrDefinitions[0].Id), 
            new cscfga__Attribute__c(Name = 'Test attribute'
            , cscfga__Product_Configuration__c = prodConfigs[1].Id
            , cscfga__Line_Item_Description__c = 'LI description'
            , cscfga__Value__c = '0.0'
            , cscfga__Display_Value__c = '0.0'
            , cscfga__Price__c = 0.0
            
            , cscfga__Is_Required__c = false
            , cscfga__Attribute_Definition__c = attrDefinitions[1].Id)
            };
            insert attrs;
            
            List<csbb__Product_Configuration_Request__c> prodRequests = new List<csbb__Product_Configuration_Request__c> {
            new csbb__Product_Configuration_Request__c(csbb__Product_Basket__c = basket.Id
            , csbb__Product_Category__c = productCategory.Id
            , csbb__Product_Configuration__c = prodConfigs[0].Id
            , csbb__Optionals__c = '{}'),
            new csbb__Product_Configuration_Request__c(csbb__Product_Basket__c = basket.Id
            , csbb__Product_Category__c = productCategory.Id
            , csbb__Product_Configuration__c = prodConfigs[1].Id
            , csbb__Optionals__c = '{}')
            };
            insert prodRequests; 
            
            
            List<custom_object_to_product_configuration__c> custToProdMaps = new List<custom_object_to_product_configuration__c> {
            new custom_object_to_product_configuration__c (Name = 'Test NRC cost'
            , Source_Object__c= 'Pricing_Approval_Request_Data__c'
            , Source_Field__c = 'Act_Approved_NRC__c'
            , Production_Configuration_Attribute_Name__c = attrs[0].Name
            , Product_Name__c = 'Test familyPricingApproval')
            };
            insert custToProdMaps;
           
          /*  Map<string,custom_object_to_product_configuration__c> customsetting = new Map<string,custom_object_to_product_configuration__c>();
            customsetting.put(custToProdMaps[0].Name,custToProdMaps[0]);   
            custom_object_to_product_configuration__c cs = custom_object_to_product_configuration__c.getvalues('Test NRC cost');
            //system.assert(CurrencyIsoCode == USD);
            system.assertEquals('Test attribute',cs.Production_Configuration_Attribute_Name__c);     
            System.Assert(CS.Source_Field__c == 'Act_Approved_NRC__c');
            System.assertEquals('Test familyPricingApproval',CS.Product_Name__c);*/
            System.assertEquals('Test family',prodConfigs[0].cscfga__Product_Family__c );     
    }
    
    private static void preparePriceRequestData(Integer prodConfigIndex) {
        Exception ee = null;
        Integer userid = 0;
        try {
            disableAll(UserInfo.getUserId());
            system.assert(userid!=null);
            prepareTestData();
            Pricing_Approval_Request_Data__c approvalRequest = new Pricing_Approval_Request_Data__c(Product_Basket__c = basket.Id
                , Approved_NRC__c = 1.1
                , Approved_RC__c =  1.1
                , Product_Configuration__c = prodConfigs[prodConfigIndex].Id
                , is_Pricing__c = true);
            insert approvalRequest;
        } catch(Exception e) {
            ee = e;
            ErrorHandlerException.ExecutingClassName='ApproveCaseCtrlBatchTest:preparePriceRequestData';         
            ErrorHandlerException.sendException(e); 
        } finally {
            enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
    
   private static testMethod void testCaseBatchOnRoot() 
   {     
       preparePriceRequestData(1);
         ApproveCaseCtrlBatch bc = new ApproveCaseCtrlBatch(basket.Id);
                Database.executeBatch(bc);  
      Map<string,custom_object_to_product_configuration__c> customsetting = new Map<string,custom_object_to_product_configuration__c>();
      custom_object_to_product_configuration__c cs = custom_object_to_product_configuration__c.getvalues('Test NRC cost');
      List<cscfga__Product_Basket__c> bask = [select id,name,Quote_Status__c from cscfga__Product_Basket__c ]; 
      List<cscfga__Attribute__c> attrr = [select id,name,Product_Basket_CurrencyISOCode__c,CurrencyIsoCode from cscfga__Attribute__c] ;   
            system.assertEquals('Test attribute',cs.Production_Configuration_Attribute_Name__c);     
            System.Assert(CS.Source_Field__c == 'Act_Approved_NRC__c');
            System.assertEquals('Test familyPricingApproval',CS.Product_Name__c);
           // System.assertEquals('Test family',prodConfigs[0].cscfga__Product_Family__c );  
           // system.assertEquals(bask[0].Quote_Status__c ,'Approved');
            system.assertEquals(attrr[0].CurrencyIsoCode,attrr[0].Product_Basket_CurrencyISOCode__c);
    }    
    
   /* private static testMethod void testCaseBatchOnChild() 
    {      
        preparePriceRequestData(1);       
        ApproveCaseCtrlBatch bc = new ApproveCaseCtrlBatch(basket.Id);
        Database.executeBatch(bc);         
    } */
}