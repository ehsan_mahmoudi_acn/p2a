@isTest(SeeAllData = false)
public class ZEndPOPLookupTest {
    
    private static List<IPL_Rate_Card_Item__c> rateCardItemList;
    private static List<Additional_Data__c> additionalDataList;
    private static List<CloudSense_POP__c> cloudSensePOPList;
    private static Map<String, String> searchFields;
    private static String productDefinitionId;
    private static Id[] excludeIds = new List<Id>();
    private static Integer pageOffset, pageLimit;
    private static Object[] data = new List<Object>();
    
    private static void initTestData(){
        searchFields = new Map<String, String>();
        searchFields.put('searchValue','');
        
        additionalDataList = new List<Additional_Data__c>{
            new Additional_Data__c(Name = 'Croatia', Type__c = 'Country'),
            new Additional_Data__c(Name = 'Australia', Type__c = 'Country'),
            new Additional_Data__c(Name = 'Germany', Type__c = 'Country'),
            new Additional_Data__c(Name = 'Poland', Type__c = 'Country'),
            new Additional_Data__c(Name = 'Peru', Type__c = 'Country')
        };

        insert additionalDataList;
        list<Additional_Data__c> prod = [select id,Name from Additional_Data__c where Name = 'Croatia'];
        system.assertEquals(additionalDataList[0].Name , prod[0].Name);
        System.assert(additionalDataList!=null);
        
        searchFields.put('A-End Country', additionalDataList[1].Id);
        searchFields.put('Z-End Country', additionalDataList[2].Id);
        
        cloudSensePOPList = new List <CloudSense_POP__c>{
          new CloudSense_POP__c(Name = 'POP 1', Address__c = 'Address 1', City__c = 'City 1'),  
          new CloudSense_POP__c(Name = 'POP 2', Address__c = 'Address 2', City__c = 'City 2'),
          new CloudSense_POP__c(Name = 'POP 3', Address__c = 'Address 3', City__c = 'City 3'),
          new CloudSense_POP__c(Name = 'POP 4', Address__c = 'Address 4', City__c = 'City 4')
        };
        
        insert cloudSensePOPList;
          list<CloudSense_POP__c> prod2 = [select id,Name from CloudSense_POP__c where Name = 'POP 1'];
        system.assertEquals(cloudSensePOPList[0].Name , prod2[0].Name);
        System.assert(cloudSensePOPList!=null);  
        
        searchFields.put('A-End POP/CLS 1', cloudSensePOPList[1].Id);
        
        rateCardItemList = new List<IPL_Rate_Card_Item__c>{
            new IPL_Rate_Card_Item__c(Aend_Country__c = additionalDataList[0].Id, Zend_Country__c = additionalDataList[3].Id, Aend_POP__c = cloudSensePOPList[0].Id, Zend_POP__c = cloudSensePOPList[3].Id),
            new IPL_Rate_Card_Item__c(Aend_Country__c = additionalDataList[1].Id, Zend_Country__c = additionalDataList[2].Id, Aend_POP__c = cloudSensePOPList[1].Id, Zend_POP__c = cloudSensePOPList[2].Id),
            new IPL_Rate_Card_Item__c(Aend_Country__c = additionalDataList[2].Id, Zend_Country__c = additionalDataList[1].Id, Aend_POP__c = cloudSensePOPList[2].Id, Zend_POP__c = cloudSensePOPList[1].Id),
            new IPL_Rate_Card_Item__c(Aend_Country__c = additionalDataList[3].Id, Zend_Country__c = additionalDataList[0].Id, Aend_POP__c = cloudSensePOPList[3].Id, Zend_POP__c = cloudSensePOPList[0].Id)
        };
        
        insert rateCardItemList;
        
        system.assertEquals(rateCardItemList[0].Aend_Country__c , additionalDataList[0].Id);
        System.assert(rateCardItemList!=null);
        
       
    }
    

    static testMethod void doLookupSearchTest(){
        Exception ee = null;
        
        try{
            disableAll(UserInfo.getUserId());
            Test.startTest();
            initTestData();
            
            ZEndPOPLookup zEndPopLookup = new ZEndPOPLookup();  
            zEndPopLookup.doDynamicLookupSearch(searchFields, productDefinitionID);
            String requiredAtts = zEndPopLookup.getRequiredAttributes();
            data = zEndPopLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            System.debug('*******Data: ' + data);
            
            System.assert(data.size() > 0, '');
            Test.stopTest();
        } catch(Exception e){
            ee = e;
                ErrorHandlerException.ExecutingClassName='ZEndPOPLookupTest :doLookupSearchTest';         
                ErrorHandlerException.sendException(e);
        } finally {
           // Test.stopTest();
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }
    }
    
    static testMethod void doLookupSearchExceptionTest(){
        Exception ee = null;
        
        try{
            disableAll(UserInfo.getUserId());
            Test.startTest();
            //initTestData();
            
            ZEndPOPLookup zEndPopLookup = new ZEndPOPLookup();
            data = zEndPopLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            System.debug('*******Data: ' + data);
          System.assert(zEndPopLookup!=null);
            Test.stopTest();
        } catch(Exception e){
            ee = e;
            ErrorHandlerException.ExecutingClassName='ZEndPOPLookupTest :doLookupSearchExceptionTest';         
            ErrorHandlerException.sendException(e);
        } finally {
           // Test.stopTest();
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }
    }

    

    /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }
}