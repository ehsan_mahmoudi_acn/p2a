@isTest(SeeAllData = false)
    private class CS_RateCardLookupTest {

        private static List<CS_Country__c> countryList;
        private static List<CS_City__c> cityList;       
        private static List<CS_POP__c> popList;
        private static List<CS_Route_Segment__c> routeSegmentList;
        private static List<CS_Resilience__c> resilienceList;
        private static Map<String, String> searchFields = new Map<String, String>();
        private static String productDefinitionId;
        private static Id[] excludeIds = new List<Id>();
        private static Integer pageOffset, pageLimit;
        private static Object[] data = new List<Object>();
        private static List<CS_Bandwidth__c> bandwidthList;
        private static List<CS_Bandwidth_Product_Type__c> bandwidthProdTypeList;
        private static List<CS_Route_Bandwidth_Product_Type_Join__c> routeBandwidthProductTypeJoinList;
        
        
        private static void initTestData(){
        
            countryList = new List<CS_Country__c>{                
                new CS_Country__c(Name = 'India'),                
                new CS_Country__c(Name = 'Hong Kong')
            };        
            insert countryList;
     list<CS_Country__c> prod = [select id,Name from CS_Country__c where Name = 'India'];
                                system.assertEquals(countryList[0].name , prod[0].name);
                                System.assert(countryList!=null); 
                                System.assertEquals('India',countryList[0].Name ); 


      
            cityList = new List<CS_City__c>{
                new CS_City__c(Name = 'Bangalore', CS_Country__c = countryList[0].Id),
                new CS_City__c(Name = 'Delhi', CS_Country__c = countryList[1].Id)                
            };        
            insert cityList;
     list<CS_City__c> AWW = [select id,Name from CS_City__c where Name = 'Bangalore'];
                                system.assertEquals(cityList[0].name , AWW[0].name);
                                System.assert(cityList!=null);
                                 System.assertEquals('Bangalore', cityList[0].Name ); 
            
            popList = new List<CS_POP__c>{
                new CS_POP__c(Name = 'POP 1', CS_Country__c = countryList[0].Id),
                new CS_POP__c(Name = 'POP 2', CS_Country__c = countryList[1].Id)                
            };        
            insert popList;
             list<CS_POP__c> AVV = [select id,Name from CS_POP__c where Name = 'POP 1'];
                                system.assertEquals(popList[0].name , AVV[0].name);
                                System.assert(popList!=null);
            
      System.assertEquals('POP 1', popList[0].Name ); 

            
            resilienceList = new List<CS_Resilience__c>{
                new CS_Resilience__c(Name = 'Protected'), 
                new CS_Resilience__c(Name = 'Unprotected'), 
                new CS_Resilience__c(Name = 'Restored')               
            };        
            insert resilienceList;
             list<CS_Resilience__c> ATT = [select id,Name from CS_Resilience__c where Name = 'Protected'];
                                system.assertEquals(resilienceList[0].name , ATT[0].name);
                                System.assert(resilienceList!=null);
            
    System.assertEquals('Protected', resilienceList[0].Name ); 

                    
            routeSegmentList = new List<CS_Route_Segment__c>{
                new CS_Route_Segment__c(Name = 'Route Segment 1', Product_Type__c = 'EVPL', POP_A__c = popList[0].Id, POP_Z__c = popList[0].Id, CS_Resilience__c = resilienceList[0].Id),
                new CS_Route_Segment__c(Name = 'Route Segment 2', Product_Type__c = 'EVPL', POP_A__c = popList[0].Id, POP_Z__c = popList[0].Id, CS_Resilience__c = resilienceList[1].Id),
                new CS_Route_Segment__c(Name = 'Route Segment 3', Product_Type__c = 'EVPL', POP_A__c = popList[0].Id, POP_Z__c = popList[0].Id, CS_Resilience__c = resilienceList[2].Id)
             };        
            insert routeSegmentList;
            list<CS_Route_Segment__c> AGG = [select id,Name from CS_Route_Segment__c where Name = 'Route Segment 1'];
                                system.assertEquals(routeSegmentList[0].name , AGG[0].name);
                                System.assert(routeSegmentList!=null);
            
         System.assertEquals('Route Segment 1', routeSegmentList[0].Name ); 
       
            
            bandwidthList = new List<CS_Bandwidth__c>{
                new CS_Bandwidth__c(Name = '16', Bandwidth_Value_MB__c = 16),
                new CS_Bandwidth__c(Name = '32', Bandwidth_Value_MB__c = 32),
                new CS_Bandwidth__c(Name = '64', Bandwidth_Value_MB__c = 64)
            };        
            insert bandwidthList;
              list<CS_Bandwidth__c> BAA = [select id,Name from CS_Bandwidth__c where Name = '16'];
                                system.assertEquals(bandwidthList[0].name , BAA[0].name);
                                System.assert(bandwidthList!=null);
            
     System.assertEquals('16', bandwidthList[0].Name ); 

            
            bandwidthProdTypeList = new List<CS_Bandwidth_Product_Type__c>{
                new CS_Bandwidth_Product_Type__c(Name = 'Bandwidth Product Type 1', CS_Bandwidth__c = bandwidthList[0].Id, Bandwidth_Group__c ='test1,Test1', Product_Type__c = 'EVPL'),
                new CS_Bandwidth_Product_Type__c(Name = 'Bandwidth Product Type 2', CS_Bandwidth__c = bandwidthList[1].Id, Bandwidth_Group__c ='test1,Test1', Product_Type__c = 'EVPL'),
                new CS_Bandwidth_Product_Type__c(Name = 'Bandwidth Product Type 3', CS_Bandwidth__c = bandwidthList[2].Id, Bandwidth_Group__c ='test1,Test1', Product_Type__c = 'EVPL')
            };
            insert bandwidthProdTypeList;
            list<CS_Bandwidth_Product_Type__c> BBB = [select id,Name from CS_Bandwidth_Product_Type__c where Name = 'Bandwidth Product Type 1'];
                                system.assertEquals(bandwidthProdTypeList[0].name , BBB[0].name);
                                System.assert(bandwidthProdTypeList!=null);
            
   System.assertEquals('Bandwidth Product Type 1', bandwidthProdTypeList[0].Name); 
  
            
            routeBandwidthProductTypeJoinList = new List<CS_Route_Bandwidth_Product_Type_Join__c>{
               new CS_Route_Bandwidth_Product_Type_Join__c(Name = 'Product Type Join 1', CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[0].Id, CS_Route_Segment__c = routeSegmentList[0].Id),
               new CS_Route_Bandwidth_Product_Type_Join__c(Name = 'Product Type Join 2', CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[1].Id, CS_Route_Segment__c = routeSegmentList[1].Id),
               new CS_Route_Bandwidth_Product_Type_Join__c(Name = 'Product Type Join 3', CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[2].Id, CS_Route_Segment__c = routeSegmentList[2].Id)
               };        
            insert routeBandwidthProductTypeJoinList;
           list<CS_Route_Bandwidth_Product_Type_Join__c> BCC = [select id,Name from CS_Route_Bandwidth_Product_Type_Join__c where Name = 'Product Type Join 1'];
                                system.assertEquals(routeBandwidthProductTypeJoinList[0].name , BCC[0].name);
                                System.assert(routeBandwidthProductTypeJoinList!=null);
                                System.assertEquals('Product Type Join 1', routeBandwidthProductTypeJoinList[0].Name);
    }
        
        private static testMethod void doLookupSearchTestMethod1() {
            Exception ee = null;

            try{
                disableAll(UserInfo.getUserId());
                Test.startTest();
                initTestData();
                
                searchFields.put('A End Country',countryList[0].id);
                searchFields.put('Z-End Country',countryList[1].id);
                searchFields.put('A End City',cityList[0].id);
                searchFields.put('Z End City',cityList[1].id);
                searchFields.put('Bandwidth',bandwidthList[0].id);
                searchFields.put('attValue','Yes');             
                
                CS_RateCardLookup csRateCardLookup = new CS_RateCardLookup();                
                data = csRateCardLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
                String requiredAtts = csRateCardLookup.getRequiredAttributes();
                system.assert(csRateCardLookup !=null);
            } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='CS_RateCardLookupTest :doLookupSearchTest_Method1';         
            ErrorHandlerException.sendException(e); 
                ee = e;
            } finally {
                Test.stopTest();
                enableAll(UserInfo.getUserId());
                if(ee != null){
                    throw ee;
                }
            }
      }

      private static testMethod void doLookupSearchTestMethod2() {
            Exception ee = null;

            try{
                disableAll(UserInfo.getUserId());
                Test.startTest();
                initTestData();
                
                searchFields.put('A End Country',countryList[0].id);
                searchFields.put('Z-End Country',countryList[1].id);
                searchFields.put('A End City',cityList[0].id);
                searchFields.put('Z End City',cityList[1].id);
                searchFields.put('Bandwidth',bandwidthList[0].id); 
                searchFields.put('EVPL','Yes'); 
                searchFields.put('ProductTypeNames','EVPL');
                searchFields.put('Show Protected Routes','Yes');
                searchFields.put('Show Unprotected Routes','Yes');
                searchFields.put('Show Restored Routes','Yes');             
                searchFields.put('A-End POPCLS 1',popList[0].id);
                searchFields.put('Z-End POPCLS 1',popList[1].id);
                
                CS_RateCardLookup csRateCardLookup = new CS_RateCardLookup();                
                data = csRateCardLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
                String requiredAtts = csRateCardLookup.getRequiredAttributes();   
                system.assert(csRateCardLookup !=null);
                Test.stopTest();             
                
            } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='CS_RateCardLookupTest :doLookupSearchTest_Method2';         
            ErrorHandlerException.sendException(e); 
                ee = e;
            } finally {
               
                enableAll(UserInfo.getUserId());
                if(ee != null){
                    throw ee;
                }
            }
      }
        /**
         * Disables triggers, validations and workflows for the given user
         * @param userId Id
         */
        private static void disableAll(Id userId) {
            Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

            if(globalMute == null) {
                globalMute = new Global_Mute__c();
                globalMute.SetupOwnerId = userId;
                globalMute.Mute_Triggers__c = true;
                globalMute.Mute_Validations__c = true;
                globalMute.Mute_Workflows__c = true;
            }
            else {
                globalMute.Mute_Triggers__c = true;
                globalMute.Mute_Validations__c = true;
                globalMute.Mute_Workflows__c = true;
            }

            upsert globalMute;
        }
        
        /**
         * Enables triggers, validations and workflows 
         * @param userId Id
         */
        private static void enableAll(Id userId) {
            Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

            if(globalMute == null) {
                globalMute = new Global_Mute__c();
                globalMute.SetupOwnerId = userId;
                globalMute.Mute_Triggers__c = false;
                globalMute.Mute_Validations__c = false;
                globalMute.Mute_Workflows__c = false;
            }
            else {
                globalMute.Mute_Triggers__c = false;
                globalMute.Mute_Validations__c = false;
                globalMute.Mute_Workflows__c = false;
            }

            upsert globalMute;
        }
    }