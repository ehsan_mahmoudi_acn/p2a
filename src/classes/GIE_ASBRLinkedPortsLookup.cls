global with sharing class GIE_ASBRLinkedPortsLookup extends cscfga.ALookupSearch {


   public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){

        String confId = searchFields.get('Product_config_ID__c');
        String basketId = searchFields.get('BasketId');
        String Ids = '';
        
        system.debug('***Before entering ');
        
        
        String confString = '%'+confId+'%';
        if(basketId != '' && basketId != null && confId != ''){
            /*List<cscfga__Product_Configuration__c> portList = [SELECT Id, GIE_Selected_ASBR__c, cscfga__Product_Basket__c 
                                                                FROM cscfga__Product_Configuration__c 
                                                                WHERE GIE_Selected_ASBR__c LIKE :confString
                                                                AND cscfga__Product_Basket__c = :basketId];

            system.debug('***ASBR GIE Ports= ' + portList);*/
    
            for(cscfga__Product_Configuration__c item : [SELECT Id, GIE_Selected_ASBR__c, cscfga__Product_Basket__c 
                                                                FROM cscfga__Product_Configuration__c 
                                                                WHERE GIE_Selected_ASBR__c LIKE :confString
                                                                AND cscfga__Product_Basket__c = :basketId]){
                if(Ids == ''){
                    Ids = Ids + item.Id;
                }else{
                    Ids = Ids + ','+item.Id;
                }
            }
        }
        system.debug('***ASBR GIE Ports Ids= ' + Ids);
        
        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(Name = Ids);
        
        List<cscfga__Product_Configuration__c> result  = new List<cscfga__Product_Configuration__c>();

        result.add(pc);

        system.debug('***ASBR GIE Ports result = ' + result);

        return result;
    }


  public override String getRequiredAttributes(){ 
      return '["Product_config_ID__c","BasketId"]';
  }  
}