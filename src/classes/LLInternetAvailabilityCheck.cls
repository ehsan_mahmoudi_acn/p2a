global with sharing class LLInternetAvailabilityCheck {
  
  webservice static String doWork (String city, String id, String country, String pop) {

    system.debug('******** LocalLoopAvailabilityCheck ****** '+ city +' -- '+id+' -- '+country +' -- '+pop);
    Map<String, String> retMap = new Map<String, String>{'city' => city, 'id' => id,'country' => country, 'pop' => pop}; 
    return JSON.serialize(retMap);
  }
}