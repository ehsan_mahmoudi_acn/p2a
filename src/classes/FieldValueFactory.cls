public class FieldValueFactory {
    private List<FieldValueGenerator> generators = new List<FieldValueGenerator> {
        new FieldLoremValueGenerator(),
        new FieldTimeValueGenerator(),
        new FieldDateTimeValueGenerator(),
        new FieldDateValueGenerator(),
        new FieldBooleanValueGenerator(),
        new FieldCurrencyValueGenerator(),
        new FieldDoubleValueGenerator(),
        new FieldReferenceValueGenerator(),
        new FieldEmailValueGenerator(),
        new FieldPicklistValueGenerator(),
        new FieldBase64ValueGenerator(),
        new FieldIntegerValueGenerator(),
        new FieldPhoneValueGenerator(),
        new FieldUrlValueGenerator()
    };

    public Object generate(Schema.DescribeFieldResult fieldDesc) {
        FieldValueGenerator generator = getGeneratorFor(fieldDesc);
        if (generator == null) {
            throw new FieldValueGeneratorNotFoundException(fieldDesc);
        }
        return generator.generate(fieldDesc);
    }
    
    private FieldValueGenerator getGeneratorFor(Schema.DescribeFieldResult fieldDesc) {
        for (FieldValueGenerator generator: this.generators) {
            if (generator.canGenerateValueFor(fieldDesc)) {
                return generator;
            }
        }
        throw new FieldValueGeneratorNotFoundException(fieldDesc);
    }
}