@isTest(SeeAllData=False) 
public class CaseExtensionTest{

    public static cscfga__Product_Basket__c basket = null;
    private static List<cscfga__Product_Configuration__c> prodConfigs = null;
    public static list<Account> accList = P2A_TestFactoryCls.getAccounts(1);   
    public static list<User> usrList = P2A_TestFactoryCls.get_Users(1);  
    public static user usr = usrList[0];      
    public static List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1,accList);
    public static List<cscfga__Product_Basket__c> basketList =  P2A_TestFactoryCls.getProductBasket(1);
    public static List<csord__Order_Request__c> ordReqList =  P2A_TestFactoryCls.getorderrequest(1);
    public static List<csord__Order__c> ordList = P2A_TestFactoryCls.getorder(1, OrdReqList);
    public static List<case> caseList1= P2A_TestFactoryCls.getcases(1,AccList,oppList,basketList,ordList,usrList);    
    public static case upCase = caseList1[0];

    static testMethod void unitTest() {
            list<case> CaseList = new list<case>();
            case cs = new case();
            cs.Status ='Unassigned';
            cs.Send_Overdue_Email__c  = false;
            cs.Is_Feasibility_Request__c = true;
            cs.Is_Resource_Reservation__c = true ;
            //cs.type = 'Enrichment';
            Date d = date.today(); 
            cs.Due_Date__c = d.adddays(-1);
            CaseList.add(cs);
          insert CaseList;
          System.assertEquals(cs.Status,'Unassigned');
          system.debug('-----'+CaseList);
        
        Test.startTest();
            Case Cas= new Case();
            Cas.Approval_Status_Reason__c ='Other';
            insert cas;
           //ApexPages.StandardController sc = new ApexPages.StandardController(cs);
           CaseExtension cex = new CaseExtension (new ApexPages.StandardController(Cas)); 
           //cex.reject();
           cex.returnToCase();
           
      /* List<case> updateCase = [select id,Status,Approval_Rejection_Date__c from case where id =:CaseList.get(0).id limit 1];
       updateCase.get(0).Status= 'Rejected';
       updateCase.get(0).Approval_Rejection_Date__c= date.today();
       update updateCase.get(0);
       Integer i = [SELECT COUNT() FROM case];
       //System.assertEquals(i, 1);
        
        
      */    
       Test.stopTest();
      
        
    }
     Public Static testMethod void  caseTest(){ 
     
        Account acc1 = new Account(Name = 'Account1');
        insert acc1;

        Opportunity opp1 = new Opportunity(Name = 'Opp1', Account = acc1,CloseDate = system.today() + 10,stagename = 'Identify & Define');
 
        insert opp1;
        system.assertNotEquals(opp1.CloseDate, system.today());
        
     
     Test.startTest();
            Case Cas= new Case();
            Cas.Approval_Status_Reason__c ='Other';
           // Cas.type = 'Enrichment';
           /* cas.TCV__c = 1000;
            cas.status__c='unassighned';
            cas.type = 'Enrichment';
            cas.Approval_Status_Reason__c = 'Other';
            cas.Enrichment_Status__c = 'Rejected';
            cas.Opportunity_Name__c=opp1.id;*/
            
            insert cas;
            system.assertEquals(Cas.Approval_Status_Reason__c,'Other');
            
            Case Cas1= new Case();
            Cas1.Approval_Status_Reason__c ='';           
            insert cas1;
            system.assert(Cas1.Approval_Status_Reason__c =='');
            
            try{

            ApexPages.StandardController controller = new ApexPages.StandardController(cas);           
            CaseExtension cex = new CaseExtension(controller); 
            cex.reject();
            
            cas.status = 'Rejected';
            update cas; 
            
            }catch(Exception e){
             system.debug('exception'+ e);
            }          
                          
            
           CaseExtension app = new CaseExtension (new ApexPages.StandardController(cas));
           app.reject();
       
     Test.stopTest();
    } 
    
   Public Static testMethod void  caseTest8(){ 
     
        Account acc1 = new Account(Name = 'Account1');
        insert acc1;

        Opportunity opp1 = new Opportunity(Name = 'Opp1', Account = acc1,CloseDate = system.today() + 10,stagename = 'Identify & Define'); 
        insert opp1;
        system.assert(opp1.Name=='Opp1');
        
     
     Test.startTest();
           Case Cas= new Case();
            Cas.Approval_Status_Reason__c ='';
           /* cas.TCV__c = 1000;
            cas.status__c='unassighned';
            cas.type = 'Enrichment';
            cas.Approval_Status_Reason__c = 'Other';
            cas.Enrichment_Status__c = 'Rejected';
            cas.Opportunity_Name__c=opp1.id;*/
            
            insert cas;

            ApexPages.StandardController controller = new ApexPages.StandardController(cas);           
            CaseExtension cex = new CaseExtension(controller); 
            cex.reject();
            
            cas.status = 'Rejected';
            update cas;   
                system.assertNotEquals(cas.status,'');
                          
            
              CaseExtension app = new CaseExtension (new ApexPages.StandardController(cas));
           app.reject();
       
     Test.stopTest();
    } 
    
   
    
  }