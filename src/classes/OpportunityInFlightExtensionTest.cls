@isTest

public class OpportunityInFlightExtensionTest {
   static Account acc1 = getAccount(); 
   static opportunity opp1=getOpportunity(); 
   

   private static Account getAccount(){                
        Account acc = new Account();            
        Country_Lookup__c c = getCountry();            
        acc.Name = 'Test Account';            
        acc.Customer_Type__c = 'MNC';            
        acc.Country__c = c.Id;            
        acc.Selling_Entity__c = 'Telstra INC';  
        acc.Activated__c= true;  
        acc.Account_ID__c = '9090';  
        acc.Customer_Legal_Entity_Name__c = 'Test' ;     
        insert acc; 
      
        System.assert(acc!=null);       
        return acc;    
   } 
   private static Country_Lookup__c getCountry(){        
           Country_Lookup__c country = new Country_Lookup__c();            
           country.CCMS_Country_Code__c = 'HK';            
           country.CCMS_Country_Name__c = 'India';            
           country.Country_Code__c = 'HK';            
           insert country; 
           system.assert(country!=null);       
           return country;    
         } 
       private static Opportunity getOpportunity(){                 
        Opportunity opp = new Opportunity();            
        //Account acc = getAccount();           
        opp.Name = 'Test Opportunity';            
        opp.AccountId = acc1.Id;   
         //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement         
        opp.StageName = 'Identify & Define' ;  
        opp.Stage__c= 'Identify & Define' ;          
        opp.CloseDate = System.today();   
        opp.Opportunity_Type__c = 'Complex';
        opp.Estimated_MRC__c=8;
            opp.Estimated_NRC__c=2;
            opp.ContractTerm__c='1';
            opp.Order_Type__c='New';
        //opp.Stage_Gate_1_Action_Created__c = true;         
        //opp.Approx_Deal_Size__c = 9000;                 
        System.assert(opp!=null);
        return opp; 
        
        
       } 
      private static cscfga__Product_Basket__c  getProductbasket(){
      cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c ();
      pb.cscfga__Opportunity__c =opp1.id;
      pb.csordtelcoa__Synchronised_with_Opportunity__c =true;
      pb.csbb__Synchronised_With_Opportunity__c =true;
      System.assert(pb!=null);
       return pb; 
        
       }
    static testMethod void opportunityInFlightExtensionTest() { 
    
        String Subject = 'Testmsg';
        csord__Order_Request__c ordreq = new csord__Order_Request__c();
        ordreq.Name = 'OrderName';
        ordreq.csord__Module_Name__c = 'SansaStark123683468Test';
        ordreq.csord__Module_Version__c = 'YouknownothingJhonSwon';
        ordreq.csord__Process_Status__c = 'Requested';
        ordreq.csord__Request_DateTime__c = System.now();
        insert ordreq;
        //list<csord__Order_Request__c > ord = [select id,Name from csord__Order_Request__c where Name = 'OrderName'];
        System.assert(ordreq!=null);
       // system.assertEquals(ordreq[0].name,'OrderName');
        
        Test.startTest();
     
      
      ApexPages.StandardController sc = new ApexPages.StandardController(opp1);
      OpportunityInFlightExtension oppex = new OpportunityInFlightExtension(sc);
      oppex.loadOrders();
      oppex.addError(Subject);
      //oppex.selectChangeOrders();
      oppex.returnToOpportunity();
      
      Test.stopTest();
         
     } 
     
     static testMethod void opportunityInFlightExtensionTest3() { 
     P2A_TestFactoryCls.sampleTestData();
     P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
          Product_Definition_Id__c pdI = new Product_Definition_Id__c();
                pdI.Name = 'IPVPN_Product_ID';
                pdI.Product_Id__c = 'IPVPN';
                insert pdI;
                system.assert(pdI!=null);
                Product_Definition_Id__c pdI1 = new Product_Definition_Id__c();
                pdI1.Name = 'VPLS_Product_ID';
                pdI1.Product_Id__c = 'VLM';
                insert pdI1;
                system.assert(pdI1!=null);
                Product_Definition_Id__c pdI2 = new Product_Definition_Id__c();
                pdI2.Name = 'IPC_Product_ID';
                pdI2.Product_Id__c = 'IPTS-C';
                insert pdI2;
                system.assert(pdI2!=null);
        
        List<csord__Order_Request__c> orqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1,orqList);
        //List<csord__Service__c> serList = P2A_TestFactoryCls.getService(1,orqList,subList);
            
    
        String Subject = 'Testmsg';
        csord__Order_Request__c ordreq = new csord__Order_Request__c();
        ordreq.Name = 'OrderName';
        ordreq.csord__Module_Name__c = 'SansaStark123683468Test';
        ordreq.csord__Module_Version__c = 'YouknownothingJhonSwon';
        ordreq.csord__Process_Status__c = 'Requested';
        ordreq.csord__Request_DateTime__c = System.now();
        insert ordreq;
        
        
        System.assertEquals(ordreq.name,'OrderName'); 
        
        
        csord__Order__c order = new csord__Order__c(
                                                    name='Test Order', 
                                                    Status__c='New',
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = orqList[0].id,
                                                    csordtelcoa__Opportunity__c=opp1.id,                                                    
                                                    Customer_Required_Date__c = Date.newInstance(2016,08,01));
        insert order;
        
        //csord__Service__c ser = new csord__Service__c(Name = 'Test Service',csord__Identification__c = 'Test-Catlyne-4238362',csord__Order_Request__c = orqList[0].id,csord__Subscription__c = subList[0].id,Billing_Commencement_Date__c = System.Today(),RAG_Status_Red__c = false,RAG_Reason_Code__c = '',csord__Order__c = order.id);
         //insert ser;
        
        
        System.assertEquals(order.name,'Test Order'); 
        
        Test.startTest();
        
      P2A_TestFactoryCls.SetupTestData();
      
  
      ApexPages.StandardController sc = new ApexPages.StandardController(opp1);
      OpportunityInFlightExtension op = new OpportunityInFlightExtension(sc);
          
      OpportunityInFlightExtension.OrderWrapper oppex = new OpportunityInFlightExtension.OrderWrapper(order);
      List<OpportunityInFlightExtension.OrderWrapper> wrpList = new List<OpportunityInFlightExtension.OrderWrapper>();
        wrpList.add(oppex);
      
      OpportunityInFlightExtension oppex1 = new OpportunityInFlightExtension(sc);
      oppex.selected=true;
      oppex1.loadOrders();
      
      op.orderWrappers = wrpList;
      try{
      op.selectChangeOrders();
      }catch(exception e){
         
         ErrorHandlerException.ExecutingClassName='OpportunityInFlightExtensionTest:OpportunityInFlightExtension_Test';         
         ErrorHandlerException.sendException(e); 
         system.debug('Exception' +e);
      }
      //
     
      Test.stopTest();
         
     } 
       
     
}