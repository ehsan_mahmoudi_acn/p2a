global with sharing class CS_ASBRNNIGroupLookup extends cscfga.ALookupSearch {
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){

        String scenario = searchFields.get('Scenario');
        String nniType = searchFields.get('Type Of NNI');
        String asbrType = searchFields.get('Type Of ASBR');
        String csProviderCalculated = searchFields.get('CS Provider Calculated');

        List<CS_NNI_Group__c> returnList = new  List<CS_NNI_Group__c> ();
        Set<Id> provIds = new Set<Id>();
        List<CS_NNI_Service_NNI_Group__c> providerList;
        if (asbrType == 'Standard') {
            providerList = [SELECT CS_NNI_Group__c, CS_NNI_Service__r.NNI_Type__c 
                            FROM CS_NNI_Service_NNI_Group__c
                            WHERE CS_NNI_Service__r.NNI_Type__c = :nniType
                            AND ASBR1__c = true];

        } else { 
            if(scenario == 'NAS'){
                providerList = [SELECT CS_NNI_Group__c, CS_NNI_Service__r.NNI_Type__c 
                                FROM CS_NNI_Service_NNI_Group__c
                                WHERE CS_NNI_Service__r.NNI_Type__c = :nniType
                                AND NAS1__c = true];
            }else{
                providerList = [SELECT CS_NNI_Group__c, CS_NNI_Service__r.NNI_Type__c 
                                FROM CS_NNI_Service_NNI_Group__c
                                WHERE CS_NNI_Service__r.NNI_Type__c = :nniType
                                AND Customer_NNI1__c = true];//AND Standalone_ASBR__c = true
            }
        }
        for(CS_NNI_Service_NNI_Group__c item : providerList){
            provIds.add(item.CS_NNI_Group__c);
        }
        
        returnList = [SELECT Id, Name, CS_Provider__c, City_Code__c,NNI_Product_Id__c,Path_Id__c FROM CS_NNI_Group__c WHERE id in :provIds and CS_Provider__c = :csProviderCalculated];
        

        return returnList;
    }

    public override String getRequiredAttributes(){ 
        return '["Scenario", "CS Provider Calculated", "Type Of NNI", "Type Of ASBR"]';
    }

    
    
}