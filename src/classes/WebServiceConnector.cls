public virtual class WebServiceConnector {
    protected String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
    protected String xsi = 'http://www.w3.org/2001/XMLSchema-instance';
    protected String wsa = 'http://www.w3.org/2005/08/addressing';
    protected String wsse = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';

    protected DOM.Document request(DOM.Document doc, String endPoint, String soapAction) {
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(endPoint);
        req.setTimeout(20000);
        req.setHeader('Content-Type', 'text/xml');
        req.setHeader('SoapAction',soapAction);
        req.setBodyDocument(doc);
        
        Http http = new Http();
        
        DateTime startTime = System.now();
        HttpResponse res = http.send(req);
      //  System.Debug('Time taken to get the response is :: '+(System.now().getTime()-startTime.getTime())+'ms');
        
       // System.Debug('Response = '+res.getBodyDocument().toXmlString());
        //System.assertEquals(200, res.getStatusCode());
        if(res!=null){
        return res.getBodyDocument(); 

        } else {
        return null;
        }      
    }
    protected DOM.XmlNode createDocumentBody(DOM.Document doc, Map<String, String> nameSpaces) {
        dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'soapenv');
        for (String prefix : nameSpaces.keySet())
        {
            envelope.setNamespace(prefix, nameSpaces.get(prefix));
        }
        // envelope.setNamespace('ord',ord);
        // envelope.setNamespace('ord1',ord1);
        //envelope.setNamespace('com',com);
        dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);
        dom.XmlNode security = header.addChildElement('Security',wsse, 'wsse');
        security.setAttributeNS('mustUnderstand','1',soapNS,null);
        dom.XmlNode usernameToken = security.addChildElement('UsernameToken',wsse,null);
        usernameToken.addChildElement('Username',wsse,null).addTextNode('admin');
        dom.XmlNode password = usernameToken.addChildElement('Password',wsse,null);
        password.setAttribute('Type', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText');
        password.addTextNode('admin');
        dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
        return body;
    }

}