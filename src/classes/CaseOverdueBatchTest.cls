@isTest
private class CaseOverdueBatchTest{
    
    public static List<Account> acclist = P2A_TestFactoryCls.getAccounts(1);
    public static List<case> caselist =  P2A_TestFactoryCls.getcase(1,acclist);
    
    static testMethod void unitTest() 
    {
        caselist[0].Status ='Unassigned';
        caselist[0].Is_Feasibility_Request__c = true;
        caselist[0].Is_Resource_Reservation__c = true ;
        caselist[0].Send_Overdue_Email__c  = false;
        caselist[0].type = 'enrichment';
        Date d = date.today(); 
        caselist[0].Due_Date__c = d.adddays(-1);
        update caselist;
        
        Test.startTest();
        CaseOverdueBatch  cleanorch = new CaseOverdueBatch (); 
        cleanorch.execute(null,CaseList);
        Database.executeBatch(cleanorch);
        Test.stopTest();
        
        List<case> updateCase = [select id,Send_Overdue_Email__c  from case where id =:CaseList.get(0).id limit 1];
        updateCase.get(0).Send_Overdue_Email__c = true;
        update updateCase.get(0);
        Integer i = [SELECT COUNT() FROM case];
        System.assertEquals(i, 1);
    }
}