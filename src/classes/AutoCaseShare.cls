public class AutoCaseShare{
List<CaseShare> CaseShares = new List<CaseShare>();

// Prasad : 21-June-2017 - TGP0041763 – Sharing Rules Implementation - Starts
@future
public static void insertCustomCaseShares(Set<Id> setCaseIds){

List<Case> lstCases = new List<Case>();
Map<Id,Id> mapCaseIdToAccountOwnerId = new Map<Id,Id>();
Map<Id,Id> mapCaseIdToOpttyOwnerId = new Map<Id,Id>();
List<CaseShare> lstCaseShares = new List<CaseShare>();


if(!setCaseIds.isEmpty()){
    lstCases = [Select Id,Account.OwnerId, Opportunity_Name__r.OwnerId from Case where Id in :setCaseIds ];
}
if(!lstCases.isEmpty()){
  for(Case c : lstCases){
    mapCaseIdToAccountOwnerId.put(c.Id,c.Account.OwnerId);
    mapCaseIdToOpttyOwnerId.put(c.Id,c.Opportunity_Name__r.OwnerId);
  }
}


for(case t : lstCases){

/** Create a new Action_Item_Share record to be inserted in to the Action_Item_Share table. **/
//CaseShare caseCreatedBy = new CaseShare();
CaseShare caseAccountOwner = new CaseShare();
CaseShare caseOppOwner = new CaseShare();

/** Populate the Order_Share record with the ID of the record to be shared. **/
//caseCreatedBy.CaseId = t.Id;
caseAccountOwner.CaseId = t.Id;
caseOppOwner.CaseId = t.Id;

//Debug Testing
//system.debug('Sid.OwnerId'+t.CreatedById);

/** Share to - case Creator **/ 
//caseCreatedBy.UserOrGroupId = t.CreatedById;
/** Specify that the Apex sharing reason **/
//caseCreatedBy.RowCause = Schema.CaseShare.RowCause.Creator__c;
//caseCreatedBy.RowCause =  Schema.CaseShare.RowCause.Owner; 

/** Share to - Account Owner **/ 
caseAccountOwner.UserOrGroupId = mapCaseIdToAccountOwnerId.get(t.Id); // Prasad : 20-June-2017
system.debug('Account.OwnerId :'+mapCaseIdToAccountOwnerId.get(t.Id)); // Prasad : 20-June-2017

/** Specify that the Apex sharing reason **/
//caseAccountOwner.RowCause = Schema.CaseShare.RowCause.ImplicitChild;

/** Share to - Related Opportunity Owner **/ 
caseOppOwner.UserOrGroupId = mapCaseIdToOpttyOwnerId.get(t.Id); // Prasad : 20-June-2017
system.debug('Opportunity_Name__r.OwnerId :'+mapCaseIdToOpttyOwnerId.get(t.Id)); // Prasad : 20-June-2017
/** Specify that the Apex sharing reason **/
//caseOppOwner.RowCause = Schema.CaseShare.RowCause.ImplicitChild;

   
/** Specify that the AI should have edit/read access for this particular Action_Item record. **/
//caseCreatedBy.CaseAccessLevel = 'Edit';
caseAccountOwner.CaseAccessLevel = 'Read';
caseOppOwner.CaseAccessLevel = 'Read';

 /** Add the new Share record to the list of new Share records. **/
//CaseShares.add(caseCreatedBy); // Prasad : 21-June-2017
if(!mapCaseIdToAccountOwnerId.isEmpty() && mapCaseIdToAccountOwnerId.get(t.Id)!= null ){
    lstCaseShares.add(caseAccountOwner);
}

if(!mapCaseIdToOpttyOwnerId.isEmpty() && mapCaseIdToOpttyOwnerId.get(t.Id)!= null ){
    lstCaseShares.add(caseOppOwner);
}
      System.debug('Apex sharing2 : ' + lstCaseShares);

}
System.debug('Apex sharing1');

/** Insert all of the newly created Share records and capture save result **/
if(!lstCaseShares.isEmpty()){
    Database.SaveResult[] jobShareInsertResult = Database.insert(lstCaseShares,false);
}

}

// Prasad : 21-June-2017 - TGP0041763 – Sharing Rules Implementation - Ends

/** For Each record in the trigger perform the sharing tasks below **/
public void customShare(List<case> newCases){

// Prasad : 20-June-2017 - Starts
List<Case> lstCases = new List<Case>();
Set<Id> setCaseIds = new Set<Id>();
Map<Id,Id> mapCaseIdToAccountOwnerId = new Map<Id,Id>();
Map<Id,Id> mapCaseIdToOpttyOwnerId = new Map<Id,Id>();
// Prasad : 20-June-2017 - Ends

for(Case c : newCases){
  setCaseIds.add(c.Id);
}

if(!setCaseIds.isEmpty()){
    lstCases = [Select Id,Account.OwnerId, Opportunity_Name__r.OwnerId from Case where Id in :setCaseIds ];
}
if(!lstCases.isEmpty()){
  for(Case c : lstCases){
    mapCaseIdToAccountOwnerId.put(c.Id,c.Account.OwnerId);
    mapCaseIdToOpttyOwnerId.put(c.Id,c.Opportunity_Name__r.OwnerId);
  }
}
// Prasad : 20-June-2017 - Ends

for(case t:newCases){

/** Create a new Action_Item_Share record to be inserted in to the Action_Item_Share table. **/
CaseShare caseCreatedBy = new CaseShare();
CaseShare caseAccountOwner = new CaseShare();
CaseShare caseOppOwner = new CaseShare();

/** Populate the Order_Share record with the ID of the record to be shared. **/
caseCreatedBy.CaseId = t.Id;
caseAccountOwner.CaseId = t.Id;
caseOppOwner.CaseId = t.Id;

//Debug Testing
system.debug('Sid.OwnerId'+t.CreatedById);

/** Share to - case Creator **/ 
caseCreatedBy.UserOrGroupId = t.CreatedById;
/** Specify that the Apex sharing reason **/
//caseCreatedBy.RowCause = Schema.CaseShare.RowCause.Creator__c;
//caseCreatedBy.RowCause =  Schema.CaseShare.RowCause.Owner; 

/** Share to - Account Owner **/ 
caseAccountOwner.UserOrGroupId = mapCaseIdToAccountOwnerId.get(t.Id); // Prasad : 20-June-2017
system.debug('Account.OwnerId :'+mapCaseIdToAccountOwnerId.get(t.Id)); // Prasad : 20-June-2017

/** Specify that the Apex sharing reason **/
//caseAccountOwner.RowCause = Schema.CaseShare.RowCause.ImplicitChild;

/** Share to - Related Opportunity Owner **/ 
caseOppOwner.UserOrGroupId = mapCaseIdToOpttyOwnerId.get(t.Id); // Prasad : 20-June-2017
system.debug('Opportunity_Name__r.OwnerId :'+mapCaseIdToOpttyOwnerId.get(t.Id)); // Prasad : 20-June-2017
/** Specify that the Apex sharing reason **/
//caseOppOwner.RowCause = Schema.CaseShare.RowCause.ImplicitChild;

   
/** Specify that the AI should have edit/read access for this particular Action_Item record. **/
caseCreatedBy.CaseAccessLevel = 'Edit';
caseAccountOwner.CaseAccessLevel = 'Read';
caseOppOwner.CaseAccessLevel = 'Read';

 /** Add the new Share record to the list of new Share records. **/
//CaseShares.add(caseCreatedBy); // Prasad : 21-June-2017
CaseShares.add(caseAccountOwner);
CaseShares.add(caseOppOwner);
System.debug('Apex sharing2 : ' + CaseShares);

}
System.debug('Apex sharing1');
/** Insert all of the newly created Share records and capture save result **/
//Database.SaveResult[] jobShareInsertResult = Database.insert(CaseShares,false);
//List<CaseShare> lstNewShares = new List<CaseShare>();
List<Group> lstGroups = new List<Group>();
Set<Id> setGroupIds = new Set<Id>();

try{
insert CaseShares; 
//lstNewShares = [Select Id, CaseId, UserOrGroupId, UserOrGroup.Name, RowCause, CaseAccessLevel from CaseShare where CaseId in :setCaseIds ];
//System.debug('Inserted Case Shares : ' + lstNewShares);
for(CaseShare cs : [Select Id, CaseId, UserOrGroupId, UserOrGroup.Name, RowCause, CaseAccessLevel from CaseShare where CaseId in :setCaseIds ]){
    if(cs.RowCause == 'Rule'){
     setGroupIds.add(cs.UserOrGroupId);
    }
}
lstGroups = [Select Id, Name, CreatedDate, CreatedBy.Id from Group where Id in : setGroupIds];
System.Debug('***+++*** Group Details : ' + lstGroups);

}catch(Exception e){
    ErrorHandlerException.ExecutingClassName='AutoCaseShare:CustomShare';
    ErrorHandlerException.objectList=newCases;
    ErrorHandlerException.sendException(e);
 System.debug('Exception : ' + e.getMessage());
}
//system.debug('jobShareInsertResult'+jobShareInsertResult);
}

}