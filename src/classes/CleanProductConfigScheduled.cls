global class CleanProductConfigScheduled implements Schedulable{
    global void execute(SchedulableContext sc){
        String query = 'select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c != null and LastModifiedDate < LAST_N_DAYS:5';
        CleanProductConfigBatch delBatch = new CleanProductConfigBatch(query);
        Id BatchProcessId = Database.ExecuteBatch(delBatch, 1);
    }
}