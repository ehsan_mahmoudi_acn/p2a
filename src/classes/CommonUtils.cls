/**
* @company      
* @author       
* @date         May 2016
* @description  Provides helper methods to do common operations in reusable way
*/
public class CommonUtils {
    
    
    /**
    * @description  Return values of specified field in records
    * @param        results AggregateResult records
    * @param        alias Aggregate expression alias
    * @return       field values set
    */
    public static Set<ID> getIDFieldValues(List<SObject> records, SObjectField fld) {
        if (records == null || records.isEmpty() || fld == null) return null;
        
        Set<ID> values = new Set<ID>();
        for (SObject record : records) {
            Id id = (ID) record.get(fld);
            if (id != null) values.add(id);
        }
        
        return values;
    }
    
    public static Map<ID, List<SObject>> groupByIDField(List<SObject> records, SObjectField fld) {
        if (records == null || records.isEmpty() || fld == null) return null;
        
        Map<ID, List<SObject>> result = new Map<ID, List<SObject>>();
        for (SObject record : records) {
            Id id = (ID) record.get(fld);
            if (id == null) continue;
            
            if (result.containsKey(id)) result.get(id).add(record); 
            else result.put(id, new List<SObject>{ record }); 
        }
        
        return result;
    }
    
    public static void setFieldValues(List<SObject> records, Map<SObjectField, Object> fldValues) {
        if (records == null || records.isEmpty() || fldValues == null || fldValues.isEmpty()) return;
    
        for (SObject record : records) {
            for (SObjectField fld : fldValues.keySet()) {
                record.put(fld, fldValues.get(fld));
            }
        }
    }

}