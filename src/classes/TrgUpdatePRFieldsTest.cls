/**
    @author - Accenture
    @date - 20- Jun-2012
    @version - 1.0
    @description - This is the test class for OrderLineItemAfterUpdate Trigger.**/

@isTest(SeeAllData = false)
Public class TrgUpdatePRFieldsTest {

/*public static testMethod void testUpdatePRFeilds1(){
      
  Test.startTest();
        
  Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
  insert cntryLkupObj;
              
  
  PriceBook2 pricebook2Obj = new PriceBook2(Name='Test');
  insert pricebook2obj; 

  Product2 product2obj = new Product2(Name = 'GFTS - enepath',ProductCode = 'GFTS-e',Product_ID__c ='IPT',
                                           Installment_NRC_Bill_Text__c='test',IsActive=true,ETC_Bill_Text__c='testing',
                                           MRC_Bill_Text__c='test123',NRC_Bill_Text__c='working',
                                           Root_Product_Bill_Text__c='Dell',Service_Bill_Text__c='service',
                                           RC_Credit_Bill_Text__c='credit',NRC_Credit_Bill_Text__c='NRcredit'); 
  insert product2obj;   
  
  PricebookEntry pricebookobj = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(),Product2Id =  product2obj.Id,
                                                        IsActive = true, CurrencyIsoCode='USD',
                                                        UseStandardPrice=false, UnitPrice=0.01);
                
  insert pricebookobj; 
  List<Resource__c> resourceList = new List<Resource__c>();
  Resource__c resObj = new Resource__c(NRC_Credit_Bill_Text__c='hdghj',NRC_Bill_Text__c='eytfduwie',
                                  Product_Code__c='hgdjhs',Root_Product_Bill_Text__c='hgh',
                                  Service_Bill_Text__c='jhgfjf',RC_Credit_Bill_Text__c='hdsgfjshd',
                                 Installment_NRC_Charge_ID__c='test',Installment_NRC_duration__c='test',
                                 Installment_NRC_Amount_per_month__c= 1.00,Parent_Charge_Id__c='working',
                                 Charge_Id__c='Yes',NRC_Charge_ID__c='test',Parent_Bundle_Flag__c=true,
                                 Bundle_Flag__c=true,Bundle_Label_name__c='test',Zero_Charge_ETC_Flag__c=true,
                                 Zero_Charge_MRC_Flag__c=true,Zero_Charge_NRC_Flag__c=true);
       
    
  insert resObj;  
   
        
  Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
  insert accObj;  
  
  
    
  Opportunity oppObj = new Opportunity(Name='GFTS Ph 2',AccountID=accObj.Id,Opportunity_Type__c='Simple',
                                           CurrencyIsoCode = 'USD', CloseDate = Date.today(),StageName='Identify & Define',Stage__c='Identify & Define',
                                            QuoteStatus__c ='Order',Sales_Status__c= 'Won',Win_Loss_Reasons__c ='Product',
                                            Order_Type__c= 'New', Estimated_MRC__c=8,
                                       Estimated_NRC__c=2,ContractTerm__c='1',TigFin_PR_Actions__c='TigFin_PR_Actions__c');
                                      
  insert oppObj;
    
  Service__c serviceObj = new Service__c(Name='test',AccountId__c=accObj.Id, Is_GCPE_shared_with_multiple_services__c='test',
                                        CurrencyIsoCode='USD') ;
   
  insert serviceObj;  
    
  OpportunityLineItem   opplineobj = new OpportunityLineItem(OpportunityId = oppobj.Id,IsMainItem__c = 'Yes',
                              Quantity = 5,UnitPrice = 10,PricebookEntryId = pricebookobj.Id,ContractTerm__c = '12',
                              OffnetRequired__c = 'Yes',Location__c = 'Bangalore',
                              Is_GCPE_shared_with_multiple_services__c = 'NA',CPQItem__c = '1', 
                              Installment_NRC_Amount_per_month__c= 12.23,OrderType__c ='Terminate',
                               Type_of_Professional_Service__c='other',What_Handset_Type_is_required__c='other',
                               ParentCPQItem__c='working', Select_committed_circuit_bandwidth__c ='N/A',
                              Is_Standalone_GCPE__c =true,CartItemGuid__c='test',Resource__c=resObj.Id,
                ServiceId__c=serviceObj.Id);  
    
  
  insert opplineobj;
  //List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>();
  //oppLineItemList.add(opplineobj);
 
    
  Map<Id,OpportunityLineItem> oppLineItemMap=new Map<Id,OpportunityLineItem>();  
  oppLineItemMap.put(opplineobj.id,opplineobj);
 // insert  oppLineItemMap;
      
  Opportunity_Line_Item1__c oppLineItem1Obj = new Opportunity_Line_Item1__c(Opportunity__c=oppObj.Id,
                                         CurrencyIsoCode='USD',CartItemGuid__c='test',Is_Bundle__c=true,
                                       Bundle_Label_Name__c='working',Bundle_Type__c='testing' );
  
  insert oppLineItem1Obj   ;
   
  Map<String,Opportunity_Line_Item1__c> oppLineItem1Map=new Map<String,Opportunity_Line_Item1__c>();
  oppLineItem1Map.put('12314', oppLineItem1Obj);
  

  Map<Id,Resource__c> resourceMap = new Map<Id,Resource__c>(); 
    
  Test.stopTest();
  
   
    
} 
static testMethod void testUpdatePRFeilds2()
{
   Test.startTest();
   
   Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
   insert cntryLkupObj;
               
   City_Lookup__c cityObj = new City_Lookup__c(Name ='Bangalore',City_Code__c='Beijing',
                                                    Generic_Site_Code__c ='HK#');
   insert cityObj;
        
   Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
   insert accObj; 
   
   Opportunity oppObj = new Opportunity(Name='GFTS Ph 2',AccountID=accObj.Id,Opportunity_Type__c='Simple',
                                           CurrencyIsoCode = 'USD', CloseDate = Date.today(),StageName='Identify & Define',Stage__c='Identify & Define',
                                            QuoteStatus__c ='Order',Sales_Status__c= 'Won',Win_Loss_Reasons__c ='Product',
                                            Order_Type__c= 'New', Estimated_MRC__c=8,
                                       Estimated_NRC__c=2,ContractTerm__c='1',TigFin_PR_Actions__c='TigFin_PR_Actions__c');
                                      
   insert oppObj; 
       
   Opp_Product_Type__c oppProdTypeObj = new Opp_Product_Type__c(Name='test',CurrencyIsoCode='USD',Opportunity__c=oppObj.Id);
   insert oppProdTypeObj;
    
      
    
   Site__c siteObj = new Site__c(Name='Test_site',Address1__c='43',Address2__c='Bangalore',Country_Finder__c=cntryLkupObj.Id,
                             City_Finder__c=cityObj.Id,AccountId__c=accObj.Id,Address_Type__c='Billing Address');
   insert siteObj;
   
   BillProfile__c billProfObj = new BillProfile__c(Billing_Entity__c='Telstra Limited',Account__c=siteObj.AccountId__c,
                                              Bill_Profile_Site__c=siteObj.Id, Start_Date__c= Date.today(),
                                             Invoice_Breakdown__c='Summary Page', First_Period_Date__c=Date.today(),
                                             Status__c='Active'   );
  
   insert   billProfObj; 
        
   CostCentre__c costCenObj = new CostCentre__c(Name='test Cost',BillProfile__c=billProfObj.Id);
   insert costCenObj;
  
   Test.stopTest();
    }*/  
}