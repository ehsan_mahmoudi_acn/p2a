@isTest(SeeAllData = false)
public class SubmitOrderTest{
public static List<csord__Order__c> Orderlist;
public static List<csord__Order_Request__c > OrdReqList;
private static List<cscfga__Product_Configuration__c> pclist;
private static List<Product_Definition_Id__c> PdIdlist;
private static List<cscfga__Product_Definition__c> Pdlist;
private static List<Offer_Id__c>offIdlist;
private static List<NewLogo__c> NewLogolist;
private static List<Account> Accountslist;

private static void initTestData(){
    
    OrdReqList= new List<csord__Order_Request__c>{ new csord__Order_Request__c(
    Name = 'OrderName',
    csord__Module_Name__c = 'SansaStark123683468Test',
    csord__Module_Version__c = 'YouknownothingJhonSwon',
    csord__Process_Status__c = 'Requested',
    csord__Request_DateTime__c = System.now())
    };

    insert OrdReqList;
    
    System.assertEquals(OrdReqList[0].Name ,'OrderName'); 



    Orderlist = new List<csord__Order__c>{ new csord__Order__c(

    csord__Identification__c = 'Test-JohnSnow-4238362',
    csord__Order_Request__c = OrdReqList[0].id
    )
    };

insert Orderlist;

    System.assertEquals(Orderlist[0].csord__Identification__c ,'Test-JohnSnow-4238362'); 


    
      try{
     offIdlist = new List<Offer_Id__c>{
                 new Offer_Id__c(name ='Master_IPVPN_Service_Offer_Id',Offer_Id__c = 'null'),
                 new Offer_Id__c(name ='Master_VPLS_Transparent_Offer_Id',Offer_Id__c = 'null'),
                 new Offer_Id__c(name ='Master_VPLS_VLAN_Offer_Id',Offer_Id__c = 'null')
       };
     }catch (Exception e) {
       }
     
     insert offIdlist;
     
     System.assertEquals(offIdlist[0].name ,'Master_IPVPN_Service_Offer_Id'); 

     NewLogolist = new List<NewLogo__c>{
                    new NewLogo__c(name = 'Active',Action_Item__c = 'Active'),
                    new NewLogo__c(name = 'Action_Item__c',Action_Item__c = 'Action_Item__c'),
                    new NewLogo__c(name = 'Logo_Approved',Action_Item__c = 'Logo_Approved'),
                    new NewLogo__c(name = 'Logo_Customer',Action_Item__c = 'Logo_Customer'),
                    new NewLogo__c(name = 'Logo_Former_Customer',Action_Item__c = 'Logo_Former_Customer'),
                    new NewLogo__c(name = 'Logo_New_Customer',Action_Item__c = 'Logo_New_Customer'),
                    new NewLogo__c(name = 'Logo_New_Logo',Action_Item__c = 'Logo_New_Logo'),
                    new NewLogo__c(name = 'Logo_Prospect',Action_Item__c = 'Logo_Prospect'),
                    new NewLogo__c(name = 'Logo_Rejected',Action_Item__c = 'Logo_Rejected')                      
       };
       Insert NewLogolist;
       
      System.assertEquals(NewLogolist[0].name ,'Active'); 
      
       
     Pdlist = new List<cscfga__Product_Definition__c>{
              new cscfga__Product_Definition__c(name  = 'Master IPVPN Service',cscfga__Description__c = 'Test master IPVPN'),
              new cscfga__Product_Definition__c(name  = 'Master VPLS Service', cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'VLANGroup_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'IPVPN_Port_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'SMA_Gateway_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'VPLS_Transparent_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'VPLS_VLAN_Port_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'Master_IPVPN_Service_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'Master_VPLS_Service_Definition_Id',cscfga__Description__c = 'Test master VPLS')
            };
     insert Pdlist;      
     System.assertEquals('Master IPVPN Service',Pdlist[0].Name);
     
     PdIdlist = new List<Product_Definition_Id__c>{
                new Product_Definition_Id__c(name = 'Master IPVPN Service',Product_Id__c = Pdlist[0].Id),
                new Product_Definition_Id__c(name = 'Master VPLS Service',Product_Id__c = Pdlist[1].id),
                new Product_Definition_Id__c(name = 'VLANGroup_Definition_Id',Product_Id__c = Pdlist[2].id),
                new Product_Definition_Id__c(name = 'IPVPN_Port_Definition_Id',Product_Id__c = Pdlist[3].id),
                new Product_Definition_Id__c(name = 'SMA_Gateway_Definition_Id',Product_Id__c = Pdlist[4].id),
                new Product_Definition_Id__c(name = 'VPLS_Transparent_Definition_Id',Product_Id__c = Pdlist[5].id),
                new Product_Definition_Id__c(name = 'VPLS_VLAN_Port_Definition_Id',Product_Id__c = Pdlist[6].id),
                new Product_Definition_Id__c(name = 'Master_IPVPN_Service_Definition_Id',Product_Id__c = Pdlist[7].id),
                new Product_Definition_Id__c(name = 'Master_VPLS_Service_Definition_Id',Product_Id__c = Pdlist[8].id)
     };
     insert PdIdlist;
     System.assertEquals('Master VPLS Service',PdIdlist[1].Name); 
       
     List<cscfga__Product_Basket__c> pblist =  P2A_TestFactoryCls.getProductBasket(9);  
         
     pclist= new List<cscfga__Product_Configuration__c>{
             new cscfga__Product_Configuration__c(Name = 'productConfiguration',cscfga__Product_Definition__c = Pdlist[0].id,cscfga__Product_Basket__c = pblist[0].Id,cscfga__Product_Family__c = 'Test family',Product_Name__c = 'ATM'),
             new cscfga__Product_Configuration__c(Name = 'productConfiguration1',cscfga__Product_Definition__c = Pdlist[1].id,cscfga__Product_Basket__c = pblist[1].Id,cscfga__Product_Family__c = 'Test family1'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[2].id,cscfga__Product_Basket__c = pblist[2].Id,cscfga__Product_Family__c = 'Test family2'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[3].id,cscfga__Product_Basket__c = pblist[3].Id,cscfga__Product_Family__c = 'Test family3'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[4].id,cscfga__Product_Basket__c = pblist[4].Id,cscfga__Product_Family__c = 'Test family4'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[5].id,cscfga__Product_Basket__c = pblist[5].Id,cscfga__Product_Family__c = 'Test family5'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[6].id,cscfga__Product_Basket__c = pblist[6].Id,cscfga__Product_Family__c = 'Test family6'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[7].id,cscfga__Product_Basket__c = pblist[7].Id,cscfga__Product_Family__c = 'Test family7'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[8].id,cscfga__Product_Basket__c = pblist[8].Id,cscfga__Product_Family__c = 'Test family8')
            };
     insert pclist;      
     System.assertEquals('Test family2',pclist[2].cscfga__Product_Family__c); 
     
     Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
     insert cntryLkupObj;
     
    try{ 
     Accountslist = new List<Account>{
                      //new Account(name = 'TestAccount',Account_Status__c = 'Active',Selling_Entity__c = 'Telstra Limited',Customer_Type_New__c = 'MNC',
                      //Customer_Legal_Entity_Name__c ='test',Account_Verified__c = true,Activated__c = true,Country__c = cntryLkupObj.id,Industry = 'Education',Account_ORIG_ID_DM__c = 'OrigAccountId',Type = 'Logo_Former_Customer',Account_ID__c = 'ExternalIds'),
                      new Account(name = 'TestAccount1',Account_Type__c = 'Logo_Former_Customer',Selling_Entity__c = 'Telstra Limited',Customer_Type_New__c = 'MNC',
                      Customer_Legal_Entity_Name__c ='test',Account_Verified__c = true,Activated__c = true,Account_Status__c = 'Active',Country__c = cntryLkupObj.id,Industry = 'Education',Account_ORIG_ID_DM__c = 'OrigAccountId',Type = 'Logo_Former_Customer',Account_ID__c = '10610'),
                      new Account(name = 'TestAccount2',Account_Type__c = 'Logo_Customer',Selling_Entity__c = 'Telstra Limited',Customer_Type_New__c = 'MNC',
                      Customer_Legal_Entity_Name__c ='test',Account_Verified__c = true,Activated__c = true,Account_Status__c = 'Active',Country__c = cntryLkupObj.id,Industry = 'Education',Account_ORIG_ID_DM__c = 'OrigAccountId',Type = 'Logo_Customer',Account_ID__c = 'ExternalIds'),
                      new Account(name = 'TestAccount3',Account_Type__c = 'Logo_Prospect',Selling_Entity__c = 'Telstra Limited',Customer_Type_New__c = 'MNC',
                      Customer_Legal_Entity_Name__c ='test',Account_Verified__c = true,Activated__c = true,Account_Status__c = 'Active',Country__c = cntryLkupObj.id,Industry = 'Education',Account_ORIG_ID_DM__c = 'OrigAccountId',Type = 'Logo_Prospect',Account_ID__c = 'ExternalIds')
                     };
                     
         }catch (Exception e) {
       } 
      Insert Accountslist;
      
      System.assertEquals(Accountslist[0].name ,'TestAccount1'); 
        
        
        City_Lookup__c citylkp = new City_Lookup__c();
        citylkp.Name = 'JALANDHAR';
        citylkp.City_Code__c = 'JUC';
        citylkp.Generic_Site_Code__c = 'JUC&';
        citylkp.Country__c = 'INDIA';
        citylkp.State_Province__c = 'KOMAROM-ESZTERGOM';
        Insert citylkp;
        
       System.assertEquals(citylkp.name ,'JALANDHAR'); 
       
        
         Site__c sites = new Site__c();
            sites.Name = 'TestSite';
            sites.AccountId__c = Accountslist[0].id;
            sites.Address_Type__c = 'Billing Address' ;
            sites.Address1__c = 'AddressTest';
            sites.Country_Finder__c = cntryLkupObj.id;
            Insert sites;
            
       System.assertEquals(sites.name ,'TestSite'); 
           

        List<csord__Order_Request__c> csoreq =  P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1, csoreq);
        
         csord__Order__c ord = new csord__Order__c();
         ord.Name = 'ODR - ON00022313';
         //csord__Order_Number__c = 
         //ord.csord__Account__r.Account_ID__c = Accountslist.get(0).id;
         ord.Customer_Turn_Up_Test_Date__c =  date.today();
         ord.csord__Identification__c = 'Identification';
         ord.csord__Order_Type__c = 'Subscription Creation';
         ord.Order_Type__c = 'New';
         ord.Error_Description__c = 'Description';
         ord.csord__Identification__c = 'Test-JohnSnow-4238362';
         ord.csord__Order_Request__c= csoreq[0].id;
         ord.csord__Account__c = Accountslist.get(0).id;
         insert ord;
     
       System.assertEquals(ord.name ,'ODR - ON00022313'); 
        
       
       
        csord__Service__c ser = new csord__Service__c();
            ser.Name = 'Test Service'; 
            ser.csord__Identification__c = 'Test-Catlyne-4238362';
            ser.csord__Order_Request__c = csoreq[0].id;
            ser.csord__Subscription__c = SUBList[0].id;
            ser.Billing_Commencement_Date__c = System.Today();
            ser.Stop_Billing_Date__c = System.Today();
            insert ser;
            
        System.assertEquals(ser.name ,'Test Service'); 
           
       
      csord__Service__c serv = new csord__Service__c();
            serv.Name = 'Test Service'; 
            serv.Line_Number__c = 12;
            serv.csordtelcoa__Product_Configuration__c = pclist[0].id;
            serv.Site_Contact_Number__c = '456';
            serv.Supplier_s_leadtime__c = '789';
            serv.Specific_quote_requirement__c = '546';
            serv.Carrier_Quote_obtained__c = false;
            serv.Customer_Site_Office_Hours__c = 'Hours';
            serv.Purchase_Requisition_Number__c = 'Number';
            serv.acity_side__c = sites.id;
            serv.zcity_side__c = sites.id;
            serv.csord__Service__c = ser.id;
            serv.csord__Identification__c = 'Test-Catlyne-4238362';
            serv.csord__Order_Request__c = csoreq[0].id;
            serv.csord__Subscription__c = SUBList[0].id;
            serv.Billing_Commencement_Date__c = System.Today();
            serv.Stop_Billing_Date__c = System.Today();
            upsert serv;
            system.assert(serv!=null);
        System.assertEquals(serv.name ,'Test Service'); 
            
            
            Orderlist = P2A_TestFactoryCls.getorder(1, OrdReqList);
            Service_Values__c sv= new Service_Values__c(name ='Oppurtunity_Id__c',Service_Types__c='Oppurtunity_Id__c');
            insert sv;
    
}

    private static testMethod void submitOrderToFOMTest()
    {
        
            CS_TestUtil.disableAll(UserInfo.getUserId());
            initTestData();
            Test.startTest();

            
            String ID =Orderlist[0].id;
             Test.setMock(HttpCalloutMock.class, new WebServiceConnectorMock());
            String S1,S2,S3,S4;
            SubmitOrder SubmitOrders = new SubmitOrder();
             SubmitOrder.submitOrderToFOM(ID);
            TibcoController Tibco = new TibcoController();
            //  TibcoServiceOrder order = new TibcoServiceOrder();
            TibcoServiceOrderCreator creator = new TibcoServiceOrderCreator();
            TibcoServiceOrder order = creator.CreateOrderFromServiceId(ID);
            system.assert(creator!=null);
            //SubmitOrderRequest sr = new SubmitOrderRequest();
            //sr.externalBusinessTransactionID ='12';
            //OrderRequest ors= new OrderRequest();
            //sr.OrderRequest = ors;
            Test.stopTest();
                
            
        }
}