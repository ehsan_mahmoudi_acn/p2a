@isTest(SeeAllData=false)
private class SubmitOrderBatchtest {


    /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
        system.assert(globalMute!=null);
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
        system.assert(globalMute!=null);
    }
    
  
    public static testmethod void batchtest() {
    
      Set<ID> master = new Set<ID>();
      List<Id> orderid = new List<Id>();
      List<Account> acc = P2A_TestFactoryCls.getaccounts(1);
      List<Opportunity> opp = P2A_TestFactoryCls.getopportunitys(1,acc); 
      List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
      List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(2,OrdReqList);
        
      solutions__c sol = new solutions__c();
      sol.batch_count__c = Orders.size();
      sol.Submitted_Orders__c=Orders[0].id;
      sol.Account_Name__c = acc[0].id;
      insert sol ;
      //update sol ;
      for(csord__Order__c Ord : Orders){
                  ord.Solutions__c = sol.id ;
                  orderid.add(ord.id);
                  master.add(ord.id);                 
             } 
      Orders[0].Order_Type__c='Parallel Upgrade';        
      update orders;
      List<csord__Order__c> o = [Select id,Order_Type__c from csord__Order__c where Order_Type__c = 'Parallel Upgrade'];
     // system.assertequals(orders.Order_Type__c,o[0].Order_Type__c);
      system.assert(orders!=null);
      test.starttest();
      set<Id>tempSet=new Set<ID>();
      SubmitOrderBatch submit = new SubmitOrderBatch(orderid, master,true,true);
      Database.executeBatch(submit);
      test.stoptest();
    
    }
    }