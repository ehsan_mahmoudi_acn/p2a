@isTest(SeeAllData = false)
private class ExportExcelApprovalRequestDataTest {    
    private static List<cscfga__Product_Configuration__c> pclist;
    private static List<Product_Definition_Id__c> PdIdlist;
    private static List<cscfga__Product_Definition__c> Pdlist;
    private static ApexPages.StandardController sc;
    private static ApexPages.StandardController sc1;
    private static ApexPages.StandardController sc2;    
    private static List<case>ListCase = new List<Case>();
    private static List<case>ListCase1 = new List<Case>();
    private static Boolean skip = false;
    private static List<Pricing_Approval_Request_Data__c> lstpricingApprovalData;
    private static List<Pricing_Approval_Request_Data__c> pricingApprovalData;
    
    private static void initTestData(){  
         P2A_TestFactoryCls.sampleTestData();    
         skip = true;         
        Pdlist = new List<cscfga__Product_Definition__c>{
          new cscfga__Product_Definition__c(name  = 'IPT Singlehome',cscfga__Description__c = 'Test master VPLS')          
        };
        insert Pdlist;
        List<cscfga__Product_Definition__c> Pdlist1 = new List<cscfga__Product_Definition__c>{
          new cscfga__Product_Definition__c(name  = 'Master VPLS Service',cscfga__Description__c = 'Test master IPVPN')          
        };
        insert Pdlist1;
        List<cscfga__Product_Definition__c> Pdlist2 = new List<cscfga__Product_Definition__c>{
          new cscfga__Product_Definition__c(name  = 'XYZ',cscfga__Description__c = 'Test master IPVPN')          
        };
        insert Pdlist2;   
        List<cscfga__Product_Definition__c> Pdlist3 = new List<cscfga__Product_Definition__c>{
          new cscfga__Product_Definition__c(name  = 'Internet-Onnet',cscfga__Description__c = 'Test master IPVPN')          
        };
        insert Pdlist3;    
        List<cscfga__Product_Definition__c> Pdlist4 = new List<cscfga__Product_Definition__c>{
          new cscfga__Product_Definition__c(name  = 'GVOIP',cscfga__Description__c = 'Test master IPVPN')          
        };
        insert Pdlist4;    
        List<cscfga__Product_Definition__c> Pdlist5 = new List<cscfga__Product_Definition__c>{
          new cscfga__Product_Definition__c(name  = 'Colocation',cscfga__Description__c = 'Test master IPVPN')          
        };
        insert Pdlist5;    
        
        PdIdlist = new List<Product_Definition_Id__c>{
            new Product_Definition_Id__c(name = 'Master IPVPN Service',Product_Id__c = Pdlist[0].Id)
        };
        insert PdIdlist;
        System.assertEquals('Master IPVPN Service',PdIdlist[0].Name);
        
        List<Product_Definition_Id__c> PdIdlist1 = new List<Product_Definition_Id__c>{
            new Product_Definition_Id__c(name = 'Master VPLS Service',Product_Id__c = Pdlist1[0].Id)
        };
        insert PdIdlist1;
        System.assertEquals('Master VPLS Service',PdIdlist1[0].Name);
        
        List<Product_Definition_Id__c> PdIdlist2 = new List<Product_Definition_Id__c>{
            new Product_Definition_Id__c(name = 'XYZ',Product_Id__c = Pdlist2[0].Id)
        };
        insert PdIdlist2;
        System.assertEquals('XYZ',PdIdlist2[0].Name);
        
        List<Product_Definition_Id__c> PdIdlist3 = new List<Product_Definition_Id__c>{
            new Product_Definition_Id__c(name = 'Internet-Onnet',Product_Id__c = Pdlist3[0].Id)
        };
        insert PdIdlist3;
        List<Product_Definition_Id__c> PdIdlist4 = new List<Product_Definition_Id__c>{
            new Product_Definition_Id__c(name = 'GVOIP',Product_Id__c = Pdlist4[0].Id)
        };
        insert PdIdlist4;
        List<Product_Definition_Id__c> PdIdlist5 = new List<Product_Definition_Id__c>{
            new Product_Definition_Id__c(name = 'Colocation',Product_Id__c = Pdlist5[0].Id)
        };
        insert PdIdlist5;       
        
        List<cscfga__Product_Basket__c> pblist =  P2A_TestFactoryCls.getProductBasket(10);  

        pclist= new List<cscfga__Product_Configuration__c>{
         new cscfga__Product_Configuration__c(name = 'Master IPVPN Service',cscfga__Product_Definition__c = Pdlist[0].id,cscfga__Product_Basket__c = pblist[0].Id,Hierarchy__c=String.valueOf(1),cscfga__Product_Family__c = 'Test family')
        };
        List<cscfga__Product_Configuration__c> pclist1= new List<cscfga__Product_Configuration__c>{         
         new cscfga__Product_Configuration__c(name = 'Master VPLS Service',cscfga__Product_Definition__c = Pdlist1[0].id,cscfga__Product_Basket__c = pblist[1].Id,Hierarchy__c=String.valueOf(2),cscfga__Product_Family__c = 'Test family1')         
        };
        List<cscfga__Product_Configuration__c> pclist2= new List<cscfga__Product_Configuration__c>{         
         new cscfga__Product_Configuration__c(name = 'XYZ',cscfga__Product_Definition__c = Pdlist2[0].id,cscfga__Product_Basket__c = pblist[2].Id,Hierarchy__c=String.valueOf(3),cscfga__Product_Family__c = 'Test family3')
        };
        List<cscfga__Product_Configuration__c> pclist3= new List<cscfga__Product_Configuration__c>{         
         new cscfga__Product_Configuration__c(name = 'IPT',Internet_Onnet_BillingType__c = 'Aggregated',cscfga__Product_Definition__c = Pdlist3[0].id,cscfga__Product_Basket__c = pblist[3].Id,Hierarchy__c=String.valueOf(3),cscfga__Product_Family__c = 'Test family4')
        };
        List<cscfga__Product_Configuration__c> pclist4= new List<cscfga__Product_Configuration__c>{         
         new cscfga__Product_Configuration__c(name = 'GVOIP',cscfga__Product_Definition__c = Pdlist4[0].id,cscfga__Product_Basket__c = pblist[4].Id,Hierarchy__c=String.valueOf(3),cscfga__Product_Family__c = 'Test family4')
        };
        List<cscfga__Product_Configuration__c> pclist5= new List<cscfga__Product_Configuration__c>{         
         new cscfga__Product_Configuration__c(name = 'Colocation',cscfga__Product_Definition__c = Pdlist5[0].id,cscfga__Product_Basket__c = pblist[5].Id,Hierarchy__c=String.valueOf(3),cscfga__Product_Family__c = 'Test family2')
        };
        
        insert pclist;      
        //System.assertEquals('Test family',pclist[0].cscfga__Product_Family__c); 
        
        if(pclist != null){
        
        case c = new case();
        c.Is_Feasibility_Request__c = true;
        c.Product_Configuration__c = pclist[0].id;
        c.status = 'Approved';
        c.Number_of_Child_Case__c = 2;
        c.Number_of_Child_Case_Closed__c = 1;
        c.Product_Basket__c = pblist[0].id;
        c.Reason_for_selecting_one_supplier__c = 'SingleSource';
        c.Is_Resource_Reservation__c = true;  

        case c1 = new case();
        c1.Is_Feasibility_Request__c = true;
        c1.Product_Configuration__c = pclist1[0].id;        
        c1.Product_Basket__c = pblist[1].id;
        
        case c2 = new case();
        c2.Is_Feasibility_Request__c = true;
        c2.Product_Configuration__c = pclist2[0].id;        
        c2.Product_Basket__c = pblist[2].id;
        
        case c3 = new case();
        c3.Is_Feasibility_Request__c = true;
        c3.Product_Configuration__c = pclist3[0].id;        
        c3.Product_Basket__c = pblist[3].id;
        
        case c4 = new case();
        c4.Is_Feasibility_Request__c = true;
        c4.Product_Configuration__c = pclist4[0].id;        
        c4.Product_Basket__c = pblist[4].id;
        
        case c5 = new case();
        c5.Is_Feasibility_Request__c = true;
        c5.Product_Configuration__c = pclist5[0].id;        
        c5.Product_Basket__c = pblist[5].id;        
        
                
        ListCase.add(c);
        ListCase.add(c1);
        ListCase.add(c2);
        ListCase.add(c3);
        ListCase.add(c4);
        ListCase.add(c5);
        insert ListCase;
        }
        
        lstpricingApprovalData = new  List<Pricing_Approval_Request_Data__c>();
        Pricing_Approval_Request_Data__c PricingApprovalData = new Pricing_Approval_Request_Data__c();
        PricingApprovalData.Product_Configuration__c = ListCase[0].Product_Configuration__c;
        
        PricingApprovalData.Rate_RC__c=0.00;
        PricingApprovalData.Rate_NRC__c=0.00;
        PricingApprovalData.Offer_RC__c=0.00;
        PricingApprovalData.Offer_NRC__c=0.00;
        PricingApprovalData.Approved_RC__c=120.00;
        PricingApprovalData.Approved_NRC__c=50.00;
        PricingApprovalData.Cost_RC__c=100.00;
        PricingApprovalData.Cost_NRC__c=0.00;
        PricingApprovalData.Contract_Term__c=12;
        PricingApprovalData.Product_Basket__c=ListCase[0].Product_Basket__c;
        
        
        PricingApprovalData.CurrencyIsoCode= 'USD';   
        PricingApprovalData.Product_Information__c = 'PEN, 12, AUS SITE';
        
        lstpricingApprovalData.add(PricingApprovalData);
        
        Insert lstpricingApprovalData; 
        
        //2nd prd
        List<Pricing_Approval_Request_Data__c> lstpricingApprovalData1= new  List<Pricing_Approval_Request_Data__c>();
        Pricing_Approval_Request_Data__c PricingApprovalData1 = new Pricing_Approval_Request_Data__c();
        PricingApprovalData1.Product_Configuration__c = ListCase[1].Product_Configuration__c;        
        PricingApprovalData1.Product_Basket__c=ListCase[1].Product_Basket__c;        
               
        lstpricingApprovalData1.add(PricingApprovalData1);
        
        Insert lstpricingApprovalData1; 
        
        //3rd prd
        List<Pricing_Approval_Request_Data__c> lstpricingApprovalData2= new  List<Pricing_Approval_Request_Data__c>();
        Pricing_Approval_Request_Data__c PricingApprovalData2 = new Pricing_Approval_Request_Data__c();
        PricingApprovalData2.Product_Configuration__c = ListCase[2].Product_Configuration__c;        
        PricingApprovalData2.Product_Basket__c=ListCase[2].Product_Basket__c;        
               
        lstpricingApprovalData2.add(PricingApprovalData2);
        
        Insert lstpricingApprovalData2; 
        
        //4th prd
        List<Pricing_Approval_Request_Data__c> lstpricingApprovalData3= new  List<Pricing_Approval_Request_Data__c>();
        Pricing_Approval_Request_Data__c PricingApprovalData3 = new Pricing_Approval_Request_Data__c();
        PricingApprovalData3.Product_Configuration__c = ListCase[3].Product_Configuration__c;           
        PricingApprovalData3.Product_Basket__c=ListCase[3].Product_Basket__c;        
               
        lstpricingApprovalData3.add(PricingApprovalData3);
        
        Insert lstpricingApprovalData3; 
        
        //5th prd
        List<Pricing_Approval_Request_Data__c> lstpricingApprovalData4= new  List<Pricing_Approval_Request_Data__c>();
        Pricing_Approval_Request_Data__c PricingApprovalData4 = new Pricing_Approval_Request_Data__c();
        PricingApprovalData4.Product_Configuration__c = ListCase[2].Product_Configuration__c;        
        PricingApprovalData4.Product_Basket__c=ListCase[2].Product_Basket__c;        
               
        lstpricingApprovalData2.add(PricingApprovalData4);
        
        Insert lstpricingApprovalData4; 
        
        ApexPages.currentPage().getParameters().put('id',ListCase[0].Id);

        sc = new ApexPages.StandardController(ListCase[0]);
        
        ApexPages.currentPage().getParameters().put('id',ListCase[1].Id);

        
    }    
    static testMethod void excelApprovalRequestData() {        

        initTestData();
        Test.startTest();
        exportExcelApprovalRequestData.wrapper wrp = new exportExcelApprovalRequestData.wrapper();
        wrp.id='fdfs';
        wrp.RateRc= 'sdfsd';
        wrp.RateNrc= 'dfsdf';
        wrp.OfferRc='sfsdf';
        wrp.OfferNrc='sfsdf';
        wrp.ApprovedRC='fsdf';
        wrp.ApprovedNRC='sdfsd';
        wrp.Product='fdsf';
        wrp.ProductDescription='sfdsf';
        wrp.CostRC='dfdfdf';
        wrp.CostNrc='dfsdfds';
        wrp.ContractTerm='sfdsdfsd';
        wrp.Margin='sdfsdf';
        
        exportExcelApprovalRequestData ac = new exportExcelApprovalRequestData(sc);
        ac.isExcel=true;
        ac.isCsv=true;
        ac.exportToExcel();     
        
        boolean b = true;
        String id = ApexPages.currentPage().getParameters().get('id');
        System.assert(b,id==null); 
        
        Test.stopTest();
    
    } 
    static testMethod void excelApprovalRequestData1() {  
        List<cscfga__Product_Basket__c> pblist1 =  P2A_TestFactoryCls.getProductBasket(1);  
        
        List<cscfga__Product_Definition__c> Pdlist1 = new List<cscfga__Product_Definition__c>{          
          new cscfga__Product_Definition__c(name  = 'XYZ',cscfga__Description__c = 'Test ASBR')
        }; 
        
        List<cscfga__Product_Configuration__c> pclist1= new List<cscfga__Product_Configuration__c>{
         new cscfga__Product_Configuration__c(name = 'XYZ',cscfga__Product_Definition__c = Pdlist1[0].id,cscfga__Product_Basket__c = pblist1[0].Id,cscfga__Product_Family__c = 'Test1 family')       
        };  
        
        List<Case> ListCase1 = new List<Case>();
        case c5 = new case();
        c5.Is_Feasibility_Request__c = true;
        c5.Product_Configuration__c = pclist1[0].id;        
        c5.Product_Basket__c = pblist1[0].id;       
                
        ListCase1.add(c5);
        insert ListCase1;
        
        List<Pricing_Approval_Request_Data__c> lstpricingApprovalData3= new  List<Pricing_Approval_Request_Data__c>();
        Pricing_Approval_Request_Data__c p22 = new Pricing_Approval_Request_Data__c();
        p22.Rate_RC__c = 22;
        p22.rate_nrc__c = 23;
        p22.offer_rc__c =23;
        p22.offer_nrc__c = 24;
        p22.approved_rc__c = 34;
        p22.approved_nrc__c = 45;
        p22.cost_rc__c = 45;
        p22.cost_nrc__c = 54;
        p22.contract_term__c = 6;
       // p22.margin__c = 5;
        p22.Product_Configuration__c = pclist1[0].id;
        p22.Product_Configuration__c = ListCase1[0].Product_Configuration__c;
        p22.Product_Basket__c=ListCase1[0].Product_Basket__c; 
        insert p22;
        lstpricingApprovalData3.add(p22);
        
     //   Pricing_Approval_Request_Data__c PricingApprovalData3 = new Pricing_Approval_Request_Data__c();
      //  PricingApprovalData3.Product_Configuration__c = ListCase1[0].Product_Configuration__c;          
     //   PricingApprovalData3.Product_Basket__c=ListCase1[0].Product_Basket__c;        
               
     
        
       
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('id',ListCase1[0].Id);

        ApexPages.StandardController sc1 = new ApexPages.StandardController(ListCase1[0]);
        ApexPages.currentPage().getParameters().put('id',ListCase1[0].Id);
            
        exportExcelApprovalRequestData ac = new exportExcelApprovalRequestData(sc1);  
          ac.exportToExcel(); 
         exportExcelApprovalRequestData.wrapper wrapper = new  exportExcelApprovalRequestData.wrapper();
          wrapper.id =lstpricingApprovalData3[0].id;
          wrapper.raterc = lstpricingApprovalData3[0].id;
          
        
        Test.stopTest();
        system.assertEquals(true,ac!=null); 
        system.assertEquals(true,wrapper!=null); 
    
    }  
    @istest
    public static void unitest()
    {
    P2A_TestFactoryCls.sampleTestData();
    //insert of account
       List<Account> acclist = new List<Account>();
       account a = new account();
                a.name    = 'Test Accenture ';
                a.BillingCountry  = 'GB';
                a.Activated__c  = True;
                a.Account_ID__c  = 'test';
                a.Account_Status__c = 'Active';
                a.Customer_Legal_Entity_Name__c = 'Test';
                a.Customer_Type_New__c = 'MNO';
                a.Region__c = 'Australia';
                a.Website='abc.com';
                a.Selling_Entity__c='Telstra Limited';
                a.Industry__c='BPO';
                a.Account_ORIG_ID_DM__c = 'test';
                acclist.add(a);
                insert acclist;
               system.assert(acclist[0].id!=null); 
              //  insert of opportunity 
                
                List<Opportunity> opplist = new List<Opportunity>();
                Opportunity opp = new Opportunity(); 
                opp.AccountId = acclist[0].id;
                opp.name  = 'Test Opp ';
                opp.stagename = 'Identify & Define';
                opp.CloseDate= system.today()+5;
                opp.Pre_Contract_Provisioning_Required__c = 'yes';
                opp.QuoteStatus__c = 'draft';
                opp.Product_Type__c = 'IPL';
                opp.Estimated_MRC__c = 100.00;
                opp.Estimated_NRC__c = 100.00;
                opp.Opportunity_Type__c = 'Simple';
                opp.Sales_Status__c = 'Open';
                opp.Order_Type__c = 'New';
                opp.Stage__c='Identify & Define';
                opp.ContractTerm__c='10'; 
                opp.CreditCheckDoneBy__c= 'testname';
                opplist.add(opp);
                insert opplist ;
               system.assert(opplist[0].id!=null);
               
             
                
        List<cscfga__Product_Basket__c> ProductBasketlist = new List<cscfga__Product_Basket__c>();
          cscfga__Product_Basket__c p = new cscfga__Product_Basket__c();
                p.csordtelcoa__Synchronised_with_Opportunity__c = true;
                p.Name = 'Test basket ';
                p.cscfga__Opportunity__c = opplist[0].id ;
                ProductBasketlist.add(p);
                insert ProductBasketlist ;
               system.assert(ProductBasketlist[0].id!=null);
               
       List<cscfga__Product_Definition__c> ProductDeflist = new List<cscfga__Product_Definition__c>();
        cscfga__Product_Definition__c pb = new cscfga__Product_Definition__c();
        pb.name = 'Master IPVPN Service'; 
        pb.cscfga__Description__c= 'GCPEbdkjdvbhe;ghwfguwefkjwf';
        ProductDeflist.add(pb);
        
        cscfga__Product_Definition__c pb1 = new cscfga__Product_Definition__c();
        pb1.name = 'ASBR'; 
        pb1.cscfga__Description__c= 'GCPEbdkjdvehwfguwefkjwf';
        ProductDeflist.add(pb1); 
        
        cscfga__Product_Definition__c pb2 = new cscfga__Product_Definition__c();
        pb2.name = 'Master VPLS Service'; 
        pb2.cscfga__Description__c= 'GCPEbdkjdvehwfguwefkjwf'; 
        ProductDeflist.add(pb2);
        
        cscfga__Product_Definition__c pb3 = new cscfga__Product_Definition__c();
        pb3.name = 'VLAN Group'; 
        pb3.cscfga__Description__c= 'GCPEbdkjdvehwfguwefkjwf'; 
        ProductDeflist.add(pb3);
        
        cscfga__Product_Definition__c pb4 = new cscfga__Product_Definition__c();
        pb4.name = 'TWI Singlehome'; 
        pb4.cscfga__Description__c= 'GCPEbdkjdvehwfguwefkjwf'; 
        ProductDeflist.add(pb4); 
        
        cscfga__Product_Definition__c pb5 = new cscfga__Product_Definition__c();
        pb5.name = 'TWI Multihome'; 
        pb5.cscfga__Description__c= 'GCPEbdkjdvehwfguwefkjwf'; 
        ProductDeflist.add(pb5); 
        
        cscfga__Product_Definition__c pb6 = new cscfga__Product_Definition__c();
        pb6.name = 'IPT Singlehome'; 
        pb6.cscfga__Description__c= 'GCPEbdkjdvehwfguwefkjwf'; 
        ProductDeflist.add(pb6); 
        
        cscfga__Product_Definition__c pb7 = new cscfga__Product_Definition__c();
        pb7.name = 'IPT Singlehome'; 
        pb7.cscfga__Description__c= 'GCPEbdkjdvehwfguwefkjwf'; 
        ProductDeflist.add(pb7); 
        
        cscfga__Product_Definition__c pb8 = new cscfga__Product_Definition__c();
        pb8.name = 'EVP Port'; 
        pb8.cscfga__Description__c= 'GCPEbdkjdvehwfguwefkjwf'; 
        ProductDeflist.add(pb8); 
        
        cscfga__Product_Definition__c pb9 = new cscfga__Product_Definition__c();
        pb9.name = 'GVOIP'; 
        pb9.cscfga__Description__c= 'GCPEbdkjdvehwfguwefkjwf'; 
        ProductDeflist.add(pb9);   
        
        cscfga__Product_Definition__c pb11 = new cscfga__Product_Definition__c();
        pb11.name = 'Customer Site'; 
        pb11.cscfga__Description__c= 'GCPEbdkjdvehwfguwefkjwf'; 
        ProductDeflist.add(pb11); 
        
        
        cscfga__Product_Definition__c pb12 = new cscfga__Product_Definition__c();
        pb12.name = 'IPC'; 
        pb12.cscfga__Description__c= 'GCPEbdkjdvehwfguwefkjwf'; 
        ProductDeflist.add(pb12);
        
        cscfga__Product_Definition__c pb13 = new cscfga__Product_Definition__c();
        pb13.name = 'Colocation'; 
        pb13.cscfga__Description__c= 'GCPEbdkjdvehwfguwefkjwf'; 
        ProductDeflist.add(pb13);
        
        cscfga__Product_Definition__c pb14 = new cscfga__Product_Definition__c();
        pb14.name = 'Master IPVPN Service'; 
        pb14.cscfga__Description__c= 'GCPEbdkjdvehwfguwefkjwf'; 
        ProductDeflist.add(pb14);
        
        cscfga__Product_Definition__c pb15 = new cscfga__Product_Definition__c();
        pb15.name = 'ASBR'; 
        pb15.cscfga__Description__c= 'GCPEbdkjdvehwfguwefkjwf'; 
        ProductDeflist.add(pb15);
        
        cscfga__Product_Definition__c pb16 = new cscfga__Product_Definition__c();
        pb16.name = 'Internet-Onnet'; 
        pb16.cscfga__Description__c= 'GCPEbdkjdvehwfguwefkjwf'; 
        ProductDeflist.add(pb16);
        
        
        
        insert ProductDeflist;
        
        system.assert(ProductDeflist[0].id!=null);
              
      List<cscfga__Product_Configuration__c> ProductConfiglist = new List<cscfga__Product_Configuration__c>(); 
      cscfga__Product_Configuration__c pb10 = new cscfga__Product_Configuration__c();
      pb10.Name = 'Standalone ASBR';
      pb10.Product_Name__c = 'IPT';
      pb10.cscfga__Product_Family__c ='Master IPVPN Service';
      pb10.Internet_Onnet_BillingType__c = 'Aggregated';
      pb10.cscfga__Product_Basket__c = ProductBasketlist[0].id;
      pb10.cscfga__Product_Definition__c = ProductDeflist[14].id;
      pb10.Product_Code__c = 'ASBRB';
       ProductConfiglist.add(pb10);
       insert ProductConfiglist;
       
        system.assert(ProductConfiglist[0].id!=null);
        
        List<case>  caseList = new List<case>();
                case c = new case();
                c.Status ='Unassigned';
                c.Opportunity_Name__c = opplist[0].id;
                
                //c.Account_Name__c= acclist[0].id;
               c.Product_Configuration__c = ProductConfiglist[0].id;        
               c.Product_Basket__c = ProductBasketlist[0].id;       
                c.Is_Feasibility_Request__c= true;
                c.Is_Resource_Reservation__c = true;
                caseList.add(c); 
                insert caseList;
                system.assert(caseList[0].id!=null);
       
    List<Pricing_Approval_Request_Data__c>  prequestlist = new List<Pricing_Approval_Request_Data__c>();  
       Pricing_Approval_Request_Data__c pr = new Pricing_Approval_Request_Data__c();
      // pr.Product_Basket__c =ProductBasketlist[0].id;
               // pr.Product_Configuration__c = ProductConfiglist[0].id;
                
               pr.Product_Basket__c = caseList[0].Product_Basket__c ;
               pr.Product_Configuration__c = caseList[0].Product_Configuration__c ;
                pr.Last_Approved_NRC__c = 500;
                pr.Last_Approved_RC__c = 500;
                pr.Rate_RC__c = 45;
                pr.rate_nrc__c = 45;
                pr.offer_rc__c = 45;
                pr.offer_nrc__c= 45;
                pr.approved_rc__c= 45;
                pr.Approved_NRC__c = 43;
                pr.Cost_RC__c = 45;
                pr.Cost_NRC__c =53;
                pr.contract_term__c= 12;
                pr.Approved_Offer_Price_MRC__c = 500;
                pr.Approved_Offer_Price_NRC__c = 500;
                pr.Actual_Configuration__c = 'bcd';
                pr.Approved_Configuration__c = 'abc';
               
                prequestlist.add(pr);
                insert prequestlist;
                system.assert(prequestlist[0].id!=null);
                
                         
                
    Test.starttest();
    ApexPages.StandardController sc = new ApexPages.StandardController(caseList[0]);
    //ApexPages.currentPage().getParameters().put('id',caseList[0].Id);
    exportExcelApprovalRequestData ac = new exportExcelApprovalRequestData(sc);
    exportExcelApprovalRequestData.wrapper wrapper = new  exportExcelApprovalRequestData.wrapper();
   
    ac.exportToExcel();
    
    Test.stoptest();
                
                  
                     
                
    } 
}