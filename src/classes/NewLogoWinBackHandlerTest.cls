@isTest(SeeAllData = false)
public with sharing class NewLogoWinBackHandlerTest
{
  static testmethod void testNewLogoWinBackHandler1()
    {
        NewLogoWinBackHandler newLogoWinBackHandler = new NewLogoWinBackHandler();
                      
        Test.startTest();   
                       
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='34');
        insert cntryLkupObj;
        
         List<NewLogo__c> NewLogolist = new List<NewLogo__c>{
                    new NewLogo__c(name = 'Active',Action_Item__c = 'Active'),
                    new NewLogo__c(name = 'Logo_Prospect',Action_Item__c = 'Prospect'),
                    new NewLogo__c(name = 'Logo_Customer',Action_Item__c = 'Customer'),
                    new NewLogo__c(name = 'Logo_Approved',Action_Item__c = 'Logo_Approved'),                    
                    new NewLogo__c(name = 'Logo_Former_Customer',Action_Item__c = 'Logo_Former_Customer'),
                    new NewLogo__c(name = 'Logo_New_Customer',Action_Item__c = 'Logo_New_Customer'),
                    new NewLogo__c(name = 'Logo_New_Logo',Action_Item__c = 'Logo_New_Logo'),                    
                    new NewLogo__c(name = 'Logo_Rejected',Action_Item__c = 'Logo_Rejected') };
        Insert NewLogolist;
        
        Account accObj = new Account();
        accObj.Name = 'abcd';
        accObj.Type ='Prospect';
        accObj.Account_Type__c = 'Prospect';
        accObj.Account_Status__c = 'Active';
        accObj.Account_ID__c = '34567';
        accObj.Activated__c = true;
        accObj.Account_Verified__c = true;
        accObj.Customer_Legal_Entity_Name__c ='test';
        accObj.Industry = 'Education';
        accObj.Country__c = cntryLkupObj.Id;
        accObj.Customer_Type_New__c ='GW';
        accObj.Selling_Entity__c = 'Telstra International Limited';
        accObj.Account_ORIG_ID_DM__c ='test';        
        insert accObj;      
        
        Account accObj1 = new Account();
        accObj1.id=accObj.id;
        accObj1.Type =accObj.Type;
        accObj1.Account_Type__c = accObj.Account_Type__c;       
        
        System.assert(accObj!=null); 
        
        /*accObj.Type ='Logo_Former_Customer';
        accObj.Account_Type__c = 'Logo_Former_Customer';
        update accObj;
        
        Account accObj2 = new Account();
        accObj2.id=accObj.id;
        accObj2.Type =accObj.Type;
        accObj2.Account_Type__c = accObj.Account_Type__c;*/
        
        accObj.Type ='Customer';
        accObj.Account_Type__c = 'Customer';
        update accObj;  
        
        List<Account> accList = new List<Account>();
        accList.add(accObj);
        
        map<id,Account> oldMap = new map<id,Account>();
        map<id,Account> newMap = new map<id,Account>();
        oldMap.put(accObj1.id,accObj1);
        newMap.put(accList[0].id,accList[0]);
               
              Contact cons = new Contact();
      cons.LastName = 'Test';
      cons.accountid = accObj.id; 
      cons.OwnerId = UserInfo.getUserId();
      cons.Contact_Type__c = 'Billing';
      cons.Country__c = cntryLkupObj.id;
      cons.Is_updated__c = true;
      cons.Is_Attached_to_Bill_Profile__c = true;
      cons.Primary_Contact__c = true;
      cons.Postal_Code__c = '12345';
      insert cons;
               
        Opportunity oppObj = new Opportunity(name = 'GFTS Ph 2',AccountID =accObj.ID, Opportunity_Type__c='Simple',
                 CurrencyIsoCode ='USD', CloseDate = Date.today(),StageName ='Closed Won',Stage__c='Identify & Define',
                 QuoteStatus__c = 'Order', Sales_Status__c = 'Open', Win_Loss_Reasons__c='Product', 
                 Order_Type__c ='Renewal',ContractTerm__c = '24',Action_Item_Created_SD__c=true,Signed_Contract__c = 'Yes',
                 Signed_Contract_Order_form_Recieved_Date__c = System.today(),Upload_signed_order_Contract__c = True,
                 Customer_Primary_Contact__c = cons.id, Technical_Sales_Contact__c = UserInfo.getUserId());
                         
        List<Opportunity> OppList = new List<Opportunity>();
        OppList.add(oppObj);
        insert OppList;      
                
        try{
            newLogoWinBackHandler.newLogoWinBackHdlrMthd(accList,true,true,oldMap,newMap);
            
            //newLogoWinBackHandler.newLogoWinBackHdlrMthd(accList,true,true,oldMap1,newMap);
        newLogoWinBackHandler.newLogoCommonMthdHdlr(accList,true,true,oldMap,newMap);
            
        }catch(Exception e){}
        
        
        Test.stopTest();
    }
    
    static testmethod void testNewLogoWinBackHandler2()
    {
        NewLogoWinBackHandler newLogoWinBackHandler = new NewLogoWinBackHandler();
                
        Test.startTest();  
        
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='34');
        insert cntryLkupObj;

         List<NewLogo__c> NewLogolist = new List<NewLogo__c>{
                    new NewLogo__c(name = 'Active',Action_Item__c = 'Active'),
                    new NewLogo__c(name = 'Logo_Prospect',Action_Item__c = 'Prospect'),
                    new NewLogo__c(name = 'Logo_Customer',Action_Item__c = 'Customer'),
                    new NewLogo__c(name = 'Logo_Approved',Action_Item__c = 'Logo_Approved'),                    
                    new NewLogo__c(name = 'Logo_Former_Customer',Action_Item__c = 'Logo_Former_Customer'),
                    new NewLogo__c(name = 'Logo_New_Customer',Action_Item__c = 'Logo_New_Customer'),
                    new NewLogo__c(name = 'Logo_New_Logo',Action_Item__c = 'Logo_New_Logo'),                    
                    new NewLogo__c(name = 'Logo_Rejected',Action_Item__c = 'Logo_Rejected') };
        Insert NewLogolist;
        
        Account accObj = new Account();
        accObj.Name = 'abcd';
        accObj.Type ='Logo_Former_Customer';
        accObj.Account_Type__c = 'Logo_Former_Customer';
        accObj.Account_Status__c = 'Active';
        accObj.Account_ID__c = '34567';
        accObj.Activated__c = true;
        accObj.Account_Verified__c = true;
        accObj.Customer_Legal_Entity_Name__c ='test';
        accObj.Industry = 'Education';
        accObj.Country__c = cntryLkupObj.Id;
        accObj.Customer_Type_New__c ='GW';
        accObj.Selling_Entity__c = 'Telstra International Limited';
        accObj.Account_ORIG_ID_DM__c ='test';        
        insert accObj;      
        
        Account accObj1 = new Account();
        accObj1.id=accObj.id;
        accObj1.Type =accObj.Type;
        accObj1.Account_Type__c = accObj.Account_Type__c;   
        
        System.assert(accObj!=null); 
        
        accObj.Type ='Customer';
        accObj.Account_Type__c = 'Customer';
        update accObj;      
        
        List<Account> accList = new List<Account>();
        accList.add(accObj);
        
        map<id,Account> oldMap = new map<id,Account>();
        map<id,Account> newMap = new map<id,Account>();
        oldMap.put(accObj1.id,accObj1);
        newMap.put(accList[0].id,accList[0]);
        
        System.assert(accObj!=null); 
       
        List<Opportunity> oppList = new List<Opportunity>();
        
              Contact cons = new Contact();
      cons.LastName = 'Test';
      cons.accountid = accObj.id; 
      cons.OwnerId = UserInfo.getUserId();
      cons.Contact_Type__c = 'Billing';
      cons.Country__c = cntryLkupObj.id;
      cons.Is_updated__c = true;
      cons.Is_Attached_to_Bill_Profile__c = true;
      cons.Primary_Contact__c = true;
      cons.Postal_Code__c = '12345';
      insert cons;
               
        Opportunity oppObj = new Opportunity(name = 'GFTS Ph 2',AccountID =accObj.ID, Opportunity_Type__c='Simple',
                 CurrencyIsoCode ='USD', CloseDate = Date.today(),StageName ='Closed Won',Stage__c='Identify & Define',
                 QuoteStatus__c = 'Order', Sales_Status__c = 'Open', Win_Loss_Reasons__c='Product', 
                 Order_Type__c ='Renewal',ContractTerm__c = '24',Action_Item_Created_SD__c=true,Signed_Contract__c = 'Yes',
                 Signed_Contract_Order_form_Recieved_Date__c = System.today(),Upload_signed_order_Contract__c = True,
                 Customer_Primary_Contact__c = cons.id, Technical_Sales_Contact__c = UserInfo.getUserId());
                 
        OppList.add(OppObj);
        insert OppList;
        
        try{
            newLogoWinBackHandler.newLogoWinBackHdlrMthd(accList,true,true,oldMap,newMap);
        }catch(Exception e){}
        
        Test.stopTest();
    }
    
}