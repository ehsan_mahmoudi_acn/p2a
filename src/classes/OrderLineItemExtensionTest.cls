@isTest(seealldata=false)
public class OrderLineItemExtensionTest {
   static testmethod void testOrderLineItemExtension1()
    {
        // Country_Lookup__c Object instance and Mandatory attributes with values
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
        City_Lookup__c cityObj = new City_Lookup__c(Name ='Bangalore',City_Code__c='Beijing',
                                                    Generic_Site_Code__c ='HK#');
        insert cityObj;
        list<City_Lookup__c> prod = [select id,Name from City_Lookup__c where Name = 'Bangalore'];
        system.assertEquals(cityObj.Name , prod[0].Name);
        System.assert(cityObj!=null); 
        
        // Account Object instance and Mandatory attributes with values
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
        insert accObj;
        Contact contObj = new Contact(Country__c=cntryLkupObj.id,AccountId=accObj.Id,LastName='tech',
                                  email='test@gmail.com');
        insert contObj; 
      
        Opportunity oppObj = new Opportunity(Name='GFTS Ph 2',AccountID=accObj.Id,Opportunity_Type__c='Simple',
                                           CurrencyIsoCode = 'USD', CloseDate = Date.today(),StageName='Identify & Define',Stage__c='Identify & Define',
                                            QuoteStatus__c ='Approved',Sales_Status__c= 'Won',Win_Loss_Reasons__c ='Product',
                                            Order_Type__c= 'New', ContractTerm__c = '24');
        insert oppObj;
        Site__c siteObj = new Site__c(Name='Test_site',Address1__c='43',Address2__c='Bangalore',Country_Finder__c=cntryLkupObj.Id,
                             City_Finder__c=cityObj.Id,AccountId__c=accObj.Id,Address_Type__c='Billing Address');
        insert siteObj;
    
        BillProfile__c billProfObj = new BillProfile__c(Billing_Entity__c='Pacnet Limited',Account__c=siteObj.AccountId__c,
                                              Bill_Profile_Site__c=siteObj.Id, Start_Date__c= Date.today(),
                                             Invoice_Breakdown__c='Summary Page', First_Period_Date__c=Date.today(),
                                             Status__c='Active' ,Name ='test');
        List<BillProfile__c> lstBprofile=new List<BillProfile__c>();
        insert   billProfObj; 
    
        Order__c orderObj = new Order__c(Requested_Termination_Date__c=Date.today(), Account__c =accObj.Id);
        insert orderObj; 
    
        order_line_item__c ordLineItemObj = new order_line_item__c(Bill_Profile__c=billProfObj.Id,
          Is_GCPE_shared_with_multiple_services__c='NA',ParentOrder__c = OrderObj.Id);
        Set<Id> orderset=new Set<Id>();
        List<Id> listid=new List<Id>();
            orderset.add(ordLineItemObj.ParentOrder__c);
            ListId.add(ordLineItemObj.Bill_Profile__c);
       // insert ordLineItemObj;
     
    
        Profile profObj = [select Id from Profile  where name ='Standard User'];
        User userObj = new User(profileId = profObj.id, username = 'mohantesh@telstra.com',
            email = 'mohantesh@telstra.com', emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
            alias='nuser1',  lastname='lastname1',EmployeeNumber='1234589',Region__c='Australia');
            insert userObj;
     Test.startTest();
        ApexPAges.StandardController sc = new ApexPages.StandardController(ordLineItemObj);
        ApexPages.currentPage().getParameters().put('id',ordLineItemObj.Id);
       // String id = ApexPages.currentPage().getParameters().get('id');
        String oliId = ApexPages.currentPage().getParameters().get('id');
        OrderLineItemExtension ordprodext=new OrderLineItemExtension(sc);
       // EmailGenerationClass emailgen = new EmailGenerationClass (); 
        ordprodext.getErrorMessage();
      //  ordprodext.SendEmail();
        ordprodext.BillingSME='sekhartest';
        ordprodext.BillingSMEEmail='sekhar@gmail.com';
        ordprodext.BillingSMEmobile='9994309902';
        ordprodext.BillName='testdata';
     Test.stopTest();
    }
    static testmethod void testOrderLineItemExtension2(){
      
         Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
        City_Lookup__c cityObj = new City_Lookup__c(Name ='Bangalore',City_Code__c='Beijing',
                                                    Generic_Site_Code__c ='HK#');
        insert cityObj;
        list<City_Lookup__c> prod1 = [select id,Name from City_Lookup__c where Name = 'Bangalore'];
        system.assertEquals(cityObj.Name , prod1[0].Name);
        System.assert(cityObj!=null);
        
        // Account Object instance and Mandatory attributes with values
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
        insert accObj;
        Contact contObj = new Contact(Country__c=cntryLkupObj.id,AccountId=accObj.Id,LastName='tech',
                                  email='test@gmail.com');
        insert contObj; 
      
        Opportunity oppObj = new Opportunity(Name='GFTS Ph 2',AccountID=accObj.Id,Opportunity_Type__c='Simple',
                                           CurrencyIsoCode = 'USD', CloseDate = Date.today(),StageName='Identify & Define',Stage__c='Identify & Define',
                                            QuoteStatus__c ='Approved',Sales_Status__c= 'Won',Win_Loss_Reasons__c ='Product',
                                            Order_Type__c= 'New', ContractTerm__c = '24');
        insert oppObj;
        Site__c siteObj = new Site__c(Name='Test_site',Address1__c='43',Address2__c='Bangalore',Country_Finder__c=cntryLkupObj.Id,
                             City_Finder__c=cityObj.Id,AccountId__c=accObj.Id,Address_Type__c='Billing Address');
        insert siteObj;
    
        BillProfile__c billProfObj = new BillProfile__c(Billing_Entity__c='Telstra Limited',Account__c=siteObj.AccountId__c,
                                              Bill_Profile_Site__c=siteObj.Id, Start_Date__c= Date.today(),
                                             Invoice_Breakdown__c='Summary Page', First_Period_Date__c=Date.today(),
                                             Status__c='Active' ,Name ='test');
        List<BillProfile__c> lstBprofile=new List<BillProfile__c>();
        //lstBprofile.add();
        insert   lstBprofile; 
    
        Order__c orderObj = new Order__c(Requested_Termination_Date__c=Date.today(), Account__c =accObj.Id);
        insert orderObj; 
    
        order_line_item__c ordLineItemObj = new order_line_item__c(Bill_Profile__c=billProfObj.Id,
          Is_GCPE_shared_with_multiple_services__c='NA',ParentOrder__c = OrderObj.Id);
        Set<Id> orderset=new Set<Id>();
        List<Id> listid=new List<Id>();
            orderset.add(ordLineItemObj.ParentOrder__c);
            ListId.add(ordLineItemObj.Bill_Profile__c);
       // insert ordLineItemObj;
     
    
        Profile profObj = [select Id from Profile  where name ='Standard User'];
        User userObj = new User(profileId = profObj.id, username = 'mohantesh@telstra.com',
        email = 'mohantesh@telstra.com', emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
        alias='nuser1',  lastname='lastname1',EmployeeNumber='1234589',Region__c='Australia');
        insert userObj;
        
        List<Account> acclist = new List<Account>();
    account a = new account();
    a.name    = 'Test Accenture ';
    a.BillingCountry  = 'GB';
    a.Activated__c  = True;
    a.Account_ID__c  = 'test';
    a.Account_Status__c = 'Active';
    a.Customer_Legal_Entity_Name__c = 'Test';
    a.Customer_Type_New__c = 'MNO';
    a.Region__c = 'Australia';
    a.Website='abc.com';
    a.Selling_Entity__c='Telstra Limited';
    a.Industry__c='BPO';
    a.Account_ORIG_ID_DM__c = 'test';
    acclist.add(a);
    insert acclist;
    system.assert(acclist[0].id!=null); 
    
     List<contact> contactList = new List<contact>();
                contact con = new contact();
                con.LastName='Test';

                //con.name = 'test';
                con.Accountid= acclist[0].id;
                contactList.add(con);
                insert contactList;
                List<Country_Lookup__c> countrys= P2A_TestFactoryCls.getcountry(1);
                 List<Site__c> siteslist = P2A_TestFactoryCls.getsites(1,acclist,countrys);
                
         List<BillProfile__c>  BPsList = new List<BillProfile__c>();
           BillProfile__c BPS = new BillProfile__c();
                BPS.Account__c = acclist[0].id;
                BPS.Name = 'TestBP' ;
                Bps.Status__c = 'Active';
                bps.company__c = null;
                BPS.Billing_Entity__c='Telstra Limited';
                 BPS.Billing_Contact__c =  contactList[0].id;
                
                BPS.Bill_Profile_Site__c = siteslist[0].id;
                BPS.Start_Date__c= Date.today();
                BPS.Invoice_Breakdown__c='Summary Page';
                BPsList.add(BPS);
                insert BPsList ;
        
        List<csord__Order_Request__c> OredrRequetslist = new List<csord__Order_Request__c>();
        csord__Order_Request__c ordreq = new csord__Order_Request__c();
        ordreq.Name = 'OrderName';
        ordreq.csord__Module_Name__c = 'SansaStark123683468Test';
        ordreq.csord__Module_Version__c = 'YouknownothingJhonSwon';
        ordreq.csord__Process_Status__c = 'Requested';
        ordreq.csord__Request_DateTime__c = System.now();
        OredrRequetslist.add(ordreq); 
        insert  OredrRequetslist ;
        system.assert(OredrRequetslist[0].id!=null);
        
         
        
         List<csord__Subscription__c> Subscriptionlist = new List<csord__Subscription__c>();
      csord__Subscription__c sub = new csord__Subscription__c();
                 sub.Name ='Test'; 
                sub.csord__Identification__c = 'Test-Catlyne-4238362';
                sub.csord__Order_Request__c = OredrRequetslist[0].id;
                sub.OrderType__c = null;
                Subscriptionlist.add(sub);
                insert Subscriptionlist;
        
            List<csord__Order__c> Orderlist = new List<csord__Order__c>();          
            csord__Order__c ord = new csord__Order__c();
            ord.csord__Identification__c = 'Test-JohnSnow-4238362';
            ord.RAG_Order_Status_RED__c = false;
            ord.RAG_Reason_Code__c = '';
            ord.Jeopardy_Case__c = null;
            ord.csord__Order_Request__c = OredrRequetslist [0].id;
            Orderlist.add(ord);
            insert Orderlist ;
            
           List<Order__c> order1 = new List<Order__c>();
        order1.add(new Order__c(Status__c='test' , Account__c = acclist[0].Id));
        insert order1;
        
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        insert prod;

        
        
        list<Order_Line_Item__c> oli1= new list<Order_Line_Item__c>();
        Order_Line_Item__c oli = new Order_Line_Item__c();
        oli.ParentOrder__c = order1[0].Id;
        oli.CPQItem__c = '1'; 
        oli.OrderType__c = 'New Provide'; 
        oli.Product__c = prod.Id;
        oli.Opportunity_Line_Item_ID__c ='111';
         oli.Is_GCPE_shared_with_multiple_services__c='No';
        oli1.add(oli);
        insert oli1;
        
        
        
       List<csord__Order_Line_Item__c> OrderLineItemlist = new List<csord__Order_Line_Item__c>();
       csord__Order_Line_Item__c pb = new csord__Order_Line_Item__c();
                pb.csord__Order__c= Orderlist[0].id;
                pb.csord__Identification__c ='hnerkfwfefwrjge';
                pb.csord__Order_Request__c= OredrRequetslist[0].id;
                OrderLineItemlist.add(pb);
                
               
            
            order_line_item__c ordLineItemObj1 = new order_line_item__c();
            ordLineItemObj1.Bill_Profile__c= BPsList[0].Id;
            ordLineItemObj1.Is_GCPE_shared_with_multiple_services__c='NA';
            ordLineItemObj1.ParentOrder__c = order1[0].Id;
            ordLineItemObj1.Parent_Order_Line_Item__c = oli1[0].id;
            insert ordLineItemObj1;
        
        Test.startTest();
        ApexPAges.StandardController sc = new ApexPages.StandardController(ordLineItemObj);
        ApexPages.currentPage().getParameters().put('id',ordLineItemObj.Id);
       // String id = ApexPages.currentPage().getParameters().get('id');
        String oliId = ApexPages.currentPage().getParameters().get('id');
        OrderLineItemExtension ordprodext=new OrderLineItemExtension(sc);
      // EmailGenerationClass emailgen = new EmailGenerationClass (); 
       
        ordprodext.getErrorMessage();
        try{
        ordprodext.SendEmail();
        }catch(exception e){}
        EmailGenerationClass.emailSending(userObj, ordLineItemObj1.Id);
        ordprodext.BillingSME='sekhartest';
        ordprodext.BillingSMEEmail='sekhar@gmail.com';
        ordprodext.BillingSMEmobile='9994309902';
        ordprodext.BillName='testdata';
     Test.stopTest();
    }
        
}