@isTest
private class SiteInstallerAddressExtControllerTest{
        
    static testMethod void unitTest() {

     City_Lookup__c cntrylkp = new City_Lookup__c();
     cntrylkp.Name = 'countrylookup';
     cntrylkp.City_Code__c = 'country';
     cntrylkp.Country__c = 'country';
     cntrylkp.Generic_Site_Code__c = 'sitecode';
     cntrylkp.State_Province__c = 'provience';
     insert cntrylkp;
     system.assertEquals(cntrylkp.Name , 'countrylookup');
     List<Account> AccList = P2A_TestFactoryCls.getAccounts(1);
     
     List<Country_Lookup__c> countrylist = P2A_TestFactoryCls.getcountry(1);
     
     list<Site__c> Sitelist = new list<Site__c>();
     Site__c sites = new Site__c();
     sites.Name = 'TestSiteupdate';
     sites.AccountId__c = Acclist[0].id;
     sites.Name = 'TestSite';
     sites.Address_Type__c = 'Billing Address' ;
     sites.Address1__c = 'AddressTest';
     sites.Country_Finder__c = countrylist[0].id;
     sites.City_Finder__c = cntrylkp.id;
     sites.Address1__c = 'Bangalore';
     //sites[0].Address_Type__c = 'Site Address';
     //sites[0].AccountId__c = AccList.get(0).id;
     //siteList[0].Address_Type__c = 'Billing Address';
     //sites[0].Country_Finder__c =countrylist.get(0).id;
     Sitelist.add(sites);
     
     Test.startTest();
         ApexPages.StandardController sc = new ApexPages.StandardController(siteList.get(0));
         SiteInstallerAddressExtController ac = new SiteInstallerAddressExtController(sc);     
         ac.save();
     Test.stopTest();   
    
    }
    
     static testMethod void unitTest1(){
     
     City_Lookup__c cntrylkp = new City_Lookup__c();
     cntrylkp.Name = 'countrylookup';
     cntrylkp.City_Code__c = 'country';
     cntrylkp.Country__c = 'country';
     cntrylkp.Generic_Site_Code__c = 'sitecode';
     cntrylkp.State_Province__c = 'provience';
     insert cntrylkp;
     system.assertEquals(cntrylkp.Name , 'countrylookup');
     List<Account> AccList = P2A_TestFactoryCls.getAccounts(1);
     
     List<Country_Lookup__c> countrylist = P2A_TestFactoryCls.getcountry(1);
     list<Site__c> Sitelist = new list<Site__c>();
     Site__c sites = new Site__c();
     sites.Name = 'TestSiteupdate';
     sites.AccountId__c = Acclist[0].id;
     sites.Name = 'TestSite';
     sites.Address_Type__c = 'Billing Address' ;
     sites.Address1__c = 'AddressTest';
     sites.Country_Finder__c = countrylist[0].id;
     sites.City_Finder__c = cntrylkp.id;
     sites.Address1__c = 'Bangalore';
     Sitelist.add(sites);
     
     Test.startTest();
         ApexPages.StandardController sc = new ApexPages.StandardController(siteList.get(0));
         SiteInstallerAddressExtController ac = new SiteInstallerAddressExtController(sc);     
         ac.SaveAndNew();
     Test.stopTest();   
     
     }
    
 }