global class SortingWrapper implements Comparable {

    public TreeSorting pac;

    public SortingWrapper (TreeSorting pc) {
        pac = pc;
    }

    global Integer compareTo(Object compareTo) {/*

        SortingWrapper compareToPac = (SortingWrapper)compareTo;

        Integer returnValue = 0;

        // Ordered arrays of integers
        // Lista A
        Integer[] ListA = new Integer[]{};
        if(pac.Position_Edit != null)
        {
            if(pac.Position_Edit.contains('.')){
                for(string a : pac.Position_Edit.split('\\.')){
                    ListA.add(Integer.Valueof(a));
                }
            }
            else
            {
                ListA.add(Integer.Valueof(pac.Position_Edit));
            }
        }

        // Lista B
        Integer[] ListB = new Integer[]{};
        if(compareToPac.pac.Position_Edit != null)
        {
            if(compareToPac.pac.Position_Edit.contains('.')){
                for(string b : compareToPac.pac.Position_Edit.split('\\.')){
                    ListB.add(Integer.Valueof(b));
                }
            }
            else
            {
                ListB.add(Integer.Valueof(compareToPac.pac.Position_Edit));
            }
        }

        //If the size of the arrays is different, makes both arrays have the same size, complete with zero
        if(ListB.size() != ListA.size()){

            Integer diferenca = 0;

            if(ListA.size() > ListB.size()){
                diferenca = ListA.size() - ListB.size();
                for(Integer i =0; i < diferenca; i++){
                    ListB.add(0);
                }
            }else{
                diferenca = ListB.size() - ListA.size();
                for(Integer i =0; i < diferenca; i++){
                    ListA.add(0);
                }
            }
        }   

        Integer index = 0;
        while (index < ListA.size()) {

            if (ListA[index] > ListB[index]) {
                return 1;
            } else if (ListA[index] < ListB[index]) {
                return -1;
            }

            index++;
        }*/

        return null;
    }

}