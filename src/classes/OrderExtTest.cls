@isTest(SeeAllData = false)
public class OrderExtTest{
 public static List<csord__Order__c> Orderlist;
 public static List<csord__Order_Request__c> OrdReqList;
  private static testMethod void orderextstest()
    {
       OrdReqList= new List<csord__Order_Request__c>{
            new csord__Order_Request__c(
           Name = 'OrderName',
                csord__Module_Name__c = 'SansaStark123683468Test',
                csord__Module_Version__c = 'YouknownothingJhonSwon',
                csord__Process_Status__c = 'Requested',
                csord__Request_DateTime__c = System.now()

            )
        };
        insert OrdReqList;  
        List<csord__Order_Request__c> ordreq = [select Name,csord__Module_Name__c,csord__Module_Version__c,csord__Process_Status__c,csord__Request_DateTime__c  from csord__Order_Request__c where Name = 'OrderName'];
        System.assert(ordreq!=null);
        system.assertEquals(OrdReqList[0].name,ordreq[0].name);
        
         Exception ee = null;
        try{
        
        Test.startTest();
           
           
        Orderlist = new List<csord__Order__c>{
            new csord__Order__c(csord__Identification__c = 'Test-JohnSnow-4238362',Is_InFlight_Cancel__c = false,Status__c = 'Approved',
                                    Order_Type__c = 'default',csord__Order_Request__c =OrdReqList[0].id)
        };
        insert Orderlist;
        System.assert(Orderlist!=null);
        system.assertEquals(Orderlist[0].csord__Identification__c ,'Test-JohnSnow-4238362');
        list<csord__Order__c> ordList = [select id,Is_InFlight_Cancel__c,Status__c,Order_Type__c from csord__Order__c where id =:Orderlist[0].id Limit 1];
        ordList[0].Is_InFlight_Cancel__c = true;
        ordList[0].Status__c = 'Cancelled';
        ordList[0].Order_Type__c = 'Suspend'; 
        update ordList[0];
        system.assert(ordList!=null);
        
         ApexPages.currentPage().getParameters().put('id',ordList[0].id); 
         ApexPages.StandardController stdController = new ApexPages.StandardController(ordList[0]);
         OrderExt oe = new OrderExt(stdController);
         //OrderExt oe = new OrderExt(new ApexPages.StandardController(ordList[0]));
            
         //PageReference pageRef = Page.OrderCancel; 
         oe.cancel();
              
        }
        catch(Exception e){
                ee = e;
                ErrorHandlerException.ExecutingClassName='OrderExtTest:orderextstest';         
                ErrorHandlerException.sendException(e); 
                }
            finally {
                Test.stopTest();                
                if(ee != null){
                    throw ee;
                }
            }
       
       }
       
     }