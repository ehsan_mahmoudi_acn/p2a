public Class AccountBaseShareViewController{
    public String recordId{get;set;}
    List<AccountShare> allAccountShare = new List<AccountShare>();
    List<SharingTemplate> temp = new List<SharingTemplate>();
   Account acc;
   Public String acName{get;set;}
    
     public AccountBaseShareViewController()
    {    
        recordId = ApexPages.currentPage().getParameters().get('id');    
        acc = [Select id,Name from Account where id= :recordId];
        acName = acc.Name;       
        System.debug('The record id  is ' + recordId);  
        temp =   new List<SharingTemplate>();      
        allAccountShare = [Select a.id,a.UserOrGroupId, a.OpportunityAccessLevel, a.ContactAccessLevel, a.CaseAccessLevel, a.AccountAccessLevel,a.AccountId,a.Rowcause From AccountShare a where a.AccountId =: recordId and Rowcause='Manual'];
        System.debug('The Size is ' + allAccountShare.size());
    }
    
    public List<SharingTemplate> getManualSharedAccounts()
    {
         
        // Can use Database.query with extension along with Custom Settings to have modified query
         performAction();
        return temp;
    }
    
    public PageReference addSharing(){
        PageReference newPage1 = Page.NewAccountUserSharing;
        newPage1.getParameters().put('recordId',recordId);
        return newPage1;
    } 
    
     public PageReference editRequest()
     {
         PageReference spage = Page.EditAccountSharing;
          String idToEdit = ApexPages.currentPage().getParameters().get('recordIdToEdit');
          spage.getParameters().put('recordId',idToEdit);
          spage.getParameters().put('accountId',recordId);
         return spage;
     }
     
     public PageReference deleteRequest()
     {
         String idToDelete = ApexPages.currentPage().getParameters().get('recordIdToDelete');
         AccountShare share  =null;
         if (idToDelete!=null){
             try {             
                  share = [Select a.id from AccountShare a where id=:idToDelete];
              }catch (Exception e){
                  ErrorHandlerException.ExecutingClassName='AccountBaseShareViewController:deleteRequest';           
                  ErrorHandlerException.sendException(e);
                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error occurred while processing the data. Please contact your GS team.'));
                  
              }
           }
           if (share!=null){
               delete share;
           }
          // allAccountShare =  new List<AccountShare>();
          // performAction();
          //PageReference pg = Page.AccountBaseShareView;
          //pg.getParameters().put('id',recordId);
         return null;
     }
     
     private void performAction()
     {
         recordId = ApexPages.currentPage().getParameters().get('id');  
          temp =   new List<SharingTemplate>();                  
          allAccountShare = [Select a.id,a.UserOrGroupId, a.OpportunityAccessLevel, a.ContactAccessLevel, a.CaseAccessLevel, a.AccountAccessLevel,a.AccountId,a.Account.Name,a.Rowcause From AccountShare a where a.AccountId =: recordId and Rowcause='Manual'];      
        if (acc==null){
            acc = [Select id,Name from Account where id= :recordId];
            acName = acc.Name;       
         }
         Set<Id> userSet = new Set<Id>();
         Set<Id> groupSet = new Set<Id>();
         Set<Id> roleSet = new Set<Id>();
         if(!allAccountShare.isEmpty())
         {             
                     // Specify User
                     // Specify Group                 
                     // Specify Role
                 for (AccountShare shr : allAccountShare)
                 {
                     String testId = shr.UserOrGroupid+'';
                     if (testId .startswith('005')){
                         userSet.add(shr.UserOrGroupid);
                     }
                 }    
                 //User Query
                 //List<User> allUsers = [Select Id,Name from User where id in : userSet];
                 Map<Id,User> userList = new Map<Id,User>();
                 for (User usr: [Select Id,Name from User where id in : userSet])
                 {
                     userList.put(usr.id,usr);
                 }
                 //Group Query
                 //Select Id, Type from Group where Type='Group'
                 //Role Query
                 //Select Id, Type from Group where Type='Role'
                 for (AccountShare shr : allAccountShare)
                 {
                    String testId = shr.UserOrGroupid+'';
                    if (testId .startswith('005'))
                    {   
                         SharingTemplate newShare = new SharingTemplate();
                         newShare.id = shr.Id;
                         newShare.AccountAccess= getPicklistVal(shr.AccountAccessLevel);
                         newShare.OpportunityAccess=getPicklistVal(shr.OpportunityAccessLevel);
                         newShare.CaseAccess=getPicklistVal(shr.CaseAccessLevel);
                         newShare.Type='User';
                         newShare.Name = userList.get(testId).Name;
                         temp.add(newShare); 
                      }
                 }
         }
     }
     
     private String getPicklistVal(String baseVal)
     {
         String modVal = '';
         if (baseVal=='Edit')
         {
               modVal='Read/Write';
         }else if (baseVal=='Read')
         {
               modVal='Read Only';
         }else if (baseVal=='None')
         {
             modVal='Private';
         }else{
             modVal='Private';
         }         
         return modVal;
     }
     
    class SharingTemplate{
        public Id id{get;set;}
        public String Type{get;Set;}
        public String Name{get;Set;}
        public String AccountAccess{get;Set;}
        public String OpportunityAccess{get;Set;}
        public String CaseAccess{get;Set;}       
    }
}