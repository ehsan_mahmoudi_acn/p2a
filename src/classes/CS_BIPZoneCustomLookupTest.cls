@isTest(SeeAllData = false)
private class CS_BIPZoneCustomLookupTest {

    private static testMethod void doLookupSearchtest() {
        
        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('BIPPostcode','BIPESACodeName');
         
        CS_BIP_PostCode_ESACode_Zone__c postcode = new CS_BIP_PostCode_ESACode_Zone__c();
        postcode.Name = 'TestPostCode';
        postcode.CS_BIP_ESACode__c = 'BIPPostcode';
        postcode.CS_BIP_Zone__c = 'BIPESACodeName';
        insert postcode;
        
        CS_BIPZoneCustomLookup  lookup = new CS_BIPZoneCustomLookup();
         
        Object[] obj = lookup.doDynamicLookupSearch(searchFields,'productDefinitionID');
        
        String str = lookup.getRequiredAttributes();
       list<CS_BIP_PostCode_ESACode_Zone__c > postcode1 = [select id ,CS_BIP_ESACode__c , CS_BIP_Zone__c from  CS_BIP_PostCode_ESACode_Zone__c ];
       system.assertEquals(postcode.CS_BIP_ESACode__c , postcode1[0].CS_BIP_ESACode__c ); 
       system.assertEquals(postcode.CS_BIP_Zone__c ,postcode1[0].CS_BIP_Zone__c );  
        
    }

}