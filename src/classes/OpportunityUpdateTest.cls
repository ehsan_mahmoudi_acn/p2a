@isTest(seeAllData = false)
private class OpportunityUpdateTest{
   Static Account acc1 = getAccount();

   private static void setupData() {
       List<Global_Constant_Data__c> globalConstantDataObjList = new List<Global_Constant_Data__c>();       
       Global_Constant_Data__c globalConstantDataObj;
       
       // providing the values to custom setting object
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Status';
       globalConstantDataObj.Value__c = 'Assigned';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Priority';
       globalConstantDataObj.Value__c = 'High';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       insert globalConstantDataObjList;
       
       System.assertEquals(globalConstantDataObjList[0].Name ,'Action Item Status'); 

       
        List<Action_Item__c> previousActionItem = new List<Action_Item__c>();
           Action_Item__c action = new Action_Item__c();
           
           List<Action_Item__c> lastAction= [Select Id,Name,Previous_MRC__c,Previous_NRC__c,Account_Number__c,Opportunity__c,Status__c,Feedback__c,Account__r.Account_ID__c,LastModifiedDate,MRC_Monthly_Recurring_Charges__c,NRC_Non_Recurring_Charges__c,Total_Estimated_MRC__c,Total_Estimated_NRC__c from Action_Item__c where Id =: globalConstantDataObjList[0].id];
         //action.Previous_Credit_Check_Date__c=Date.newInstance(lastAction.get(0).LastModifiedDate.year(),lastAction.get(0).LastModifiedDate.month(),lastAction.get(0).LastModifiedDate.day());
                       if(lastAction != null && lastAction.size()>0){
                            action.Action_Item_ID_Previous_Credit_Check__c=lastAction[0].Name;                    
                            action.Previous_MRC__c=lastAction[0].MRC_Monthly_Recurring_Charges__c;
                            action.Previous_NRC__c=lastAction[0].NRC_Non_Recurring_Charges__c;
                            action.Previous_Estimated_MRC__c=lastAction[0].Total_Estimated_MRC__c;
                            action.Previous_Estimated_NRC__c=lastAction[0].Total_Estimated_NRC__c;
                            } 
                        previousActionItem.add(action);
                        insert previousActionItem;  
                        system.assert(previousActionItem!=null);    
       
        Opportunity opportunityObj = getOpportunity();
        insert opportunityObj;
        system.assert(opportunityObj!=null); 
        
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
         
        
       System.assertEquals(cntryLkupObj.Name ,'HK'); 
        
        
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                        Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                        Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                        Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
        insert accObj;
        
        System.assertEquals(accObj.Name ,'abcd'); 
       
        
        Contact conObj = DataFactoryTest.createContact('Bill','Profile',accobj,'Billprofile',cntryLkupObj,true);
        
        BillProfile__c billProfileObject = getBillProfile(accObj,conObj);
       
        Action_Item__c objActionItem= new Action_Item__c();
        objActionItem.Bill_Profile__c = billProfileObject.Id;
        objActionItem.Account__c =  accObj.Id;
        objActionItem.Due_Date__c = System.Today() + 2;  
        objActionItem.Subject__c = 'Update Contract Documents';
        objActionItem.Comments__c = 'Payment Terms and Billing Frequency has changed, please update the contract documents (if any).';
        objActionItem.Opportunity__c = opportunityObj.Id;
        insert objActionItem;

        System.assert(objActionItem.Id != null);
       
   }
   
    static testMethod void testBillProfileActivation() {
        setupData();       

        Set<Opportunity> oppSet = new Set<Opportunity>();
        oppSet.add([SELECT Id, AccountId, StageName, Stage__c FROM Opportunity]);

        Test.startTest();
        OpportunityUpdate oppObjUpdate = new OpportunityUpdate();
        oppObjUpdate.BillProfileActivation(oppSet);
        Test.stopTest();

        // checking
        List<Action_Item__c> actItems = [SELECT Id, Name, Subject__c FROM Action_Item__c];
        System.assert(actItems.size() > 0, 'ERROR: Action_Item__c was not created.');
    }

    static testMethod void testBillProfileApproval() {
        setupData();       

        Set<Opportunity> oppSet = new Set<Opportunity>();
        oppSet.add([SELECT Id, AccountId, StageName, Stage__c FROM Opportunity]);

        OpportunityUpdate oppObjUpdate = new OpportunityUpdate();
        oppObjUpdate.BillProfileActivation(oppSet);

        Test.startTest();
        oppObjUpdate.BillProfileApproval(oppSet);
        Test.stopTest();

        List<Action_Item__c> actItems = [SELECT Id, Name, Subject__c FROM Action_Item__c];
        System.assert(actItems.size() > 0, 'ERROR: Action_Item__c was not created.');
    }

    static testMethod void testCreditcheckCreation() {
        setupData();       

        Set<Opportunity> oppSet = new Set<Opportunity>();
        oppSet.add([SELECT Id, AccountId, StageName, Stage__c FROM Opportunity]);

        OpportunityUpdate oppObjUpdate = new OpportunityUpdate();
        oppObjUpdate.BillProfileActivation(oppSet);
        oppObjUpdate.BillProfileApproval(oppSet);

        Test.startTest();
        oppObjUpdate.CreditcheckCreation(oppSet);
        Test.stopTest();

        List<Action_Item__c> actItems = [SELECT Id, Name, Subject__c FROM Action_Item__c];
        System.assert(actItems.size() > 0, 'ERROR: Action_Item__c was not created.');
    }

    static testMethod void testPreviousCreditcheckupdation() {
        setupData();       

        Set<Opportunity> oppSet = new Set<Opportunity>();
        oppSet.add([SELECT Id, AccountId, StageName, Stage__c FROM Opportunity]);

        OpportunityUpdate oppObjUpdate = new OpportunityUpdate();
        oppObjUpdate.CreditcheckCreation(oppSet);

        Test.startTest();
        oppObjUpdate.PreviousCreditcheckupdation(oppSet);
        Test.stopTest();

        List<Action_Item__c> actItems = [SELECT Id, Name, Subject__c FROM Action_Item__c];
        System.assert(actItems.size() > 0, 'ERROR: Action_Item__c was not created.');
    }

    static testMethod void deleteOpp(){
        List<Global_Constant_Data__c> globalConstantDataObjList = new List<Global_Constant_Data__c>();
       
       Global_Constant_Data__c globalConstantDataObj;
       
       // providing the values to custom setting object
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Status';
       globalConstantDataObj.Value__c = 'Assigned';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Priority';
       globalConstantDataObj.Value__c = 'High';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       insert globalConstantDataObjList;
       system.assert(globalConstantDataObjList!=null);
       
         
        Opportunity opportunityObj = getOpportunity();
        insert opportunityObj;
        system.assert(opportunityObj!=null);
        delete opportunityObj;
        
      }
       
     
    // Creating the Test Data
    private static Opportunity getOpportunity(){                 
        Opportunity opp = new Opportunity();            
        //Account acc = getAccount();           
        opp.Name = 'Test Opportunity';            
        opp.AccountId = acc1.Id;   
         //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement         
        opp.StageName = 'Identify & Define' ;  
        opp.Stage__c= 'Identify & Define' ;          
        opp.CloseDate = System.today();   
        opp.Opportunity_Type__c = 'Complex';
        opp.Estimated_MRC__c=8;
            opp.Estimated_NRC__c=2;
            opp.ContractTerm__c='1';
            system.assert(opp!=null);
        //opp.Stage_Gate_1_Action_Created__c = true;         
        //opp.Approx_Deal_Size__c = 9000;                 
        return opp;    
       }
       private static Account getAccount(){                
        Account acc = new Account();            
        Country_Lookup__c c = getCountry();            
        acc.Name = 'Test Account';            
        acc.Customer_Type__c = 'MNC';            
        acc.Country__c = c.Id;            
        acc.Selling_Entity__c = 'Telstra INC';  
        acc.Activated__c= true;  
        acc.Account_ID__c = '9090';  
        acc.Customer_Legal_Entity_Name__c = 'Test' ;     
        insert acc;
        
       // system.assertequals(acc.name,a[0].name);  
        system.assert(acc!=null);     
        return acc;    
      }        
      
      private static Country_Lookup__c getCountry(){        
           Country_Lookup__c country = new Country_Lookup__c();            
           country.CCMS_Country_Code__c = 'HK';            
           country.CCMS_Country_Name__c = 'India';            
           country.Country_Code__c = 'HK';            
           insert country;  
           system.assert(country!=null);      
           return country;    
         }
         
    private static testMethod void oppstage6(){
        Integer OppCount=0;    
        OpportunityUpdate.doAdd();
        OpportunityUpdate.getcount();
        system.assert(OppCount!=null);
        
           }
    
    private static testMethod void oppstage7(){
        List<Global_Constant_Data__c> globalConstantDataObjList = new List<Global_Constant_Data__c>();       
        Global_Constant_Data__c globalConstantDataObj;
       
        // providing the values to custom setting object
        globalConstantDataObj = new Global_Constant_Data__c();
        globalConstantDataObj.Name = 'Action Item Status';
        globalConstantDataObj.Value__c = 'Assigned';
        globalConstantDataObjList.add(globalConstantDataObj);
       
        globalConstantDataObj = new Global_Constant_Data__c();
        globalConstantDataObj.Name = 'Action Item Priority';
        globalConstantDataObj.Value__c = 'High';
        globalConstantDataObjList.add(globalConstantDataObj);
        
        insert globalConstantDataObjList;
        
        System.assertEquals(globalConstantDataObjList[0].Name ,'Action Item Status'); 
        system.assert(globalConstantDataObjList!=null);
        
        Opportunity opportunityObj = getOpportunity();
        insert opportunityObj;
        
        Product2 p = new Product2();
        p.Name = 'IP VPN';
        p.ProductCode = 'G-VPN';
        p.IsActive = true;
        insert p;
        
       System.assertEquals(p.Name ,'IP VPN'); 
        
        
      
        // Pricebook2 pb = [SELECT Id FROM Pricebook2 WHERE IsStandard = true][0];
        
        PricebookEntry pbe = new PricebookEntry();
        pbe.Pricebook2Id ='01s90000000KWfFAAW';
        pbe.Product2Id = p.Id;
        pbe.IsActive = true;
        pbe.UseStandardPrice = false;
        pbe.UnitPrice = 999999;
        insert pbe;
        
       System.assertEquals(pbe.IsActive , True); 
        
        
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
        
       System.assertEquals(cntryLkupObj.Name , 'HK'); 
        
        
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                        Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                        Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                        Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
        insert accObj;

       System.assertEquals(accObj.Name , 'abcd'); 
        
        
        Contact conObj = DataFactoryTest.createContact('Bill','Profile',accobj,'Billprofile',cntryLkupObj,true);
        
        BillProfile__c billProfileObject = getBillProfile(accObj,conObj);
        
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = opportunityObj.Id;
        oli.Quantity = 1;
        oli.TotalPrice = 100;
        oli.PricebookEntryId = pbe.Id;
        oli.Description = 'Unit Test 001';
        oli.BillProfileId__c = null;
        oli.ProductLeadTime__c = 0;
        oli.OrderType__c = 'New Provide';
        oli.IsMainItem__c = 'Yes';
        oli.CPQItem__c = '1';
        oli.BillProfileId__c = billProfileObject.Id;
        insert oli;
        
    System.assertEquals(oli.OpportunityId , opportunityObj.Id); 

        
        System.debug('oli: ' + oli);
        
        Set<Opportunity> oppSet = new Set<Opportunity>();
        oppSet.add(opportunityObj);
        
        OpportunityUpdate oppObjUpdate = new OpportunityUpdate();
        oppObjUpdate.BillProfileApproval(oppSet);
    }
    
    private static testMethod void oppstage8(){
        
        List<Global_Constant_Data__c> globalConstantDataObjList = new List<Global_Constant_Data__c>();       
        Global_Constant_Data__c globalConstantDataObj;
       
        // providing the values to custom setting object
        globalConstantDataObj = new Global_Constant_Data__c();
        globalConstantDataObj.Name = 'Action Item Status';
        globalConstantDataObj.Value__c = 'Assigned';
        globalConstantDataObjList.add(globalConstantDataObj);
       
        globalConstantDataObj = new Global_Constant_Data__c();
        globalConstantDataObj.Name = 'Action Item Priority';
        globalConstantDataObj.Value__c = 'High';
        globalConstantDataObjList.add(globalConstantDataObj);
        
        insert globalConstantDataObjList;
        system.assert(globalConstantDataObjList!=null);
    System.assertEquals(globalConstantDataObjList[0].Name , 'Action Item Status'); 
        
        Opportunity opportunityObj = getOpportunity();
        insert opportunityObj;
        
        Product2 p = new Product2();
        p.Name = 'IP VPN';
        p.ProductCode = 'G-VPN';
        p.IsActive = true;
        insert p;
        
    System.assertEquals(p.Name ,'IP VPN'); 

        // Pricebook2 pb = [SELECT Id FROM Pricebook2 WHERE IsStandard = true][0];
        
        PricebookEntry pbe = new PricebookEntry();
        pbe.Pricebook2Id ='01s90000000KWfFAAW';
        pbe.Product2Id = p.Id;
        pbe.IsActive = true;
        pbe.UseStandardPrice = false;
        pbe.UnitPrice = 999999;
        insert pbe;
        system.assert(pbe!=null);
        
       
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
        
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                        Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                        Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                        Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
        insert accObj;
        system.assert(accObj!=null);
        
        System.assertEquals(accObj.Name , 'abcd'); 

        
        Contact conObj = DataFactoryTest.createContact('Bill','Profile',accobj,'Billprofile',cntryLkupObj,true);
        
        BillProfile__c billProfileObject = getBillProfile(accObj,conObj);
        billProfileObject.Activated__c = false;
        update billProfileObject;
        
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = opportunityObj.Id;
        oli.Quantity = 1;
        oli.TotalPrice = 100;
        oli.PricebookEntryId = pbe.Id;
        oli.Description = 'Unit Test 001';
        oli.BillProfileId__c = null;
        oli.ProductLeadTime__c = 0;
        oli.OrderType__c = 'New Provide';
        oli.IsMainItem__c = 'Yes';
        oli.CPQItem__c = '1';
        oli.BillProfileId__c = billProfileObject.Id;
        insert oli;
        
        System.assertEquals(oli.OpportunityId , opportunityObj.Id); 

        
        System.debug('oli: ' + oli);
        
        Set<Opportunity> oppSet = new Set<Opportunity>();
        oppSet.add(opportunityObj);
        
        OpportunityUpdate oppObjUpdate = new OpportunityUpdate();
        oppObjUpdate.BillProfileActivation(oppSet);
    }
    
    private static BillProfile__c getBillProfile(Account accobj,Contact conobj){
      List<Account> acclist=P2A_TestFactoryCls.getAccounts(1);
          List<Contact> conList=P2A_TestFactoryCls.getContact(1,acclist);
          List<Country_Lookup__c> conlook=P2A_TestFactoryCls.getcountry(1);
          List<Site__c> siteList=P2A_TestFactoryCls.getsites(1,acclist,conlook);
          List<BillProfile__c> bpList=P2A_TestFactoryCls.getBPs(1,acclist,siteList,conList);
        BillProfile__c BProfObj = new BillProfile__c();
        BProfObj.Account__c = acclist[0].Id;
        BProfObj.Billing_Entity__c = 'Telstra International Limited';
        BProfObj.Payment_Terms__c = '45 Days';
        BProfObj.Billing_Frequency__c = 'Hong Kong Annual Voice';
        BProfObj.Billing_Contact__c = conList[0].id;
        BProfObj.Activated__c = true;
        BProfObj.Status__c = 'Active';
        BProfObj.Approved__c = FALSE;
          BProfObj.Bill_Profile_Site__c = siteList[0].id;
        insert BProfObj;
        system.assert(BProfObj!=null);
        
        return BProfObj;
     }
  
}