/**
    @author - Accenture
    @date - 25-APRIL-2012
    @version - 1.0
    @description - This is Test Class for Lead Queue Change Action Trigger .
*/
@isTest
private class LeadQueueChangeActionTest {

/**
        @return - void 
        @description - When Lead Queue is changed, Action Item is generated.
**/
   /* static testMethod void myUnitTest() {
        // Lead Queue is changed
        Lead Ld1= getLead();
        QueueSobject q = [select Queue.Id from QueueSobject where SobjectType='Lead' and Queue.Name='Enterprise Sales Leads' limit 1];
        Ld1.OwnerId=q.Queue.Id;
        update Ld1;
        // Action Item is created
        Action_Item__c ai = new Action_Item__c();
        // Get the Message "Successfully" on Action Item created
        if(ai.Lead__c == Ld1.Id)
        {
           System.debug('Successfully');
        }
      
    }*/
    // Putting the values in Lead on calling the method "getLead()"
    /*private static Lead getLead()
    {
       Lead Ld= new Lead();
       Ld.LastName='abc';
       Ld.Company='N/A';
       Ld.Status='Unengaged';
       Ld.LeadSource='Other'; 
       Ld.Region__c='';
       QueueSobject q = [select Queue.Id from QueueSobject where SobjectType='Lead' and Queue.Name='Marketing' limit 1];
       Ld.OwnerId=q.Queue.Id;
       Ld.Email = 'testabc@def.com';
       Ld.Industry = 'BPO';
       Ld.Country_Picklist__c = 'AFGHANISTAN';
       insert Ld;
       
       return Ld;  
    }*/
}