/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=false)
private class NotifyBillingUpdateTest {
 /*   private static Account a;
    private static CostCentre__c c;
    private static BillProfile__c b;
    private static Billing_Update__c buParent;
    private static Billing_Update__c buChild;
    static testMethod void TestNotifyBillingUpdate() {
        // TO DO: implement unit test
        a=getAccount();
        b=getBillProfile();
       // b.Account__c=a.Id;
        insert b;
        Service__c serviceObj= new Service__c();
        serviceObj=getServiceObj();
        serviceObj.AccountId__c=a.id;
        insert serviceObj;
        c=getCostCentre();
        c.BillProfile__c = b.Id;
        c.Cost_Centre_Status__c='Active';
        insert c;
        Resource__c res=getResourceObj();
        res.AccountId__c=a.id;
        res.CostCentre_Id__c=c.Id;
        res.Bill_Profile_Id__c=b.Id;
        insert res;
        system.debug('=======Cost_Centre_integration_number__c===='+c.Cost_Centre_integration_number__c);
        system.debug('=======Cost_Centre_ID__c===='+c.Cost_Centre_ID__c);
        try{
        test.startTest();
        
        buParent=getBillUpdateparentObj();
        buParent.Bill_Update_Against__c=serviceObj.Id;
        buParent.Account__c=a.Id; 
        insert buParent;
        buChild=getBillUpdateChildObj();
        buChild.Bill_Update_Service__c=buParent.Id;
        buChild.Bill_Update_Against__c=buParent.Bill_Update_Against__c;
        buChild.Resource_Service_Id__c=serviceObj.Id;
        buChild.Cost_Centre_integration_number__c=c.Cost_Centre_integration_number__c;
        insert buChild;
        buChild.ROC_Line_Item_Status__c='Error';
        buChild.ROC_Line_Item_Error__c='RC Bill Text not updated';
        update buChild;
        
        }catch(Exception ex){
    
        system.debug('exception'+ex);
        }try{
        buChild.ROC_Line_Item_Status__c='Success';
        buChild.ROC_Line_Item_Error__c='RC Bill Text not updated';
        update buChild; 
        }catch(Exception ex){
            
        system.debug('exception'+ex);   
        }try{
        buChild.Resource_Service_Id__c=res.Id;
        buChild.ROC_Line_Item_Status__c='Success';
        buChild.ROC_Line_Item_Error__c='RC Bill Text not updated';
        update buChild; 
        
        }catch(Exception ex){
            
        system.debug('exception'+ex);   
        }try{
        buChild=getBillUpdateChildObj();
        buChild.Resource_Service_Id__c=res.Id;
        buChild.ROC_Line_Item_Status__c='Success';
        buChild.ROC_Line_Item_Error__c='RC Bill Text not updated';
        buChild.Parent_of_the_bundle_flag__c=false;
        buChild.Bundle_Flag__c=false;  
        insert buChild; 
            
        test.stopTest();    
        }catch(Exception ex){
            
        system.debug('exception'+ex);   
        }
    }
    
    
    public static Billing_Update__c getBillUpdateparentObj(){
    Billing_Update__c billupdateServiceObj=new Billing_Update__c();
    billupdateServiceObj.static_flag__c = True;
        
    return billupdateServiceObj;
    }
    
     public static Billing_Update__c getBillUpdateChildObj(){
    Billing_Update__c billUpdateObj=new Billing_Update__c();
   // billUpdateObj.Bill_Update_Service__c=billupdateServiceObj.Id;
    billUpdateObj.Customer_PO_Number__c='423423';
    billUpdateObj.Bill_Profile_Integration_Number__c='25252';
    //billUpdateObj.Bill_Update_Against__c=billupdateServiceObj.Bill_Update_Against__c;
    billUpdateObj.ETC_Bill_Text__c='tretettterte';
    billUpdateObj.MRC_Bill_Text__c='tretettterte';
    billUpdateObj.NRC_Bill_Text__c='tretettterte';
    billUpdateObj.Root_Product_Bill_Text__c='tretettterte';
    billUpdateObj.Service_Status__c='Active';
    billUpdateObj.RC_Credit_Bill_Text__c='tretettterte';
    billUpdateObj.NRC_Credit_Bill_Text__c='tretettterte';
    billUpdateObj.Service_Bill_Text__c='tretettterte';
    billUpdateObj.Parent_of_the_bundle_flag__c=true;
    billUpdateObj.Bundle_Flag__c=true;   
    //billUpdateObj.Service__c=buVO.parentService;
    billUpdateObj.Bundle_Label__c='Test Bundle';
    billUpdateObj.Recurring_Credit_Bill_Text__c='tretettterte';
    billUpdateObj.One_Off_Credit_Bill_Text__c='tretettterte';
    billUpdateObj.Root_Product_ID__c='test root product Id';
   /* billUpdateObj.Billing_Entity__c=buVO.BillingEntity;
    billUpdateObj.Charge_Currency__c=buVO.ChargeCurrency;
    billUpdateObj.Primary_Service_ID__c=buVO.PrimaryServiceID;
    billUpdateObj.Parent_ID__c=buVO.ParentServiceID;
    billUpdateObj.Charge_id__c=buVO.ChargeID;
    billUpdateObj.NRC_Charge_Id__c=buVO.NRCChargeID;
    billUpdateObj.Pin_Service_ID__c=buVO.PINServiceID;
    billUpdateObj.Usage_Base_Flag__c=buVO.UsageFlag;
    billUpdateObj.Order_Line_Item_name__c=buVO.Name;
    billUpdateObj.Service_Resource__c=buVO.serviceResource;
    billUpdateObj.Resource_Service_Id__c=buVO.ResourceServiceId;
    billUpdateObj.isService__c=buVO.isService;
    billUpdateObj.Parent_Bundle_ID__c =buVO.parentBundleID;
    billUpdateObj.Bundle_Action__c =buVO.bundleAction;*/
        
   /* return billUpdateObj;
    }
     public static Service__c getServiceObj(){
      Service__c ser = new Service__c();
        ser.name='serv1';
        ser.Path_Instance_ID__c='1467000';
        ser.Parent_Path_Instance_id__c = '';
        ser.Is_GCPE_shared_with_multiple_services__c  = 'Yes-New';
        ser.is_this_data_from_PSL__c = True;
        
      
      return ser;
 }
  public static Account getAccount(){
        if(a == null){
            a = new Account();
            a.Name = 'Test Account';
            a.Customer_Type__c = 'MNC';
            a.Customer_Legal_Entity_Name__c = 'Sample Name';
            a.Selling_Entity__c = 'Telstra INC';
            a.Activated__c = True;
            a.Account_ID__c = '5555';
            insert a;
        }
        return a;
    }
    public static CostCentre__c getCostCentre(){
    if(c == null) {
        c = new CostCentre__c();
        c.Name= 'test cost';
        
   
    }
        return c;
}
public static BillProfile__c getBillProfile(){
       if(b == null){
       
         
    
     Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
    insert cntryLkupObj;
    
       City_Lookup__c cityObj = new City_Lookup__c(Name ='Bangalore',City_Code__c='Beijing',
                                                    Generic_Site_Code__c ='HK#');
    insert cityObj;
    
     Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
    insert accObj;
    
    Site__c siteObj = new Site__c(Name='Test_site',Address1__c='43',Address2__c='Bangalore',Country_Finder__c=cntryLkupObj.Id,
                             City_Finder__c=cityObj.Id,AccountId__c=accObj.Id,Address_Type__c='Billing Address');
    insert siteObj;
    
        b = new BillProfile__c(Billing_Entity__c='Telstra Limited',Account__c=siteObj.AccountId__c,
                                              Bill_Profile_Site__c=siteObj.Id, Start_Date__c= Date.today(),
                                             Invoice_Breakdown__c='Summary Page', First_Period_Date__c=Date.today(),
                                             Status__c='Active' ,Name ='test');
                                             }
   
        return b;
    }
 public static Resource__c getResourceObj(){
      Resource__c res = new Resource__c();
        res.name='resource';
        res.Path_Instance_ID__c='1467000';
        res.Parent_Path_Instance_id__c = '';
      return res;
 }  
 */   
}