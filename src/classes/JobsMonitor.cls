//Schedulable job to monitor telco jobs and restart them in case of failure
//parameters for this job configured through custom setting "Monitor Job"

// how to start: execute from Developer Console:
/*
DateTime now  = DateTime.now();
DateTime nextRunTime = now.addMinutes(2);
String cronString = '' +
    nextRunTime.second() + ' ' + nextRunTime.minute() + ' ' + 
    nextRunTime.hour() + ' ' + nextRunTime.day() + ' ' + 
    nextRunTime.month() + ' ? ' + nextRunTime.year();
System.schedule(JobsMonitor.class.getName() + '-' + now.format(), cronString, new JobsMonitor());
*/

global class JobsMonitor implements Schedulable {


    public void execute(SchedulableContext sc) {
        // get the list of telcoa jobs that are active (BatchEngine and OSModuleBatchEngine )
        List<AsyncApexJob> jobList = [select id, status, totaljobitems, jobitemsprocessed, createdDate, completeddate, methodname, apexclass.name, apexclass.NamespacePrefix,  jobtype from AsyncApexJob where ApexClass.name LIKE '%BatchEngine%' and apexclass.NamespacePrefix = 'csordtelcoa' and status NOT IN ('Aborted', 'Completed', 'Failed') order by completeddate desc];
        //get custom settings values
        MonitorJob__c mjSetting = MonitorJob__c.getOrgDefaults();
        //if:
        // - there are no active jobs, or 
        // - more time has passed since last engine execution than the engine frequency allows 
        // then: start queuable engine, send notification email and update last restarted field on monitor job custom setting
        if (jobList.isEmpty()){

            csordtelcoa.API_V1.startQueueableEngine();
            sendEmail(mjSetting);
            mjSetting.Last_Restart__c = DateTime.now();

        }

        mjSetting.Last_Checked__c = DateTime.now();
        update mjSetting;

        // Re-schedule ourself to run again in "intervalMinutes" time
        DateTime now  = DateTime.now();
        DateTime nextRunTime = now.addMinutes(Integer.valueOf(mjSetting.Interval__c));
        String cronString = '' +
            nextRunTime.second() + ' ' + nextRunTime.minute() + ' ' + 
            nextRunTime.hour() + ' ' + nextRunTime.day() + ' ' + 
            nextRunTime.month() + ' ? ' + nextRunTime.year();
        System.schedule(JobsMonitor.class.getName() + '-' + now.format(), cronString, new JobsMonitor());

        // Abort the current job
        Id jobId = sc.getTriggerId();
        System.abortJob(jobId);     
    }

    private void sendEmail (MonitorJob__c mjSetting) {
        String org = UserInfo.getUserName().substringAfterLast('.').toUpperCase() + '(' + UserInfo.getOrganizationId() + ')';
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<String> toAddresses = mjSetting.Support_Email__c.split(';');
        String body = 'Telco jobs failed on ' + org + ', restarted on '+ DateTime.now();
        mail.setToAddresses(toAddresses);
        mail.setSubject(org + ' Telco jobs failed and restarted');
        mail.setBccSender(false);
        mail.setUseSignature(false);
        mail.setHtmlBody(body);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

}