/**
 * Porcess logic after basket is created 
 */
global class CS_BatchMACDObserver implements csordcb.ObserverApi.IObserver {
    
    public CS_BatchMACDObserver() {
        
    }
    
    global void execute(csordcb.ObserverApi.Observable o, Object arg) {
        csordtelcoa.ChangeRequestObservable observable = (csordtelcoa.ChangeRequestObservable)o;
        List<Id> clonedIds = observable.getProductConfigurationIds();
        linkConfigurationsInMacd(new Set<Id>(clonedIds));
    }
    
    @TestVisible
    private void linkConfigurationsInMacd(Set<Id> cloneIds) {
    	MACDUtilities.LinkProductConfigurationsInMACD(cloneIds);
    }
}