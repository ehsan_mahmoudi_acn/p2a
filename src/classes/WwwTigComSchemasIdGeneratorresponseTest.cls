//Author: Atul
//Created Date - 10/04/2017
//Comments - Test class for wwwTigComSchemasIdGeneratorresponse because owner did not create one :)

@istest(seealldata=false)
public class WwwTigComSchemasIdGeneratorresponseTest{
     
    static testMethod void unittest() {
      wwwTigComSchemasIdGeneratorresponse.TSID_element Response = new wwwTigComSchemasIdGeneratorresponse.TSID_element();
      system.assertEquals(true,Response !=null);
    }
 }