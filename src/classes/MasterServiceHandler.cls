public class MasterServiceHandler {
    
    // method shold resolve should a batch or a single item work method should be called
    public static void process(Map<Id, cscfga__Product_Configuration__c> newPcsMap) {
        
        Map<Id, Id> basketMap = new Map<Id, String>();

        // iterate and collect data
        for (cscfga__Product_Configuration__c pc: newPcsMap.values()) {
            if (pc.cscfga__Product_Basket__c != null) {
                basketMap.put(pc.cscfga__Product_Basket__c, pc.cscfga__Product_Basket__c);
            }
        }
        
        // resolve the type of call you are going to make, single call process or batch process
        /* if (ApexPages.currentPage() != null) { 
            system.debug('>> from ApexPages.currentPage!');
            MasterServiceBatch bc = new MasterServiceBatch(basketMap.keySet()); 
            Database.executeBatch(bc, 1);
        } else */ if (basketMap.size() == 1) {
            //MasterServiceLogicOld.process(new Set<Id>(basketIds));
            MasterServiceLogic.process(basketMap.values()[0]);
        } else if (basketMap.size() > 1) {
            MasterServiceBatch bc = new MasterServiceBatch(basketMap.keySet()); 
            Database.executeBatch(bc, 1);
        }
    }
}