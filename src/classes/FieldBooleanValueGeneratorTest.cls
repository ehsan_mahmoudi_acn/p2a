@isTest
private class FieldBooleanValueGeneratorTest {
    
    static testMethod void unittest() {
        
        CurrencyValue__c cv = new CurrencyValue__c();
        cv.name = 'Currency';
        cv.Currency_Value__c = 45;
        insert cv;
        System.assertEquals('Currency',cv.Name);
        
        Schema.DescribeFieldResult SdFRcuurency = CurrencyValue__c.Currency_Value__c.getDescribe();
        
        Test.startTest();
        FieldBooleanValueGenerator fcvg = new FieldBooleanValueGenerator();
        Boolean  b = fcvg.canGenerateValueFor(SdFRcuurency);
        object obj = fcvg.generate(SdFRcuurency);
        Test.stopTest();
    
    }
}