public class MasterServiceToServiceLogic {
    
    /** Private context data **/
    private static Map<Id, csord__Service__c> serviceMap;
    private static Map<String, csord__Service__c> configServiceMap;
     
    /** Main business logic method **/
    public static List<csord__Service__c> process(List<csord__Service__c> serviceList) {
      
      Set<Id> basketIds = new Set<Id>();
      /** Iterate and collect basket data **/
      for (csord__Service__c service :serviceList) {
        if (service.csordtelcoa__Product_Basket__c != null && !basketIds.contains(service.csordtelcoa__Product_Basket__c)) {
          basketIds.add(service.csordtelcoa__Product_Basket__c);
        }
      }    
      
      /** Initialize context data **/
      initContextData(basketIds);
      
      /** Process context data **/
      return processContextData(serviceList);
    }
    
    @TestVisible
    private static void initContextData(Set<Id> basketIds) {
      
      Map<Id, csord__Service__c> allServicesMap = new Map<Id, csord__Service__c>([SELECT Id
        , Name
        , csordtelcoa__Product_Basket__c
        , csordtelcoa__Product_Basket__r.Name
        , csordtelcoa__Product_Configuration__c
        , csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__c
        , csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.Name
        , csordtelcoa__Product_Configuration__r.Master_IPVPN_Configuration__c
        , Master_Service__c
      FROM 
        csord__Service__c 
      WHERE 
        csordtelcoa__Product_Basket__c in :basketIds]);
      /** Initialize data context map **/
      serviceMap = new Map<Id, csord__Service__c>();
      configServiceMap = new Map<String, csord__Service__c>();
      
      /** Iterate and collect data **/
      for (csord__Service__c sc: allServicesMap.values()) {
        serviceMap.put(sc.Id, sc);
        configServiceMap.put(getConfigServiceKey(sc), sc);
      }
    }
    
    @TestVisible
    private static List<csord__Service__c> processContextData(List<csord__Service__c> serviceList) {

      /** Check the basket map if has items to process for **/
      if (serviceMap.Size() > 0) {
        /** Update master services references **/
        serviceList = updateServicesMasterService(serviceList);
      }
      return serviceList;
    }
    
    @TestVisible
    private static string key15(Id sfdcId) {
      return 
        sfdcId != null  
          ? string.valueOf(sfdcId).substring(0, 15) 
          : '';
    }

    @TestVisible
    private static String getConfigServiceKey(csord__Service__c service) {
      return 
        key15(service.csordtelcoa__Product_Basket__c) + key15(service.csordtelcoa__Product_Configuration__c);
    }
    
    private static String getMasterConfigServiceKey(csord__Service__c service) {
      return 
        key15(service.csordtelcoa__Product_Basket__c) + key15(service.csordtelcoa__Product_Configuration__r.Master_IPVPN_Configuration__c);
    }
    
    @TestVisible
    private static List<csord__Service__c> updateServicesMasterService(List<csord__Service__c> serviceList) {
      
      /** Iterate through the services and look for the service.configuration.master_configuration_id reference in the basker **/
      Map<Id, Id> toUpdate = new Map<Id, Id>();
      for(csord__Service__c service : serviceMap.Values()) {
        /** This condition is needed so we don't pickup the abandoned services (those that doens't have pc and pcr), which is a common situation on test environment! **/
        if (service.csordtelcoa__Product_Configuration__c != null && service.csordtelcoa__Product_Configuration__r.Master_IPVPN_Configuration__c != null) {
          csord__Service__c masterService = configServiceMap.get(getMasterConfigServiceKey(service));
          if (masterService != null) {
            toUpdate.put(service.Id, masterService.Id);
          }
        }
      }
      for(csord__Service__c service :serviceList){
        if(!toUpdate.IsEmpty() && toUpdate.get(service.Id) != null){
          service.Master_Service__c = toUpdate.get(service.Id);
        }
      }
      return serviceList;
    } 
  }