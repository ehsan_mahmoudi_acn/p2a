public with sharing class ProductDetailController {

    public String serviceId {get;set;}
    public List<cscfga__Attribute__c> attributes {get; set;}
    //for @testvisible, modified by Anuradha
	@testvisible
    private csord__Service__c service;
	@testvisible
    private List<String> additionalFields = new List<String>{'csordtelcoa__Product_Configuration__c'};

    public ProductDetailController(ApexPages.StandardController controller) {
        if (!Test.isRunningTest()) {
            controller.addFields(additionalFields);
        }
        service = (csord__Service__c)controller.getRecord(); 

        attributes = [select Id, cscfga__Attribute_Definition__r.Name, cscfga__Attribute_Definition__r.csordtelcoa__Calculate_Delta__c,cscfga__Attribute_Definition__r.cscfga__Label__c, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c = :service.csordtelcoa__Product_Configuration__c and cscfga__is_active__c=true and cscfga__Attribute_Definition__r.cscfga__Label__c!='' and cscfga__Attribute_Definition__r.csordtelcoa__Calculate_Delta__c = true and cscfga__Attribute_Definition__r.cscfga__Label__c NOT IN ('Account Id','RateCardIDTemp','RateCardTable','Add VLAN','SegmentJSON','Override Display') and cscfga__Attribute_Definition__r.cscfga__Type__c NOT IN ('Lookup','Related Product') Order By cscfga__Attribute_Definition__r.cscfga__Label__c];
        attributes = removeContractTermFromChildProduct(attributes);
    }   

    /*
    *   Child products will inherit the contract term from the parent. Remove this values from the list displayed in the clean order checklist
    */
	//for @testvisible, modified by Anuradha
	@testvisible
    private List<cscfga__Attribute__c> removeContractTermFromChildProduct(List<cscfga__Attribute__c> attribList){
        Integer contractTermIndex = null;
        Integer mainContractTermIndex = null;
        for(Integer index = 0; index < attribList.size(); index++){
            if(attribList.get(index).cscfga__Attribute_Definition__r.cscfga__Label__c == 'Main Contract Term'){
                mainContractTermIndex = index;
            }
            else if(attribList.get(index).cscfga__Attribute_Definition__r.cscfga__Label__c == 'Contract Term'){
                contractTermIndex = index;
            }
        }

        if(contractTermIndex != null && mainContractTermIndex != null){
            if(attribList.get(contractTermIndex).cscfga__Value__c != attribList.get(mainContractTermIndex).cscfga__Value__c && attribList.get(contractTermIndex).cscfga__Value__c != 'Other'){
                attribList.remove(contractTermIndex);
            }
        }

        return attribList;
    }
}