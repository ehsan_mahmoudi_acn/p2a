/**
    @author - Accenture
    @date - 07-May-2012
    @version - 1.0
    @description - This is Test class for Account Activation Required Action trigger .
*/

@isTest
private class AccountActivationReqActionTest {

/**
        @return - void 
        @description - When Account is sent for verification, Action Item is generated
*/
    static testMethod void myUnitTest() {
        Account acc= getAccount();
        acc.Account_Status__c= 'New';
        acc.Requested_for_Verification__c=True;
        update acc;
        System.assert(acc!=null);
        system.assertEquals(acc.Account_Status__c, 'New');
    }
    // Putting the values on calling the method "getAccount()"
    private static Account getAccount()
    {
        Account acc1= new Account();
        acc1.Name='abc';
        acc1.Customer_Type__c='MNC';
        acc1.CurrencyIsoCode='GBP';
        acc1.Selling_Entity__c='Telstra International HQ';
        acc1.Customer_Legal_Entity_Name__c='Test';
        //acc1.Country=' ';
        insert acc1;
        system.assert(acc1!=null);
        return acc1;
    }
}