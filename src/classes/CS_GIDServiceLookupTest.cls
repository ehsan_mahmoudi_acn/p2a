/*
Test class was converted as class - so re-created@16/2/2017
*/
@isTest(SeeAllData = false)
public class CS_GIDServiceLookupTest {

    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId = '';
    private static Id[] excludeIds = new List<Id>();
    private static Integer pageOffset, pageLimit;
    private static Object[] data = new List<Object>();
    
    private static List<Account> accountList = new List<Account>();
    private static List<csord__Service__c> serviceList = new List<csord__Service__c>();
    private static List<csord__Service__c> gidService = new List<csord__Service__c>();
    private static List<csord__Order_Request__c> orderReqList = new List<csord__Order_Request__c>();
    private static List<csord__Subscription__c> subscriptionList = new List<csord__Subscription__c>(); 
    private static List<cscfga__Product_Definition__c> pds = new List<cscfga__Product_Definition__c>(); 
    private static List<cscfga__Product_Configuration__c> pcs = new List<cscfga__Product_Configuration__c>();
    
    private static void initTestData(){
        
        accountList = P2A_TestFactoryCls.getAccounts(1);
        orderReqList = P2A_TestFactoryCls.getorderrequest(2);
        subscriptionList = P2A_TestFactoryCls.getSubscription(2, orderReqList);
       // serviceList = P2A_TestFactoryCls.getService(2, orderReqList, subscriptionList);
       // gidService = P2A_TestFactoryCls.getService(2, orderReqList, subscriptionList);
        
        pds = P2A_TestFactoryCls.getProductdef(1);
       // pcs = P2A_TestFactoryCls.getProductonfig(1);
       
        list<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> pcs = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
        
        for(cscfga__Product_Definition__c pd : pds){
            pd.Name = 'Internet-Onnet';
        }
        
        update pds;
        system.assert(pds!=null);
        System.debug('*****Product defs: ' + pds);
        
        for(cscfga__Product_Configuration__c pc : pcs){
            pc.cscfga__Product_Definition__c = pds[0].Id;
        }
        
        update pcs;
        system.assert(pcs!=null);
        System.debug('*****Product confs: ' + pcs);
        
        for(csord__Service__c service : serviceList){
            service.AccountId__c = accountList[0].Id;
            service.csordtelcoa__Product_Configuration__c = pcs[0].Id;
            service.Product_Id__c = 'GIDSSTD';
            service.GID_Service__c = null;
        }
        
      //  serviceList[0].GID_Service__c = gidService[0].Id;
        
        searchFields.put('Account_Id', accountList[0].Id);
        pageOffset = 0;
        pageLimit = 0;
        
        update serviceList;
        system.assert(serviceList!=null);
        System.debug('*****Product configs: ' + pcs);
    }
    
        static testMethod void gidServiceTest(){
    Exception ee = null;
        
        try{
            disableAll(UserInfo.getUserId());
            Test.startTest();
      initTestData();
            
            CS_GIDServiceLookup gidServiceLookup = new CS_GIDServiceLookup();  
            String account = gidServiceLookup.getRequiredAttributes();
            data = gidServiceLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            system.assertEquals(true,gidServiceLookup!=null);
            
            System.debug('*******Data: ' + data);
            
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='CS_GIDServiceLookupTest:gidServiceTest';         
            ErrorHandlerException.sendException(e); 
            ee = e;
        } finally {
            Test.stopTest();
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }
    }
    
     /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }
}