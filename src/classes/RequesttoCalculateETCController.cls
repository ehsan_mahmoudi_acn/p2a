public class RequesttoCalculateETCController {
    public Action_Item__c actionItem { get; set; }
    public Id actionItemId{get;set;}
    public Id orderId{get;set;}
    public List<Action_Item__c> actionItemList{get;set;}
    // Constructor
    public RequesttoCalculateETCController(){
        
       actionItem = new Action_Item__c();
       actionItemList=new List<Action_Item__c>(); 
       actionItemId = System.currentPageReference().getParameters().get('Id');
       system.debug('actionItemId***********'+actionItemId);
       //Enhancements as part of IR 088 -- Defect#8389 -- querying Notifying Period and Notifying Period Days fields -- Bhargav
       //Enhancements as part of IR 088 -- Defect#8390 -- querying the requested termination date and the termination reason from the action item -- Bhargav
       actionItem=[select Id,Name,Order_Action_Item__r.Status__c,Account__c,CurrencyIsoCode,Completed_By_Billing__c,Billing_Currency__c,Completion_Date_Billing__c,Order_Line_Item__r.Id,Account__r.Customer_Type__c,OwnerId,Account_Name__c,Subject__c,Service_Resource__c,Service__c,Resource__c,Billing_Commencement_Date__c,Order_Action_Item__c,Order_Action_Item__r.CreatedById,Order_Type__c,Contract_Duration__c,Contract_Expiry_Date__c,Status__c,Due_Date__c,Comments__c,Request_to_Waive_ETC__c,ETC__c,ETC_Request__c,New_ETC__c,Credit_Check_Comments__c,Special_Comments__c,Completed_By__c,Completed_By_Commercial__c,Completion_Date__c,Completion_Date_Commercial__c,Waiver_Request__c,Waiver_Request_Status__c,Notifying_Period__c,Notifying_Period_Days__c,Requested_Termination_Date__c,Termination_Reason__c,Opportunity_Email__c,Service__r.Billing_Entity__c,Resource__r.Billing_Entity__c,Assigned_To__c  from Action_Item__c where id=:actionItemId];
       system.debug('====actionItem===='+actionItem.New_ETC__c);
      
}
        
//Enhancement as part of IR 088 -- Populating Billing User email id in the Opportunity Email field -- Bhargav
public void userEmailUpdation(){
    if(actionItem.Opportunity_Email__c==null && actionItem.Completed_By_Billing__c!=null){
        List<User> user = [select id, Name, Email from User where Name=:actionItem.Completed_By_Billing__c];
        for(User userEmail : user){
        actionItem.Opportunity_Email__c = userEmail.Email;
        }
        system.debug('=====User Email for Opportunity Email field====='+actionItem.Opportunity_Email__c);
        Util.etcActionItemFlag = false;
        update actionItem;
    }
      
}

public PageReference save(){
     if(actionItem.Order_Action_Item__r.Status__c=='Cancelled'){
        ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'ETC cannot be requested/calculated/waived as the Order has been cancelled.');
        ApexPages.addMessage(errMsg);   
            
        return null;    
        }
    system.debug('====actionItem===='+actionItem.New_ETC__c);
       if(actionItem.Request_to_Waive_ETC__c && actionItem.Comments__c == null) {
           actionItem.Comments__c.addError('You must enter a value');
           return null;
        }
        
       
  
 
 //List<QueueSobject> objQueue = [Select Id,QueueId, Queue.Name from QueueSobject where SobjectType='Action_Item__c'];
  Map<String, Id> QueueMap= new Map<String, Id> ();
  for (QueueSObject q:[Select Id,QueueId, Queue.Name from QueueSobject where SobjectType='Action_Item__c']){
        QueueMap.put(q.Queue.Name,q.QueueId);
   }
   
   //**Logic Changes to create on basis of Account Owner Region
   List<User> usrLst =[Select Name, Region__c from User where id in(select ownerid from Account where id=:actionItem.Account__c) limit 1];
   User usr = usrLst[0];
   Id profileId=UserInfo.getProfileId();
  List<Profile> profileNameList=[Select Id,Name from Profile where Id=:profileId];
  String profileName=profileNameList[0].Name;
  String accCustomerType= actionItem.Account__r.Customer_Type__c;
  String billingEntiy = actionItem.Service__r.Billing_Entity__c!=null?actionItem.Service__r.Billing_Entity__c:actionItem.Resource__r.Billing_Entity__c!=null?actionItem.Resource__r.Billing_Entity__c:null;
  String queueStr='';
  
 if(usr.Region__c!=null && accCustomerType!=null){      
                if(usr.Region__c=='North Asia'&& accCustomerType=='MNC'){
                    queueStr='GPD Asia ANZ';  
                }
                else if(usr.Region__c=='South Asia'&& accCustomerType=='MNC')
                {
                    queueStr='GPD Asia ANZ';
                 }
                 else if (usr.Region__c=='Australia'&& accCustomerType=='MNC')
                {
                    queueStr='GPD Asia ANZ';
                }               
                else if(usr.Region__c=='US'&& accCustomerType=='MNC')
                {
                    queueStr='GPD ENT US EMEA';
                }
                else if(usr.Region__c=='EMEA'&& accCustomerType=='MNC')
                {
                    queueStr='GPD ENT US EMEA';
                }
                else if(accCustomerType=='GSP')
                {
                   queueStr='Commercial Global GSP';
                }
            }else 
            {
                queueStr='Commercial';
            }   
  //Changes as part of IR 088 -- Defect#8381 -- Changed LOV values of Status field wherever necessary -- Bhargav
   If(actionItem.Subject__C=='Request to Calculate Early Termination Charge' && actionItem.Status__c=='Sales User in Progress' && (profileName=='TI Account Manager' || profileName=='TI Sales Manager' || profileName=='TI Sales Admin' || profileName=='TI Order Desk' || profileName=='TI Account Manager' || profileName=='TI Terminate Team' || profileName=='System Administrator' || profileName=='TI Service Delivery' || profileName=='TI Service Delivery PM')){
  //actionItem.OwnerId=QueueMap.get('Billing - '+usr.Region__c);
  
  // IR 215 start
  if(billingEntiy!=null && System.Label.PACNET_Entities.contains(billingEntiy)){
                  actionItem.OwnerId = QueueMap.get('Billing - PACNET');
              }else{
                actionItem.OwnerId=QueueMap.get('Billing - '+usr.Region__c);
              }
   // IR 215 end
  
  List<User> SalesUserList=[Select Name, Region__c from User where id =:UserInfo.getUserId() limit 1];
  User Salesusr = SalesUserList[0];
  actionItem.Completed_By__c=Salesusr.Name;
  actionItem.Completion_Date__c=system.today();
  actionItem.Status__c='Billing User in Progress';
  system.debug('======queueStr======'+queueStr);
  }else if(actionItem.Request_to_Waive_ETC__c && actionItem.Status__c=='Billing User in Progress' && (profileName=='TI Billing' || profileName=='System Administrator')){
        system.debug('======'+'Commercial '+usr.Region__c);
        actionItem.OwnerId=QueueMap.get(queueStr);
        List<User> BillingUserList=[Select Name, Region__c from User where id =:UserInfo.getUserId() limit 1];
        User Billingusr = BillingUserList[0];
        actionItem.Completed_By_Billing__c=Billingusr.Name;
        actionItem.Completion_Date_Billing__c=system.today();
        actionItem.Status__c='Commercial User in Progress';
    }else if(!actionItem.Request_to_Waive_ETC__c && actionItem.Status__c=='Billing User in Progress' && (profileName=='TI Billing' || profileName=='System Administrator')){
        system.debug('Im here in this condition');
        actionItem.OwnerId=actionItem.Order_Action_Item__r.CreatedById;
        List<User> BillingUserList1=[Select Name, Region__c from User where id =:UserInfo.getUserId() limit 1];
        User Billingusr1 = BillingUserList1[0];
        actionItem.Completed_By_Billing__c=Billingusr1.Name;
        actionItem.Completion_Date_Billing__c=system.today();
        actionItem.Status__c='Completed';
    }else if(actionItem.Request_to_Waive_ETC__c && actionItem.Status__c=='Commercial User in Progress' && (profileName=='TI Commercial' || profileName=='TI Legal/Contracts Mgt' || profileName=='System Administrator' || profileName=='TI Pricing User')){
        system.debug('Im here in this condition');
        actionItem.OwnerId=actionItem.Order_Action_Item__r.CreatedById;
        List<User> CommercialUserList=[Select Name, Region__c from User where id =:UserInfo.getUserId() limit 1];
        User Commercialusr = CommercialUserList[0];
        actionItem.Completed_By_Commercial__c=Commercialusr.Name;
        actionItem.Completion_Date_Commercial__c=system.today();
        actionItem.Status__c='Completed';
    }
        if(profileName=='System Administrator' || profileName=='TI Terminate Team'){
        actionItem.OwnerId=actionItem.Order_Action_Item__r.CreatedById;
        if(actionItem.Request_to_Waive_ETC__c){actionItem.Completed_By_Commercial__c=usr.Name;actionItem.Completion_Date_Commercial__c=system.today();actionItem.Completed_By_Billing__c=usr.Name;actionItem.Completion_Date_Billing__c=system.today();}
        else{actionItem.Completed_By_Billing__c=usr.Name;actionItem.Completion_Date_Billing__c=system.today();actionItem.Waiver_Request__c='';actionItem.New_ETC__c=0;}
        actionItem.Status__c='Completed';
        
        
    }
   
    update actionItem;
    //Defect fix -- defect wherein currency was not flowing down correctly to the order line item -- Bhargav
    List<CurrencyType> currencyRates = [Select Id, ISOCode, ConversionRate from CurrencyType where IsActive=true];
    Decimal conversionRatio=0;
        for(Integer i = 0; i < currencyRates.size(); i++ ){
        if(actionItem.CurrencyIsoCode==currencyRates[i].ISOCode){
            conversionRatio=currencyRates[i].ConversionRate;
        }
    }
       if(actionItem.Order_Line_Item__c!=null){
        Order_Line_Item__c orderLineItemObj=[Select ID,Name,Early_Termination_charge__c,Net_NRC_Price__c from Order_Line_Item__C where id=:actionItem.Order_Line_Item__r.Id];
        /*Defect ID : 8262*/
        if(actionItem.Request_to_Waive_ETC__c){
            if(actionItem.Waiver_Request__c=='Fully Waived'){
                orderLineItemObj.Early_Termination_charge__c=0;
                //orderLineItemObj.Net_NRC_Price__c=orderLineItemObj.Net_NRC_Price__c+orderLineItemObj.Early_Termination_charge__c;
                orderLineItemObj.Net_NRC_Price__c=orderLineItemObj.Early_Termination_charge__c;
            }
            else if(actionItem.Waiver_Request__c=='Partially Waived'){
                if(actionItem.New_ETC__c!=null){
                    orderLineItemObj.Early_Termination_charge__c=actionItem.New_ETC__c;
                    //orderLineItemObj.Net_NRC_Price__c=orderLineItemObj.Net_NRC_Price__c+orderLineItemObj.Early_Termination_charge__c;
                    orderLineItemObj.Net_NRC_Price__c=orderLineItemObj.Early_Termination_charge__c;
                }
                else{
                    orderLineItemObj.Early_Termination_charge__c=actionItem.ETC__c;
                   // orderLineItemObj.Net_NRC_Price__c=orderLineItemObj.Net_NRC_Price__c+orderLineItemObj.Early_Termination_charge__c;
                     orderLineItemObj.Net_NRC_Price__c=orderLineItemObj.Early_Termination_charge__c;
                }
            }
            else if(actionItem.Waiver_Request__c=='Waiver Not Approved')
            {
                orderLineItemObj.Early_Termination_charge__c=actionItem.ETC__c;
                //orderLineItemObj.Net_NRC_Price__c=orderLineItemObj.Net_NRC_Price__c+orderLineItemObj.Early_Termination_charge__c;
                orderLineItemObj.Net_NRC_Price__c=orderLineItemObj.Early_Termination_charge__c;
            }
        }else{
            if(actionItem.ETC__c!=null){
                orderLineItemObj.Early_Termination_charge__c=actionItem.ETC__c;
               // orderLineItemObj.Net_NRC_Price__c=orderLineItemObj.Net_NRC_Price__c+orderLineItemObj.Early_Termination_charge__c; 
                orderLineItemObj.Net_NRC_Price__c=orderLineItemObj.Early_Termination_charge__c;  
            }
        }
    if(orderLineItemObj.Early_Termination_charge__c==null){
      orderLineItemObj.Early_Termination_charge__c=0;
    }
    Util.etcActionItemFlag = false;
    update orderLineItemObj;
     integer totalETCAIstatus=0;
 actionItemList=[select Id,Name,Order_Action_Item__r.Name,Order_Action_Item__r.CreatedBy.Name,Order_Action_Item__r.CreatedBy.Email,Completed_By_Billing__c,Billing_Currency__c,Completion_Date_Billing__c,Order_Line_Item__r.Id,Account__r.Customer_Type__c,OwnerId,Account_Name__c,Subject__c,Service_Resource__c,Service__r.Name,Resource__r.Name,Service__c,Resource__c,Billing_Commencement_Date__c,Order_Action_Item__c,Order_Action_Item__r.CreatedById,Order_Type__c,Contract_Duration__c,Contract_Expiry_Date__c,Status__c,Due_Date__c,Comments__c,Request_to_Waive_ETC__c,ETC__c,ETC_Request__c,New_ETC__c,Credit_Check_Comments__c,Special_Comments__c,Completed_By__c,Completed_By_Commercial__c,Completion_Date__c,Completion_Date_Commercial__c,Waiver_Request__c,Waiver_Request_Status__c from Action_Item__c where Order_Action_Item__c=:actionItem.Order_Action_Item__c];
 for(Action_Item__c ac:actionItemList){
    //Changes as part of IR 088 enhancement -- changing the condition to see how many action items have been completed -- Bhargav
   if(ac.Status__c=='Completed'){
    
    totalETCAIstatus++;
   }

}
    system.debug('==========actionItemList Size===='+actionItem.New_ETC__c);
     system.debug('=============totalETCAIstatus====='+totalETCAIstatus); 
    if(actionItemList.size()==totalETCAIstatus){
    getEmailList(actionItemList);
    
}
  
    /*Defect ID : 8262*/
    }
return new PageReference('/'+actionItem.Id);

}

  public void insertDynDataInBody(List<Action_Item__c> actionItemList,String body){
        try{
        EMailVO prEMailVO= new EMailVO();
       
        prEMailVO.to=new String[]{actionItemList[0].Order_Action_Item__r.CreatedBy.Email};
        prEMailVO.subject='Salesforce:Request to Calculate Early Termination Charge" Action Item is Complete for Order #'+actionItemList[0].Order_Action_Item__r.Name;
        String body1 = 'Hi'+' '+actionItemList[0].Order_Action_Item__r.CreatedBy.Name+','+'</br>'+'</br>';
                
        String body2 = 'This is to inform you that all "Request to Calculate ETC" Action Item(s) is complete.'+'</br>';
       // String body3=URL.getSalesforceBaseUrl().toExternalForm() + '/'+trigger.new[0].Id+'</br></br>';
        prEMailVO.body=body1+body2+'Details as follows.'+'</br>'+'</br>'+body;
        
        sendMail(prEMailVO);
        }catch (Exception ex) {
            ErrorHandlerException.ExecutingClassName='RequesttoCalculateETCController:insertDynDataInBody';
            ErrorHandlerException.objectList = actionItemList;
            ErrorHandlerException.sendException(ex);
            System.Debug('Error in inserting Dynamic data in EmailBody ..'+ex);
        }
    }
        public  Boolean sendMail(EMailVO prEMailVO){
        Boolean mailSent=false;
        try{
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(prEMailVO.to);
            mail.setSubject(prEMailVO.subject);
            mail.setHtmlBody(prEMailVO.body);
            List<Messaging.SendEmailResult> results= Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            mailSent=results.get(0).isSuccess();
            if(!mailSent){
                System.Debug('Errors while sending Mail ..'+results.get(0).getErrors()[0] );
            }
        }catch(Exception ex){
            ErrorHandlerException.ExecutingClassName='RequesttoCalculateETCController:sendMail';
            ErrorHandlerException.sendException(ex);
            System.Debug('Errors while sending Mail ..'+ex );
        }
        return mailSent;
    }
 
      public void getEmailList(List<Action_Item__c> actionItemList){
        
        List<String> dynDatas = new List<String>();
     

        //get list of all the ordered items
        string htmlOrderedList='';
        string htmlOrderedList1='';
        string htmlOrderedList2='';
        string htmlOrderedList3='';
        string htmlOrderedList4='';
         htmlOrderedList1='<style type="text/css">'+
        '.tg  {border-collapse:collapse;border-spacing:0;border-color:#000000;}'+
        '.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}'+
        '.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}'+
        '</style>';
        
        htmlOrderedList2='<table class="tg">'+
        '<tr>'+
        '<th class="tg-031e"><strong>Action Item</strong></th>'+
        '<th class="tg-031e"><strong>Service ID</strong></th>'+
        '<th class="tg-031e"><strong>Resource ID</strong></th>'+
        '<th class="tg-031e"><strong>Overall status</strong></th>'+
        '</tr>';
        
        for(Action_Item__c aIObj:actionItemList)
        {
         htmlOrderedList4+='<tr>'+
        '<td class="tg-031e">'+'<a href='+URL.getSalesforceBaseUrl().toExternalForm() + '/'+aIObj.Id+'>'+aIObj.Name+'</a></td>'
         +'<td class="tg-031e">'+(aIObj.Service__c==null?'N/A':'<a href='+URL.getSalesforceBaseUrl().toExternalForm() + '/'+aIObj.Service__c+'>'+aIObj.Service__r.Name+'</a>')+'</td>'
         +'<td class="tg-031e">'+(aIObj.Resource__c==null?'N/A':'<a href='+URL.getSalesforceBaseUrl().toExternalForm() + '/'+aIObj.Resource__c+'>'+aIObj.Resource__r.Name+'</a>')+'</td>'
         +'<td class="tg-031e">'+(aIObj.Status__c==null?'N/A':aIObj.Status__c)+'</td>'
         +'</tr>';
        
        }
        
        String htmlOrderedList5='</table>';
        
        
    
    
                 
        //insert dynamic data in email html body
        insertDynDataInBody(actionItemList,htmlOrderedList1+htmlOrderedList+htmlOrderedList2+'</br>'+htmlOrderedList3+htmlOrderedList4+htmlOrderedList5+'Thanks,'+'</br>'+'***This is an auto-generated notification, please do not reply to this email***.');
        //send email
        
        
     }
      public class EmailVO{
    public String sender{get;set;}
    public String senderDispName{get;set;}
    public String[] to{get;set;}
    public String[] cc{get;set;}
    public String[] bcc{get;set;}
    public String subject{get;set;}
    public String body{get;set;}
    public String header{get;set;}
    public String footer{get;set;}
    public String module{get;set;}
    public String originator{get;set;}
    public String replaceCharStart{get;set;}
    public String replaceCharEnd{get;set;}
    public Boolean isCIC{get;set;}
    }
 

public PageReference cancel(){
    return new PageReference('/'+actionItem.Id);
    
}
}