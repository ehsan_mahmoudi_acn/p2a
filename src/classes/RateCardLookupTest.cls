@isTest(SeeAllData = false)
private class RateCardLookupTest {

   private static List<CS_Country__c> countryList;
  private static List<IPL_Rate_Card_Item__c> ratecardlist;
    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    private static Id[] excludeIds = new List<Id>();
    private static Integer pageOffset, pageLimit;
 
    
    
    private static void initTestData(){
        countryList = new List<CS_Country__c>{
            new CS_Country__c(Name = 'Croatia'),
            new CS_Country__c(Name = 'Australia'),
            new CS_Country__c(Name = 'India'),
           new CS_Country__c(Name = 'Germany'),
           new CS_Country__c(Name = 'Hong Kong')
        };
        
        insert countryList;
        List<CS_Country__c> c = [Select id,name from CS_Country__c where name ='Croatia'];
        system.assertequals(countryList[0].name, c[0].name);
        system.assert(countryList!=null);
       ratecardlist = new List<IPL_Rate_Card_Item__c>{
       new IPL_Rate_Card_Item__c(Name = 'ratecard')
       };
       insert ratecardlist;
    }
    
  private static testMethod void doLookupSearchTest() {
        Exception ee = null;

        try{
           
            Test.startTest();
            initTestData();
            
            searchFields.put('A End Country', 'Z End Country');
            searchFields.put('searchValue', '');
            
            
            RateCardLookup RateCard = new RateCardLookup(); 
         Object[] data =   RateCard.doDynamicLookupSearch(searchFields, productDefinitionId); 
        RateCard.buildWhereClause(searchFields);
         Object[] datas = RateCard.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            String requiredAtts = RateCard.getRequiredAttributes();
            system.assert(requiredAtts!=null);
          
            
        } catch(Exception e){
            ee = e;
        } finally {
            Test.stopTest();
           
            if(ee != null){
                throw ee;
            }
        }
  }

   

      
       
}