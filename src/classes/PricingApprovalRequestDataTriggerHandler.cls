public with sharing class PricingApprovalRequestDataTriggerHandler extends BaseTriggerHandler{
        public static Boolean muteAllTriggers() {        
            try{
                if(Global_Mute__c.getInstance(UserInfo.getUserId()) != null) {
                    return Global_Mute__c.getInstance(UserInfo.getUserId()).Mute_Triggers__c;
                } else {
                    return Global_Mute__c.getOrgDefaults().Mute_Triggers__c;
                }
            } catch (Exception e) {
                return false;
              }
        }

        public PricingApprovalRequestDataTriggerHandler(){
            super();
            allowRecursion();
        }
        
        /**
         * Before update handler
         */
        public override void beforeUpdate(List<SObject> newPARD, Map<Id, SObject> newPARDMap, Map<Id, SObject> oldPARDMap){
            updateCurrency(newPARD);
        }

         /**
         * Sets the Currency on the Pricing Approval Request Data.
         */
        @testvisible
        public static void updateCurrency(List<Pricing_Approval_Request_Data__c> newPARD){
            try{
                for(Pricing_Approval_Request_Data__c pard :newPARD){
                    if(pard.CurrencyValue_of_PC__c != null && pard.CurrencyValue_of_PC__c != pard.CurrencyIsoCode){
                        pard.CurrencyIsoCode = pard.CurrencyValue_of_PC__c;
                    }
                    if(pard.Cost_RC__c != null && pard.Cost_RC__c != pard.Cost_MRC__c){
                        pard.Cost_MRC__c = pard.Cost_RC__c;
                    }
                    if(pard.Cost_NRC__c != null && pard.Cost_NRC__c != pard.Cost_NRC_Currency__c){
                        pard.Cost_NRC_Currency__c = pard.Cost_NRC__c;
                    }
                }
            } catch(exception e){
                system.debug('Error: "updateCurrency" mehotd update failure - ' +e);
            }
        }
    }