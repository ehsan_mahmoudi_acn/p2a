@isTest(seealldata=false)
public class EmailGenerationClassTest {

  static testmethod void emailgeneration() 
  { 
    EmailGenerationClass emailgen = new EmailGenerationClass (); 
    Test.startTest();    
    Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
    insert cntryLkupObj;
    system.assertEquals(true,cntryLkupObj!=null);  
    City_Lookup__c cityObj = new City_Lookup__c(Name ='Bangalore',City_Code__c='Beijing',
                                                    Generic_Site_Code__c ='HK#');
    insert cityObj;
     system.assertEquals(true,cityObj!=null);    
    Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
    insert accObj;
    system.assertEquals(true,accObj!=null);  
    Contact contObj = new Contact(Country__c=cntryLkupObj.id,AccountId=accObj.Id,LastName='tech',
                                  email='test@gmail.com');
    insert contObj; 
    system.assertEquals(true,contObj!=null);  
    Opportunity oppObj = new Opportunity(Name='GFTS Ph 2',AccountID=accObj.Id,Opportunity_Type__c='Simple',
                                           CurrencyIsoCode = 'USD', CloseDate = Date.today(),StageName='Identify & Define',Stage__c='Identify & Define',
                                            QuoteStatus__c ='Approved',Sales_Status__c= 'Won',Win_Loss_Reasons__c ='Product',
                                            Order_Type__c= 'New', ContractTerm__c = '24');
    insert oppObj;
    system.assertEquals(true,oppObj!=null);
    Site__c siteObj = new Site__c(Name='Test_site',Address1__c='43',Address2__c='Bangalore',Country_Finder__c=cntryLkupObj.Id,
                             City_Finder__c=cityObj.Id,AccountId__c=accObj.Id,Address_Type__c='Billing Address');
    insert siteObj;
    system.assertEquals(true,siteObj!=null);
    BillProfile__c billProfObj = new BillProfile__c(Billing_Entity__c='Pacnet Limited',Account__c=siteObj.AccountId__c,
                                              Bill_Profile_Site__c=siteObj.Id, Start_Date__c= Date.today(),
                                             Invoice_Breakdown__c='Summary Page', First_Period_Date__c=Date.today(),
                                             Status__c='Active' ,Name ='test');
    
    insert   billProfObj; 
    system.assertEquals(true,billProfObj!=null);
    Order__c orderObj = new Order__c(Requested_Termination_Date__c=Date.today(), Account__c =accObj.Id);
    insert orderObj; 
    system.assertEquals(true,orderObj!=null);
    order_line_item__c ordLineItemObj = new order_line_item__c(Bill_Profile__c=billProfObj.Id,
          Is_GCPE_shared_with_multiple_services__c='NA',ParentOrder__c = OrderObj.Id);
    insert ordLineItemObj;
    system.assertEquals(true,ordLineItemObj!=null);  
    
    Profile profObj = [select Id from Profile  where name ='Standard User'];
    User userObj = new User(profileId = profObj.id, username = 'mohantesh@telstra.com',
            email = 'mohantesh@telstra.com', emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
            alias='nuser1',  lastname='lastname1',EmployeeNumber='1234589',Region__c='Australia');
            insert userObj;
            system.assertEquals(true,userObj!=null); 
    
    EmailGenerationClass.emailSending(userObj, ordLineItemObj.Id)  ;
    Test.stopTest();    

  }
    
   static testmethod void insertDynDataInBody() 
   {
       EmailGenerationClass emailgen = new EmailGenerationClass ();
       EmailGenerationClass.EmailVO emailVOObj = new EmailGenerationClass.EmailVO();
       
             
       Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
       insert cntryLkupObj;
        system.assertEquals(true,cntryLkupObj!=null); 
       Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');
       insert accObj;
       list<Account> acc = [select id,name from Account where name = 'abcd'];
        system.assertEquals(accObj.name , acc[0].name);
        System.assert(accObj!=null);
       
       Order__c orderObj = new Order__c(Requested_Termination_Date__c=Date.today(),
                                         Account__c =accObj.Id);
       insert orderObj; 
       system.assertEquals(true,orderObj!=null); 
       Action_Item__c actionItemObj = new Action_Item__c(Order_Action_Item__c=orderObj.Id);
       List<Action_Item__c> actionItemList = new List<Action_Item__c>();
       actionItemList.add(actionItemObj);
        insert actionItemList;
        system.assertEquals(true,actionItemList!=null); 
       //actionItemList[0].Order_Action_Item__c = orderObj.Id;
       emailVOObj.subject = 'Hello'+actionItemList[0].Order_Action_Item__c;
       String body ='testing';
       emailVOObj.body = body;
       emailVOObj.sender ='Telstra';
       emailVOObj.senderDispName ='Telstra Account';
       emailVOObj.header = 'Head';
       emailVOObj.footer = 'foot';
       emailVOObj.module ='module Test';
       emailVOObj.originator ='test';
       emailVOObj.replaceCharStart ='test';
       emailVOObj.replaceCharEnd ='testingEnd';
       emailVOObj.isCIC = true;
       String[] toString = new String[]{'to@gmail.com'};
       String[] ccString = new String[]{'cc@gmail.com'};
       String[] bccString = new String[]{'bcc@gmail.com'};
       emailVOObj.to = toString;
       emailVOObj.cc = ccString;
       emailVOObj.bcc = bccString;
       
       Test.startTest();
       Profile profObj = [select Id from Profile  where name ='Standard User'];
       User userObj = new User(profileId = profObj.id, username = 'mohantesh12@telstra.com',
            email = 'mohantesh12@telstra.com', emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
            alias='nuser12',  lastname='lastname1',EmployeeNumber='1234589',Region__c='Australia');
            insert userObj;
            system.assertEquals(true,userObj!=null); 
       Map<Id,User> mapIdUser = new Map<Id,User>();
       mapIdUser.put(userObj.Id, userObj);
       Id idObj = userObj.Id;
       List<Id> userLst = new List<Id>();
       
       
      emailgen.insertDynDataInBody(mapIdUser, userLst, actionItemList, body) ;
       
       Test.stopTest(); 
       
   }
   static testmethod void getEmailList() 
   {
       EmailGenerationClass emailgen = new EmailGenerationClass ();
       Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
       insert cntryLkupObj;
        
       Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');
       insert accObj;
       list<Account> acc = [select id,name from Account where name = 'abcd'];
        system.assertEquals(accObj.name , acc[0].name);
        System.assert(accObj!=null);
       
       Order__c orderObj = new Order__c(Requested_Termination_Date__c=Date.today(),
                                         Account__c =accObj.Id);
       insert orderObj; 
       system.assertEquals(true,orderObj!=null); 
       Action_Item__c actionItemObj = new Action_Item__c(Order_Action_Item__c=orderObj.Id);
       List<Action_Item__c> actionItemList = new List<Action_Item__c>();
       actionItemList.add(actionItemObj);
       insert actionItemList;
       system.assertEquals(true,actionItemList!=null); 
       
       Map<Id,List<Action_Item__c>> mapActItem = new Map<Id,List<Action_Item__c>>();
       mapActItem.put(actionItemList[0].Id, actionItemList);
       Test.startTest();
       emailgen.getEmailList(mapActItem);
       Test.stopTest();
   }  
     @isTest static void testExceptions(){
         EmailGenerationClass alls=new EmailGenerationClass();
        
         try{alls.insertDynDataInBody(null,null,null,null);}catch(Exception e){}
         try{alls.sendMail(null);}catch(Exception e){}
          system.assertEquals(true,alls!=null);    
         
         
     }
   
}