@istest (seealldata = false)
public class CustomAccntMultipleSubCtrlTest{
    static csord__Service__c csServ;
    static List<csordtelcoa__Change_Types__c> changeTypeCSList;
    
    private static List<Account> accList;
    private static List<Opportunity> oppList;
    private static list<cscfga__Product_Category__c> prodCategoryLists;
    private static cscfga__Product_Category__c prodCategoryList; 
    private static List<csbb__Product_Configuration_Request__c> prodConfReq;
    private static List<cscfga__product_basket__c> prodBaskList;
    private static List<csord__Order_Request__c> requestList;
    private static List<csord__Order__c> orderList;
    private static List<cscfga__Product_Definition__c> ProductDeflist;
    private static List<cscfga__Product_Bundle__c> Pbundle;
    private static List<cscfga__Configuration_Offer__c> Offerlists;
    private static List<cscfga__Product_Configuration__c> proconfigs;
    private static List<csord__Subscription__c> subscriptionList;
    private static List<csord__Service__c> serviceList;
    private static List<csord__Service_Line_Item__c> serLineItemList;
    private static List<cscfga__Product_Basket__c> basketList;
    private static list<String> listChoice;
    private static List<csord__Subscription__c> listSubscriptionNumber;
    

    Private static void intiTestData(){
    
    P2A_TestFactoryCls.SampleTestData();
    accList = P2A_TestFactoryCls.getAccounts(1);
    oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
    prodCategoryLists=  P2A_TestFactoryCls.getProductCategory(1); 
    prodConfReq = P2A_TestFactoryCls.getProductonfigreq(1,prodCategoryLists);
    prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
    requestList = P2A_TestFactoryCls.getorderrequest(1);
    orderList = P2A_TestFactoryCls.getorder(1,requestList);
    ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
    Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
    Offerlists = P2A_TestFactoryCls.getOffers(1);   
    proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
    subscriptionList = P2A_TestFactoryCls.getSubscription(1, requestList);
    serviceList = P2A_TestFactoryCls.getService(1, requestList, subscriptionList);
    serLineItemList = P2A_TestFactoryCls.getSerLineItem(1, serviceList,requestList);
    basketList = P2A_TestFactoryCls.getProductBasket(1); 
    
    system.assert(oppList!=null);
    
    }
    
    /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        } else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
        system.assert(globalMute!=null);
    }
    
        /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableTelcoAll(Id userId) {
        csordtelcoa__Orders_Subscriptions_Options__c telcoOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance(userId);

        if(telcoOptions == null) {
            telcoOptions = new csordtelcoa__Orders_Subscriptions_Options__c ();
            telcoOptions.csordtelcoa__Disable_Triggers__c = true;
            telcoOptions.SetupOwnerId = userId;
        } else {
            telcoOptions.csordtelcoa__Disable_Triggers__c = true;
        }

        upsert telcoOptions;
        system.assert(telcoOptions!=null);
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        } else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
        system.assert(globalMute!=null);
    }

    private static void enableTelcoAll(Id userId) {
        csordtelcoa__Orders_Subscriptions_Options__c telcoOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance(userId);
        
        if(telcoOptions == null) {
            telcoOptions = new csordtelcoa__Orders_Subscriptions_Options__c ();
            telcoOptions.csordtelcoa__Disable_Triggers__c = false;
            telcoOptions.SetupOwnerId = userId;
        } else {
            telcoOptions.csordtelcoa__Disable_Triggers__c = false;
        }

        upsert telcoOptions;
        system.assert(telcoOptions!=null);
    }    
    
    
   /* private static testMethod void CustomAccountMultipleSubCtrlTest_method() {
        //id useId = UserInfo.getUserId();
        //disableAll(useId);
        //disableTelcoAll(useId);       
        //CustomAccountMultipleSubscriptionsCtrl(new ApexPages.StandardController(accList[0]));
        //enableAll(useId);
        //enableTelcoAll(useId);
    }*/
    @istest static void customAccountMultipleSubCtrlTestmethod1() {
    string a,b,c;
        Test.startTest();
        intiTestData();
        id useId = UserInfo.getUserId();
        disableAll(useId);
        disableTelcoAll(useId);
        CustomAccountMultipleSubscriptionsCtrl ctrl = new CustomAccountMultipleSubscriptionsCtrl(new ApexPages.StandardController(accList[0]));
        ctrl.createProcesses();
        ctrl.getOppRecordTypes();        
        ctrl.getNextButton();
        ctrl.getPreviousButton();
        ctrl.previousPage();
        ctrl.getPreviousChoices();
        //ctrl.nextPage();
        ctrl.rerenderButtons();
        ctrl.getInvokeProcess();
        ctrl.getRequestedDateLabel();
        ctrl.getRequestDate();
        ctrl.getChangeTypes();
        ctrl.getList();
        ctrl.nextPage();
        
        ctrl.processName = 'Termination';
        ctrl.dateString = system.today().format();
        
        
        ctrl.addMessageToPage();
        ctrl.previous();
        ctrl.next();
        system.assertEquals(true,ctrl!=null); 
        List<String> queues = CustomAccountMultipleSubscriptionsCtrl.getAvailableRecordTypeNamesForSObject(Case.SObjectType);
        List<String> queuess = CustomAccountMultipleSubscriptionsCtrl.getDefaultRecordTypeNameForSObject(Case.SObjectType);     
        csordtelcoa__Change_Types__c emp = new csordtelcoa__Change_Types__c(Name = 'Test');
        CustomAccountMultipleSubscriptionsCtrl.Change_Types_Wrapper empW = new CustomAccountMultipleSubscriptionsCtrl.Change_Types_Wrapper(emp);  //Covering inner/wrapper class
        empW.compareTo(empW);  
        ctrl.createProcesses();        
        ctrl.dateString = system.today().format();
        ctrl.changeType = 'Renewal';        
       // ctrl.CreateMultipleSubscriptionMacOpportunity();        
        //ctrl.CreateMultipleSubscriptionMacBasket();        
        CustomAccountMultipleSubscriptionsCtrl.getStatusesNotAllowingChange();
        CustomAccountMultipleSubscriptionsCtrl.getOpportunityRecordTypes();        
        ctrl.getOppRecordTypes();
        ctrl.rerenderButtons();  
        List<String> queues1 = CustomAccountMultipleSubscriptionsCtrl.getDefaultRecordTypeNameForSObject(Case.SObjectType);        
        csordtelcoa__Change_Types__c emp1 = new csordtelcoa__Change_Types__c(Name = 'Test');
        CustomAccountMultipleSubscriptionsCtrl.Change_Types_Wrapper empW1 = new CustomAccountMultipleSubscriptionsCtrl.Change_Types_Wrapper(emp1);  //Covering inner/wrapper class
        empW1.compareTo(empW1); 
        system.assertEquals(true,empW1!=null); 
        ctrl.processName = 'Termination';
        ctrl.dateString = system.today().format();
        //Date aDate = date.parse(ctr.dateString);
        ctrl.createProcesses();
        enableAll(useId);
        enableTelcoAll(useId);      
        Test.stopTest();
        system.assert(ctrl!=null);
    }
    
     @istest static  void doLookupSearchTest() {
        Exception ee = null;
        integer userid = 0;
        try{
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assertEquals(true,userid!=null); 
            //CustomAccountMultipleSubCtrlTest_method1();
            
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='CustomAccountMultipleSubCtrlTest :doLookupSearchTest';         
            ErrorHandlerException.sendException(e); 
            ee = e;
        } finally {
           
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
        
    }
    //additional test method
    @istest static void testnegativescenarios()
    {
        test.startTest();
        try{
         List<Country_Lookup__c> cLookup=P2A_TestFactoryCls.getcountry(1);
         List<csord__Order_Request__c> OrdReqList=P2A_TestFactoryCls.getorderrequest(1);
            Account a = new Account();
            a.name                          = 'Test Accenture ';
            a.BillingCountry                = 'GB';
            a.Activated__c                  = True;
            a.Account_ID__c                 = 'test';
            a.Account_Status__c             = 'Active';
            a.Customer_Legal_Entity_Name__c = 'Test';
            a.Customer_Type_New__c = 'MNC' ;
            a.Region__c = 'US';
            a.Website='abc.com';
            a.Selling_Entity__c='Telstra Limited';          
            a.Country__c=cLookup[0].Id;
            a.Industry__c='BPO';
            a.Account_ORIG_ID_DM__c = 'test';
            insert a;
            system.assertEquals(true,a!=null); 
            csord__Subscription__c sub = new csord__Subscription__c();
            sub.Name ='Test'; 
            sub.csord__Identification__c = 'Test-Catlyne-4238362';
            sub.csord__Order_Request__c =OrdReqList[0].id;
            sub.csord__Account__c=a.id;
            sub.csordtelcoa__closed_Replaced__c=false;
            sub.Path_Status__c='abc';
            sub.OrderType__c = null;
            insert sub;
            system.assertEquals(true,sub!=null); 
            List<csord__Subscription__c>subList=new List<csord__Subscription__c>();
            subList.add(sub);
            List<csord__Subscription__c>subscriptionListList=new List<csord__Subscription__c>();
            subscriptionListList.add(sub);
            csordtelcoa__Change_Types__c ch=new csordtelcoa__Change_Types__c();
            ch.name='Termination';
            ch.csordtelcoa__Allow_Multiple_Subscriptions__c=true;
            ch.csordtelcoa__Sort_Order__c=11;
            ch.csordtelcoa__Invoke_Process__c=true;
            ch.csordtelcoa__Process_To_Invoke__c='Terminate Order Creation';
            insert ch;          
            system.assertEquals(true,ch!=null); 
            
            Test.setCurrentPage(page.CustomAccountMultipleSubscriptionsPage);              
            ApexPages.StandardController sc = new ApexPages.standardController(a);
            CustomAccountMultipleSubscriptionsCtrl controller = new CustomAccountMultipleSubscriptionsCtrl(sc);
            
            //System.assertEquals(true,controller.changeTypeMap!=null);
            controller.productBasketTitle='';
            try{controller.CreateMultipleSubscriptionMacBasket();
            }catch(exception e){
                ErrorHandlerException.ExecutingClassName='CustomAccountMultipleSubCtrlTest :testnegativescenarios';         
                ErrorHandlerException.sendException(e); 
                System.debug(e);
                }
            controller.productBasketTitle='abc';
            controller.Subnumber='2';
            controller.changeType='Termination';
            controller.pageNumber=0;
            controller.title='some value';
            controller.dateString='some value';
            controller.processName = 'Termination';
            controller.subscriptions=subList;
            system.assertEquals(true,controller!=null); 
            List<SelectOption>bcc=controller.getList();
            controller.createProcesses();
            controller.CreateMultipleSubscriptionMacOpportunity();
            
            controller.pageNumber=1;
            controller.title=null;
            controller.processName = 'Termination';
            controller.dateString=system.today().format();
            List<SelectOption>bcc1=controller.getList();            
            controller.createProcesses();
            controller.CreateMultipleSubscriptionMacOpportunity();
            controller.previousChoices.add('prev');         
            controller.nextPage();
            controller.previousPage();
            controller.rerenderButtons();
            boolean aa=controller.getInvokeProcess();
            string b=controller.getRequestedDateLabel();
            boolean bb=controller.getRequestDate();
            List<SelectOption> cc=controller.getChangeTypes();
            try{//controller.CreateMultipleSubscriptionMacBasket();
            }catch(Exception e){
            ErrorHandlerException.ExecutingClassName='CustomAccountMultipleSubCtrlTest :testnegativescenarios';         
            ErrorHandlerException.sendException(e); 
            System.debug(e);}
        }catch(exception e){
            ErrorHandlerException.ExecutingClassName='CustomAccountMultipleSubCtrlTest :testnegativescenarios';         
            ErrorHandlerException.sendException(e); 
            system.debug(e);}
            test.stopTest();
           
    }
    
    @isTest static void customMasterServiceSubscriptionsCtrlMethod3(){
        
        List<csord__Order__c> Orderlist = new list<csord__Order__c>();
        P2A_TestFactoryCls.sampleTestData();
        List<Country_Lookup__c>Countrytlist  = new List<Country_Lookup__c>();
        Country_Lookup__c country = new Country_Lookup__c();
        country.name = 'test';
        country.Country_Code__c = 'TestP2A99';
        Countrytlist.add(country);
        insert Countrytlist; 
        
        List<cscfga__Product_Definition__c> pdlist = new List<cscfga__Product_Definition__c>();
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.name = 'TWI Singlehome';
        pd.cscfga__Description__c= 'GCPEbdkjdvbhe;ghwfguwefkjwf';
        pdlist.add(pd);
        insert pdlist;
        
        List<Account>AccList = new List<Account>();
        Account a = new Account();
        a.name                          = 'Test Accenture';
        a.BillingCountry                = 'GB';
        a.Activated__c                  = True;
        a.Account_ID__c                 = 'test';
        a.Account_Status__c             = 'Active';
        a.Customer_Legal_Entity_Name__c = 'Test';
        a.Customer_Type_New__c = 'GW';
        a.Region__c = 'Region';
        a.Website='abc.com';
        a.Selling_Entity__c='Telstra Limited';
        a.Country__c=Countrytlist[0].Id;
        a.Industry__c='BPO';
        a.Account_ORIG_ID_DM__c = 'test';
        AccList.add(a);
        Insert AccList; 
        
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,Products,pdlist,pbundlelist,Offerlists);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        SUBList[0].CurrencyIsoCode = 'USD';
        update SUBList;
        
       List<csord__Service__c> servListnew = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        
         changeTypeCSList = new List<csordtelcoa__Change_Types__c>();
        
        // providing the values to custom setting object
        csordtelcoa__Change_Types__c changeTypeCS = new csordtelcoa__Change_Types__c();
        changeTypeCS.Name = 'Parallel Upgrade';
        changeTypeCS.csordtelcoa__Add_To_Existing_Subscription__c = FALSE;
        changeTypeCS.csordtelcoa__Allow_Multiple_Subscriptions__c = TRUE;
        changeTypeCS.csordtelcoa__Auto_Create_Bundle__c = TRUE;
        changeTypeCS.csordtelcoa__Invoke_Process__c = TRUE;
        changeTypeCS.csordtelcoa__Process_To_Invoke__c = 'Test';
        changeTypeCS.csordtelcoa__Request_Date__c = FALSE;
        changeTypeCS.csordtelcoa__Requested_Date_Label__c = '';
        changeTypeCS.csordtelcoa__Sort_Order__c = 7;
        changeTypeCS.csordtelcoa__Subscription_Date_Field_API_Name__c = '';
        changeTypeCSList.add(changeTypeCS);
        
        changeTypeCS = new csordtelcoa__Change_Types__c();
        changeTypeCS.Name = 'Parallel Downgrade';
        changeTypeCS.csordtelcoa__Add_To_Existing_Subscription__c = FALSE;
        changeTypeCS.csordtelcoa__Allow_Multiple_Subscriptions__c = TRUE;
        changeTypeCS.csordtelcoa__Auto_Create_Bundle__c = TRUE;
        changeTypeCS.csordtelcoa__Invoke_Process__c = FALSE;
        changeTypeCS.csordtelcoa__Process_To_Invoke__c = '';
        changeTypeCS.csordtelcoa__Request_Date__c = FALSE;
        changeTypeCS.csordtelcoa__Requested_Date_Label__c = '';
        changeTypeCS.csordtelcoa__Sort_Order__c = 8;
        changeTypeCS.csordtelcoa__Subscription_Date_Field_API_Name__c = '';
        changeTypeCSList.add(changeTypeCS);
        
        insert changeTypeCSList;
        system.assert(changeTypeCSList!=null);
        string str = changeTypeCSList[0].Name;
        String productBasketTitle = 'productBasketTitle-12-07-2017';
        
        List<csord__Subscription__c> singlesub = [SELECT Id, Name, csord__Account__c, CreatedDate, CurrencyIsoCode FROM csord__Subscription__c WHERE Id = :SUBList[0].id];
        
        csordtelcoa__Change_Types__c changeType = [SELECT Id, Name,csordtelcoa__Add_To_Existing_Subscription__c, csordtelcoa__Auto_Create_Bundle__c FROM csordtelcoa__Change_Types__c WHERE Name ='Parallel Upgrade'];
        
        String subscription = singlesub[0].id;  
        String changeTypeName = changeType.Name;
                
        Test.setCurrentPage(page.CustomAccountMultipleSubscriptionsPage);              
        ApexPages.StandardController sc = new ApexPages.standardController(AccList[0]);
        CustomAccountMultipleSubscriptionsCtrl controller = new CustomAccountMultipleSubscriptionsCtrl(sc);
        Id basketId = csordtelcoa.API_V1.createMacBasketFromSubscriptions(singlesub, changeType,productBasketTitle, null);        
        String Customerservicesubscr = CustomAccountMultipleSubscriptionsCtrl.createBasketFromSubscription(subscription,changeTypeName,productBasketTitle);
        
         
            
      
      }
    
    @isTest static void serviceTerminateordermethod(){
    
        String SubscriptionList;
        boolean isService = False;
        Set<Id> subs = new Set<Id>();
        List<csord__Order__c> Orderlist = new list<csord__Order__c>();
        
        P2A_TestFactoryCls.sampleTestData();
        List<Country_Lookup__c>Countrytlist  = new List<Country_Lookup__c>();
        Country_Lookup__c country = new Country_Lookup__c();
        country.name = 'test';
        country.Country_Code__c = 'TestP2A99';
        Countrytlist.add(country);
        insert Countrytlist; 
        
        List<cscfga__Product_Definition__c> pdlist = new List<cscfga__Product_Definition__c>();
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.name = 'TWI Singlehome';
        pd.cscfga__Description__c= 'GCPEbdkjdvbhe;ghwfguwefkjwf';
        pdlist.add(pd);
        insert pdlist;
        
        List<Account>AccList = new List<Account>();
        Account a = new Account();
        a.name                          = 'Test Accenture';
        a.BillingCountry                = 'GB';
        a.Activated__c                  = True;
        a.Account_ID__c                 = 'test';
        a.Account_Status__c             = 'Active';
        a.Customer_Legal_Entity_Name__c = 'Test';
        a.Customer_Type_New__c = 'GW';
        a.Region__c = 'Region';
        a.Website='abc.com';
        a.Selling_Entity__c='Telstra Limited';
        a.Country__c=Countrytlist[0].Id;
        a.Industry__c='BPO';
        a.Account_ORIG_ID_DM__c = 'test';
        AccList.add(a);
        Insert AccList; 
        
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,Products,pdlist,pbundlelist,Offerlists);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        SUBList[0].CurrencyIsoCode = 'USD';
        update SUBList;
        
       List<csord__Service__c> servListnew = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        
         changeTypeCSList = new List<csordtelcoa__Change_Types__c>();
        
        
        Id orderid=ApexPages.currentPage().getParameters().get('Id');
        
        csord__Order__c ord = new csord__Order__c();
        ord.csord__Identification__c = 'Test-JohnSnow-4238362';
        ord.RAG_Order_Status_RED__c = false;
        ord.RAG_Reason_Code__c = '';
        ord.Id = orderid;
        ord.Order_Type__c = 'New';
        ord.Is_Terminate_Order__c = false;
        ord.Is_Order_Submitted__c = true;
        ord.SD_PM_Contact__c = NULL; 
        ord.Reseller_Account_ID__c = NULL ; 
        ord.csord__Order_Request__c = OrdReqList[0].Id;
        Orderlist.add(ord);
        insert orderlist;
        
        
        
        cscfga__Product_Configuration__c proconfig1 = new cscfga__Product_Configuration__c();
        proconfig1.cscfga__Product_basket__c = Products[0].id;
        proconfig1.cscfga__Product_Definition__c = pdlist[0].id;
        proconfig1.name ='TWI Singlehome';
        proconfig1.cscfga__contract_term_period__c = 12;
        proconfig1.Is_Offnet__c = 'yes';
        proconfig1.cscfga_Offer_Price_MRC__c = 200;
        proconfig1.cscfga_Offer_Price_NRC__c = 300;
        proconfig1.cscfga__Product_Bundle__c = pbundlelist[0].id;
        proconfig1.Rate_Card_NRC__c = 200;
        proconfig1.Rate_Card_RC__c = 300;
        proconfig1.cscfga__total_contract_value__c = 1000;
        proconfig1.cscfga__Contract_Term__c = 12;
        proconfig1.CurrencyIsoCode = 'USD';
        proconfig1.cscfga_Offer_Price_MRC__c = 100;
        proconfig1.cscfga_Offer_Price_NRC__c = 100;
        proconfig1.Child_COst__c = 100;
        proconfig1.Cost_NRC__c = 100;
        proconfig1.Cost_MRC__c = 100;
        proconfig1.Product_Name__c = 'test product';
        proconfig1.Added_Ports__c = proconfig[0].Id;
        proconfig1.cscfga__Product_Family__c = 'Point to Point';
        proconfig1.EvplId__c = proconfig1.Id;
        insert proconfig1;
        
        csServ = new csord__Service__c();
        csServ.Name = 'Test Service'; 
        csServ.csord__Identification__c = 'Test-Catlyne-4238362';
        csServ.csord__Order_Request__c = OrdReqList[0].Id;
        csServ.csord__Subscription__c = SUBList[0].Id;
        csServ.Billing_Commencement_Date__c = System.Today();
        csServ.Stop_Billing_Date__c = System.Today();
        csServ.RAG_Status_Red__c = false ; 
        csServ.RAG_Reason_Code__c = 'Text Reason';
        csServ.Opportunity__c = oppList[0].Id;
        csServ.csord__Order__c = Orderlist[0].Id;
        csServ.Customer_Required_Date__c = System.today();
        csServ.Estimated_Start_Date__c = System.now().addDays(10);
        csServ.Selling_Entity__c = 'Telstra Limited';
        csServ.Service_Type__c = 'Test service';
        csServ.Product_Code__c = 'VLV';
        csServ.Order_Channel_Type__c = 'Direct Sale';
        csServ.csordtelcoa__Product_Basket__c = Products[0].Id;
        //csServ.csordtelcoa__Product_Configuration__c = proconfig2.Id;
        csServ.Updated__c = true;
        csServ.Product_Id__c = 'VLV';
        csServ.Path_Instance_ID__c = NULL;
        csServ.AccountId__c = accList[0].Id;
        csServ.Bundle_Label_name__c = 'Test Bundle';
        csServ.Parent_Bundle_Flag__c = true;
        csServ.Inventory_Status__c = System.Label.PROVISIONED;
        csServ.csordtelcoa__Replacement_Service__c = servListnew[0].id;
        insert csServ;
        system.assert(csServ!=null);
        List<csord__Service__c> servList = [Select Id, AccountId__c, csord__Subscription__c, csord__Subscription__r.Name, csordtelcoa__Replacement_Service__c from csord__Service__c where csord__Subscription__c=:SUBList[0].Id];  
        
         for(csord__Service__c srvc:servList) {
            if(srvc.csordtelcoa__Replacement_Service__c != null) {
                if(SubscriptionList == null){
                    SubscriptionList = srvc.csord__Subscription__r.Name +', ';
                    subs.add(srvc.csord__Subscription__c);                  
                }
        
                else if(!subs.contains(srvc.csord__Subscription__c)){
                    SubscriptionList = SubscriptionList + srvc.csord__Subscription__r.Name +', ';
                    subs.add(srvc.csord__Subscription__c);
                }
                isService = True;               
            } 
                
        }
         
        Test.setCurrentPage(page.CustomAccountMultipleSubscriptionsPage);              
        ApexPages.StandardController sc = new ApexPages.standardController(AccList[0]);
        CustomAccountMultipleSubscriptionsCtrl controller = new CustomAccountMultipleSubscriptionsCtrl(sc);
        boolean boo = controller.ServiceTerminateorder(); 
         
       } 
        
        @isTest static void customMasterServiceSubscriptionsCtrlMethod1(){
        
        intiTestData();
        
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        P2A_TestFactoryCls.sampleTestData();
        
        List<Country_Lookup__c>Countrytlist  = new List<Country_Lookup__c>();
        Country_Lookup__c country = new Country_Lookup__c();
        country.name = 'test';
        country.Country_Code__c = 'TestP2A99';
        Countrytlist.add(country);
        insert Countrytlist; 
        
        List<Account>AccList = new List<Account>();
        Account a = new Account();
        a.name                          = 'Test Accenture';
        a.BillingCountry                = 'GB';
        a.Activated__c                  = True;
        a.Account_ID__c                 = 'test';
        a.Account_Status__c             = 'Active';
        a.Customer_Legal_Entity_Name__c = 'Test';
        a.Customer_Type_New__c = 'GW';
        a.Region__c = 'Region';
        a.Website='abc.com';
        a.Selling_Entity__c='Telstra Limited';
        a.Country__c=Countrytlist[0].Id;
        a.Industry__c='BPO';
        a.Account_ORIG_ID_DM__c = 'test';
        AccList.add(a);
        Insert AccList; 
        

        try{
        Test.startTest();
        PageReference tpageRef1 = Page.CustomMasterServiceSubscriptionsPage;
        Test.setCurrentPage(tpageRef1);        
        ApexPages.currentPage().getParameters().put('Id', a.id);
        ApexPages.currentPage().getParameters().put('changeType', 'Parallel Upgrade');
        
        Apexpages.StandardController stdController = new Apexpages.StandardController(AccList[0]);
        CustomAccountMultipleSubscriptionsCtrl ms  = new CustomAccountMultipleSubscriptionsCtrl(stdController);
        ms.changeType = 'Parallel Upgrade';
        ms.processName = 'Parallel Upgrade';
        system.assertEquals(true,ms!=null);
        
        csordtelcoa__Change_Types__c changeTypeCS = new csordtelcoa__Change_Types__c(Name = 'Test');
        CustomAccountMultipleSubscriptionsCtrl.Change_Types_Wrapper ctw = new CustomAccountMultipleSubscriptionsCtrl.Change_Types_Wrapper(changeTypeCS);  //Covering inner/wrapper class
        ctw.compareTo(ctw);
        
        //ms.CustomMasterServiceSubscriptionsCtrl(new ApexPages.StandardController(AccList[0]));
        //ms.getChangeTypes();
        ms.getRequestedDateLabel();
        ms.getList();
        ms.getOppRecordTypes();
        CustomAccountMultipleSubscriptionsCtrl.getStatusesNotAllowingChange();
         
        ms.getRequestDate();
        ms.getInvokeProcess();
        ms.rerenderButtons();

        List<String> strResultList = CustomAccountMultipleSubscriptionsCtrl.getAvailableRecordTypeNamesForSObject(Case.SObjectType);
        
        ms.dateString = System.today().format();
        ms.createProcesses();
        ms.processName = '';
        ms.createProcesses();
        //ms.CreateMultipleSubscriptionMacOpportunity();
        ms.Title = '';
        //ms.CreateMultipleSubscriptionMacOpportunity();
        ms.CreateMultipleSubscriptionMacBasket();
        ms.productBasketTitle = '';
        ms.CreateMultipleSubscriptionMacBasket();
        //Pagereference p1 = trcustomCon1.validateService(); 
        
        Test.stopTest();
        system.assert(ms!=null);
        }catch(Exception e){
        ErrorHandlerException.ExecutingClassName='CustomAccntMultipleSubCtrlTest:customMasterServiceSubscriptionsCtrlMethod1';         
          ErrorHandlerException.sendException(e); 
        }
        
    }
    
         
}