/**
    @author - Accenture
    @date - 26- Jun-2012
    @version - 1.0
    @description - This is the test class for trg_aiu_Quote Trigger.
*/
@isTest
private class AfterInsertUpdateQuoteTest {
/*
    static Account acc;
    static Opportunity opp;
    static Country_Lookup__c cl;
    
    static testMethod void getQuoteDetails() {
        Quote__c quote = getQuote();
        opp = getOpportunity();
        insert quote;
        
        quote.Status__c = 'Completed';
        quote.Primary__c = true;
        update quote;
        
        Test.startTest();       
        
        opp = [SELECT Id, External_Id__c, QuoteStatus__c FROM Opportunity WHERE Id =: opp.Id];
        system.assertEquals(opp.External_Id__c,quote.Name);
        system.assertEquals(opp.QuoteStatus__c,quote.Status__c);
        
        Test.stopTest();
    }
    private static Quote__c getQuote(){
        Quote__c q = new Quote__c();
        opp = getOpportunity();
        q.Opportunity__c = opp.Id;
        q.Name = 'Test Quote';
        return q;
    }
    private static Opportunity getOpportunity(){
        if(opp == null){
            opp = new Opportunity();
            Account a = getAccount();
            opp.Name = 'Test Opportunity';
            opp.AccountId = a.Id;
             //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
            opp.StageName = 'Identify & Define';
            opp.Stage__c='Identify & Define';
            opp.CloseDate = System.today();
            opp.Estimated_MRC__c=800;
            opp.Estimated_NRC__c=1000;
            opp.ContractTerm__c='10';
            
           // opp.Approx_Deal_Size__c = 9000;
            
            insert opp;
        }
        return opp;
    }
    private static Account getAccount(){
        if(acc == null){    
            acc = new Account();
            Country_Lookup__c c = getCountry();
            acc.Name = 'Test Account';
            acc.Customer_Type__c = 'MNC';
            acc.Country__c = c.Id;
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Activated__c = True;
            acc.Account_ID__c = '9090';
            acc.Customer_Legal_Entity_Name__c='Test';
            insert acc;
        }
        return acc;
    }
    private static Country_Lookup__c getCountry(){
        if(cl == null){ 
            cl = new Country_Lookup__c();
            cl.CCMS_Country_Code__c = 'HKG';
            cl.CCMS_Country_Name__c = 'India';
            cl.Country_Code__c = 'HKG';
            insert cl;
        }
        return cl;
    } */
   
}