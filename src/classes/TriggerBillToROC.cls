/**
    @author - Accenture
    @date - 02-Oct-2013
    @version - 1.0
    @description - This class is used to Trigger IP006 interface based on bundle flag and Billable Activation flag

*/

Public class TriggerBillToROC{
    
    /*Public static boolean isSendToROC = True;
    
    
    @future (callout=true)
    public static void sendOLIToROC(List<String> oLIIDLIst){
        System.debug('I am in SendOLIToROC');
        ROCTiWebservicesVo.Acknowledgement ackOBJ = null;
    
        List<Order_Line_Item__c> oLIList = new List<Order_Line_Item__c>();
        List<Id> idOLIList = new List<Id>();
        
        Id oliId;
        string oliregion;
        system.debug('oLIIDLIst**'+oLIIDLIst);
        for(String idname : oLIIDLIst){
            oliId = (ID)idname;
            idOLIList.add(oliId);
        }
        
        
       
        oLIList = [Select id,(Select Name,Id,Installment_NRC_Charge_ID__c,Installment_NRC_Amount_per_month__c,Installment_NRC_Start_Date__c,Installment_NRC_Bill_Text__c,Installment_NRC_End_Date__c,Installment_NRC_duration__c from Product_Configurations1__r),Line_Item_Status__c,Recurring_Credit_Amount__c,One_Off_Credit_Amount__c,RC_Credit_Bill_Text__c,NRC_Credit_Bill_Text__c,CPQItem__c,ParentCPQItem__c,Product_ID__c,Bill_Profile_Integration_Number__c,Root_Product_ID__c,CurrencyIsoCode,Country__c,Order_Date__c,Customer_Required_Date__c,
            Billing_Commencement_Date__c,Stop_Billing_Date__c,OrderType__c,Primary_Service_ID__c,Pin_Service_ID__c,Parent_Customer_PO__c,Product_Code__c,Zero_out_NRC_flag__c,
            Billing_Frequency__c,Cost_Centre_integration_number__c,Parent_ID__c,Site_A_For_ROC__c,Site_B_For_ROC__c,Select_committed_circuit_bandwidth__c,
            ContractTerm__c,Account_Manager__c,Net_NRC_Price__c,NRC_Price__c,Net_MRC_Price__c,MRC_Price__c,MRC_Bill_Text__c,NRC_Bill_Text__c,Service_Bill_Text__c,
            Root_Product_Bill_Text__c,Bundle_Label__c,Early_Termination_charge__c,ETC_Bill_Text__c,Product_Name__c,In_Advance_or_In_arrears__c,Align_with_Billing_Cycle__c,
            Bill_Activation_Flag__c,GenerateCreditFlag__c,Path_Id__c,name,Last_Modified_User_Emp_No__c,AEndZipPlus4__c,BEndZipPlus4__c,AEndCountryISOCode__c,BEndCountryISOCode__c,ROC_Line_Item_Status__c,
            Bundle_Flag__c,Charge_Flag__c,Usage_Base_Flag__c,Parent_of_the_bundle_flag__c,Master_Service_ID__c,Billing_Entity__c,Charge_ID__c,Parent_Bundle_ID__c,Bundle_Action__c,pivot_date__c,
            In_Service_Date__c,Zero_Charge_MRC_Flag__c,Billable_Flag__c,Charging_Frequency__c,Zero_Charge_NRC_Flag__c,Zero_Charge_ETC_Flag__c,NRC_Charge_ID__c,U2C_Master_Code__c,Validate_Order_Flag__c,Available_in_ROC__c,ParentOrder__c,Service_Type__c,Port_Type__c,Do_you_want_to_associate_a_UIA__c,Display_line_item_on_Invoice__c from Order_Line_Item__c where id in :idOLIList];
        
        //here getting all line items whose OLI status is completed,Available in ROC and validate flag is checked (Line_Item_Status__c,Validate_Order_Flag__c,Available_in_ROC__c)
        List<Order_Line_Item__c> rocOLILst = new List<Order_Line_Item__c>();
        Set<String> bundleNameSet = new Set<String>();
        Map<String,List<Order_Line_Item__c>> bundleLabelListOfOLIMap = new Map<String,List<Order_Line_Item__c>>();
        List<Order_Line_Item__c> existingOLIList = new List<Order_Line_Item__c>();
        List<Id> parentOrderIDList = new List<Id>();
        Map<String,List<Order_Line_Item__c>> cpqAndChildOLIListMap = new Map<String,List<Order_Line_Item__c>>(); //for bill activation flag
        
        List<Order_Line_Item__c> parentBillActivationList = new List<Order_Line_Item__c>();
        //IP006.......mail sending
        List<User> usrLst =[Select Name, Region__c from User where id= :UserInfo.getUserId()];
                      User usr = usrLst[0];
        
        for(Order_Line_Item__c oliObj:oLIList){
            oliregion = oliObj.Billing_Entity__c;
            if(oliObj.Line_Item_Status__c == 'Complete' && oliObj.Validate_Order_Flag__c && oliObj.Available_in_ROC__c && oliObj.ROC_Line_Item_Status__c == null ){
                System.debug('In clss TriggerBillToROC'+oliObj.Account_Manager__c);
                rocOLILst.add(oliObj);
                bundleNameSet.add(oliObj.Bundle_Label__c);
                parentOrderIDList.add(oliObj.ParentOrder__c);
                
            }
        
        }
        
        System.debug('ROC List is '+rocOLILst.size());
        
        if(rocOLILst.size() > 0){
        
            existingOLIList = [Select id,(Select Name,Id,Installment_NRC_Charge_ID__c,Installment_NRC_Amount_per_month__c,Installment_NRC_Start_Date__c,Installment_NRC_Bill_Text__c,Installment_NRC_End_Date__c,Installment_NRC_duration__c from Product_Configurations1__r),Line_Item_Status__c,Recurring_Credit_Amount__c,One_Off_Credit_Amount__c,RC_Credit_Bill_Text__c,NRC_Credit_Bill_Text__c,CPQItem__c,ParentCPQItem__c,Product_ID__c,Bill_Profile_Integration_Number__c,Root_Product_ID__c,CurrencyIsoCode,Country__c,Order_Date__c,Customer_Required_Date__c,
            Billing_Commencement_Date__c,Stop_Billing_Date__c,OrderType__c,Primary_Service_ID__c,Pin_Service_ID__c,Parent_Customer_PO__c,Product_Code__c,Zero_out_NRC_flag__c,
            Billing_Frequency__c,Cost_Centre_integration_number__c,Parent_ID__c,Site_A_For_ROC__c,Site_B_For_ROC__c,Select_committed_circuit_bandwidth__c,
            ContractTerm__c,Account_Manager__c,Net_NRC_Price__c,NRC_Price__c,Net_MRC_Price__c,MRC_Price__c,Master_Service_ID__c,MRC_Bill_Text__c,NRC_Bill_Text__c,Service_Bill_Text__c,
            Root_Product_Bill_Text__c,Bundle_Label__c,Early_Termination_charge__c,ETC_Bill_Text__c,Product_Name__c,In_Advance_or_In_arrears__c,Align_with_Billing_Cycle__c,
            Bill_Activation_Flag__c,GenerateCreditFlag__c,Path_Id__c,name,Last_Modified_User_Emp_No__c,AEndZipPlus4__c,BEndZipPlus4__c,AEndCountryISOCode__c,BEndCountryISOCode__c,ROC_Line_Item_Status__c,
            Bundle_Flag__c,Charge_Flag__c,Usage_Base_Flag__c,Parent_of_the_bundle_flag__c,Billing_Entity__c,Charge_ID__c,Parent_Bundle_ID__c,Bundle_Action__c,pivot_date__c,
            In_Service_Date__c,Zero_Charge_MRC_Flag__c,Zero_Charge_NRC_Flag__c,Zero_Charge_ETC_Flag__c,NRC_Charge_ID__c,Charging_Frequency__c,Billable_Flag__c,U2C_Master_Code__c,ParentOrder__c,Service_Type__c,Port_Type__c,Do_you_want_to_associate_a_UIA__c,Display_line_item_on_Invoice__c from Order_Line_Item__c where ParentOrder__c in :parentOrderIDList];
        }
        
        
        //if billactivation flag is true, then get all child line items.
        
        for(Order_Line_Item__c oliObj:rocOLILst){
        
            String cpqItem = oliObj.CPQItem__c;
            //If Bill Activation flag is true, then get all child line items
            if(oliObj.Bill_Activation_Flag__c){
            
                for(Order_Line_Item__c childOLIObj: existingOLIList){
                    if((childOLIObj.CPQItem__c).startswith(cpqItem) && !childOLIObj.Bundle_Flag__c && childOLIObj.ROC_Line_Item_Status__c == null ){
                        if(cpqAndChildOLIListMap.keyset().contains(cpqItem)){
                            cpqAndChildOLIListMap.get(cpqItem).add(childOLIObj);
                        
                        }else{
                            List<Order_Line_Item__c> oliList1 = new List<Order_Line_Item__c>();
                            oliList1.add(childOLIObj);
                            cpqAndChildOLIListMap.put(cpqItem,oliList1);
                        }
                    
                    }
                }
            }else{ // if bill activation flag is false, then check any of parent having bill activation flag
                        for(Order_Line_Item__c childOLIObj: existingOLIList){
                            if(childOLIObj.Bill_Activation_Flag__c && !childOLIObj.Bundle_Flag__c){
                                parentBillActivationList.add(childOLIObj);
                            }
                            
                        }
                        
                        for(Order_Line_Item__c oliObj1 : parentBillActivationList){
                            
                            if(cpqItem.startswith(oliObj1.CPQItem__c)){
                            
                            
                            }
                        
                        }
            
                }
        }
       system.debug('rocOLILst***' +rocOLILst);
      system.debug('cpqAndChildOLIListMap**' +cpqAndChildOLIListMap);
        system.debug('parentBillActivationList**' + parentBillActivationList );
        
        
        //If oli is part of bundle, then get all line items for that bundle name 
        
        for(Order_Line_Item__c oliObj:existingOLIList){
        
            if(oliObj.Bundle_Label__c != null){
            
                if(bundleLabelListOfOLIMap.keyset().contains(oliObj.Bundle_Label__c)){
                    
                    bundleLabelListOfOLIMap.get(oliObj.Bundle_Label__c).add(oliObj);
                
                }else{
                
                    List<Order_Line_Item__c> oliList11 = new List<Order_Line_Item__c>();
                    oliList11.add(oliObj);
                    bundleLabelListOfOLIMap.put(oliObj.Bundle_Label__c,oliList11);
                }
            }
        
        
        }
        
        System.debug('bundle list '+bundleLabelListOfOLIMap);
        //here finding all childs for this line item is completed and then send all bundle line items to ROC
        for(Order_Line_Item__c oliObj:rocOLILst){
            boolean allChildOLICompleted = true;
            String cpqitem = oliObj.CPQItem__c;
            //Added as part of 6901 for sending email on not hiiting Tibco
            boolean sendEmail = true;
            
            
            //Here checking whether this line item is part of Bundle
            if(oliObj.Bundle_Flag__c){
            
                List<Order_Line_Item__c> sameBundleNameList = bundleLabelListOfOLIMap.get(oliObj.Bundle_Label__c);
                
                for(Order_Line_Item__c oliChildObj : sameBundleNameList){
                    
                    if(!(oliChildObj.Line_Item_Status__c == 'Complete')&& (oliChildObj.ROC_Line_Item_Status__c == null) && oliObj.id != oliChildObj.id){
                        if(allChildOLICompleted != false){
                            System.debug('inside bundle if con'+oliChildObj.id);
                            allChildOLICompleted = false;
                        }
                    }else if(oliChildObj.Line_Item_Status__c == 'Complete' && (oliChildObj.ROC_Line_Item_Status__c != null)&& oliObj.id != oliChildObj.id){
                        if(allChildOLICompleted != false){
                            System.debug('inside bundle if con'+oliChildObj.id);
                            allChildOLICompleted = false;
                        }
                        //Added as part of 6901 defect....
                    }               
                
                }
                System.debug('abt to call '+allChildOLICompleted);
                
                if(allChildOLICompleted){
                    //Send All Order line item which are part of Bundle to ROC
                    //List<Order_Line_Item__c> sendToRocOLIList = new List<Order_Line_Item__c>();
                    
                    //sendToRocOLIList.addAll(sameBundleNameList);
                    system.debug('going into allChildOLICompleted');
                    //Creating instance of object and passing all lineitems to createUpdateBilling as argument
                    ROCTiWebservicesImplementationU2ccre.CreateUpdateBillingPortSoapPort   rocObj  = new ROCTiWebservicesImplementationU2ccre.CreateUpdateBillingPortSoapPort ();
                    CovertOLIToROCFormat convertToROCObj = new CovertOLIToROCFormat();
                    List<ROCTiWebservicesVo.OrderLineItem> rocOLILIst = convertToROCObj.requiredFormat(sameBundleNameList);
                    System.debug('Number of OLI Passes'+rocOLILIst.size());
                    ackOBJ = rocObj.CreateUpdateBilling(rocOLILIst);
                    System.debug('After calling wsdl class1');
                    
                    
                    
                
                }
            }else if(oliObj.Bill_Activation_Flag__c){
            
                    Boolean childBillActcompleted = true;
                    List<Order_Line_Item__c> childBillActList = cpqAndChildOLIListMap.get(cpqitem);
                    system.debug('childBillActList***' +childBillActList);
                    for(Order_Line_Item__c childBillOLIObj : childBillActList){
                        
                        if(!(childBillOLIObj.Line_Item_Status__c == 'Complete') && (oliObj.id !=  childBillOLIObj.id)){
                            childBillActcompleted = false;
                        }
                    
                    }
                    //If all child lineitems of Bill Activated OLi are completed,then send all line items to ROC
                    if(childBillActcompleted){
                        system.debug('Go into childBillActcompleted');
                        ROCTiWebservicesImplementationU2ccre.CreateUpdateBillingPortSoapPort   rocObj  = new ROCTiWebservicesImplementationU2ccre.CreateUpdateBillingPortSoapPort ();
                        CovertOLIToROCFormat convertToROCObj = new CovertOLIToROCFormat();
                        List<ROCTiWebservicesVo.OrderLineItem> rocOLILIst = convertToROCObj.requiredFormat(childBillActList);
                    
                        ackOBJ = rocObj.CreateUpdateBilling(rocOLILIst);
                        System.debug('After calling wsdl class2');
                             
                    
                    
                    }
            
            }else if(oliObj.Bill_Activation_Flag__c == false){ //check if any of parent line item has bill activated, if true don't send ,else send to ROC
                    
                    boolean parentBillActivatedOli = false;
                    
                    for(Order_Line_Item__c parentBillOLIObj : parentBillActivationList){
                        
                        if(cpqitem.startswith(parentBillOLIObj.CPQItem__c) && parentBillOLIObj.Line_Item_Status__c != 'Complete'){
                            if(parentBillActivatedOli != true){
                                parentBillActivatedOli = true;
                            }   
                        }
                    
                    }
                    system.debug('parentBillActivationList***'+parentBillActivationList);
                    if(!parentBillActivatedOli){
                        system.debug('Go into the parentBillActivatedOli');
                        List<Order_Line_Item__c> toPassCovertOLIObj = new List<Order_Line_Item__c>();
                        toPassCovertOLIObj.add(oliObj);
                        ROCTiWebservicesImplementationU2ccre.CreateUpdateBillingPortSoapPort   rocObj  = new ROCTiWebservicesImplementationU2ccre.CreateUpdateBillingPortSoapPort ();
                        CovertOLIToROCFormat convertToROCObj = new CovertOLIToROCFormat();
                        List<ROCTiWebservicesVo.OrderLineItem> rocOLILIst = convertToROCObj.requiredFormat(toPassCovertOLIObj);
                    
                        ackOBJ = rocObj.CreateUpdateBilling(rocOLILIst);
                        System.debug('After calling wsdl class3');
                                             
                   
                    }
                
            }
            
        
        
        }
        GenericEmailSending genericemailsend = new GenericEmailSending();
        if(ackOBJ != null && (ackOBJ.Ack_xc==False || ackOBJ.Ack_xc!=true)){
            ErrorhandlingEmail.emailSendingP2OError(genericemailsend.getRegionBasedOnBillingEntity(oliregion),oliId);
        }

        
    }
    
    /**
        This method unchecks isSendToROC to false
    */
    /*Public static void sendBillToROC(){
        isSendToROC = false;
    }*/
    
    
   
}