@isTest (SeeAllData=false)
private class ErrorMessagingExceptionTest{

@istest static void showErrorVFpage(){
ErrorHandlerMechanism__c ehm=new ErrorHandlerMechanism__c (name='Logger',isUserMessagingEnabled__c=true);
insert ehm;
system.assertEquals(true,ehm!=null); 
Exception_Setting__c eset=new Exception_Setting__c(name='System.MathException',isEnabled__c=true,errorCode__c='001');
insert eset;

try{
integer i=1/0;
}
catch(Exception e){
ErrorMessagingException.showError(e,null);
}

}


//this one is not working right now
@istest static void showErrorTrigger(){
ErrorHandlerMechanism__c ehm=new ErrorHandlerMechanism__c (name='Logger',isUserMessagingEnabled__c=true);
insert ehm;
system.assertEquals(true,ehm!=null); 
Exception_Setting__c eset=new Exception_Setting__c(name='System.MathException',isEnabled__c=true,errorCode__c='001');
insert eset;
try{
integer i=1/0;
}
catch(Exception e){
ErrorMessagingException.showError(e,eset);
}

}

@istest static void isMessagingEnabledPositive(){
    ErrorHandlerMechanism__c ehm=new ErrorHandlerMechanism__c (name='Logger',isUserMessagingEnabled__c=true);
    insert ehm;
	system.assertEquals(true,ehm!=null); 
    ErrorMessagingException.isMessagingEnabled();
}
@istest static void isMessagingEnabledNegative(){
ErrorHandlerMechanism__c ehm=new ErrorHandlerMechanism__c (name='Logger',isUserMessagingEnabled__c=false);
insert ehm;
system.assertEquals(true,ehm!=null); 
ErrorMessagingException.isMessagingEnabled();
}

@istest static void isMessagingEnabledException(){
ErrorHandlerMechanism__c ehm=new ErrorHandlerMechanism__c (name='test');
system.assertEquals(true,ehm!=null); 
ErrorMessagingException.isMessagingEnabled();
}


}