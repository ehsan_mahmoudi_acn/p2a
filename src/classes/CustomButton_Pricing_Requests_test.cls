@istest(seealldata = false)
Public class CustomButton_Pricing_Requests_test {
    public static List<User> usrList = P2A_TestFactoryCls.get_Users(1);
    public static list<Account> accList = P2A_TestFactoryCls.getAccounts(1);   
    public static List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1,accList);
    public static List<cscfga__Product_Basket__c> basketList =  P2A_TestFactoryCls.getProductBasket(1);
    public static List<csord__Order_Request__c> ordReqList =  P2A_TestFactoryCls.getorderrequest(1);
    public static List<csord__Order__c> ordList = P2A_TestFactoryCls.getorder(1, OrdReqList);
    public static List<cscfga__Product_Definition__c> prods = P2A_TestFactoryCls.getProductdef(1);
    public static List<cscfga__Configuration_Screen__c> configscreen = P2A_TestFactoryCls.getConfigScreen(1,prods);
    public static List<cscfga__Screen_Section__c> screensections=P2A_TestFactoryCls.getScreenSec(1,configscreen);
    public static List<case> caseList= P2A_TestFactoryCls.getcases(1,AccList,oppList,basketList,ordList,usrList);
    
    public static List<case> caseList1=  P2A_TestFactoryCls.getcase(1,accList);
    
    public static testmethod void customButtonPricingRequeststestmethod1(){
        
        P2A_TestFactoryCls.sampleTestData();
      //  List<cscfga__Product_Configuration__c> prodconfig = P2A_TestFactoryCls.getProductonfig(1);
                  list<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> prodconfig = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
        accList[0].Customer_Type_New__c = 'ISO';
        update accList[0];        
        system.assertEquals(true,accList[0]!=null); 
        List<cscfga__Product_Configuration__c> updatepc = new List<cscfga__Product_Configuration__c>();
        for(cscfga__Product_Configuration__c pc : prodconfig)
        {
            pc.cscfga__Product_Basket__c = basketList[0].id;
            pc.Product_Id__c = 'abc';
            updatepc.add(pc);
        }
        Test.startTest();
        update updatepc;
        system.assertEquals(true,updatepc!=null); 
        basketList[0].csbb__Account__c = accList[0].id;
        upsert basketList[0];
        system.assertEquals(basketList[0].cscfga__Opportunity__c,null);     
        
        CustomButtonPricingRequests cpes1= new CustomButtonPricingRequests();
        cpes1.performAction(basketList[0].id);
        CustomButtonPricingRequests.rtnURL(basketList[0].id);
        system.assertEquals(true,cpes1!=null); 
        Test.stopTest();
    }
    public static testmethod void customButtonPricingRequeststestmethod2()
    {
        Test.startTest();  
        CustomButtonPricingRequests cpes1= new CustomButtonPricingRequests();
        cpes1.performAction(basketList[0].id);
         system.assertEquals(true,cpes1!=null); 
        Test.stopTest();
    }
    public static testmethod void customButtonPricingRequeststestmethod3(){
        P2A_TestFactoryCls.sampleTestData(); 
        
        basketList[0].Quote_Status__c = 'Pending supplier quotation';
        update basketList[0];
          system.assertEquals(true,basketList[0]!=null); 
        Test.startTest();  
        CustomButtonPricingRequests cpes1= new CustomButtonPricingRequests();
        cpes1.performAction(basketList[0].id);
        Test.stopTest();
    }
    public static testmethod void customButtonPricingRequeststestmethod4(){
        P2A_TestFactoryCls.sampleTestData(); 
      //  list<Account> accList1 = P2A_TestFactoryCls.getAccounts(1); 
     //   List<Opportunity> oppList1 = P2A_TestFactoryCls.getOpportunitys(1,accList1);
     //   List<cscfga__Product_Configuration__c> prodconfig1 = P2A_TestFactoryCls.getProductonfig(1);
               list<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> prodconfig1 = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
        List<cscfga__Product_Basket__c> basketList1 =  P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Attribute_Definition__c >  attDef = P2A_TestFactoryCls.getAttributesdef(1,prodconfig1,prods,configscreen,screensections);
        List<cscfga__Attribute__c> attributes = P2A_TestFactoryCls.getAttributes(1,prodconfig1,attDef);
        
        List<cscfga__Product_Configuration__c> updatepc = new List<cscfga__Product_Configuration__c>();
        for(cscfga__Product_Configuration__c pc : prodconfig1)
        {
            pc.cscfga__Product_Basket__c = basketList1[0].id;
            pc.Product_Id__c = 'abc';
            pc.Name ='IPVPN';
            updatepc.add(pc);
        }
        Test.startTest();  
        update updatepc;
         system.assertEquals(true,updatepc!=null); 
        attributes[0].name = 'ContractTerm_Gen__c';
        attributes[0].cscfga__Value__c = '48';
        update attributes[0];
         system.assertEquals(true,attributes[0]!=null); 
        basketList1[0].cscfga__Opportunity__c = oppList[0].id;
        basketList1[0].csbb__Account__c  = accList[0].id;
        update basketList1[0];
        system.assertEquals(true,basketList1[0]!=null); 
        update updatepc;
        CustomButtonPricingRequests cpes1= new CustomButtonPricingRequests();
        cpes1.performAction(basketList1[0].id);
        Test.stopTest();
    }
    public static testmethod void customButtonPricingRequeststestmethod5(){
        P2A_TestFactoryCls.sampleTestData();
        case cs = new case();
        cs.status = 'unassigned';
        cs.Is_Price_Approval_Request__c = true;
        cs.Product_Basket__c = basketList[0].id;
        insert cs;
        system.assertEquals(true,cs!=null); 
        case cs1 = new case();
        cs1.status = 'unassigned';
        cs1.Is_Price_Approval_Request__c = true;
        cs1.Product_Basket__c = basketList[0].id;
        insert cs1;
         system.assertEquals(true,cs1!=null); 
        System.assert(cs!=null);
        List<case> css = [select id,status from Case where Product_Basket__c =:basketList[0].id and Is_Price_Approval_Request__c=True and status NOT IN ('Approved','Rejected')];
        System.assert(!css.isEmpty());
        Test.startTest();  
        CustomButtonPricingRequests cpes1= new CustomButtonPricingRequests();
        cpes1.performAction(basketList[0].id);
        Test.stopTest();
    }
}