global with sharing class PriceItemQueryEVPL extends cscfga.ALookupSearch {
    
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){

        String bandwidhtID = searchFields.get('Bandwidth');
        String accountID = searchFields.get('Account Id');
        String rateCardID = searchFields.get('RateCardIDTemp');
        //System.Debug('doDynamicLookupSearch' + searchFields);
        System.debug('Before query');
        List <cspmb__Price_Item__c> data = [SELECT Id, Critical_MRC__c
                                            FROM cspmb__Price_Item__c 
                                            WHERE CS_Route_Bandwidth_Product_Type_Join__r.CS_Bandwidth_Product_Type__r.CS_Bandwidth__c=:bandwidhtID 
                                            AND CS_Route_Bandwidth_Product_Type_Join__r.CS_Route_Segment__c=:rateCardID
                                            ];

        System.Debug('PriceItemQuery : '+ data);
        return data;
    }
    public override String getRequiredAttributes(){ 
        return '[ "Account Id","RateCardIDTemp","Bandwidth"]';
    }
}