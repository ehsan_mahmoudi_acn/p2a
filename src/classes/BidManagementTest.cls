@isTest(seeAllData = false)
private class BidManagementTest{
    public static List<User> usr = P2A_TestFactoryCls.get_Users(1);
    public static list<Account> accList = P2A_TestFactoryCls.getAccounts(1);   
    public static List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1,accList);
    public static List<cscfga__Product_Basket__c> basketList =  P2A_TestFactoryCls.getProductBasket(1);
    public static List<Case> caselist = P2A_TestFactoryCls.getcase(1,accList);
     public static testmethod void bidManagementTest(){
     
     basketList[0].cscfga__Opportunity__c = oppList[0].id;
      basketList[0].csordtelcoa__Synchronised_with_Opportunity__c = true;
      basketList[0].csbb__Synchronised_With_Opportunity__c = true;
      update basketList[0];
     System.RunAs(usr[0]){ 
     List<Group> groups = [select id, name from group where type = 'Queue' and Name='Bid Management Queue'];
     system.assert(!groups.isEmpty());
     //List<GroupMember> gm = [select UserOrGroupId From GroupMember where GroupId IN : groups];
    // GroupMember gm = new GroupMember();
      GroupMember grpMem1 = new GroupMember();
        grpMem1.UserOrGroupId = UserInfo.getUserId();
        grpMem1.GroupId = groups[0].Id;
        Insert grpMem1;
     
     }
     
     //caselist[0].ownerid = gm.id;
     //update caselist;
        
        
     
     
     Test.startTest();
        List<QueueSobject> queues = QueueUtil.getQueuesListForObject(Case.SObjectType);
        BidManagement.BidManagementCase(oppList);
        Test.stopTest();
     }
    
/*
Static Account acc1 = getAccount();

static opportunity opp1=getOpportunity();

  private static Account getAccount(){                
        Account acc = new Account();            
        Country_Lookup__c c = getCountry();            
        acc.Name = 'Test Account';            
        acc.Customer_Type__c = 'MNC';            
        acc.Country__c = c.Id;            
        acc.Selling_Entity__c = 'Telstra INC';  
        acc.Activated__c= true;  
        acc.Account_ID__c = '9090';  
        acc.Customer_Legal_Entity_Name__c = 'Test' ;     
        insert acc;       
        return acc;    
   } 
   private static Country_Lookup__c getCountry(){        
           Country_Lookup__c country = new Country_Lookup__c();            
           country.CCMS_Country_Code__c = 'HK';            
           country.CCMS_Country_Name__c = 'India';            
           country.Country_Code__c = 'HK';            
           insert country;        
           return country;    
         } 
           private static Opportunity getOpportunity(){                 
        Opportunity opp = new Opportunity();            
        //Account acc = getAccount();           
        opp.Name = 'Test Opportunity';            
        opp.AccountId = acc1.Id;   
         //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement         
        opp.StageName = 'Identify & Define' ;  
        opp.Stage__c= 'Identify & Define' ;          
        opp.CloseDate = System.today();   
        opp.Opportunity_Type__c = 'Complex';
        opp.Estimated_MRC__c=8;
            opp.Estimated_NRC__c=2;
            opp.ContractTerm__c='1';
            opp.Order_Type__c='New';
        //opp.Stage_Gate_1_Action_Created__c = true;         
        //opp.Approx_Deal_Size__c = 9000;                 
        return opp;    
       } 
       private static cscfga__Product_Basket__c  getProductbasket(){
      cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c ();
      pb.cscfga__Opportunity__c =opp1.id;
      pb.csordtelcoa__Synchronised_with_Opportunity__c =true;
      pb.csbb__Synchronised_With_Opportunity__c =true;
     
     return pb;  
       }
       
      
 @isTest static void getQueuesListForObjectTest() {
       List<opportunity> Optylist = new list<opportunity>();
        Optylist.add(opp1);
        Test.startTest();
        List<QueueSobject> queues = QueueUtil.getQueuesListForObject(Case.SObjectType);
        BidManagement.BidManagementCase(Optylist);
        Test.stopTest();

        System.assertNotEquals(0, queues.size());
    }   */
 
 }