@isTest
private class AccounttriggerUpdateTest {
    static Account a;
    
    static testMethod void updateAccountTest() {
            
            a = createAccount();
            a.ownerId = getUser().id;
            update a;
            system.assert(a!=null);
            a.ownerId = getUser1().id;
            update a;
            
            
           
    }
    
    private static Account createAccount()
    {
        if(a== null){
            
        
            a = new Account();
            a.Name = 'UnitTestSite1';
            a.Customer_Type__c = 'MNC';
            a.Selling_Entity__c = 'Telstra Intenational HQ'; 
            a.Customer_Legal_Entity_Name__c='Test';
            a.Activated_By__c = UserInfo.getUserId();
            
            // Start of IR 222 New Logo Changes 
            a.New_Logo__c = true;
            a.Win_Back__c = false;
            system.assert(a!=null);
            
        }  
             
       insert a; 
       
       a.New_Logo__c = false;
       a.Win_Back__c = true;
       update a;
        
       a.New_Logo__c = false;
       a.Win_Back__c = false;
       update a;
       
       // End of IR 222 New Logo     
       return a;
    }
    private static User getUser(){
        User u = new User();
        u.FirstName = 'suraj';
        u.LastName = 'Talreja';
        u.Alias = 'stalr';
        u.Email = 'suraj.talreja@accenture.com';
        u.Username = 'suraj.talreja@accenture.com';
        u.CommunityNickname = 'suraj.talreja';
        u.TimeZoneSidKey = 'Australia/Sydney';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_AU';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.EmployeeNumber='1456';
        u.ProfileId = [SELECT Id, Name FROM Profile Where Name='System Administrator' limit 1].Id;
        insert u;
        system.assert(u!=null);
        return u;
    }
    private static User getUser1(){
        User u = new User();
        u.FirstName = 'suraj1';
        u.LastName = 'Talreja';
        u.Alias = 'stalr';
        u.Email = 'suraj123.talreja@accenture.com';
        u.Username = 'suraj123.talreja@accenture.com';
        u.CommunityNickname = 'suraj1.talreja';
        u.TimeZoneSidKey = 'Australia/Sydney';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_AU';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.EmployeeNumber='14561';
        u.ProfileId = [SELECT Id, Name FROM Profile Where Name='System Administrator' limit 1].Id;
        insert u;
        system.assert(u!=null);
        return u;
    }
}