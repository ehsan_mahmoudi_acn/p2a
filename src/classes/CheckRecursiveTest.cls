@isTest
public class CheckRecursiveTest{
    
    private static Set<String> checkValues = new Set<String>();
    
    private static testMethod void testCheckRecursive(){
    string value;
    Test.startTest();
    
    List<Account>  AccList = P2A_TestFactoryCls.getAccounts(8);
    
    set<Id>AccIds = new set<Id>();
    for(Account a:AccList) {
        AccIds.add(a.id);     
    }
    
    Boolean canExecuteUpdate =  CheckRecursive.canExecuteUpdate(AccIds);
    Boolean canExecuteBlocking =  CheckRecursive.canExecuteBlocking(value);
    Boolean canExecuteToggle = CheckRecursive.canExecuteToggle(value);
    Boolean getCurrentSetting = CheckRecursive.getCurrentSetting(value);
    Boolean runCopyNniNumberToProductBasket = CheckRecursive.runCopyNniNumberToProductBasket();
    Boolean runMACDupdate = CheckRecursive.runMACDupdate();
    Boolean runCalculateTotalMargin = CheckRecursive.runCalculateTotalMargin();
    Boolean runPricingAssigntoOpportunityowner = CheckRecursive.runPricingAssigntoOpportunityowner();
    Boolean runOverridePortRating = CheckRecursive.runOverridePortRating();
    Boolean runGetCurrencyRatioMap = CheckRecursive.runGetCurrencyRatioMap();
    Boolean runOppCloseWon = CheckRecursive.runOppCloseWon();
    Boolean runAllProductBasketTrigger = CheckRecursive.runAllProductBasketTrigger();
    Boolean runAllProductBasketTriggerForAfter = CheckRecursive.runAllProductBasketTriggerForAfter();
    Boolean runValidationOptyBeforeTrigger = CheckRecursive.runValidationOptyBeforeTrigger();
    Boolean runCaseBeforeUpdate = CheckRecursive.runCaseBeforeUpdate();
    Boolean runValidationOptyAfterTrigger = CheckRecursive.runValidationOptyAfterTrigger();
    Boolean calQuoteRequest = CheckRecursive.calQuoteRequest();
    Boolean updateSite = CheckRecursive.updateSite();
    Boolean setQuoteStatus = CheckRecursive.setQuoteStatus();
    Boolean serTrgAccHdlrMthd = CheckRecursive.serTrgAccHdlrMthd();
    Boolean setOpportunityOwnerEmailId = CheckRecursive.setOpportunityOwnerEmailId();
    Boolean caseAfterUpdate = CheckRecursive.caseAfterUpdate();
    Boolean opportunityBeforeUpdate = CheckRecursive.opportunityBeforeUpdate();
    Boolean opportunityAfterUpdate = CheckRecursive.opportunityAfterUpdate();
    Boolean orderBeforeUpdate = CheckRecursive.orderBeforeUpdate();
    Boolean orderAfterUpdate = CheckRecursive.orderAfterUpdate();
    Boolean productConfigurationBeforeUpdate = CheckRecursive.productConfigurationBeforeUpdate();
    Boolean productConfigurationAfterUpdate = CheckRecursive.productConfigurationAfterUpdate();
    Boolean stepsBeforeUpdate = CheckRecursive.stepsBeforeUpdate();
    Boolean stepsAfterUpdate = CheckRecursive.stepsAfterUpdate();
    Boolean NewLogoActionItemHdlr = CheckRecursive.NewLogoActionItemHdlr();
    Boolean NewLogoActionItemHdlr1 = CheckRecursive.NewLogoActionItemHdlr1();
    Boolean NewLogoActionItemHdlr2 = CheckRecursive.NewLogoActionItemHdlr2();
    Boolean ActionItemBusinessClass = CheckRecursive.ActionItemBusinessClass();
    Boolean PricingApprovalExtCr1Class = CheckRecursive.PricingApprovalExtCr1Class();
    Boolean PricingApprovalExtCr1grand = CheckRecursive.PricingApprovalExtCr1grand();
    Boolean PricingApprovalExtCr = CheckRecursive.PricingApprovalExtCr();
    Boolean PricingApprovalclass = CheckRecursive.PricingApprovalclass();
    Boolean AttributeAfterUpdate = CheckRecursive.AttributeAfterUpdate();
    Boolean createJeopardyCase = CheckRecursive.createJeopardyCase();
    Boolean updateParentPathInventoryInstanceIDCheckRecursive = CheckRecursive.updateParentPathInventoryInstanceIDCheckRecursive(AccIds);
    Test.stopTest();
    
    system.assertEquals(true,canExecuteBlocking!=null); 
    system.assertEquals(true,stepsAfterUpdate!=null); 
    system.assertEquals(true,getCurrentSetting!=null); 
    system.assertEquals(true,runPricingAssigntoOpportunityowner!=null); 
    system.assertEquals(true,stepsAfterUpdate!=null); 
    }
}