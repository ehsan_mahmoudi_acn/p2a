@isTest (SeeAllData=false)
private class PerformanceSOVRollupControllerTest{
    
@TestSetup static void setUpTestData(){
    List<user>userList=[select Id,name,userRole.DeveloperName,SIP_User__c,IsActive from user where userroleId!=null and userrole.DeveloperName !=null Limit 1];
        userList[0].IsActive=true;
        userList[0].SIP_User__c=true;       
        update userList;          
}   

@isTest static void updatePerformanceTrackingTest(){
    
        List<Account> listAcco = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> ListOppty = P2A_TestFactoryCls.getOpportunitys(1, listAcco);              
        
        List<user>userList=[select Id,name,userRole.DeveloperName,SIP_User__c,IsActive from user where userroleId!=null and userrole.DeveloperName !=null and SIP_User__c=:true Limit 1];
        
        
        ListOppty[0].ownerId=userList[0].id;
        ListOppty[0].CurrencyIsoCode='HKD';
        ListOppty[0].Account=listAcco[0];
       
        update ListOppty[0];
        System.assert(ListOppty[0].CurrencyIsoCode!=null);
        
        System.assert(ListOppty[0].Account!=null);
        
        List<Product_Type_with_Role__c>ptcList=new List<Product_Type_with_Role__c>();
        Product_Type_with_Role__c ptcObj1=new Product_Type_with_Role__c(name='TestOPPR1',Product_Type__c='Conferencing and Collaboration (UC)',Role__c='Collaboration');
        Product_Type_with_Role__c ptcObj2=new Product_Type_with_Role__c(name='TestOPPR2',Product_Type__c='EPL Express',Role__c='FSI');
        Product_Type_with_Role__c ptcObj3=new Product_Type_with_Role__c(name='TestOPPR3',Product_Type__c='EPL OSS',Role__c='Other');
        
        ptcList.add(ptcObj1);
        ptcList.add(ptcObj2);
        ptcList.add(ptcObj3);
        
        insert ptcList;     

       /*********************************/
       List<Prod_PM_Mapping__c>pmList=new List<Prod_PM_Mapping__c>();
        Prod_PM_Mapping__c pmObj1=new Prod_PM_Mapping__c(name='TestOPPR1',PM__c='Conferencing and Collaboration (UC)',Prod_Cat__c='Collaboration');
        Prod_PM_Mapping__c pmObj2=new Prod_PM_Mapping__c(name='TestOPPR2',PM__c='EPL Express',Prod_Cat__c='FSI');
        Prod_PM_Mapping__c pmObj3=new Prod_PM_Mapping__c(name='TestOPPR3',PM__c='EPL OSS',Prod_Cat__c='Other');
        Prod_PM_Mapping__c pmObj4=new Prod_PM_Mapping__c(name='TestOPPR4',PM__c=userList[0].userRole.DeveloperName,Prod_Cat__c='Other');
        
        pmList.add(pmObj1);
        pmList.add(pmObj2);
        pmList.add(pmObj3);
        pmList.add(pmObj4);
        
        insert pmList;
       
         
         
             opportunityteammember  tm=new opportunityteammember (TeamMemberRole='Opportunity Owner',userId=userList[0].id ,
             OpportunityId=ListOppty[0].id);
             insert tm;
             
             List<OpportunitySplit>  oppSplitList=[select Total_Product_SOV__c,Opportunity.Opportunity_ID__c,SplitOwner.Profile.Name,Opportunity.CurrencyIsoCode,
             CurrencyIsoCode,isApproved__c,isRejected__c,OppSplitType__c,SplitAmount_Product_SOV__c,
             IsGAMSplit__c,Team_Role__c,Opportunityid,Opportunity.Account.Customer_Type_New__c,
             Opportunity.Account.ParentId,Opportunity.AccountId,Opportunity.Account.Parent.GAM_Account__c,
             Opportunity.Account.GAM_Account__c,Opportunity.Reported_Total_SOV__c,
             Opportunity.CloseDate,SplitOwnerId,SplitAmount from OpportunitySplit where Opportunityid =:ListOppty[0].id];
               
              
              System.assert(oppSplitList.size()>0);
              
            
              Role_Mapping_Object__c rObj1=new Role_Mapping_Object__c(name='testRole1',Parent_Role__c='Sales_Other_Key_Account_SG',User_Role__c='Asia_MNC_Key_Accounts_HK');
              Role_Mapping_Object__c rObj2=new Role_Mapping_Object__c(name='testRole2',Parent_Role__c='Asia_MNC_Key_Accounts_HK',User_Role__c='Sales_Other_Key_Account_SG');
              insert rObj1;
              insert rObj2;

              
              PerformanceSOVRollupController obj=new PerformanceSOVRollupController();
              obj.updatePerformanceTracking(oppSplitList);
              Set<Id> tempRoleset=obj.getRoleSet();
              Map<String,Id>tempgetAllUsersRole=obj.getAllUsersRoleMap();
               //System.assertEquals(tempgetAllUsersRole,null);
              //System.assertEquals(tempRoleset,null);
              set<Id>temSet1=obj.getMapRoleIdParentSet();
              Map<id,set<id>>temMap1=obj.MapRoleIdParentSet();
              Boolean isGamAcc=obj.isGAMAccount(ListOppty[0]);
              obj.getCustomerType(ListOppty[0]);
              
              
              test.startTest();
              
              try{obj.retry(null,null);}catch(Exception e){}//scenario 2
              test.stopTest();
              
}

@isTest static void getperformanceTrackingListTest(){
    
    List<Account> listAcco = P2A_TestFactoryCls.getAccounts(1);
    List<Opportunity> ListOppty = P2A_TestFactoryCls.getOpportunitys(1, listAcco);
    ListOppty[0].CloseDate     = system.today() + 300;
    ListOppty[0].CurrencyIsoCode='HKD'; 
    update ListOppty;
    System.assert(ListOppty[0].CloseDate!=null);
        
    List<user>userList=[select Id,name,userRole.DeveloperName,SIP_User__c,IsActive from user where userroleId!=null and userrole.DeveloperName !=null and SIP_User__c=:true Limit 1];
        
    
    PerformanceSOVRollupController obj=new PerformanceSOVRollupController();
    
    PerformanceSOVRollupController.FiscalYear fiscalYearObj=obj.getQuarter(ListOppty[0].CloseDate);
    Id OpportunityId=ListOppty[0].id;
    
    
    //first insertion
    Performance_Tracking__c perfobj=new Performance_Tracking__c();
                perfobj.Employee__c=userList[0].id;
                perfobj.Fiscal_year__c=fiscalYearObj.fiscalyear;
                perfobj.SOV_Type__c='Performance';               
                perfobj.OwnerId=perfobj.Employee__c;
                perfobj.ParentRoleId__c='';
                //common
                perfobj.Q1_Rolled_Up__c=0;
                perfobj.Q1_Individual_Performance__c=0;
                perfobj.Q2_Rolled_Up__c=0;
                perfobj.Q2_Individual_Performance__c=0;
                perfobj.Q3_Rolled_Up__c=0;
                perfobj.Q3_Individual_Performance__c=0;
                perfobj.Q4_Rolled_Up__c=0;
                perfobj.Q1_SOV_Credit__c=0;
                perfobj.Q2_SOV_Credit__c=0;
                perfobj.Q3_SOV_Credit__c=0;
                perfobj.Q4_SOV_Credit__c=0;
                perfobj.Q4_Individual_Performance__c=0;
                perfobj.Q1_GAM_Roll_Up__c=0;
                perfobj.Q2_GAM_Roll_Up__c=0;
                perfobj.Q3_GAM_Roll_Up__c=0;
                perfobj.Q4_GAM_Roll_Up__c=0;
                perfObj.Opportunity_Look_Up__c = ListOppty[0].Id; 
                 //sov in Opp currency
                perfobj.Q1_Rolled_Up_Opp__c=0;
                perfobj.Q1_Individual_Performance_Opp__c=0;
                perfobj.Q2_Rolled_Up_Opp__c=0;
                perfobj.Q2_Individual_Performance_Opp__c=0;
                perfobj.Q3_Rolled_Up_Opp__c=0;
                perfobj.Q3_Individual_Performance_Opp__c=0;
                perfobj.Q4_Rolled_Up_Opp__c=0;
                perfobj.Q4_Individual_Performance_Opp__c=0;
                if(perfobj.Currency__c==null ){perfobj.Currency__c='AUD';}
                perfobj.CurrencyIsoCode=perfobj.Currency__c;
                List<Performance_Tracking__c> perflist=new list<Performance_Tracking__c>(); 
                
                perflist.add(perfobj);
            
/********************************************************************************************************/          
            //second insertion
               Performance_Tracking__c perfobj1=new Performance_Tracking__c();
                perfobj1.Employee__c=userList[0].id;
                perfobj1.Fiscal_year__c=fiscalYearObj.fiscalyear;
                perfobj1.SOV_Type__c='Transaction CR/DR';
               
                perfobj1.OwnerId=perfobj.Employee__c;
                perfobj1.ParentRoleId__c='';
                //common
                perfobj1.Q1_Rolled_Up__c=0;
                perfobj1.Q1_Individual_Performance__c=0;
                perfobj1.Q2_Rolled_Up__c=0;
                perfobj1.Q2_Individual_Performance__c=0;
                perfobj1.Q3_Rolled_Up__c=0;
                perfobj1.Q3_Individual_Performance__c=0;
                perfobj1.Q4_Rolled_Up__c=0;
                perfobj1.Q1_SOV_Credit__c=0;
                perfobj1.Q2_SOV_Credit__c=0;
                perfobj1.Q3_SOV_Credit__c=0;
                perfobj1.Q4_SOV_Credit__c=0;
                perfobj1.Q4_Individual_Performance__c=0;
                perfobj1.Q1_GAM_Roll_Up__c=0;
                perfobj1.Q2_GAM_Roll_Up__c=0;
                perfobj1.Q3_GAM_Roll_Up__c=0;
                perfobj1.Q4_GAM_Roll_Up__c=0;
                perfobj1.Opportunity_Look_Up__c = ListOppty[0].Id; 
                 //sov in Opp currency
                perfobj1.Q1_Rolled_Up_Opp__c=0;
                perfobj1.Q1_Individual_Performance_Opp__c=0;
                perfobj1.Q2_Rolled_Up_Opp__c=0;
                perfobj1.Q2_Individual_Performance_Opp__c=0;
                perfobj1.Q3_Rolled_Up_Opp__c=0;
                perfobj1.Q3_Individual_Performance_Opp__c=0;
                perfobj1.Q4_Rolled_Up_Opp__c=0;
                perfobj1.Q4_Individual_Performance_Opp__c=0;
                if(perfobj1.Currency__c==null ){perfobj1.Currency__c='AUD';}
                perfobj1.CurrencyIsoCode=perfobj.Currency__c;                
                
                perflist.add(perfobj1);
                
                insert perflist;
                PerformanceSOVRollupController.FiscalYear fiscalYearObj1=obj.getQuarter(system.today());
                for(Performance_Tracking__c perf:perflist){
                perf.Currency__c='INR';
                perf.fiscal_year__c=fiscalYearObj1.fiscalyear;
                }
    update perflist;
    List<Performance_Tracking__c>ptObj=obj.getperformanceTrackingList(fiscalYearObj,OpportunityId);
    
}

@isTest static void debitPerformanceTrackingTest(){
    
        List<Account> listAcco = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> ListOppty = P2A_TestFactoryCls.getOpportunitys(1, listAcco);
        
        
        List<user>userList=[select Id,name,userRole.DeveloperName,SIP_User__c,IsActive from user where userroleId!=null and userrole.DeveloperName !=null and SIP_User__c=:true Limit 1];
        
        
        ListOppty[0].ownerId=userList[0].id;
        ListOppty[0].CurrencyIsoCode='HKD';
        ListOppty[0].Account=listAcco[0];
        ListOppty[0].CloseDate = system.today() + 300;
       
        update ListOppty;
        System.assert(ListOppty[0].CurrencyIsoCode!=null);
        
        System.assert(ListOppty[0].Account!=null);
        
        List<Product_Type_with_Role__c>ptcList=new List<Product_Type_with_Role__c>();
        Product_Type_with_Role__c ptcObj1=new Product_Type_with_Role__c(name='TestOPPR1',Product_Type__c='Conferencing and Collaboration (UC)',Role__c='Collaboration');
        Product_Type_with_Role__c ptcObj2=new Product_Type_with_Role__c(name='TestOPPR2',Product_Type__c='EPL Express',Role__c='FSI');
        Product_Type_with_Role__c ptcObj3=new Product_Type_with_Role__c(name='TestOPPR3',Product_Type__c='EPL OSS',Role__c='Other');
        
        ptcList.add(ptcObj1);
        ptcList.add(ptcObj2);
        ptcList.add(ptcObj3);
        
        insert ptcList;     

       /*********************************/
       List<Prod_PM_Mapping__c>pmList=new List<Prod_PM_Mapping__c>();
        Prod_PM_Mapping__c pmObj1=new Prod_PM_Mapping__c(name='TestOPPR1',PM__c='Conferencing and Collaboration (UC)',Prod_Cat__c='Collaboration');
        Prod_PM_Mapping__c pmObj2=new Prod_PM_Mapping__c(name='TestOPPR2',PM__c='EPL Express',Prod_Cat__c='FSI');
        Prod_PM_Mapping__c pmObj3=new Prod_PM_Mapping__c(name='TestOPPR3',PM__c='EPL OSS',Prod_Cat__c='Other');
        Prod_PM_Mapping__c pmObj4=new Prod_PM_Mapping__c(name='TestOPPR4',PM__c=userList[0].userRole.DeveloperName,Prod_Cat__c='Other');
        
        pmList.add(pmObj1);
        pmList.add(pmObj2);
        pmList.add(pmObj3);
        pmList.add(pmObj4);
        
        insert pmList;
       
         
         
             opportunityteammember  tm=new opportunityteammember (TeamMemberRole='Opportunity Owner',userId=userList[0].id ,
             OpportunityId=ListOppty[0].id);
             insert tm;
             
             List<OpportunitySplit>  oppSplitList=[select Total_Product_SOV__c,Opportunity.Opportunity_ID__c,SplitOwner.Profile.Name,Opportunity.CurrencyIsoCode,
             CurrencyIsoCode,isApproved__c,isRejected__c,OppSplitType__c,SplitAmount_Product_SOV__c,
             IsGAMSplit__c,Team_Role__c,Opportunityid,Opportunity.Account.Customer_Type_New__c,
             Opportunity.Account.ParentId,Opportunity.AccountId,Opportunity.Account.Parent.GAM_Account__c,
             Opportunity.Account.GAM_Account__c,Opportunity.Reported_Total_SOV__c,
             Opportunity.CloseDate,SplitOwnerId,SplitAmount from OpportunitySplit where Opportunityid =:ListOppty[0].id];
             
             PerformanceSOVRollupController obj=new PerformanceSOVRollupController();
    
             PerformanceSOVRollupController.FiscalYear fiscalYearObj=obj.getQuarter(ListOppty[0].CloseDate);
             PerformanceSOVRollupController.FiscalYear fiscalYearObj1=obj.getQuarter(Date.newInstance(2016, 01, 01));
             PerformanceSOVRollupController.FiscalYear fiscalYearObj2=obj.getQuarter(Date.newInstance(2016, 05, 01));
             PerformanceSOVRollupController.FiscalYear fiscalYearObj3=obj.getQuarter(Date.newInstance(2016, 08, 01));
             PerformanceSOVRollupController.FiscalYear fiscalYearObj4=obj.getQuarter(Date.newInstance(2016, 11, 01));
             
             Id OpportunityId=ListOppty[0].id;
    
    
                //first insertion
                Performance_Tracking__c perfobj=new Performance_Tracking__c();
                perfobj.Employee__c=userList[0].id;
                perfobj.Fiscal_year__c=fiscalYearObj.fiscalyear;
                perfobj.SOV_Type__c='Performance';               
                perfobj.OwnerId=perfobj.Employee__c;
                perfobj.ParentRoleId__c='';
                //common
                perfobj.Q1_Rolled_Up__c=0;
                perfobj.Q1_Individual_Performance__c=0;
                perfobj.Q2_Rolled_Up__c=0;
                perfobj.Q2_Individual_Performance__c=0;
                perfobj.Q3_Rolled_Up__c=0;
                perfobj.Q3_Individual_Performance__c=0;
                perfobj.Q4_Rolled_Up__c=0;
                perfobj.Q1_SOV_Credit__c=0;
                perfobj.Q2_SOV_Credit__c=0;
                perfobj.Q3_SOV_Credit__c=0;
                perfobj.Q4_SOV_Credit__c=0;
                perfobj.Q4_Individual_Performance__c=0;
                perfobj.Q1_GAM_Roll_Up__c=0;
                perfobj.Q2_GAM_Roll_Up__c=0;
                perfobj.Q3_GAM_Roll_Up__c=0;
                perfobj.Q4_GAM_Roll_Up__c=0;
                perfObj.Opportunity_Look_Up__c = ListOppty[0].Id; 
                 //sov in Opp currency
                perfobj.Q1_Rolled_Up_Opp__c=0;
                perfobj.Q1_Individual_Performance_Opp__c=0;
                perfobj.Q2_Rolled_Up_Opp__c=0;
                perfobj.Q2_Individual_Performance_Opp__c=0;
                perfobj.Q3_Rolled_Up_Opp__c=0;
                perfobj.Q3_Individual_Performance_Opp__c=0;
                perfobj.Q4_Rolled_Up_Opp__c=0;
                perfobj.Q4_Individual_Performance_Opp__c=0;
                if(perfobj.Currency__c==null ){perfobj.Currency__c='AUD';}
                perfobj.CurrencyIsoCode=perfobj.Currency__c;
                List<Performance_Tracking__c> perflist=new list<Performance_Tracking__c>(); 
                
                perflist.add(perfobj);
            
/********************************************************************************************************/          
            //second insertion
               Performance_Tracking__c perfobj1=new Performance_Tracking__c();
                perfobj1.Employee__c=userList[0].id;
                perfobj1.Fiscal_year__c=fiscalYearObj.fiscalyear;
                perfobj1.SOV_Type__c='Transaction CR/DR';
               
                perfobj1.OwnerId=perfobj.Employee__c;
                perfobj1.ParentRoleId__c='';
                //common
                perfobj1.Q1_Rolled_Up__c=0;
                perfobj1.Q1_Individual_Performance__c=0;
                perfobj1.Q2_Rolled_Up__c=0;
                perfobj1.Q2_Individual_Performance__c=0;
                perfobj1.Q3_Rolled_Up__c=0;
                perfobj1.Q3_Individual_Performance__c=0;
                perfobj1.Q4_Rolled_Up__c=0;
                perfobj1.Q1_SOV_Credit__c=0;
                perfobj1.Q2_SOV_Credit__c=0;
                perfobj1.Q3_SOV_Credit__c=0;
                perfobj1.Q4_SOV_Credit__c=0;
                perfobj1.Q4_Individual_Performance__c=0;
                perfobj1.Q1_GAM_Roll_Up__c=0;
                perfobj1.Q2_GAM_Roll_Up__c=0;
                perfobj1.Q3_GAM_Roll_Up__c=0;
                perfobj1.Q4_GAM_Roll_Up__c=0;
                perfobj1.Opportunity_Look_Up__c = ListOppty[0].Id; 
                 //sov in Opp currency
                perfobj1.Q1_Rolled_Up_Opp__c=0;
                perfobj1.Q1_Individual_Performance_Opp__c=0;
                perfobj1.Q2_Rolled_Up_Opp__c=0;
                perfobj1.Q2_Individual_Performance_Opp__c=0;
                perfobj1.Q3_Rolled_Up_Opp__c=0;
                perfobj1.Q3_Individual_Performance_Opp__c=0;
                perfobj1.Q4_Rolled_Up_Opp__c=0;
                perfobj1.Q4_Individual_Performance_Opp__c=0;
                if(perfobj1.Currency__c==null ){perfobj1.Currency__c='AUD';}
                perfobj1.CurrencyIsoCode=perfobj.Currency__c;                
                
                perflist.add(perfobj1);
                perflist.add(getTrackingObj(ListOppty,fiscalYearObj1.fiscalyear,userList,'Performance'));
                perflist.add(getTrackingObj(ListOppty,fiscalYearObj1.fiscalyear,userList,'Transaction CR/DR'));
                perflist.add(getTrackingObj(ListOppty,fiscalYearObj2.fiscalyear,userList,'Performance'));
                perflist.add(getTrackingObj(ListOppty,fiscalYearObj2.fiscalyear,userList,'Transaction CR/DR'));
                perflist.add(getTrackingObj(ListOppty,fiscalYearObj3.fiscalyear,userList,'Performance'));
                perflist.add(getTrackingObj(ListOppty,fiscalYearObj3.fiscalyear,userList,'Transaction CR/DR'));
                perflist.add(getTrackingObj(ListOppty,fiscalYearObj4.fiscalyear,userList,'Performance'));
                perflist.add(getTrackingObj(ListOppty,fiscalYearObj4.fiscalyear,userList,'Transaction CR/DR'));
                
                insert perflist;
                
                Boolean returnflag=obj.debitPerformanceTracking(oppSplitList);
                obj.updatePerformanceTracking(oppSplitList);
 
}



public static Performance_Tracking__c getTrackingObj(List<opportunity>ListOppty,String fiscalYear,List<user>userList,String SOVType){
    
                Performance_Tracking__c perfobj=new Performance_Tracking__c();
                perfobj.Employee__c=userList[0].id;
                perfobj.Fiscal_year__c=fiscalYear;
                perfobj.SOV_Type__c=SOVType;               
                perfobj.OwnerId=perfobj.Employee__c;
                perfobj.ParentRoleId__c='';
                //common
                perfobj.Q1_Rolled_Up__c=0;
                perfobj.Q1_Individual_Performance__c=0;
                perfobj.Q2_Rolled_Up__c=0;
                perfobj.Q2_Individual_Performance__c=0;
                perfobj.Q3_Rolled_Up__c=0;
                perfobj.Q3_Individual_Performance__c=0;
                perfobj.Q4_Rolled_Up__c=0;
                perfobj.Q1_SOV_Credit__c=0;
                perfobj.Q2_SOV_Credit__c=0;
                perfobj.Q3_SOV_Credit__c=0;
                perfobj.Q4_SOV_Credit__c=0;
                perfobj.Q4_Individual_Performance__c=0;
                perfobj.Q1_GAM_Roll_Up__c=0;
                perfobj.Q2_GAM_Roll_Up__c=0;
                perfobj.Q3_GAM_Roll_Up__c=0;
                perfobj.Q4_GAM_Roll_Up__c=0;
                perfObj.Opportunity_Look_Up__c = ListOppty[0].Id; 
                 //sov in Opp currency
                perfobj.Q1_Rolled_Up_Opp__c=0;
                perfobj.Q1_Individual_Performance_Opp__c=0;
                perfobj.Q2_Rolled_Up_Opp__c=0;
                perfobj.Q2_Individual_Performance_Opp__c=0;
                perfobj.Q3_Rolled_Up_Opp__c=0;
                perfobj.Q3_Individual_Performance_Opp__c=0;
                perfobj.Q4_Rolled_Up_Opp__c=0;
                perfobj.Q4_Individual_Performance_Opp__c=0;
                if(perfobj.Currency__c==null ){perfobj.Currency__c='AUD';}
                perfobj.CurrencyIsoCode=perfobj.Currency__c;                
    
                return perfobj;
}
@istest
public static void testcatchblock()
{

PerformanceSOVRollupController obj=new PerformanceSOVRollupController();
  try
 {
 obj.updatePerformanceTracking(null);
 }catch(Exception e){}
 try
 {
      List<Performance_Tracking__c>ptObj=obj.getperformanceTrackingList(null,null);
  }catch(Exception e){}
  try
  {
        PerformanceSOVRollupController.FiscalYear fiscalYearObj=obj.getQuarter(null);
}catch(Exception e){}
try
{
    Boolean isGamAcc=obj.isGAMAccount(null);
}catch(Exception e){}
 try
 {
   obj.createUpdatePerformanceRecord(null,null,null,null,null);
 }catch(Exception e){}
 try
 {
   Boolean returnflag=obj.debitPerformanceTracking(null);
 }catch(Exception e){}
 system.assertEquals(true,obj!=null);
}

}