public with sharing class MACDUtilities {
    public static void linkProductConfigurationsInMACD(Set<Id> setClonedProductConfigIds){

        // Attribute list for update
        List<cscfga__Attribute__c> lstAttribsToUpdate = new List<cscfga__Attribute__c>();
        //Map<String, String> mapAttrGroupNamesValues = new Map<String, String>();
        
        // Attributes per cloned product configurations
        Map<Id, List<cscfga__Attribute__c>> clonedPCattMap = new Map<Id, List<cscfga__Attribute__c>>();

        Map<Id, cscfga__Product_Configuration__c> replacedConfigMap = new Map<Id, cscfga__Product_Configuration__c>();

        Map<Id, cscfga__Product_Configuration__c> clonedProductConfMap = new Map<Id, cscfga__Product_Configuration__c>(
                                                                                [SELECT Id, Name,
                                                                                        csordtelcoa__Replaced_Product_Configuration__c,
                                                                                        csordtelcoa__Replaced_Service__c
                                                                                 FROM cscfga__Product_Configuration__c
                                                                                 WHERE ID in :setClonedProductConfigIds]);
        system.debug('>>>>>>> LinkProductConfigurationsInMACD  - clonedProductConfMap: '+clonedProductConfMap);
        for(cscfga__Product_Configuration__c pc : clonedProductConfMap.values()){
            
            replacedConfigMap.put(pc.csordtelcoa__Replaced_Product_Configuration__c, pc);

        }

        
        List<cscfga__Attribute__c> lstAttribsFromClonedConfig  = [select Id, Name, cscfga__Attribute_Definition__r.Attribute_Type__c, cscfga__Product_Configuration__c, 
                                                            cscfga__Value__c, cscfga__Display_Value__c, cscfga__Attribute_Definition__r.Group_Name__c, cscfga__Attribute_Definition__r.Relationship_Type__c
                                                            from cscfga__Attribute__c 
                                                             where 
                                                             cscfga__Product_Configuration__c IN :setClonedProductConfigIds 
                                                             AND cscfga__Attribute_Definition__r.Attribute_Type__c != null AND cscfga__Attribute_Definition__r.Group_Name__c != null];
        
        system.debug('>>>>>>> LinkProductConfigurationsInMACD  - lstAttribsFromClonedConfig: '+lstAttribsFromClonedConfig);
        Set<Id> livePCserIds = new Set<Id>();
        Set<Id> liveSerIds = new Set<Id>();
        // Order attributes by product configurations  
        for(cscfga__Attribute__c att : lstAttribsFromClonedConfig){
            if(clonedPCattMap.containsKey(att.cscfga__Product_Configuration__c)){
                clonedPCattMap.get(att.cscfga__Product_Configuration__c).add(att);
            } else {
                clonedPCattMap.put(att.cscfga__Product_Configuration__c, new List<cscfga__Attribute__c> { att });
            }
            // Creating set of linked product configurations so we can get their live Services
            if(att.cscfga__Attribute_Definition__r.Attribute_Type__c == 'Product Configuration' &&
                att.cscfga__Value__c != null && 
                att.cscfga__Value__c != ''){
                if(att.cscfga__Attribute_Definition__r.Relationship_Type__c == 'ASBRPointerMultiple'){
                    String[] ids = att.cscfga__Value__c.split(',');
                    for(String pcId : ids){
                        livePCserIds.add(pcId);
                    }
                }else{
                    livePCserIds.add(att.cscfga__Value__c);
                }
                
            } else if(att.cscfga__Attribute_Definition__r.Attribute_Type__c == 'Service' &&
                att.cscfga__Value__c != null && 
                att.cscfga__Value__c != ''){
                if(att.cscfga__Attribute_Definition__r.Relationship_Type__c == 'ASBRPointerMultiple'){
                    String[] ids = att.cscfga__Value__c.split(',');
                    for(String pcId : ids){
                        liveSerIds.add(pcId);
                    }
                }else{
                    liveSerIds.add(att.cscfga__Value__c);
                }
            }
        }
        system.debug('>>>>>>> LinkProductConfigurationsInMACD  - livePCserIds: '+livePCserIds);
        system.debug('>>>>>>> LinkProductConfigurationsInMACD  - liveSerIds: '+liveSerIds);
        Map<Id, csord__Service__c> livePcServiceMap = new Map<Id, csord__Service__c>();

        Map<Id,csord__Service__c> liveServiceMap = new Map<Id,csord__Service__c> ([SELECT Id, Name, csordtelcoa__Product_Configuration__c
                                                    FROM csord__Service__c
                                                    WHERE csordtelcoa__Product_Configuration__c in :livePCserIds OR Id in :liveSerIds]);
        system.debug('>>>>>>> LinkProductConfigurationsInMACD  - liveServiceMap: '+liveServiceMap);
        for(csord__Service__c ser : liveServiceMap.values()){
            livePcServiceMap.put(ser.csordtelcoa__Product_Configuration__c, ser);
        }

        for(cscfga__Attribute__c att : lstAttribsFromClonedConfig){

            // Re-linking products
            if(att.cscfga__Attribute_Definition__r.Attribute_Type__c == 'Product Configuration' && 
                att.cscfga__Attribute_Definition__r.Relationship_Type__c != 'ASBRPointerMultiple' && 
                att.cscfga__Value__c != null && 
                att.cscfga__Value__c != ''){
 
                cscfga__Product_Configuration__c pc = replacedConfigMap.get(att.cscfga__Value__c);

                // there is linked product in MACD basket
                if(pc != null){
                    att.cscfga__Value__c = pc.Id;
                    att.cscfga__Display_Value__c = pc.Name;
                    lstAttribsToUpdate.add(att);

                    cscfga__Attribute__c checkBoxAtt = setCheckBoxAttributeInGroup(att, clonedPCattMap.get(att.cscfga__Product_Configuration__c));
                    if(checkBoxAtt != null){
                        lstAttribsToUpdate.add(checkBoxAtt);
                    }

                // change relationship to from PC to Service - because there isn't linked product in MACD basket
                } else {
                    if(!livePcServiceMap.isEmpty()){
                        lstAttribsToUpdate.addAll(setAttributeInGroup(att, clonedPCattMap.get(att.cscfga__Product_Configuration__c), livePcServiceMap.get(att.cscfga__Value__c), 'Service'));    
                    }
                    
                    att.cscfga__Value__c = null;
                    att.cscfga__Display_Value__c = '';
                    lstAttribsToUpdate.add(att);

                }
            
            } else if (att.cscfga__Attribute_Definition__r.Attribute_Type__c == 'Service' && 
                att.cscfga__Attribute_Definition__r.Relationship_Type__c != 'ASBRPointerMultiple' && 
                att.cscfga__Value__c != null && 
                att.cscfga__Value__c != ''){

                csord__Service__c ser = liveServiceMap.get(att.cscfga__Value__c);

                cscfga__Product_Configuration__c pc = replacedConfigMap.get(ser.csordtelcoa__Product_Configuration__c);

                if(pc != null){
                    lstAttribsToUpdate.addAll(setAttributeInGroup(att, clonedPCattMap.get(att.cscfga__Product_Configuration__c), pc, 'Product Configuration'));
                }

            } else if (att.cscfga__Attribute_Definition__r.Relationship_Type__c == 'ASBRPointerMultiple' &&
                att.cscfga__Value__c != null && 
                att.cscfga__Value__c != ''){

                if(att.cscfga__Attribute_Definition__r.Attribute_Type__c == 'Product Configuration'){

                    String[] asbrIDs = att.cscfga__Value__c.split(',');
                    List<String> serviceASBRS = new List<String>();
                    List<String> pcASBRS = new List<String>();
                    for (String asbr : asbrIDs){
                        cscfga__Product_Configuration__c pc = replacedConfigMap.get(asbr);
                
                        if(pc != null){
                            system.debug('>>>>>>> LinkProductConfigurationsInMACD  -PC.Id ASBRPointerMultiple: '+pc.Id);
                            pcASBRS.add(pc.Id);  
                        } else{
                            system.debug('>>>>>>> LinkProductConfigurationsInMACD  -PC as Service ASBRPointerMultiple: '+asbr);
                            serviceASBRS.add(asbr);
                        }
                        
                    }
                    if(!pcASBRS.isEmpty()){
                        if(pcASBRS.size() ==1){
                            att.cscfga__Value__c = pcASBRS.get(0);
                        }else{
                            att.cscfga__Value__c = String.join(pcASBRS,',');
                        }
                        
                    } else {
                        att.cscfga__Value__c ='';
                    }
                    system.debug('>>>>>>> LinkProductConfigurationsInMACD  -Product Configuration ASBRPointerMultiple: '+att);
                    lstAttribsToUpdate.add(att);

                    if(!serviceASBRS.isEmpty()){
                        List<String> serIds = new List<String>();
                        for(String pcID : serviceASBRS){
                            if(livePcServiceMap.get(pcID) != null){
                                serIds.add(livePcServiceMap.get(pcID).Id);    
                            }
                        }
                        if(!serIds.isEmpty()){
                            lstAttribsToUpdate.add(setAttributeValueInGroup(att, clonedPCattMap.get(att.cscfga__Product_Configuration__c), serIds, 'Service'));    
                        }
                        
                        system.debug('>>>>>>> LinkProductConfigurationsInMACD  -Product Configuration lstAttribsToUpdate: '+lstAttribsToUpdate);
                    }
                    

                } else if(att.cscfga__Attribute_Definition__r.Attribute_Type__c == 'Service'){

                    String[] asbrIDs = att.cscfga__Value__c.split(',');
                    List<String> serviceASBRS = new List<String>();
                    List<String> pcASBRS = new List<String>();
                    system.debug('>>>>>>> LinkProductConfigurationsInMACD  -Service ASBRPointerMultiple: '+att);
                    for (String asbr : asbrIDs){

                        csord__Service__c ser = liveServiceMap.get(asbr);
                        cscfga__Product_Configuration__c pc = replacedConfigMap.get(ser.csordtelcoa__Product_Configuration__c);
                
                        if(pc != null){
                            pcASBRS.add(pc.Id);  
                        } else{
                            serviceASBRS.add(asbr);
                        }
                        
                    }
                    if(!serviceASBRS.isEmpty()){
                        if(serviceASBRS.size() ==1){
                            att.cscfga__Value__c = serviceASBRS.get(0);
                        }else{
                            att.cscfga__Value__c = String.join(serviceASBRS,',');
                        }
                    } else {
                        att.cscfga__Value__c ='';
                    }
                    system.debug('>>>>>>> LinkProductConfigurationsInMACD  -Service changed ASBRPointerMultiple: '+att);
                    if(!pcASBRS.isEmpty()){
                        lstAttribsToUpdate.add(setAttributeValueInGroup(att, clonedPCattMap.get(att.cscfga__Product_Configuration__c), pcASBRS, 'Product Configuration'));
                    }
                    system.debug('>>>>>>> LinkProductConfigurationsInMACD  -Service lstAttribsToUpdate ASBRPointerMultiple lstAttribsToUpdate: '+lstAttribsToUpdate);
                }
            }
        }
        system.debug('>>>>>>> LinkProductConfigurationsInMACD  - lstAttribsToUpdate: '+lstAttribsToUpdate);
        if(!lstAttribsToUpdate.isEmpty()){ upsert lstAttribsToUpdate; }

    }
    //for @testvisible, modified by Anuradha
    @testvisible
    private static cscfga__Attribute__c setCheckBoxAttributeInGroup(cscfga__Attribute__c att, List<cscfga__Attribute__c> attList){
        for(cscfga__Attribute__c groupAtt : attList){
            if(groupAtt.cscfga__Attribute_Definition__r.Group_Name__c == att.cscfga__Attribute_Definition__r.Group_Name__c && groupAtt.Id != att.Id){
                    // Update Check Box attibute so PC will be shown when user will edit the product
                    if (groupAtt.cscfga__Attribute_Definition__r.Attribute_Type__c == 'Check Box'){

                        
                        if(att.cscfga__Attribute_Definition__r.Attribute_Type__c == 'Service'){
                            groupAtt.cscfga__Value__c = 'Yes';
                        } else {
                            groupAtt.cscfga__Value__c = 'No';
                        }
                            
                        return groupAtt;
                    }
            }
        }

        return null;
    }
    //for @testvisible, modified by Anuradha
    @testvisible
    private static List<cscfga__Attribute__c> setAttributeInGroup(cscfga__Attribute__c att, List<cscfga__Attribute__c> attList, SObject obj, String attType){
        List<cscfga__Attribute__c> attListToUpdate = new List<cscfga__Attribute__c>();
        for(cscfga__Attribute__c groupAtt : attList){
            if(groupAtt.cscfga__Attribute_Definition__r.Group_Name__c == att.cscfga__Attribute_Definition__r.Group_Name__c && groupAtt.Id != att.Id){
                
                if(groupAtt.cscfga__Attribute_Definition__r.Attribute_Type__c == attType){
                        groupAtt.cscfga__Value__c = (String)obj.get('Id');
                        groupAtt.cscfga__Display_Value__c = (String)obj.get('Name');
                        attListToUpdate.add(groupAtt);  
                } else if (groupAtt.cscfga__Attribute_Definition__r.Attribute_Type__c == 'Check Box'){
                    if(attType == 'Service'){
                        groupAtt.cscfga__Value__c = 'Yes';
                    } else {
                        groupAtt.cscfga__Value__c = 'No';
                    }
                    attListToUpdate.add(groupAtt);
                }
            }
        }
        return attListToUpdate;
    }
    //for @testvisible, modified by Anuradha
    @testvisible
    private static cscfga__Attribute__c setAttributeValueInGroup(cscfga__Attribute__c att, List<cscfga__Attribute__c> attList, List<String> valueList, String attType){

        for(cscfga__Attribute__c groupAtt : attList){
            if(groupAtt.cscfga__Attribute_Definition__r.Group_Name__c == att.cscfga__Attribute_Definition__r.Group_Name__c && groupAtt.Id != att.Id){
                
                if(groupAtt.cscfga__Attribute_Definition__r.Attribute_Type__c == attType){
                    if(groupAtt.cscfga__Value__c == null || groupAtt.cscfga__Value__c == ''){
                        if(valueList.size() == 1){
                            groupAtt.cscfga__Value__c = valueList.get(0);
                        }else{
                            groupAtt.cscfga__Value__c = String.join(valueList, ',');
                        }
                        
                    }  else {
                        
                        for (String value : valueList){
                            if(!groupAtt.cscfga__Value__c.contains(value)){
                                groupAtt.cscfga__Value__c += ','+value;
                            }

                        }
                    }                   
                    return groupAtt;
                }               
            }
        }
        return null;
    }


    public static void upsertASBRtoServiceLinks(Set<Id> serviceIds){

        List<CS_Port_ASBR_Service__c> linkToUpdate = new List<CS_Port_ASBR_Service__c>();
        List<CS_Port_ASBR_Service__c> linkToDelete = new List<CS_Port_ASBR_Service__c>();
        Map<Id,List<CS_Port_ASBR_Service__c>> asbrLinkMap = new Map<Id,List<CS_Port_ASBR_Service__c>>();
        Map<Id,List<CS_Port_ASBR_Service__c>> portLinkMap = new Map<Id,List<CS_Port_ASBR_Service__c>>();
        
        Map<Id,csord__Service__c> replacedSerMap = new Map<Id,csord__Service__c>();
        Map<Id,csord__Service__c> rpcSerMap = new Map<Id,csord__Service__c>();

        Map<String, Id> productDef = new Map<String, Id> { 'IPVPN' => Product_Definition_Id__c.getvalues('IPVPN_Port_Definition_Id').Product_Id__c,
                                                           'Trans' => Product_Definition_Id__c.getvalues('VPLS_Transparent_Definition_Id').Product_Id__c,
                                                           'VLAN' => Product_Definition_Id__c.getvalues('VPLS_VLAN_Port_Definition_Id').Product_Id__c,
                                                           'ASBR' =>Product_Definition_Id__c.getvalues('ASBR_Definition_Id').Product_Id__c };

        Map<Id, csord__Service__c> serviceMap = new Map<Id, csord__Service__c>([SELECT Id, Name, csordtelcoa__Product_Configuration__c, csordtelcoa__Replaced_Service__c,
                                                                                    csordtelcoa__Product_Configuration__r.ASBR_Product_Configuration__c,
                                                                                    csordtelcoa__Product_Configuration__r.ASBR_Service__c,
                                                                                    csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__c
                                                                            FROM csord__Service__c
                                                                            WHERE Id in :serviceIds and 
                                                                                  csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__c in :productDef.values()]);
        system.debug('>>>>>>> upsertASBRtoServiceLinks  - serviceMap: '+serviceMap);
        for(csord__Service__c ser : serviceMap.values()){
            replacedSerMap.put(ser.csordtelcoa__Replaced_Service__c, ser);
            rpcSerMap.put(ser.csordtelcoa__Product_Configuration__c, ser);
        }

        List<CS_Port_ASBR_Service__c> portASBRlinks = [SELECT Id, Name, ASBR__c, Port__c,
                                                        ASBR__r.csordtelcoa__Replaced_Service__c,
                                                        Port__r.csordtelcoa__Replaced_Service__c
                                                        FROM CS_Port_ASBR_Service__c
                                                        WHERE ASBR__c in :replacedSerMap.keySet() OR 
                                                              Port__c in :replacedSerMap.keySet()];

        system.debug('>>>>>>> upsertASBRtoServiceLinks  - portASBRlinks: '+portASBRlinks);
        for(CS_Port_ASBR_Service__c apl : portASBRlinks){
            
            if(asbrLinkMap.containsKey(apl.ASBR__c)){
                asbrLinkMap.get(apl.ASBR__c).add(apl);
            } else {
                asbrLinkMap.put(apl.ASBR__c, new List<CS_Port_ASBR_Service__c>{apl});
            }
        }

        for(CS_Port_ASBR_Service__c apl : portASBRlinks){
            
            if(portLinkMap.containsKey(apl.Port__c)){
                portLinkMap.get(apl.Port__c).add(apl);
            } else {
                portLinkMap.put(apl.Port__c, new List<CS_Port_ASBR_Service__c>{apl});
            }
        }

        for(csord__Service__c ser : serviceMap.values()){

            if(ser.csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__c == productDef.get('ASBR')){
                if(ser.csordtelcoa__Replaced_Service__c != null){
                    List<CS_Port_ASBR_Service__c> links = asbrLinkMap.get(ser.csordtelcoa__Replaced_Service__c);
                    if (links != null){
                        for(CS_Port_ASBR_Service__c link : links){
                            link.ASBR__c = ser.Id;
                            linkToUpdate.add(link);
                        }
                    }
                }
                
            } else {
                List<Id> pcASBRList = new List<Id>();
                List<Id> serASBRList = new List<Id>();
                if(ser.csordtelcoa__Product_Configuration__r.ASBR_Product_Configuration__c != null){
                        String[] pcASBR = ser.csordtelcoa__Product_Configuration__r.ASBR_Product_Configuration__c.split(',');
                        for(String s : pcASBR){
                            pcASBRList.add(s);
                        }
                    }
                    if(ser.csordtelcoa__Product_Configuration__r.ASBR_Service__c != null){
                        String[] serASBR = ser.csordtelcoa__Product_Configuration__r.ASBR_Service__c.split(',');
                        for(String s : serASBR){
                            serASBRList.add(s);
                        }
                }
                if(ser.csordtelcoa__Replaced_Service__c == null){
                    for(Id asbrId : pcASBRList){
                        CS_Port_ASBR_Service__c apl = new CS_Port_ASBR_Service__c();
                        apl.ASBR__c = rpcSerMap!=null && rpcSerMap.containsKey(asbrId)?rpcSerMap.get(asbrId).Id:null;
                        apl.Port__c = ser.Id;

                        linkToUpdate.add(apl);

                    }
                    for(Id serId : serASBRList){
                        CS_Port_ASBR_Service__c apl = new CS_Port_ASBR_Service__c();
                        apl.ASBR__c = serId;
                        apl.Port__c = ser.Id;

                        linkToUpdate.add(apl);

                    }

                } else {
                    List<CS_Port_ASBR_Service__c> links = portLinkMap.get(ser.csordtelcoa__Replaced_Service__c);
                    if (links != null){
                        for(CS_Port_ASBR_Service__c link : links){
                            linkToDelete.add(link);
                        }
                    }
                    for(Id asbrId : pcASBRList){

                        CS_Port_ASBR_Service__c apl = new CS_Port_ASBR_Service__c();
                        apl.ASBR__c = rpcSerMap.get(asbrId).Id;
                        apl.Port__c = ser.Id;

                        linkToUpdate.add(apl);
                    }
                    for(Id serId : serASBRList){
                        CS_Port_ASBR_Service__c apl = new CS_Port_ASBR_Service__c();
                        apl.ASBR__c = serId;
                        apl.Port__c = ser.Id;

                        linkToUpdate.add(apl);

                    }               
                }       
            }
        }
        system.debug('>>>>>>> upsertASBRtoServiceLinks  - linkToUpdate: '+linkToUpdate);
        if(!linkToUpdate.isEmpty()){ upsert linkToUpdate;}
        system.debug('>>>>>>> upsertASBRtoServiceLinks  - linkToDelete: '+linkToDelete);
        if(!linkToDelete.isEmpty()){ delete linkToDelete;}

    }


    public static void updateServiceHierarchy (Set<Id> serviceIds){
        Map<String, MACD_Service_Links__c> macdFields = MACD_Service_Links__c.getall();
        List<csord__Service__c> serviceToUpdate = new List<csord__Service__c>();
        Set<Id> replacedSerIds = new Set<Id>();
        Set<String> fileds = new Set<String>();
        for(MACD_Service_Links__c item : macdFields.values()){
            fileds.add(item.Service_Field__c);
        }
        Map<Id,csord__Service__c> serviceMap = new Map<Id,csord__Service__c>([SELECT Id, Name, 
                                                                                            csordtelcoa__Product_Configuration__r.Product_Definition_Name__c, 
                                                                                            csordtelcoa__Replaced_Service__c
                                                                                    FROM csord__Service__c
                                                                                    WHERE Id in :serviceIds AND csordtelcoa__Replaced_Service__c <> null]);
        system.debug('>>>>>>> updateServiceHierarchy  - serviceMap: '+serviceMap);
        for(csord__Service__c ser : serviceMap.values()){
            replacedSerIds.add(ser.csordtelcoa__Replaced_Service__c);
        }
        system.debug('>>>>>>> updateServiceHierarchy  - replacedSerIds: '+replacedSerIds);
        Map<Id,csord__Service__c> serviceLinkedMap = new Map<Id,csord__Service__c>((List<csord__Service__c>)Database.query(serviceQueryBuilder(replacedSerIds, fileds)));
        system.debug('>>>>>>> updateServiceHierarchy  - serviceLinkedMap: '+serviceLinkedMap);
        for(csord__Service__c ser : serviceMap.values()){
            MACD_Service_Links__c macdField = macdFields.get(ser.csordtelcoa__Product_Configuration__r.Product_Definition_Name__c);
            system.debug('>>>>>>> updateServiceHierarchy  - macdField: '+macdField);
            if(macdField != null){
                List<csord__Service__c> likedSerList = getLinkedServices(ser.csordtelcoa__Replaced_Service__c, macdField.Service_Field__c, serviceLinkedMap.values());
                system.debug('>>>>>>> updateServiceHierarchy  - likedSerList: '+likedSerList);
                for (csord__Service__c item : likedSerList){

                    item.put(macdField.Service_Field__c, ser.Id);
                    system.debug('>>>>>>> updateServiceHierarchy  - likedSerList item: '+item);
                    serviceToUpdate.add(item);
                }
            }
        }
        system.debug('>>>>>>> updateServiceHierarchy  - serviceToUpdate: '+serviceToUpdate);
        if(!serviceToUpdate.isEmpty()){ update serviceToUpdate;}

    }
    //for @testvisible, modified by Anuradha
    @testvisible
    private static String serviceQueryBuilder(Set<Id> replacedSerIds, Set<String> macdSerFieldListfields) {

        String query = 'SELECT Id, Name, csordtelcoa__Replaced_Service__c';

        for(String item :macdSerFieldListfields){
            query += ',' + item;
        }

        query += ' FROM csord__Service__c WHERE Inventory_Status__c <> \'Decommissioned\' ';

        Boolean isFirst = true;
        Boolean andPart = false;
        
        for(String item :macdSerFieldListfields){
            if(isFirst){
                query += 'AND(' + item + ' in :replacedSerIds';
                isFirst = false;
                andPart = true;
            }else{
                query += ' OR '+item + ' in :replacedSerIds';
            }
        }
        
        if(andPart){
            query +=')';
        }
        
        system.debug('>>>>>>> updateServiceHierarchy  - query: '+query);
        return query;
    }
    //for @testvisible, modified by Anuradha
    @testvisible
     private static List<csord__Service__c> getLinkedServices(Id value, String field, List<csord__Service__c> services) {

        List<csord__Service__c> returnServiceList = new List<csord__Service__c>();

        for(csord__Service__c ser : services){
        system.debug('>>>>>>> updateServiceHierarchy  - getLinkedServices ser-value: '+value+' - '+ser);
            if(ser.get(field) == value){
                returnServiceList.add(ser);
                
            }
        }
        system.debug('>>>>>>> updateServiceHierarchy  - getLinkedServices returnServiceList: '+returnServiceList);
        return returnServiceList;
    }


}