@isTest(SeeAllData = false)
private class CS_IPVPNPrimaryPortLookupTest {

    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    private static Id[] excludeIds  = new List<Id>();
    private static Integer pageOffset, pageLimit;
    
    private static List<csord__Service__c> serviceList;
    private static List<csord__Service__c> masterServiceList;
    private static List<csord__Order_Request__c> orderRequestList;
    private static List<csord__Subscription__c> subscriptionList;
    private static List<Account> accountList;
    private static List<CS_Country__c> countryList;
    private static List<CS_City__c> cityList;
    private static List<CS_POP__c> popList;
    
    private static void initTestData(){
        
        countryList = new List<CS_Country__c>{
            new CS_Country__c(Name = 'Croatia'),
            new CS_Country__c(Name = 'USA'),
            new CS_Country__c(Name = 'India'),
            new CS_Country__c(Name = 'Hong Kong'),
            new CS_Country__c(Name = 'Australia')
        };
        
        insert countryList;
        System.debug('***** Country List ' + countryList);
          System.assertEquals('Croatia',countryList[0].Name ); 
        
        cityList = new List<CS_City__c>{
            new CS_City__c(Name = 'Zagreb', CS_Country__c = countryList[0].Id),
            new CS_City__c(Name = 'New York', CS_Country__c = countryList[1].Id),
            new CS_City__c(Name = 'Bangalore', CS_Country__c = countryList[2].Id),
            new CS_City__c(Name = 'Hong Kong', CS_Country__c = countryList[3].Id),
            new CS_City__c(Name = 'Sydney', CS_Country__c = countryList[4].Id)
        };
        
        insert cityList;
        System.debug('***** City List ' + cityList);
        System.assertEquals('Zagreb',cityList[0].Name ); 
        
        popList = new List<CS_POP__c>{
            new CS_POP__c(Name = 'POP 1', CS_Country__c = countryList[0].Id, Active__c = Boolean.valueOf('true'), CS_City__c = cityList[0].Id, Connectivity__c = 'Onnet',  IPVPN_Critical_Data__c = Boolean.valueOf('true')),
            new CS_POP__c(Name = 'POP 2', CS_Country__c = countryList[0].Id, Active__c = Boolean.valueOf('true'), CS_City__c = cityList[0].Id, Connectivity__c = 'Offnet',  IPVPN_Critical_Data__c = Boolean.valueOf('true')),
            new CS_POP__c(Name = 'POP 3', CS_Country__c = countryList[0].Id, Active__c = Boolean.valueOf('false'), CS_City__c = cityList[0].Id, Connectivity__c = 'Onnet',  IPVPN_Critical_Data__c = Boolean.valueOf('true'))
        };
        
        insert popList;
        System.debug('***** Pop List ' + popList);
        System.assertEquals('POP 1',popList[0].Name ); 
        
        orderRequestList = new List<csord__Order_Request__c>{
            new csord__Order_Request__c(Name = 'Order Request 1', csord__Module_Name__c = 'Module name', csord__Module_Version__c = 'Module version')
        };
        
        insert orderRequestList;
          System.assertEquals('Order Request 1',orderRequestList[0].Name );  
        
        subscriptionList = new List<csord__Subscription__c>{
            new csord__Subscription__c(Name = 'Subscription 1', csord__Identification__c = 'Identification string',csord__Order_Request__c = orderRequestList[0].Id)
        };
        insert subscriptionList; 
        System.assertEquals('Subscription 1',subscriptionList[0].Name );     
        
        accountList = new List<Account>{
            new Account(Name = 'Account 1')
        };
        
        insert accountList;
         System.assertEquals('Account 1',accountList[0].Name ); 
        
        masterServiceList = new List<csord__Service__c>{
            new csord__Service__c(Name = 'Master Service 1', csord__Identification__c = 'Master Service 1', csord__Subscription__c = subscriptionList[0].Id, csord__Order_Request__c = orderRequestList[0].Id, AccountId__c = accountList[0].Id),
            new csord__Service__c(Name = 'Master Service 2', csord__Identification__c = 'Master Service 2', csord__Subscription__c = subscriptionList[0].Id, csord__Order_Request__c = orderRequestList[0].Id, AccountId__c = accountList[0].Id )
        };
        
        insert masterServiceList;
        System.debug('**** Parent Service List: ' + masterServiceList);
         System.assertEquals('Master Service 1',masterServiceList[0].Name ); 
        
        serviceList = new List<csord__Service__c>{
            new csord__Service__c(Name = 'IPVPN', csord__Identification__c = 'Service 1', Primary_Or_Backup__c = 'Primary', CS_Port_POP__c = popList[0].Id ,csord__Subscription__c = subscriptionList[0].Id, csord__Order_Request__c = orderRequestList[0].Id, AccountId__c = accountList[0].Id, Master_Service__c = masterServiceList[0].Id, Inventory_Status__c = 'Commissioned'),
            new csord__Service__c(Name = 'IPVPN', csord__Identification__c = 'Service 2', Primary_Or_Backup__c = 'Primary', CS_Port_POP__c = popList[1].Id ,csord__Subscription__c = subscriptionList[0].Id, csord__Order_Request__c = orderRequestList[0].Id, AccountId__c = accountList[0].Id, Master_Service__c = masterServiceList[0].Id, Inventory_Status__c = 'Commissioned'),
            new csord__Service__c(Name = 'IPVPN', csord__Identification__c = 'Service 3', Primary_Or_Backup__c = 'Primary', CS_Port_POP__c = popList[0].Id ,csord__Subscription__c = subscriptionList[0].Id, csord__Order_Request__c = orderRequestList[0].Id, AccountId__c = accountList[0].Id, Master_Service__c = masterServiceList[0].Id, Inventory_Status__c = 'Commissioned'),
            new csord__Service__c(Name = 'IPVPN', csord__Identification__c = 'Service 4', Primary_Or_Backup__c = 'Primary', CS_Port_POP__c = popList[0].Id ,csord__Subscription__c = subscriptionList[0].Id, csord__Order_Request__c = orderRequestList[0].Id, AccountId__c = accountList[0].Id, Master_Service__c = masterServiceList[0].Id, Inventory_Status__c = 'Decommissioned')
        };
        
        insert serviceList;
        System.debug('**** Service List: ' + serviceList);
        System.assertEquals('IPVPN',serviceList[0].Name ); 
    }

    private static testMethod void doLookupSearchTest() {
        
        Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
                             
            initTestData();  
            
            searchFields.put('Account Id', accountList[0].Id);
            searchFields.put('Backup Port Options', 'Active/ Standby Different POP');
            searchFields.put('Product Definition Name', 'IPVPN Port');
            searchFields.put('POP', popList[0].Id);
            searchFields.put('Onnet or Offnet', 'Onnet');
            searchFields.put('Replaced Master Service', masterServiceList[0].Id);
                        
            CS_IPVPNPrimaryPortLookup ipvpnPrimaryPortLookup = new CS_IPVPNPrimaryPortLookup();
            String reqAtts = ipvpnPrimaryPortLookup.getRequiredAttributes();
            Object[] data = ipvpnPrimaryPortLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
        
            System.debug('*******Data: ' + data);
            System.assert(data.size() >= 0, '');
            
            data.clear();
            searchFields.put('Backup Port Options', 'Active/ Standby Same POP');
            
            data = ipvpnPrimaryPortLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
        
            System.debug('*******Data: ' + data);
            System.assert(data.size() >= 0, '');
            
            data.clear();
            searchFields.put('Onnet or Offnet', 'Offnet');
            
            data = ipvpnPrimaryPortLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
        
            System.debug('*******Data: ' + data);
            System.assert(data.size() >= 0, '');            
            
            
        } catch(Exception e){
            ee = e;
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }  
    }

}