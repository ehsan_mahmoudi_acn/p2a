Public with sharing class PricingFinancialSummaryManishcls
{
   /* public case caseObj;
    private list<case> casequery;
    public Boolean isVisible;
    public Decimal revenueGrandTotal {get;set;}
    public Decimal opexGrandTotal {get;set;}
    public Decimal cogsrandTotal {get;set;}
    public Decimal CapexrandTotal {get;set;}
    public Decimal grossmarginGrandTotal {get;set;}
    public Decimal grossmarginPerGrandTotal {get;set;}
    public Decimal onneroffnetgrandTotal {get;set;}
    
    public List<RevenueWrapper> revLst {get;set;}
    Public boolean getisVisible(){
        ID pID = UserInfo.getProfileID();
        Boolean checkVisibility = false;
        list<profile> pList = [Select id from profile where name in ('TI Commercial','System Administrator')];
        for (profile p : pList) {
            if (p.id == pID)
                checkVisibility = true;
        }
        return checkVisibility;
    }
    
    public PricingFinancialSummaryManishcls(ApexPages.StandardController controller) 
    {
        revLst  = new List<RevenueWrapper>();
        this.caseObj = (case)controller.getrecord();
        casequery = [select id, Product_Basket__c,Product_Basket__r.Total_Margin__c,Product_Basket__r.Onnet_vs_Offnet__c,Product_Basket__r.Gross_Margin__c, Product_Basket__r.Max_Contract_Term__c, Product_Basket__r.Capex__c from case where id=: caseobj.id];
        revenueGrandTotal  = 0;
        opexGrandTotal = 0;
        cogsrandTotal = 0;
        CapexrandTotal = 0;
        grossmarginGrandTotal = 0;
        grossmarginPerGrandTotal = 0;
        onneroffnetgrandTotal = 0;
        initlize();
        
    } 
    
    public class RevenueWrapper 
    {
        public Decimal revenue {get;set;}
        public Decimal opex {get;set;}
        public Decimal cogs {get;set;}
        public Decimal capex {get;set;}
        public Decimal grossmargin {get;set;}
        public Decimal grossmarginPer {get;set;}
        public Decimal onnetofnet {get;set;}
        public String years {get;set;}
    }
    
    public void initlize()
    {   
        decimal Cterm =1, pcrevenue=0.0, pcopex=0.0, pccogs=0.0, grossmarginRound, grossmarginPreRound;
        decimal maxContractTerm = 1;
        //updated by Anuradha for the defect 13328 and the description is capex values coming as default 12/19/2016             
         if(casequery[0].Product_Basket__r.capex__c == null){       
         casequery[0].Product_Basket__r.capex__c = 0.00 ;       
             }      
         //defect end    
        List<Pricing_Approval_Request_Data__c> PCList= [Select Id,Approved_NRC__c,Approved_RC__c, Offer_NRC__c, Offer_RC__c, Cost_RC__c,Cost_NRC__c, Contract_Term__c,Is_offnet__c from Pricing_Approval_Request_Data__c where Product_Basket__c=: casequery[0].Product_Basket__c ];
        
        for(Pricing_Approval_Request_Data__c iterator : PCList){
            if(iterator.Contract_Term__c > maxContractTerm) maxContractTerm = iterator.Contract_Term__c;
              //updated by Anuradha for the defect 13328 and the description is capex values coming as default 12/19/2016
            if(casequery[0].Product_Basket__r.capex__c != null)
            {
                casequery[0].Product_Basket__r.capex__c += iterator.Cost_NRC__c;
            }
            //defect end             
            }
        if(maxContractTerm>0 && maxContractTerm<=12 ){
            Cterm=1;
        }
        if(maxContractTerm>0 && maxContractTerm>12){
            Cterm = (maxContractTerm)/12;
        }
              
        system.debug('***********'+Cterm);
        integer i=1;
        for(i=1; i<=Cterm; i++){
            RevenueWrapper  s = new RevenueWrapper ();
            if(Cterm==1){
                for(Pricing_Approval_Request_Data__c pc: PCList){
                    
                    Decimal offerNRC = pc.Approved_NRC__c == null || pc.Approved_NRC__c == 0 ? pc.Offer_NRC__C: pc.Approved_NRC__c;
                    Decimal offerRC = pc.Approved_RC__c == null || pc.Approved_RC__c == 0 ? pc.Offer_RC__C : pc.Approved_RC__c;
                    pcrevenue=pcrevenue + (offerRC*pc.Contract_Term__c) + offerNRC;
                    if(pc.Is_offnet__c){
                        pccogs=pccogs + (pc.Cost_RC__c*pc.Contract_Term__c);
                        pccogs=pccogs.setscale(2);
                    }
                    if(!pc.Is_offnet__c){
                        pcopex=pcopex + (pc.Cost_RC__c*pc.Contract_Term__c) + pc.Cost_NRC__c;
                         pcopex = pcopex.setscale(2);
                    }
                }
                
                if(casequery[0].Product_Basket__r.capex__c!=null){
                        s.capex = (casequery[0].Product_Basket__r.capex__c).setscale(2);
                        CapexrandTotal= s.capex;
                        }else{
                        s.capex = 0.0;
                        CapexrandTotal= 0.0;}
                s.revenue = pcrevenue.setscale(2);
                s.opex= pcopex.setscale(2);
                s.cogs= pccogs.setscale(2);
                grossmarginRound = pcrevenue-(pcopex + pccogs + CapexrandTotal);
                s.grossmargin= grossmarginRound.setScale(2);
                if(pcrevenue!=0.0 && pcrevenue !=null){
                grossmarginPreRound = ((100* (pcrevenue - (pcopex + pccogs + CapexrandTotal)))/pcrevenue).setscale(2);
                }else{
                grossmarginPreRound=0.0;
                }
                s.grossmarginPer= grossmarginPreRound.setScale(2);
                
            }
            if(Cterm>1 && i==1){
                for(Pricing_Approval_Request_Data__c pc0: PCList){
                    Decimal offerNRC = pc0.Approved_NRC__c == null || pc0.Approved_NRC__c == 0 ? pc0.Offer_NRC__C : pc0.Approved_NRC__c;
                    Decimal offerRC = pc0.Approved_RC__c == null || pc0.Approved_RC__c == 0 ? pc0.Offer_RC__C : pc0.Approved_RC__c;
                    pcrevenue=pcrevenue + (offerRC*12) + offerNRC;
                    if(pc0.Is_offnet__c){
                        pccogs=pccogs + (pc0.Cost_RC__c*12) ;
                    }
                    if(!pc0.Is_offnet__c){
                        pcopex=pcopex + (pc0.Cost_RC__c*12) + pc0.Cost_NRC__c;
                    }
                }
                if(casequery[0].Product_Basket__r.capex__c!=null){
                        s.capex = casequery[0].Product_Basket__r.capex__c.setscale(2);
                        CapexrandTotal= s.capex.setscale(2);
                        }else{
                        s.capex = 0.0;
                        CapexrandTotal= 0.0;}
                s.revenue = pcrevenue.setscale(2);
                s.opex= pcopex.setscale(2);
                s.cogs= pccogs.setscale(2);
                grossmarginRound = pcrevenue - (pcopex + pccogs + CapexrandTotal);
                s.grossmargin= grossmarginRound.setScale(2);
                if(pcrevenue!=0.0 && pcrevenue !=null){
                grossmarginPreRound = ((100* (pcrevenue - (pcopex + pccogs + CapexrandTotal)))/pcrevenue).setscale(2);
                }else{
                grossmarginPreRound=0.0;
                }
                s.grossmarginPer= grossmarginPreRound.setScale(2);
                pcrevenue=0.00;
                pcopex=0.00;
                pcopex=0.00;
            }       
            if(Cterm>1 && i>1){
                for(Pricing_Approval_Request_Data__c pc1: PCList){
                    Decimal offerNRC = pc1.Approved_NRC__c == null || pc1.Approved_NRC__c == 0 ? pc1.Offer_NRC__C : pc1.Approved_NRC__c;
                    Decimal offerRC = pc1.Approved_RC__c == null || pc1.Approved_RC__c == 0 ? pc1.Offer_RC__C : pc1.Approved_RC__c;
                    
                    if((pc1.Contract_Term__c/12)>=i){
                        pcrevenue=pcrevenue + (offerRC*12);
                        if(pc1.Is_offnet__c){
                        pccogs=pccogs + (pc1.Cost_RC__c*12);
                        }
                        if(!pc1.Is_offnet__c){
                            pcopex=pcopex + (pc1.Cost_RC__c*12);
                        }
                    }else if ((pc1.Contract_Term__c/12)<i && math.mod(pc1.Contract_Term__c.intValue(),12)>0){
                        pcrevenue=pcrevenue + (offerRC*(math.mod(pc1.Contract_Term__c.intValue(),12))) ;
                        if(pc1.Is_offnet__c){
                        pccogs=pccogs + (pc1.Cost_RC__c*(math.mod(pc1.Contract_Term__c.intValue(),12)));
                    }
                    if(!pc1.Is_offnet__c){
                        pcopex=pcopex +  (pc1.Cost_RC__c*(math.mod(pc1.Contract_Term__c.intValue(),12)));
                    }
                    }else{
                        pcrevenue=pcrevenue +0.0;
                        pccogs=pccogs + 0.0;
                        pcopex=pcopex + 0.0;
                    }
                }
                s.capex = 0.0;
                s.revenue = pcrevenue.setscale(2);
                s.opex= pcopex.setscale(2);
                s.cogs= pccogs.setscale(2);
                grossmarginRound = (pcrevenue - (pcopex + pccogs)).setscale(2);
                s.grossmargin= grossmarginRound.setScale(2);
                if(pcrevenue!=0.0 && pcrevenue !=null){
                    grossmarginPreRound = ((100* (pcrevenue - (pcopex + pccogs)))/pcrevenue).setscale(2);
                }else{
                    grossmarginPreRound=0.0;
                }
                s.grossmarginPer= grossmarginPreRound.setScale(2);
                pcrevenue=0.00;
                pcopex=0.00;
                pcopex=0.00;
            }
            
            s.onnetofnet = 100;
            s.onnetofnet = (s.onnetofnet).setscale(2);
            s.years = 'Year '+ i; 
            revenueGrandTotal+= s.revenue;
            opexGrandTotal+= s.opex;
            cogsrandTotal+= s.cogs;
            //if(casequery[0].Product_Basket__r.Gross_Margin__c>=0){
                        //grossmarginGrandTotal= casequery[0].Product_Basket__r.Gross_Margin__c;
           // }
            grossmarginGrandTotal += s.grossmargin;
            grossmarginPerGrandTotal += s.grossmarginPer;
            //if(casequery[0].Product_Basket__r.Total_Margin__c>=0){
                //grossmarginPerGrandTotal=casequery[0].Product_Basket__r.Total_Margin__c;
            //}
            if(casequery[0].Product_Basket__r.Onnet_vs_Offnet__c>=0){
                onneroffnetgrandTotal=(casequery[0].Product_Basket__r.Onnet_vs_Offnet__c).setscale(2);
            }
            revLst.add(s);
        }
        grossmarginPerGrandTotal = grossmarginPerGrandTotal / (i-1);
        grossmarginPerGrandTotal = grossmarginPerGrandTotal.setScale(2);
    }*/
}