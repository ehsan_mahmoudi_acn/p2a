public class ErrorhandlingEmail{
    public static void emailSending(String billingRegion, Id id){
        buildAndSendEmail(billingRegion, id, 'Activation');
    }
    
    public static void emailSendingP2OError(String billingRegion, Id id){
        buildAndSendEmail(billingRegion, id, 'P2O');
    }
    
    //Added as part of 6901 for email not hitting TIBCO for downgrade/upgrade provide and cease order.
    public static void emailOnOrderType(String billingRegion, Id id){   
        buildAndSendEmail(billingRegion, id, 'OrderType');
    } 

    /*
    *   Generic method to handle the common email build/send functionality across the error emails
    */
    //for @Testvisible modified by Anuradha on 2/20/2017
    @Testvisible
    private static void buildAndSendEmail(String billingRegion, Id id, String type){
        System.debug('Inside Email generation class');

        // order_line_item__c objoli = [select id,parentorder__c from order_line_item__c where id =: id];
        csord__Service__c objsli = [select id,csord__Order__c from csord__Service__c where id =: id];

        // Load all queues corresponding to Bill profile Object
        List<QueueSobject> que = QueueUtil.getQueuesListForObject(BillProfile__c.SObjectType);
            
        Id queid;
        String billTeam;  

        //Get Billing Team Queue id corresponding to sales user region.
        for (QueueSObject q : que){
            if((q.Queue.Name).contains(billingRegion)){
                queid = q.QueueId;
                billTeam = q.Queue.Name;
            }
        }

        //Getting the all Users id's  corresponding to queue
        //List<GroupMember> list_GM = [SELECT GroupId, Id, SystemModstamp, UserOrGroupId FROM GroupMember where GroupId = :queid];
            
        List<Id> lst_Ids = new List<ID>();
        for(GroupMember gmObj : [SELECT GroupId, Id, SystemModstamp, UserOrGroupId FROM GroupMember where GroupId = :queid]){
            lst_Ids.add(gmObj.UserOrGroupId);
        }

        //Query the Billing Team Users 
        //List<User> lst_UserObj = [Select Id, Name, Region__c, Email from User where id in :lst_Ids];
            
        //Here concanating all email addresses of Users belong to Billing Queue
        String emailAddresses = '';
        String userRegion = '';
        for(User usrObj : [Select Id, Name, Region__c, Email from User where id in :lst_Ids]){
            if(emailAddresses == ''){
                emailAddresses = usrObj.Email;
                userRegion = usrObj.Region__c;
            }
            else{
                emailAddresses += ':'+usrObj.Email; 
                userRegion = usrObj.Region__c;
            }
        }

        if(emailAddresses  != ''){
            // csord__Order__c ordr = [Select id,name from csord__Order__c where csord__Order_Request__c  =: objsli.csord__Order_Request__c];

            GenericEmailSending  genericObj = new GenericEmailSending();
            genericObj.mail(emailAddresses, getEmailSubject(type), getEmailBody(type, billTeam, objsli.csord__Order__c, objsli)); 
        }
        else{
            System.debug('ERROR:  No email recipients');
        }
    } 

    /*
    *   Return the email body depending on the email type
    */
    //for @Testvisible modified by Anuradha on 2/20/2017
    @Testvisible
    private static String getEmailBody(String type, String billTeam, String orderId, csord__Service__c ServiceItem){
        //Getting sales force URL of Instance
        String instanceURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' ;
        String body = '';

        if('Activation'.equals(type)){
            body = 'Dear ' + billTeam +' Team' + ','+'\n'+'\n'+'For order ' + instanceURL + orderId;
            body += ' ,the following line item has failed to activate in the SubexROC billing system.'+'\n';
            body += '\n' + instanceURL + ServiceItem.Id + '\n';
            body += 'Your PSM team will be looking to correct the error and automatically flow down the change to ROC. ';
            body += 'The PSM team will contact you if they are unable to correct the error and if you need to enter the order/change manually.';
        }
        else if('P2O'.equals(type)){
            body = 'Dear ' + billTeam +' Team' + ','+'\n';
            body += '\n'+'Order ';
            body += instanceURL + orderId; 
            body += ' has failed in the middle layer P2O TIBCO and has not reached the SubexROC billing system.The reason for failure could be one of the following:'+'\n\n';
            body += '1. P2O TIBCO is down'+'\n';
            body += '2. The XML header going downstream from SFDC to P2O TIBCO is not appropriate'+'\n';
            body += '\n'+'Your PSM team will be looking to correct the error and automatically flow down the change to ROC.';
            body += 'The PSM team will contact you if they are unable to correct the error and if you need to enter the order/change manually.';
        }
        else if('OrderType'.equals(type)){
            body = 'Dear ' + billTeam +' Team' + ',\n\n';
            body = 'Bundle ' + ServiceItem.Name + ' in the order ';
            body = instanceURL + orderId;  
            body = ' was not sent to billing application as one or more line items of the bundle have Upgrade/Downgrade Provide and Cease Order type\n';
        }

        return body;
    }

    /*
    *   Return the email subject depending on the email type
    */
    //for @Testvisible modified by Anuradha on 2/20/2017
    @Testvisible
    private static String getEmailSubject(String type){
        if('Activation'.equals(type)){
            return 'Order has failed to activate in the SubexROC billing system';
        }
        else if('P2O'.equals(type)){
            return 'Order has failed in the P2O TIBCO layer';
        }
        else if('OrderType'.equals(type)){
            return 'Order has failed for Upgrade/Downgrade provide and cease order type';
        }
        return '';
    }
}