/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=false)
private class CustomerCreditExtensionTest {
/*private static Account a;
private static Order__c ordr;
private static CostCentre__c c;
static testMethod void CustomerCreditMethod() {
        // TO DO: implement unit test
        BillProfile__c b=getBillProfile();
        Account acc=getAccount();
        Product2 p=getProduct();
        Service__c serv=getServiceObj();
        serv.AccountId__c=acc.id;
        serv.Product__c=p.Id;
        insert serv;
        
        Opportunity opp=getOpportunity();
        opp.AccountId = a.Id;
        insert opp;
        ordr=getOrder();
        ordr.Opportunity__c = opp.Id;
        insert ordr;
        Order_Line_Item__c orderLineItem=getOrderLineItem();
        orderLineItem.Service__c=serv.Id;
        orderLineItem.Product__c=p.Id;
        orderLineItem.ParentOrder__c = ordr.Id;
        //insert orderLineItem;
        c=getCostCentre();
        c.BillProfile__c = b.Id;
        c.Cost_Centre_Status__c='Active';
        insert c;
        Miscellaneous_Charge__c misc=getMiscObj();
        insert misc;
        Resource__c res=getResourceObj();
        res.AccountId__c=acc.id;
        res.Product__c=p.Id;
        res.CostCentre_Id__c=c.Id;
        res.Bill_Profile_Id__c=b.Id;
        insert res;
        
       try{
        test.startTest();
         PageReference pageTest2 = Page.TestCustomerCreditPage;
        pageTest2.getParameters().put('Id',acc.id);
        pageTest2.getParameters().put('Name',acc.Name);
        Test.setCurrentPage(pageTest2);
        CustomerCreditExtension customerExt=new CustomerCreditExtension();
        customerExt.accId=acc.id;
        customerExt.cancel();
        customerExt.customerCreditvo=new CustomerCreditExtension.CustomerCreditVO();
        customerExt.customerCreditvo.billProfile=b.Id;
        customerExt.getAccountDetails() ;
        CustomerCreditExtension.CustomerCreditVO creditVO = new CustomerCreditExtension.CustomerCreditVO();
        customerExt.customerCreditvo=new CustomerCreditExtension.CustomerCreditVO();
        customerExt.customerCreditvo.billProfile=b.Id;
        customerExt.customerCreditvo.oneoffservice=serv.Id;
        customerExt.getServiceOneOffDetails();
        CustomerCreditExtension.getPicklistValues('Miscellaneous_Charge__c','Approve_By__c');
        customerExt.insertToMiscChargeObj();
        
        }catch(Exception ex){
    
        system.debug('exception'+ex);
        }    
        try{
        CustomerCreditExtension customerExt=new CustomerCreditExtension();
        CustomerCreditExtension.CustomerCreditExtensionException creditExVO = new CustomerCreditExtension.CustomerCreditExtensionException();
        //creditExVO.CustomerCreditExtensionException('test exception', true);
        customerExt.customerCreditvo=new CustomerCreditExtension.CustomerCreditVO();
        customerExt.customerCreditvo.billProfile=b.Id;
        customerExt.customerCreditvo.oneoffservice='--None--';
        customerExt.getServiceOneOffDetails();  
        }catch(Exception ex){
    
        system.debug('exception'+ex);
        }
        try{
            
        CustomerCreditExtension customerExt=new CustomerCreditExtension();
        customerExt.accId=acc.id;
        customerExt.customerCreditvo=new CustomerCreditExtension.CustomerCreditVO();
        customerExt.customerCreditvo.billProfile=b.Id;
        customerExt.customerCreditvo.recurringservice=serv.Id;
        customerExt.getServiceRecurringDetails();
        
        }catch(Exception ex){
    
        system.debug('exception'+ex);
        }
        try{
        CustomerCreditExtension customerExt=new CustomerCreditExtension();
        customerExt.customerCreditvo=new CustomerCreditExtension.CustomerCreditVO();
        customerExt.customerCreditvo.billProfile=b.Id;
        customerExt.customerCreditvo.recurringservice='--None--';
        customerExt.getServiceRecurringDetails();
        PageReference pageTest2 = Page.TestCustomerCreditPage;
        pageTest2.getParameters().put('Id',misc.Id);
        Test.setCurrentPage(pageTest2);
        customerExt.getCustomerCreditDetails();
            
        }catch(Exception ex){
    
        system.debug('exception'+ex);
        }
        try{
        CustomerCreditExtension customerExt=new CustomerCreditExtension();
        PageReference pageTest2 = Page.TestCustomerCreditPage;
        pageTest2.getParameters().put('Id',misc.Id);
        Test.setCurrentPage(pageTest2);
        misc.Credit_Status__c='Active';
        update misc;
        customerExt.getCustomerCreditDetails();
            
        }catch(Exception ex){
    
        system.debug('exception'+ex);
        }
        try{
        CustomerCreditExtension customerExt=new CustomerCreditExtension();
        PageReference pageTest2 = Page.TestCustomerCreditPage;
        pageTest2.getParameters().put('creditStatus','Approved');
        pageTest2.getParameters().put('approveBy','123456');
        pageTest2.getParameters().put('Id',misc.Id);
        Test.setCurrentPage(pageTest2);
        insert getUser();
        customerExt.getDataDetails();
        }catch(Exception ex){
    
        system.debug('exception'+ex);
        }
        try{
        CustomerCreditExtension customerExt=new CustomerCreditExtension();
        customerExt.customerCreditvo=new CustomerCreditExtension.CustomerCreditVO();
        customerExt.customerCreditvo.billProfile=b.Id;
        customerExt.accId=acc.id;
        customerExt.customerCreditvo.oneoffresource=res.Id;
        customerExt.getResourceOneOffDetails();
        }catch(Exception ex){
        system.debug('exception'+ex);
            
            
        }
        try{
            
        CustomerCreditExtension customerExt=new CustomerCreditExtension();
        customerExt.accId=acc.id;
        customerExt.customerCreditvo=new CustomerCreditExtension.CustomerCreditVO();
        customerExt.customerCreditvo.billProfile=b.Id;
        customerExt.customerCreditvo.recurringresource=res.Id;
        customerExt.getResourceRecurringDetails();
        
        }catch(Exception ex){
    
        system.debug('exception'+ex);
        }
        
                try{
        CustomerCreditExtension customerExt=new CustomerCreditExtension();
        customerExt.customerCreditvo=new CustomerCreditExtension.CustomerCreditVO();
        customerExt.customerCreditvo.billProfile=b.Id;
        customerExt.accId=acc.id;
        customerExt.customerCreditvo.oneoffresource='--None--';
        customerExt.getResourceOneOffDetails();
        }catch(Exception ex){
        system.debug('exception'+ex);
            
            
        }
        try{
            
        CustomerCreditExtension customerExt=new CustomerCreditExtension();
        customerExt.accId=acc.id;
        customerExt.customerCreditvo=new CustomerCreditExtension.CustomerCreditVO();
        customerExt.customerCreditvo.billProfile=b.Id;
        customerExt.customerCreditvo.recurringresource='--None--';
        customerExt.getResourceRecurringDetails();
        customerExt.deriveLogicforBillText('Text MRC Bill Text');
        SObject[] sObjects = new SObject[]{};
        sObjects.add(misc);
        CustomerCreditExtension.saveSObjects(sObjects);
        }catch(Exception ex){
    
        system.debug('exception'+ex);
        }
        
        try{
         Apexpages.StandardController stdController = new Apexpages.StandardController(acc);
        PageReference pageTest2 = Page.TestCustomerCreditPage;
        pageTest2.getParameters().put('Id',misc.Id);
        pageTest2.getParameters().put('sfdc.override',null);
        Test.setCurrentPage(pageTest2);
        CustomerCreditExtension customerExt1=new CustomerCreditExtension(stdController);
        System.assertEquals(customerExt1.accId, acc.Id);
         test.stopTest();
        }catch(Exception ex){
            
        system.debug('exception'+ex);   
            
        }
        
    /*
    public static testMethod void testmyExtension() {
    Account a = new Account(name='Tester');
    insert a;
    ApexPages.StandardController sc = new ApexPages.standardController(a);
    myExtension e = new myExtension(sc);
    System.assertEquals(e.acct, a);
}
    
    
    */        
  /*  }
        private static User getUser()
    {
       
       User userObj = new User();
       userObj.FirstName = 'Test';
       userObj.LastName = 'SFDC';
       userObj.IsActive  = true;
       userObj.Username = 'Test.SFDC@team.telstra.com.evolution';
       userObj.Email = 'Test.SFDC@team.telstra.com';
       userObj.Region__c = 'US';
       userObj.TimeZoneSidKey = 'Australia/Sydney';
       userObj.LocaleSidKey = 'en_AU';
       userObj.EmailEncodingKey = 'ISO-8859-1';
       userObj.LanguageLocaleKey = 'en_US';
       userObj.Alias = 'TSFDC';
       userObj.EmployeeNumber='123456';
       Profile p = [select id from Profile where name = 'TI Service Management'];
       userObj.ProfileId = p.id;
       return userObj;
       
    }
    
     private static Account getAccount(){
        if(a == null){
            a = new Account();
            a.Name = 'Test Account Test 1';
            a.Customer_Type__c = 'MNC';
            a.Customer_Legal_Entity_Name__c = 'Sample Name';
            a.Selling_Entity__c = 'Telstra INC';
            a.Activated__c = True;
            a.Account_ID__c = '5555';
            a.Account_Status__c = 'Active';
            insert a;
        }
        return a;
    }
    
   private static Miscellaneous_Charge__c getMiscObj(){
     Miscellaneous_Charge__c miscObj=new Miscellaneous_Charge__c();
     BillProfile__c billProfile=getBillProfile();
     miscObj.Bill_Profile__c=billProfile.Id;
     miscObj.Account__c=billProfile.Account__c;
     miscObj.Charge_Frequency__c='Monthly';
     miscObj.Credit_Against__c='Account';
     miscObj.Credit_Currency__c='AUD';
     miscObj.One_of_credit_bill_text__c='Test Bill text';
     miscObj.Recuring_credit_bill_text__c='Test Bill Text';
     miscObj.Start_Date_for_credit__c='05/22/2014';    
     miscObj.Type_of_Credit__c='Recurring Credit';
     miscObj.Credit_Status__c='Unapproved';
     miscObj.Recuring_Credit_amount__c=100.00;
     miscObj.One_of_Credit_amount__c=100.00;
     miscObj.Credit_Duration__c='2';
     return miscObj;
}    
    

    private static Product2  getProduct(){
            
            Product2 prod = new Product2();
            prod.name = 'Test Product';
            prod.CurrencyIsoCode='EUR';
            prod.Product_ID__c='GCPE';      
            prod.Create_Resource__c= false;
            prod.Recurring_Credit_Bill_Text__c='trttr';
            insert prod;
            return prod;
            
     }

  private static BillProfile__c getBillProfile(){
        BillProfile__c b = new BillProfile__c();
        Account acc = getAccount();
        //b.Bill_Profile_Number__c = 'Test Bill Profile';
        b.Billing_Entity__c = 'Telstra Limited';
        b.Account__c = acc.Id;
        b.Status__c='Active';
        insert b;
        return b;
    }
    public static Service__c getServiceObj(){
      Service__c ser = new Service__c();
        ser.name='serv1';
        ser.Path_Instance_ID__c='1467000';
        ser.Parent_Path_Instance_id__c = '';
        ser.Is_GCPE_shared_with_multiple_services__c  = 'Yes-New';
        ser.is_this_data_from_PSL__c = True;
        
      
      return ser;
 } 
 private static Order_Line_Item__c getOrderLineItem(){
        Order_Line_Item__c oli = new Order_Line_Item__c();
        oli.Opportunity_Line_Item_ID__c = 'Opp Line Item 001';
        oli.OrderType__c = 'New Provide';
        oli.Service_Assurance_Assigned_Date__c = System.now();
        oli.Service_Assurance_Assigned_Date__c = System.now();
        oli.Billing_Contact_Assigned_Date__c = System.now();
        oli.Service_Assurance_Assigned_Date__c = System.now();
        oli.Billing_Contact_Assigned_Date__c = System.now();
        oli.CPQItem__c = '1';
        oli.Is_GCPE_shared_with_multiple_services__c = 'NA';
        oli.MRC_Bill_Text__c='rgtetertetee';
        
        return oli;
    }
  private static Order__c getOrder(){
    if(ordr == null){        
        ordr = new Order__c();
      }
        return ordr;
    }   
public static Opportunity getOpportunity(){
            
                Opportunity opp = new Opportunity();
                opp.Name = 'Test Opportunity';
                 opp.StageName = 'Identify & Define';
                 opp.Stage__c='Identify & Define';
              
                opp.CloseDate = System.today();
                opp.Estimated_MRC__c=800;
                opp.Estimated_NRC__c=1000;
                opp.ContractTerm__c='10';
                opp.SOF_signed_by_customer__c = True;
                opp.QuoteStatus__c = 'Order';
                return opp;
        }
        
private static CostCentre__c getCostCentre(){
    if(c == null) {
        c = new CostCentre__c();
        c.Name= 'test cost';
       
   
    }
        return c;
}
    public static Resource__c getResourceObj(){
      Resource__c res = new Resource__c();
        res.name='resource';
        res.Path_Instance_ID__c='1467000';
        res.Parent_Path_Instance_id__c = '';
      return res;
 } 
*/
}