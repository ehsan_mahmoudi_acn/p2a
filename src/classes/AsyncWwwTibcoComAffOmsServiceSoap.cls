//Generated by wsdl2apex

public class AsyncWwwTibcoComAffOmsServiceSoap {
    public class AsyncOrderServicePort {
        public String endpoint_x = 'http://10.56.17.13:8080/omsServer/api/orderService';
        public Map<String,String> inputHttpHeaders_x;
        public String clientCertName_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://www.tibco.com/aff/plan', 'wwwTibcoComAffPlan', 'http://www.tibco.com/aff/enrichedPlan', 'wwwTibcoComAffEnrichedplan', 'http://www.tibco.com/aff/planfragments', 'wwwTibcoComAffPlanfragments', 'http://www.tibco.com/aff/oms/service/soap', 'wwwTibcoComAffOmsServiceSoap', 'http://www.tibco.com/aff/order', 'wwwTibcoComAffOrder', 'http://www.tibco.com/aff/orderservice', 'wwwTibcoComAffOrderservice', 'http://www.tibco.com/aff/orderservice/result', 'wwwTibcoComAffOrderserviceResult', 'http://www.tibco.com/aff/commontypes', 'wwwTibcoComAffCommontypes'};
        public AsyncWwwTibcoComAffOrderservice.SuspendOrderResponse_elementFuture beginSuspendOrder(System.Continuation continuation,String orderID,String orderRef) {
            wwwTibcoComAffOrderservice.SuspendOrderRequest_element request_x = new wwwTibcoComAffOrderservice.SuspendOrderRequest_element();
            request_x.orderID = orderID;
            request_x.orderRef = orderRef;
            return (AsyncWwwTibcoComAffOrderservice.SuspendOrderResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncWwwTibcoComAffOrderservice.SuspendOrderResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://www.tibco.com/aff/service/soap/SuspendOrder',
              'http://www.tibco.com/aff/orderservice',
              'SuspendOrderRequest',
              'http://www.tibco.com/aff/orderservice',
              'SuspendOrderResponse',
              'wwwTibcoComAffOrderservice.SuspendOrderResponse_element'}
            );
        }
        public AsyncWwwTibcoComAffOrderservice.SyncSubmitOrderResponse_elementFuture beginSyncSubmitOrder(System.Continuation continuation) {
            wwwTibcoComAffOrderservice.SubmitOrderRequest_element request_x = new wwwTibcoComAffOrderservice.SubmitOrderRequest_element();
            return (AsyncWwwTibcoComAffOrderservice.SyncSubmitOrderResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncWwwTibcoComAffOrderservice.SyncSubmitOrderResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://www.tibco.com/aff/service/soap/SyncSubmitOrder',
              'http://www.tibco.com/aff/orderservice',
              'SubmitOrderRequest',
              'http://www.tibco.com/aff/orderservice',
              'SyncSubmitOrderResponse',
              'wwwTibcoComAffOrderservice.SyncSubmitOrderResponse_element'}
            );
        }
        public AsyncWwwTibcoComAffOrderservice.GetOrdersResponse_elementFuture beginGetOrders(System.Continuation continuation,wwwTibcoComAffOrderservice.sortCriteria sortCriteria,String orderId,String orderRef,String customerID,String subscriberID,wwwTibcoComAffOrderservice.dateRange_element dateRange,String status,wwwTibcoComAffOrderservice.headerUDF_element[] headerUDF,wwwTibcoComAffOrderservice.orderLineUDF_element[] orderLineUDF,Long count,wwwTibcoComAffOrderservice.pagination_element pagination,Boolean orderSummary) {
            wwwTibcoComAffOrderservice.GetOrdersRequest_element request_x = new wwwTibcoComAffOrderservice.GetOrdersRequest_element();
            request_x.sortCriteria = sortCriteria;
            request_x.orderId = orderId;
            request_x.orderRef = orderRef;
            request_x.customerID = customerID;
            request_x.subscriberID = subscriberID;
            request_x.dateRange = dateRange;
            request_x.status = status;
            request_x.headerUDF = headerUDF;
            request_x.orderLineUDF = orderLineUDF;
            request_x.count = count;
            request_x.pagination = pagination;
            request_x.orderSummary = orderSummary;
            return (AsyncWwwTibcoComAffOrderservice.GetOrdersResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncWwwTibcoComAffOrderservice.GetOrdersResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://www.tibco.com/aff/service/soap/GetOrders',
              'http://www.tibco.com/aff/orderservice',
              'GetOrdersRequest',
              'http://www.tibco.com/aff/orderservice',
              'GetOrdersResponse',
              'wwwTibcoComAffOrderservice.GetOrdersResponse_element'}
            );
        }
        public AsyncWwwTibcoComAffOrderservice.PerformBulkOrderActionResponse_elementFuture beginPerformBulkOrderAction(System.Continuation continuation,String action,String[] orderID,String[] orderRef) {
            wwwTibcoComAffOrderservice.PerformBulkOrderActionRequest_element request_x = new wwwTibcoComAffOrderservice.PerformBulkOrderActionRequest_element();
            request_x.action = action;
            request_x.orderID = orderID;
            request_x.orderRef = orderRef;
            return (AsyncWwwTibcoComAffOrderservice.PerformBulkOrderActionResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncWwwTibcoComAffOrderservice.PerformBulkOrderActionResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://www.tibco.com/aff/service/soap/PerformBulkOrderAction',
              'http://www.tibco.com/aff/orderservice',
              'PerformBulkOrderActionRequest',
              'http://www.tibco.com/aff/orderservice',
              'PerformBulkOrderActionResponse',
              'wwwTibcoComAffOrderservice.PerformBulkOrderActionResponse_element'}
            );
        }
        public AsyncWwwTibcoComAffOrderservice.GetOrderDetailsResponse_elementFuture beginGetOrderDetails(System.Continuation continuation,String orderId,String orderRef) {
            wwwTibcoComAffOrderservice.GetOrderDetailsRequest_element request_x = new wwwTibcoComAffOrderservice.GetOrderDetailsRequest_element();
            request_x.orderId = orderId;
            request_x.orderRef = orderRef;
            return (AsyncWwwTibcoComAffOrderservice.GetOrderDetailsResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncWwwTibcoComAffOrderservice.GetOrderDetailsResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://www.tibco.com/aff/service/soap/GetOrderDetails',
              'http://www.tibco.com/aff/orderservice',
              'GetOrderDetailsRequest',
              'http://www.tibco.com/aff/orderservice',
              'GetOrderDetailsResponse',
              'wwwTibcoComAffOrderservice.GetOrderDetailsResponse_element'}
            );
        }
        public AsyncWwwTibcoComAffOrderservice.AmendOrderResponse_elementFuture beginAmendOrder(System.Continuation continuation) {
            wwwTibcoComAffOrderservice.AmendOrderRequest_element request_x = new wwwTibcoComAffOrderservice.AmendOrderRequest_element();
            return (AsyncWwwTibcoComAffOrderservice.AmendOrderResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncWwwTibcoComAffOrderservice.AmendOrderResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://www.tibco.com/aff/service/soap/AmendOrder',
              'http://www.tibco.com/aff/orderservice',
              'AmendOrderRequest',
              'http://www.tibco.com/aff/orderservice',
              'AmendOrderResponse',
              'wwwTibcoComAffOrderservice.AmendOrderResponse_element'}
            );
        }
        public AsyncWwwTibcoComAffOrderservice.SubmitOrderResponse_elementFuture beginSubmitOrder(System.Continuation continuation) {
            wwwTibcoComAffOrderservice.SubmitOrderRequest_element request_x = new wwwTibcoComAffOrderservice.SubmitOrderRequest_element();
            return (AsyncWwwTibcoComAffOrderservice.SubmitOrderResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncWwwTibcoComAffOrderservice.SubmitOrderResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://www.tibco.com/aff/service/soap/SubmitOrder',
              'http://www.tibco.com/aff/orderservice',
              'SubmitOrderRequest',
              'http://www.tibco.com/aff/orderservice',
              'SubmitOrderResponse',
              'wwwTibcoComAffOrderservice.SubmitOrderResponse_element'}
            );
        }
        public AsyncWwwTibcoComAffOrderservice.CancelOrderResponse_elementFuture beginCancelOrder(System.Continuation continuation,String orderID,String orderRef,Boolean rollback_x) {
            wwwTibcoComAffOrderservice.CancelOrderRequest_element request_x = new wwwTibcoComAffOrderservice.CancelOrderRequest_element();
            request_x.orderID = orderID;
            request_x.orderRef = orderRef;
            request_x.rollback_x = rollback_x;
            return (AsyncWwwTibcoComAffOrderservice.CancelOrderResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncWwwTibcoComAffOrderservice.CancelOrderResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://www.tibco.com/aff/service/soap/CancelOrder',
              'http://www.tibco.com/aff/orderservice',
              'CancelOrderRequest',
              'http://www.tibco.com/aff/orderservice',
              'CancelOrderResponse',
              'wwwTibcoComAffOrderservice.CancelOrderResponse_element'}
            );
        }
        public AsyncWwwTibcoComAffOrderservice.GetOrderExecutionPlanResponse_elementFuture beginGetOrderExecutionPlan(System.Continuation continuation,String orderID,String orderRef) {
            wwwTibcoComAffOrderservice.GetOrderExecutionPlanRequest_element request_x = new wwwTibcoComAffOrderservice.GetOrderExecutionPlanRequest_element();
            request_x.orderID = orderID;
            request_x.orderRef = orderRef;
            return (AsyncWwwTibcoComAffOrderservice.GetOrderExecutionPlanResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncWwwTibcoComAffOrderservice.GetOrderExecutionPlanResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://www.tibco.com/aff/service/soap/GetOrderExecutionPlan',
              'http://www.tibco.com/aff/orderservice',
              'GetOrderExecutionPlanRequest',
              'http://www.tibco.com/aff/orderservice',
              'GetOrderExecutionPlanResponse',
              'wwwTibcoComAffOrderservice.GetOrderExecutionPlanResponse_element'}
            );
        }
        public AsyncWwwTibcoComAffOrderservice.WithdrawOrderResponse_elementFuture beginWithdrawOrder(System.Continuation continuation,String orderID,String orderRef) {
            wwwTibcoComAffOrderservice.WithdrawOrderRequest_element request_x = new wwwTibcoComAffOrderservice.WithdrawOrderRequest_element();
            request_x.orderID = orderID;
            request_x.orderRef = orderRef;
            return (AsyncWwwTibcoComAffOrderservice.WithdrawOrderResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncWwwTibcoComAffOrderservice.WithdrawOrderResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://www.tibco.com/aff/service/soap/WithdrawOrder',
              'http://www.tibco.com/aff/orderservice',
              'WithdrawOrderRequest',
              'http://www.tibco.com/aff/orderservice',
              'WithdrawOrderResponse',
              'wwwTibcoComAffOrderservice.WithdrawOrderResponse_element'}
            );
        }
        public AsyncWwwTibcoComAffOrderservice.GetEnrichedExecutionPlanResponse_elementFuture beginGetEnrichedExecutionPlan(System.Continuation continuation,String orderID,String orderRef,Boolean includePlanFragments,Boolean includejeopardyHeader) {
            wwwTibcoComAffOrderservice.GetEnrichedExecutionPlanRequest_element request_x = new wwwTibcoComAffOrderservice.GetEnrichedExecutionPlanRequest_element();
            request_x.orderID = orderID;
            request_x.orderRef = orderRef;
            request_x.includePlanFragments = includePlanFragments;
            request_x.includejeopardyHeader = includejeopardyHeader;
            return (AsyncWwwTibcoComAffOrderservice.GetEnrichedExecutionPlanResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncWwwTibcoComAffOrderservice.GetEnrichedExecutionPlanResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://www.tibco.com/aff/oms/service/soap/GetEnrichedExecutionPlan',
              'http://www.tibco.com/aff/orderservice',
              'GetEnrichedExecutionPlanRequest',
              'http://www.tibco.com/aff/orderservice',
              'GetEnrichedExecutionPlanResponse',
              'wwwTibcoComAffOrderservice.GetEnrichedExecutionPlanResponse_element'}
            );
        }
        public AsyncWwwTibcoComAffOrderservice.ActivateOrderResponse_elementFuture beginActivateOrder(System.Continuation continuation,String orderID,String orderRef) {
            wwwTibcoComAffOrderservice.ActivateOrderRequest_element request_x = new wwwTibcoComAffOrderservice.ActivateOrderRequest_element();
            request_x.orderID = orderID;
            request_x.orderRef = orderRef;
            return (AsyncWwwTibcoComAffOrderservice.ActivateOrderResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncWwwTibcoComAffOrderservice.ActivateOrderResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://www.tibco.com/aff/service/soap/ActivateOrder',
              'http://www.tibco.com/aff/orderservice',
              'ActivateOrderRequest',
              'http://www.tibco.com/aff/orderservice',
              'ActivateOrderResponse',
              'wwwTibcoComAffOrderservice.ActivateOrderResponse_element'}
            );
        }
    }
}