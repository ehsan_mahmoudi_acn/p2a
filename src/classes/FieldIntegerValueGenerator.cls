public class FieldIntegerValueGenerator implements FieldValueGenerator {
    
    public Boolean canGenerateValueFor(Schema.DescribeFieldResult fieldDesc) {
        return Schema.DisplayType.INTEGER == fieldDesc.getType();
    }
    
    public Object generate(Schema.DescribeFieldResult fieldDesc) {
        return Math.round(Math.random() * 100);
    }
}