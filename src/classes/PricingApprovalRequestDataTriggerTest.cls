@isTest(SeeAllData=false) 
public class PricingApprovalRequestDataTriggerTest
 {
   Public static testMethod void pricingApprovalRequestDataTriggertest1() 
    {
    
    P2A_TestFactoryCls.sampleTestData();
    
         Test.StartTest();

        
        List<Pricing_Approval_Request_Data__c> PriAppReqDatList = new List<Pricing_Approval_Request_Data__c>{
            new Pricing_Approval_Request_Data__c(CurrencyIsoCode = 'INR',Cost_RC__c=5,Cost_NRC__c=6),
            new Pricing_Approval_Request_Data__c(CurrencyIsoCode = 'USD',Cost_RC__c=2,Cost_NRC__c=3)
        };
        List<CurrencyValue__c> CurValList = new List<CurrencyValue__c>{
            new CurrencyValue__c(CurrencyIsoCode = 'USD', Currency_Value__c = 1.0, Name = 'USD'),
            new CurrencyValue__c(CurrencyIsoCode = 'AUD', Currency_Value__c = 11.0, Name = 'AUD'),
            new CurrencyValue__c(CurrencyIsoCode = 'INR', Currency_Value__c = 62.0, Name = 'INR'),
            new CurrencyValue__c(CurrencyIsoCode = 'HKD', Currency_Value__c = 99.0, Name = 'HKD')
        };
        insert CurValList;
        list<CurrencyValue__c> prod = [select id,Name from CurrencyValue__c where Name = 'USD'];
        system.assertEquals(CurValList[0].Name , prod[0].Name);
        System.assert(CurValList!=null);
        
        List<Account> Acclist = new List<Account>{
            new Account(name = 'Test Normal', Customer_Type_New__c = 'GW', Selling_Entity__c = 'Telstra Incorporated',Account_Status__c = 'Active', Activated__c = True, Account_ID__c = '3333', Customer_Legal_Entity_Name__c = 'Test', Region__c = 'US'),
            new Account(name = 'ISO', Customer_Type_New__c = 'ISO', Selling_Entity__c = 'Telstra Incorporated',Account_Status__c = 'Active', Activated__c = True, Account_ID__c = '3333', Customer_Legal_Entity_Name__c = 'Test', Region__c = 'US')
        };
        insert AccList;
        list<Account> prod2 = [select id,Name from Account where Name = 'Test Normal'];
        system.assertEquals(AccList[0].Name , prod2[0].Name);
        System.assert(AccList!=null);
        
        List<Opportunity> OppList = new List<Opportunity>{
            new Opportunity(name = 'Opp', AccountID = AccList[0].id, Opportunity_Type__c = 'Complex', CloseDate = system.today()+ 10,CurrencyIsoCode = 'USD', StageName = 'Identify & Define', Sales_Status__c = 'Open', Order_Type__c = 'New', Win_Loss_Reasons__c = 'Duplicate' , Product_Type__c = 'IPL' , Estimated_MRC__c = 100.00 , Estimated_NRC__c = 100.00)
        };
        insert OppList;
        list<Opportunity> prod3 = [select id,Name from Opportunity where Name = 'Opp'];
        system.assertEquals(OppList[0].Name , prod3[0].Name);
        System.assert(OppList!=null);
        
        List<cscfga__Product_Basket__c> ProBasList = new List<cscfga__Product_Basket__c>{
            new cscfga__Product_Basket__c(name = 'Product Basket 0',Exchange_Rate__c=17.00,csordtelcoa__Synchronised_with_Opportunity__c = false, cscfga__Opportunity__c = OppList[0].id)
        };
         insert ProBasList;
         list<cscfga__Product_Basket__c> prod4 = [select id,Name from cscfga__Product_Basket__c where Name = 'Product Basket 0'];
        //system.assertEquals(ProBasList[0].Name , prod4[0].Name);
        System.assert(ProBasList!=null);
         
         ProBasList[0].CurrencyIsoCode='INR';
         update ProBasList;
         System.assert(ProBasList!=null);
        system.assertEquals(ProBasList[0].CurrencyIsoCode , 'INR');
         
    PriAppReqDatList = new List<Pricing_Approval_Request_Data__c>{
        new Pricing_Approval_Request_Data__c(CurrencyIsoCode = 'INR',Cost_RC__c=5,Cost_NRC__c=6)
    };
     Map<Id, SObject> newPARDMap;
     Map<Id, SObject> oldPARDMap;
    
    
    
    PricingApprovalRequestDataTriggerHandler pard = new PricingApprovalRequestDataTriggerHandler();
    pard.beforeUpdate(PriAppReqDatList, newPARDMap, oldPARDMap);
    pricingApprovalRequestDataTriggerHandler.updateCurrency(PriAppReqDatList);
    Boolean muteAllTriggers = pricingApprovalRequestDataTriggerHandler.muteAllTriggers(); 
    
    Test.StopTest(); 
     
    }
    
   @istest
    public static void testcatchblock(){

        PricingApprovalRequestDataTriggerHandler pard = new PricingApprovalRequestDataTriggerHandler();
      try{
          pard.beforeUpdate(null, null, null);
      }catch(Exception e){}
      try{
          pricingApprovalRequestDataTriggerHandler.updateCurrency(null);
      }catch(Exception e){}
      try{ 
         Boolean muteAllTriggers = pricingApprovalRequestDataTriggerHandler.muteAllTriggers();       
      }catch(Exception e){}
       system.assertEquals(true,pard!=null);
    }
     
 }