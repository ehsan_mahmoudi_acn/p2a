@isTest(SeeAllData=false) 
public class OrderConfigPricingValActionItemTest {
   
    static testmethod void positivetestcases()
    {     
       
         P2A_TestFactoryCls.sampleTestData();
         List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
       List<User> users = P2A_TestFactoryCls.get_Users(1);
        
       List<case> caselist = P2A_TestFactoryCls.getcases(1,AccList,opplist,Products,orders,users);
        List<Action_Item__c> ailist=P2A_TestFactoryCls.getActIteList(1);
        
        Action_Item__c act = new Action_Item__c(FeasibilityAOPPlan__c='Order support');
        insert act;
        
        
        String Template;
        String quename;
        List<String> ccAddressList;
        List<String> toAddressList;
        ailist[0].recordtypeid= RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support');
        ailist[0].Status__c='Unassigned';
        ailist[0].Support_Details__c='fsdfdsf';
        ailist[0].Pricing_Validation_Order_Configuration__c=act.id;
        Group g=[select id from Group where Type ='Queue' limit 1];
        ailist[0].OwnerId=g.id;
        ailist[0].Commercial_Impact__c='No';
        update ailist[0];
        
        Map<Id,Action_Item__c> alMap = new Map<Id,Action_Item__c>();
        alMap.put(ailist[0].id,ailist[0]);
        system.assertEquals(true,ailist!=null);     
        Id actionItemId;Id actionItemId1;
       
        try
        {
        OrderConfigPricingValActionItem OCV=new OrderConfigPricingValActionItem();
        OCV.SendEmailforOrderConfigAIonInsert(ailist);
        OCV.SendEmailforPricingValRejection(ailist,Template,quename,ccAddressList,toAddressList);
        OCV.SendEmailforOrderConfigRejection(ailist,Template,quename,ccAddressList,toAddressList);
        OCV.rejectPricingFromOrderConfigAI(ailist,alMap,alMap);
        OCV.updateSalesJustificationtoPricing(ailist,alMap,alMap);
        OCV.unAssignAgainPricingValAI(ailist,alMap,alMap);
        
        OrderConfigPricingValActionItem.updatingAILink(actionItemId,actionItemId1);
        
         system.assertEquals(true,OCV!=null);         
        }
            Catch(Exception e)
            {
            ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItemTest :positivetestcases';         
            ErrorHandlerException.sendException(e);     
            }
            
            
        
    }
    static testmethod void positivetestcases12() {       
         P2A_TestFactoryCls.sampleTestData();
          List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
         Test.starttest();
         Action_Item__c act = new Action_Item__c(FeasibilityAOPPlan__c='Order support');
        insert act;
         List<Action_Item__c> ailist1=P2A_TestFactoryCls.getActIteList(1);
            ailist1[0].recordtypeid= RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support');
            ailist1[0].Status__c='Rejected';
            ailist1[0].Support_Details__c='fsdfdsf';
            ailist1[0].Pricing_Validation_Order_Configuration__c=act.id;
            Group g=[select id from Group where Type ='Queue' limit 1];
            ailist1[0].OwnerId=g.id;
            ailist1[0].Opportunity__c=oppList[0].id;
            ailist1[0].Commercial_Impact__c='Yes';
            update ailist1[0];
        
        
        Map<Id,Action_Item__c> alMap = new Map<Id,Action_Item__c>();
        alMap.put(ailist1[0].id,ailist1[0]);
         
         
            OrderConfigPricingValActionItem OCV=new OrderConfigPricingValActionItem();
            OCV.unAssignAgainPricingValAI(ailist1,alMap,alMap);
            OCV.rejectPricingFromOrderConfigAI(ailist1,alMap,alMap);
         Test.stoptest();  
          list<Action_Item__c> action = [select id ,  Status__c,Commercial_Impact__c  from Action_Item__c where Commercial_Impact__c ='yes'];
       system.assertEquals(ailist1[0].Status__c ,action[0].Status__c );  
       
    }
    static testmethod void positivetestcases123() {      
         P2A_TestFactoryCls.sampleTestData();
          List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
         Test.starttest();
         Action_Item__c act = new Action_Item__c(FeasibilityAOPPlan__c='Order support');
        insert act;
         List<Action_Item__c> ailist1=P2A_TestFactoryCls.getActIteList(1);
            ailist1[0].recordtypeid= RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support');
            ailist1[0].Status__c='Completed';
            ailist1[0].Support_Details__c='fsdfdsf';
            ailist1[0].Pricing_Validation_Order_Configuration__c=act.id;
            Group g=[select id from Group where Type ='Queue' limit 1];
            ailist1[0].OwnerId=g.id;
            ailist1[0].Opportunity__c=oppList[0].id;
            ailist1[0].Commercial_Impact__c='Yes';
            try{
             update ailist1[0];
            }catch(Exception e){}
            
        
        List<String> str = new List<String>{'fff','ddd','sss'};
        Map<Id,Action_Item__c> alMap = new Map<Id,Action_Item__c>();
        alMap.put(ailist1[0].id,ailist1[0]);
         
         
            OrderConfigPricingValActionItem OCV=new OrderConfigPricingValActionItem();
            OCV.unAssignAgainPricingValAI(ailist1,alMap,alMap);
            OCV.rejectPricingFromOrderConfigAI(ailist1,alMap,alMap);
            try{
             OCV.SendEmailforOrderConfigRejection(ailist1,'sss','fdfd',str,str);
            }catch(Exception e){}
            
         Test.stoptest();
      system.assertEquals(ailist1[0].Status__c ,'Completed');  
                    
    }
    
     static testmethod void positivetestcases1()
    {
       /* User u=[Select id from user where Isactive=true limit 1];
        System.runAs(u)*/
        {
         P2A_TestFactoryCls.sampleTestData();
         List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
        List<User> users = P2A_TestFactoryCls.get_Users(1);
        // profile pr = [select id from profile where name='System administrator'];
        List<case> caselist = P2A_TestFactoryCls.getcases(1,AccList,opplist,Products,orders,users);
        List<Action_Item__c> ailist=P2A_TestFactoryCls.getActIteList(2);
        system.assertEquals(true,ailist!=null); 
       
            try
            {
            ailist[0].Status__c='Rejected';
            ailist[0].Rejection_Reason__c='Other';
             ailist[0].Pricing_Validation_Order_Configuration__c=ailist[1].id;
            ailist[0].recordtypeid= RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Pricing Validation');
           update ailist[0];
            system.assertEquals(true,ailist!=null);            
            }
            Catch(Exception e)
            {
              ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItemTest :positivetestcases1';         
                ErrorHandlerException.sendException(e);   
            }
      
        }    
        
    }
    static testmethod void positivetestcases2()
    {
    
        /*User u=[Select id from user where Isactive=true limit 1];
        System.runAs(u)*/
        //  P2A_TestFactoryCls.sampleTestData();
         List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
        List<User> users = P2A_TestFactoryCls.get_Users(1);
        // profile pr = [select id from profile where name='System administrator'];
        List<case> caselist = P2A_TestFactoryCls.getcases(1,AccList,opplist,Products,orders,users);
        List<Action_Item__c> ailist=P2A_TestFactoryCls.getActIteList(2);
        system.assertEquals(true,ailist!=null); 
            test.starttest();   
            try
            {
             
             Action_item__c ai = new Action_item__c();
                ai.Comments__c = 'hi';
            ai.Status__c='UnAssigned';
            ai.recordtypeid= RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support');
            ai.Opportunity__c=oppList[0].id;
             ai.Account__c=accList[0].id;
            insert ai;
              system.assertEquals(true,ai!=null);    
            ai.Status__c='Rejected';
            ai.Rejection_Reason__c='Other';
             ai.Pricing_Validation_Order_Configuration__c=ailist[1].id;
            ai.recordtypeid= RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support');
           update ai;  
            }
           
            Catch(Exception e)
            {
               ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItemTest :positivetestcases2';         
                ErrorHandlerException.sendException(e);    
            }
            test.stoptest();
       
    }
    
         @isTest static void testExceptions(){
         OrderConfigPricingValActionItem alls=new OrderConfigPricingValActionItem();
        try{OrderConfigPricingValActionItem.updatingAILink(null,null);}catch(Exception e){}
         try{
         alls.SendEmailforOrderConfigAIonInsert(null);
         }
         catch(Exception e)
         
         {
          ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItemTest :testExceptions';         
          ErrorHandlerException.sendException(e); 
         }
         
         try{alls.rejectOrderConfigfromPricingAI(null,null,null);}catch(Exception e){
         ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItemTest :testExceptions';         
          ErrorHandlerException.sendException(e); 
         }
         
         try{alls.rejectPricingFromOrderConfigAI(null,null,null);}catch(Exception e){
         ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItemTest :testExceptions';         
          ErrorHandlerException.sendException(e); 
         }
         try{alls.updateSalesJustificationtoPricing(null,null,null);}catch(Exception e){
         ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItemTest :testExceptions';         
          ErrorHandlerException.sendException(e); 
         }
         try{alls.unAssignAgainPricingValAI(null,null,null);}catch(Exception e){
         ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItemTest :testExceptions';         
          ErrorHandlerException.sendException(e); 
         }
         try{alls.unCommercialIMPACTUpdateAI(null,null,null);}catch(Exception e){
         ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItemTest :testExceptions';         
          ErrorHandlerException.sendException(e); 
         }
         try{alls.approveOrderConfigPricingValAI(null,null,null);}catch(Exception e){
         ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItemTest :testExceptions';         
          ErrorHandlerException.sendException(e); 
         }
         try{alls.updateOrderConfigAIDetailsOnInsert(null);}catch(Exception e){
         ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItemTest :testExceptions';         
          ErrorHandlerException.sendException(e); 
         }
         try{alls.createPricingValidationAI(null);}catch(Exception e){
         ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItemTest :testExceptions';         
          ErrorHandlerException.sendException(e); 
         }
         try{OrderConfigPricingValActionItem.updatingAILink(null,null);}catch(Exception e){
         ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItemTest :testExceptions';         
          ErrorHandlerException.sendException(e); 
         }
          system.assertEquals(true,alls!=null);    
         
         
     }

}