@isTest(SeeAllData = false) 
public class ErrorHandlerExceptionTest {
@isTest static void testMethod1(){
    List<Account> ListAcc  = P2A_TestFactoryCls.getAccounts(1); 
    Solutions__c sol=new Solutions__c(Account_Name__c=ListAcc[0].id);   
    
    List<Solutions__c>solList=new List<Solutions__c>();
    solList.add(sol);
    List<Id>solIdList=new List<Id>();
    solIdList.add(sol.id);  
    system.assert(solIdList!=null);
    
    ErrorHandlerMechanism__c ehm=new ErrorHandlerMechanism__c();
    ehm.name='Logger';
    ehm.enableMailer__c=true;
    insert ehm;
    
    try{
    
    integer i=1/0;//will cause the exception

    }catch(Exception e){
    ErrorHandlerException.ExecutingClassName='AllServiceTriggerHandler:afterUpdate';
    ErrorHandlerException.objectList=solList;
    ErrorHandlerException.objectIdList=solIdList;
    ErrorHandlerException.sendException(e);
    ErrorHandlerException.logException('Some Exception Name',e.getMessage(),null,solIdList,ErrorHandlerException.ExecutingClassName);
    }
}

@isTest static void testMethod2(){
    List<Account> ListAcc  = P2A_TestFactoryCls.getAccounts(1); 
    Solutions__c sol=new Solutions__c(Account_Name__c=ListAcc[0].id);
    system.assert(sol!=null);
    
    ErrorHandlerMechanism__c ehm=new ErrorHandlerMechanism__c();
    ehm.name='Logger';
    ehm.enableMailer__c=true;
    insert ehm;
    
    try{
    
    integer i=1/0;//will cause the exception

    }catch(Exception e){
    ErrorHandlerException.ExecutingClassName='AllServiceTriggerHandler:afterUpdate';
    ErrorHandlerException.objectId=sol.id;  
    ErrorHandlerException.sendException(e);
    ErrorHandlerException.logException('Some Exception Name',e.getMessage(),ErrorHandlerException.objectId,new List<Id>(),ErrorHandlerException.ExecutingClassName);
    }
}

@isTest static void testMethod3(){
    List<Account> ListAcc  = P2A_TestFactoryCls.getAccounts(1); 
    Solutions__c sol=new Solutions__c(Account_Name__c=ListAcc[0].id);
    system.assert(sol!=null);
    
    ErrorHandlerMechanism__c ehm=new ErrorHandlerMechanism__c();
    ehm.name='Logger';
    ehm.enableMailer__c=true;
    insert ehm;
    
    try{
    
    integer i=1/0;//will cause the exception

    }catch(Exception e){
    ErrorHandlerException.ExecutingClassName='AllServiceTriggerHandler:afterUpdate';    
    ErrorHandlerException.sendException(e);
    ErrorHandlerException.logException('Some Exception Name',e.getMessage(),null,new List<Id>(),ErrorHandlerException.ExecutingClassName);
    }
}

@isTest static void testMethod4(){
    
    ErrorHandlerMechanism__c ehm=new ErrorHandlerMechanism__c();
    ehm.name='Logger';
    ehm.enableMailer__c=true;
    insert ehm;
    
    String errormsg= 'test';
    ErrorHandlerException.sendExceptionMail('SomeErrorMessage');
    system.assert(errormsg!=null);
 }
}