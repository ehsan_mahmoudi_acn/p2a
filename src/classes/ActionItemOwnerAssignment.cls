/**
    @author - Accenture
    @date - 09-JUNE-2012
    @version - 1.0
    @description - This class is used for Action item Owner Assignment .
*/

global class  ActionItemOwnerAssignment {
    
     /**
        @author - Accenture
        @date - 09-JUNE-2012
        @param - String accountId
        @param - String leadId
        @return - void 
    **/
        //description - This method is used for Account Varification Request action item owner assignment based on region.
    

    webService static void assignActionItemOwner (String accountId) 
    {
              //Map to store region and queueID mapping 
              Map<String,String> regionQueueMapping   = new Map<String,String>();   
                          
              //query account to get AccountObject based on request account ID
              Account objAccount = [Select Name,Account_Status__c,Requested_for_Verification__c,Region__c,Customer_Type__c from Account where Id=:accountId];
                //system.debug('checking'+objAccount.Customer_type__c);
              // query user to get the current user's region.   
              List<User> usrLst =[Select Name, Region__c,Role_Name__c from User where id= :UserInfo.getUserId()];
              User usr = usrLst[0];  
             //Get data from custom setting for Account Verification Request    
              List<Action_Item_Owner_Assignment__c> actionOwnerAssignments = Action_Item_Owner_Assignment__c.getAll().values(); 
              
              //Populate Map updated map key as part of CR320 part2
              for (Action_Item_Owner_Assignment__c objAssignment:actionOwnerAssignments) {

                  if(objAssignment.Module__c=='Account Verification Request'){
                    if (objAssignment.Region_AND_Customer_Type__c!=null){                          
                       regionQueueMapping.put(objAssignment.Region_AND_Customer_Type__c,objAssignment.Queue_ID__c);
                     }else{
                         regionQueueMapping.put(objAssignment.Region__c,objAssignment.Queue_ID__c);
                     }
                  }
                }
    
              //Create Action item record
              Action_Item__c objActionItem = new Action_Item__c();              
              objActionItem.Account__c = accountId;
              objActionItem.Due_Date__c = System.Today() + 1 ;

              //get recordtypeId from Util class.  
              objActionItem.RecordTypeId = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType, 'Adhoc Request');
              //populate region from account.
             objActionItem.Region__c =  usr.region__c;             
                //populate subject, priority & status from custom setting.
              objActionItem.Subject__c = Global_Constant_Data__c.getvalues('Account Verification Request Subject').value__c;
              objActionItem.Status__c = Global_Constant_Data__c.getvalues('Account Verification Request Status').value__c;
              objActionItem.Priority__c = Global_Constant_Data__c.getvalues('Account Verification Request Priority').value__c;
              //query action item owner assignment to fetch region, customer type.
                List<Action_Item_Owner_Assignment__c> s=[select Region_And_Customer_type__c,Region__c from Action_Item_Owner_Assignment__c where Action_Item_Owner_Assignment__c.region__C='Australia' limit 1];
                Action_Item_Owner_Assignment__c act = s[0];             
              
              //populate action owner queue based on account region and account customer type.CR320 part2
            if(usr.region__c!=null)
              {
                   // Logic Disabled for TGME0020957 to route all account activations to a single queue - Account_Admin_Support : Date - 25/04/2015 
                   /*if (usr.region__c=='North Asia' || usr.region__c=='South Asia')
                   {
                        act.Region_And_Customer_type__c ='Asia';
                   }else if (usr.region__c=='US')
                   {
                         act.Region_And_Customer_type__c ='US';
                   }else if (usr.region__c=='EMEA')
                   {
                         act.Region_And_Customer_type__c ='EMEA';
                   }else if (usr.region__c=='Australia' && objAccount.Customer_Type__c =='MNC')
                   {
                       act.Region_And_Customer_Type__c ='Australia MNC';
                   }else if (usr.region__c=='Australia' && objAccount.Customer_Type__c =='GSP')
                   {
                        act.Region_And_Customer_Type__c ='Australia GSP';                       
                   } */
                   // Here we are setting AI Owner to a single queue as defined in custom setting
                   act.Region_And_Customer_Type__c ='Account_Admin_Support';  
                   objActionItem.OwnerId = regionQueueMapping.get(act.Region_And_Customer_Type__c);
              }else{
                   objActionItem.OwnerId = regionQueueMapping.get(Global_Constant_Data__c.getvalues('Default Action Item Region').value__c);
              } 
              
               /*if(usr.region__c!=null){
                 objActionItem.OwnerId = regionQueueMapping.get(usr.region__c);
              }else{
                objActionItem.OwnerId = regionQueueMapping.get(Global_Constant_Data__c.getvalues('Default Action Item Region').value__c);
              }*/
              
              //insert action item.
               //Suparna/UAT Defect-7606:Commented out try/catch  and handled the exception in the javascript of “Request for Verification “ button
              //try {
                    insert objActionItem;
              // }
              // catch (Exception e) {
                //code to be inserted for error frame work.
              //}
              
              //populate account status and flag for verification
              objAccount.Account_Status__c = 'Pending Activation';
              objAccount.Requested_for_Verification__c =true;


              try {
                    update objAccount;
               }
               catch (Exception e) {
                //code to be inserted for error frame work.
               ErrorHandlerException.ExecutingClassName='AllCaseTriggerHandler:beforeInsert';      
               ErrorHandlerException.objectId=objAccount.id;
               ErrorHandlerException.sendException(e);
               }    

              
      
            }
        webService static void assignActionItemOwnertoLead (String leadId) 
    {
              //Map to store region and queueID mapping 
              Map<String,String> regionQueueMapping   = new Map<String,String>();   
                          
              //query account to get LeadObject based on request lead ID
              Lead objLead = [Select Name,Request_for_Conversion__c,Region__c,Sales_Type__c from Lead where Id=:leadId];      
              
              //Account objAccount = [Select Name,Account_Status__c,Requested_for_Verification__c,Region__c,Customer_Type__c from Account where Id=:accountId];
              
              // query user to get the current user's region.   
              List<User> usrLst =[Select Name, Region__c from User where id= :UserInfo.getUserId()];
              User usr = usrLst[0]; 
                
             //Get data from custom setting for Lead Conversion Request    
              List<Action_Item_Owner_Assignment__c> actionOwnerAssignments = Action_Item_Owner_Assignment__c.getAll().values(); 
              
              //Populate Map updated as part CR320 part2
              for (Action_Item_Owner_Assignment__c objAssignment:actionOwnerAssignments) {

                  if(objAssignment.Module__c=='Lead Conversion Request'){
                       if (objAssignment.Region_AND_Customer_Type__c!=null){ 
                       system.debug('checking..' +objAssignment.Region_AND_Customer_Type__c);                         
                       regionQueueMapping.put(objAssignment.Region_AND_Customer_Type__c,objAssignment.Queue_ID__c);
                     }else{
                         regionQueueMapping.put(objAssignment.Region__c,objAssignment.Queue_ID__c);
                     }
                  }
                }
    
              //Create Action item record
              Action_Item__c objActionItem = new Action_Item__c();              
              objActionItem.Lead__c = leadId;
              objActionItem.Due_Date__c = System.Today() + 5 ;

              //get recordtypeId from Util class.  
              objActionItem.RecordTypeId = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType, 'Adhoc Request');
              //populate region from account.
             objActionItem.Region__c =usr.region__c;
                //populate subject, priority & status from custom setting.
              objActionItem.Subject__c = Global_Constant_Data__c.getvalues('Lead Conversion Request Subject').value__c;
              objActionItem.Status__c = Global_Constant_Data__c.getvalues('Lead Conversion Request Status').value__c;
              objActionItem.Priority__c = Global_Constant_Data__c.getvalues('Lead Conversion Request Priority').value__c;
              
                List<Action_Item_Owner_Assignment__c> s=[select Region_And_Customer_type__c,Region__c from Action_Item_Owner_Assignment__c where Action_Item_Owner_Assignment__c.region__C='Australia' limit 1];
                Action_Item_Owner_Assignment__c act = s[0];
                 //system.debug('checking..' +objLead.Sales_Type__c);
              
              //populate action owner queue based on lead region and account customer type. CR320 part2
              //PSM Fix for action item not getting created - 11/03/2015
              if(usr.region__c!=null)
              {
              // Logic Disabled for TGME0020981 to route all Lead Conversion to a single queue - Account_Admin_Support : Date - 30/07/2015 
              /**     if (usr.region__c=='North Asia' || usr.region__c=='South Asia')
                   {
                        act.Region_And_Customer_type__c ='Asia';                        
                   }else if (usr.region__c=='US')
                   {
                         act.Region_And_Customer_type__c ='US';
                   }else if (usr.region__c=='EMEA')
                   {
                         act.Region_And_Customer_type__c ='EMEA';
                   }else if (usr.region__c=='Australia' &&  objLead.Sales_Type__c =='MNC')
                   {
                       act.Region_And_Customer_Type__c ='Australia MNC';
                   }else if (usr.region__c=='Australia' &&  objLead.Sales_Type__c =='GSP')
                   {
                        act.Region_And_Customer_Type__c ='Australia GSP';                       
                   } 
                **/
                   act.Region_And_Customer_Type__c ='Account_Admin_Support';  
                   objActionItem.OwnerId = regionQueueMapping.get(act.Region_And_Customer_Type__c);
              }else{
                   objActionItem.OwnerId = regionQueueMapping.get(Global_Constant_Data__c.getvalues('Default Action Item Region').value__c);
              } 
              system.debug('Sid=OwnerId'+objActionItem.OwnerId);
                            
              //insert action item.
              try {
                    insert objActionItem;
               }
               catch (Exception e) {
                ErrorHandlerException.ExecutingClassName='ActionItemOwnerAssignment:assignActionItemOwnertoLead';      
                ErrorHandlerException.sendException(e);
                //code to be inserted for error frame work.
               }
              
              //flag for lead conversion
              objLead.Request_for_Conversion__c =true;
              objLead.Status='Sales Accepted Lead (SAL)';


              try {
                    update objLead;
               }
               catch (Exception e) {
                //code to be inserted for error frame work.
               ErrorHandlerException.ExecutingClassName='AllCaseTriggerHandler:assignActionItemOwnertoLead ';      
               ErrorHandlerException.objectId=objLead.id;
               ErrorHandlerException.sendException(e);
               }    

              
      
            }


    }