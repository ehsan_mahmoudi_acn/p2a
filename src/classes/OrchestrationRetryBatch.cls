public with sharing class OrchestrationRetryBatch implements Database.Batchable<CSPOFA__Orchestration_Step__c> {
	public OrchestrationRetryBatch() {
		
	}

	public List<CSPOFA__Orchestration_Step__c> start(Database.BatchableContext BC) {
		return [SELECT Id, CSPOFA__Status__c, CSPOFA__Orchestration_Process__r.CSPOFA__Status__c, CSPOFA__Orchestration_Process__r.CSPOFA__State__c FROM CSPOFA__Orchestration_Step__c WHERE CSPOFA__Status__c = 'Error' and CSPOFA__Message__c like '% unable to obtain exclusive access to this record %'];
	}

   	public void execute(Database.BatchableContext BC, List<CSPOFA__Orchestration_Step__c> orchestrationSteps) {
   		Set<CSPOFA__Orchestration_Process__c> orchestrationProcessSet = new Set<CSPOFA__Orchestration_Process__c>();

   		for(CSPOFA__Orchestration_Step__c orchestrationStep : orchestrationSteps){
   			orchestrationStep.CSPOFA__Status__c = 'In Progress';
   			orchestrationStep.CSPOFA__Orchestration_Process__r.CSPOFA__Status__c = 'In Progress';
   			orchestrationStep.CSPOFA__Orchestration_Process__r.CSPOFA__State__c = 'ACTIVE';

   			orchestrationProcessSet.add(orchestrationStep.CSPOFA__Orchestration_Process__r);
   		}

   		update orchestrationSteps;
   		update new List<CSPOFA__Orchestration_Process__c>(orchestrationProcessSet);
	}
	
	public void finish(Database.BatchableContext BC) {
		
	}
}