@isTest(SeeAllData=false)
public class MasterServiceLogicOldTest {
    
    
    private static List<Offer_Id__c> offerlist;
    private static List<cscfga__Product_Configuration__c> pclist;
    private static List<Product_Definition_Id__c> PdIdlist;
    private static List<cscfga__Product_Definition__c> Pdlist; 
    private static cscfga__Product_Basket__c basket = null;
    
        private static List<cscfga__Product_Category__c> getProductCategory(){
        List<cscfga__Product_Category__c> productCategoryList = new List<cscfga__Product_Category__c>();
        cscfga__Product_Category__c PC = new cscfga__Product_Category__c();
        PC.Name = 'Sample';
        productCategoryList.add(PC);
        insert productCategoryList;
          System.assert(productCategoryList!=null);
        return productCategoryList;
    }
    
     static testmethod void controllerTest() { 
        
          P2A_TestFactoryCls.sampleTestData();
        
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
        system.assertEquals(true,proconfig!=null); 
        List<csbb__Product_Configuration_Request__c> productConfigReqList = P2A_TestFactoryCls.getProductonfigreq(1, getProductCategory());
             set<id> pbids = new set<id>();
             pbids.add(Products[0].id); 
            system.assertEquals(true,pbids!=null); 
        
        MasterServiceLogicOld old = new MasterServiceLogicOld();
        
        MasterServiceLogicOld.initContextData(pbids);
        MasterServiceLogicOld.process(pbids); 
        MasterServiceLogicOld.initDefinitionMap();
        MasterServiceLogicOld.getMasterId(proconfig[0]);
        MasterServiceLogicOld.getBasketMasterId(proconfig [0]);
        try{
        MasterServiceLogicOld.createMasterConfigurationRequests(proconfig);
        }
        catch(Exception e)
        {
        ErrorHandlerException.ExecutingClassName='MasterServiceLogicOldTest:controllerTest';         
        ErrorHandlerException.sendException(e); 
        }
        MasterServiceLogicOld.finishMasterConfigurations(proconfig);
        MasterServiceLogicOld.createBasketMasterMap(pbids);
        MasterServiceLogicOld.createBasketMasterService();
        MasterServiceLogicOld.updateProductsMasterService();
        
        
    }
    @istest
    public static void testcatchblock()
    {
    try
    {
     MasterServiceLogicOld.createMasterConfigurationRequests(null);
    }catch(Exception e)
    {
    ErrorHandlerException.ExecutingClassName='MasterServiceLogicOldTest:controllerTest';         
    ErrorHandlerException.sendException(e); 
    
    Boolean expectedExceptionThrown =  e.getMessage().contains('My Error Message')?true:false;
    System.AssertNOTEquals(expectedExceptionThrown, true);
    
    //String message = e.getMessage();
    //System.assert(e.getMessage().contains('ERROR'), 'message=' + e.getMessage());
    }
    }
}