@isTest
private class ContactSiteOverRideNewTest 
 {
    static Account a;
    static Country_Lookup__c cl;
    static Contact c;
    
    static testMethod void myUnitTest() 
       {
            
    List<ObjectPrefixIdentifiers__c> objPrfList = new List<ObjectPrefixIdentifiers__c>();
          ObjectPrefixIdentifiers__c objprf = new ObjectPrefixIdentifiers__c(Name='Site',ObjectIdPrefix__c='a0E');
          objPrfList.add(objprf);
          ObjectPrefixIdentifiers__c objprf1 = new ObjectPrefixIdentifiers__c(Name='Contact',ObjectIdPrefix__c='003');
          objPrfList.add(objprf1);
           ObjectPrefixIdentifiers__c objprf2 = new ObjectPrefixIdentifiers__c(Name='Sites & Contacts',ObjectIdPrefix__c='a0f');
          objPrfList.add(objprf2);
          insert objPrfList ;  
          
         Contact cont= getContact();
          system.debug('cont.Id');  
          system.currentPageReference().getParameters().put('retUrl', '/'+cont.Id);    
          ApexPages.StandardController sc1 = new ApexPages.StandardController(cont);                  
          NewContactSiteOverRideController nCS= new NewContactSiteOverRideController(sc1);    
         // System.assertEquals(nCS.reDirectPage().getUrl(),'/'+cont.id);
          nCS.reDirectPage();
          system.assert(nCS!=null);     
        //Logic
       }
        private static Account getAccount()
        {
            RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Account' LIMIT 1];
           if(a == null)
           {    
            a = new Account();
            Country_Lookup__c country = getCountry();
            a.Name = 'Test Account Test 1';
            a.Customer_Type__c = 'MNC';
            a.Country__c = country.Id;
            a.Selling_Entity__c = 'Telstra INC';
            a.Account_Manager__c = 'Albert Thomas';
            a.Activated__c = true;
            a.RecordTypeId = rt.Id;
            a.Account_Status__c ='Active';
            a.Customer_Legal_Entity_Name__c='Test';
            a.Account_ID__c='123';
            insert a;
           
           list<Account> prod = [select id,Name from Account where Name = 'Test Account Test 1'];
          system.assertEquals(a.Name , prod[0].Name);
        System.assert(a!=null); 
           }
        return a;
    }
    private static Contact getContact()
    {
    if( c== Null)
   {
    c = new Contact();
    Account acc = getAccount();
    c.Description='testContact';
    c.AccountId=acc.Id; 
    c.LastName='testName';
    c.Contact_Type__c='Sales';
    insert c;
    system.assert(c!=null);
   }
    return c; 
    }
    
    private static Country_Lookup__c getCountry()
    {
        if(cl == null)
       { 
        cl = new Country_Lookup__c();
        cl.CCMS_Country_Code__c = 'SG';
        cl.CCMS_Country_Name__c = 'India';
        cl.Country_Code__c = 'SG';
        insert cl;
        system.assert(cl!=null);
        }
        return cl;
    } 
}