global class SupplierQuoteQuery extends cscfga.ALookupSearch {
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){
        // NOT IMPLEMENTED
        List <VendorQuoteDetail__c> data = [SELECT Id, Non_Recurring_Cost__c,
                                            recurring_Cost__c
                                            FROM VendorQuoteDetail__c 
                                            WHERE Product_config__c=:productDefinitionID 
                                            AND Winning_Quote__c = TRUE];
        return data;
    }
    public override String getRequiredAttributes(){ 
        return '[ "Supplier_Quote_Needed__c"]';
    }

}