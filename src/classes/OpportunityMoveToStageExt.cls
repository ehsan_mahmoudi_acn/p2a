/**    @author - Hengky Ilawan Djapar <hengky.djapar@team.telstra.com>
       @date - 2016-08-30    
       @version - 0.1 
       @description - This class is used as an Extension to update Stage, Sales Status and notify user with Message.
       @platform - Salesforce1
**/

public with sharing class OpportunityMoveToStageExt{

    public class OpportunityMoveToStageException extends Exception {}

    public Opportunity oppt {get;set;}
    public String errorMsg {get;set;}
    public OpportunityStage oppNextStage {get;set;}
    public List<SelectOption> WinReasonOptions {get;set;}
    public List<String> WinReasonSelected {get;set;}

    private OpportunityStage nextStage(String pStageName) {
        if (oppt != null) {
            Integer currentStageOrder = [
                SELECT SortOrder 
                FROM OpportunityStage 
                WHERE MasterLabel = :pStageName 
                    AND IsActive = true].SortOrder;
            
            Integer nextStageOrder = currentStageOrder + 1;
            
            List<OpportunityStage> oppStages = [
                SELECT Masterlabel, DefaultProbability
                FROM OpportunityStage 
                WHERE SortOrder = :nextStageOrder
                    AND IsActive = true];

            if (!oppStages.isEmpty() && oppStages.size() == 1) {
                return oppStages[0];
            }
        }
        return null;
    }

   
    public OpportunityMoveToStageExt(ApexPages.StandardController stdController) {
        WinReasonSelected = new List<String>{};
        if (oppt == null) {
            oppt = (Opportunity)stdController.getRecord().clone(true, true);

            oppNextStage = nextStage(oppt.StageName);
            oppt.Is_Edited_from_SF1__c = true;
            oppt.Last_Modified_Date_By_SF1__c = System.NOW();

            WinReasonOptions = new List<SelectOption>();
            for (String reason : getPicklistValues(oppt,'Win_Reasons__c')) {
                WinReasonOptions.add(new SelectOption(reason,reason));
            }
            if (oppt.Win_Loss_Reasons__c != null) {
                WinReasonSelected = oppt.Win_Loss_Reasons__c.split(';');
            }
        }
    }
 
    public PageReference saveStage() {
        try {
            errorMsg = '';

            if (oppNextStage != null) {

                if (oppNextStage.Masterlabel == 'Prove & Close') {
                    oppt.Win_Loss_Reasons__c = String.join(WinReasonSelected, ';');
                }
            
                oppt.StageName = oppNextStage.Masterlabel;
                oppt.Stage__c = oppNextStage.Masterlabel;
                oppt.UIFlag__c = oppt.UIFlag__c != null ? oppt.UIFlag__c + 1 : 1;
                oppt.Probability = oppNextStage.DefaultProbability;
                oppt.Is_Edited_from_SF1__c = true;
                oppt.Last_Modified_Date_By_SF1__c = System.NOW();

                //util.skipOpportunitySplitFlag = false;
                //util.FlagtoSkipOpportunityTeamUpdateTwice = false;
                
                update(oppt);
                return new ApexPages.StandardController(oppt).view();
            }
            else {
                throw new OpportunityMoveToStageException('Invalid next stage.');
            }
        }
        catch(System.DMLException ex) {
            for (Integer i = 0; i < ex.getNumDml(); i++) {
                errorMsg = errorMsg != '' ? 
                  errorMsg + '; ' + ex.getDmlMessage(i) :
                  ex.getDmlMessage(i);
            }
            return null;
        }
        catch(System.Exception ex) {
            errorMsg = ex.getMessage();
            return null;
        }
    }

    private List<String> getPicklistValues(SObject obj, String fld){
        List<String> options = new List<String>();
        Schema.sObjectType objType = obj.getSObjectType(); 
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        List<Schema.PicklistEntry> values = fieldMap.get(fld).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : values) { 
            options.add(a.getValue());
        }
        return options;
    } 

}