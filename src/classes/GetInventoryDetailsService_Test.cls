@isTest(SeeAllData = false)
public class GetInventoryDetailsService_Test{
    private static String ObjectType = 'sdfd';
    private static String ObjectID = 'dded';
    
    static testMethod void detailsService() {
            
        Test.startTest();
            GetInventoryDetailsService.InventoryDetailsServicePortSoapPort hndlr1 = new GetInventoryDetailsService.InventoryDetailsServicePortSoapPort();
            Test.setMock(WebServiceMock.class, new MainMockClass.InvDetailsOutputVO());
            GetInventoryDetailsParams.InvDetailsOutputVO  InvDetail =  hndlr1.getInventoryDetails(ObjectType,ObjectID); 
            system.assertEquals(true,hndlr1!=null); 
            system.assertEquals(true,InvDetail!=null); 
        Test.stopTest();        

    }
}