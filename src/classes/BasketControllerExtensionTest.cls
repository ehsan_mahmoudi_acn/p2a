@isTest(seealldata= false)
public class BasketControllerExtensionTest {
 public static List<csbb__Product_Configuration_Request__c> ProductConfigreqlist;
  public static List<cscfga__Product_Category__c> ProdCatlist;   
   public static List<cscfga__Product_Configuration__c> pc; 
    public static List<cscfga__Product_Definition__c> Pdlist;
    static testmethod void controllerTest() {
        P2A_TestFactoryCls.sampletestdata();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
  
        ProdCatlist= new List<cscfga__Product_Category__c>{
            new cscfga__Product_Category__c(name = 'product')            
        };        
        insert ProdCatlist;
        integer i= [Select count() from cscfga__Product_Category__c];
        system.assert(i>0);
        
        
        
      /*  cscfga__Product_Configuration__c pcs = new cscfga__Product_Configuration__c();        
        pcs.name ='Test PC';
        insert PCS;*/
        
         cscfga__Product_Configuration__c pb = new cscfga__Product_Configuration__c();
                pb.cscfga__Product_basket__c = Products[0].id ;
                pb.cscfga__Product_Definition__c = Prodef[0].id ;
                pb.name ='IPL';
                pb.cscfga__contract_term_period__c = 12;
                pb.Is_Offnet__c = 'yes';
                pb.cscfga_Offer_Price_MRC__c = 200;
                pb.cscfga_Offer_Price_NRC__c = 300;
                pb.cscfga__Product_Bundle__c = pbundlelist[0].id;
                pb.Rate_Card_NRC__c = 200;
                pb.Rate_Card_RC__c = 300;
                pb.cscfga__total_contract_value__c = 1000;
                pb.cscfga__Contract_Term__c = 12;
                pb.CurrencyIsoCode = 'USD';
                pb.cscfga_Offer_Price_MRC__c = 100;
                pb.cscfga_Offer_Price_NRC__c = 100;
                pb.Child_COst__c = 100;
                pb.Cost_NRC__c = 100;
                pb.Cost_MRC__c = 100;
                pb.Product_Name__c = 'test product';
                pb.Added_Ports__c = null ;
                Pb.cscfga__Product_Family__c = 'Point to Point';
                Pb.csordtelcoa__Replaced_Product_Configuration__c = Pb.csordtelcoa__Replaced_Product_Configuration__c;
          insert pb; 
        
        integer j= [Select count() from cscfga__Product_Configuration__c];
        system.assert(j>0);
         
          
        // Test constructor
        ApexPages.StandardController sc = new ApexPages.StandardController(Products[0]);
        BasketControllerExtension bc = new BasketControllerExtension(sc);
        BasketControllerExtension.updateProductConfigurationsRequest(pb.id,proconfig[0].id,Products[0].id);
        //ctrl = new BasketEditorController(sc);
        
        List<csbb__Product_Configuration_Request__c> pcreqlist = P2A_TestFactoryCls.getProductonfigreq(1, ProdCatlist);

        set<Id> pcreqsettids = new set<Id>();  

      /*  for(csbb__Product_Configuration_Request__c serv : pcreqlist){
            pcreqsettids.add(serv.Id);
        }*/
       
      BasketControllerExtension.deleteConfigs(pcreqsettids); 
       
    }
    
}