@istest
public class GetServiceNowParamDetailsTest{
    
    static testMethod void serviceNowmethod() {
    
    GetServiceNowParamDetails.Task task = new GetServiceNowParamDetails.Task();
    
   // task.Task_Inst_Id = 'test';
    task.Action = 'Test';
   // task.Priority = 'test';
    task.Site_hum_id = 'test';
    task.Elm_compl_status = 'test';
    task.Jeopardy_status = 'test';
    task.User_x = 'test';
   // task.WoInstId = 'test';
    task.TaskName = 'test';
    task.Status = 'test';
    task.TaskOperation = 'test';
    task.ElementType = 'test';
    task.LockElemonCo = 'test';
    task.QueueName = 'test';
    system.assertEquals(true,task!=null); 
    GetServiceNowParamDetails.InvDetailsOutputVO outputVO = new GetServiceNowParamDetails.InvDetailsOutputVO();
    
    GetServiceNowParamDetails.WorkOrder workOrder = new GetServiceNowParamDetails.WorkOrder();
    
    workOrder.WoName = 'test';
    workOrder.ProjectId = 'test';
    workOrder.ElementType = 'test';
    workOrder.ElementName = 'test';
    workOrder.Status = 'test';
   // workOrder.NumTasks = 'test';
   // workOrder.CmpltedTasks = 'test';
    workOrder.Description = 'test';
    workOrder.ElementStatus = 'test';
    workOrder.ElmComplStatus = 'test';
    workOrder.ArchiveElement = 'test';
    workOrder.PendingRevisionStatus = 'test';
    workOrder.CrmOrderNum = 'test';
    workOrder.SharePointURL = 'test';
    system.assertEquals(true,workOrder!=null); 
    GetServiceNowParamDetails.Service_xc service = new GetServiceNowParamDetails.Service_xc();
    
    GetServiceNowParamDetails.Error err = new GetServiceNowParamDetails.Error();
    err.ErrorCode = 'test';
    err.ErrorDescription = 'test';
    
    GetServiceNowParamDetails.Path path = new GetServiceNowParamDetails.Path();
    
    path.aSideCustomer = 'test';
    path.aSideSiteName = 'test';
   // path.NbrChanAssigned = 'test';
    path.Type_x = 'test';
   // path.StartChanNbr = 'test';
   // path.EndChanNbr = 'test';
    path.Decommision = 'test';
    path.Due = 'test';
    path.CircPathHumId = 'test';
    path.InService = 'test';
    path.Installed = 'test';
    path.OrderNum = 'test';
    path.Ordered = 'test';
    path.CustomerId = 'test';
   // path.PathRevNbr = 'test';
    path.ShcedDate = 'test';
    path.Status = 'test';
    path.Topology = 'test';
   // path.WarnThreshPct = 'test';
    path.zSideCustomer = 'test';
    path.zSideSiteId = 'test';
    path.Bandwidth = 'test';
    path.Role = 'test';
   // path.NbrMembers = 'test';
    path.MemberBw = 'test';
    path.Direction = 'test';
    path.BpsAvailable = 'test';
    path.VirtualChans = 'test';
  //  path.MaxPctOversubscription = 'test';
    path.PctUtil = 'test';
    path.ManagementBw = 'test';
    path.ActualSupplyDate = 'test';
    path.CustomerASNumber = 'test';
    path.OrderPlacedWithSupplier = 'test';
    path.PurchaseOrderNumber = 'test';
    path.PurchaseReqNumbers = 'test';
    path.SupplierCommitmentDate = 'test';
    path.SupplierOrderAccepted = 'test';
    path.SupplierTermination = 'test';
    path.InServices = 'test';
    path.CustomerOrderRef = 'test';
    path.LegacyAccount = 'test';
    path.LegacyProductName = 'test';
    path.Migration = 'test';
    path.PrimaryService = 'test';
    path.ProductCode = 'test';
    path.Remedy = 'test';
    path.PrimaryServiceID = 'test';
    path.ShortServiceReference = 'test';
    path.SiteNotes = 'test';
    path.TerminationEffectiveDate = 'test';
    path.TerminationReason = 'test';
    path.AccountName = 'test';
    path.OperationsStatus = 'test';
    path.ContractSignedDate = 'test';
    system.assertEquals(true,path!=null); 
    
    
    }
    }