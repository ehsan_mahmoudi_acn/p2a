/*
    @author - Accenture
    @date - 20- Dec -2012
    @version - 1.0
    @description - This is the test class for InvoiceCreateUpdateController class.
*/

@isTest(SeeAllData = false)
private class InvoiceCreateUpdateControllerTest
{
static Account acc;
static Country_Lookup__c cl;
static BillProfile__c b;
static Site__c s;
static City_Lookup__c ci;
static Contact con;
static Invoice__c inv;
static Invoice_Line_Item__c invli;
Static Invoice_Line_Item__c invLI1;

static testMethod void invoiceCreateUpdateController(){
    b = getBillProfile();
    invli = getInvoiceLineItem(b.Bill_Profile_Integration_Number__c);
    invLI1= getInvoiceLineItem1(b.Bill_Profile_Integration_Number__c);
   system.assert(b!=null);
}


private static Account getAccount(){
    if(acc == null){
        acc = new Account();
        cl = getCountry();
        acc.Name = 'Test Account Test 1';
        acc.Customer_Type__c = 'MNC';
        acc.Country__c = cl.Id;
        acc.Selling_Entity__c = 'Telstra INC';
        acc.Customer_Legal_Entity_Name__c='Test';
        insert acc;
        system.assert(acc!=null);
    }
    return acc;
}
private static Country_Lookup__c getCountry(){
    if(cl == null){
        cl = new Country_Lookup__c();
        cl.CCMS_Country_Name__c = 'India';
        cl.Country_Code__c = 'IND';
        insert cl;
        system.assert(cl!=null);
    }
    return cl;
} 
private static City_Lookup__c getCity(){
    if(ci == null){
        ci = new City_Lookup__c();
        ci.City_Code__c ='MUM';
        ci.Name = 'MUMBAI';
        insert ci;
        system.assert(ci!=null);
    }
    return ci;
}  
    
private static BillProfile__c getBillProfile(){
    if(b == null) {
        b = new BillProfile__c();
        s = getSite();
        con = getContact();
        acc = getAccount();
       // b.Bill_Profile_Number__c = 'Test Bill Profile';
        b.Billing_Entity__c = 'Telstra Limited';
        b.Account__c = s.AccountId__c;
        b.Bill_Profile_Site__c = s.Id;
       // b.Operating_Unit__c = 'TI_HK_I52S';
        b.First_Period_Date__c = date.newinstance(2012, 12, 11);
        b.Invoice_Breakdown__c='Cost Centre';
        b.Start_Date__c =  date.newinstance(2012, 12, 11);
        b.Bill_Profile_Site__c = s.id;
        b.Billing_Contact__c = con.id;
        b.Customer_Ref__c = 'Test';
        
         insert b;
         system.assert(b!=null);
           
    }
        return b;
}
private static Site__c getSite(){
    if(s == null){
        s = new Site__c();
        acc = getAccount();
        s.Name = 'Test_site';
        s.Address1__c = '43';
        s.Address2__c = 'Bangalore';
        cl = getCountry();
        s.Country_Finder__c = cl.Id;
        ci = getCity();
        s.City_Finder__c = ci.Id;
        s.AccountId__c =  acc.Id; 
        s.Address_Type__c = 'Billing Address';
        insert s;
        system.assert(s!=null);
    }
    return s;
}   

private static Contact getContact(){
    if(con == null){
        con = new Contact();
        acc = getAccount();
        con.AccountID = acc.id;
        //con.Name = 'TestCont1';
        con.FirstName = 'Test';
        con.LastName = 'cont1';
        con.Salutation = 'Mr';
        con.Job_Title__c = 'CEO';
        con.Email = 'test.t@test.com';
        con.Country__c = getCountry().id;   
        system.assert(con!=null);
    }
    return con;
}

private static Invoice_Line_Item__c getInvoiceLineItem(String billIntergratedNumber){
        acc = getAccount();
        b = getBillProfile();
        Invoice__c invoice = new Invoice__c();
        invoice.Billing_Account__c = acc.id;
        invoice.Bill_Profile_Name__c = b.id;
        insert invoice;
        invli = new Invoice_Line_Item__c();
        invli.Customer_Account_Number__c = billIntergratedNumber;
        invli.Invoice_Number__c = 'TEST_TEST_123';
        invli.Invoice_Line_Amount__c ='12343';
        invli.Invoice_Line_Number__c = 908909;
        invli.Invoice__c = invoice.id;
        
        insert invli;
        system.assert(invli!=null);
        return invli;
}
private static Invoice_Line_Item__c getInvoiceLineItem1(String billIntergratedNumber){
            acc = getAccount();
        b = getBillProfile();
        Invoice__c invoice = new Invoice__c();
        invoice.Billing_Account__c = acc.id;
        invoice.Bill_Profile_Name__c = b.id;
        insert invoice;

        invLI1 = new Invoice_Line_Item__c();
        invLI1.Customer_Account_Number__c = billIntergratedNumber;
        invLI1.Invoice_Number__c = 'TEST_TEST_123';
        invLI1.Invoice_Line_Amount__c ='12343';
        invLI1.Invoice_Line_Number__c = 9089;
        invLI1.Invoice__c = invoice.id;
        
        insert invLI1;
        system.assert(invLI1!=null);
        return invLI1;
}
@istest
public static void unit1method()
{
List<Account> acclist = P2A_TestFactoryCls.getAccounts(1);
List<Country_Lookup__c> cl = P2A_TestFactoryCls.getcountry(1);
List<Site__c> sitelist = P2A_TestFactoryCls.getsites(1,accList, cl);
List<Contact> conlst = P2A_TestFactoryCls.getContact(1,accList);
List<BillProfile__c> bp =  P2A_TestFactoryCls.getBPs(1,accList,sitelist,conlst);
//bp[0].Bill_Profile_ORIG_ID_DM__c = '6661';
//bp[0].Bill_Profile_ORIG_ID_DM__c = il.Customer_Account_Number__c ;
update bp[0];


List<Invoice__c> invoicelst = new List<Invoice__c>();

Invoice__c i = new Invoice__c();
i.CurrencyIsoCode = 'USD';
i.Billing_Account__c = acclist[0].id; 
//i.BillAmount__c = 2;
//i.Invoice_Number__c = inlst[0].Invoice_Number__c ;
i.Invoice_Number__c = '555' ;
invoicelst.add(i);
insert invoicelst ;
system.assert(invoicelst!=null);

List<Invoice_Line_Item__c> inlst = new List<Invoice_Line_Item__c>();
Invoice_Line_Item__c il = new Invoice_Line_Item__c();
il.Billing_Account__c = acclist[0].id;
il.Invoice_Number__c = '555';
il.Bill_Profile_Name__c = bp[0].id;
il.Billing_Amount__c = 7;
il.Invoice__c = invoicelst[0].id;
il.Customer_Account_Number__c = bp[0].Bill_Profile_Integration_Number__c;

insert il;
system.assert(il!=null);


test.starttest();
//InvoiceCreateUpdateController ic = new InvoiceCreateUpdateController();
InvoiceCreateUpdateController.createUpdateInvoice(inlst);
test.stoptest();
}

}