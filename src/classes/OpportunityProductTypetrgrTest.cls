/**
    @author - Accenture
    @date - 26- Jun-2012
    @version - 1.0
    @description - This is the test class for trg_biu_Opportunity Trigger.
*/
@isTest(SeeAllData=false)
private class OpportunityProductTypetrgrTest {

    static Account acc;
    static Opportunity opp;
    static Country_Lookup__c country;
    static Site__c site;
    static City_Lookup__c ci;
    
    static testMethod void getOppProductType() {
        Opp_Product_Type__c oli = new Opp_Product_Type__c();
        acc=getAccount();
        insert acc;
        opp=getOpportunity();
        opp.AccountId=acc.Id;
        insert opp;
        oli.Opportunity__c=opp.Id;
       try{
        insert oli;        
        } Catch(Exception e){
        
            system.debug('exception'+e.getmessage());
            ErrorHandlerException.ExecutingClassName='TestOpportunityProductTypetrgr:getOppProductType';         
            ErrorHandlerException.sendException(e);
            
        }
        Test.startTest();
      Opp_Product_Type__c oli1 = [SELECT Calculated_New_SOV__c,Reported_New_SOV__c FROM Opp_Product_Type__c WHERE Id =: oli.Id];
      system.debug('===oli1===='+oli1);
      oli1.Reported_New_SOV__c=123.00;
      oli1.MRC__c=100.00;
      oli1.NRC__c=99.00;
      update oli1;
        system.assertEquals(oli1!=null, true);
        
                
        Test.stopTest();
                     
    }
 private static Opportunity getOpportunity(){
        if(opp == null) {
            opp = new Opportunity();
           
            opp.Name = 'Test Opportunity';
           // opp.AccountId = acc.Id;
             //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
            opp.StageName = 'Identify & Define';
            opp.Stage__c='Identify & Define';
            opp.CloseDate = System.today();
              opp.Estimated_MRC__c=800;
            opp.Estimated_NRC__c=1000;
            opp.ContractTerm__c='10';
            opp.Order_Type__c='Termination';
          //  opp.Approx_Deal_Size__c = 9000;
           
        }
        return opp;
    }
    
    public static Account getAccount(){
       
            Account a = new Account();
            a.Name = 'Test Account Test 1';
            a.Customer_Type__c = 'MNC';
            a.Customer_Legal_Entity_Name__c = 'Sample Name';
            a.Selling_Entity__c = 'Telstra INC';
            a.Activated__c = True;
            a.Account_ID__c = '5555';
            a.Account_Status__c = 'Active';
           
        
        return a;
    }
    
}