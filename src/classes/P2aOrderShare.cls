/**
     * Developer: Anuradha
     * Auto Order share.
     * TGP0041763 - Sharing rule implementation
     */
public class P2aOrderShare{
    
    List<csord__Order__Share> OrdShares = new List<csord__Order__Share>();
    List<Opportunity> lstOpp  = new List<Opportunity>();
    
    /** For Each record in the trigger perform the sharing tasks below **/
    public void customShare(List<Csord__Order__c> neworders){
          try{
         // if(AllCaseTriggerHandler.oppId != null && AllCaseTriggerHandler.oppId != '')  
       // lstOpp = [Select Id, OwnerId from Opportunity where Id = :AllCaseTriggerHandler.oppId];
       
           String creator = Schema.csord__Order__Share.RowCause.Creator__c;
           String AccountOwner = Schema.csord__Order__Share.RowCause.Account_Owner__c;
           String OpportunityOwner = Schema.csord__Order__Share.RowCause.Opportunity_Owner__c;
                                            
            for(Csord__Order__c t:neworders){
            
            if(t.Is_opportunity_share__c == false){
                
                /** Create a new Action_Item_Share record to be inserted in to the Action_Item_Share table. **/
                csord__Order__Share Ord_CreatedBy = new csord__Order__Share();
                csord__Order__Share Ord_AccountOwner = new csord__Order__Share();
                csord__Order__Share Ord_OppOwner = new csord__Order__Share();
                
                /** Populate the Order_Share record with the ID of the record to be shared. **/
                if( t.Id != null){
                Ord_CreatedBy.ParentId = t.Id;
                Ord_AccountOwner.ParentId = t.Id;
                Ord_OppOwner.ParentId = t.Id;
                }
                
                /** Share to - Order Creator **/
                if(t.CreatedById != null) 
                Ord_CreatedBy.UserOrGroupId = t.CreatedById;
                /** Specify that the Apex sharing reason **/
                Ord_CreatedBy.RowCause = creator ;
                
                /** Share to - Account Owner **/
                if(t.Account_Owner_Id__c != null) 
                Ord_AccountOwner.UserOrGroupId = t.Account_Owner_Id__c;
                /** Specify that the Apex sharing reason **/
                Ord_AccountOwner.RowCause = AccountOwner ;
                
                 /** Share to - Opportunity Owner **/
                if(t.Opportunity_Owner_ID__c != null) 
                Ord_OppOwner.UserOrGroupId = t.Opportunity_Owner_ID__c;
                /** Specify that the Apex sharing reason **/
                Ord_OppOwner.RowCause = OpportunityOwner  ;
                
                /** Share to - Related Opportunity Owner **/ 
                /*if(lstOpp.size() > 0)
                Ord_OppOwner.UserOrGroupId = lstOpp[0].OwnerId;
                 
                system.debug('AllCaseTriggerHandler.oppId : '+AllCaseTriggerHandler.oppId);
                System.debug('Opportunity_Owner_ID__c '+ t.csordtelcoa__Opportunity__c);
                System.debug('Opportunity_Owner_ID__c '+ t.Opportunity_Owner_ID__c);
                /** Specify that the Apex sharing reason **/
                //Ord_OppOwner.RowCause = OpportunityOwner ; 
                
                
                /** Specify that the AI should have edit/read access for this particular Action_Item record. **/
                Ord_CreatedBy.AccessLevel = 'Edit';
                Ord_AccountOwner.AccessLevel = 'Edit';
                Ord_OppOwner.AccessLevel = 'Read';
                
                /** Add the new Share record to the list of new Share records. **/
                OrdShares.add(Ord_CreatedBy);
                OrdShares.add(Ord_AccountOwner);
                OrdShares.add(Ord_OppOwner);
                
                t.Is_opportunity_share__c = true;//added this line to avoid running the method even after sharing is implemented
            }
        }
        
        /** Insert all of the newly created Share records and capture save result **/
        
            System.debug('OrdSharesbefore'+OrdShares);
            if(OrdShares.size()>0){
                Database.SaveResult[] jobShareInsertResult = Database.insert(OrdShares,false);
                System.debug('OrdShares'+jobShareInsertResult);
                
            }
            System.debug('OrdShares'+OrdShares);
        }catch(exception e){
               ErrorHandlerException.ExecutingClassName='P2aOrderShare:CustomShare';
               ErrorHandlerException.objectList= neworders;
               ErrorHandlerException.sendException(e);
            System.debug('Opportunity Owner Error'+e);
        }
    }
    
}