public class AllTaskTriggerHandler extends BaseTriggerHandler{
@TestVisible
    private Boolean triggerDisabled = false;

    public override Boolean isDisabled(){
        if(Util.muteAllTriggers()){
            return true;
        }
        return triggerDisabled;
    }
    
    
    public override void beforeInsert(List<SObject> newTasks){
       setAssociatedGroupManual((List<Task>)newTasks); 
    }
    
    public override void beforeUpdate(List<SObject> newTasks, Map<Id, SObject> newTasksMap, Map<Id, SObject> oldTasksMap){
        changeRecordTypeToClosed((List<Task>)newTasks);
    }
    
     @testvisible
    public void setAssociatedGroupManual(List<Task> newTasks){
        //logic of setAssociatedGroupManual trigger is moved here to merge triggers into one  
        for (Task t : newTasks)
        {
            String associatedGroup = t.Associated_Group_list__c;
            
            /*if (associatedGroup!=NULL && associatedGroup.length()>0)
            {
                t.CS_GT__Associated_Group__c = associatedGroup;
            }*/
            // Please fill in this once this field is enabled through package
        }
    }

    @testvisible
    public void changeRecordTypeToClosed(List<Task> newTasks){
        for (Task t : newTasks)
        {
            if (t.Status == 'Completed')
            {
                /** query RecordType to get RecordType Object based on request 
                    Sobjecttype
                    Name
                */
                //List<RecordType> recordTypeList = [SELECT Id FROM RecordType WHERE Sobjecttype = 'Task' and Name = 'Closed Task'];
                 //There should only be one response to this so just take the first entry
                //RecordType rt = recordTypeList.get(0);
                //t.recordTypeId = rt.Id;
                t.recordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Closed Task').getRecordTypeId();
               
            }
        }
    
    }
}