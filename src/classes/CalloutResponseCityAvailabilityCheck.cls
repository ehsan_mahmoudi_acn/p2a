global with sharing class CalloutResponseCityAvailabilityCheck extends csbb.CalloutResponseManagerExt {

	global CalloutResponseCityAvailabilityCheck (Map<String, csbb.CalloutResponse> mapCR, csbb.ProductCategory productCategory, csbb.CalloutProduct.ProductResponse productResponse) {
		system.debug('******** CalloutResponseCityAvailabilityCheck ******');
		this.setData(mapCR, productCategory, productResponse);

	}
	global CalloutResponseCityAvailabilityCheck () {
	}

	global void setData(Map<String, csbb.CalloutResponse> mapCR, csbb.ProductCategory productCategory, csbb.CalloutProduct.ProductResponse productResponse) {

		this.service = 'CityAvailabilityCheck'; 
		this.productCategoryId = productCategory.productCategoryId; 
		this.mapCR = mapCR;
		this.productCategory = productCategory; 
		this.productResponse = productResponse; 
		this.setPrimaryCalloutResponse();

	}

	global Map<String, Object> processResponseRaw (Map<String, Object> inputMap) {
		return new Map<String, Object>();
	}

	global Map<String, Object> getDynamicRequestParameters (Map<String, Object> inputMap) {
		return new Map<String, Object>();
	}

	global void runBusinessRules (String categoryIndicator) { 
		system.debug('******** CalloutResponseCityAvailabilityCheck ****** categoryIndicator - '+categoryIndicator);
		this.productResponse.displayMessage = 'Sample message from CityAvailabilityCheck'; 
		this.productResponse.available = 'true';
		String resultJson = csbb.CalloutDisplay.takeString(crPrimary, 'Envelope.Body.doWorkResponse.result');
		
		Map<String, String> resultMap = (Map<String, String>)JSON.deserialize(resultJson, Map<String, String>.class);
		Id objectId = resultMap.get('id');

		if(objectId.getSobjectType() == Schema.CS_POP__c.SObjectType){
			this.crPrimary.mapDynamicFields.put('city', resultMap.get('city'));
			this.crPrimary.mapDynamicFields.put('id', resultMap.get('id'));
			this.crPrimary.mapDynamicFields.put('country', resultMap.get('country'));
			this.crPrimary.mapDynamicFields.put('connectivity', resultMap.get('connectivity'));
		}
		system.debug('******** CalloutResponseCityAvailabilityCheck ****** this.crPrimary.mapDynamicFields - '+this.crPrimary.mapDynamicFields);
	}

	global csbb.Result canOffer (Map<String, String> attMap, Map<String, String> responseFields, csbb.CalloutProduct.ProductResponse productResponse) {

		csbb.Result canOfferResult = new csbb.Result(); 
		canOfferResult.status = 'OK';
		
		return canOfferResult;
	}

}