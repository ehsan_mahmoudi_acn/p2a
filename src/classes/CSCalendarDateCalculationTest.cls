@isTest(SeeAllData = false)
private class CSCalendarDateCalculationTest {

	private static testMethod void test() {
        Exception ee = null;
        
        try{
            Datetime startTime = Datetime.newInstance(2016,11,26,11,23,11);
            Datetime endTime = Datetime.newInstance(2016,11,26,11,23,11);
            Datetime result;
            Integer duration = 20;
            
            Test.startTest();
            
            result = CSCalendarDateCalculation.calculateEndDate(startTime, duration);
            System.assert(result > startTime, 'Endtime is not bigger than startTime');
            System.debug('EndTime: ' + result + ' is bigger then StartTime: ' + startTime);
            
            result = CSCalendarDateCalculation.calculateStartDate(endTime, duration);
            System.assert(result < endTime, 'StartTime is not smaller then EndTime');
            System.debug('StartTime: ' + result + ' is smaller then EndTime: ' + endtime);
            
        } catch(Exception e){
            ee = e;
        } finally {
            Test.stopTest();
            if(ee != null){
                throw ee;
            }
        }     
	}

}