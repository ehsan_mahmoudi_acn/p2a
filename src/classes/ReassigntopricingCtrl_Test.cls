@isTest
Public class ReassigntopricingCtrl_Test{
    Public static testmethod void reassighningpricingpmeth()
    {
        List<Account> Acclist =  P2A_TestFactoryCls.getaccounts(1);
        System.assert(Acclist[0].id!=null);
        
        list<Opportunity> Opp = P2A_TestFactoryCls.getOpportunitys(1,Acclist);
        System.assert(Opp[0].id!=null);
              
        List<case> Caselist= P2A_TestFactoryCls.getcase(1, AccList);
                   System.assert(Caselist[0].id!=null);

        List<User> userList = P2A_TestFactoryCls.get_Users(1);
        
        ReassigntopricingCtrl repric = new ReassigntopricingCtrl(new ApexPages.StandardController(caselist[0]));
        repric.reAssigntopriceMtd();
        
        
        list<Case> Updatecases = new List<Case>(); 
        Case upcase = new Case();
        upcase.id = caselist[0].id;
        upcase.Comments__c = 'Margin is high'; 
        upcase.Is_Reassign_to_Pricing__c = false;
        upcase.Is_Reassign_to_Sales__c   = true;
        upcase.status='Assighned';
        upcase.Comments_Prior__c = 'high';
        upcase.Assigned_To__c = userlist[0].id;
        upcase.Ownerid = userList[0].id;
        updatecases.add(upcase);
        update updatecases;
        System.assert(Updatecases[0].id!=null);
     
        //ReassigntopricingCtrl repric1 = new ReassigntopricingCtrl(new ApexPages.StandardController(updatecases[0]));
        ApexPages.StandardController sc = new ApexPages.StandardController(caselist[0]);
        reassigntopricingCtrl repric1 = new reassigntopricingCtrl(sc);
        repric1.reAssigntopriceMtd();
        system.assert(updatecases[0].status=='Assighned');
        
              
    }
    Public static testmethod void reassighningpricingpmeth1()
    {
        List<Account> Acclist =  P2A_TestFactoryCls.getaccounts(1);
        System.assert(Acclist[0].id!=null);
        
        list<Opportunity> Opp = P2A_TestFactoryCls.getOpportunitys(1,Acclist);
        System.assert(Opp[0].id!=null);
              
        List<case> Caselist= P2A_TestFactoryCls.getcase(1, AccList);
                   System.assert(Caselist[0].id!=null);

        List<User> userList = P2A_TestFactoryCls.get_Users(1);
        
        ReassigntopricingCtrl repric = new ReassigntopricingCtrl(new ApexPages.StandardController(caselist[0]));
        repric.reAssigntopriceMtd();
        
        
        list<Case> Updatecases = new List<Case>(); 
        Case upcase = new Case();
        upcase.id = caselist[0].id;
        upcase.Comments__c = 'Margin is high'; 
        upcase.Is_Reassign_to_Pricing__c = true;
        upcase.Is_Reassign_to_Sales__c   = true;
        upcase.status='Assighned';
        upcase.Comments_Prior__c = 'high';
        upcase.Ownerid = userList[0].id;
        updatecases.add(upcase);
        test.starttest();
        update updatecases;
        System.assert(Updatecases[0].id!=null);
     
        //ReassigntopricingCtrl repric1 = new ReassigntopricingCtrl(new ApexPages.StandardController(updatecases[0]));
        ApexPages.StandardController sc = new ApexPages.StandardController(caselist[0]);
        reassigntopricingCtrl repric1 = new reassigntopricingCtrl(sc);
        repric1.reAssigntopriceMtd();
        test.stoptest();
        system.assert(updatecases[0].status=='Assighned');
    }
}