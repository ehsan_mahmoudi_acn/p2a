@isTest(SeeAllData = false)
private class PriceItemQueryNewTest {

    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    
    private static List<Account> accountList;
    private static List<cspmb__Price_Item__c> priceItemList;
    private static List<CS_Route_Segment__c> routeSegmentList;
    private static List<CS_Bandwidth__c> bandwidthList;
    private static List<CS_Bandwidth_Product_Type__c> bandwidthProdTypeList;
    private static List<CS_Route_Bandwidth_Product_Type_Join__c> routeBandwidthProductTypeJoinList;
    
    private static void initTestData(){

        accountList = new List<Account>{
            new Account(Name = 'Account 1', Customer_Type_New__c = 'MNC'),
            new Account(Name = 'Account 2', Customer_Type_New__c = 'MNC')
        };
        
        insert accountList;
          System.assertEquals('Account 1',accountList[0].Name );   
        
        routeSegmentList = new List<CS_Route_Segment__c>{
            new CS_Route_Segment__c(Name = 'Route Segment 1', Product_Type__c = 'IPL'),
            new CS_Route_Segment__c(Name = 'Route Segment 2', Product_Type__c = 'IPL')
        };
        
        insert routeSegmentList;
          System.assertEquals('Route Segment 1',routeSegmentList[0].Name );
        
        bandwidthList = new List<CS_Bandwidth__c>{
            new CS_Bandwidth__c(Name = '64k', Bandwidth_Value_MB__c = 64),
            new CS_Bandwidth__c(Name = '128k', Bandwidth_Value_MB__c = 128)
        };
        
        insert bandwidthList;
          System.assertEquals('64k',bandwidthList[0].Name );
        
        bandwidthProdTypeList = new List<CS_Bandwidth_Product_Type__c>{
            new CS_Bandwidth_Product_Type__c(Name = 'Bandwidth Product Type 1', CS_Bandwidth__c = bandwidthList[0].Id, Product_Type__c = 'IPL'),
            new CS_Bandwidth_Product_Type__c(Name = 'Bandwidth Product Type 2', CS_Bandwidth__c = bandwidthList[1].Id, Product_Type__c = 'IPL')
        };
        
        insert bandwidthProdTypeList;  
        System.assertEquals('Bandwidth Product Type 1',bandwidthProdTypeList[0].Name );   
        
        routeBandwidthProductTypeJoinList = new List<CS_Route_Bandwidth_Product_Type_Join__c>{
            new CS_Route_Bandwidth_Product_Type_Join__c(Name = 'Product Type Join 1', CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[0].Id, CS_Route_Segment__c = routeSegmentList[0].Id),
            new CS_Route_Bandwidth_Product_Type_Join__c(Name = 'Product Type Join 2', CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[1].Id, CS_Route_Segment__c = routeSegmentList[1].Id)
        };
        
        insert routeBandwidthProductTypeJoinList;
            System.assertEquals('Product Type Join 1',routeBandwidthProductTypeJoinList[0].Name );
        
        priceItemList = new List<cspmb__Price_Item__c>{
            new cspmb__Price_Item__c(Name = 'Price Item 1', CS_Route_Bandwidth_Product_Type_Join__c = routeBandwidthProductTypeJoinList[0].Id, cspmb__Account__c = accountList[0].Id),
            new cspmb__Price_Item__c(Name = 'Price Item 2', CS_Route_Bandwidth_Product_Type_Join__c = routeBandwidthProductTypeJoinList[1].Id, cspmb__Account__c = null)
        };
        
        insert priceItemList;
            System.assertEquals('Price Item 1',priceItemList[0].Name );
        
    }

    private static testMethod void doDynamicLookupSearchTest() {
        Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
            
            initTestData();
            
            searchFields.put('Bandwidth', bandwidthList[0].Id);
            searchFields.put('Account Id', accountList[0].Id);
            searchFields.put('RateCardIDTemp', routeSegmentList[0].Id);
            searchFields.put('Selected Product Type', 'IPL');
            
            PriceItemQueryNew priceItemQuery = new PriceItemQueryNew();
            Object[] data = priceItemQuery.doDynamicLookupSearch(searchFields, productDefinitionId);
            String reqAtts = priceItemQuery.getRequiredAttributes();
            
            System.debug('*******Data: ' + data);
            System.assert(data.size() > 0, '');
            
            data.clear();
            searchFields.put('Account Id', accountList[1].Id);
            searchFields.put('RateCardIDTemp', routeSegmentList[1].Id);
            searchFields.put('Bandwidth', bandwidthList[1].Id);
            
            data = priceItemQuery.doDynamicLookupSearch(searchFields, productDefinitionId);
            
            System.debug('*******Data: ' + data);
            System.assert(data.size() > 0, '');
            Test.stopTest();
        } catch(Exception e){
            ee = e;
        } finally {
           // Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
    }

}