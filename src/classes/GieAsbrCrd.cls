global with sharing class GieAsbrCrd extends cscfga.ALookupSearch {

public override String getRequiredAttributes(){ 
        return '[ "LinkedGIEs" ]';
    }


   public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){
        String linkedGIE = searchFields.get('LinkedGIEs');
        List<Id> GIEIds = new List<Id>();
        for( String s : linkedGIE.split(',')){
            GIEIds.add(Id.valueof(s));
        }
        
        List<cscfga__Product_Configuration__c> CRDList = [select id,Customer_Required_Date__c from cscfga__Product_Configuration__c where Id IN :GIEIds];
    
        CRDList.sort();
        
        List<cscfga__Product_Configuration__c> asbrcrd = [select id,Customer_Required_Date__c from cscfga__Product_Configuration__c where id =: CRDList[0].id];
        
        return asbrcrd;
        }
    }