public Class CreditCardCheckActionItem{
public Static Integer OppCount=0;
Public Static Boolean flag=false;
public void  creditcheckCreation(Set<Opportunity> OppObj ){
Map<ID,ID> accountOppMapId = new Map<ID,ID>();
      
       // query Opportunity to get Opportunity Object based on updated Opportunity
         if(OppObj != null && flag == false){ 
         
         Opportunity opp2  = [Select Id,Order_Type__c,ownerid ,CurrencyIsoCode,Region__c,AccountId,Account.Account_ID__c,Account.AccountNumber,Account.Customer_Group__c,ContractTerm__c,Total_Contract_Value__c,Total_MRC__c,Total_NRC__c,Account.Credit_Limit__c,Account.credit_limit_expiry_date__c,Account.type,Account.Business_Registration__c, By_Pass_Renewal_Credit_Check__c from Opportunity where Id=:OppObj limit 1];
                accountOppMapId.put(opp2.id,opp2.AccountId);
                Action_Item__c action = new Action_Item__c();
                action = Util.createActionItem('Request Credit Check', accountOppMapId, opp2.id, null);
                action.Subject__c = 'Request Credit Check';
                action.Region__c = opp2.Region__c;
                //Querrying current user and checking for the right queue
                List<User> userLst = [select id,name,Region__c from user where Id =:UserInfo.getUserId()];
                User userid = userLst[0];
                Util.assignOwnersToRegionalQueue(userid,action,'Request Credit Check');
                action.Feedback__c = '';
         
                // Check Previous Credit check Date
                action.Account_Number__c= opp2.Account.Account_ID__c;
              
                //Setting the values from Opportunity to Action Item
                action.Total_Contract_Value__c = opp2.Total_Contract_Value__c;
                action.Contract_Term__c = opp2.ContractTerm__c;
                action.MRC_Monthly_Recurring_Charges__c = opp2.Total_MRC__c;
                action.NRC_Non_Recurring_Charges__c= opp2.Total_NRC__c;
                action.Business_Registration_Number__c = opp2.Account.Business_Registration__c;
                action.Billing_Currency__c = opp2.CurrencyIsoCode;
                action.CurrencyIsoCode = opp2.CurrencyIsoCode;
                action.creditlimit__c = opp2.Account.credit_limit__c;
                action.Opportunity_related_user__c =opp2.ownerid;
                action.credit_limit_expiry_date__c = opp2.Account.credit_limit_expiry_date__c;
                if(opp2.Account.type == 'Prospect')
                    action.new_existing_customer__c = 'New';
                else
                    action.new_existing_customer__c = 'Existing';   
                
                List<Action_Item__c> lastAction=null;
               List<Action_Item__c> previousActionItem = new List<Action_Item__c>();       
                        
                try{
                    
                     lastAction= [Select Id,Name,Account_Number__c,Opportunity__c,Status__c,Feedback__c,Account__r.Account_ID__c,LastModifiedDate,MRC_Monthly_Recurring_Charges__c,NRC_Non_Recurring_Charges__c,Total_Estimated_MRC__c,Total_Estimated_NRC__c from Action_Item__c where Opportunity__c=:opp2.id  and RecordTypeId=:action.RecordTypeId order by LastModifiedDate Desc];
                           system.debug('lastAction===='+lastAction);       
                    }catch(Exception e){
                        lastAction=null;
                    }
                if (lastAction!=null){
                    if (lastAction.size()!=0){
                        action.Previous_Credit_Check_Date__c=Date.newInstance(lastAction.get(0).LastModifiedDate.year(),lastAction.get(0).LastModifiedDate.month(),lastAction.get(0).LastModifiedDate.day());
                        action.Action_Item_ID_Previous_Credit_Check__c=lastAction[0].Name;                    
                        action.Previous_MRC__c=lastAction[0].MRC_Monthly_Recurring_Charges__c;
                        action.Previous_NRC__c=lastAction[0].NRC_Non_Recurring_Charges__c;
                        action.Previous_Estimated_MRC__c=lastAction[0].Total_Estimated_MRC__c;
                        action.Previous_Estimated_NRC__c=lastAction[0].Total_Estimated_NRC__c;
                        if(action.MRC_Monthly_Recurring_Charges__c != null && lastAction[0].MRC_Monthly_Recurring_Charges__c != null){
                            action.Difference_in_MRC__c=action.MRC_Monthly_Recurring_Charges__c-lastAction[0].MRC_Monthly_Recurring_Charges__c;
                        }
                        if(action.NRC_Non_Recurring_Charges__c != null && lastAction[0].NRC_Non_Recurring_Charges__c != null){
                            action.Difference_in_NRC__c=action.NRC_Non_Recurring_Charges__c-lastAction[0].NRC_Non_Recurring_Charges__c;
                        }

                     // action.feedback__c = lastAction[0].Feedback__c;
                        previousActionItem.add(lastAction[0]);
                        //system.debug('++++++++++previousActionItem========'+previousActionItem);
                    }
                }
                  List<Opportunity> openOpportunities = new List<Opportunity>();
                    try{
                      //added Sales_Status__c to the query and updated stage name closed won and closed lost as per Somp requirement
                     openOpportunities =[Select Total_MRC__c,CurrencyIsoCode,Order_Type__c,Accountid,Total_NRC__c,Account_Number__c,StageName,Sales_Status__c from Opportunity where AccountId =:opp2.AccountId and (StageName!='Prove & Close' and Sales_Status__c!='Won') and Sales_Status__c!='Lost'];
                            //system.debug('openOpportunities===if=='+openOpportunities.size());
                    }catch(Exception e){
                            openOpportunities=null;
                            //system.debug('openOpportunities======'+openOpportunities.size());                                         
                    }
                    if (openOpportunities.size()>0){
                        //Here getting all currencies type and form Map key as Currencycode and value as CurrencyType Object
                        //List<CurrencyType> currencyTypeList = new List<CurrencyType>();
                        Map<String,CurrencyType> isoCodeCurrencyTypeObjMap = new Map<String,CurrencyType>();
                        //currencyTypeList = [Select id,ConversionRate,IsCorporate,IsoCode from CurrencyType];
                        
                        for(CurrencyType currencyTypeObj : [Select id,ConversionRate,IsCorporate,IsoCode from CurrencyType]){
                            isoCodeCurrencyTypeObjMap.put(currencyTypeObj.IsoCode,currencyTypeObj);
                        }
                        
                        Double Mrcvalues=0;
                        Double NrcValues=0;
                            for(Opportunity opp1:openOpportunities){
                                    Double currencyExchangeRateValue = isoCodeCurrencyTypeObjMap.get(opp1.CurrencyIsoCode).ConversionRate;
                                   if(opp1.Total_MRC__c != null ){
                                     Mrcvalues += (opp1.Total_MRC__c/currencyExchangeRateValue).setscale(2);
                                     }
                                   if(opp1.Total_NRC__c != null){
                                     NrcValues += (opp1.Total_NRC__c/currencyExchangeRateValue).setscale(2);
                                   }    
                            }
                            action.Total_MRC_for_all_Open_Opportunities__c = Mrcvalues;
                            action.Total_NRC_for_all_Open_Opportunities__c = NrcValues;
                            //system.debug('Total MRC =========' + action.Total_MRC_for_all_Open_Opportunities__c);
                            //system.debug('Total NRC =========' + action.Total_NRC_for_all_Open_Opportunities__c);       
                   }                        
                   System.debug('testttttttttttt'+action);
            //if( opp2.Order_Type__c != 'Termination' && opp2.Order_Type__c != 'Relocation' ){  
                if( opp2.Order_Type__c != 'Termination' && opp2.Order_Type__c != 'Relocation' && !(opp2.Order_Type__c == 'Renewal' && opp2.By_Pass_Renewal_Credit_Check__c == true) ){
                 if(lastAction.size() == 0){
                     insert action;
                     flag = true;
                }
                else if(lastAction.size() != 0 && lastAction[0].feedback__c == 'Approved' && (opp2.Total_NRC__c > lastAction[0].NRC_Non_Recurring_Charges__c || opp2.Total_MRC__c > lastAction[0].MRC_Monthly_Recurring_Charges__c)){
                     insert action;
                     flag = true;
                 }
               }      
            }              
        }
     public void  previousCreditcheckupdation(Set<Opportunity> OppObj ){
        
        // query Opportunity to get Opportunity Object based on updated Opportunity
         if(OppObj != null){ 
         Opportunity opp2  = [Select Id,Order_Type__c,CurrencyIsoCode,Region__c,AccountId,Account.Account_ID__c,Account.AccountNumber,Account.Customer_Group__c,ContractTerm__c,Total_Contract_Value__c,Total_MRC__c,Total_NRC__c,Account.Business_Registration__c from Opportunity where Id=:OppObj limit 1];
               List<Action_Item__c> lastAction=null; 
               List<Action_Item__c> previousActionItem = new List<Action_Item__c>();  
           RecordType rt = [select Id,Name from RecordType where Name='Request Credit Check']; 
            Action_Item__c action = new Action_Item__c();
            action.recordtypeId = rt.id;            
                        
                try{                    
                     lastAction= [Select Id,Name,Account_Number__c,Opportunity__c,status__c,Feedback__c,Account__r.Account_ID__c,LastModifiedDate,MRC_Monthly_Recurring_Charges__c,NRC_Non_Recurring_Charges__c,Total_Estimated_MRC__c,Total_Estimated_NRC__c from Action_Item__c where Opportunity__c=:opp2.id  and RecordTypeId=:action.RecordTypeId and Status__c != 'Reversed' order by LastModifiedDate Desc];
                           system.debug('lastAction=1111==='+lastAction);       
                    }catch(Exception e){
                        lastAction=null;
                    }
                if(lastAction.size() != 0 && lastAction[0].feedback__c == 'Approved' && flag == false && (opp2.Total_NRC__c > lastAction[0].NRC_Non_Recurring_Charges__c || opp2.Total_MRC__c > lastAction[0].MRC_Monthly_Recurring_Charges__c)){
                    lastAction[0].status__c = 'Reversed';
                    update lastAction[0];
                    system.debug('lastAction=1111==='+lastAction[0]);  
              }

        }
    }              
 }