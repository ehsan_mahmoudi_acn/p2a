global  class TriggerBilling {
    @future (callout=true)
    
    webservice static void sendBillingdata(String serviceId){
        
        set<String> services = new set<String>();
        set<id> servicesId = new set<id>();
        set<id> opp= new set<id>();
        Boolean Bundled;
        Boolean Bundledflag = false;
        String ParentId='';
        List<csord__Service_Line_Item__c> serviceLineItemQueried = new List<csord__Service_Line_Item__c>();
        // csord__Subscription__c subscription = [SELECT Id, Account_Manager__c, Bundle_Label__c, Parent_of_the_bundle_flag__c, Bundle_Action__c FROM csord__Subscription__c WHERE Id = :subscriptionId];
        LIST<csord__Service__c> lineItems = new list<csord__Service__c>();
        csord__Service__c lineItemsparent = new csord__Service__c();
        List<csord__Service__c> lineItemsparentList = new List<csord__Service__c>();
        
        /*csord__Service__c[] serviceItems = [SELECT id,Product__r.Name,Product_Configuration_Type__c,Ordertype__c,
                                                   OLI_Type__c,csordtelcoa__Service_Number__c,Order_Date__c,Billable_Flag__c,
                                                   ResourceId__c, ROC_Line_Item_Status__c,Bundle_Flag__c,Parent_Bundle_Flag__c,
                                                   Parent_ServiceID__c,Telstra_Billing_Entity__c, Product_Id__c, 
                                                   BillingProfileID__c, Root_Product_ID__c, Countries__c, 
                                                   Purchase_Order_Date__c, Contract_Sign_Date__c, Order_Type__c, 
                                                   Primary_Service_ID__c,Bundle_Label_name__c,Bill_Activation_Flag__c,
                                                   Generate_Credit_Flag__c,csordtelcoa__Parent_Product_Configuration__r.Name,
                                                   Parent_Bundle_ID__c,Parent_Customer_PO__c,Bundle_Action__c,
                                                   Account_Managers__c, Product_Code__c,Opportunity__c, 
                                                   Contract_Duration__c, Root_Bill_Text__c, Service_Bill_Text__c, 
                                                   Bandwidth__c,csord__Order__r.Is_InFlight_Cancel__c, Service_Type__c,
                                                   Port_Type__c,Do_you_want_to_associate_a_UIA__c,
                                                   csord__Order__r.Is_Terminate_Order__c,Stop_Billing_Date__c,
                                                   Billing_Commencement_Date__c 
                                            FROM csord__Service__c 
                                            WHERE id = :serviceId];*/
        //system.debug('=====serviceItems ===='+serviceItems);
        for(csord__Service__c cservice : [SELECT id,Product__r.Name,Product_Configuration_Type__c,Ordertype__c,
                                                   OLI_Type__c,csordtelcoa__Service_Number__c,Order_Date__c,Billable_Flag__c,
                                                   ResourceId__c, ROC_Line_Item_Status__c,Bundle_Flag__c,Parent_Bundle_Flag__c,
                                                   Parent_ServiceID__c,Telstra_Billing_Entity__c, Product_Id__c, 
                                                   BillingProfileID__c, Root_Product_ID__c, Countries__c, 
                                                   Purchase_Order_Date__c, Contract_Sign_Date__c, Order_Type__c, 
                                                   Primary_Service_ID__c,Bundle_Label_name__c,Bill_Activation_Flag__c,
                                                   Generate_Credit_Flag__c,csordtelcoa__Parent_Product_Configuration__r.Name,
                                                   Parent_Bundle_ID__c,Parent_Customer_PO__c,Bundle_Action__c,
                                                   Account_Managers__c, Product_Code__c,Opportunity__c, 
                                                   Contract_Duration__c, Root_Bill_Text__c, Service_Bill_Text__c, 
                                                   Bandwidth__c,csord__Order__r.Is_InFlight_Cancel__c, Service_Type__c,
                                                   Port_Type__c,Do_you_want_to_associate_a_UIA__c,
                                                   csord__Order__r.Is_Terminate_Order__c,Stop_Billing_Date__c,
                                                   Billing_Commencement_Date__c 
                                            FROM csord__Service__c 
                                            WHERE id = :serviceId])
        {
            //system.debug('********Product Configuration: ' + cservice.Product_Configuration_Type__c);
            if(cservice.Bundle_Flag__c == true && cservice.Parent_Bundle_Flag__c == true)
            {
                services.add(cservice.Bundle_Label_name__c);
                opp.add(cservice.Opportunity__c);
                ParentId=cservice.id;
            }
            else if(cservice.Product_Configuration_Type__c != 'New Provide' && cservice.Bundle_Flag__c == true && cservice.Parent_Bundle_Flag__c == false)
            {
                services.add(cservice.Bundle_Label_name__c);
                opp.add(cservice.Opportunity__c);
                Bundledflag = true;
            }
            else{
                if(cservice.Bundle_Flag__c == false){
                    lineItems.add(cservice);
                    servicesId.add(cservice.id);
                }
            }
        }
        
        if(!services.isEmpty())
        {
            lineItems = [SELECT id,csord__Status__c,Product__r.Name,Product_Configuration_Type__c,Ordertype__c,
                                ROC_Line_Item_Status__c,csordtelcoa__Service_Number__c,Order_Date__c,OLI_Type__c,
                                Billable_Flag__c,ResourceId__c, Bundle_Flag__c,Parent_Bundle_Flag__c,Parent_ServiceID__c, 
                                Product_Id__c,Telstra_Billing_Entity__c, BillingProfileID__c, Root_Product_ID__c, 
                                Countries__c, Purchase_Order_Date__c, Contract_Sign_Date__c, Order_Type__c,Opportunity__c,
                                Primary_Service_ID__c,Bundle_Label_name__c,Bill_Activation_Flag__c,Generate_Credit_Flag__c,
                                csordtelcoa__Parent_Product_Configuration__r.Name,Parent_Bundle_ID__c,Parent_Customer_PO__c,
                                Bundle_Action__c,Account_Managers__c, Product_Code__c, Contract_Duration__c, Root_Bill_Text__c, 
                                Service_Bill_Text__c, Bandwidth__c,csord__Order__r.Is_InFlight_Cancel__c, Service_Type__c,
                                Port_Type__c,Do_you_want_to_associate_a_UIA__c,csord__Order__r.Is_Terminate_Order__c,
                                Stop_Billing_Date__c,Billing_Commencement_Date__c 
                         FROM csord__Service__c 
                         WHERE Bundle_Flag__c = true And 
                               Bundle_Label_name__c in :services and 
                               ((Opportunity__c in:opp) or(Order_type__c=:'Terminate' and 
                               (csord__Status__c!='Cancelled'or csord__Status__c!='Rejected')))];
            //system.debug('####lineItems'+lineItems);
            for(csord__Service__c cservices : lineItems)
            {
                //system.debug('####cservices.Parent_Bundle_Flag__c'+cservices.Parent_Bundle_Flag__c);
                if(cservices.Parent_Bundle_Flag__c == true)
                {
                    Bundled = true;
                    break;
                }
                else
                {
                    Bundled = false;
                }
            }
            //system.debug('####Bundled '+Bundled );
            if(Bundled == true)
            {
                for(csord__Service__c cservicespar : lineItems)
                {
                    servicesId.add(cservicespar.id);
                    //system.debug('####'+servicesId);
                } 
            }
            
            //Only for Test Class run - Start
            /*if(Test.isRunningTest()){
                for(String str : services){
                    if(str == NULL)
                        services.remove(str);
                }
                services.add('Test Bundle');
            }*/
            //Only for Test Class run - End
            System.debug('services: ' + services);
            if(Bundled == false)   
            {
                lineItemsparentList = [SELECT id,Product__r.Name,Product_Configuration_Type__c,Ordertype__c,ROC_Line_Item_Status__c,
                                          csordtelcoa__Service_Number__c,Order_Date__c,OLI_Type__c,Billable_Flag__c,
                                          ResourceId__c, Bundle_Flag__c,Parent_Bundle_Flag__c,Parent_ServiceID__c, 
                                          Product_Id__c,Telstra_Billing_Entity__c, BillingProfileID__c, Root_Product_ID__c, 
                                          Countries__c, Purchase_Order_Date__c, Contract_Sign_Date__c, Order_Type__c,
                                          Opportunity__c,Primary_Service_ID__c,Bundle_Label_name__c,Bill_Activation_Flag__c,
                                          Generate_Credit_Flag__c,csordtelcoa__Parent_Product_Configuration__r.Name,
                                          Parent_Bundle_ID__c,Parent_Customer_PO__c,Bundle_Action__c,Account_Managers__c, 
                                          Product_Code__c, Contract_Duration__c, Root_Bill_Text__c, Service_Bill_Text__c, 
                                          Bandwidth__c,csord__Order__r.Is_InFlight_Cancel__c, Service_Type__c,Port_Type__c,
                                          Do_you_want_to_associate_a_UIA__c,csord__Order__r.Is_Terminate_Order__c,
                                          Stop_Billing_Date__c,Billing_Commencement_Date__c 
                                   FROM csord__Service__c 
                                   WHERE Bundle_Flag__c = true And Bundle_Label_name__c in :services And Parent_Bundle_Flag__c = true order by LastModifiedDate desc limit 1];
                //system.debug('@@@lineItemsparent '+lineItemsparent );
                if(lineItemsparentList != NULL && lineItemsparentList.size() > 0)
                    lineItemsparent = lineItemsparentList[0];
                if(lineItemsparent != null)
                {
                    servicesId.add(lineItemsparent.id);
                    lineItems.add(lineItemsparent);
                }
                for(csord__Service__c cservicespar : lineItems)
                {
                    servicesId.add(cservicespar.id);
                    
                } 
                //system.debug('####servicesId'+servicesId);
            }           
        }
        
        List<TibcoComSchemasXsdgenerationBill.Service_element> serviceElements = new List<TibcoComSchemasXsdgenerationBill.Service_element>();
        
        User u = [select id,firstname,EmployeeNumber from user where id=:userinfo.getuserid()]; 
        
        if(!servicesId.isEmpty())
        {
            serviceLineItemQueried = [select id,csord__Service__r.Order_Type__c,MISC_Charge_Amount__c,
                                             csord__Is_Recurring__c,Charge__c,Is_ETC_Line_Item__c,csord__Service__c,Country__c,
                                             Bill_Profile_Integration_Number__c,Pin_Service_ID__c,Parent_Bundle_ID__c,
                                             Account_ID__r.Country__c,Billing_Entity__c,AEndCountryISOCode__c,
                                             Termination_Date__c,Bill_Profile__c,Root_Bill_Text__c,Parent_ServiceID__c,
                                             CurrencyISOCode,NRC_Bill_Text__c ,RC_Credit_Bill_Text__c,Bill_Text__c,
                                             Bill_Texts__c ,NRC_Credit_Bill_Text__c ,AEndZipPlus4__c,
                                             Align_with_Billing_Cycle__c,BEndCountryISOCode__c,BEndZipPlus4__c,
                                             MRC_Price_Changed__c ,Billing_Commencement_Date__c,Recurring_Credit_Amount__c,
                                             Charge_Amount__c,Charge_Amounts__c,Cost__c,Charge_IDs__c,Zero_Charge_Flag__c,
                                             Parent_of_Bundle__c,Charging_Frequency__c,Charge_ID__c,NRC_Price__c,
                                             Cost_Center_Code__c,Credit_Approved_By__c,Credit_Duration__c,
                                             Display_line_item_on_Invoice__c,In_Advance_or_In_arrears__c,
                                             Installment_NRC_End_Date__c,Installment_NRC_Start_Date__c,
                                             Is_Miscellaneous_Credit_Flag__c,pivot_date__c,Path_Id__c,Site_A_For_ROC__c,
                                             Site_B_For_ROC__c,Stop_Billing_Date__c,U2C_Master_Code__c,Charge_Action__c,
                                             Type_of_Charge__c,Type_of_Miscellaneous_Credit__c,Zero_Charge_MRC_Flag__c,
                                             csord__Service__r.Product_Configuration_Type__c,csord__Service__r.Usage_Flag__c 
                                      from csord__Service_Line_Item__c 
                                      where csord__Service__c in :servicesId and Is_Miscellaneous_Credit_Flag__c=false and 
                                            (NOT Name like '%Burst%')];
        }
        
        
        string riteshlineitem='';//ritesh change
        for (csord__Service__c lineItem : lineItems)
        {
            List<TibcoComSchemasXsdgenerationBill.ServiceLineItem_element> serviceLineElements = new List<TibcoComSchemasXsdgenerationBill.ServiceLineItem_element >();
            riteshlineitem+=lineitem.Primary_Service_ID__c+' ';
            
            for(csord__Service_Line_Item__c serviceLineItemElement : serviceLineItemQueried )
            {
                if(lineItem.id == serviceLineItemElement.csord__Service__c && 
                   (serviceLineItemElement.csord__Service__r.Order_Type__c!='Terminate' || 
                   (serviceLineItemElement.csord__Service__r.Order_Type__c=='Terminate' && 
                   lineItem.Bundle_Flag__c==false && (serviceLineItemElement.Is_ETC_Line_Item__c || 
                   serviceLineItemElement.csord__Is_Recurring__c)) || 
                   (serviceLineItemElement.csord__Service__r.Order_Type__c=='Terminate' && 
                   lineItem.Bundle_Flag__c)))//Suparna:ETC:IP006
                {
                    riteshlineitem+=serviceLineItemElement.Charge_ID__c+''; 
                    //system.debug('@@@@@ashishcheck'+serviceLineItemElement.csord__Service__c+'ashishaiha insideloop');     
                    TibcoComSchemasXsdgenerationBill.ServiceLineItem_element serviceLineElement = new TibcoComSchemasXsdgenerationBill.ServiceLineItem_element ();
                    /*ETC/IP006/Defect#12142*/
                    if(serviceLineItemElement.Is_ETC_Line_Item__c){
                        serviceLineElement.ChargeValue = serviceLineItemElement.MISC_Charge_Amount__c;
                    }else if(serviceLineItemElement.csord__Service__r.Order_Type__c=='Terminate' && !serviceLineItemElement.Is_ETC_Line_Item__c && !serviceLineItemElement.csord__Is_Recurring__c){
                        serviceLineElement.ChargeValue=0;
                        
                    }else{
                        serviceLineElement.ChargeValue=serviceLineItemElement.Charge_Amounts__c;
                        
                    }
                    /*ETC/IP006/Defect#12142*/
                    if(Bundledflag == true && lineItem.Parent_Bundle_Flag__c == true)
                    {
                        serviceLineElement.ChargeAction = 'No Change';
                    }
                    else if(lineItem.ROC_Line_Item_Status__c == 'Success')
                    {
                        serviceLineElement.ChargeAction = 'UPDATE';
                    }
                    else if(lineItem.Ordertype__c == 'PROVIDE')
                    {
                        serviceLineElement.ChargeAction = 'CREATE';
                    }
                    /*/ else if(serviceLineItemElement.MRC_Price_Changed__c == true && 
                     * serviceLineItemElement.csord__Service__r.Product_Configuration_Type__c!='New Provide' && 
                     * serviceLineItemElement.csord__Service__r.Product_Configuration_Type__c!='Terminate' && 
                     * serviceLineItemElement.csord__Service__r.Product_Configuration_Type__c!='Parallel Upgrade' && 
                     * serviceLineItemElement.csord__Service__r.Product_Configuration_Type__c!='Parallel Downgrade')
                    {
                    serviceLineElement.ChargeAction = 'No Change';  
                    }/*/
                    else
                    {
                        serviceLineElement.ChargeAction = serviceLineItemElement.Charge_Action__c;
                    }
                    
                    serviceLineElement.TypeofCharge = serviceLineItemElement.Type_of_Charge__c;
                    serviceLineElement.BillText = serviceLineItemElement.Bill_Texts__c;
                    serviceLineElement.Cost = serviceLineItemElement.Cost__c;
                    serviceLineElement.InstallmentNRCStartDate = String.valueof(serviceLineItemElement.Installment_NRC_Start_Date__c);
                    serviceLineElement.InstallmentNRCEndDate = String.valueof(serviceLineItemElement.Installment_NRC_End_Date__c);
                    serviceLineElement.TerminationDate = String.valueof(serviceLineItemElement.Termination_Date__c);
                    serviceLineElement.BillingCommencementDate = serviceLineItemElement.csord__Service__r.Product_Configuration_Type__c!='Terminate'?String.valueof(serviceLineItemElement.Billing_Commencement_Date__c):String.valueof(serviceLineItemElement.Stop_Billing_Date__c);//ETC:Suparna:IP006
                    serviceLineElement.StartDate = serviceLineItemElement.csord__Service__r.Product_Configuration_Type__c!='Terminate'?String.valueof(serviceLineItemElement.Billing_Commencement_Date__c):String.valueof(serviceLineItemElement.Stop_Billing_Date__c);//ETC:Suparna:IP006
                    serviceLineElement.EndDate = String.valueof(serviceLineItemElement.Stop_Billing_Date__c);
                    serviceLineElement.CreditDuration = String.valueof(serviceLineItemElement.Credit_Duration__c);
                    if(Bundledflag == true && lineItem.Parent_Bundle_Flag__c == true)
                    {
                        serviceLineElement.ChargeFrequency = 'Monthly';
                    }
                    else{
                        serviceLineElement.ChargeFrequency = serviceLineItemElement.Charging_Frequency__c;
                    }
                    serviceLineElement.CurrencyISOCode = serviceLineItemElement.CurrencyISOCode;
                    serviceLineElement.SiteA_SiteAcityname = serviceLineItemElement.Site_A_For_ROC__c;
                    serviceLineElement.SiteB_SiteBCityname = serviceLineItemElement.Site_B_For_ROC__c;
                    serviceLineElement.AEndZipPlus4 = serviceLineItemElement.AEndZipPlus4__c;
                    serviceLineElement.BEndZipPlus4 = serviceLineItemElement.BEndZipPlus4__c;
                    serviceLineElement.AEndCountryISOCode = serviceLineItemElement.AEndCountryISOCode__c;
                    serviceLineElement.BEndCountryISOCode = serviceLineItemElement.BEndCountryISOCode__c;
                    serviceLineElement.InAdvanceorInarrears = serviceLineItemElement.In_Advance_or_In_arrears__c;
                    serviceLineElement.AlignwithBillingcycle = String.valueof(serviceLineItemElement.Align_with_Billing_Cycle__c);
                    serviceLineElement.PivotDate = serviceLineItemElement.pivot_date__c;
                    serviceLineElement.DisplaylineitemonInvoice = String.valueof(serviceLineItemElement.Display_line_item_on_Invoice__c);
                    serviceLineElement.CreditApprovedBy = serviceLineItemElement.Credit_Approved_By__c;
                    if(serviceLineItemElement.Is_ETC_Line_Item__c && serviceLineItemElement.csord__Service__r.Order_Type__c=='Terminate'){
                        serviceLineElement.ChargeID =serviceLineItemElement.Charge__c;
                    }else{
                        serviceLineElement.ChargeID =serviceLineItemElement.Charge_ID__c;
                        
                    }
                    serviceLineElement.ZeroChargeFlag = serviceLineItemElement.Zero_Charge_Flag__c;
                    serviceLineElement.IsMiscellaneousFlag = String.valueof(serviceLineItemElement.Is_Miscellaneous_Credit_Flag__c);
                    serviceLineElement.TypeofMiscellaneousCredit = serviceLineItemElement.Type_of_Miscellaneous_Credit__c;
                    serviceLineElement.CostCentreCode = serviceLineItemElement.Cost_Center_Code__c;
                    serviceLineElement.U2CMasterCode = serviceLineItemElement.U2C_Master_Code__c;
                    if(serviceLineItemElement.csord__Service__r.Usage_Flag__c=='Yes'){
                        serviceLineElement.UsageBaseFlag = true;
                    }
                    else{
                        serviceLineElement.UsageBaseFlag = false;
                    }
                    serviceLineElement.PinServiceID = serviceLineItemElement.Pin_Service_ID__c;
                    serviceLineElement.PinBillText = serviceLineItemElement.Root_Bill_Text__c;
                    serviceLineElement.Country = serviceLineItemElement.Country__c;
                    serviceLineElement.BillProfileId = serviceLineItemElement.Bill_Profile_Integration_Number__c;
                    serviceLineElement.Parentofthebundleflag = serviceLineItemElement.Parent_of_Bundle__c;
                    serviceLineElement.BillingEntity = String.valueof(serviceLineItemElement.Billing_Entity__c);
                    serviceLineElements.add(serviceLineElement);
                }
                
            }
            TibcoComSchemasXsdgenerationBill.Service_element serviceElement = new TibcoComSchemasXsdgenerationBill.Service_element();
            serviceElement.AccountManager = lineItem.Account_Managers__c;
            serviceElement.BundleLabel = lineItem.Bundle_Label_name__c;
            if(lineItem.Bundle_Flag__c){
                serviceElement.BundleFlag = 'true';
            }else{
                serviceElement.BundleFlag = 'false';
            }
            serviceElement.ParentBundleID = ParentId==''?lineItem.id:ParentId;
            
            if(Bundledflag == true && lineItem.Parent_Bundle_Flag__c == true)
            {
                serviceElement.OrderType = 'Update';
            }
            else if(lineItem.csord__Order__r.Is_InFlight_Cancel__c == True)
            {
                serviceElement.BundleAction = 'Modify';
            }
            else if(lineItem.ROC_Line_Item_Status__c == 'Success')
            {
                serviceElement.BundleAction = 'Bill Update';
            }
            //ETC/IP006/Defect#12132
            else if(((lineItem.Product_Configuration_Type__c == 'Parallel Upgrade' || 
                      lineItem.Product_Configuration_Type__c == 'Parallel Downgrade') && 
                      lineItem.Ordertype__c != 'PROVIDE') || (lineItem.Product_Configuration_Type__c=='Terminate' || 
                      lineItem.Order_type__c=='Terminate'))
                //ETC/IP006/Defect#12132  
            {
                serviceElement.BundleAction = 'Terminate';
            }
            else if(lineItem.Ordertype__c == 'PROVIDE')
            {
                serviceElement.BundleAction = 'NEW';
            }
            else{
                serviceElement.BundleAction = lineItem.Bundle_Action__c;
            }
            serviceElement.OLIType = lineItem.OLI_Type__c;
            if(lineItem.Billable_Flag__c=='Yes' || lineItem.Billable_Flag__c=='yes' || lineItem.Billable_Flag__c=='YES'){
                serviceElement.BillableFlag = 'true';
            }else{
                serviceElement.BillableFlag = 'false';
            }
            //system.debug('@@@@serviceElement.BillableFlag'+serviceElement.BillableFlag);
            serviceElement.ProductID = lineItem.Product_Id__c;
            serviceElement.RootProductID = lineItem.Root_Product_ID__c;
            serviceElement.OrderDate = String.valueof(lineItem.Order_Date__c);
            
            if(lineItem.csord__Order__r.Is_Terminate_Order__c == true)
            {
                serviceElement.ContractStartDate = lineItem.Stop_Billing_Date__c;
            }
            else
            {
                serviceElement.ContractStartDate = lineItem.Billing_Commencement_Date__c;
            }
            if(Bundledflag == true && lineItem.Parent_Bundle_Flag__c == true)
            {
                serviceElement.OrderType = 'Change Order';
            }
            else if(lineItem.ROC_Line_Item_Status__c == 'Success')
            {
                serviceElement.OrderType = 'Bill Update';
            }
            else if(lineItem.csord__Order__r.Is_InFlight_Cancel__c == True && 
                   (lineItem.ROC_Line_Item_Status__c == '' || lineItem.ROC_Line_Item_Status__c == null))
            {
                serviceElement.OrderType = 'Cancel';  
                
            }
            else
            {
                serviceElement.OrderType = (lineItem.Order_type__c!='Subscription Change' && 
                                           (lineItem.Product_Configuration_Type__c == 'Parallel Upgrade' || 
                                           lineItem.Product_Configuration_Type__c == 'Parallel Downgrade'))?
                                           'New Provide':lineItem.Product_Configuration_Type__c;
            }
            serviceElement.PrimaryServiceID = lineItem.Primary_Service_ID__c;
            serviceElement.ParentCustomerPO = lineItem.Parent_Customer_PO__c;
            serviceElement.ProductCode = lineItem.Product_Code__c;
            serviceElement.ParentServiceID = lineItem.Parent_ServiceID__c;
            serviceElement.ContractDuration = String.valueof(lineItem.Contract_Duration__c);
            serviceElement.RootProductBillText = lineItem.Root_Bill_Text__c;
            serviceElement.ServiceBillText = lineItem.Service_Bill_Text__c;
            serviceElement.ProductName = lineItem.Product_Code__c;
            if(lineItem.Bill_Activation_Flag__c=='Yes' || lineItem.Bill_Activation_Flag__c=='yes' || lineItem.Bill_Activation_Flag__c=='YES'){
                serviceElement.BillActivationFlag = 'true';
            }else{
                serviceElement.BillActivationFlag = 'false';
            }
            serviceElement.GenerateCreditFlag = String.valueof(lineItem.Generate_Credit_Flag__c);
            serviceElement.ResourceId = lineItem.ResourceId__c;
            serviceElement.OliId = lineItem.id;
            serviceElement.UserName = u.EmployeeNumber;
            serviceElement.ServiceLineItem = serviceLineElements;
            
            serviceElements.add(serviceElement);
        }
        //system.debug('@@@@@serviceElements=outsideloop'+serviceElements);
        
        TibcoComSchemasXsdgenerationBill.Subscription_element request= new TibcoComSchemasXsdgenerationBill.Subscription_element();
        // request.AccountManager=subscription.Account_Manager__c;
        // request.BundleLabel=subscription.Bundle_Label__c;
        request.Service = serviceElements;
        
        TibcoComBillingactivate.BillingActivatePortTypeEndpoint1 billingActivate = new TibcoComBillingactivate.BillingActivatePortTypeEndpoint1();
        billingActivate.timeout_x =40000;
        TibcoComSchemasXsdgenerationBill.BillingActivateResponse_element respone= billingActivate.BillingActivateOperation(serviceElements);
        
    }
}