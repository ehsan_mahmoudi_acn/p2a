/**
* This class is used as controller to update an existing order. 
* @author - Accenture
* @date - 18-April-2015
* @version - 1.0
    
*/
public class CreateUpdateOrderlineitem{
    
    /** 
    * It returns an updated instance of the matching "order line item" for the "opportunity line item". If no match is found it creates a new instance of the "order line item" for the "opportunity line item".
    */
    /*public order_line_item__c createOLIObject(map<String,order_line_item__c> orderliMap,map<String,OpportunityLineItem> opliMap, String mapKey,String parentcpqItem, ID parentOrdID,List<FieldMapper__c> fmList,decimal quantity,integer curser,Integer lineitemcount){

        // Check and set the append text for GUID if decomposing is required.
        String appendCartItem = '';
        if(curser > 1){
            appendCartItem = String.valueof(curser-1);
        }
        // Retrieve the matching order line item object for the opportunity 
        order_line_item__c matchOrdObject;
        
        if(Util.strStreamId.contains(',' + (opliMap.get(mapKey)).Product2.Product_ID__c + ',')
            || Util.strStreamId.startswith((opliMap.get(mapKey)).Product2.Product_ID__c)
            || Util.strStreamId.endswith((opliMap.get(mapKey)).Product2.Product_ID__c)
        ){
            matchOrdObject = (orderliMap.get((opliMap.get(mapKey)).CartItemGuid__c + 1));
        }else{
            matchOrdObject = (orderliMap.get((opliMap.get(mapKey)).CartItemGuid__c + (curser > 1 ? appendCartitem : '')));
        }

        // If no matching order object found then create a new order line item object
        if (matchOrdObject == null){
            matchOrdObject = new order_line_item__c();
            matchOrdObject.put('parentorder__c', parentOrdID);
            matchOrdObject.put('ParentCPQItem__c',parentcpqItem);
            if((opliMap.get(mapKey)).Site_Type__c == 'SiteA'){
                matchOrdObject.Site_A_SDPM__c = (opliMap.get(mapKey)).Site__c != null ? (opliMap.get(mapKey)).Site__c : (opliMap.get(mapKey)).Site_B__c;
                matchOrdObject.Customer_Site_A_Address_SDPM__c = (opliMap.get(mapKey)).Site__c != null ? (opliMap.get(mapKey)).Site__r.Address1__c : (opliMap.get(mapKey)).Site_B__r.Address1__c;
            }else if((opliMap.get(mapKey)).Site_Type__c == 'SiteB'){
                matchOrdObject.Site_B_SDPM__c = (opliMap.get(mapKey)).Site_B__c != null ? (opliMap.get(mapKey)).Site_B__c : (opliMap.get(mapKey)).Site__c;
                matchOrdObject.Customer_Site_B_Address_SDPM__c = (opliMap.get(mapKey)).Site_B__c != null ? (opliMap.get(mapKey)).Site_B__r.Address1__c : (opliMap.get(mapKey)).Site__r.Address1__c;
            }else if((opliMap.get(mapKey)).Site_Type__c == 'SiteZ'){
                matchOrdObject.Site_A_SDPM__c = null;
                matchOrdObject.Site_B_SDPM__c = null;                      
            }else if((opliMap.get(mapKey)).Site_Type__c == '' || (opliMap.get(mapKey)).Site_Type__c == null){
                matchOrdObject.Site_A_SDPM__c = (opliMap.get(mapKey)).Site__c;
                matchOrdObject.Site_B_SDPM__c = (opliMap.get(mapKey)).Site_B__c;
                matchOrdObject.Customer_Site_A_Address_SDPM__c = (opliMap.get(mapKey)).Site__r.Address1__c;
                matchOrdObject.Customer_Site_B_Address_SDPM__c = (opliMap.get(mapKey)).Site_B__r.Address1__c;
            }
        }
        // Override the order line item fields with the opportunity lilne item fields
        if (matchOrdObject != null){
            for (Fieldmapper__c fmObj:fmList){
                if((opliMap.get(mapKey)).get((fmObj.source_field__c)) != null){
                    matchOrdObject.put((fmObj.Destination_Field__c), (opliMap.get(mapKey)).get((fmObj.source_field__c)));
                }
            }
            // If a opportunity lin item product is decomposible and quantity > 1 then update the price and charge ids.
            if(quantity > 1){
                matchOrdObject.put('Net_NRC_Price__c',double.valueOf((matchOrdObject.Net_NRC_Price__c)/quantity));
                matchOrdObject.put('Net_MRC_Price__c',double.valueOf(matchOrdObject.Net_MRC_Price__c)/quantity);
                matchOrdObject.put('NRC_Price__c',double.valueOf(matchOrdObject.NRC_Price__c)/quantity);
                matchOrdObject.put('MRC_Price__c',double.valueOf(matchOrdObject.MRC_Price__c)/quantity);
                matchOrdObject.put('Charge_ID__c', (String)matchOrdObject.get('Charge_ID__c')+String.valueof(curser));
                matchOrdObject.put('NRC_Charge_ID__c', (String)matchOrdObject.get('NRC_Charge_ID__c')+String.valueof(curser));
            }
            // Update the new cpq line item id. If the product is non decomposible then it will reset to the old value.
            if((matchOrdObject.cpqitem__c).contains('.')){
                matchOrdObject.put('cpqitem__c',parentcpqItem + '.' + lineitemCount);
                matchOrdObject.put('root_product_code__c',matchOrdObject.Root_Product_Id__c);
            }else{
                matchOrdObject.put('root_product_code__c','');
            }
            matchOrdObject.put('Line_Item_Status__c','New');
          //  matchOrdObject.put('Is_reject_button_clicked__c',false);
          // matchOrdObject.put('Reject_Order__c',false);
         //  matchOrdObject.put('Updated_Order__c',true);
        }
        // Return the updated matching order line item object.
        return matchOrdObject;
    }
    
    /**
    * It returns an updated instance of the matching "product configuration" for the "opportunity line item". If no match is found it creates a new instance of the "product configuration" for the "opportunity line item". 
    

    public product_configuration__c createPCObj(
        map<String,product_configuration__c> pcMap,map<String, Opportunitylineitem> opliPCMap,
        String mapKey,String parentcpqItem, ID parentOrdID,List<FieldMapper__c> fmPCList,decimal quantity,integer curser,Integer lineitemcount){
        // Check and set the append text for GUID if decomposing is required.
        String appendCartItem = '';
        if(curser > 1){
            appendCartItem = String.valueof(curser-1);
        }
        // Retrive the matching product configuration object for the opportunity line item.
        product_configuration__c matchPcObject;
         if(Util.strStreamId.contains(',' + (opliPCMap.get(mapKey)).Product2.Product_ID__c + ',')
            || Util.strStreamId.startswith((opliPCMap.get(mapKey)).Product2.Product_ID__c)
            || Util.strStreamId.endswith((opliPCMap.get(mapKey)).Product2.Product_ID__c)
        ){
            matchPcObject = (pcMap.get((opliPCMap.get(mapKey)).CartItemGuid__c + 1));
        }else{
            matchPcObject = (pcMap.get((opliPCMap.get(mapKey)).CartItemGuid__c + (curser > 1 ? appendCartItem : '')));
        }
        // If no matching product configuration object found then create a new instance of the  product configuration object.
        if(matchPcObject == null){
            system.debug('No Matching PC record found for CPQID ' + (opliPCMap.get(mapKey)).CartItemGuid__c );
            matchPCObject = new product_configuration__c();
            matchPcObject.put('ParentCPQItem__c',parentcpqItem);
        }
        // Override the fields in the product configuration object with the opportunity line item fields.
        if(matchPCObject != null){
            for (Fieldmapper__c fmObj:fmPCList){
                if((opliPCMap.get(mapKey)).get((fmObj.source_field__c)) != null){
                    matchPCObject.put((fmObj.Destination_Field__c), (opliPCMap.get(mapKey)).get((fmObj.source_field__c)));
                }
            }
            if((matchPcObject.cpqitem__c).contains('.')){
                matchPCObject.put('cpqitem__c',parentcpqItem + '.' + lineitemCount);
            }
        }
        // Return the updated matching product configuration object.
        return matchPCObject;
    }*/
    
    /*
     This function returns a comma seperated string for the source or destination fields in the fieldMapper object.
    
     public static String selectFMFields(string sourceColumn, List<FieldMapper__c> fmList ){
        String oppLIFieldString;
        for (Fieldmapper__c fmObj:fmList){
            if(sourcecolumn == 'source'){
                oppLIFieldString = oppLIFieldString == null ? fmObj.source_field__c : (oppLIFieldString.startsWith(fmObj.source_field__c) || oppLIFieldString.contains(','+fmObj.source_field__c)) ? oppLIFieldString : oppLIFieldString + ',' + fmObj.source_field__c;    
            }else{
                oppLIFieldString = oppLIFieldString == null ? fmObj.Destination_Field__c : (oppLIFieldString.startsWith(fmObj.Destination_Field__c) || oppLIFieldString.contains(','+fmObj.Destination_Field__c)) ? oppLIFieldString : oppLIFieldString + ',' + fmObj.Destination_Field__c;    
            }      
        }
        return oppLIFieldString;
    }*/
    
    /*
    * This is a function that creates the OLI and PC objects for the child line item of the pass parent opportunity line item object.
    * It recursively calls itself to traverse the hierarchy of the opportunity line item
   
    public List<sobject> recursiveFunction(
        String cpqItem, map<String,opportunitylineitem> opliMap, map<String, opportunitylineitem> opliPcMap,String parentcpqItem,
        List<FieldMapper__c> fmList, List<FieldMapper__c> fmPCList,map<String,order_line_item__c> orderliMap, map<String, 
        product_configuration__c> pcMap,Id parentOrdID,integer parentCurser){
            // i is a variable that stores the sequence number of the child items of the level for which the function was called. 
            // For example: if the function was called for level 1, it will help create key 1.1, 1.2, etc.
            integer i = 1;
            integer lineitemCount = 1;
            // List that will store the created/updated OLI and PC Objects
            List<sobject> oliListReturn = new list<sobject>();
            while (i > 0) {
                //Variable to store the sequence number in case of decomposition
                integer curser = 1;
                // Variable to store the key to retrieve the opportunity line items
                string mapKey=cpqItem + '.' + i;
                // Check if an opportunity line item exists for the mapkey 
                if(opliMap.get(mapKey) != null){
                    // Calculate the number of order line items the opportunity fields will be decomposed into.
                    Decimal quantity = (opliMap.get(mapKey)).Decomposable_Flag__c == true ? (opliMap.get(mapKey)).quantity__c : 1;           
                    // Iterate for the required number of the order line items
                    while(curser <= quantity){
                        (opliMap.get(mapKey)).cpqitem__c = parentcpqItem + '.' + lineitemCount;
                        // Create OLI and PC Objects
                        oliListReturn.add(createOLIObject(orderliMap,opliMap,mapKey,parentcpqItem,parentOrdID,fmList,quantity,(Integer)(quantity*(parentCurser -1)+curser),lineitemcount));
                        oliListReturn.add(createPCObj(pcMap,opliMap,mapKey,parentCPQItem, parentOrdID,fmPCList,quantity,(Integer)(quantity*(parentCurser -1)+curser),lineitemcount));
                        // Call the recursive function again with the current opportunity as parent.
                        oliListReturn.addall(recursiveFunction(
                            mapKey,opliMap,oplipcMap, parentcpqItem + '.' + lineitemCount,fmList,fmPCList,orderliMap,pcMap,parentOrdID,(Integer)(quantity*(parentCurser -1)+curser)));
                        curser++;
                        lineitemCount++;
                    }
                    i++;
                }
                else{
                // There are no more child opportunity line items for the parent opportunity line item so return back to parent level.
                    break;
                }
            }
            return oliListReturn;
        } */
    
    /**
    * Main function which will update the order line item and product configuration object for an existing order.
    */    
    /*public void updateOrder(String oppID){
        //List to store the field mapper fields for the order line item object
        List<FieldMapper__c> fmList = [Select f.Source_Field__c, f.Name, f.Mapping_Type__c, f.Id, f.Destination_Field__c From FieldMapper__c f Where Mapping_Type__c = 'Order Line Item'];
        //List to store the field mapper fields for the product configuration object
        List<FieldMapper__c> fmPCList = [Select f.Source_Field__c, f.Name, f.Mapping_Type__c, f.Id, f.Destination_Field__c From FieldMapper__c f Where Mapping_Type__c = 'Product Configuration'];
        
        //Retrieve the source fields for OLI from the opportunity line item and store them to a map
        String queryString = 'Select Product2.Product_ID__c,' + selectFMFields('source',fmList) + ' from opportunitylineitem where opportunity.id=\''+ oppID + '\' order by CPQitem__c';
        List<opportunitylineitem> oppliList = database.query(queryString);
        map<String, opportunitylineitem> opliCpqitemMap = new map<String,opportunitylineitem>();
        for (opportunitylineitem opliobj: oppliList){
            opliCpqitemMap.put(opliobj.cpqitem__C,opliobj);
        }
        
        // Retrieve the fields from the order line item and store them to a map
        queryString = 'select parentorder__c,root_product_code__c, ' + selectFMFields('destination',fmList) + ' from order_line_item__c where parentorder__r.Opportunity__c=\''+ oppID + '\'';
        List<order_line_item__c> ordliList = database.query(queryString);
        map<String, order_line_item__c> ordGUIDMap = new map<String,order_line_item__c>();
        ID parentOrdID;
        for (order_line_item__c orderliobj: ordliList){
            // check if an object with the same GUID is already added to the map.
            if (ordGUIDMap != null && ordGUIDMap.get(orderliobj.CartItemGuid__c) != null){
                // If added append the GUID with a counter to make the key unique
                integer i=1;
                while(ordGUIDMap.get(orderliobj.CartItemGuid__c + i) != null){
                    i++;
                }
                ordGUIDMap.put(orderliobj.CartItemGuid__c + i,orderliobj);
            }else{
                ordGUIDMap.put(orderliobj.CartItemGuid__c,orderliobj);
            }
            parentOrdID = orderliobj.parentorder__c;
        }
        
        // Retrieve the fields from the product configuration object and store them to a map
        queryString = 'select Order_line_item__c,Order_Line_Item__r.Decomposable_Flag__c,Order_Line_Item__r.Charge_ID__c, ' + selectFMFields('destination',fmPCList) + ' from Product_Configuration__c where Order_Line_Item__r.parentorder__r.Opportunity__c=\''+ oppID + '\'';
        List<Product_Configuration__c> pcList = database.query(queryString);
        map<String, Product_Configuration__c> pcMap = new map<String,Product_Configuration__c>();
        for (Product_Configuration__c pcobj: pcList){
            // check if an object with the same GUID is already added to the map.
            if (pcMap != null && pcMap.get(pcobj.CartItemGuid__c) != null){
                // If added append the GUID with a counter to make the key unique
                integer i=1;
                while(pcMap.get(pcobj.CartItemGuid__c + i) != null){
                    i++;
                }
                pcMap.put(pcobj.CartItemGuid__c + i,pcobj);
            }else{
                pcMap.put(pcobj.CartItemGuid__c,pcobj);
            }
        }
        
        /* Query the PC fields from the OpportunityLineItem Object and store to map  
        queryString = 'Select Product2.Product_ID__c,' + selectFMFields('source',fmPCList) + ' from opportunitylineitem where opportunity.id=\''+ oppID + '\' order by CPQitem__c';
        system.debug('Querystring'+queryString);
        list<opportunitylineitem> oppliPCList = database.query(queryString);
        map <String,opportunitylineitem> opliPCCpqitemMap = new map<String,opportunitylineitem>();
        for (opportunitylineitem opliobj: oppliPCList){
            opliPCCpqitemMap.put(opliobj.cpqitem__C,opliobj);
        }*/
        
        /* call the recursive function to create an update list 
        list<sobject> ordliUpdateList = new list<sobject>();
        for(opportunitylineitem opliobj: oppliList){
            if(!(opliobj.cpqitem__c).contains('.')){
                ordliUpdateList.add(createOLIObject(ordGUIDMap,opliCpqitemMap,opliobj.cpqitem__c,'',parentOrdID,fmList,1.0,1,1));
                ordliUpdateList.add(createPCObj(pcMap,opliPCCpqitemMap,opliobj.cpqitem__c,'',parentOrdID,fmPCList,1.0,1,1));
                ordliUpdateList.addall(recursiveFunction(
                    opliobj.cpqitem__c,opliCpqitemMap,opliPCCpqitemMap,opliobj.cpqitem__c,fmList,fmPCList,ordGUIDMap,pcMap, parentOrdID,1));
            }
        }*/
        
        /* Split the sobject list into the OLI and PC list 
        List<Order_line_item__c> ordupList = new List<Order_line_item__c>();
        List<Product_configuration__c> pcupList = new List<Product_configuration__c>();
        Integer intCounter = 1;
        for (sobject objIterator:ordliUpdateList){
            if(intCounter == 1) {
                ordupList.add((Order_line_item__c)objIterator);
                intcounter = 0;
            }else{
                pcupList.add((product_configuration__c)objIterator);      
                intcounter = 1;
            }
        }*/
        
        /* Upsert the order line item list 
        List<Database.UpsertResult> updateResult = database.upsert(ordupList);
        for (Database.UpsertResult sr : updateResult) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                //System.debug('Successfully updated OrderLineitem ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Orderlineitem fields that affected this error: ' + err.getFields());
                }
            }
        }*/
        
        /* Requery the updated order line item to find the records that were created newly in the last upsert transaction to update the PC object
        queryString = 'select id,CPQItem__c,Parent_Order_Line_Item__c,parentCPQItem__c from order_line_item__c where parentorder__r.Opportunity__c=\''+ oppID + '\'';
        List<order_line_item__c> updatedordliList = database.query(queryString);
        List<order_line_item__c> parentOrdLIList = new List<order_line_item__c>();                
        map<String,String> updatedOrderLIMap = new map<String,String>();

        for (order_line_item__c orderliobj: updatedordliList){
            updatedOrderLIMap.put(orderliobj.CPQItem__c,orderliobj.id);
            system.debug(orderliobj.Parent_Order_Line_Item__c);
            if(orderliobj.Parent_Order_Line_Item__c == null){
                parentOrdLIList.add(orderliobj);
            }
        }
        
        List<order_line_item__c> finalOrdUpdList = new List<order_line_item__c>();*/
        /* For newly created order line item set the parent order line item 
        if(parentOrdLIList.size() > 0){
            for(order_line_item__c oliObj:parentOrdLIList){
                oliObj.put('Parent_Order_Line_Item__c',updatedOrderLIMap.get(oliObj.parentCPQItem__c));
                finalOrdUpdList.add(oliObj);
            }
        }*/
        
        /* Update the order line item records in database for which the parent order line item was set
        if(finalOrdUpdList.size() > 0)
        updateResult = database.upsert(finalOrdUpdList);
        for (Database.UpsertResult sr : updateResult) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully updated OrderLineitem ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Orderlineitem fields that affected this error: ' + err.getFields());
                }
            }
        } */
        
        /* For the new product configuration objects update the parent order line item id        
        List<product_configuration__c> finalPCUpdateList = new List<product_configuration__c>();
        for (product_configuration__c pcObject: pcupList){
            system.debug(pcObject.order_line_item__c);
            if(pcObject.order_line_item__c == null){
                pcObject.put('order_line_item__c', updatedOrderLIMap.get(pcObject.CPQItem__c));
            }
            finalPCUpdateList.add(pcObject);
        }*/ 
        /* Upsert the product configuration object
        updateResult = database.upsert(finalPCUpdateList);
        for (Database.UpsertResult sr : updateResult) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully updated OrderLineitem ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Orderlineitem fields that affected this error: ' + err.getFields());
                }
            }
        }
    }*/
}