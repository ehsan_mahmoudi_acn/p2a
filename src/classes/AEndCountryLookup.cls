global class AEndCountryLookup extends cscfga.ALookupSearch {
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){
        System.Debug('doDynamicLookupSearch');
        List <Additional_Data__c> data = [SELECT Name, Type__c FROM Additional_Data__c WHERE Type__c = 'Country'];
        return data;
    }
    public override String getRequiredAttributes(){ return '["IPL","EPL","EPLX","ICBS"]';
    }
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
        List <String> ProductTypeNames = new List<String>();
        List <String> countryNames = new List<String>();
        System.Debug('searchFields = ' + searchFields);
        for (String key : searchFields.keySet())
        {
            String attValue = searchFields.get(key);
            if(attValue == 'Yes' || attValue == 'true')
                ProductTypeNames.add(key);
        }
        System.Debug('ProductTypeNames >> ' + ProductTypeNames);
        String query = 'SELECT A_End_Country_Name__c, Product_Type__c FROM IPL_Rate_Card_Item__c';
        if (ProductTypeNames != null)
        {
            query = query + ' WHERE Product_Type__c in :ProductTypeNames';
        }
        System.Debug('query = '+query);
        for (IPL_Rate_Card_Item__c rateCard : Database.query(query))
        {
            countryNames.add(rateCard.A_End_Country_Name__c);
        }
        System.Debug('countryNames = '+countryNames);
        System.Debug('doLookupSearch');
        System.Debug(searchFields);
        String searchValue = searchFields.get('searchValue') +'%';
         List <Additional_Data__c> data = [SELECT Name, Type__c FROM Additional_Data__c WHERE Type__c = 'Country' AND Name IN :countryNames AND Name LIKE :searchValue ORDER BY Name];
        System.Debug(data);
       return data;
    }

}