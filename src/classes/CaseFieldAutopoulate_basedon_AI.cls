/* This Class is implemeted as part of the CR 52, for case object pricing case approval field value update based upon the PV AI pricing case approval field value */
public without sharing class CaseFieldAutopoulate_basedon_AI
{
    public static void fieldPopulate(List<case> newCases)
    {
        list<id> oppidList=new list<id>();
        for(case CaseID:newCases)
        {
           //Bases upon the "subject" field in Case object we are writing this condition .
           if(CaseID.subject.contains('Pricing approval request for Quote'))
         {
           System.debug('Rec Type Name'+CaseID.RecordType.Name);
           System.debug('Rec Type Id'+CaseID.RecordType.Name);
           //Adding the list of caseid having opportunity in the opplist.
           oppidList.add(CaseID.Opportunity_Name__c);
         }
        }
        if(oppidList.size()>0){
        Action_Item__c pricingValAIObj=[Select id,Opportunity__c,Quote_Subject__c,Status__c,Pricing_Case_Approval__c from Action_Item__c where Quote_Subject__c='Pricing Validation' and Opportunity__c IN :oppidList order by LastModifieddate DESC][0];
        system.debug('=====pricingValAIObj====='+pricingValAIObj);
        for(Case CaseID:newCases)
        {
         if(CaseID.subject.contains('Pricing approval request for Quote') && pricingValAIObj.Opportunity__c==CaseID.Opportunity_Name__c)
          {
         system.debug('====pricingValAIOb==inside');
         if(pricingValAIObj.Status__c!='Approved' || pricingValAIObj.Status__c=='Closed - Non Commercial')
          {
         CaseID.Pricing_Approval_Case__c= 'Pricing';
          }
          else
            {
          CaseID.Pricing_Approval_Case__c= pricingValAIObj.Pricing_Case_Approval__c;
            } 
          }
       }    
  }
  }
}