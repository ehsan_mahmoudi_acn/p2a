public class AllActionItemTriggerHandler extends BaseTriggerHandler{
    public override void beforeInsert(List<SObject> newActionItems) {
        // New Logo changes for Before Insert Method
        NewLogoActionItemHdlr newLogoActItmHdlr = new NewLogoActionItemHdlr();
        newLogoActItmHdlr.befInstActItmHdlrMthd(trigger.new,trigger.isBefore,trigger.isInsert);
          /* CR52-Pricing Entry Process*/
        OrderConfigPricingValActionItem ordPricingAI=new OrderConfigPricingValActionItem();
        ordPricingAI.updateOrderConfigAIDetailsOnInsert((List<Action_Item__c>) newActionItems);
        //
        //SPT
         if(label.MuteSPT.equalsignorecase('FALSE') || test.isrunningtest()){
        requesttomodifySplitAI((List<Action_Item__c>) newActionItems);
         } 
    }

    public override void afterInsert(List<SObject> newActionItems, Map<Id, SObject> newActionItemsMap) 
    {
     /* CR52-Pricing Entry Process*/
         OrderConfigPricingValActionItem ordPricingAI=new OrderConfigPricingValActionItem();
         ordPricingAI.SendEmailforOrderConfigAIonInsert((List<Action_Item__c>) newActionItems);
         ordPricingAI.createPricingValidationAI((List<Action_Item__c>)newActionItems);
    
        //Credit Check AI to update Opportunity 
        creditCheckAI((List<Action_Item__c>)newActionItems);
        
        ActionItemSequenceController objActionItemSequence = new ActionItemSequenceController();
        objActionItemSequence.afterInsert();
        
        UpdateOpportunityFromActionItemMethod((List<Action_Item__c>)newActionItems);

        runManagedSharing((List<Action_Item__c>)newActionItems);
        
        // New Logo changes for After Insert Method
        NewLogoActionItemHdlr newLogoActItmHdlr = new NewLogoActionItemHdlr();
        newLogoActItmHdlr.aftInstActItmHdlrMthd(trigger.new,trigger.isAfter,trigger.isInsert);
    }

    public override void beforeUpdate(List<SObject> newActionItems, Map<Id,SObject> newActionItemsMap, Map<Id,SObject> oldActionItemsMap) {
    
        // New Logo changes for Before Update Method
        NewLogoActionItemHdlr newLogoActItmHdlr = new NewLogoActionItemHdlr();
        newLogoActItmHdlr.befUpdActItmHdlrMthd(trigger.new,trigger.isBefore,trigger.isUpdate);
        /* CR52-Pricing Entry Process*/
         OrderConfigPricingValActionItem ordPricingAI=new OrderConfigPricingValActionItem();
      
       ordPricingAI.approveOrderConfigPricingValAI((List<Action_item__c>) newActionItems, (Map<Id,Action_item__c>) newActionItemsMap, (Map<Id,Action_item__c>) oldActionItemsMap);
        if(label.MuteSPT.equalsignorecase('FALSE')){
       requesttomodifySplitAI((List<Action_item__c>) newActionItems);
       updateCompletedByAndDate((List<Action_Item__c>) newActionItems,(Map<Id,Action_Item__c>) oldActionItemsMap);
        }
    }

    public override void afterUpdate(List<SObject> newActionItems, Map<Id, SObject> newActionItemsMap, Map<Id, SObject>  oldActionItemsMap) {
    /* CR52-Pricing Entry Process*/
       OrderConfigPricingValActionItem ordPricingAI=new OrderConfigPricingValActionItem();
       ordPricingAI.unCommercialIMPACTUpdateAI((List<Action_item__c>) newActionItems, (Map<Id,Action_item__c>) newActionItemsMap, (Map<Id,Action_item__c>) oldActionItemsMap);
       ordPricingAI.unAssignAgainPricingValAI((List<Action_item__c>) newActionItems, (Map<Id,Action_item__c>) newActionItemsMap, (Map<Id,Action_item__c>) oldActionItemsMap);
       ordPricingAI.rejectPricingFromOrderConfigAI((List<Action_item__c>) newActionItems, (Map<Id,Action_item__c>) newActionItemsMap, (Map<Id,Action_item__c>) oldActionItemsMap);
       ordPricingAI.rejectOrderConfigfromPricingAI((List<Action_item__c>) newActionItems, (Map<Id,Action_item__c>) newActionItemsMap, (Map<Id,Action_item__c>) oldActionItemsMap);  
       ordPricingAI.updateSalesJustificationtoPricing((List<Action_item__c>) newActionItems, (Map<Id,Action_item__c>) newActionItemsMap, (Map<Id,Action_item__c>) oldActionItemsMap);
       ActionItemCaseFieldAutopoulate.Fieldpopulate((List<Action_Item__c>)newActionItems);
      /* CR52-Pricing Entry Process*/  
        if(label.MuteSPT.equalsignorecase('FALSE')){
        updateCrossCountryAppAI((List<Action_Item__c>) newActionItems, (Map<Id, Action_Item__c>) newActionItemsMap, (Map<Id, Action_Item__c>)  oldActionItemsMap);
         }
        //Credit Check AI to update Opportunity 
        creditCheckAI((List<Action_Item__c>)newActionItems);
        
        ActionItemSequenceController ObjActionItemSequence = new ActionItemSequenceController();
        ObjActionItemSequence.afterUpdate(); 
        
        UpdateOpportunityFromActionItemMethod((List<Action_Item__c>)newActionItems);

        runManagedSharing((List<Action_Item__c>)newActionItems);
         pricingValidationActionItem((List<Action_item__c>) newActionItems,(Map<Id,Action_item__c>) oldActionItemsMap);
        
        // New Logo changes for After Update Method
        NewLogoActionItemHdlr newLogoActItmHdlr = new NewLogoActionItemHdlr();
        newLogoActItmHdlr.aftUpdActItmHdlrMthd(trigger.new,trigger.isAfter,trigger.isUpdate);
        updatingcommercialimpactinpricingvalidationAI((List<Action_Item__c>) newActionItems);
    }
    public void updateCrossCountryAppAI(List<Action_Item__c> newActionItems, Map<Id, Action_Item__c> newActionItemsMap, Map<Id, Action_Item__c>  oldActionItemsMap){
        ActionItemSequenceController ObjActionItemSequence = new ActionItemSequenceController();
       Util.OppTeamMemberSkipOnCCAAIUpdate=false;//flag to skip o team -- 30 may
    ObjActionItemSequence.afterUpdate();
        /*IR171:Suparna:update OpportunitySplit with split percentage for Cross Country Application.*/
    
    Map<Id,Action_Item__c> splitOwnerIdset=new Map<Id,Action_Item__c>();
    //Opportunity Split Modification AI enhancements -- Bhargav
    Map<ID, Action_Item__c> modificationsplits = new Map<ID,Action_Item__c>();
    Set<ID> modifyset = new Set<ID>();
    Set<Id> oppSet=new Set<Id>();
    //if(label.MuteSPT != 'TRUE'){
for(Action_Item__c AIObj:newActionItems){
    system.debug(AIObj.Feedback__c+AIObj.Subject__c);
    if((AIObj.Feedback__c=='Approved' || AIObj.Feedback__c=='Rejected') && AIObj.Status__c=='Completed' && AIObj.Subject__c=='Request Cross Country Application Approval'){
        splitOwnerIdset.put(AIObj.Opportunity_Split_Owner__c,AIObj);
        oppSet.add(AIObj.Opportunity__c);
    }
    
    if(AIObj.Subject__c=='Request to Modify Opportunity Split' && AIObj.Status__c=='Completed'){
        modificationsplits.put(AIObj.Opportunity_Split_Owner__c,AIObj);
        modifyset.add(AIObj.Opportunity__c);
    }
}

if(modificationsplits.size()>0){
    system.debug('Bhargav Modify Debug 1');
    List<OpportunitySplit> ModificationOppSplits = [select id, Total_Product_SOV__c, SplitPercentage_Product_SOV__c, Team_Role__c, SplitAmount, SplitNote, SplitPercentage, SplitOwnerID, SplitOwner.UserRole.Name, OpportunityID from OpportunitySplit where OppSplitType__c=:true and SplitOwnerID IN:modificationsplits.keyset() and OpportunityID IN:modifyset];
    system.debug('Bhargav Modify Debug 2');
    for(OpportunitySplit o : ModificationOppSplits){
        if(modificationsplits.containskey(o.SplitOwnerID) && modificationsplits.get(o.SplitOwnerID).Opportunity__c == o.OpportunityID){
            if(modificationsplits.get(o.SplitOwnerID).Status__c=='Completed' && modificationsplits.get(o.SplitOwnerID).New_Product_SOV__c!=null){
                if(o.SplitPercentage_Product_SOV__c!=null){
                    o.SplitPercentage_Product_SOV__c=modificationsplits.get(o.SplitOwnerID).New_Product_SOV__c;
                    o.SplitAmount_Product_SOV__c=(o.SplitPercentage_Product_SOV__c*o.Total_Product_SOV__c)/100;
                }else{
                    newActionItems[0].addError('User Product SOV should not be populated since User Reported SOV is populated at Split level');
                }
            }else if(modificationsplits.get(o.SplitOwnerID).Status__c=='Completed' && modificationsplits.get(o.SplitOwnerID).New_Reported_Total_SOV__c!=null){
                if(o.SplitPercentage!=null && (o.SplitPercentage>0 || (o.SplitPercentage==0 && (o.SplitOwner.UserRole.Name.contains('Account Manager') || o.SplitOwner.UserRole.Name.contains('Channels and Alliances') || o.SplitOwner.UserRole.Name.contains('C&A')|| o.SplitOwner.UserRole.Name.contains('PreSales'))))){
                    o.SplitPercentage=modificationsplits.get(o.SplitOwnerID).New_Reported_Total_SOV__c;
                }else{
                    newActionItems[0].addError('User Reported SOV should not be populated since User Product SOV is populated at Split level');
                }
            }else if(modificationsplits.get(o.SplitOwnerID).Status__c=='Rejected'){
                if(o.SplitPercentage!=null){
                    o.SplitPercentage=0;
                }
                if(o.SplitPercentage_Product_SOV__c!=null){
                    o.SplitPercentage_Product_SOV__c=0;
                    o.SplitAmount_Product_SOV__c=0;
                }
            }
        }
    }
    system.debug('Bhargav Modify Debug 3');
    if(ModificationOppSplits.size()>0){ 
        //flag to update opportunity when modification AI is completed -- Bhargav -- 2 June 2016
        try{
            update ModificationOppSplits;
        }catch(Exception e){
            newActionItems[0].addError('The total SOV recognition of an opportunity cannot exceed 100%, unless cross country application is submitted and approved.');
            ErrorHandlerException.ExecutingClassName='AllActionItemTriggerHandler:updateCrossCountryAppAI';      
            ErrorHandlerException.objectList=newActionItems;      
            ErrorHandlerException.sendException(e);
        }
    }
    system.debug('Bhargav Modify Debug 4');
}

if(splitOwnerIdset.size()>0){
List<OpportunitySplit> OpportunitySplits = [Select id,Total_Product_SOV__c,SplitPercentage_Product_SOV__c,Team_Role__c, SplitAmount, SplitNote,SplitPercentage, SplitOwnerID, SplitOwner.UserRole.Name, OpportunityID from OpportunitySplit where OppSplitType__c=:true and SplitOwnerID IN:splitOwnerIdset.keyset() and OpportunityID IN:oppSet];

for(OpportunitySplit oppSplit:OpportunitySplits){
    
    if(splitOwnerIdset.containskey(oppSplit.SplitOwnerID) && splitOwnerIdset.get(oppSplit.SplitOwnerID).Opportunity__c==oppSplit.OpportunityID){
        oppSplit.isApproved__c=(splitOwnerIdset.get(oppSplit.SplitOwnerID).Feedback__c=='Approved' && splitOwnerIdset.get(oppSplit.SplitOwnerID).Status__c=='Completed')?true:false;
        oppSplit.isRejected__c=(splitOwnerIdset.get(oppSplit.SplitOwnerID).Feedback__c=='Rejected' && splitOwnerIdset.get(oppSplit.SplitOwnerID).Status__c=='Completed')?true:false;
        if(oppSplit.isApproved__c && newActionItems[0].Status__c=='Completed' && splitOwnerIdset.get(oppSplit.SplitOwnerID).Product_SOV_Perc__c>0){
            if(oppSplit.SplitPercentage_Product_SOV__c!=null){
                oppSplit.SplitPercentage_Product_SOV__c=splitOwnerIdset.get(oppSplit.SplitOwnerID).Product_SOV_Perc__c;
                oppsplit.SplitAmount_Product_SOV__c=(oppSplit.SplitPercentage_Product_SOV__c*oppsplit.Total_Product_SOV__c)/100;
            }else{
                newActionItems[0].addError('User Product SOV should not be populated since User Reported SOV is populated at Split level');
            }
            system.debug('=====oppsplit.SplitAmount_Product_SOV__c==='+oppsplit.SplitAmount_Product_SOV__c);
        
        }else if(oppSplit.isApproved__c && newActionItems[0].Status__c=='Completed' && splitOwnerIdset.get(oppSplit.SplitOwnerID).Reported_Total_SOV_Perc__c>0){
            if(oppSplit.SplitPercentage!=null && (oppSplit.SplitPercentage>0 || (oppSplit.SplitPercentage==0 && (oppsplit.SplitOwner.UserRole.Name.contains('Account Manager') || oppsplit.SplitOwner.UserRole.Name.contains('Channels and Alliances') || oppsplit.SplitOwner.UserRole.Name.contains('C&A')|| oppsplit.SplitOwner.UserRole.Name.contains('PreSales'))))){
                oppSplit.SplitPercentage=splitOwnerIdset.get(oppSplit.SplitOwnerID).Reported_Total_SOV_Perc__c;
            }else{
                newActionItems[0].addError('User Reported SOV should not be populated since User Product SOV is populated at Split level');
            }
        }else if(oppSplit.isRejected__c && newActionItems[0].Status__c=='Completed'){
            
            if(oppSplit.SplitPercentage!=null){
                oppSplit.SplitPercentage=0;
            }
            if(oppSplit.SplitPercentage_Product_SOV__c!=null){
                oppSplit.SplitPercentage_Product_SOV__c=0;
                oppsplit.SplitAmount_Product_SOV__c=0;
            }
            
        }
        
    }
    
}
system.debug('debug bhargav CCA ai after update');
if(OpportunitySplits.size()>0){Util.skipOpportunitySplitFlag=false;Util.OppTeamMemberSkipOnCCAAIUpdate=false;Util.skipOpportunitySplitTeamTrgFlag=false;update OpportunitySplits;}


}
   //}
   
  /* if(trigger.isbefore){
       List<OpportunitySplit> OppSplitListValues = [Select ID, SplitOwnerID, SplitPercentage, SplitPercentage_Product_SOV__c from OpportunitySplit where OpportunityID=:trigger.new[0].Opportunity__c and OppSplitType__c=:true];
    
    for(Action_Item__c AIObj:trigger.new){
    if(AIObj.Feedback__c=='Rejected' && AIObj.Status__c=='Completed' && AIObj.Subject__c=='Request Cross Country Application Approval'){
                    AIObj.Product_SOV_Perc__c=0;
                    AIObj.Reported_Total_SOV_Perc__c=0;
        
    }
    //Automatically populating Completed By and Completion Date on action item for Opportunity Split Approval -- Bhargav
    if(AIObj.Status__c=='Completed' && AIObj.Subject__c=='Request Cross Country Application Approval'){
        AIObj.Completion_Date__c = System.today();
        AIObj.Completed_By__c = UserInfo.getName();
    }
    
    //Automatically populating Completed By and Completion Date on action item for Opportunity Split Modification -- Bhargav
    if(AIObj.Status__c=='Completed' && AIObj.Subject__c=='Request to Modify Opportunity Split'){
        AIObj.Completion_Date__c = System.today();
        AIObj.Completed_By__c = UserInfo.getName();
    }
    
        
    
  }}*/
    /*IR171:Suparna:update OpportunitySplit with split percentage for Cross Country Application.*/
}
    
public void updateCompletedByAndDate(List<Action_Item__c> newActionItems,Map<Id,Action_Item__c> oldActionItemsMap){
    for(Action_Item__c AIObj:newActionItems){
    system.debug(AIObj.Feedback__c+AIObj.Subject__c);
    if((AIObj.Feedback__c=='Approved' || AIObj.Feedback__c=='Rejected') && AIObj.Status__c!=null && AIObj.Status__c!=oldActionItemsMap.get(AIObj.id).Status__c && AIObj.Status__c=='Completed' && AIObj.Subject__c=='Request Cross Country Application Approval'){
         AIObj.Completed_By_Order__c=userinfo.getUserId();
        AIObj.Completion_Date__c=system.today();
    }
    
    if(AIObj.Subject__c=='Request to Modify Opportunity Split' && AIObj.Status__c!=null && AIObj.Status__c!=oldActionItemsMap.get(AIObj.id).Status__c && AIObj.Status__c=='Completed'){
        
        AIObj.Completed_By_Order__c=userinfo.getUserId();
        AIObj.Completion_Date__c=system.today();
    }
}
    
    
}
  public void requesttomodifySplitAI(List<Action_Item__c> newActionItems){
    
    Map<ID,ID> ActionOppMapping = new Map<ID,ID>();
       
        Map<Id,String> OppMap = new Map<Id,String>();
        Map<String,String> rtMap = new Map<String, String>();       
        Map<ID,Opportunity> objOppMap = new Map<ID,Opportunity>();
        Set<ID> OppIds = new Set<ID>();
        for(Action_Item__c ObjActionItem : newActionItems){
         if(ObjActionItem.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request to Modify Opportunity Split') && ObjActionItem.Comments__c!=''){
         if(ObjActionItem.Opp_Owner_Region__c=='South Asia' || ObjActionItem.Opp_Owner_Region__c=='North Asia'){
        ObjActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Opp Split Approval - Asia');   
         }else{
        ObjActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Opp Split Approval - '+ ObjActionItem.Opp_Owner_Region__c);
        }
             
             
             
       }
             
            
        }
        
        //Changes as part of Opportunity Split Modification AI enhancement -- Bhargav
        system.debug('entered trigger ai before insert -- Bhargav');
        for(Action_Item__c ObjActionItem : newActionItems){
            if(ObjActionItem.Subject__c=='Request to Modify Opportunity Split' && ObjActionItem.Modification_of_existing_Split__c && ObjActionItem.Opportunity_Split_Owner__c!=null){
                Oppids.add(ObjActionItem.Opportunity__c);
            }
        }
        
        if(Oppids.size()>0){
            //List<OpportunitySplit> oslist = [select id, SplitOwnerID, SplitPercentage, SplitPercentage_Product_SOV__c from OpportunitySplit where OpportunityID IN : Oppids ];
            for(Action_Item__c AIObj : newActionItems){
                for(OpportunitySplit o : [select id, SplitOwnerID, SplitPercentage, SplitPercentage_Product_SOV__c from OpportunitySplit where OpportunityID IN : Oppids ]){
                    if(o.SplitOwnerID == AIObj.Opportunity_Split_Owner__c){
                        AIObj.Reported_Total_SOV_Perc__c = o.SplitPercentage;
                        AIObj.Product_SOV_Perc__c = o.SplitPercentage_Product_SOV__c;
                    }
                }
            }
         }
    
    
    
  }   
    // Method to update Credit Check AI to Opportunity 
    @TestVisible
    private void creditCheckAI(List<Action_Item__c> newActionItems)    
    {
        Map<Id,String> opMap = new Map<Id, String>();
        
        
        List<Opportunity> opList = new List<Opportunity>();
        
        Set<Id> oppAIList = new Set<Id>();
        //query RecordType to get RecordType Object based on request SobjectType
        Id reqCreditCheckRecTypeId = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType, 'Request Credit Check');
        
       /*
       * Developer: KD
       * Date of Modification: 19th Jan 2017
       * Added Where cndition for select opportunity query
       * Added this code snippet to filter the opportunity select query.
       */ 
        set<Id> oppAIID = new Set<Id>();
        for (Action_Item__c ai : newActionItems)
        {
                if(ai.Opportunity__c != null)
                {
                        oppAIID.add(ai.Opportunity__c);
                }
        }
        //  query Opportunity to get Opportunity Object 
        if(oppAIID != null)
        {
            for(Opportunity op :[select Id,Name from Opportunity where id in :oppAIID])
            {
            opMap.put(op.Id,op.Name);
        }
        }
    
        for (Action_Item__c ai : newActionItems) 
        {
            if(ai.RecordTypeId == reqCreditCheckRecTypeId && ai.Status__c=='Completed')
            {
                if(opMap.get(ai.Opportunity__c) != null)
                { 
                    // update the fields of Opportunity with Action Item values     
                   Opportunity opObj = new Opportunity(Id = ai.Opportunity__c,CreditCheckStatus__c=ai.Status__c,CreditCheckValidity__c=ai.CreditLimitValidity__c,SecurityDeposit__c=ai.SecurityDeposit__c,SecurityDepositHoldingPeriod__c=ai.SecurityDepositHoldingPeriod__c,CreditCheckDoneBy__c=ai.Completed_By__c,CreditCheckCompletedDate__c=ai.Completion_Date__c,Credit_Check_Feedback__c =ai.Feedback__c);
                    opList.add(opObj);
                    
                }
            }
        
        }
        update opList;              
    }                  

    
    /* Type   : Managed Apex Sharing for Action_Item__c
    // Author : Siddharth Sinha
    // Date   : 30-04-2015
    // The AI object's OWD has been made as Private
    // This is to extend the Manual Sharing of the AI to AI Account Owner, AI Created By, AI Last Modified By and AI Opportunity Owner
    */
    //for @testvisible, modified by Anuradha
    @testvisible
    public void runManagedSharing(List<Action_Item__c> newActionItems){
        /** Action_Item_Share is the "Share" table that was created when the Organization Wide Default sharing setting was set to "Private" **/
        List<Action_Item__Share> AIShares = new List<Action_Item__Share>();
          string Createdby = Schema.Action_Item__Share.RowCause.AI_Creator__c;
          string Lastmodifiedby = Schema.Action_Item__Share.RowCause.Last_Modified_By__c;
          string accowner = Schema.Action_Item__Share.RowCause.Account_Owner__c;
          string Oppowner = Schema.Action_Item__Share.RowCause.Opportunity_Owner__c;

        /** For each of the Action_Item records being inserted, do the following: **/
        for(Action_Item__c t : newActionItems){
            /** Create a new Action_Item_Share record to be inserted in to the Action_Item_Share table. **/
            Action_Item__Share AI_CreatedBy = new Action_Item__Share();
            Action_Item__Share AI_AccountOwner = new Action_Item__Share();
            Action_Item__Share AI_OppOwner = new Action_Item__Share();
            Action_Item__Share AI_LastModBy = new Action_Item__Share();
            system.debug('===t=='+t.CreatedBy.ProfileId);
            /** Populate the Action_Item_Share record with the ID of the record to be shared. **/
            AI_CreatedBy.ParentId = t.Id;
            AI_AccountOwner.ParentId = t.Id;
            AI_OppOwner.ParentId = t.Id;
            AI_LastModBy.ParentId = t.Id;

            /** Share to - AI Creator **/ 
            if(t.CreatedById !=null){
                AI_CreatedBy.UserOrGroupId = t.CreatedById;
                /** Specify that the Apex sharing reason **/
                AI_CreatedBy.RowCause = createdby ;
            }
        
            /** Share to - AI Last Modified By **/ 
            if(t.lastModifiedById !=null){
                AI_LastModBy.UserOrGroupId = t.LastModifiedById;
                /** Specify that the Apex sharing reason **/
                AI_LastModBy.RowCause = lastmodifiedby ;
            }
            
            /** Share to - AI Account Owner **/ 
            if(t.Account_Owner_Id__c !=null){
                AI_AccountOwner.UserOrGroupId = t.Account_Owner_Id__c;
                
                /** Specify that the Apex sharing reason **/
                AI_AccountOwner.RowCause = accowner ; 
            }

            /** Share to - AI Opportunity Owner **/ 
            if(t.Opportunity_Owner_Id__c !=null){
                AI_OppOwner.UserOrGroupId = t.Opportunity_Owner_Id__c;
                
                /** Specify that the Apex sharing reason **/
                AI_OppOwner.RowCause = Oppowner ;              }
       
            /** Specify that the AI should have edit/read access for this particular Action_Item record. **/
            if(t.recordtypeid!=RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Pricing Validation')){
            if(t.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support') /*|| t.recordtypeid!=RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Pricing Validation')*/){
            AI_CreatedBy.AccessLevel = 'Edit';
            AI_OppOwner.AccessLevel = 'Edit';
            }else{
             AI_CreatedBy.AccessLevel = 'Read';
             AI_OppOwner.AccessLevel = 'Read';  
            }
            AI_AccountOwner.AccessLevel = 'Read';
            AI_LastModBy.AccessLevel = 'Read';

            /** Add the new Share record to the list of new Share records. **/
            
            AIShares.add(AI_CreatedBy);
            AIShares.add(AI_AccountOwner);
            AIShares.add(AI_OppOwner);
            AIShares.add(AI_LastModBy);
            
        }

        } /** Insert all of the newly created Share records and capture save result **/
        if(AIShares.size()>0){
        Database.SaveResult[] jobShareInsertResult = Database.insert(AIShares,false);
        }
    }
 public void updatingcommercialimpactinpricingvalidationAI(List<Action_Item__c> newActionItems){
     try{
     List<Action_Item__c> Pricingvalupdate = new List<Action_Item__c>();
      //List<Action_Item__c> PvAI = new List<Action_Item__c>();
     set<Id> Oppids = new set<Id>();
     for(Action_Item__c ai : newActionItems){
     If(ai.Quote_Subject__c == 'Order Configuration' && ai.Commercial_Impact__c == 'Opp Split'){
         Oppids.add(ai.Opportunity__c);
         }
      }
      If(Oppids.size() > 0){
    /*PvAI = [Select Commercial_Impact__c, Status__c, Pricing_Case_Approval__c from Action_Item__c 
                                 where Quote_Subject__c = 'Pricing Validation' and Opportunity__c in : Oppids]; */ 
      for(Action_Item__c pv : [Select Commercial_Impact__c, Status__c, Pricing_Case_Approval__c from Action_Item__c 
                                 where Quote_Subject__c = 'Pricing Validation' and Opportunity__c in : Oppids]){
        If(pv.Status__c != 'Closed - Non Commercial'){  
        pv.Commercial_Impact__c = 'Opp Split';
        pv.Status__c = 'Closed - Non Commercial';
        pv.Pricing_Case_Approval__c = 'Order Desk';
        Pricingvalupdate.add(pv);
         }
       } 
      }
        If(Pricingvalupdate.size()>0)
        update Pricingvalupdate;
        
        }catch(exception e)
        {
        ErrorHandlerException.ExecutingClassName='AllActionItemTriggerHandler :updatingcommercialimpactinpricingvalidationAI';   
        ErrorHandlerException.objectList=newActionItems;
        ErrorHandlerException.sendException(e);
        }
        
}

//This Method has been written as a replacement of "TrgUpdateOpportunityFromActionItem" Trigger
public void updateOpportunityFromActionItemMethod(List<Action_Item__c> newActionItems){
    
    List<Opportunity> opList = new List<Opportunity>();
    Map<Id,Integer> actionOppMap = new Map<Id, Integer>();
    
    // to map the record type ID and Name
    Map<Id,String> rtMap = new Map<Id, String>();
    
    // to map the opportunity ID and Name
    Map<Id,String> opMap = new Map<Id, String>();
    
    // Creating the set to store IDs
    Set<id> oppIds = new Set<Id>();
    
    Integer StageNumber;
    for (Action_Item__c ai : newActionItems) {
        oppIds.add(ai.Opportunity__c);
    }
    
    String Action = 'Action_Item__c';

     //query RecordType to get RecordType Object based on request SobjectType
    for(RecordType rt :[select Id,Name from RecordType Where SobjectType =:Action ]){   
        rtMap.put(rt.Id,rt.Name);
    }
    
    //query Opportunity to get Opportunity Object 
    for(Opportunity op :[select Id,Name from Opportunity where id IN: oppIds]){   
        opMap.put(op.Id,op.Name);
    }
    
    // Action Item is created for record type of which button is clicked
    for (Action_Item__c ai : newActionItems) {
     String Complete = 'Completed';
     String Assign = 'Assigned';
     String RCC= 'Request Credit Check';
     String RFS= 'Request Feasibility Study';
     String RTPQ= 'Request Third Party Quote';
     String SGRR = 'Stage Gate Review Request';
     String RPA = 'Request Pricing Approval';
     
        try{
            //System.debug('Record Type :'+rt.Name);            
            if(rtMap.get(ai.RecordTypeId) == RCC && ai.Status__c== Complete){
                   if(opMap.get(ai.Opportunity__c) != null){ 
                    // update the fields of Opportunity with Action Item values                 
                    Opportunity op = new Opportunity(Id = ai.Opportunity__c,CreditCheckStatus__c=ai.Status__c,CreditCheckValidity__c=ai.CreditLimitValidity__c,SecurityDeposit__c=ai.SecurityDeposit__c,SecurityDepositHoldingPeriod__c=ai.SecurityDepositHoldingPeriod__c,CreditCheckDoneBy__c=ai.Completed_By__c,CreditCheckCompletedDate__c=ai.Completion_Date__c,Credit_Check_Feedback__c =ai.Feedback__c);
                    opList.add(op);
                   }
             } else if(rtMap.get(ai.RecordTypeId) == RFS && ai.Status__c== Complete){
                if(opMap.get(ai.Opportunity__c) != null){
                   // update the fields of Opportunity with Action Item values  
                   Opportunity op = new Opportunity(Id = ai.Opportunity__c,FeasibilityStatus__c=ai.Status__c,FeasibilityDoneBy__c=ai.Completed_By__c,FeasibilityCompletedDate__c=ai.Completion_Date__c, F_Expiration_Date__c=ai.F_Expiration_Date__c,Feasibility_Result__c =ai.FeasibilityResult__c);
                   op.FeasibilityDoneBy__c = userinfo.getFirstName()+' ' +userinfo.getLastName();
                   opList.add(op);
                }
             } else if(rtMap.get(ai.RecordTypeId) == RPA && ai.Status__c== Complete){
                if(opMap.get(ai.Opportunity__c) != null){
                    // update the fields of Opportunity with Action Item values  
                    Opportunity op = new Opportunity(Id = ai.Opportunity__c,PricingApprovalStatus__c=ai.Status__c,PricingApprovalDoneBy__c=ai.Completed_By__c,PricingApprovalCompletedDate__c=ai.Completion_Date__c,Pricing_Approval_Feedback__c =ai.Pricing_Feedback__c);
                    op.PricingApprovalDoneBy__c = userinfo.getFirstName()+' ' +userinfo.getLastName();
                    opList.add(op);
                }
             } else if(rtMap.get(ai.RecordTypeId) == RTPQ && ai.Status__c== Complete){
                if(opMap.get(ai.Opportunity__c) != null){
                    // update the fields of Opportunity with Action Item values  
                    Opportunity op = new Opportunity(Id = ai.Opportunity__c,CarrierQuoteStatus__c=ai.Status__c,CarrierQuoteDoneBy__c=ai.Completed_By__c,CarrierQuoteCompletedDate__c=ai.Completion_Date__c, Third_Party_Feedback__c = ai.Feedback__c);
                    op.CarrierQuoteDoneBy__c = userinfo.getFirstName()+' ' +userinfo.getLastName();
                    opList.add(op);
                }
             } else if(rtMap.get(ai.RecordTypeId) == SGRR && ai.Status__c== Complete){
                if(opMap.get(ai.Opportunity__c) != null){
                    // update the fields of Opportunity with Action Item values  
                    if(ai.Stage_Gate_Number__c == 1){
                        Opportunity op = new Opportunity(Id = ai.Opportunity__c,Stage_Gate_1_Review_Completed__c = true);
                        opList.add(op);
                    }
                    else if(ai.Stage_Gate_Number__c == 2){
                        Opportunity op = new Opportunity(Id = ai.Opportunity__c,Stage_Gate_2_Review_Completed__c = true);
                        opList.add(op);
                    }
                    else if(ai.Stage_Gate_Number__c == 3){
                        Opportunity op = new Opportunity(Id = ai.Opportunity__c,Stage_Gate_3_Review_Completed__c = true);
                        opList.add(op);
                    }
                    else if(ai.Stage_Gate_Number__c == 4){
                        Opportunity op = new Opportunity(Id = ai.Opportunity__c,Stage_Gate_4_Review_Completed__c = true);
                        opList.add(op);
                    }                   
                }
            } else  if(rtMap.get(ai.RecordTypeId) == SGRR && ai.Status__c== Assign){
                if(opMap.get(ai.Opportunity__c) != null){
                    // update the fields of Opportunity with Action Item values  
                    if(ai.Stage_Gate_Number__c == 1){
                        Opportunity op = new Opportunity(Id = ai.Opportunity__c,Stage_Gate_1_Action_Created__c = true);
                        opList.add(op);
                    }
                    else if(ai.Stage_Gate_Number__c == 2){
                        Opportunity op = new Opportunity(Id = ai.Opportunity__c,Stage_Gate_2_Action_Created__c = true);
                        opList.add(op);
                    }
                    else if(ai.Stage_Gate_Number__c == 3){
                        Opportunity op = new Opportunity(Id = ai.Opportunity__c,Stage_Gate_3_Action_Created__c = true);
                        opList.add(op);
                    }
                    else if(ai.Stage_Gate_Number__c == 4){
                        Opportunity op = new Opportunity(Id = ai.Opportunity__c,Stage_Gate_4_Action_Created__c = true);
                        opList.add(op);
                    }
                    
                }
            }
       } catch(Exception excp) {
                System.debug('Error In Action Item Trigger');       
                CreateApexErrorLog.InsertHandleException('Trigger','AllActionItemTriggerHandler',excp.getMessage(),'Action_Item__c',Userinfo.getName());
                ErrorHandlerException.ExecutingClassName='AllActionItemTriggerHandler :UpdateOpportunityFromActionItemMethod';   
                ErrorHandlerException.objectList=newActionItems;
                ErrorHandlerException.sendException(excp); 
       }
     }

    // Bulk Update
    if(opList.size() > 0 ) {
        try{
                update opList;
        }catch(Exception excp){
            
            CreateApexErrorLog.InsertHandleException('Trigger','AllActionItemTriggerHandler',excp.getMessage(),'Action_Item__c',Userinfo.getName());
            ErrorHandlerException.ExecutingClassName='AllActionItemTriggerHandler :UpdateOpportunityFromActionItemMethod';   
            ErrorHandlerException.objectList=newActionItems;
            ErrorHandlerException.sendException(excp);
        }
    }
}

Public static void pricingValidationActionItem(List<Action_Item__c> newActionItems,Map<Id,Action_Item__c> oldActionItemsMap){
	For (Action_Item__c act : newActionItems){
	system.debug('action item stage' +act.Opportunity_Stage__c);
	if(act.Status__c=='Approved' && oldActionItemsMap.get(act.id).Status__c=='Unassigned'){
		act.addError('Please Assign the Action Item before changing it to Approved.');
	}
	else if(act.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Pricing Validation') && oldActionItemsMap.get(act.ID).Status__c == 'Approved' ){

		   // List<Action_Item__c> aiOppo = [select ID, Opportunity__c, Opportunity__r.StageName from Action_Item__c where ID=:act.ID ];
		if(act.Status__c != 'Closed Lost'){
		act.addError('You cannot roll back from the Approved status.');
		}

			else if(act.Status__c == 'Closed Lost' )
			{
				if (act.Opportunity_Stage__c!= 'Closed Lost')
				{
					act.addError('You cannot roll back from the Approved status.');
				}
			}
		}
	}
} 
    
}