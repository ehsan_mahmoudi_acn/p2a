global without sharing class CustomButtonSynchronizeWithOpportunity extends csbb.CustomButtonExt {
	public static boolean skipValidation = false;	
    public String performAction(String basketId) {
        
		try{
        id uid = userinfo.getUserId();
        id pid = userinfo.getProfileId();
        
        cscfga__Product_Basket__c baskets = [select id,csbb__Account__c, cscfga__Opportunity__c, csbb__Account__r.Customer_Type_New__c, Account_Owner_ID__c, Opportunity_Owner_ID__c from cscfga__Product_Basket__c where id =: basketId];
        string OppTeamOpp = baskets.cscfga__Opportunity__c;
        List<OpportunityTeamMember> OppTeam = [SELECT Id,Name,TeamMemberRole,UserId FROM OpportunityTeamMember WHERE OpportunityId =: OppTeamOpp];
        if(skipValidation==false){
			if(uid != baskets.Account_Owner_ID__c && uid != baskets.Opportunity_Owner_ID__c && pid != label.Profile_System_Administrator && pid != label.Profile_TI_Terminate_Team && pid != label.Profile_TI_Account_Manager && pid != label.Profile_TI_Sales_Admin && pid != label.Profile_TI_Order_Desk)
			{
				return '{"status":"error","title":"Error","text":"You are not authorized to request this action"}';
			}
		}
        String newUrl = CustomButtonSynchronizeWithOpportunity.syncWithOpportunity(basketId);
        if (newURL.contains('Synchronise with opportunity failed'))
        {
            return '{"status":"error","text":"' + newUrl + '"}';
        }
        else{
            return '{"status":"ok","text":"Basket has been synced successfully","redirectURL":"' + newUrl + '"}';
        }
	  }catch(Exception e){System.debug(e);return null;}
    }

    public static String syncWithOpportunity(String basketId) {
		try{
        List <cscfga__Product_Configuration__c> orphanProdConfs = 
            [select c.id
            from cscfga__Product_Configuration__c c
            where 
            cscfga__product_basket__c = :basketId and
            cscfga__root_configuration__c = null and
            id not in 
            (select csbb__product_configuration__c
            from csbb__Product_Configuration_Request__c
            where csbb__product_basket__c = :basketId
            and csbb__product_configuration__c != null
          )
        ];
     
     
        if (orphanProdConfs.size() > 0) 
            delete orphanProdConfs;
            
         //desc : opp should not be synced when only Colo is there in basket
        //name : Sowmya
        //date : 2/7/2017
        Boolean flag = true;
        //cscfga__Product_Basket__c pbid = [select id, cscfga__opportunity__c, csbb__Synchronised_with_Opportunity__c, cscfga__Basket_Status__c  
            //from cscfga__Product_Basket__c where id = :basketId];
    
        List<cscfga__Product_Configuration__c> config = [select id, Product_Id__c,Added_Racks__c from cscfga__Product_Configuration__c where cscfga__product_basket__c = :basketId and (Product_Id__c = 'COLO-CNTDCOLO' or Product_Id__c = 'COLO-REH' or Product_Id__c = 'COLO-CAGE' )];
        
        for(cscfga__Product_Configuration__c pc : config){
                if(pc.Added_Racks__c == '' || pc.Added_Racks__c == null ){
                        flag = false;
                        if(pc.Product_Id__c == 'COLO-REH'){
                                flag = true;
                            }
                    }
            }
        
        if(flag == false){
            return 'Synchronise with opportunity failed: basket must have at least one second level child.';    
            }
    
    
        cscfga__Product_Basket__c pb = [select id, cscfga__opportunity__c, csbb__Synchronised_with_Opportunity__c, cscfga__Basket_Status__c  
            from cscfga__Product_Basket__c where id = :basketId];

        if(pb.cscfga__Basket_Status__c != 'Valid') {
            return 'Synchronise with opportunity failed: product basket has to be valid.';
        }
    
        //unsync first
       
        if (pb.csbb__Synchronised_with_Opportunity__c)
        {
            pb.csbb__Synchronised_with_Opportunity__c=false;
            update pb;
        }
        
        String queryString = 'select ' + CustomButtonSynchronizeWithOpportunity.getSobjectFields('cscfga__Product_Basket__c') +
                             ', cscfga__opportunity__r.pricebook2.isstandard' +
                             ',cscfga__Opportunity__r.CurrencyIsoCode'+
                             ',(select cscfga__product_definition__r.name ' +
                             ', cscfga__product_definition__r.cscfga__product_category__r.name ' +
                             ', ' + CustomButtonSynchronizeWithOpportunity.getSobjectFields('cscfga__Product_Configuration__c') +
                             ' from cscfga__Product_Configurations__r) ' +
                             ' from cscfga__Product_Basket__c where id = \'' + basketId + '\'';

        List<cscfga__Product_Basket__c> productBasketList = Database.query(queryString);
        
                Opportunity opp =
                    [select
                     id,
                     QuoteStatus__c,
                     pricebook2id,
                     (select id
                      from OpportunityLineItems
                     )
                     from Opportunity
                     where id = :productBasketList[0].cscfga__Opportunity__c
                    ];
                    
        // if pricebook attached to the opportunity is not standard delete all opportunity line items and attach opportunity to standard pricebook
        //try{
            if (!productBasketList[0].cscfga__Opportunity__r.Pricebook2.IsStandard 
                /* BB 5/6/2016: multicurrency part for oppty sync */
                || productBasketList[0].CurrencyIsoCode != productBasketList[0].cscfga__Opportunity__r.CurrencyIsoCode) {
                

                if (opp.OpportunityLineItems.size() > 0) {
                    delete opp.OpportunityLineItems;
                }

                // get Id of standard pricebook and attach it to the opportunity
                if (Test.IsRunningTest()) {
                    opp.Pricebook2Id = Test.getStandardPricebookId();
                } else {
                    opp.Pricebook2Id = [select id from Pricebook2 where isstandard = true].Id;
                }
                /* BB 5/6/2016: multicurrency part for oppty sync */
                opp.CurrencyIsoCode = productBasketList[0].CurrencyIsoCode;
            }
        //} catch (exception e){
        //    return 'Synchronise with opportunity failed: There is no associated Opportunity';
        //}
        opp.QuoteStatus__c = productBasketList[0].Quote_Status__c;
         opp.ContractTerm__c = String.valueof(Integer.valueof(ProductBasketList[0].Max_contract_Term__c));
         //Defect#13648
       opp.Total_Contract_Value__c = productBasketList[0].cscfga__total_contract_value__c;
       opp.Total_MRC__c = productBasketList[0].Total_MRC_of_all_PConfig__c;
       opp.Total_NRC__c = productBasketList[0].Total_NRC_allPC__c;
     Util.syncOpp=true;
     TriggerFlags.NoOpportunitySplitTriggers=true;
        update opp;
      
      // clear synchronized with opportunity flag for baskets attached to the opportunity
        productBasketList = /// [ZD] variable reused
            [select
             id,
             csbb__synchronised_with_opportunity__c, csordtelcoa__Synchronised_with_Opportunity__c
             from cscfga__Product_Basket__c
             where
             cscfga__opportunity__c = :productBasketList[0].cscfga__Opportunity__c and
                                      csbb__synchronised_with_opportunity__c = true and
                                              id != :productBasketList[0].Id
            ];

        if (productBasketList.size() > 0) {
            for (Integer i = 0; i < productBasketList.size(); i++) {
                productBasketList[i].csbb__Synchronised_with_Opportunity__c = false;
                productBasketList[i].csordtelcoa__Synchronised_with_Opportunity__c = false;
            }

            update productBasketList;
      }
        //then sync  
        pb.csbb__Synchronised_with_Opportunity__c = true;
        pb.csordtelcoa__Synchronised_with_Opportunity__c = true;

    List<cscfga__Product_Basket__c> synchronizedBaskets = new List<cscfga__Product_Basket__c>();
        List<cscfga__Product_Basket__c> oldBaskets = new List<cscfga__Product_Basket__c>();
        synchronizedBaskets.add(pb);
        oldBaskets.add(new cscfga__Product_Basket__c(
            csbb__Synchronised_with_Opportunity__c = false,
            csordtelcoa__Synchronised_with_Opportunity__c = false
        ));
        
        update pb;
        system.debug('******* syncWithOpportunity unsync productBasketList: '+productBasketList);
        
        AllProductBasketTriggerHandler.UnSyncProductBasketsAfterInsertUpdate(synchronizedBaskets, oldBaskets); 
        system.debug('******* syncWithOpportunity UnSyncProductBasketsAfterInsertUpdate '); 
        AllProductBasketTriggerHandler.DeleteOLIsProductDetailsAfterUpdate(synchronizedBaskets, oldBaskets);
        system.debug('******* syncWithOpportunity DeleteOLIsProductDetailsAfterUpdate'); 
        AllProductBasketTriggerHandler.InsertOLIsProductDetailsAfterUpdate(synchronizedBaskets, oldBaskets);
        system.debug('******* syncWithOpportunity InsertOLIsProductDetailsAfterUpdate'); 
    
          
        Opportunity oppToGoBackTo = [select id from Opportunity where id = :pb.cscfga__Opportunity__c];
    
        PageReference oppPage = new ApexPages.StandardController(oppToGoBackTo).view();
    
        return oppPage.getUrl();
	}catch(Exception e){System.debug(e);return null;}

}

    // Get a comma separated SObject Field list
    public static String getSobjectFields(String so) {
        try{
		String fieldString;
        SObjectType sot = Schema.getGlobalDescribe().get(so);

        List<Schema.SObjectField> fields = sot.getDescribe().fields.getMap().values();

        fieldString = fields[0].getDescribe().LocalName;
        for (Integer i = 1; i < fields.size(); i++) {
            fieldString += ',' + fields[i].getDescribe().LocalName;
        }
        system.debug('---fieldString--'+fieldString);
        
        return fieldString;
		}catch(Exception e){System.debug(e);return null;}
    }
 }