@isTest
Public class MasterServiceNameIdentifierHandlerTest{
    /**
* Disables triggers, validations and workflows for the given user
* @param userId Id
*/
     
    private static voId disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
        system.assertEquals(globalMute.Mute_Workflows__c , true);
    }
    
/**
* Enables triggers, valIdations and workflows 
* @param userId Id
*/
    private static voId enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_ValIdations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_ValIdations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
        system.assertEquals(globalMute.Mute_Workflows__c , false);
    }
//Initializing 
    private static Account act;
    private static Opportunity oppObj;
    private static cscfga__Product_Basket__c pbasket;
    private static List<cscfga__Product_Configuration__c> pclist;

//creating test data
    static void createTestData()
    {       
        //Creating Account object records
        act = new Account(Name='Test112233M1',Customer_Type__c='MNC',Selling_Entity__c ='Telstra Incorporated',cscfga__Active__c='Yes',
                Activated__c= True,
                Account_ID__c = '33331',
                Account_Status__c = 'Active',
                Customer_Legal_Entity_Name__c = 'Test');
        insert act;
        list<Account> acc = [select id,Name from Account where Name = 'Test112233M1'];
        system.assertEquals(act.Name , acc[0].Name);
        System.assert(act!=null);
        //Creating Opportunity object records
         oppObj = new Opportunity(Name='GFTS Ph 2',AccountID=act.Id,Opportunity_Type__c='Simple',
                                           CurrencyIsoCode = 'USD', CloseDate = Date.today(),StageName='Identify & Define',Stage__c='Identify & Define',
                                            QuoteStatus__c ='Approved',Sales_Status__c= 'Won',Win_Loss_Reasons__c ='Product',
                                            Order_Type__c= 'New', ContractTerm__c = '24',Product_Type__c = 'IPL',
                Estimated_MRC__c = 100.00,
                Estimated_NRC__c = 100.00);
        insert oppObj; 
        list<Opportunity> opp = [select id,Name from Opportunity where Name = 'GFTS Ph 2'];
        system.assertEquals(oppObj.Name , opp[0].Name);
        System.assert(oppObj!=null);
        //insert contact object records
         Contact contObj = new Contact(AccountId=act.Id,LastName='tech',
                                  email='test@gmail.com');
        insert contObj; 
        system.assertEquals(contObj.LastName,'tech');
        
        //insert Product Basket object record
        pbasket = new cscfga__Product_Basket__c();
        pbasket.csbb__Account__c = act.id;
        pbasket.csordtelcoa__Account__c = act.id;
        pbasket.cscfga__Opportunity__c = oppObj.id;
        insert pbasket;
        system.assertEquals(pbasket.csbb__Account__c,act.id);
        //insert product definition object record
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.name = 'Master IPVPN Service';
        pd.cscfga__Description__c ='Test master IPVPN';
        insert pd;
        system.assertEquals(pd.name , 'Master IPVPN Service');
        
        cscfga__Product_Definition__c pd1 = new cscfga__Product_Definition__c();
        pd1.name = 'Master VPLS Service';
        pd1.cscfga__Description__c ='Test master VPLS';
        insert pd1;
        system.assertEquals(pd1.name,'Master VPLS Service');
          
        //insert custom setting records
        Product_Definition_Id__c pdIdCustomSetting = new Product_Definition_Id__c();
        pdIdCustomSetting.Name = 'Master_IPVPN_Service_Definition_Id';
        pdIdCustomSetting.Product_Id__c =pd.Id;
        insert pdIdCustomSetting;
        system.assertEquals(pdIdCustomSetting.Name , 'Master_IPVPN_Service_Definition_Id');
        
        Product_Definition_Id__c pdIdCustomSetting1 = new Product_Definition_Id__c();
        pdIdCustomSetting1.Name = 'Master_VPLS_Service_Definition_Id';
        pdIdCustomSetting1.Product_Id__c =pd1.Id;
        insert pdIdCustomSetting1;
        system.assertEquals(pdIdCustomSetting1.Name , 'Master_VPLS_Service_Definition_Id');
        
        //insert product configuration record for Master IPVPN Service
        pclist= new List<cscfga__Product_Configuration__c>{
                     new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd.id,cscfga__Product_Basket__c = pbasket.Id),
            new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd1.id,cscfga__Product_Basket__c = pbasket.Id,Solution_Type__c='IPVPN'),
            new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd1.id,cscfga__Product_Basket__c = pbasket.Id,Solution_Type__c='Transparent Mode'),
            new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd1.id,cscfga__Product_Basket__c = pbasket.Id,Solution_Type__c='VLAN Mode')
           };
        insert pclist;
       system.assertEquals(true,pclist!=null); 
            
    }
    
    private static testmethod void  masterServiceNameIdentifier()
    
    {
    
        Profile profObj = [select Id from Profile  where name ='System Administrator'];
        User userObj = new User(profileId = profObj.id, username = 'test@telstra.com',
            email = 'test@telstra.com', emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
            alias='nuser1',  lastname='lastname1',EmployeeNumber='123456789',Region__c='Australia');
            insert userObj;
            system.assertEquals(true,userObj!=null); 
        System.RunAs(userObj)
        {
            disableAll(userObj.Id);
            createTestData();
            enableAll(userObj.Id);
            Test.startTest();
            MasterServiceNameIdentifierHandler.masterServiceNameIdentifierlogic(pclist);
            Test.stopTest();
        }   
    }
    
}