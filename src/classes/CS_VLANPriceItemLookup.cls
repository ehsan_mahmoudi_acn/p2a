global with sharing class CS_VLANPriceItemLookup extends cscfga.ALookupSearch {
    
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){

        String cityID = searchFields.get('PortPOP City');
        String countryId = searchFields.get('VLAN Port Country');
        String accountID = searchFields.get('Account Id');
        String portSpeedID = searchFields.get('VLAN speed');
        String IPVPNtype = searchFields.get('VLAN type');
        String onnetOffnet = searchFields.get('Onnet or Offnet');
        System.Debug('CS_VLANPriceItemLookup : doDynamicLookupSearch searchFields' + searchFields);
        
        
        List<cspmb__Price_Item__c> cityData = new List<cspmb__Price_Item__c>();
        List<cspmb__Price_Item__c> data;
		

        data = getPriceItems(searchFields, new Map<String,Object>());
        
        System.Debug('CS_VLANPriceItemLookup : doDynamicLookupSearch data' + data);
        if(cityID != '' && cityID != null){
        	for(cspmb__Price_Item__c item : data){
        		if(item.City__c == cityID){
        			cityData.add(item);
        		}
        	}
        }
        System.Debug('CS_VLANPriceItemLookup : doDynamicLookupSearch cityData' + cityData);
        
        if(!cityData.isEmpty()){ data = cityData; }

        return getAccountPricing(data, accountID);
	}

	public override String getRequiredAttributes(){ 
	    return '["Product Type", "Account Id","VLAN type","PortPOP City","VLAN Port Country","VLAN speed", "Account Customer Type","Onnet or Offnet"]';
	}

	public List<cspmb__Price_Item__c> getAccountPricing(List<cspmb__Price_Item__c> data, String accountID){
		List<cspmb__Price_Item__c> returnData = new List<cspmb__Price_Item__c>();
		for(cspmb__Price_Item__c item : data){
			if(item.cspmb__Account__c == accountID){
				returnData.add(item);
				return returnData;
			} else if (item.cspmb__Account__c == null){
				returnData.add(item);
			}
		}
		return returnData;
	}
	public List<cspmb__Price_Item__c> getPriceItems(Map<String, String> attributesValues, Map<String,Object> wherePart){
		
		String portType = attributesValues.get('VLAN type') == 'Burstable' ? 'Standard' : attributesValues.get('VLAN type');
		
		String countryID = attributesValues.get('VLAN Port Country');
		String productType = attributesValues.get('Product Type');
		String portSpeed = attributesValues.get('VLAN speed');
		String connectivity = attributesValues.get('Onnet or Offnet');
		String customerType = attributesValues.get('Account Customer Type');
		System.Debug('CS_VLANPriceItemLookup : getPriceItems attributesValues' + portType +' - '+ countryID +' - '+ portSpeed +' - '+ connectivity+' - '+ customerType);
		String query = 'SELECT Id, cspmb__One_Off_Cost__c,'+
							'cspmb__Recurring_Cost__c,'+
							'cspmb__One_Off_Charge__c,'+
							'cspmb__Recurring_Charge__c,'+
							'cspmb__Account__c,'+
							'Country__c,'+
							'City__c,'+
							'Critical_NRC__c,'+
							'Critical_MRC__c,'+
							'CS_Bandwidth_Product_Type__c,'+
							'Interactive_MRC__c,'+
							'Interactive_NRC__c,'+
							'IPVPN_Type__c,'+
							'cspmb__Is_Active__c,'+
							'Low_Priority_MRC__c,'+
							'Low_Priority_NRC__c,'+
							'Pricing_Segment__c,'+
							'Standard_MRC__c,'+
							'Standard_NRC__c,'+
							'Video_MRC__c,'+
							'Video_NRC__c,'+
							'Voice_MRC__c,'+
							'Voice_MRC_Cost__c,'+
							'Video_MRC_Cost__c,'+
							'Standard_MRC_Cost__c,'+
							'Low_Priority_MRC_Cost__c,'+
							'Interactive_MRC_Cost__c,'+
							'Critical_MRC_Cost__c,'+
							'Trans_Tasman_Type__c,'+
							'Connectivity__c,'+
							'BIP_Connection_Type__c,'+
							'Voice_NRC__c '+
						'FROM cspmb__Price_Item__c '+
        				'WHERE Country__c =:countryID'+
        					' AND CS_Bandwidth_Product_Type__c=:portSpeed'+
        					' AND cspmb__Is_Active__c= true'+
        					' AND IPVPN_Type__c =:portType'+
        					' AND Connectivity__c =:connectivity'+
        					' AND Pricing_Segment__c = :customerType'+
        					' AND Generic_Product_Type__c = :productType';

     	System.Debug('CS_VLANPriceItemLookup : getPriceItems query1: ' + query);
        String whereQuery = '';
        for(String key : wherePart.keySet()){
        	Object value = wherePart.get(key);

        	if(value instanceof String){
        		whereQuery += ' AND '+key+' = \''+value + '\'';
        	} else if (value instanceof Integer || value instanceof Boolean){
        		whereQuery += ' AND '+key+' = '+value;
        	}
        }
        query += whereQuery;
        System.Debug('CS_VLANPriceItemLookup : getPriceItems query2: ' + query);
		
        return Database.query(query);
	}
}