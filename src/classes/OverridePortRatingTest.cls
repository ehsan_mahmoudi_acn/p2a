@isTest(seealldata=false)
private class OverridePortRatingTest{
    private static List<cscfga__Product_Configuration__c> pclist;
    private static List<cscfga__Product_Definition__c> Pdlist;
   
    static testmethod void unittest() {
    P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
    
        P2A_TestFactoryCls.sampleTestData();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        accList[0].Customer_Type__c='MNC';
        accList[0].Customer_Type_New__c = 'MNC';
        upsert accList[0];
        system.assertEquals(accList[0].Customer_Type__c,'MNC');

        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        oppList[0].Name = 'Opp1';
        oppList[0].CloseDate = system.today()+10;
        //oppList[0].Customer_Type__c = 'MNC';
        oppList[0].stagename = 'Identify & Define';
        oppList[0].Probability = 30;
        oppList[0].csordtelcoa__Change_Type__c = 'Upgrade';
        oppList[0].Product_Type__c = 'CPE';
        oppList[0].Quote_Simplification__c = false;
        oppList[0].Win_Loss_Reasons__c = 'Cancelled';
        oppList[0].Sales_Status__c = 'Lost';
        oppList[0].UIFlag__c = 256;
        oppList[0].Stage__c = 'InFlight';
        oppList[0].Internet_Product__c = false;
        upsert oppList[0];
        system.assertEquals(oppList[0].Product_Type__c , 'CPE');
        
        list<cscfga__Product_Basket__c> pblist  = new list<cscfga__Product_Basket__c>();
        cscfga__Product_Basket__c p = new cscfga__Product_Basket__c();
        p.csordtelcoa__Synchronised_with_Opportunity__c = true;
        p.Name = 'Test basket';
        p.csordtelcoa__Account__c = accList[0].id;
        p.cscfga__Opportunity__c = oppList[0].id;
        pblist.add(p);
        insert pblist;
        system.assertEquals(p.Name , 'Test basket');
        
        List<cscfga__Product_Definition__c> Pdlist1 = P2A_TestFactoryCls.getProductdef(1);
        Pdlist1[0].name = 'TWI Singlehome';
        upsert  Pdlist1[0];
        system.assertEquals(Pdlist1[0].name , 'TWI Singlehome');
        
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,pblist,Pdlist1,pbundlelist,Offerlists);
        proconfig[0].name ='TWI';
        proconfig[0].Internet_Onnet_Port_Rating__c = 'Burstable - 95th percentile';
        upsert proconfig[0];
        system.assertEquals(proconfig[0].name ,'TWI');
        
        List<cscfga__Configuration_Screen__c> ConfigLst = P2A_TestFactoryCls.getConfigScreen(1, Pdlist1);
        List<cscfga__Screen_Section__c> ssList1 = P2A_TestFactoryCls.getScreenSec(1, ConfigLst);
        List<cscfga__Attribute_Definition__c > Attributedeflist = P2A_TestFactoryCls.getAttributesdef(1,proconfig,Pdlist1,ConfigLst,ssList1);
               
        Test.startTest();
        
        string str = proconfig[0].id;
        string str1 = Pdlist1[0].Name; 
        string str3 = accList[0].id; 
        string str4 = accList[0].Customer_Type_New__c;
        string str5  = 'value';
        
        Set<String> checkValues = new Set<String>();
        CheckRecursive.canExecuteBlocking(str5);
        
        //CheckRecursive crs = new CheckRecursive();
        //crs.checkValues;
        
        pclist= new List<cscfga__Product_Configuration__c>{
                                                    
             new cscfga__Product_Configuration__c(name = 'IPT Multihome',cscfga__Product_Definition__c = Pdlist1[0].id,
                                                    cscfga__Parent_Configuration__c = proconfig[0].Id,cscfga__Product_Basket__c = pblist[0].Id,cscfga__Product_Family__c = 'Test family',Added_Ports__c = str
                                                    ,Master_IPVPN_Configuration__c = proconfig[0].Id,Internet_Onnet_Port_Rating__c = 'Burstable - 95th percentile',Product_Code__c = 'IPC',Product_Id__c ='EPLSWCS-STRM',EVPLAEndPop__c = 'SYDP'/*Order_Type__c = 'New Provide'*/),
             
             new cscfga__Product_Configuration__c(name = 'TWI Multihome',cscfga__Product_Definition__c = Pdlist1[0].id,
                                                    cscfga__Parent_Configuration__c = proconfig[0].Id,cscfga__Product_Basket__c = pblist[0].Id,cscfga__Product_Family__c = 'Test family',Added_Ports__c = str
                                                    ,Master_IPVPN_Configuration__c = proconfig[0].Id,Internet_Onnet_Port_Rating__c = 'Burstable - 95th percentile',Product_Code__c = 'IPM',Product_Id__c ='EPLSWCS-STRM',EVPLAEndPop__c = 'SYDP'/*Order_Type__c = 'New Provide'*/)                            
            };
        insert pclist;
        integer i = [Select count() from cscfga__Product_Configuration__c];
        system.assert(i>0);
          
      cscfga__Attribute__c Attributes1 = new cscfga__Attribute__c();
      Attributes1.name = 'Port Rating Type';
      Attributes1.cscfga__Product_Configuration__c = str;
      Attributes1.cscfga__Value__c = 'Burstable - 95th percentile';
      Attributes1.cscfga__Display_Value__c = 'Burstable - 95th percentile';
      Attributes1.cscfga__Attribute_Definition__c = Attributedeflist[0].id; 
      insert Attributes1;
      
     /* cscfga__Attribute__c Attributes2 = new cscfga__Attribute__c();
      Attributes2.name = 'Account Id';
      Attributes2.cscfga__Product_Configuration__c= pclist[0].id;
      Attributes2.cscfga__Value__c = 'Burstable - 95th percentile';
      Attributes2.cscfga__Display_Value__c = 'Burstable - 95th percentile';
      Attributes2.cscfga__Attribute_Definition__c = Attributedeflist[0].id; 
      insert Attributes2;
      
      cscfga__Attribute__c Attributes3 = new cscfga__Attribute__c();
      Attributes3.name = 'AccountType';
      Attributes3.cscfga__Product_Configuration__c= pclist[0].Id;
      Attributes3.cscfga__Value__c = str3;
      Attributes3.cscfga__Display_Value__c = str3;
      Attributes3.cscfga__Attribute_Definition__c = Attributedeflist[0].id; 
      insert Attributes3;
      
      cscfga__Attribute__c Attributes4 = new cscfga__Attribute__c();
      Attributes4.name = 'AccountType';
      Attributes4.cscfga__Product_Configuration__c= pclist[0].Id;
      Attributes4.cscfga__Value__c = null;
      Attributes4.cscfga__Display_Value__c = str4;
      Attributes4.cscfga__Attribute_Definition__c = Attributedeflist[0].id; 
      insert Attributes4; */
      
      OverridePortRating.afterUpdate(pclist);
      
    }

 }