public with sharing class OrderLineItemExtension{
   Public String oliId;
   Private String ErrorMessage = '';
   set<Id> ordId=new set<Id>();
   List<Id> ListId=new List<Id>();
   List<Order_Line_Item__c> lstoli=new List<Order_Line_Item__c>();
   Public Boolean isError{get;set;}
   Public Integer Counter{get;set;}
   //public string usr1{get;set;}
   public string orderName{get;set;}
   public String BillName{get;set;}
   public String BillingSME{get;set;}
   public String BillingSMEEmail{get;set;}
  
   public String BillingSMEmobile{get;set;}
   public user usr{get;set;}
     
  // Public user usr{get;set;}
   
   public void setErrorMessage(String error) {
          this.isError  = true;
          this.ErrorMessage=error;
          
    }
    
    public String getErrorMessage() {
         return this.ErrorMessage;
    }
    
    public void sendEmail()
    {
        List<User> usrLst =[Select Name, email,Region__c from User where id= :UserInfo.getUserId()];
        if(usrLst != null && usrLst.size() > 0){
            usr = usrLst[0];
        }
        EmailGenerationClass.emailSending(usr,oliId);
       // GenericEmailSending obj=new GenericEmailSending(); 
              
        setErrorMessage(Label.BIlling_SME_Error_msg);
         
       
            
        
    }
 
    public OrderLineItemExtension(ApexPages.StandardController controller){
        System.debug('I am in OrderLineItemExtension');
        oliId = System.currentPageReference().getParameters().get('id');
        isError = false;      
        // setErrorMessage('There is an error '+oliId);
        Counter = 0; 
        String emailAddresses= '';    
        
        lstoli=[Select id,ParentOrder__c,ParentOrder__r.Name,Bill_Profile__c,Billing_Entity__c from Order_Line_Item__c where Id=:oliId limit 1];
        List<BillProfile__c> lstBprofile=new List<BillProfile__c>();
        
        //getting bill profile users details
         List<User> usrLst =[Select Name, email,Region__c,MobilePhone from User where id= :UserInfo.getUserId()];
         if(usrLst != null && usrLst.size()>0){
            usr = usrLst[0];
        }
        // query to get all queues corresponding to Bill profile Object
            List<QueueSobject> que = [Select Id,QueueId, Queue.Name from QueueSobject where SobjectType='BillProfile__c'];
            
            Id queid;
            String billTeam;
            
        // IR 215 start
            String queueName = null;
            String billingEntiy = null;
            
            for(Order_Line_Item__c oli:lstoli){
            ordId.add(oli.ParentOrder__c);
            ListId.add(oli.Bill_Profile__c);
            billingEntiy = oli.Billing_Entity__c;
        }
            
            System.debug('billingEntiy: '+billingEntiy);
            
            if(billingEntiy!=null && System.Label.PACNET_Entities.contains(billingEntiy)){
                  queueName = 'Billing - PACNET';
              }else{                
                queueName = usr.Region__c;
              }
           // IR 215 end 
            
        Map<String,Id> queMap= new Map<String,Id> ();
            
            //Get Billing Team Queue id corresponding to sales user region.
            for (QueueSObject q:que){
                if((q.Queue.Name).contains(queueName)){
                    queMap.put(q.Queue.Name,q.QueueId);
                    queid = q.QueueId;
                    billTeam = q.Queue.Name;
                   // System.debug('queue name ..'+q.Queue.Name);
                }
            }

         String groupMemberQuery = 'SELECT GroupId, Id, SystemModstamp, UserOrGroupId FROM GroupMember where GroupId =\''+queid+'\'';
            
            //Getting the all Users id's  corresponding to queue
            List<GroupMember> list_GM = Database.query(groupMemberQuery);
            
            List<Id> lst_Ids = new List<ID>();
            for(GroupMember gmObj : list_GM){
                lst_Ids.add(gmObj.UserOrGroupId);
            }
            //Query the Billing Team Users 
            List<User> lst_UserObj = [Select Id,name,Email,Phone from User where id in :lst_Ids];
            
            //Here concanating all email addresses of Users belong to Billing Queue
            for(User usrObj : lst_UserObj){
                if(emailAddresses == ''){
                    emailAddresses = usrObj.Email;
                }else{
                    emailAddresses += ':'+usrObj.Email; 
                }
            }   

       /* for(Order_Line_Item__c oli:lstoli){
            ordId.add(oli.ParentOrder__c);
            ListId.add(oli.Bill_Profile__c);
        } */
        
        lstBprofile=[Select id,Name, Activated__c from BillProfile__c where Id IN :ListId and Activated__c=:False];
        if(lstBprofile.size()!=0 && lst_UserObj != null && lst_UserObj.size() > 0){
             Counter = lstBprofile.size();    
             orderName= lstoli[0].ParentOrder__r.Name;   
             BillName= lstBprofile[0].Name ; 
            BillingSME=lst_UserObj[0].Name;
            BillingSMEEmail=lst_UserObj[0].email;
             //BillingSMEEmail='le.nguyen@team.telstra.com';
            BillingSMEMobile=lst_UserObj[0].Phone; 
        }else if(lstBprofile.size() == 0){
            setErrorMessage('All the bill profile(s) for this order are already activated, hence system will not send any notification to the billing team.');
        }else{
            setErrorMessage('There is no Queue members in this region');
        }

    }
  
}