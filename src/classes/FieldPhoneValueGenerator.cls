public class FieldPhoneValueGenerator implements FieldValueGenerator {
    
    public Boolean canGenerateValueFor(Schema.DescribeFieldResult fieldDesc) {
        return Schema.DisplayType.PHONE == fieldDesc.getType();
    }
    
    public Object generate(Schema.DescribeFieldResult fieldDesc) {
        return '+61 12143425324';
    }
}