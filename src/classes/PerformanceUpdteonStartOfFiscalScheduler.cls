global class PerformanceUpdteonStartOfFiscalScheduler implements Schedulable{

    public static String sched = '0 00 00 1 7 ? *';  //Start of every fiscal year
   // public static String sched = '0 00 00 10 12 ? *';  //Start of every fiscal year
//SELECT Id, CronJobDetail.Name, NextFireTime, PreviousFireTime, State, StartTime, EndTime, CronExpression, TimeZoneSidKey, OwnerId, LastModifiedById, CreatedById, CreatedDate, TimesTriggered FROM CronTrigger where OwnerId='00590000001RkKN'
  //system.abortjob(cronid);  
    global static String scheduleMe() {
        PerformanceUpdteonStartOfFiscalScheduler SC = new PerformanceUpdteonStartOfFiscalScheduler(); 
        return System.schedule('Performance Tarcking Object Update Job', sched, SC);
    }

    global void execute(SchedulableContext sc) {

        BatchUpdateForPerformanceTracking b1 = new BatchUpdateForPerformanceTracking();
        ID batchprocessid = Database.executeBatch(b1,200);           
    }
}