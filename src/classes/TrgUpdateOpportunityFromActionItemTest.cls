@isTest(seeAllData=false)

public class TrgUpdateOpportunityFromActionItemTest {

    static Account acc;
    static Opportunity opp;
    static Country_Lookup__c cl;
    static City_Lookup__c ci;
    static Site__c s;
    static Contact con;
    static Action_Item__c at1;
    
    static testMethod void testUpdateOpportunity() {
        
        Map<Id,String> rtMap = new Map<Id,String>{getRecordTypeRCC() => 'Request Credit Check', getRecordTypeRFS() => 'Request Feasibility Study', getRecordTypeRPA() => 'Request Pricing Approval', getRecordTypeRTPQ() => 'Request Third Party Quote', getRecordTypeSGRR() => 'Stage Gate Review Request'};
        Map<Id,String> opMap = new Map<Id, String>();
        List<Opportunity> opList = new List<Opportunity>();
        
        List<Action_Item__c> acList = new List<Action_Item__c>();
        Action_Item__c ai1 = new Action_Item__c();
        Action_Item__c ai2 = new Action_Item__c();
        Action_Item__c ai3 = new Action_Item__c();
        Action_Item__c ai4 = new Action_Item__c();
        Action_Item__c ai5_1 = new Action_Item__c();
        Action_Item__c ai5_2 = new Action_Item__c();
        Action_Item__c ai5_3 = new Action_Item__c();
        Action_Item__c ai5_4 = new Action_Item__c();
        Action_Item__c ai6_1 = new Action_Item__c();
        Action_Item__c ai6_2 = new Action_Item__c();
        Action_Item__c ai6_3 = new Action_Item__c();
        Action_Item__c ai6_4 = new Action_Item__c();
        Action_Item__c ai7 = new Action_Item__c();

        ai1.RecordTypeId = getRecordTypeRCC();
        ai1.Opportunity__c = getOpportunity().Id;
        ai1.Status__c = 'Completed';
        ai1.CreditLimitValidity__c= System.today();
        ai1.SecurityDeposit__c = 100.00;
        ai1.SecurityDepositHoldingPeriod__c = System.today();
        ai1.Completed_By__c = 'Sumit Suman';
        ai1.Completion_Date__c = System.today();
        ai1.Feedback__c = 'Approved';
        acList.add(ai1);
    
        ai2.RecordTypeId = getRecordTypeRFS();
        ai2.Opportunity__c = getOpportunity().Id;
        ai2.Status__c = 'Completed';
        ai2.Completed_By__c = 'sumit';
        ai2.Completion_Date__c = System.today();
        ai2.F_Expiration_Date__c = System.today();
        ai2.FeasibilityResult__c = 'Sumit Suman';
        acList.add(ai2);
        
        ai3.RecordTypeId = getRecordTypeRPA();
        ai3.Opportunity__c = getOpportunity().Id;
        ai3.Status__c = 'Completed';
        ai3.Completed_By__c = 'sumit';
        ai3.Completion_Date__c = System.today();
        ai3.Pricing_Feedback__c = 'xyz';
        acList.add(ai3);
    
        ai4.RecordTypeId = getRecordTypeRTPQ();
        ai4.Opportunity__c = getOpportunity().Id;
        ai4.Status__c = 'Completed';
        ai4.Completed_By__c = 'sumit';
        ai4.Completion_Date__c = System.today();
        ai4.Feedback__c = 'xyz';
        acList.add(ai4);        
    
        ai5_1.RecordTypeId = getRecordTypeSGRR();
        ai5_1.Opportunity__c = getOpportunity().Id;
        ai5_1.Stage_Gate_Number__c = 1;
        ai5_1.Status__c = 'Completed';
        acList.add(ai5_1);
        
        ai5_2.RecordTypeId = getRecordTypeSGRR();
        ai5_2.Opportunity__c = getOpportunity().Id;
        ai5_2.Stage_Gate_Number__c = 2;
        ai5_2.Status__c = 'Completed';
        acList.add(ai5_2);

        ai5_3.RecordTypeId = getRecordTypeSGRR();
        ai5_3.Opportunity__c = getOpportunity().Id;
        ai5_3.Stage_Gate_Number__c = 3;
        ai5_3.Status__c = 'Completed';
        acList.add(ai5_3);

        ai5_4.RecordTypeId = getRecordTypeSGRR();
        ai5_4.Opportunity__c = getOpportunity().Id;
        ai5_4.Stage_Gate_Number__c = 4;
        ai5_4.Status__c = 'Completed';
        acList.add(ai5_4);

        ai6_1.RecordTypeId = getRecordTypeSGRR();
        ai6_1.Opportunity__c = getOpportunity().Id;
        ai6_1.Stage_Gate_Number__c = 1;
        ai6_1.Status__c = 'Assigned';
        acList.add(ai6_1);
        
        ai6_2.RecordTypeId = getRecordTypeSGRR();
        ai6_2.Opportunity__c = getOpportunity().Id;
        ai6_2.Stage_Gate_Number__c = 2;
        ai6_2.Status__c = 'Assigned';
        acList.add(ai6_2);

        ai6_3.RecordTypeId = getRecordTypeSGRR();
        ai6_3.Opportunity__c = getOpportunity().Id;
        ai6_3.Stage_Gate_Number__c = 3;
        ai6_3.Status__c = 'Assigned';
        acList.add(ai6_3);

        ai6_4.RecordTypeId = getRecordTypeSGRR();
        ai6_4.Opportunity__c = getOpportunity().Id;
        ai6_4.Stage_Gate_Number__c = 4;
        ai6_4.Status__c = 'Assigned';
        acList.add(ai6_4);
    

        ai7.Opportunity__c = getOpportunity().Id;
        ai7.Status__c = 'ThrowExp';
        ai7.CreditLimitValidity__c= System.today();
        ai7.SecurityDeposit__c = 100.00;
        ai7.SecurityDepositHoldingPeriod__c = System.today();
        ai7.Completed_By__c = '';
        ai7.Completion_Date__c = System.today();
        ai7.Feedback__c = '';
        acList.add(ai7);
        
        Test.startTest();
        insert acList;
        system.assert(acList!=null);
        
    /*    
    for(RecordType rt :[select Id,Name from RecordType Where SobjectType = 'Action_Item__c' ]){   
        rtMap.put(rt.Id,rt.Name);
    }
    */
    for(Opportunity op :opList){   
        opMap.put(op.Id,op.Name);
    }
            
    for(Action_Item__c aitem :acList){
    try{
        
        if(rtMap.get(aitem.RecordTypeId) == 'Request Credit Check' && aitem.Status__c=='Completed'){
            if(opMap.get(aitem.Opportunity__c) != null){ 
                Opportunity op = new Opportunity(Id = aitem.Opportunity__c,CreditCheckStatus__c=aitem.Status__c,CreditCheckValidity__c=aitem.CreditLimitValidity__c,SecurityDeposit__c=aitem.SecurityDeposit__c,SecurityDepositHoldingPeriod__c=aitem.SecurityDepositHoldingPeriod__c,CreditCheckDoneBy__c=aitem.Completed_By__c,CreditCheckCompletedDate__c=aitem.Completion_Date__c,Credit_Check_Feedback__c =aitem.Feedback__c);
                opList.add(op);
                }
            }else if(rtMap.get(aitem.RecordTypeId) == 'Request Feasibility Study' && aitem.Status__c=='Completed'){
                if(opMap.get(aitem.Opportunity__c) != null){
                   Opportunity op = new Opportunity(Id = aitem.Opportunity__c,FeasibilityStatus__c=aitem.Status__c,FeasibilityDoneBy__c=aitem.Completed_By__c,FeasibilityCompletedDate__c=aitem.Completion_Date__c, F_Expiration_Date__c=aitem.F_Expiration_Date__c,Feasibility_Result__c =aitem.FeasibilityResult__c);
                   opList.add(op);
                }
             } else if(rtMap.get(aitem.RecordTypeId) == 'Request Pricing Approval' && aitem.Status__c=='Completed'){
                if(opMap.get(aitem.Opportunity__c) != null){
                    Opportunity op = new Opportunity(Id = aitem.Opportunity__c,PricingApprovalStatus__c=aitem.Status__c,PricingApprovalDoneBy__c=aitem.Completed_By__c,PricingApprovalCompletedDate__c=aitem.Completion_Date__c,Pricing_Approval_Feedback__c =aitem.Pricing_Feedback__c);
                    opList.add(op);
                }
             } else if(rtMap.get(aitem.RecordTypeId) == 'Request Third Party Quote' && aitem.Status__c=='Completed'){
                if(opMap.get(aitem.Opportunity__c) != null){
                    Opportunity op = new Opportunity(Id = aitem.Opportunity__c,CarrierQuoteStatus__c=aitem.Status__c,CarrierQuoteDoneBy__c=aitem.Completed_By__c,CarrierQuoteCompletedDate__c=aitem.Completion_Date__c, Third_Party_Feedback__c = aitem.Feedback__c);
                    opList.add(op);
                }
             } else if(rtMap.get(aitem.RecordTypeId) == 'Stage Gate Review Request' && aitem.Status__c=='Completed'){
                if(opMap.get(aitem.Opportunity__c) != null){
                    if(aitem.Stage_Gate_Number__c == 1){
                        Opportunity op = new Opportunity(Id = aitem.Opportunity__c,Stage_Gate_1_Review_Completed__c = true);
                        opList.add(op);
                    }
                    else if(aitem.Stage_Gate_Number__c == 2){
                        Opportunity op = new Opportunity(Id = aitem.Opportunity__c,Stage_Gate_2_Review_Completed__c = true);
                        opList.add(op);
                    }
                    else if(aitem.Stage_Gate_Number__c == 3){
                        Opportunity op = new Opportunity(Id = aitem.Opportunity__c,Stage_Gate_3_Review_Completed__c = true);
                        opList.add(op);
                    }
                    else if(aitem.Stage_Gate_Number__c == 4){
                        Opportunity op = new Opportunity(Id = aitem.Opportunity__c,Stage_Gate_4_Review_Completed__c = true);
                        opList.add(op);
                    }                   
                }
            } else  if(rtMap.get(aitem.RecordTypeId) == 'Stage Gate Review Request' && aitem.Status__c=='Assigned'){
                if(opMap.get(aitem.Opportunity__c) != null){
                    if(aitem.Stage_Gate_Number__c == 1){
                        Opportunity op = new Opportunity(Id = aitem.Opportunity__c,Stage_Gate_1_Action_Created__c = true);
                        opList.add(op);
                    }
                    else if(aitem.Stage_Gate_Number__c == 2){
                        Opportunity op = new Opportunity(Id = aitem.Opportunity__c,Stage_Gate_2_Action_Created__c = true);
                        opList.add(op);
                    }
                    else if(aitem.Stage_Gate_Number__c == 3){
                        Opportunity op = new Opportunity(Id = aitem.Opportunity__c,Stage_Gate_3_Action_Created__c = true);
                        opList.add(op);
                    }
                    else if(aitem.Stage_Gate_Number__c == 4){
                        Opportunity op = new Opportunity(Id = aitem.Opportunity__c,Stage_Gate_4_Action_Created__c = true);
                        opList.add(op);
                    }
                    
                }
            } else if(aitem.Status__c=='ThrowExp'){
                    Opportunity op = new Opportunity(Id = aitem.Opportunity__c,Stage_Gate_4_Action_Created__c = false);
                    opList.add(op);
            }
        }

       catch(Exception excp) {
                System.debug('Error In Action Item Trigger');       
                CreateApexErrorLog.InsertHandleException('Trigger','TrgUpdateOpportunityFromActionItem',excp.getMessage(),'Action_Item__c',Userinfo.getName());
                ErrorHandlerException.ExecutingClassName='TestTrgUpdateOpportunityFromActionItem:TestUpdateOpportunity';         
                ErrorHandlerException.sendException(excp); 
       }
       }
      //Test.startTest();
        if(opList.size() > 0 ) {
            try{
                    upsert opList;//changed
            }catch(Exception excp){
            
                CreateApexErrorLog.InsertHandleException('Trigger','TrgUpdateOpportunityFromActionItem',excp.getMessage(),'Action_Item__c',Userinfo.getName());
                ErrorHandlerException.ExecutingClassName='TestTrgUpdateOpportunityFromActionItem:TestUpdateOpportunity';         
                ErrorHandlerException.sendException(excp); 
            }
        }
      Test.stopTest();
    }
      private static Opportunity getOpportunity(){
        if(opp == null) {
            opp = new Opportunity();
            acc =getAccount();
            //opp.Id= getRecordType().id;
            opp.Name = 'test';
            opp.Opportunity_Type__c = 'Simple';
            opp.Sales_Status__c = 'Open';
            opp.AccountId = acc.id ; //'001O000000qapH2';
            opp.Order_Type__c = 'New';
            opp.StageName = 'Identify & Define';
            opp.Stage__c='Identify & Define';
            opp.CloseDate = System.today();
            opp.Estimated_MRC__c=800;
            opp.Estimated_NRC__c=1000;
            opp.ContractTerm__c='10';
                        
            insert opp;
            system.assert(opp!=null);
            system.assertEquals(opp.Name , 'test'); 
        }
        return opp;
    }
    
    private static Account getAccount(){
        if(acc == null){    
            acc = new Account();
            cl = getCountry();
            acc.Name = 'Test Account';
            acc.Customer_Type__c = 'MNC';
            acc.Country__c = cl.Id;
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Activated__c = True;
            acc.Account_Status__c = 'Active';
            acc.Billing_Status__c = 'Active';
            acc.Account_ID__c = '9090';
            acc.Customer_Legal_Entity_Name__c='Test';
            insert acc;
            system.assert(acc!=null);
            system.assertEquals(acc.Name , 'Test Account');
        }
        return acc;
    }
    private static Country_Lookup__c getCountry(){
        if(cl == null){ 
            cl = new Country_Lookup__c();
            cl.CCMS_Country_Code__c = 'SG';
            cl.CCMS_Country_Name__c = 'India';
            cl.Country_Code__c = 'SG';
            insert cl;
            system.assert(cl!=null);
        }
        return cl;
    }
    private static Action_Item__c getRecordType(){
        if(at1 == null){ 
            at1 = new Action_Item__c();
            //at1.RecordTypeId = 'SG';

            insert at1;
            system.assert(at1!=null);
        }
        return at1;
    }
    /*RecordType getRecordTypeRCC(){

        RecordType rt = new RecordType();
        rt.Name = 'Request Credit Check';
        insert rt;
        return rt;
    }*/
    private static Id getRecordTypeRCC(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Request Credit Check').getRecordTypeId();
        return rtId;
    }
    private static Id getRecordTypeRFS(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Request Feasibility Study').getRecordTypeId();
        return rtId;
    }
    private static Id getRecordTypeRPA(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Request Pricing Approval').getRecordTypeId();
        return rtId;
    }
    private static Id getRecordTypeRTPQ(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Request Third Party Quote').getRecordTypeId();
        return rtId;
    }
    private static Id getRecordTypeSGRR(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Stage Gate Review Request').getRecordTypeId();
        return rtId;
    }
}