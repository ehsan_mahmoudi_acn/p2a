@isTest
public class OrderSummaryPageExtensionControllerTest{
    public static Order__c ord;
    public static City_Lookup__c city;
        static testMethod void getOrderDetailsTest() {
    
        ord = getOrder();
        system.debug('order****'+ ord.id);

      //  List<Order_Line_Item__c> oli = getOrderLineItem();
        PageReference pageRef = Page.orderSummaryPage;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(ord);
        orderSummaryPageExtensionController controller = new orderSummaryPageExtensionController(new ApexPages.StandardController(ord));
        ApexPages.currentPage().getParameters().put('id', ord.id);
        controller.getOliList();
        controller.previous();
        controller.next(); 
        controller.end();  
        controller.getMyCommandButtons();
        controller.refreshGrid();
        controller.getPageNumber();
        controller.getTotal_size();
        controller.getDisablePrevious();
        controller.getDisableNext();
        controller.getTotalPages();
        System.assert(controller != null);

              
    }
    
      private static Order__c getOrder(){
        if(ord == null){
        ord = new Order__c();
        Opportunity o = getOpportunity();
        ord.Opportunity__c = o.Id;
        //ord.name = 'TestODR-99999';
        insert ord;
        //System.assertEquals('TestODR-99999',ord.name);
        }
        return ord;
    }
    
    private static List<Order_Line_Item__c> getOrderLineItem(){
        
        List<Order_Line_Item__c> ordli = new List<Order_Line_Item__c>();
        ord = getOrder();
        Order_Line_Item__c oli1 = new Order_Line_Item__c();
        oli1.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli1.CPQItem__c = '1';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli1.Line_Item_Status__c = 'New';
        oli1.OrderType__c = 'New Provide';
        oli1.Master_Service_ID__c = 'System Of A Down';
        oli1.Primary_Service_ID__c = 'Chop Suey';
        oli1.Path_ID__c = '';
        oli1.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli1.Path_Status__c = 'new';
        oli1.Net_NRC_Price__c = 100;
        oli1.Net_MRC_Price__c = 20;
        oli1.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli1);
        
        Order_Line_Item__c oli2 = new Order_Line_Item__c();
        //Order__c ord = getOrder();
        oli2.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli2.CPQItem__c = '1.1';
        /*oli2.Product_Name__c = 'Intranet Service';
        oli2.name = 'OLI-078966';*/
        oli2.Line_Item_Status__c = 'New';
        oli2.OrderType__c = 'New Provide';
        oli2.Master_Service_ID__c = '';
        oli2.Primary_Service_ID__c = 'Toxic';
        oli2.Path_ID__c = '';
        oli2.Customer_Site_A_Address_SDPM__c = 'Mexico City, Mexico';
        oli2.Path_Status__c = 'new';
        oli2.Net_NRC_Price__c = 50;
        oli2.Net_MRC_Price__c = 10;
        oli2.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli2);

        Order_Line_Item__c oli3 = new Order_Line_Item__c();
        oli3.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli3.CPQItem__c = '1.1.1';
        /*oli3.Product_Name__c = 'SkyNet';
        oli3.name = 'OLI-078967';*/
        oli3.Line_Item_Status__c = 'New';
        oli3.OrderType__c = 'New Provide';
        oli3.Master_Service_ID__c = '';
        oli3.Primary_Service_ID__c = '';
        oli3.Path_ID__c = 'TERMINATOR 101';
        oli3.Customer_Site_A_Address_SDPM__c = 'Hollywood, California';
        oli3.Path_Status__c = 'new';
        oli3.Net_NRC_Price__c = 25;
        oli3.Net_MRC_Price__c = 5;
        oli3.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli3);
        
        Order_Line_Item__c oli4 = new Order_Line_Item__c();
        oli4.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli4.CPQItem__c = '1.1.2';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli4.Line_Item_Status__c = 'New';
        oli4.OrderType__c = 'New Provide';
        oli4.Master_Service_ID__c = 'System Of A Down';
        oli4.Primary_Service_ID__c = 'Bounce';
        oli4.Path_ID__c = '';
        oli4.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli4.Path_Status__c = 'new';
        oli4.Net_NRC_Price__c = 100;
        oli4.Net_MRC_Price__c = 20;
        oli4.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli4);
        
        Order_Line_Item__c oli5 = new Order_Line_Item__c();
        oli5.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli1.CPQItem__c = '2';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli5.Line_Item_Status__c = 'New';
        oli5.OrderType__c = 'New Provide';
        oli5.Master_Service_ID__c = 'System Of A Down';
        oli5.Primary_Service_ID__c = 'Chop Suey';
        oli5.Path_ID__c = '';
        oli5.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli5.Path_Status__c = 'new';
        oli5.Net_NRC_Price__c = 100;
        oli5.Net_MRC_Price__c = 20;
        oli5.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli5);
        
        Order_Line_Item__c oli6 = new Order_Line_Item__c();
        //Order__c ord = getOrder();
        oli6.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli2.CPQItem__c = '2.1';
        /*oli2.Product_Name__c = 'Intranet Service';
        oli2.name = 'OLI-078966';*/
        oli6.Line_Item_Status__c = 'New';
        oli6.OrderType__c = 'New Provide';
        oli6.Master_Service_ID__c = '';
        oli6.Primary_Service_ID__c = 'Toxic';
        oli6.Path_ID__c = '';
        oli6.Customer_Site_A_Address_SDPM__c = 'Mexico City, Mexico';
        oli6.Path_Status__c = 'new';
        oli6.Net_NRC_Price__c = 50;
        oli6.Net_MRC_Price__c = 10;
        oli6.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli6);

        Order_Line_Item__c oli7 = new Order_Line_Item__c();
        oli7.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli3.CPQItem__c = '2.1.1';
        /*oli3.Product_Name__c = 'SkyNet';
        oli3.name = 'OLI-078967';*/
        oli7.Line_Item_Status__c = 'New';
        oli7.OrderType__c = 'New Provide';
        oli7.Master_Service_ID__c = '';
        oli7.Primary_Service_ID__c = '';
        oli7.Path_ID__c = 'TERMINATOR 101';
        oli7.Customer_Site_A_Address_SDPM__c = 'Hollywood, California';
        oli7.Path_Status__c = 'new';
        oli7.Net_NRC_Price__c = 25;
        oli7.Net_MRC_Price__c = 5;
        oli7.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli7);
        
        Order_Line_Item__c oli8 = new Order_Line_Item__c();
        oli8.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli8.CPQItem__c = '2.1.2';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli8.Line_Item_Status__c = 'New';
        oli8.OrderType__c = 'New Provide';
        oli8.Master_Service_ID__c = 'System Of A Down';
        oli8.Primary_Service_ID__c = 'Bounce';
        oli8.Path_ID__c = '';
        oli8.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli8.Path_Status__c = 'new';
        oli8.Net_NRC_Price__c = 100;
        oli8.Net_MRC_Price__c = 20;
        oli8.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli8);
        
        Order_Line_Item__c oli9 = new Order_Line_Item__c();
        oli9.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli9.CPQItem__c = '3';
        //oli9.Product_Name__c = 'Internet Service';
        //oli9.name = 'OLI-078965';
        oli9.Line_Item_Status__c = 'New';
        oli9.OrderType__c = 'New Provide';
        oli9.Master_Service_ID__c = 'System Of A Down';
        oli9.Primary_Service_ID__c = 'Chop Suey';
        oli9.Path_ID__c = '';
        oli9.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli9.Path_Status__c = 'new';
        oli9.Net_NRC_Price__c = 100;
        oli9.Net_MRC_Price__c = 20;
        oli9.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli9);
        
        Order_Line_Item__c oli10 = new Order_Line_Item__c();
        //Order__c ord = getOrder();
        oli10.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli10.CPQItem__c = '3.1';
        //oli10.Product_Name__c = 'Intranet Service';
        //oli10.name = 'OLI-078966';
        oli10.Line_Item_Status__c = 'New';
        oli10.OrderType__c = 'New Provide';
        oli10.Master_Service_ID__c = '';
        oli10.Primary_Service_ID__c = 'Toxic';
        oli10.Path_ID__c = '';
        oli10.Customer_Site_A_Address_SDPM__c = 'Mexico City, Mexico';
        oli10.Path_Status__c = 'new';
        oli10.Net_NRC_Price__c = 50;
        oli10.Net_MRC_Price__c = 10;
        oli10.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli10);

        Order_Line_Item__c oli11 = new Order_Line_Item__c();
        oli11.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli11.CPQItem__c = '3.1.1';
        /*oli3.Product_Name__c = 'SkyNet';
        oli3.name = 'OLI-078967';*/
        oli11.Line_Item_Status__c = 'New';
        oli11.OrderType__c = 'New Provide';
        oli11.Master_Service_ID__c = '';
        oli11.Primary_Service_ID__c = '';
        oli11.Path_ID__c = 'TERMINATOR 101';
        oli11.Customer_Site_A_Address_SDPM__c = 'Hollywood, California';
        oli11.Path_Status__c = 'new';
        oli11.Net_NRC_Price__c = 25;
        oli11.Net_MRC_Price__c = 5;
        oli11.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli11);
        
        Order_Line_Item__c oli12 = new Order_Line_Item__c();
        oli12.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli12.CPQItem__c = '3.1.2';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli12.Line_Item_Status__c = 'New';
        oli12.OrderType__c = 'New Provide';
        oli12.Master_Service_ID__c = 'System Of A Down';
        oli12.Primary_Service_ID__c = 'Bounce';
        oli12.Path_ID__c = '';
        oli12.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli12.Path_Status__c = 'new';
        oli12.Net_NRC_Price__c = 100;
        oli12.Net_MRC_Price__c = 20;
        oli12.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli12);
        
        Order_Line_Item__c oli13 = new Order_Line_Item__c();
        oli13.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli13.CPQItem__c = '4';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli13.Line_Item_Status__c = 'New';
        oli13.OrderType__c = 'New Provide';
        oli13.Master_Service_ID__c = 'System Of A Down';
        oli13.Primary_Service_ID__c = 'Chop Suey';
        oli13.Path_ID__c = '';
        oli13.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli13.Path_Status__c = 'new';
        oli13.Net_NRC_Price__c = 100;
        oli13.Net_MRC_Price__c = 20;
        oli13.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli13);
        
        Order_Line_Item__c oli14 = new Order_Line_Item__c();
        //Order__c ord = getOrder();
        oli14.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli14.CPQItem__c = '4.1';
        /*oli2.Product_Name__c = 'Intranet Service';
        oli2.name = 'OLI-078966';*/
        oli14.Line_Item_Status__c = 'New';
        oli14.OrderType__c = 'New Provide';
        oli14.Master_Service_ID__c = '';
        oli14.Primary_Service_ID__c = 'Toxic';
        oli14.Path_ID__c = '';
        oli14.Customer_Site_A_Address_SDPM__c = 'Mexico City, Mexico';
        oli14.Path_Status__c = 'new';
        oli14.Net_NRC_Price__c = 50;
        oli14.Net_MRC_Price__c = 10;
        oli14.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli14);

        Order_Line_Item__c oli15 = new Order_Line_Item__c();
        oli15.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli15.CPQItem__c = '4.1.1';
        /*oli3.Product_Name__c = 'SkyNet';
        oli3.name = 'OLI-078967';*/
        oli15.Line_Item_Status__c = 'New';
        oli15.OrderType__c = 'New Provide';
        oli15.Master_Service_ID__c = '';
        oli15.Primary_Service_ID__c = '';
        oli15.Path_ID__c = 'TERMINATOR 101';
        oli15.Customer_Site_A_Address_SDPM__c = 'Hollywood, California';
        oli15.Path_Status__c = 'new';
        oli15.Net_NRC_Price__c = 25;
        oli15.Net_MRC_Price__c = 5;
        oli15.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli15);
        
        Order_Line_Item__c oli16 = new Order_Line_Item__c();
        oli16.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli16.CPQItem__c = '4.1.2';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli16.Line_Item_Status__c = 'New';
        oli16.OrderType__c = 'New Provide';
        oli16.Master_Service_ID__c = 'System Of A Down';
        oli16.Primary_Service_ID__c = 'Bounce';
        oli16.Path_ID__c = '';
        oli16.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli16.Path_Status__c = 'new';
        oli16.Net_NRC_Price__c = 100;
        oli16.Net_MRC_Price__c = 20;
        oli16.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli16);
        
        Order_Line_Item__c oli17 = new Order_Line_Item__c();
        oli17.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli17.CPQItem__c = '5';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli17.Line_Item_Status__c = 'New';
        oli17.OrderType__c = 'New Provide';
        oli17.Master_Service_ID__c = 'System Of A Down';
        oli17.Primary_Service_ID__c = 'Chop Suey';
        oli17.Path_ID__c = '';
        oli17.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli17.Path_Status__c = 'new';
        oli17.Net_NRC_Price__c = 100;
        oli17.Net_MRC_Price__c = 20;
        oli17.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli17);
        
        Order_Line_Item__c oli18 = new Order_Line_Item__c();
        //Order__c ord = getOrder();
        oli18.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli18.CPQItem__c = '5.1';
        /*oli2.Product_Name__c = 'Intranet Service';
        oli2.name = 'OLI-078966';*/
        oli18.Line_Item_Status__c = 'New';
        oli18.OrderType__c = 'New Provide';
        oli18.Master_Service_ID__c = '';
        oli18.Primary_Service_ID__c = 'Toxic';
        oli18.Path_ID__c = '';
        oli18.Customer_Site_A_Address_SDPM__c = 'Mexico City, Mexico';
        oli18.Path_Status__c = 'new';
        oli18.Net_NRC_Price__c = 50;
        oli18.Net_MRC_Price__c = 10;
        oli18.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli18);

        Order_Line_Item__c oli19 = new Order_Line_Item__c();
        oli19.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli19.CPQItem__c = '5.1.1';
        /*oli3.Product_Name__c = 'SkyNet';
        oli3.name = 'OLI-078967';*/
        oli19.Line_Item_Status__c = 'New';
        oli19.OrderType__c = 'New Provide';
        oli19.Master_Service_ID__c = '';
        oli19.Primary_Service_ID__c = '';
        oli19.Path_ID__c = 'TERMINATOR 101';
        oli19.Customer_Site_A_Address_SDPM__c = 'Hollywood, California';
        oli19.Path_Status__c = 'new';
        oli19.Net_NRC_Price__c = 25;
        oli19.Net_MRC_Price__c = 5;
        oli19.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli19);
        
        Order_Line_Item__c oli20 = new Order_Line_Item__c();
        oli20.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli20.CPQItem__c = '5.1.2';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli20.Line_Item_Status__c = 'New';
        oli20.OrderType__c = 'New Provide';
        oli20.Master_Service_ID__c = 'System Of A Down';
        oli20.Primary_Service_ID__c = 'Bounce';
        oli20.Path_ID__c = '';
        oli20.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli20.Path_Status__c = 'new';
        oli20.Net_NRC_Price__c = 100;
        oli20.Net_MRC_Price__c = 20;
        oli20.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli20);
        
        Order_Line_Item__c oli21 = new Order_Line_Item__c();
        oli21.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli21.CPQItem__c = '6';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli21.Line_Item_Status__c = 'New';
        oli21.OrderType__c = 'New Provide';
        oli21.Master_Service_ID__c = 'System Of A Down';
        oli21.Primary_Service_ID__c = 'Chop Suey';
        oli21.Path_ID__c = '';
        oli21.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli21.Path_Status__c = 'new';
        oli21.Net_NRC_Price__c = 100;
        oli21.Net_MRC_Price__c = 20;
        oli21.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli21);
        
        Order_Line_Item__c oli22 = new Order_Line_Item__c();
        //Order__c ord = getOrder();
        oli22.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli22.CPQItem__c = '6.1';
        /*oli2.Product_Name__c = 'Intranet Service';
        oli2.name = 'OLI-078966';*/
        oli22.Line_Item_Status__c = 'New';
        oli22.OrderType__c = 'New Provide';
        oli22.Master_Service_ID__c = '';
        oli22.Primary_Service_ID__c = 'Toxic';
        oli22.Path_ID__c = '';
        oli22.Customer_Site_A_Address_SDPM__c = 'Mexico City, Mexico';
        oli22.Path_Status__c = 'new';
        oli22.Net_NRC_Price__c = 50;
        oli22.Net_MRC_Price__c = 10;
        oli22.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli22);

        Order_Line_Item__c oli23 = new Order_Line_Item__c();
        oli23.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli23.CPQItem__c = '6.1.1';
        /*oli3.Product_Name__c = 'SkyNet';
        oli3.name = 'OLI-078967';*/
        oli23.Line_Item_Status__c = 'New';
        oli23.OrderType__c = 'New Provide';
        oli23.Master_Service_ID__c = '';
        oli23.Primary_Service_ID__c = '';
        oli23.Path_ID__c = 'TERMINATOR 101';
        oli23.Customer_Site_A_Address_SDPM__c = 'Hollywood, California';
        oli23.Path_Status__c = 'new';
        oli23.Net_NRC_Price__c = 25;
        oli23.Net_MRC_Price__c = 5;
        oli23.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli23);
        
        Order_Line_Item__c oli24 = new Order_Line_Item__c();
        oli24.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli24.CPQItem__c = '6.1.2';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli24.Line_Item_Status__c = 'New';
        oli24.OrderType__c = 'New Provide';
        oli24.Master_Service_ID__c = 'System Of A Down';
        oli24.Primary_Service_ID__c = 'Bounce';
        oli24.Path_ID__c = '';
        oli24.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli24.Path_Status__c = 'new';
        oli24.Net_NRC_Price__c = 100;
        oli24.Net_MRC_Price__c = 20;
        oli24.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli24);
        
        Order_Line_Item__c oli25 = new Order_Line_Item__c();
        oli25.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli25.CPQItem__c = '7';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli25.Line_Item_Status__c = 'New';
        oli25.OrderType__c = 'New Provide';
        oli25.Master_Service_ID__c = 'System Of A Down';
        oli25.Primary_Service_ID__c = 'Chop Suey';
        oli25.Path_ID__c = '';
        oli25.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli25.Path_Status__c = 'new';
        oli25.Net_NRC_Price__c = 100;
        oli25.Net_MRC_Price__c = 20;
        oli25.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli25);
        
        Order_Line_Item__c oli26 = new Order_Line_Item__c();
        //Order__c ord = getOrder();
        oli26.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli26.CPQItem__c = '7.1';
        /*oli2.Product_Name__c = 'Intranet Service';
        oli2.name = 'OLI-078966';*/
        oli26.Line_Item_Status__c = 'New';
        oli26.OrderType__c = 'New Provide';
        oli26.Master_Service_ID__c = '';
        oli26.Primary_Service_ID__c = 'Toxic';
        oli26.Path_ID__c = '';
        oli26.Customer_Site_A_Address_SDPM__c = 'Mexico City, Mexico';
        oli26.Path_Status__c = 'new';
        oli26.Net_NRC_Price__c = 50;
        oli26.Net_MRC_Price__c = 10;
        oli26.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli26);

        Order_Line_Item__c oli27 = new Order_Line_Item__c();
        oli27.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli27.CPQItem__c = '7.1.1';
        //oli3.Product_Name__c = 'SkyNet';
        //oli27.name = 'OLI-078967';
        oli27.Line_Item_Status__c = 'New';
        oli27.OrderType__c = 'New Provide';
        oli27.Master_Service_ID__c = '';
        oli27.Primary_Service_ID__c = '';
        oli27.Path_ID__c = 'TERMINATOR 101';
        oli27.Customer_Site_A_Address_SDPM__c = 'Hollywood, California';
        oli27.Path_Status__c = 'new';
        oli27.Net_NRC_Price__c = 25;
        oli27.Net_MRC_Price__c = 5;
        oli27.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli27);
        
        Order_Line_Item__c oli28 = new Order_Line_Item__c();
        oli28.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli28.CPQItem__c = '7.1.2';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli28.Line_Item_Status__c = 'New';
        oli28.OrderType__c = 'New Provide';
        oli28.Master_Service_ID__c = 'System Of A Down';
        oli28.Primary_Service_ID__c = 'Bounce';
        oli28.Path_ID__c = '';
        oli28.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli28.Path_Status__c = 'new';
        oli28.Net_NRC_Price__c = 100;
        oli28.Net_MRC_Price__c = 20;
        oli28.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli28);
        
        Order_Line_Item__c oli29 = new Order_Line_Item__c();
        oli29.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli29.CPQItem__c = '8';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli29.Line_Item_Status__c = 'New';
        oli29.OrderType__c = 'New Provide';
        oli29.Master_Service_ID__c = 'System Of A Down';
        oli29.Primary_Service_ID__c = 'Chop Suey';
        oli29.Path_ID__c = '';
        oli29.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli29.Path_Status__c = 'new';
        oli29.Net_NRC_Price__c = 100;
        oli29.Net_MRC_Price__c = 20;
        oli29.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli29);
        
        Order_Line_Item__c oli30 = new Order_Line_Item__c();
        //Order__c ord = getOrder();
        oli30.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli30.CPQItem__c = '8.1';
        /*oli2.Product_Name__c = 'Intranet Service';
        oli2.name = 'OLI-078966';*/
        oli30.Line_Item_Status__c = 'New';
        oli30.OrderType__c = 'New Provide';
        oli30.Master_Service_ID__c = '';
        oli30.Primary_Service_ID__c = 'Toxic';
        oli30.Path_ID__c = '';
        oli30.Customer_Site_A_Address_SDPM__c = 'Mexico City, Mexico';
        oli30.Path_Status__c = 'new';
        oli30.Net_NRC_Price__c = 50;
        oli30.Net_MRC_Price__c = 10;
        oli30.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli30);

        Order_Line_Item__c oli31 = new Order_Line_Item__c();
        oli31.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli31.CPQItem__c = '8.1.1';
        /*oli3.Product_Name__c = 'SkyNet';
        oli3.name = 'OLI-078967';*/
        oli31.Line_Item_Status__c = 'New';
        oli31.OrderType__c = 'New Provide';
        oli31.Master_Service_ID__c = '';
        oli31.Primary_Service_ID__c = '';
        oli31.Path_ID__c = 'TERMINATOR 101';
        oli31.Customer_Site_A_Address_SDPM__c = 'Hollywood, California';
        oli31.Path_Status__c = 'new';
        oli31.Net_NRC_Price__c = 25;
        oli31.Net_MRC_Price__c = 5;
        oli31.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli31);
        
        Order_Line_Item__c oli32 = new Order_Line_Item__c();
        oli32.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli32.CPQItem__c = '8.1.2';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli32.Line_Item_Status__c = 'New';
        oli32.OrderType__c = 'New Provide';
        oli32.Master_Service_ID__c = 'System Of A Down';
        oli32.Primary_Service_ID__c = 'Bounce';
        oli32.Path_ID__c = '';
        oli32.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli32.Path_Status__c = 'new';
        oli32.Net_NRC_Price__c = 100;
        oli32.Net_MRC_Price__c = 20;
        oli32.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli32);
        
        Order_Line_Item__c oli33 = new Order_Line_Item__c();
        oli33.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli33.CPQItem__c = '9';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli33.Line_Item_Status__c = 'New';
        oli33.OrderType__c = 'New Provide';
        oli33.Master_Service_ID__c = 'System Of A Down';
        oli33.Primary_Service_ID__c = 'Chop Suey';
        oli33.Path_ID__c = '';
        oli33.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli33.Path_Status__c = 'new';
        oli33.Net_NRC_Price__c = 100;
        oli33.Net_MRC_Price__c = 20;
        oli33.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli33);
        
        Order_Line_Item__c oli34 = new Order_Line_Item__c();
        //Order__c ord = getOrder();
        oli34.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli34.CPQItem__c = '9.1';
        /*oli2.Product_Name__c = 'Intranet Service';
        oli2.name = 'OLI-078966';*/
        oli34.Line_Item_Status__c = 'New';
        oli34.OrderType__c = 'New Provide';
        oli34.Master_Service_ID__c = '';
        oli34.Primary_Service_ID__c = 'Toxic';
        oli34.Path_ID__c = '';
        oli34.Customer_Site_A_Address_SDPM__c = 'Mexico City, Mexico';
        oli34.Path_Status__c = 'new';
        oli34.Net_NRC_Price__c = 50;
        oli34.Net_MRC_Price__c = 10;
        oli34.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli34);

        Order_Line_Item__c oli35 = new Order_Line_Item__c();
        oli35.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli35.CPQItem__c = '9.1.1';
        /*oli3.Product_Name__c = 'SkyNet';
        oli3.name = 'OLI-078967';*/
        oli35.Line_Item_Status__c = 'New';
        oli35.OrderType__c = 'New Provide';
        oli35.Master_Service_ID__c = '';
        oli35.Primary_Service_ID__c = '';
        oli35.Path_ID__c = 'TERMINATOR 101';
        oli35.Customer_Site_A_Address_SDPM__c = 'Hollywood, California';
        oli35.Path_Status__c = 'new';
        oli35.Net_NRC_Price__c = 25;
        oli35.Net_MRC_Price__c = 5;
        oli35.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli35);
        
        Order_Line_Item__c oli36 = new Order_Line_Item__c();
        oli36.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli36.CPQItem__c = '9.1.2';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli36.Line_Item_Status__c = 'New';
        oli36.OrderType__c = 'New Provide';
        oli36.Master_Service_ID__c = 'System Of A Down';
        oli36.Primary_Service_ID__c = 'Bounce';
        oli36.Path_ID__c = '';
        oli36.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli36.Path_Status__c = 'new';
        oli36.Net_NRC_Price__c = 100;
        oli36.Net_MRC_Price__c = 20;
        oli36.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli36);
        
        Order_Line_Item__c oli37 = new Order_Line_Item__c();
        oli37.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli37.CPQItem__c = '10';
        //oli37.Product_Name__c = 'Internet Service';
        //oli37.name = 'OLI-078965';
        oli37.Line_Item_Status__c = 'New';
        oli37.OrderType__c = 'New Provide';
        oli37.Master_Service_ID__c = 'System Of A Down';
        oli37.Primary_Service_ID__c = 'Chop Suey';
        oli37.Path_ID__c = '';
        oli37.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli37.Path_Status__c = 'new';
        oli37.Net_NRC_Price__c = 100;
        oli37.Net_MRC_Price__c = 20;
        oli37.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli37);
        
        Order_Line_Item__c oli38 = new Order_Line_Item__c();
        //Order__c ord = getOrder();
        oli38.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli38.CPQItem__c = '10.1';
        /*oli2.Product_Name__c = 'Intranet Service';
        oli2.name = 'OLI-078966';*/
        oli38.Line_Item_Status__c = 'New';
        oli38.OrderType__c = 'New Provide';
        oli38.Master_Service_ID__c = '';
        oli38.Primary_Service_ID__c = 'Toxic';
        oli38.Path_ID__c = '';
        oli38.Customer_Site_A_Address_SDPM__c = 'Mexico City, Mexico';
        oli38.Path_Status__c = 'new';
        oli38.Net_NRC_Price__c = 50;
        oli38.Net_MRC_Price__c = 10;
        oli38.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli38);

        Order_Line_Item__c oli39 = new Order_Line_Item__c();
        oli39.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli39.CPQItem__c = '10.1.1';
        /*oli3.Product_Name__c = 'SkyNet';
        oli3.name = 'OLI-078967';*/
        oli39.Line_Item_Status__c = 'New';
        oli39.OrderType__c = 'New Provide';
        oli39.Master_Service_ID__c = '';
        oli39.Primary_Service_ID__c = '';
        oli39.Path_ID__c = 'TERMINATOR 101';
        oli39.Customer_Site_A_Address_SDPM__c = 'Hollywood, California';
        oli39.Path_Status__c = 'new';
        oli39.Net_NRC_Price__c = 25;
        oli39.Net_MRC_Price__c = 5;
        oli39.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli39);
        
        Order_Line_Item__c oli40 = new Order_Line_Item__c();
        oli40.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.Id);
        oli40.CPQItem__c = '10.1.2';
        /*oli1.Product_Name__c = 'Internet Service';
        oli1.name = 'OLI-078965';*/
        oli40.Line_Item_Status__c = 'New';
        oli40.OrderType__c = 'New Provide';
        oli40.Master_Service_ID__c = 'System Of A Down';
        oli40.Primary_Service_ID__c = 'Bounce';
        oli40.Path_ID__c = '';
        oli40.Customer_Site_A_Address_SDPM__c = 'LAFAYETTE, INDIANA';
        oli40.Path_Status__c = 'new';
        oli40.Net_NRC_Price__c = 100;
        oli40.Net_MRC_Price__c = 20;
        oli40.Is_GCPE_shared_with_multiple_services__c = 'NA';
        ordli.add(oli40);
        
       // insert ordli;
        return ordli;
    }
    private static Opportunity getOpportunity(){
        Opportunity opp = new Opportunity();
        Account a = getAccount();
        opp.Name = 'Test Opportunity CR611';
        opp.AccountId = a.Id;
         //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
        opp.StageName = 'Identify & Define';
        opp.Stage__c='Identify & Define';
        opp.CloseDate = System.today();
        opp.Estimated_MRC__c=800;
        opp.Estimated_NRC__c=1000;
        opp.ContractTerm__c='10';

        insert opp;
        return opp;
    }
    private static Account getAccount(){
        Account acc = new Account();
        Country_Lookup__c cl = getCountry();
        acc.Name = 'Test Account CR611';
        acc.Customer_Type__c = 'MNC';
        acc.Country__c = cl.Id;
        acc.Selling_Entity__c = 'Telstra INC';
        acc.Activated__c= true;
        acc.Account_Id__c ='12123';
        acc.Customer_Legal_Entity_Name__c='Test';
        insert acc;
        return acc;
    }
    private static Country_Lookup__c getCountry(){
        Country_Lookup__c c2 = new Country_Lookup__c();
        //c2.CCMS_Country_Code__c = 'IND';
        //c2.CCMS_Country_Name__c = 'India';
        c2.Country_Code__c = 'xxx';
        insert c2;
        return c2;
    } 
    private static Site__c getSite(){
        Site__c s1 = new Site__c();
        s1.name = 'Acc CR611';
        s1.Country_Finder__c = getCountry().id ;
        s1.City_Finder__c = getCity().id;
        s1.Address_Type__c = 'Site Address';
        s1.Address1__c = 'abc,2nd street, Mughal block';
        s1.Address2__c = 'xyz,Master street,2nd crown block';
        s1.AccountId__c = getAccount().id;
        insert s1;
        return s1;
        
    
    }
    private static City_Lookup__c getCity(){
        if(city == null){
        city = new City_Lookup__c();
        city.Generic_Site_Code__c = 'U12';
        city.name = 'Magu';
        city.City_Code__c='xyz';
        city.OwnerID = userinfo.getuserid();
        insert city;
     }
     return city;
     
    }
    
    
  }