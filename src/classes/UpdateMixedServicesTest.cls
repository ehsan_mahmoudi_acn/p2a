/**
    @author - Accenture
    @date - 20- Jun-2012
    @version - 1.0
    @description - This is the test class for UpdateMixedServices Trigger.
*/
@isTest(SeeAllData = false)
private class UpdateMixedServicesTest {
/*
static Account acc;
static Country_Lookup__c cl;
static Opportunity opp;
static BillProfile__c b;
static Site__c s;
static City_Lookup__c ci;
static OpportunityLineItem oli;
static CostCentre__c c;
static Opportunity_Line_Item1__c oli1;
static Resource__c res;
static Opp_Product_Type__c oppProdTypeObj ;
static testMethod void testUpdateMixedServices1(){
   try{
    oli = getOpportunityLineItem();
    oli.Location__c = 'Mumbai';
    update oli;
   }catch(Exception e){}
    
}
private static Account getAccount(){
    if(acc == null){
        acc = new Account();
        cl = getCountry();
        acc.Name = 'Test Account1';
        acc.Customer_Type__c = 'MNC';
        acc.Country__c = cl.Id;
        acc.Selling_Entity__c = 'Telstra INC';
        acc.Activated__c = True;
        acc.Account_ID__c = '2121';
        acc.Account_Status__c = 'Active';
        acc.Customer_Legal_Entity_Name__c = 'Scott';
        insert acc;
    }
    return acc;
}
private static Country_Lookup__c getCountry(){
    if(cl == null){
        cl = new Country_Lookup__c();
        cl.CCMS_Country_Name__c = 'India';
        cl.Country_Code__c = 'IND';
        insert cl;
    }
    return cl;
} 
private static City_Lookup__c getCity(){
    if(ci == null){
        ci = new City_Lookup__c();
        ci.City_Code__c ='MUM';
        ci.Name = 'MUMBAI';
        insert ci;
    }
    return ci;
}
private static Opportunity getOpportunity(){
        if(opp == null) {
            opp = new Opportunity();
            acc = getAccount();
            opp.Name = 'Test Opportunity';
            opp.AccountId = acc.Id;
             //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
            opp.StageName = 'Identify & Define';
            opp.Stage__c='Identify & Define';
            opp.CloseDate = System.today();
            opp.Estimated_MRC__c=8;
        opp.Estimated_NRC__c=2;
        opp.ContractTerm__c='1';

            //opp.Approx_Deal_Size__c = 9000;
            opp.TigFin_PR_Actions__c = 'Processed';
            opp.is_Ever_Closed__c=true;
            opp.Is_GIAAS_Available__c=23;
            insert opp;
        }
        return opp;
    }
private static OpportunityLineItem getOpportunityLineItem(){ 
    if(oli == null){
        oli = new OpportunityLineItem();
        set<Id> oppidset=new set<Id>();
        Map<Id,opportunity> oppMap=new Map<Id,Opportunity>();
        oppidset.add(oli.Opportunity.Id);
       //OpportunityLineItem oppLineItemList=[select Site__c,CPQItem__c,is_bundle__c,Charge_ID__c,NRC_Charge_ID__c,Parent_Charge_ID__c,ParentCPQItem__c,U2C_Master_Code__c,Hidden_Sequence__c,Bundle_Label_Name__c,Parent_Bundle_Flag__c,Product_id2__c,Site_B__c,OpportunityId,is_Giaas_product__c,Available_in_ROC__c,OrderType__c from OpportunityLineItem ]; 
        opp = getOpportunity();
        b = getBillProfile();
        s=getSite();
        oli.OpportunityId = opp.Id;
        oli.IsMainItem__c = 'Yes';
        oli.CartItemGuid__c = 'ggggg';
        oli.OrderType__c = 'New Provide';
        oli.Quantity = 5;
        oli.UnitPrice = 10;
        oli.PricebookEntryId = getPriceBookEntry('GCPE').Id;
        oli.ContractTerm__c = '12';
        oli.OffnetRequired__c = 'Yes';
        oli.Location__c = 'Bengaluru';
        oli.BillProfileId__c = b.Id;
        oli.ordertype__c =  'Terminate' ;
        oli.CPQItem__c = '1';
        oli.is_bundle__c=true;
        insert oli;
    }
    return oli;
}
private static Opportunity_Line_Item1__c getopportunityLineItem1(){
    if(oli1==null){
     oli1=new Opportunity_Line_Item1__c();
     opp=getOpportunity();  
     oli1.Opportunity__c=opp.Id;
     //oli1.Name=1233;
     oli1.CurrencyIsoCode='USD';
     insert oli1;   
    }
  return oli1;
}
List<Resource__c> resourceList = new List<Resource__c>();
/*Resource__c res = new Resource__c(NRC_Credit_Bill_Text__c='hdghj',NRC_Bill_Text__c='eytfduwie',
                                  Product_Code__c='hgdjhs',
                                Root_Product_Bill_Text__c='hgh',Service_Bill_Text__c='jhgfjf', 
                                RC_Credit_Bill_Text__c='hdsgfjshd');
    insert res;*/
/*private static Resource__c getResource(){
     oli=getopportunityLineitem();
     Resource__c resList = [Select id,ETC_Bill_Text__c,MRC_Bill_Text__c
                                        ,Root_Product_Bill_Text__c,Service_Bill_Text__c,RC_Credit_Bill_Text__c
                                        ,NRC_Credit_Bill_Text__c
                                        ,NRC_Bill_Text__c
                                        ,Product_Code__c,
                                        Installment_NRC_Charge_ID__c,Installment_NRC_duration__c,Installment_NRC_Amount_per_month__c,Parent_Charge_Id__c,Charge_Id__c,NRC_Charge_ID__c,Parent_Bundle_Flag__c,Bundle_Flag__c,Bundle_Label_name__c,Zero_Charge_ETC_Flag__c,Zero_Charge_MRC_Flag__c,Zero_Charge_NRC_Flag__c from Resource__c where id =:oli.Resource__c];
            
   return resList;
   }

//resourceList.add(res);  
 private static Opp_Product_Type__c getproducttype(){
       if(oppProdTypeObj ==null){
        opp=getOpportunity();       
        oppProdTypeObj=new Opp_Product_Type__c();
         oppProdTypeObj.Name='test';
         oppProdTypeObj.CurrencyIsoCode='USD';
         oppProdTypeObj.Opportunity__c =opp.Id;                                                          
        insert oppProdTypeObj;       
        }       
       return oppProdTypeObj;
   }                        

    
private static Pricebook2 getPriceBook(String prodName){
        Pricebook2 p = [SELECT Id FROM Pricebook2 where IsStandard = true LIMIT 1];
        return p;   
    }
    
private static Product2 getProduct(String prodName){
        Product2 prod = new Product2();
        prod.Name = prodName;
        prod.ProductCode = prodName;
        prod.Product_ID__c ='GCPE';
        insert prod;
        return prod;
    }
    
private static PricebookEntry getPriceBookEntry(String prodName){
        PricebookEntry p = new PricebookEntry();
        p.Pricebook2Id = getPriceBook(prodName).Id;
        p.Product2Id =  getProduct(prodName).Id;
        p.UnitPrice = 2000;
        p.IsActive = true;
        p.UseStandardPrice = false;
        insert p; 
        return p;    
    }
    
    
private static BillProfile__c getBillProfile(){
    if(b == null) {
        b = new BillProfile__c();
        s = getSite();
        acc = getAccount();
      //  b.Bill_Profile_Number__c = 'Test Bill Profile';
        b.Billing_Entity__c = 'Telstra Limited';
        b.Account__c = s.AccountId__c;
        b.Bill_Profile_Site__c = s.Id;
        //b.Operating_Unit__c = 'TI_HK_I52S';
        b.Start_Date__c = date.today();
        b.Invoice_Breakdown__c = 'Summary Page';
        b.First_Period_Date__c = date.today();

        insert b;
    }
        return b;
}
private static CostCentre__c getCostCentre(){
    if(c == null) {
        c = new CostCentre__c();
        b = getBillProfile();
       c.Name= 'test cost';
 
       c.BillProfile__c = b.Id;
    
        insert c;
    }
        return c;
}
private static Site__c getSite(){
    if(s == null){
        s = new Site__c();
        acc = getAccount();
    //opp = getOpportunityLineItem();
        s.Name = 'Test_site';
        s.Address1__c = '43';
        s.Address2__c = 'Bangalore';
        cl = getCountry();
        s.Country_Finder__c = cl.Id;
        ci = getCity();
        s.City_Finder__c = ci.Id;
        s.AccountId__c =  acc.Id; 
        s.Address_Type__c = 'Billing Address';
        insert s;
    }
    return s;
}   
     //Map<String,order_line_item__c> orderliMap = new Map<String,order_line_item__c>();
      //  orderliMap.put('1234', ordLineItemObj) ;  
     //mapRes.put(res.Id,res);



                         
     
  */   
}