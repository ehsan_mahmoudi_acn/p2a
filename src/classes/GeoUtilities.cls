/**
    @author - Accenture
    @date - 27-April-2012
    @version - 1.0
    @description - This is the class to retrieve the Lattitude and Logitude using Google APIs
.
*/

/*As part of production Ticket# TGCTASK0209037, commented //with sharing */
//public with sharing class GeoUtilities {
public class GeoUtilities {

/**  
        @author - Accenture
        @date - 27-April-2012
        @param - List <String> siteIds
        @return - void 
        @description - This method is used to update the site object 
*/
  @future(callout=true)
  public static void updateSiteGeo(List <String> siteIds)
  {
    
    // Get the custom city, state and country fields from Site
    List <Site__c> sites = [select Id, Name, City_name__c, State__c, Country_name__c, City_Finder__r.Name, Country_Finder__r.Name
                            from Site__c where Id in :siteIds ];
      
    // Putting the values of site from site Object to the local variable
    for(Site__c site: sites) {
      String address = '';
      address+=site.City_Finder__r.Name!=null?','+site.City_Finder__r.Name:'';
      address+=site.State__c!=null?','+site.State__c:'';
      address+=site.Country_Finder__r.Name!=null?','+site.Country_Finder__r.Name:'';
      String[] coordinates = GeoUtilities.getCoordinates(address.substring(1));
      
      // Getting the Lattitude and Longitude values and putting into the site object
      if(coordinates != null)
      {
        Decimal pos1 = Decimal.valueOf(coordinates[0]);
        site.Latitude__c = pos1.toPlainString();
        Decimal pos2 = Decimal.valueOf(coordinates[1]);
        site.Longitude__c = pos2.toPlainString();
        system.debug(Logginglevel.ERROR,'GeoUtilities coordinates ' + pos1 + ' ' + pos2 );
        // Update the Flag - When Co-Ordinates are done        
      }
      else
      {
        system.debug(Logginglevel.ERROR,'GeoUtilities no coordinates!!! for address' );
      }
    }
    update sites;
  }

/**  
        @param - String addressParts
        @return - coordinates 
        @description - This method is used :
                       Input list of address parts: street,  city,  state,  country.
                       Output: list of coordinates: latitude, longitude 
*/

public static String[] getCoordinates(String addressParts)
{
  String[] Coordinates;
  String address = '';
    boolean needComma = false;
  
      address = address + EncodingUtil.urlEncode(addressParts,'UTF-8');
  
    //  address = address + EncodingUtil.urlEncode(addressParts,'UTF-8');

  if(address.length() == 0)
  {
    system.debug(Logginglevel.ERROR,
      'GeoUtilities getCoordinates no address provided. Return null');
    return null;
  }

  // Giving the URL to connect to the Google APIs
  String url = 'http://maps.google.com/maps/geo?';
  url += 'q=' + address;
  url += '&output=csv';

  system.debug(Logginglevel.ERROR,'GeoUtilities getCoordinates url: ' + url);
  
  // Sending the values through HttpRequest
  Http h = new Http();
  HttpRequest req = new HttpRequest();

  req.setHeader('Content-type', 'application/x-www-form-urlencoded');
  req.setHeader('Content-length', '0');
  req.setEndpoint(url);
  req.setMethod('POST');
  String responseBody;
  
  if (!Test.isRunningTest()){
  // Methods defined as TestMethod do not support Web service callouts
    HttpResponse res = h.send(req);
    responseBody = res.getBody();
  }
  else {
    // dummy data
    responseBody = '200,4,48.5,-123.67';
  }
  String[] responseParts = responseBody.split(',',0);
  // the response has four parts:
  // status code, quality code, latitude and longitude
  Coordinates = new String[2];
  Coordinates[0] = responseParts[2];
  Coordinates[1] = responseParts[3];

  return Coordinates;
}
  
  /**  
		KD: Commented to fix Validation error.
        @return - Void 
        @description - This method is used to test the above method
  
  static testMethod void  testGetGeo()
  {
   String addressParts='';
    String[] coordinates;

    addressParts+='San Diego,';
    addressParts+='CA,';
    addressParts+='USA';

    coordinates = GeoUtilities.getCoordinates(addressParts);
    System.assert(coordinates != null);
    System.assertEquals(2, coordinates.size());
  }*/
}