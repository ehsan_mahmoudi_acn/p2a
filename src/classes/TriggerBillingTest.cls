@isTest(seealldata = false)
private class TriggerBillingTest {
    @isTest
    private static void triggerTest1() {
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        P2A_TestFactoryCls.sampleTestData();
        
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
        List<cscfga__Configuration_Screen__c>  ConfigLst = P2A_TestFactoryCls.getConfigScreen(1, ProductDeflist);
        List<cscfga__Screen_Section__c> ssList1 =  P2A_TestFactoryCls.getScreenSec(1,configLst);
        List<cscfga__Attribute_Definition__c> Attributedeflist = P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist,ConfigLst,ssList1);
        list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
        // List<cscfga__Attribute_Definition__c> Attributedeflist = P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist);
        // list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
        Orders[0].Is_Terminate_Order__c = true;
        Orders[0].Order_Type__c = 'Parallel Upgrade';
        Orders[0].Is_InFlight_Update__c = true;
        //Orders[0].Is_Terminate_Order__c = true;
        update Orders[0];
        
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<User> users = P2A_TestFactoryCls.get_Users(1);
        List<Pricing_Approval_Request_Data__c> Pricingapp = P2A_TestFactoryCls.getProductonfigappreq(1,prodBaskList,null);
        list<case> caselist = P2A_TestFactoryCls.getcases(1,AccList,Opplist,prodBaskList,Orders,Users);
        List<csord__Service__c> services = P2A_TestFactoryCls.getService(1, OrdReqList, SUBList);
        Product2 prod = new Product2();
        prod.Name = 'prodName';
        prod.ProductCode = 'prodName';
        prod.Product_ID__c = 'prodName';
        prod.Create_Service__c = 'serTyp';
        prod.Create_Path__c =TRUE;
        insert prod;
        
        csord__Service__c ser1 = new csord__Service__c();
        //ser1.Product__r.Name = prod.name;
        ser1.Name = 'Test Service';
        //ser1.Ordertype__c = 'New';
        ser1.csordtelcoa__Product_Configuration__c = proconfigs.get(0).id; 
        ser1.csordtelcoa__Product_Basket__c = prodBaskList.get(0).Id;
        ser1.Primary_Service_ID__c = 'SN-00006989';
        ser1.Cease_Service_Flag__c = true;
        ser1.csordtelcoa__Replaced_Service__c = services[0].id;
        ser1.Updated__c = true;
        ser1.Bundle_Action__c = 'New';
        ser1.csordtelcoa__Service_Number__c = 'SN-78945112';
        ser1.Billable_Flag__c = 'Yes';
        ser1.ResourceId__c = 'resource-4567';
        ser1.Bundle_Flag__c = true;
        ser1.Opportunity__c = oppList[0].Id;
        ser1.Bundle_Label_name__c = 'CRG-00132689';
        ser1.Parent_Bundle_Flag__c  = true;
        ser1.csord__Identification__c = 'Service_' + proconfigs.get(0).id;
        ser1.csord__Order_Request__c = OrdReqList.get(0).id;
        //ser1.csord__Service__c = ser.id; 
        ser1.csord__Subscription__c = SUBList[0].id;
        
        List<csord__Service__c> csServiceList = new List<csord__Service__c>();
        csServiceList.add(ser1);
        insert csServiceList;
        
        List<csord__Service_Line_Item__c> serLineItemList = P2A_TestFactoryCls.getSerLineItem(1,csServiceList,OrdReqList);
        for(csord__Service_Line_Item__c csServiceLineItem : serLineItemList){
            csServiceLineItem.Is_Miscellaneous_Credit_Flag__c = false;
            csServiceLineItem.Name = 'Test Service Line Item';
        }
        update serLineItemList;
        List<csord__Service_Line_Item__c> serlinelist = [select id,name from csord__Service_Line_Item__c where name = 'Test Service Line Item'];
      
        //ser1.Parent_Bundle_Flag__c = false;
        //update ser1;
        string serviceId = ser1.id;
        
        csord__Service__c serv = new csord__Service__c();
        //serv.Product__r.Name = 'prodName';
        serv.Name = 'Test Service';
        //serv.Ordertype__c = 'New';
        //serv.Product_Configuration_Type__c = '';
        serv.csordtelcoa__Product_Configuration__c = proconfigs.get(0).id; 
        serv.csordtelcoa__Product_Basket__c = prodBaskList.get(0).Id;
        serv.Primary_Service_ID__c = 'SN-00006989';
        serv.Bundle_Action__c = 'New';
        serv.Product_Id__c = 'Product';
        serv.csordtelcoa__Service_Number__c = 'SN-78945112';
        serv.Billable_Flag__c = 'Yes';
        serv.ResourceId__c = 'resource-4567';
        serv.Bundle_Flag__c = true;
        serv.Purchase_Order_Date__c = DateTime.Now();
        serv.Contract_Sign_Date__c = date.today();
        serv.Bill_Activation_Flag__c = 'Yes';
        serv.Product_Code__c = 'Ipw';
        serv.Contract_Duration__c = 56;
        serv.Generate_Credit_Flag__c =true;
        serv.Root_Bill_Text__c = 'Root-Bill';
        serv.Parent_Customer_PO__c  = 'Customer-Po';
        serv.Service_Bill_Text__c = 'Service-Bill';
        serv.Opportunity__c = oppList[0].Id;
        serv.Bundle_Label_name__c = 'Test Bundle';
        serv.Parent_Bundle_Flag__c  = true;
        serv.csord__Identification__c = 'Service_' + proconfigs.get(0).id;
        serv.csord__Order_Request__c = OrdReqList.get(0).id;
        //serv.csord__Service__c = ser.id; 
        serv.csord__Subscription__c = SUBList[0].id;
        insert serv;
        
        Test.startTest();   
        TriggerBilling.SendBillingdata(serviceId);   
        Test.setMock(WebServiceMock.class, new SoapServiceMockImpl());
        wwwTibcoComBillingactivate.BillingActivatePortTypeEndpoint1 point1 = new wwwTibcoComBillingactivate.BillingActivatePortTypeEndpoint1();
        wwwTibcoComSchemasXsdgenerationBill.Service_element[] tibcomsxdbilserv;
        point1.BillingActivateOperation(tibcomsxdbilserv);  
        wwwTibcoComSchemasXsdgenerationBill.BillingActivateResponse_element billingActivateResponse_element = new wwwTibcoComSchemasXsdgenerationBill.BillingActivateResponse_element();
        wwwTibcoComSchemasXsdgenerationBill.Service_element service_element = new wwwTibcoComSchemasXsdgenerationBill.Service_element();
        wwwTibcoComSchemasXsdgenerationBill.ServiceLineItem_element serviceLineItem_element = new wwwTibcoComSchemasXsdgenerationBill.ServiceLineItem_element();
        wwwTibcoComSchemasXsdgenerationBill.UDF_element uDF_element = new wwwTibcoComSchemasXsdgenerationBill.UDF_element();
        Test.stopTest();   
         list<csord__Service__c> ser = [select id,Generate_Credit_Flag__c,Full_Termination_Order__c from csord__Service__c];
          system.assertEquals(serv.Generate_Credit_Flag__c , ser[0].Generate_Credit_Flag__c );
          system.assertEquals(serv.Full_Termination_Order__c, ser[0].Full_Termination_Order__c);
       // list<csord__Service__c> ser = [select id,Bundle_Flag__c,Parent_Bundle_Flag__c,Generate_Credit_Flag__c,Full_Termination_Order__c from csord__Service__c];
         list<csord__Service_Line_Item__c> serviceLineItemQueried = [select id,Charge_Action__c from csord__Service_Line_Item__c
                                      where csord__Service__c = :serviceId and Is_Miscellaneous_Credit_Flag__c=false and 
                                            (NOT Name like '%Burst%')]; 
        system.assertEquals(serv.Generate_Credit_Flag__c , ser[0].Generate_Credit_Flag__c );
        system.assertEquals(serv.Full_Termination_Order__c, ser[0].Full_Termination_Order__c);  
        system.assertEquals(ser1.Bundle_Flag__c, ser1.Parent_Bundle_Flag__c);
        system.assertEquals(serviceLineItem_element.ChargeAction, serviceLineItemQueried[0].Charge_Action__c);
        system.assertEquals(true, serv.Parent_Bundle_Flag__c);
    }
    
    @isTest
    private static void triggerTest2() {
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        P2A_TestFactoryCls.sampleTestData();
        
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
        List<cscfga__Configuration_Screen__c>  ConfigLst = P2A_TestFactoryCls.getConfigScreen(1, ProductDeflist);
        List<cscfga__Screen_Section__c> ssList1 =  P2A_TestFactoryCls.getScreenSec(1,configLst);
        List<cscfga__Attribute_Definition__c> Attributedeflist = P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist,ConfigLst,ssList1);
        list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
        // List<cscfga__Attribute_Definition__c> Attributedeflist = P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist);
        // list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
        Orders[0].Is_Terminate_Order__c = true;
        Orders[0].Order_Type__c = 'Parallel Upgrade';
        Orders[0].Is_InFlight_Update__c = true;
        //Orders[0].Is_Terminate_Order__c = true;
        update Orders[0];
        
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<User> users = P2A_TestFactoryCls.get_Users(1);
        List<Pricing_Approval_Request_Data__c> Pricingapp = P2A_TestFactoryCls.getProductonfigappreq(1,prodBaskList,null);
        list<case> caselist = P2A_TestFactoryCls.getcases(1,AccList,Opplist,prodBaskList,Orders,Users);
        List<csord__Service__c> services = P2A_TestFactoryCls.getService(1, OrdReqList, SUBList);
        Product2 prod = new Product2();
        prod.Name = 'prodName';
        prod.ProductCode = 'prodName';
        prod.Product_ID__c = 'prodName';
        prod.Create_Service__c = 'serTyp';
        prod.Create_Path__c =TRUE;
        insert prod;
        
        csord__Service__c csService = new csord__Service__c();
        //csService.Product__r.Name = prod.name;
        csService.Name = 'Test Service';
        //csService.Ordertype__c = 'New';
        csService.csordtelcoa__Product_Configuration__c = proconfigs.get(0).id; 
        csService.csordtelcoa__Product_Basket__c = prodBaskList.get(0).Id;
        csService.Primary_Service_ID__c = 'SN-00006989';
        csService.Cease_Service_Flag__c = true;
        csService.csordtelcoa__Replaced_Service__c = services[0].id;
        csService.Updated__c = true;
        csService.Bundle_Action__c = 'New';
        csService.csordtelcoa__Service_Number__c = 'SN-78945112';
        csService.Billable_Flag__c = 'Yes';
        csService.ResourceId__c = 'resource-4567';
        csService.Bundle_Flag__c = true;
        csService.Opportunity__c = oppList[0].Id;
        csService.Bundle_Label_name__c = 'Test Bundle';
        csService.Parent_Bundle_Flag__c  = true;
        csService.csord__Identification__c = 'Service_' + proconfigs.get(0).id;
        csService.csord__Order_Request__c = OrdReqList.get(0).id;
        //csService.csord__Service__c = ser.id; 
        csService.csord__Subscription__c = SUBList[0].id;
        csService.Parent_Bundle_Flag__c = false;
        insert csService;
        
        //update csService;
        string serviceId = csService.id;
        
        Test.startTest();   
        TriggerBilling.SendBillingdata(serviceId);   
        Test.setMock(WebServiceMock.class, new SoapServiceMockImpl());
        wwwTibcoComBillingactivate.BillingActivatePortTypeEndpoint1 point1 = new wwwTibcoComBillingactivate.BillingActivatePortTypeEndpoint1();
        wwwTibcoComSchemasXsdgenerationBill.Service_element[] tibcomsxdbilserv;
        point1.BillingActivateOperation(tibcomsxdbilserv);  
        wwwTibcoComSchemasXsdgenerationBill.BillingActivateResponse_element billingActivateResponse_element = new wwwTibcoComSchemasXsdgenerationBill.BillingActivateResponse_element();
        wwwTibcoComSchemasXsdgenerationBill.Service_element service_element = new wwwTibcoComSchemasXsdgenerationBill.Service_element();
        wwwTibcoComSchemasXsdgenerationBill.ServiceLineItem_element serviceLineItem_element = new wwwTibcoComSchemasXsdgenerationBill.ServiceLineItem_element();
        wwwTibcoComSchemasXsdgenerationBill.UDF_element uDF_element = new wwwTibcoComSchemasXsdgenerationBill.UDF_element();
        Test.stopTest();   
        list<csord__Service__c> servicelists = [select id,Contract_Terms__c from csord__Service__c];
         system.assertEquals(csService.Contract_Terms__c, servicelists[0].Contract_Terms__c);  
         system.assertNotEquals('New Provide', csService.Product_Configuration_Type__c);
         system.assertNotEquals(csService.Bundle_Flag__c, csService.Parent_Bundle_Flag__c);
    }
    
    @isTest
    private static void triggerTest3() {
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        P2A_TestFactoryCls.sampleTestData();
        
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
        List<cscfga__Configuration_Screen__c>  ConfigLst = P2A_TestFactoryCls.getConfigScreen(1, ProductDeflist);
        List<cscfga__Screen_Section__c> ssList1 =  P2A_TestFactoryCls.getScreenSec(1,configLst);
        List<cscfga__Attribute_Definition__c> Attributedeflist = P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist,ConfigLst,ssList1);
        list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
        // List<cscfga__Attribute_Definition__c> Attributedeflist = P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist);
        // list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
        Orders[0].Is_Terminate_Order__c = true;
        Orders[0].Order_Type__c = 'Parallel Upgrade';
        Orders[0].Is_InFlight_Update__c = true;
        //Orders[0].Is_Terminate_Order__c = true;
        update Orders[0];
        system.assert(Orders[0].id!=null);
        
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<User> users = P2A_TestFactoryCls.get_Users(1);
        List<Pricing_Approval_Request_Data__c> Pricingapp = P2A_TestFactoryCls.getProductonfigappreq(1,prodBaskList,null);
        list<case> caselist = P2A_TestFactoryCls.getcases(1,AccList,Opplist,prodBaskList,Orders,Users);
        List<csord__Service__c> services = P2A_TestFactoryCls.getService(1, OrdReqList, SUBList);
        Product2 prod = new Product2();
        prod.Name = 'prodName';
        prod.ProductCode = 'prodName';
        prod.Product_ID__c = 'prodName';
        prod.Create_Service__c = 'serTyp';
        prod.Create_Path__c =TRUE;
        insert prod;
        
        csord__Service__c csService = new csord__Service__c();
        //csService.Product__r.Name = prod.name;
        csService.Name = 'Test Service';
        //csService.Ordertype__c = 'New';
        csService.csordtelcoa__Product_Configuration__c = proconfigs.get(0).id; 
        csService.csordtelcoa__Product_Basket__c = prodBaskList.get(0).Id;
        csService.Primary_Service_ID__c = 'SN-00006989';
        csService.Cease_Service_Flag__c = true;
        csService.csordtelcoa__Replaced_Service__c = services[0].id;
        csService.Updated__c = true;
        csService.Bundle_Action__c = 'New';
        csService.csordtelcoa__Service_Number__c = 'SN-78945112';
        csService.Billable_Flag__c = 'Yes';
        csService.ResourceId__c = 'resource-4567';
        csService.Bundle_Flag__c = false;
        csService.Opportunity__c = oppList[0].Id;
        csService.Bundle_Label_name__c = 'Test Bundle';
        csService.Parent_Bundle_Flag__c  = true;
        csService.csord__Identification__c = 'Service_' + proconfigs.get(0).id;
        csService.csord__Order_Request__c = OrdReqList.get(0).id;
        //csService.csord__Service__c = ser.id; 
        csService.csord__Subscription__c = SUBList[0].id;
        csService.Parent_Bundle_Flag__c = false;
        insert csService;
        
        //update csService;
        string serviceId = csService.id;
        
        Test.startTest();   
        TriggerBilling.SendBillingdata(serviceId);   
        Test.setMock(WebServiceMock.class, new SoapServiceMockImpl());
        wwwTibcoComBillingactivate.BillingActivatePortTypeEndpoint1 point1 = new wwwTibcoComBillingactivate.BillingActivatePortTypeEndpoint1();
        wwwTibcoComSchemasXsdgenerationBill.Service_element[] tibcomsxdbilserv;
        point1.BillingActivateOperation(tibcomsxdbilserv);  
        wwwTibcoComSchemasXsdgenerationBill.BillingActivateResponse_element billingActivateResponse_element = new wwwTibcoComSchemasXsdgenerationBill.BillingActivateResponse_element();
        wwwTibcoComSchemasXsdgenerationBill.Service_element service_element = new wwwTibcoComSchemasXsdgenerationBill.Service_element();
        wwwTibcoComSchemasXsdgenerationBill.ServiceLineItem_element serviceLineItem_element = new wwwTibcoComSchemasXsdgenerationBill.ServiceLineItem_element();
        wwwTibcoComSchemasXsdgenerationBill.UDF_element uDF_element = new wwwTibcoComSchemasXsdgenerationBill.UDF_element();
        Test.stopTest();   
         list<csord__Service_Line_Item__c> serviceLineItemQueried = [select id,Charge_Action__c from csord__Service_Line_Item__c
                                      where csord__Service__c = :serviceId and Is_Miscellaneous_Credit_Flag__c=false and 
                                            (NOT Name like '%Burst%')]; 
        list<csord__Service__c> servicelists = [select id,Parent_Bundle_Flag__c,Bundle_Flag__c,Account_ID__c  from csord__Service__c];
         system.assertEquals(csService.Parent_Bundle_Flag__c , servicelists[0].Parent_Bundle_Flag__c );  
          system.assertEquals(csService.Bundle_Flag__c , servicelists[0].Bundle_Flag__c  ); 
          system.assertEquals(csService.Account_ID__c, servicelists[0].Account_ID__c );
          system.assert(serviceLineItemQueried.size() == 0 ); 
       	 	system.assertNotEquals('Success', csService.ROC_Line_Item_Status__c );
        
         //system.assertEquals('Terminate',service_element.BundleAction);
        

    }
}