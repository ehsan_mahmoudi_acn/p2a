/**
     @author - Accenture
    @date - 01-April-2016
    @version - 1.0
    @description - This Class will assign Bill Profile to the child services.
*/
public Class BillProfileUPdate{

    // This variable is used to set flag to prevent multiple calls
    
    public void uPdateBillProfile(Set<Id> serviceids){  //'serviceIDs' will hold the list of ServiceIDs
        if(TriggerFlags.NobillProfileUPdate){
            return;
        }
        
        list<csord__Service__c> serv= new list<csord__Service__c>();
        list<cscfga__Product_Configuration__c> prodconfig= new list<cscfga__Product_Configuration__c>();
        List<csord__Service__c> parentService = new List<csord__Service__c>();
        List<csord__Service__c> VPNService = new List<csord__Service__c>();
        List<csord__Service__c> Service = new List<csord__Service__c>();
        List<csord__Service_Line_Item__c> sli = new List<csord__Service_Line_Item__c>();
        List<csord__Service_Line_Item__c> sliMRC = new List<csord__Service_Line_Item__c>();
        Map<id,csord__Service_Line_Item__c> sline= new Map<id,csord__Service_Line_Item__c>();
        Map<id,cscfga__Product_Configuration__c> pmacd = new Map<id,cscfga__Product_Configuration__c>();
        
        Set<Id> ser = new Set<Id>();
        Set<Id> serMRC = new Set<Id>();
        
        system.debug('@@@serviceidssli'+serviceids);
        if(!serviceids.isEmpty()){
            
           sli = [select id,Parent_of_Bundle__c,Charge_ID__c,Zero_Charge_MRC_Flag__c ,Is_Miscellaneous_Credit_Flag__c,Zero_Charge_NRC_Flag__c,Display_line_item_on_Invoice__c ,csord__Service__r.Product_Configuration_Type__c,Account_ID__c ,Bill_Profile__c,Charge_Action__c,csord__Is_Recurring__c,U2C_Master_Code__c,Cost_Centre__c,csord__Service__c,csord__Service__r.AccountId__c,csord__Service__r.csordtelcoa__Replaced_Service__c,csord__Service__r.Bill_ProfileId__c,csord__Service__r.Ordertype__c,csord__Service__r.Cost_Centre_Id__c,csord__Service__r.Order_Type__c from csord__Service_Line_Item__c where csord__Service__c in:serviceids];
            VPNService = [select id,U2CInternetdiff__c,InternetPrimaryBackup__c,U2CMasterCode__c,InternetBillingType__c,Product_Code__c,Product_Id__c,csordtelcoa__Product_Configuration__r.Name from csord__Service__c where id in : serviceids];
            List<U2C_MasterCode_Mapping__c> allU2C = U2C_MasterCode_Mapping__c.getall().values();
            
            if(VPNService.size()>0)
            {
                for(csord__Service__c servc :VPNService)
                {    
                if(servc.csordtelcoa__Product_Configuration__r.Name!=null && servc.Product_Code__c!=null){
                    if(servc.Product_Id__c != null && (Label.VPN_Services_ProductCode.contains(servc.Product_Code__c) || Label.Internet_Product_Codes.contains(servc.Product_Code__c) || Label.Colocation_ProductCode.contains(servc.Product_Code__c)))
                    {
                        for(U2C_MasterCode_Mapping__c U2C : allU2C)
                        {                     
                            if(servc.Product_Id__c == U2C.Product_Id__c)
                            {
                                servc.U2CMasterCode__c = U2C.U2CMasterCode__c;
                                break;                  
                            }
                            if(servc.U2CInternetdiff__c == U2C.Product_Id__c && servc.InternetPrimaryBackup__c == U2C.InternetPrimaryOrBackup__c && servc.InternetBillingType__c == U2C.InternetSingleHomeOrMultiHome__c)
                            {
                                servc.U2CMasterCode__c = U2C.U2CMasterCode__c;
                                break;                  
                            }
                        }
                    serv.add(servc);                     
                    }
                }
                }
            }

            if(!serv.isEmpty()){
                update serv;
            }
            
            system.debug('@@@Sli'+sli);
            for(csord__Service_Line_Item__c slischeck : sli)
            {
                if(slischeck.Account_ID__c == null || slischeck.Bill_Profile__c == null || slischeck.Cost_Centre__c == null || slischeck.Bill_Profile__c!=slischeck.csord__Service__r.Bill_ProfileId__c)
                {
                    ser.add(slischeck.csord__Service__c);
                }
                if(slischeck.Zero_Charge_MRC_Flag__c == false|| slischeck.Zero_Charge_NRC_Flag__c== false|| slischeck.Display_line_item_on_Invoice__c == false)
                {
                    ser.add(slischeck.csord__Service__c);
                }
            }
            for(csord__Service_Line_Item__c slischeckMRC : sli)
            {
                if(slischeckMRC.Is_Miscellaneous_Credit_Flag__c == false && slischeckMRC.csord__Is_Recurring__c == true && slischeckMRC.csord__Service__r.csordtelcoa__Replaced_Service__c != Null)
                {
                    serMRC.add(slischeckMRC.csord__Service__r.csordtelcoa__Replaced_Service__c);
                 }
            }
           if(!serMRC.isEmpty())
            {
                sliMRC = [select id,Charge_ID__c,csord__Service__c from csord__Service_Line_Item__c where csord__Service__c in:serMRC and Is_Miscellaneous_Credit_Flag__c = false and csord__Is_Recurring__c = true];
             }
            for(csord__Service_Line_Item__c slischeckMRCrec : sli)
            {
            
                for(csord__Service_Line_Item__c slischeckMRCrepl : sliMRC)
                {
                if(slischeckMRCrec.Is_Miscellaneous_Credit_Flag__c == false && slischeckMRCrec.csord__Is_Recurring__c == true && slischeckMRCrec.csord__Service__r.csordtelcoa__Replaced_Service__c != Null && slischeckMRCrec.csord__Service__r.csordtelcoa__Replaced_Service__c ==slischeckMRCrepl.csord__Service__c)
                {
                    slischeckMRCrec.Charge_ID__c = slischeckMRCrepl.Charge_ID__c;
                    sline.put(slischeckMRCrepl.id,slischeckMRCrepl);  
                }
             }
            
            }
            if(!ser.isEmpty())
            {
             Service = [select id,Parent_Bundle_Flag__c,U2CMasterCode__c,AccountId__c,csordtelcoa__Product_Configuration__c,csord__Order__r.Is_InFlight_Cancel__c,ROC_Line_Item_Status__c,Product_Configuration_Type__c,Bill_ProfileId__c,Cost_Centre_Id__c from csord__Service__c where id in :ser];
             system.debug('@@@@####'+Service);
            }
            parentService = [select id,Cascade_Bill_Profile__c,csord__Service__r.Cascade_Bill_Profile__c,U2CMasterCode__c,csord__Service__r.Bill_ProfileId__c,csord__Service__r.Cost_Centre_Id__c,Bill_ProfileId__c,Cost_Centre_Id__c,csord__Service__c,csord__Service__r.Product_Id__c,csord__Service__r.csord__Service__c,csord__Service__r.csord__Service__r.Product_Id__c,csord__Service__r.csord__Service__r.csord__Service__c,csord__Service__r.csord__Service__r.csord__Service__r.Product_Id__c,csord__Service__r.csord__Service__r.csord__Service__r.csord__Service__c, csord__Service__r.csord__Service__r.csord__Service__r.csord__Service__r.Product_Id__c,Product_Id__c from csord__Service__c where csord__Service__c in :serviceids];
            system.debug('#####'+parentService+'size=='+parentservice.size());
                   
            // Update Line Item Records
            if(!sli.isEmpty() && Service.size()>0)
            {
             for(csord__Service__c services : Service)
             {           
              for(csord__Service_Line_Item__c slis : sli)
              {
                if(services.id == slis.csord__Service__c)
                {               
                 if(services.AccountId__c != null && slis.Account_ID__c == null)
                 {
                     slis.Account_ID__c = services.AccountId__c;
                 }  
                 // commented for defect QC#13722 by Suresh Palvadi on 20'Dec'2016.
                 if(services.Bill_ProfileId__c != null && slis.Bill_Profile__c == null) /* || (services.Bill_ProfileId__c != slis.Bill_Profile__c))*/ 
                 {
                     slis.Bill_Profile__c = services.Bill_ProfileId__c;
                 }  
                 if(services.Cost_Centre_Id__c != null && slis.Cost_Centre__c == null)
                 {
                     slis.Cost_Centre__c = services.Cost_Centre_Id__c;
                 }  
                 if(services.Parent_Bundle_Flag__c == true)
                 {
                     slis.Parent_of_Bundle__c = true;
                 }

                 if((slis.csord__Service__r.Product_Configuration_Type__c == 'Terminate' || services.Product_Configuration_Type__c == 'Parallel Upgrade' || services.Product_Configuration_Type__c == 'Parallel Downgrade') && slis.csord__Is_Recurring__c == true && slis.csord__Service__r.Order_Type__c!='Subscription Creation'){
                    slis.Charge_Action__c = 'TERMINATE';
                }
                else if(slis.csord__Service__r.Product_Configuration_Type__c == 'New Provide'){
                        slis.Charge_Action__c = 'CREATE';
                        } 
                        else if(slis.csord__Service__r.Product_Configuration_Type__c != 'New Provide' && slis.csord__Service__r.Product_Configuration_Type__c != 'Terminate' && slis.csord__Service__r.Product_Configuration_Type__c != 'Parallel Upgrade' && slis.csord__Service__r.Product_Configuration_Type__c != 'Parallel Downgrade' && slis.csord__Is_Recurring__c == true){
                                slis.Charge_Action__c = 'UPDATE';
                                } 
                                else if(slis.csord__Service__r.Product_Configuration_Type__c != 'New Provide' && (slis.csord__Service__r.Product_Configuration_Type__c == 'Terminate' || services.Product_Configuration_Type__c == 'Parallel Upgrade' || services.Product_Configuration_Type__c == 'Parallel Downgrade' || services.Product_Configuration_Type__c == 'Renewal' ||services.Product_Configuration_Type__c == 'Upgrade Hot' || services.Product_Configuration_Type__c == 'Downgrade Hot' || services.Product_Configuration_Type__c == 'Re-Price' || services.Product_Configuration_Type__c == 'Relocation' || services.Product_Configuration_Type__c == 'Reconfiguration') && slis.csord__Is_Recurring__c == false){
                                    slis.Charge_Action__c = 'CREATE';
                                }else if((services.Product_Configuration_Type__c == 'Parallel Upgrade' || services.Product_Configuration_Type__c == 'Parallel Downgrade') && slis.csord__Is_Recurring__c == true && slis.csord__Service__r.Order_Type__c=='Subscription Creation'){
                slis.Charge_Action__c = 'CREATE';   
                                    
                                        
                 }

                  if(services.Parent_Bundle_Flag__c == true && (slis.Zero_Charge_MRC_Flag__c == false || slis.Zero_Charge_NRC_Flag__c== false || slis.Display_line_item_on_Invoice__c == false))
                  {
                    slis.Zero_Charge_MRC_Flag__c = true;
                    slis.Zero_Charge_NRC_Flag__c= true;
                    slis.Display_line_item_on_Invoice__c = true;
                  }
                  
                    slis.U2C_Master_Code__c = services.U2CMasterCode__c;
                   
                sline.put(slis.id,slis);  
               }            
              }  
             }        
            }
            if(!sline.isEmpty())
            {
                system.debug('!!!sline'+sline);
                update sline.values();
            }
                 
            //Updating Bill Profile, Cost center in Child Services
                    
            if(parentService.size() >0){
                system.debug('sapan'+parentService[0]);
                for(csord__Service__c servc :parentService )
                    {
                        system.debug('sapan');
                        if(servc.csord__Service__r.Cascade_Bill_Profile__c==true)
                        {
                            servc.Bill_ProfileId__c = servc.csord__Service__r.Bill_ProfileId__c;
                            servc.Cost_Centre_Id__c= servc.csord__Service__r.Cost_Centre_Id__c;
                            servc.Cascade_Bill_Profile__c=true;
                        }
                        if(servc.csord__Service__r.Cascade_Bill_Profile__c==false)
                        {
                            servc.Cascade_Bill_Profile__c=false;
                        }
                    }                   
                }
           
        } 
  
    }
      //second method call
    /*
    *first part of updateBillProfile to update service
    *developer:ritesh
    *date:6-feb-2017
    *description:updatebillprofile job was failing and hence the change in implementation was required
    */
    public static List<csord__Service__c> uPdateBillProfileService(List<csord__Service__c>newServices){        
        
     try{   
    

    List<U2C_MasterCode_Mapping__c> allU2C = U2C_MasterCode_Mapping__c.getall().values();
    //U2C Master code update for VPN Service Products and Internet Products.
    
                Map<String,U2C_MasterCode_Mapping__c>productIdToU2CMap=new Map<String,U2C_MasterCode_Mapping__c>();
                for(U2C_MasterCode_Mapping__c U2C :allU2C)
                {
                    if(U2C.Product_Id__c!=null){productIdToU2CMap.put(U2C.Product_Id__c,U2C);}
                }
                
                for(csord__Service__c servc :newServices)
                {    
                
                    if(servc.Product_Id__c != null && (Label.VPN_Services_ProductCode.contains(servc.Product_Code__c) || Label.Internet_Product_Codes.contains(servc.Product_Code__c) || Label.Colocation_ProductCode.contains(servc.Product_Code__c)))
                    {
                            System.debug('ritzzz 2'+servc);             
                            
                            
                            if(productIdToU2CMap.containsKey(servc.Product_Id__c))
                            {
                                servc.U2CMasterCode__c = productIdToU2CMap.get(servc.Product_Id__c).U2CMasterCode__c;
                                
                                
                                                  
                            }
                            if( productIdToU2CMap.containsKey(servc.U2CInternetdiff__c) 
                                && servc.InternetPrimaryBackup__c == productIdToU2CMap.get(servc.U2CInternetdiff__c).InternetPrimaryOrBackup__c 
                                && servc.InternetBillingType__c == productIdToU2CMap.get(servc.U2CInternetdiff__c).InternetSingleHomeOrMultiHome__c)
                            {
                                servc.U2CMasterCode__c= productIdToU2CMap.get(servc.U2CInternetdiff__c).U2CMasterCode__c;                                
                                                  
                            }
                                                            
                    }
                            
                }
            
                                    
            System.debug('newServices'+newServices);    
            return newServices;
    }
    catch(Exception ex){
    ErrorHandlerException.ExecutingClassName='billProfileUPdate:uPdateBillProfileService';
    ErrorHandlerException.objectList= newServices;
    ErrorHandlerException.sendException(ex);
    System.debug(ex);
    return newServices;
    }
        
    
    }

    //Update Cascade billprofile, cost center
    public static void updateCascadeBillprofile(List<csord__Service__c>newServices,Map<Id, csord__Service__c>oldServicesMap){
        
        
        Map<Id,csord__service__c>orderToServiceMap=new Map<Id,csord__service__c>();
      for(csord__Service__c serv:newServices){
         if(serv.csord__Order__c!=null && 
            serv.csord__service__c==null &&
            serv.Cascade_Bill_Profile__c!=oldServicesMap.get(serv.id).Cascade_Bill_Profile__c &&
            serv.Cascade_Bill_Profile__c==true &&
            oldServicesMap.get(serv.id).Cascade_Bill_Profile__c==false){         
            orderToServiceMap.put(serv.csord__Order__c,serv);
         } 
      } 
      if(orderToServiceMap.size()>0){
        List<csord__Service__c> serList =[select id,Country__c,Cost_Centre_Id__c,Bill_ProfileId__c,Cascade_Bill_Profile__c,csord__Order__c,csordtelcoa__Product_Configuration__r.Name,csord__Service__c,csord__Service__r.Cascade_Bill_Profile__c,csord__Service__r.Bill_ProfileId__c,csord__Service__r.Cost_Centre_Id__c from csord__Service__c where csord__Order__c IN:orderToServiceMap.keyset()];
        System.debug('Allu#serList'+serList);
        
        if(newServices.size()>0){
          //updating bill profile and cost center
          for(csord__Service__c ser1 : serList){        
              ser1.Bill_ProfileId__c = orderToServiceMap.get(ser1.csord__Order__c).Bill_ProfileId__c;
              ser1.Cost_Centre_Id__c= orderToServiceMap.get(ser1.csord__Order__c).Cost_Centre_Id__c;
              ser1.Cascade_Bill_Profile__c=orderToServiceMap.get(ser1.csord__Order__c).Cascade_Bill_Profile__c;
              ser1.Country__c=orderToServiceMap.get(ser1.csord__Order__c).Country__c;       
          } 
          if(serList.size() > 0){                
            update serList;
          }
        }
      }
    }


//third method call
/*
*first part of updateBillProfile to update service line item
*developer:ritesh
*date:6-feb-2017
*description:updatebillprofile job was failing and hence the change in implementation was required
*/
public static void uPdateBillProfileServiceLineItem(List<csord__Service_Line_Item__c>newServiceLines){
        if(TriggerFlags.NobillProfileUPdate){
            return;
        }
    
          //variable declaration

            list<csord__Service__c> serv                                      =      new list<csord__Service__c>();
            list<cscfga__Product_Configuration__c> prodconfig                 =      new list<cscfga__Product_Configuration__c>();
            List<csord__Service__c> parentService                             =      new List<csord__Service__c>();
            List<csord__Service__c> VPNService                                =      new List<csord__Service__c>();
            List<csord__Service__c> Service                                   =      new List<csord__Service__c>();
            List<csord__Service_Line_Item__c> ServiceLineItemList             =      new List<csord__Service_Line_Item__c>();
            List<csord__Service_Line_Item__c> ServiceLineItemListMRC          =      new List<csord__Service_Line_Item__c>();
            Map<id,csord__Service_Line_Item__c> ServiceLineItemListne         =      new Map<id,csord__Service_Line_Item__c>();
            Map<id,cscfga__Product_Configuration__c> pmacd                    =      new Map<id,cscfga__Product_Configuration__c>();            
            Set<Id> serviceLinkedToSLI                                        =      new Set<Id>();         
            Set<Id> ser                                                       =      new Set<Id>();
            Set<Id> serMRC                                                    =      new Set<Id>();
    
          //variable declaration ends
           system.debug('@@@newServiceLines'+newServiceLines);
           
           //making a set of all services associated with Service Line Item
           for(csord__Service_Line_Item__c sli : newServiceLines){
               if(sli.csord__Service__c!=null && !serviceLinkedToSLI.contains(sli.csord__Service__c)){
                   serviceLinkedToSLI.add(sli.csord__Service__c);
               }
            }
            
           //getting relevant details from service that will be comapared later 
           Map<id,csord__Service__c>serviceDetailsForSLI = new Map<id,csord__Service__c>([select Bill_ProfileId__c,csordtelcoa__Replaced_Service__c,Product_Configuration_Type__c,Order_Type__c from csord__service__c where id in:serviceLinkedToSLI]);    
            
           for(csord__Service_Line_Item__c sli : newServiceLines)
              {
                if(sli.Account_ID__c == null || sli.Bill_Profile__c == null || sli.Cost_Centre__c == null || (serviceDetailsForSLI.containsKey(sli.csord__Service__c) && sli.Bill_Profile__c!=serviceDetailsForSLI.get(sli.csord__Service__c).Bill_ProfileId__c))
                {
                    ser.add(sli.csord__Service__c);
                }
                if(sli.Zero_Charge_MRC_Flag__c == false|| sli.Zero_Charge_NRC_Flag__c== false|| sli.Display_line_item_on_Invoice__c == false)
                {
                    ser.add(sli.csord__Service__c);
                }
              }
        for(csord__Service_Line_Item__c sli : newServiceLines)
        {
            if(sli.Is_Miscellaneous_Credit_Flag__c == false && sli.csord__Is_Recurring__c == true && (serviceDetailsForSLI.containsKey(sli.csord__Service__c) && serviceDetailsForSLI.get(sli.csord__Service__c).csordtelcoa__Replaced_Service__c != Null))
            {
                serMRC.add(serviceDetailsForSLI.get(sli.csord__Service__c).csordtelcoa__Replaced_Service__c);
             }
        }
        
        if(!serMRC.isEmpty())
        {
            ServiceLineItemListMRC = [select id,Charge_ID__c,csord__Service__c from csord__Service_Line_Item__c where csord__Service__c in:serMRC and Is_Miscellaneous_Credit_Flag__c = false and csord__Is_Recurring__c = true];
         }
        for(csord__Service_Line_Item__c slicheckMRCrec : newServiceLines)
        {
         
         for(csord__Service_Line_Item__c slicheckMRCrepl : ServiceLineItemListMRC)
         {
            if(slicheckMRCrec.Is_Miscellaneous_Credit_Flag__c == false 
            && slicheckMRCrec.csord__Is_Recurring__c == true 
            && serviceDetailsForSLI.containsKey(slicheckMRCrec.csord__Service__c)
            && serviceDetailsForSLI.get(slicheckMRCrec.csord__Service__c).csordtelcoa__Replaced_Service__c != Null 
            && serviceDetailsForSLI.get(slicheckMRCrec.csord__Service__c).csordtelcoa__Replaced_Service__c ==slicheckMRCrepl.csord__Service__c)
            {
                slicheckMRCrec.Charge_ID__c = slicheckMRCrepl.Charge_ID__c;
                ServiceLineItemListne.put(slicheckMRCrepl.id,slicheckMRCrepl);  
            }
         }
         
        }
        if(!ser.isEmpty())
        {
         Service = [select id,Parent_Bundle_Flag__c,U2CMasterCode__c,AccountId__c,csordtelcoa__Product_Configuration__c,csord__Order__r.Is_InFlight_Cancel__c,ROC_Line_Item_Status__c,Product_Configuration_Type__c,Bill_ProfileId__c,Cost_Centre_Id__c from csord__Service__c where id in :ser];
         system.debug('@@@@####'+Service);
        }
        parentService = [select id,Cascade_Bill_Profile__c,csord__Service__r.Cascade_Bill_Profile__c,U2CMasterCode__c,csord__Service__r.Bill_ProfileId__c,csord__Service__r.Cost_Centre_Id__c,Bill_ProfileId__c,Cost_Centre_Id__c,csord__Service__c,csord__Service__r.Product_Id__c,csord__Service__r.csord__Service__c,csord__Service__r.csord__Service__r.Product_Id__c,csord__Service__r.csord__Service__r.csord__Service__c,csord__Service__r.csord__Service__r.csord__Service__r.Product_Id__c,csord__Service__r.csord__Service__r.csord__Service__r.csord__Service__c, csord__Service__r.csord__Service__r.csord__Service__r.csord__Service__r.Product_Id__c,Product_Id__c from csord__Service__c where csord__Service__c in :serviceLinkedToSLI];
        system.debug('#####'+parentService+'size=='+parentservice.size());
               
        // Update Line Item Records
        if(!newServiceLines.isEmpty() && Service.size()>0)
        {
         for(csord__Service__c services : Service)
         {           
          for(csord__Service_Line_Item__c sliLists : newServiceLines)
          {
            if(services.id == sliLists.csord__Service__c)
            {               
             if(services.AccountId__c != null && sliLists.Account_ID__c == null)
             {
                 sliLists.Account_ID__c = services.AccountId__c;
             }  
             // commented for defect QC#13722 by Suresh Palvadi on 20'Dec'2016.
             if(services.Bill_ProfileId__c != null && sliLists.Bill_Profile__c == null) /* || (services.Bill_ProfileId__c != ServiceLineItemLists.Bill_Profile__c))*/ 
             {
                 sliLists.Bill_Profile__c = services.Bill_ProfileId__c;
             }  
             if(services.Cost_Centre_Id__c != null && sliLists.Cost_Centre__c == null)
             {
                 sliLists.Cost_Centre__c = services.Cost_Centre_Id__c;
             }  
             if(services.Parent_Bundle_Flag__c == true)
             {
                 sliLists.Parent_of_Bundle__c = services.Parent_Bundle_Flag__c;
             }

             if((serviceDetailsForSLI.containsKey(sliLists.csord__Service__c) 
                 && serviceDetailsForSLI.get(sliLists.csord__Service__c).Product_Configuration_Type__c == 'Terminate' 
                 || services.Product_Configuration_Type__c == 'Parallel Upgrade' 
                 || services.Product_Configuration_Type__c == 'Parallel Downgrade') 
                 && sliLists.csord__Is_Recurring__c == true 
                 && serviceDetailsForSLI.get(sliLists.csord__Service__c).Order_Type__c!='Subscription Creation'){
                 
                 sliLists.Charge_Action__c = 'TERMINATE';
             }
             else if(serviceDetailsForSLI.containsKey(sliLists.csord__Service__c) && serviceDetailsForSLI.get(sliLists.csord__Service__c).Product_Configuration_Type__c== 'New Provide'){
                    sliLists.Charge_Action__c = 'CREATE';
             } 
             else if(serviceDetailsForSLI.containsKey(sliLists.csord__Service__c) 
                  && serviceDetailsForSLI.get(sliLists.csord__Service__c).Product_Configuration_Type__c != 'New Provide' 
                  && serviceDetailsForSLI.get(sliLists.csord__Service__c).Product_Configuration_Type__c != 'Terminate' 
                  && serviceDetailsForSLI.get(sliLists.csord__Service__c).Product_Configuration_Type__c != 'Parallel Upgrade' 
                  && serviceDetailsForSLI.get(sliLists.csord__Service__c).Product_Configuration_Type__c != 'Parallel Downgrade' 
                  && sliLists.csord__Is_Recurring__c == true){
                      
                    sliLists.Charge_Action__c = 'UPDATE';
             } 
             else if(serviceDetailsForSLI.containsKey(sliLists.csord__Service__c)
                 && serviceDetailsForSLI.get(sliLists.csord__Service__c).Product_Configuration_Type__c  != 'New Provide' 
                 && (serviceDetailsForSLI.get(sliLists.csord__Service__c).Product_Configuration_Type__c  == 'Terminate' 
                 || services.Product_Configuration_Type__c == 'Parallel Upgrade' 
                 || services.Product_Configuration_Type__c == 'Parallel Downgrade' 
                 || services.Product_Configuration_Type__c == 'Renewal' 
                 || services.Product_Configuration_Type__c == 'Upgrade Hot' 
                 || services.Product_Configuration_Type__c == 'Downgrade Hot' 
                 || services.Product_Configuration_Type__c == 'Re-Price' 
                 || services.Product_Configuration_Type__c == 'Relocation' 
                 || services.Product_Configuration_Type__c == 'Reconfiguration') 
                 && sliLists.csord__Is_Recurring__c == false){
                 
                    sliLists.Charge_Action__c = 'CREATE';
             }
             else if((services.Product_Configuration_Type__c == 'Parallel Upgrade' 
                  || services.Product_Configuration_Type__c == 'Parallel Downgrade') 
                  && sliLists.csord__Is_Recurring__c == true 
                  && serviceDetailsForSLI.containsKey(sliLists.csord__Service__c)
                  && serviceDetailsForSLI.get(sliLists.csord__Service__c).Order_Type__c=='Subscription Creation'){
                 
                     sliLists.Charge_Action__c = 'CREATE';                                
                                    
             }

              if(services.Parent_Bundle_Flag__c == true 
              && (sliLists.Zero_Charge_MRC_Flag__c == false 
              || sliLists.Zero_Charge_NRC_Flag__c== false 
              || sliLists.Display_line_item_on_Invoice__c == false))
              {
                sliLists.Zero_Charge_MRC_Flag__c = true;
                sliLists.Zero_Charge_NRC_Flag__c= true;
                sliLists.Display_line_item_on_Invoice__c = true;
              }
              
                sliLists.U2C_Master_Code__c = services.U2CMasterCode__c;
                System.debug('u2c####'+services.U2CMasterCode__c);
               
              ServiceLineItemListne.put(sliLists.id,sliLists);  
            }            
           }  
         }        
        } 
        System.debug('ritesh ServiceLineItemListne'+ServiceLineItemListne);         
    }   
}