/**    @author - Accenture    
       @date - 25- Jun-2012    
       @version - 1.0    
       @description - This is the test class for TriggerBreadthofService
*/@isTest
private class TriggerBreadthofServiceTest {

    static testMethod void testUpdateServiceField() {
        
        Service__c ServiceObj = getService();
        insert ServiceObj;
        system.assert(ServiceObj!=null);
        
    }
    
    // Creating the Test Data
    private static Service__c getService()
    {
       
       Service__c ServiceObj = new Service__c();
       Account accountObj1 = getAccount();
       ServiceObj.AccountId__c = accountObj1.Id;
       ServiceObj.Name = 'Test1234';
       ServiceObj.Product__c  = getProduct('GlobalIPVPN').Id;
      // ServiceObj.Product_Code__c = 'GIP';
       ServiceObj.Net_MRC_Price__c = 34;
       ServiceObj.Net_NRC_Price__c = 12;
       ServiceObj.Contract_Duration__c = 24;
       ServiceObj.Bandwidth__c = 300;
       ServiceObj.Type_of_Topology__c = 'Mesh';
        ServiceObj.Is_GCPE_shared_with_multiple_services__c = 'No';
       
       return ServiceObj;
       
       
    }
    
    private static Account getAccount(){        
        Account acc = new Account();        
        Country_Lookup__c cl= getCountry();        
         acc.Name = 'TestAccount1234';       
         acc.Customer_Type__c = 'MNC';        
         acc.Country__c = cl.Id;        
         acc.Selling_Entity__c = 'Telstra INC';    
         acc.Customer_Legal_Entity_Name__c='Test';    
         insert acc; 
         List<account> a = [Select id,name from Account where name = 'TestAccount1234']; 
         system.assertequals(acc.name, a[0].name);
         system.assert(acc!=null);      
         return acc;    
         }
    
    private static Country_Lookup__c getCountry(){     
         Country_Lookup__c cl = new Country_Lookup__c();       
         cl.CCMS_Country_Name__c = 'ARGENTINA';      
         cl.Country_Code__c = 'AR';      
         insert cl; 
         system.assert(cl!=null);     
         return cl;    
         } 
    private static Product2 getProduct(String prodName){
       
            Product2 prod = new Product2();
            prod.Name = prodName;
            prod.ProductCode = prodName;
            prod.Key_Milestone__c = false;
            prod.Supplier_Milestone__c = true;
            prod.Product_ID__c = 'GIP';
            prod.Eligible_for_Breadth_of_Service__c = true;
            insert prod;
            system.assert(prod!=null);
            return prod;
    }
}