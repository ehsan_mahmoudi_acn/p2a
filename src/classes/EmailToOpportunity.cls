/**
To test the Email service.Not using anywhere
*/

global class EmailToOpportunity implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {

           /* Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();

            // Add the email plain text into the local variable
            String mySubject = email.Subject;
            String myPlainText = email.plainTextBody;
            String frmAddress = email.fromAddress;
            try{
                if(mySubject.contains('Opp')){
                    String oppPartrn = 'Opp-\\d{8}';
                    Pattern emailPattern = Pattern.compile(oppPartrn);
                    Matcher myMatcher = emailPattern.matcher(mySubject);
                    String oppId = null;
                    String conId = null;
                    if(myMatcher.find())
                        oppId = mySubject.subString(myMatcher.start(),myMatcher.start()+12);
                    //System.debug('Matcher Find ============= '+ myMatcher.find());
                    //System.debug('Matcher Start ============= '+ myMatcher.start());
                    //System.debug('opp id ============= '+ myPlainText.subString(myMatcher.start(),myMatcher.start()+12));
                if(oppId!=null && oppId!=''){
                    System.debug('opp id = '+oppId);
                    Opportunity oppObj = [select id, OwnerId  from Opportunity where Opportunity_ID__c = :oppId];
                    
                    List<Contact> conLst = [select id from Contact where Email = :frmAddress ];
                    if(conLst != null && conLst.size()==1)
                        conId = conLst.get(0).id;
                    
                    Task tsk = new Task(
                    Description = myPlainText,
                                        Priority = 'Normal',
                                        Status = 'Inbound Email',
                                        Subject = mySubject,
                                        IsReminderSet = true,
                                        ReminderDateTime = System.now()+3,
                                        WhatId  = oppObj.Id,
                                        WhoId  = conId,
                                        Type = 'Email',
                                        OwnerId = oppObj.OwnerId,
                                        ActivityDate = System.today()+5
                                        );
                                        
                    insert tsk;
                    if (email.binaryAttachments!=null && email.binaryAttachments.size() > 0) {
                        for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {

                            System.debug('Binary Attachments - filename: ' + email.binaryAttachments[i].filename);
                            System.debug('Binary Attachments - size: ' +   email.binaryAttachments[i].mimeTypeSubType);
           
                                Attachment a = new Attachment(ParentId = tsk.Id,
                                                              Name = email.binaryAttachments[i].filename,
                                                              Body = email.binaryAttachments[i].body);
                                insert a;
                                }
                    }
            }
            }
            else if(mySubject.contains('ODR')){
                    String odrPartrn = 'ODR-\\d{5}';
                    Pattern emailPattern = Pattern.compile(odrPartrn);
                    Matcher myMatcher = emailPattern.matcher(mySubject);
                    String odrId = null;
                    if(myMatcher.find())
                        odrId = mySubject.subString(myMatcher.start(),myMatcher.start()+12);
                    //System.debug('Matcher Find ============= '+ myMatcher.find());
                    //System.debug('Matcher Start ============= '+ myMatcher.start());
                    //System.debug('odr id ============= '+ myPlainText.subString(myMatcher.start(),myMatcher.start()+12));
                if(odrId!=null && odrId!=''){
                    System.debug('opp id = '+odrId);
                    Order__c odrObj = [select id, OwnerId  from Order__c where Name = :odrId];
                    String conId = null;
                    List<Contact> conLst = [select id from Contact where Email = :frmAddress ];
                    if(conLst != null && conLst.size()==1)
                        conId = conLst.get(0).id;
                    
                    Task tsk = new Task(
                    Description = myPlainText,
                                        Priority = 'Normal',
                                        Status = 'Inbound Email',
                                        Subject = mySubject,
                                        IsReminderSet = true,
                                        ReminderDateTime = System.now()+3,
                                        WhatId  = odrObj.Id,
                                        WhoId  = conId,
                                        Type = 'Email',
                                        OwnerId = odrObj.OwnerId,
                                        ActivityDate = System.today()+5
                                        );
                                        
                    insert tsk;
                    if (email.binaryAttachments!=null && email.binaryAttachments.size() > 0) {
                        for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {

                            System.debug('Binary Attachments - filename: ' + email.binaryAttachments[i].filename);
                            System.debug('Binary Attachments - size: ' +   email.binaryAttachments[i].mimeTypeSubType);
           
                                Attachment a = new Attachment(ParentId = tsk.Id,
                                                              Name = email.binaryAttachments[i].filename,
                                                              Body = email.binaryAttachments[i].body);
                                insert a;
                                }
                    } 
            }
            }
            else if(mySubject.contains('Account')){
                    String[] accPartrn = mySubject.split('\\:');
                    Pattern emailPattern = Pattern.compile(accPartrn[0]);
                    Matcher myMatcher = emailPattern.matcher(mySubject);
                    String accId = null;
                    if(myMatcher.find())
                        accId = mySubject.subString(myMatcher.start(),myMatcher.start()+12);
                    //System.debug('Matcher Find ============= '+ myMatcher.find());
                    //System.debug('Matcher Start ============= '+ myMatcher.start());
                    //System.debug('odr id ============= '+ myPlainText.subString(myMatcher.start(),myMatcher.start()+12));
                if(accId!=null && accId!=''){
                    System.debug('acc id = '+accId);
                    Account accObj = [select id, OwnerId  from Account where Account_ID__c = :accId];
                    String conId = null;
                    List<Contact> conLst = [select id from Contact where Email = :frmAddress ];
                    if(conLst != null && conLst.size()==1)
                        conId = conLst.get(0).id;
                    
                    Task tsk = new Task(
                    Description = myPlainText,
                                        Priority = 'Normal',
                                        Status = 'Inbound Email',
                                        Subject = mySubject,
                                        IsReminderSet = true,
                                        ReminderDateTime = System.now()+3,
                                        WhatId  = accObj.Id,
                                        WhoId  = conId,
                                        Type = 'Email',
                                        OwnerId = accObj.OwnerId,
                                        ActivityDate = System.today()+5
                                        );
                                        
                    insert tsk;
                    if (email.binaryAttachments!=null && email.binaryAttachments.size() > 0) {
                        for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {

                            System.debug('Binary Attachments - filename: ' + email.binaryAttachments[i].filename);
                            System.debug('Binary Attachments - size: ' +   email.binaryAttachments[i].mimeTypeSubType);
           
                                Attachment a = new Attachment(ParentId = tsk.Id,
                                                              Name = email.binaryAttachments[i].filename,
                                                              Body = email.binaryAttachments[i].body);
                                insert a;
                                }
                    } 
            }
            }
            else {
                    Action_Item__c objActionItem= new Action_Item__c(
                                   objActionItem.RecordTypeId= Util.getRecordTypeMap('Action_Item__c').get('Adhoc Request');
                                   objActionItem.Subject__c= mySubject;
                                   objActionItem.Status__c = Global_Constant_Data__c.getvalues('Action Item Status').value__c;
                                   objActionItem.Priority__c = Global_Constant_Data__c.getvalues('Action Item Priority').value__c;
                                   objActionItem.Credit_Check_Comments__c = myPlainText;
                                   objActionItem.Due_Date__c = System.Today() + 3;
                                   
                                    );
            
            }
            
            }catch (Exception e) {
               System.debug('Issue: ' + e);
            }
                        return result;
                        */


            return null;
            
      }

      
}