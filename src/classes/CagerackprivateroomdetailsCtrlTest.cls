@isTest
private class CagerackprivateroomdetailsCtrlTest{
    private static List<cscfga__Product_Configuration__c> pclist;
    private static List<Product_Definition_Id__c> PdIdlist;
    private static List<cscfga__Product_Definition__c> Pdlist;
    
    static testMethod void unitTest() {
        list<RR_Cage_or_Rack__c> listrcrcage = new list<RR_Cage_or_Rack__c>();        
        P2A_TestFactoryCls.sampleTestData();   

        Pdlist = new List<cscfga__Product_Definition__c>{
              new cscfga__Product_Definition__c(name  = 'Master IPVPN Service',cscfga__Description__c = 'Test master IPVPN') 
            };
        insert Pdlist;      
        System.assertEquals('Master IPVPN Service',Pdlist[0].Name);

        PdIdlist = new List<Product_Definition_Id__c>{
                new Product_Definition_Id__c(name = 'Master IPVPN Service',Product_Id__c = Pdlist[0].Id)
        };
        insert PdIdlist;
        System.assertEquals('Master IPVPN Service',PdIdlist[0].Name); 

        List<cscfga__Product_Basket__c> pblist =  P2A_TestFactoryCls.getProductBasket(9);  
         
        pclist= new List<cscfga__Product_Configuration__c>{
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[0].id,cscfga__Product_Basket__c = pblist[0].Id,cscfga__Product_Family__c = 'Test family')
            };
        insert pclist;      
        System.assertEquals('Test family',pclist[0].cscfga__Product_Family__c); 

        
        case c = new case();
        c.Is_Feasibility_Request__c = true;
        c.Product_Configuration__c = pclist[0].id;
        c.status = 'Approved';
        c.Number_of_Child_Case__c = 2;
        c.Number_of_Child_Case_Closed__c = 1;
        c.Reason_for_selecting_one_supplier__c = 'SingleSource';
        c.Is_Resource_Reservation__c = true;
        insert c;
        System.assertEquals(c.Reason_for_selecting_one_supplier__c, 'SingleSource');

        RR_Cage_or_Rack__c rcr = new RR_Cage_or_Rack__c();
        rcr.Product_Configuration__c = pclist[0].id;
        rcr.Cage_or_Room_ID__c = 'Cageroom';
        rcr.Type__c = 'New';
        rcr.Width_per_rack_mm__c  = 'Rackmm';
        rcr.Rack_IDs_allocated__c = 'Allocated';
        rcr.Power_per_rack_KVA__c = 'Powerrack';
        rcr.No_of_ceeforms_commando_sockets_per_rack__c = 'ceeforms';
        insert rcr;
        System.assertEquals(rcr.Cage_or_Room_ID__c, 'Cageroom');

        RR_Cage_or_Rack__c upadtercr = [select id,Product_Configuration__c,Type__c from RR_Cage_or_Rack__c where id=: rcr.id];
        listrcrcage.add(upadtercr);
        update listrcrcage;

        PageReference pageRef = Page.cagerackprivateroomdetails; // Adding VF page Name here
        pageRef.getParameters().put('id', String.valueOf(rcr.id));//for page reference
        Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('id',c.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        cagerackprivateroomdetailsCtrl   ac = new cagerackprivateroomdetailsCtrl(sc);
        ac.updateItems();    
        boolean b = true;
        String id = ApexPages.currentPage().getParameters().get('id');
        System.assert(b,id==null); 
    }   
 }