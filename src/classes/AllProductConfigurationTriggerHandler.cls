public with sharing class AllProductConfigurationTriggerHandler extends BaseTriggerHandler{
    public override Boolean isDisabled(){
        //if(CheckRecursive.getCurrentSetting('supplierCaseBatchExecute')){
            if (TriggerFlags.NoProductConfigurationTriggers || muteAllTriggers()) {
                return true;
            }
        //}
        return triggerDisabled;
    }
    
    public override void beforeInsert(List<SObject> newProductConfigs) {
        RemoveCloneReference.setCloneReferenceForChangeOrder((List<cscfga__Product_Configuration__c>)newProductConfigs);        
        MasterServiceNameIdentifierHandler.masterServiceNameIdentifierlogic(newProductConfigs);
        CS_VLANGroupNameIdentifierHandler.vlanGroupNameIdentifierLogic(newProductConfigs);
        ProductConfigurationSequence.updateParentConfigForSequence(newProductConfigs);
    }
    
    public override void afterInsert(List<SObject> newProductConfigs, Map<Id, SObject> newProductConfigsMap){
        MasterServiceHandler.process((Map<Id, cscfga__Product_Configuration__c>)newProductConfigsMap);             
        UpdateProductBasketIsColo(newProductConfigs,null);
        changeConfigOwner(newProductConfigsMap.keyset()); 
        SequenceOnInternetProducts((List<cscfga__Product_Configuration__c>)newProductConfigs,null);     
    }
    
    public override void beforeUpdate(List<SObject> newProductConfigs, Map<Id, SObject> newProductConfigsMap, Map<Id, SObject> oldProductConfigsMap){
        ProductConfigurationSequence.updateParentConfigForSequence((List<cscfga__Product_Configuration__c>)newProductConfigs);
        HierarchyOnChangeInProductConfigSequence((List<cscfga__Product_Configuration__c>)newProductConfigs,(Map<Id, cscfga__Product_Configuration__c>)oldProductConfigsMap,'Before');
        hierarchySorting((List<cscfga__Product_Configuration__c>)newProductConfigs);
        RemoveCloneReference.removeCloneReferenceForChangeOrder((List<cscfga__Product_Configuration__c>)newProductConfigs);
        setCurrencyIsoCode((List<cscfga__Product_Configuration__c>)newProductConfigs);
        resetIgnoreForOrderDecompositionFlag(newProductConfigs);
        updateScreenFlowField((List<cscfga__Product_Configuration__c>)newProductConfigs);
        ScreenFlowUtilities.updateScreenFlowField((List<cscfga__Product_Configuration__c>)newProductConfigs);
        if(checkRecursive.productConfigurationBeforeUpdate()){         
            updateProductConfigStatusMLE((Map<Id,cscfga__Product_Configuration__c>)newProductConfigsMap);
        }
    }
    
    public override void afterUpdate(List<SObject> newProductConfigs, Map<Id, SObject> newProductConfigsMap, Map<Id, SObject> oldProductConfigsMap){
       
       SequenceOnInternetProducts((List<cscfga__Product_Configuration__c>)newProductConfigs,(Map<Id, cscfga__Product_Configuration__c>)oldProductConfigsMap);         
       HierarchyOnChangeInProductConfigSequence((List<cscfga__Product_Configuration__c>)newProductConfigs,(Map<Id, cscfga__Product_Configuration__c>)oldProductConfigsMap,'After');
       UpdateProductBasketIsColo((List<cscfga__Product_Configuration__c>)newProductConfigs,(Map<Id, cscfga__Product_Configuration__c>)oldProductConfigsMap);
       LinkAndAssociateServiceWithClonedPC.updateLinkedServiceIdOnEnrichment(newProductConfigs,(Map<Id, cscfga__Product_Configuration__c>)oldProductConfigsMap);       
       updateReationshipInMACD((Map<Id, cscfga__Product_Configuration__c>)newProductConfigsMap, (Map<Id, cscfga__Product_Configuration__c>)oldProductConfigsMap);       
        if(checkRecursive.productConfigurationAfterUpdate()){       
            countOffnetProducts((Map<Id, cscfga__Product_Configuration__c>)newProductConfigsMap, (Map<Id, cscfga__Product_Configuration__c>)oldProductConfigsMap);
            OverridePortRating.afterUpdate((List<cscfga__Product_Configuration__c>)newProductConfigs); 
            UpdateGIEAsbr((List<cscfga__Product_Configuration__c>) newProductConfigs);
            updateProductInformationFromPC((Map<Id, cscfga__Product_Configuration__c>)newProductConfigsMap);
            AddEVPLPort.EVPPorts((List<cscfga__Product_Configuration__c>) newProductConfigs);            
        }        
        
    }
    
    public override void afterDelete(Map<Id, SObject> oldProductConfigsMap){
        countOffnetProducts(null, (Map<Id, cscfga__Product_Configuration__c>)oldProductConfigsMap);
        calculateBasketTCV((Map<Id, cscfga__Product_Configuration__c>)oldProductConfigsMap);
        ProductConfigurationSequence.setPcHierarchy(oldProductConfigsMap.values());         
    }
    
    /** Reset Ignore for Order Decomposition flag for the newly cloned PCs **/
    private void resetIgnoreForOrderDecompositionFlag(List<cscfga__Product_Configuration__c> newProductConfigs){
     for(cscfga__Product_Configuration__c pc :newProductConfigs){
      if(pc.Inflight_Cloning_Id__c != pc.Id && pc.csordtelcoa__Ignore_For_Order_Decomposition__c == true){
        pc.csordtelcoa__Ignore_For_Order_Decomposition__c = false;
      }
     }
    }

    //method call will happen when "request to pricing case button is clicked" and also once from the after update triggerhandler
    private  void updateProductInformationFromPC(Map<Id, cscfga__Product_Configuration__c> newProductConfigsMap){
        Boolean runFlag=false;
        for(cscfga__Product_Configuration__c pc:newProductConfigsMap.values()){
            if(pc.OpportunityStage__c!='Closed Won'){
                runflag=true;
                break;
            }
        }
        if(runFlag){
            if(system.isBatch() || System.isFuture()){
                PricingApproval.updateProductInformationFromPCTrigger(newProductConfigsMap.KeySet(),null); //needs to be executed twice, or product Info after cloning will not work
            }
            else{           
                updateProductInformationFromPCTriggerAsync(newProductConfigsMap.KeySet());          
            }
        }
    }
    
    @future
    private static void updateProductInformationFromPCTriggerAsync(Set<Id> pcIDList){
        PricingApproval.updateProductInformationFromPCTrigger(pcIDList,null);
    }    
       
    

    /**
     * Currency ISO Code - Multicurrency
     */
    @testvisible
    private static void setCurrencyIsoCode(List<cscfga__Product_Configuration__c> newProductConfigs){
        for(cscfga__Product_Configuration__c pc :newProductConfigs){
           if(pc.Basket_Currency__c != null && ((pc.P2O_SFDC_id__c==null || pc.P2O_SFDC_id__c== '') || (pc.P2O_SFDC_id__c !='' && (pc.Order_Type__c !='New Provide' || pc.Order_Type__c ==''))))
                        {
                pc.CurrencyIsoCode = pc.Basket_Currency__c;             
            }
        }
    }
    
    @testvisible
    private void updateScreenFlowField(List<cscfga__Product_Configuration__c> productConfigs){
        for(cscfga__Product_Configuration__c item : productConfigs){
            item.cscfga__Screen_Flow__c = null;
        }
    }
    
    /**
     * The Product Configuration object's OWD has been made as Private
     * This is to extend the Manual Sharing of the Product Configuration to the following 
     * Product Configuration Account Owner
     * Product Configuration Opportunity Owner
     * Product Configuration Owner
     * All TI Commercial Users
     * Commercial Role
     * Service Delivery Role
     * Products Role
     * Roles and Subordinates of Billing Team
     * Roles and Subordinates of Global Channel Original
     */
   
     public void runManagedSharing(List<cscfga__Product_Configuration__c> newProductConfigs){

        if(!Test.isRunningTest())
        {
        if(Util.muteAllTriggers()) return;
        }
        
        /** Product_Configuration_Share is the "Share" table that was created when the Organization Wide Default sharing setting was set to "Private" **/
        List<cscfga__Product_Configuration__Share> ProductConfigurationShares = new List<cscfga__Product_Configuration__Share>();
        String Lastmodifiedby = Schema.cscfga__Product_Configuration__Share.RowCause.Last_Modified_By__c;
        String AccountOwner = Schema.cscfga__Product_Configuration__Share.RowCause.Account_Owner__c;
        String Oppowner = Schema.cscfga__Product_Configuration__Share.RowCause.Opportunity_Owner__c;

        /** For each of the Subscription records being inserted, do the following: **/
        for(cscfga__Product_Configuration__c t : newProductConfigs){
            /** Create a new Subscription_Share record to be inserted in to the Subscription_Share table. **/
            cscfga__Product_Configuration__Share Product_Configuration_LastModifiedBy = new cscfga__Product_Configuration__Share();
            cscfga__Product_Configuration__Share Product_Configuration_AccountOwner = new cscfga__Product_Configuration__Share();
            cscfga__Product_Configuration__Share Product_Configuration_OppOwner = new cscfga__Product_Configuration__Share();
            
            /** Populate the Subscription_Share record with the ID of the record to be shared. **/
            Product_Configuration_LastModifiedBy.ParentId = t.Id;
            Product_Configuration_AccountOwner.ParentId = t.Id;
            Product_Configuration_OppOwner.ParentId = t.Id;
            
            /** Share to - Order Last Modified By **/ 
            if(t.LastModifiedById !=null){
                Product_Configuration_LastModifiedBy.UserOrGroupId = t.LastModifiedById;
                
                /** Specify that the Apex sharing reason **/
                Product_Configuration_LastModifiedBy.RowCause = Lastmodifiedby; 
            }
            
            /** Share to - Order Account Owner **/ 
            if(t.Account_Owner_ID__c !=null){
                Product_Configuration_AccountOwner.UserOrGroupId = t.Account_Owner_ID__c;
                
                /** Specify that the Apex sharing reason **/
                Product_Configuration_AccountOwner.RowCause = AccountOwner ; 
            }
            
            /** Share to - AI Opportunity Owner **/ 
            if(t.Opportunity_Owner_ID__c !=null){
                Product_Configuration_OppOwner.UserOrGroupId = t.Opportunity_Owner_ID__c;
                
                /** Specify that the Apex sharing reason **/
                Product_Configuration_OppOwner.RowCause = Oppowner ; 
            }
            
            /** Specify that the AI should have edit/read access for this particular Action_Item record. **/
            Product_Configuration_AccountOwner.AccessLevel = 'Edit';
            Product_Configuration_OppOwner.AccessLevel = 'Edit';
            Product_Configuration_LastModifiedBy.AccessLevel = 'Edit';
            
            /** Add the new Share record to the list of new Share records. **/
            ProductConfigurationShares.add(Product_Configuration_AccountOwner);
            ProductConfigurationShares.add(Product_Configuration_OppOwner);
            ProductConfigurationShares.add(Product_Configuration_LastModifiedBy);
        }
        
        /** Insert all of the newly created Share records and capture save result **/
        Database.SaveResult[] jobShareInsertResult = Database.insert(ProductConfigurationShares,false);
    }
    
    /**
     * Description: class used to count off-net products and set field Has_Offnet_Products__c on Product Basket Level
     * This is done for the purpose of IPVPN, VPLS Transparent and VPLS VLAN products
     * Then Configurator eligibility rule defines that some products like ASBR and Standalone ASBR can not be added to Product Basket 
     * If Product Basket does not contain off-net products (ports)
     * Used after insert and afer update
     * Author: Davor Dubokovic, CloudSense 
     */
    @testvisible
    public void countOffnetProducts(Map<Id,cscfga__Product_Configuration__c> mapNewProductConfigs, Map<Id,cscfga__Product_Configuration__c> mapOldProductConfigs){
        try{
            Set<Id> setProductBasketId = new set<Id>();  
            if (mapNewProductConfigs==null){
                for (cscfga__Product_Configuration__c item : mapOldProductConfigs.values()){
                    if (item.cscfga__Product_Basket__c!=null && item.Basket_Stage__c != 'Closed Won'){
                        setProductBasketId.add(item.cscfga__Product_Basket__c);
                    }
                }  
            }
            else{
                for (cscfga__Product_Configuration__c item : mapNewProductConfigs.values()){
                    if (item.cscfga__Product_Basket__c!=null && item.Basket_Stage__c != 'Closed Won'){
                        setProductBasketId.add(item.cscfga__Product_Basket__c);
                    }
                }
            }
        
            if(setProductBasketId.size()>0) {
                if((!System.isFuture()) && (!System.isBatch()) && (!System.isScheduled())) {
                    UpdateProductBasketHasOffnetFuture(setProductBasketId);
                } else{
                    UpdateProductBasketHasOffnetExecute(setProductBasketId);
                }
            }
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='AllProductConfigurationTriggerHandler:countOffnetProducts';
            ErrorHandlerException.objectList=mapNewProductConfigs.values();
            ErrorHandlerException.sendException(e);
            
            System.debug('Error: "AllProductConfigurationTriggerHandler; countOffnetProducts" mehotd update failure - ' +e);
          }     
    }

    /**
     * Wrapper method to call the UpdateProductBasketHasOffnetExecute method
     */
    @future
    private static void updateProductBasketHasOffnetFuture(set<Id> setProductBasketId){
        UpdateProductBasketHasOffnetExecute(setProductBasketId);
    }
    
    /**
     * Author: Davor Dubokovic, CloudSense
     * Updates as a future Has_Offnet_Products__c field on Product Basket
     *
     * As part of Winter 17(CR26), this method has been updated.
     * This method will also update Quote Staus of the Product Basket to 'Pending Pricing Approval' if any of the price impacted attributes value will be changed during quotation.
     */
    @testvisible
    private static void updateProductBasketHasOffnetExecute(set<Id> setProductBasketId){
        try{
            List<cscfga__Product_Configuration__c> lstAllProductConfiguration = [select Id, Basket_Stage__c, Basket_Quote_Status__c, Is_Offnet__c, CountOfAttributeValueChange__c, cscfga__Configuration_Status__c, cscfga__Product_Basket__c, cscfga__Product_Family__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c IN :setProductBasketId];
            Map<Id, cscfga__Product_Basket__c> mapProductBasket = new map<Id, cscfga__Product_Basket__c>();
            cscfga__Product_Basket__c tmpProductBasket;
        
            for(cscfga__Product_Configuration__c tmpPC :lstAllProductConfiguration){
                if(!mapProductBasket.containsKey(tmpPC.cscfga__Product_Basket__c)){
                    tmpProductBasket = new cscfga__Product_Basket__c();
                    tmpProductBasket.Id = tmpPC.cscfga__Product_Basket__c;
                    tmpProductBasket.Has_Offnet_Products__c = 'No';
                    tmpProductBasket.csordtelcoa__Basket_Stage__c = tmpPC.Basket_Stage__c;
                    tmpProductBasket.csordtelcoa__Order_Generation_Batch_Job_Id__c = null;
                    tmpProductBasket.Quote_Status__c = tmpPC.Basket_Quote_Status__c;
                    if(tmpPC.Is_Offnet__c == 'Yes') {
                        tmpProductBasket.Has_Offnet_Products__c = 'Yes';
                    }
                    mapProductBasket.put(tmpPC.cscfga__Product_Basket__c, tmpProductBasket);
                } else{
                    if(tmpPC.Is_Offnet__c=='Yes'){
                        tmpProductBasket = mapProductBasket.get(tmpPC.cscfga__Product_Basket__c);
                        tmpProductBasket.Has_Offnet_Products__c = 'Yes';
                    }
                }
            }
            
            /** Updating Quote Status of the basket if any of the pricing attribute is updated. **/
            mapProductBasket = setPricingStatus.getpricingStatus(mapProductBasket, null, lstAllProductConfiguration, false);
        
            if(mapProductBasket.size()>0) {
                update mapProductBasket.values();
            }
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='AllProductConfigurationTriggerHandler:UpdateProductBasketHasOffnetExecute';
            List<id> templist= new list<id>(setProductBasketId);
            Errorhandlerexception.objectIDlist=templist;
            ErrorHandlerException.sendException(e);
            System.debug('Error: "AllProductConfigurationTriggerHandler; UpdateProductBasketHasOffnet" mehotd update failure - ' +e);
          }     
    }

    @testvisible    
    public void updateGIEAsbr(List<cscfga__Product_Configuration__c> newProductConfigs){
        List<cscfga__Product_Configuration__c> ASBRPcList = new List<cscfga__Product_Configuration__c>();
        List<cscfga__Attribute__c> attributeList = new List<cscfga__Attribute__c>();
        Map<Id, Id> GIESelectedASBR = new Map<Id, Id>();
           /*
            * The method should not run after opportunity is closed won 
            */
            Boolean runflag=false;
            
        try{
            for(cscfga__Product_Configuration__c pc :newProductConfigs){
                if(pc.GIE_Selected_ASBR__c != null){
                    GIESelectedASBR.put(pc.GIE_Selected_ASBR__c, pc.Id);
                }
                if(pc.OpportunityStage__c!='Closed Won'){
                    runflag=true;                   
                }
            }
            
            if(!GIESelectedASBR.isEmpty() && runflag){
                ASBRPcList = [Select Id, Added_Ports__c From cscfga__Product_Configuration__c Where Id IN :GIESelectedASBR.KeySet()];
                system.debug('++++ASBRPcList +++'+ASBRPcList);
                if(ASBRPcList.Size() > 0){
                    attributeList = [Select Id, Name, cscfga__Value__c, cscfga__Product_Configuration__c From cscfga__Attribute__c Where cscfga__Product_Configuration__c IN : ASBRPcList AND Name = 'LinkedGIEs'];
                    System.debug('++++Attribute+++'+attributeList);
                    
                    for(cscfga__Attribute__c attribute :attributeList){
                        if(attribute.cscfga__Value__c == '' && attribute.cscfga__Product_Configuration__c != null){
                            attribute.cscfga__Value__c = GIESelectedASBR.get(attribute.cscfga__Product_Configuration__c);
                            System.debug('++++VALUE+++'+attribute.cscfga__Value__c);
                        } else if(attribute.cscfga__Value__c != null && attribute.cscfga__Product_Configuration__c != null){
                            attribute.cscfga__Value__c = attribute.cscfga__Value__c + ',' + GIESelectedASBR.get(attribute.cscfga__Product_Configuration__c);
                        }
                    }
                    if(attributeList.size() > 0){
                        update(attributeList);
                    }               
                }
            }
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='AllProductConfigurationTriggerHandler:UpdateGIEAsbr';
            ErrorHandlerException.objectList=newProductConfigs;
            ErrorHandlerException.sendException(e);
            system.debug('Error: "AllProductConfigurationTriggerHandler; UpdateGIEAsbr" mehotd update failure - ' +e);
          }     
    }

    /**
     * Author: Nikola Culej - CloudSense
     * Issue: #13496 - MLE - PC's with invalid related PC's should also be invalid
     */
    @testvisible
    public void updateProductConfigStatusMLE(Map<Id, cscfga__Product_Configuration__c> newProductConfigsMap){
        try{
            System.debug('**** updateProductConfigStatusMLE ****');
            Set<Id> pcIds = newProductConfigsMap.keySet();
            System.debug('**** PCIds ' + pcIds);
            
            
            /*
            * The method should not run after opportunity is closed won 
            */
            Boolean runflag=false;
            for(cscfga__Product_Configuration__c pc:newProductConfigsMap.values()){
                if(pc.OpportunityStage__c!='Closed Won'){
                    runflag=true;
                    break;
                }
            }
            
            if(runflag){
            List<cscfga__Product_Configuration__c> pcList = [SELECT Id, Name, cscfga__Configuration_Status__c, cscfga__Root_Configuration__c
            FROM cscfga__Product_Configuration__c
            WHERE cscfga__Root_Configuration__c IN : pcIds];
            System.debug('**** Found ' + pcList.size() + ' child PCs');
        
            if(pcList.size() > 0){
                for(cscfga__Product_Configuration__c pc : pcList){
                    cscfga__Product_Configuration__c newPC = newProductConfigsMap.get(pc.Id);
                    
                    if(newPC != null){
                        if(newPC.cscfga__Configuration_Status__c == 'Valid' || newPC.cscfga__Configuration_Status__c == 'Is Valid'){
                            System.debug('****newPC -> Child PC is valid');
                        }else{
                            cscfga__Product_Configuration__c tempPc = newProductConfigsMap.get(newPC.cscfga__Root_Configuration__c);
                            tempPc.cscfga__Configuration_Status__c = 'Incomplete';    
                            System.debug('**** Updating ' + tempPc.Name + ' status to: ' + tempPc.cscfga__Configuration_Status__c);
                        }
                    } else{
                        if(pc.cscfga__Configuration_Status__c == 'Valid' || pc.cscfga__Configuration_Status__c == 'Is Valid'){
                            System.debug('**** Child PC is valid');
                        }else{
                            cscfga__Product_Configuration__c tempPc = newProductConfigsMap.get(pc.cscfga__Root_Configuration__c);
                            tempPc.cscfga__Configuration_Status__c = 'Incomplete';
                            System.debug('**** Updating ' + tempPc.Name + ' status to: ' + tempPc.cscfga__Configuration_Status__c);
                        }
                    }
                }  
            } 
          }         
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='AllProductConfigurationTriggerHandler:updateProductConfigStatusMLE';
            ErrorHandlerException.objectList=newProductConfigsMap.values();
            ErrorHandlerException.sendException(e);
            System.debug('Error: "AllProductConfigurationTriggerHandler; updateProductConfigStatusMLE" mehotd update failure - ' +e);
          }     
    }

    /**
     * Author: Nikola Culej - CloudSense
     * Issue #12308: TCV in basket not getting updated after PC is removed from basket
     */
    @testvisible
    public void calculateBasketTCV(Map<Id, cscfga__Product_Configuration__c> oldProductConfigsMap){
        try{
            Set<Id> basketIds = new Set<Id>();
            List<cscfga__Product_configuration__c> pcList = oldProductConfigsMap.values();

            for(cscfga__Product_configuration__c pc : pcList){
                if(pc.cscfga__Product_Basket__c != null){
                    basketIds.add(pc.cscfga__Product_Basket__c);
                }
            }
        
            if(!basketIds.isEmpty()){
                cscfga.ProductConfigurationBulkActions.calculateTotals(basketIds);    
            }
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='AllProductConfigurationTriggerHandler:calculateBasketTCV';
            ErrorHandlerException.objectList=oldProductConfigsMap.values();
            ErrorHandlerException.sendException(e);
            System.debug('Error: "AllProductConfigurationTriggerHandler; calculateBasketTCV" mehotd update failure - ' +e);
          }     
    }
   
  /**
     * Developer: Anuradha
     * Change Product config owner.
     * TGP0041763 - Sharing rule implementation
     */
  Public static void changeConfigOwner(Set<Id> setPcIds){   
        try{
        List<cscfga__Product_Configuration__c> updateOwner = new List<cscfga__Product_Configuration__c>();
       
        List<cscfga__Product_Configuration__c> Pconfigs = [Select Id,cscfga__Product_Basket__r.OwnerId
        From cscfga__Product_Configuration__c where id IN :setPcIds];
        System.debug('Pconfigs :' +Pconfigs);
       
        if(Pconfigs.size()>0){
        for(cscfga__Product_Configuration__c p :Pconfigs ){
            System.debug('Before cscfga__Product_Configuration__c :' +p.cscfga__Product_Basket__r.OwnerId);     
            if(p.cscfga__Product_Basket__r.OwnerId != null){
            System.debug('After cscfga__Product_Configuration__c :' +p.cscfga__Product_Basket__r.OwnerId);
            p.ownerid = p.cscfga__Product_Basket__r.OwnerId;
            System.debug('ownerid :' +p.ownerid);
            updateOwner.add(p);     
            }
        }
        update updateOwner;
       }             
   }catch(exception e){
            ErrorHandlerException.ExecutingClassName='AllProductConfigurationTriggerHandler:changeConfigOwner';
            List<id> templist1 = new list<id>(setPcIds);
            Errorhandlerexception.objectIDlist=templist1;
            ErrorHandlerException.sendException(e);
            System.debug('exception :' +e);
   }
}


    
    
    
    public static void hierarchyOnChangeInProductConfigSequence(List<cscfga__Product_Configuration__c>newProductConfigs,Map<Id, cscfga__Product_Configuration__c>oldProductConfigsMap,String TriggerType){
      List<cscfga__Product_Configuration__c>pcList=new List<cscfga__Product_Configuration__c>();
      for(cscfga__Product_Configuration__c pc:newProductConfigs){          
          
           if(pc.Sequenc_Key__c!=oldProductConfigsMap.get(pc.id).Sequenc_Key__c){
               pcList.add(pc);
           }          
           
           
          System.debug('pc.Parent_Configuration_For_Sequencing__c'+pc.Parent_Configuration_For_Sequencing__c);
          System.debug('oldProductConfigsMap.get(pc.id).Parent_Configuration_For_Sequencing__c'+oldProductConfigsMap.get(pc.id).Parent_Configuration_For_Sequencing__c);
          System.debug('pc.Sequenc_Key__c'+pc.Sequenc_Key__c);
          System.debug('oldProductConfigsMap.get(pc.id).Sequenc_Key__c'+oldProductConfigsMap.get(pc.id).Sequenc_Key__c); 
          System.debug('after update new'+pc);
          System.debug('after update old'+oldProductConfigsMap.get(pc.id));
          //if(pc.product_id__c.contains('IPTCUSTSITE')) System.assertEquals(true,false);     
      }
      
      
      //System.assertEquals(true,false);
      if(pcList.size()>0 && TriggerType=='Before'){            
        ProductConfigurationSequence.setPcHierarchyBeforeUpdate(newProductConfigs); 
      }
      
      else if(pcList.size()>0 && TriggerType=='After'){            
        ProductConfigurationSequence.setPcHierarchy(pcList); 
      }

}
public static void sequenceOnInternetProducts(List<cscfga__Product_Configuration__c>newProductConfigs,Map<Id, cscfga__Product_Configuration__c>oldProductConfigsMap){
      List<cscfga__Product_Configuration__c>IPCPcList=new List<cscfga__Product_Configuration__c>();      
      List<cscfga__Product_Configuration__c>IPTOrSingleHomePcList=new List<cscfga__Product_Configuration__c>();
      for(cscfga__Product_Configuration__c pc:newProductConfigs){
           
           if(oldProductConfigsMap!=null && pc.Product_Definition_Name__c!=null && pc.Product_Definition_Name__c.contains('Singlehome') && pc.Sequenc_Key__c!= oldProductConfigsMap.get(pc.id).Sequenc_Key__c && pc.Parent_Configuration_For_Sequencing__c==null && pc.cscfga__Parent_Configuration__c!=null){
               IPTOrSingleHomePcList.add(pc);
           }
           
           else if(oldProductConfigsMap==null && pc.Product_Definition_Name__c!=null && pc.Product_Definition_Name__c.contains('Singlehome') && pc.Parent_Configuration_For_Sequencing__c==null && pc.cscfga__Parent_Configuration__c!=null){
               IPTOrSingleHomePcList.add(pc);
           }
          
          
          if(oldProductConfigsMap!=null){         
          System.debug('pc.Parent_Configuration_For_Sequencing__c'+pc.Parent_Configuration_For_Sequencing__c);
          System.debug('oldProductConfigsMap.get(pc.id).Parent_Configuration_For_Sequencing__c'+oldProductConfigsMap.get(pc.id).Parent_Configuration_For_Sequencing__c);
          System.debug('pc.Sequenc_Key__c'+pc.Sequenc_Key__c);
          System.debug('oldProductConfigsMap.get(pc.id).Sequenc_Key__c'+oldProductConfigsMap.get(pc.id).Sequenc_Key__c); 
          System.debug('pc.Added_Ports__c'+pc.Added_Ports__c);
          System.debug('oldProductConfigsMap.get(pc.id).Added_Ports__c'+oldProductConfigsMap.get(pc.id).Added_Ports__c);           
          System.debug('after update new'+pc.product_id__c);
          System.debug('after update new'+pc);
          System.debug('after update old'+oldProductConfigsMap.get(pc.id)); 
          }
          else{
          System.debug('pc.Parent_Configuration_For_Sequencing__c'+pc.Parent_Configuration_For_Sequencing__c);
          //System.debug('oldProductConfigsMap.get(pc.id).Parent_Configuration_For_Sequencing__c'+oldProductConfigsMap.get(pc.id).Parent_Configuration_For_Sequencing__c);
          System.debug('pc.Sequenc_Key__c'+pc.Sequenc_Key__c);
          //System.debug('oldProductConfigsMap.get(pc.id).Sequenc_Key__c'+oldProductConfigsMap.get(pc.id).Sequenc_Key__c); 
          System.debug('pc.Added_Ports__c'+pc.Added_Ports__c);
          //System.debug('oldProductConfigsMap.get(pc.id).Added_Ports__c'+oldProductConfigsMap.get(pc.id).Added_Ports__c);           
          System.debug('after update new'+pc.product_id__c);
          System.debug('after update new'+pc);
          //System.debug('after update old'+oldProductConfigsMap.get(pc.id)); 
          } 
      }       
           
     if(IPTOrSingleHomePcList.size()>0){                
        ProductConfigurationSequence.updateIPTSinglehome(IPTOrSingleHomePcList);                
     }   
  
    }
    
public static void hierarchySorting(List<cscfga__Product_Configuration__c>newProductConfigs){
    for(cscfga__Product_Configuration__c pc:newProductConfigs){
      if(pc.Hierarchy__c!=null){
                 String inputStr=pc.Hierarchy__c.trim();
                 List<String>tempString=inputStr.split('\\.');
                 String newStr='';
                 if(tempString!=null){
                 for(String str:tempString){                     
                     if(str!=null && str.length()==1)newStr=newStr+'000'+str+'.';
                     else If(str!=null && str.length()==2)newStr=newStr+'00'+str+'.';
                     else If(str!=null && str.length()==3)newStr=newStr+'0'+str+'.';
                     else newStr=str!=null?newStr+str+'.':'';
                 }
                 }
                 /*else{
                                       
                     if(inputStr!=null && inputStr.length()==1)newStr=newStr+'000'+inputStr+'.';
                     else If(inputStr!=null && inputStr.length()==2)newStr=newStr+'00'+inputStr+'.';
                     else If(inputStr!=null && inputStr.length()==3)newStr=newStr+'0'+inputStr+'.';
                     else newStr=inputStr!=null?newStr+inputStr+'.':'';
                 
                 }*/
                 pc.Hierarchy_Sequence_Key__c=newStr.removeEnd('.');   
   
    }
    }
}
    //desc : CR 50; eligibilty rule; makeing is_colo_product as yes if colo is added in basket
    //name : sowmya
    //date : 2/7/2017
    
    @testVisible
    private static void updateProductBasketIsColo(List<cscfga__Product_Configuration__c> newProductConfigs,Map<Id, cscfga__Product_Configuration__c> oldProductConfigsMap){
                                
        Map<Id,cscfga__Product_Basket__c> basket = new Map<Id,cscfga__Product_Basket__c>();
        //List<cscfga__Product_Configuration__c> colo = [Select cscfga__Product_Basket__c from cscfga__Product_Configuration__c where Id in : newProductConfigs and name ='Colocation'];
        System.debug('In the function');
        Boolean runflag=false;

        //runflag will be true when a colocation pc is inserted or if basket id is populated/updated on colocation product(incase of change orders)     
        for(cscfga__Product_Configuration__c pc:newProductConfigs){
            if(pc.cscfga__Product_Basket__c!=null && pc.product_Id__c=='COLO-CNTDCOLO' && oldProductConfigsMap==null){
                runflag=true;
                break;
            }
            else if(pc.product_Id__c=='COLO-CNTDCOLO' && pc.cscfga__Product_Basket__c!=null && pc.cscfga__Product_Basket__c!=oldProductConfigsMap.get(pc.id).cscfga__Product_Basket__c){
                runflag=true;
                break;
            }
        }       
        if(runFlag){
            for(cscfga__Product_Configuration__c pc :newProductConfigs){
              if(pc.cscfga__Product_Basket__c != null && pc.product_Id__c=='COLO-CNTDCOLO'){
                    cscfga__Product_Basket__c bskt = new cscfga__Product_Basket__c();
                    bskt.Id = pc.cscfga__Product_Basket__c;
                    bskt.Is_Colo_Product__c = 'Yes';
                    basket.put(bskt.id,bskt);                                                                            
                }
            }
        }
                                
        if(basket.Size() > 0){
            System.debug('In the future function'+basket);
            update basket.values();
        }
        
    }
    
    private static void updateReationshipInMACD(Map<Id, cscfga__Product_Configuration__c> newPCmap,Map<Id, cscfga__Product_Configuration__c> oldPCmap){
        Set<Id> pcIds = new Set<Id>();
        for(cscfga__Product_Configuration__c pc :newPCmap.values()){
            system.debug('===== oldPCmap: ' + oldPCmap.get(pc.Id).csordtelcoa__Replaced_Product_Configuration__c + ' newPC: ' + pc.csordtelcoa__Replaced_Product_Configuration__c);
            if((oldPCmap.get(pc.Id).csordtelcoa__Replaced_Product_Configuration__c != pc.csordtelcoa__Replaced_Product_Configuration__c) && (pc.csordtelcoa__Replaced_Product_Configuration__c != null)){
                pcIds.add(pc.Id);
            }
        }   
        system.debug('pcIds size: ' + pcIds.size());
        if(!pcIds.isEmpty()){
            MACDUtilities.LinkProductConfigurationsInMACD(pcIds);
        }
    }
    
    public static Boolean muteAllTriggers(){        
        try {
            if(Global_Mute__c.getInstance(UserInfo.getUserId()) != null){
                return Global_Mute__c.getInstance(UserInfo.getUserId()).Mute_Triggers__c;
            } else {
                return Global_Mute__c.getOrgDefaults().Mute_Triggers__c;
            }
            
        } catch (Exception e){
            return false;
          }
    }   
}