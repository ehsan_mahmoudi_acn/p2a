global with sharing class CS_CustomAEndCountryLookup extends cscfga.ALookupSearch {

    public override String getRequiredAttributes(){ 
    	return '["Product Type"]';
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
        
        Set <Id> countryIds = new Set<Id>();
        String productType = searchFields.get('Product Type');

        System.Debug('Search field = ' + productType);
       	/*List<CS_Route_Segment__c> routeSegList = [SELECT Id, POP_A__r.CS_Country__c
       											  FROM CS_Route_Segment__c
       											  WHERE Product_Type__c = :productType];*/

       	for(CS_Route_Segment__c item : [SELECT Id, POP_A__r.CS_Country__c
       											  FROM CS_Route_Segment__c
       											  WHERE Product_Type__c = :productType]){
       		countryIds.add(item.POP_A__r.CS_Country__c);
       	}
        System.Debug('doLookupSearch');
        System.Debug(searchFields);
        String searchValue = searchFields.get('searchValue') +'%';
        List <CS_Country__c> data = [SELECT Id, Name FROM CS_Country__c WHERE  Id IN :countryIds AND Name LIKE :searchValue ORDER BY Name];
        System.Debug(data);
       return data;
    }
}