/**
* @author Telstra Accenture
* @description This class has the complete logic related to the MACD functionality for the chnage order services and Orders
*/

public class MacdHandler {
    
    public List<csord__Service__c> afterInsertChangeOrder(List<csord__Subscription__c> newSubscriptions) {
        List<csord__Service__c> servListreplaced = findAllNonReplacedServiceFromOriginalSubscriptions(newSubscriptions);
        List<csord__Service__c> clones = cloneServices(servListreplaced, newSubscriptions);
        
        return clones;
    }
 

/**
* @author Telstra Accenture
* @description Sets the Update__c field value to true on each service having a modified configuration
Marks the services as changed if any visible attribute value has changed.
* @param newSubscriptions subscriptions having the services to update 
* @return List of services
*/ 
    
    @testvisible 
    public List<csord__Service__c> markServicesWithAnUpdatedConfiguration(List<csord__Service__c> serviceList) {
        List<csord__Service__c> servListreplaceds = [
            SELECT Id, csordtelcoa__Product_Configuration__c,
            csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__c,
            csordtelcoa__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c
            FROM csord__Service__c
            WHERE Id IN :serviceList 
            AND csordtelcoa__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c != null];
        
        Set<id> replacedproddef = new Set<id>();
        Set<id> prodconfig = new Set<id>();
        Map<Id, Id> productConfigPair = new Map<Id, Id>();
        
        for (csord__Service__c replserv : servListreplaceds) {
            prodconfig.add(replserv.csordtelcoa__Product_Configuration__c);
            replacedproddef.add(replserv.csordtelcoa__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c);
            productConfigPair.put(replserv.csordtelcoa__Product_Configuration__c, replserv.csordtelcoa__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c);
        }
        
        if ((!replacedproddef.isEmpty()) && (!prodconfig.isEmpty())) {
            List<cscfga__Attribute__c> newattr = [
                SELECT Id, cscfga__Product_Configuration__c,Name, cscfga__Value__c
                FROM cscfga__Attribute__c
                WHERE (cscfga__Attribute_Definition__r.Screens_Displaying_This_Attribute__c > 0
                OR (cscfga__Attribute_Definition__r.cscfga__Configuration_Screen__c != NULL
                AND cscfga__Attribute_Definition__r.cscfga__Configuration_Screen__c != ''))
                AND cscfga__Attribute_Definition__r.cscfga__Type__c != 'Related Product' 
                AND cscfga__Product_Configuration__c IN :prodconfig
                AND Name NOT IN ('Customer_Required_Date__c', 'ScreenFlow',
            'ScreenFlowName', 'Product_config_ID__c', 'BasketId')];
            
            Map<Id, List<cscfga__Attribute__c>> newAttributesByProductConfig = new Map<Id, List<cscfga__Attribute__c>>();
            for(cscfga__Attribute__c att : newattr){
                if(!newAttributesByProductConfig.containsKey(att.cscfga__Product_Configuration__c)){
                    newAttributesByProductConfig.put(att.cscfga__Product_Configuration__c, new List<cscfga__Attribute__c>());
                }
                
                newAttributesByProductConfig.get(att.cscfga__Product_Configuration__c).add(att);
            }
            
            List<cscfga__Attribute__c> replacedattr = [
                SELECT Id, cscfga__Product_Configuration__c, Name, cscfga__Value__c
                FROM cscfga__Attribute__c
                WHERE (cscfga__Attribute_Definition__r.Screens_Displaying_This_Attribute__c > 0
                OR (cscfga__Attribute_Definition__r.cscfga__Configuration_Screen__c != NULL
                AND cscfga__Attribute_Definition__r.cscfga__Configuration_Screen__c != ''))
                AND cscfga__Attribute_Definition__r.cscfga__Type__c != 'Related Product'
                AND cscfga__Product_Configuration__c in :replacedproddef
                AND Name NOT IN ('Customer_Required_Date__c', 'ScreenFlow',
            'ScreenFlowName', 'Product_config_ID__c', 'BasketId')];
            
            Map<Id, List<cscfga__Attribute__c>> replacedAttributesByProductConfig = new Map<Id, List<cscfga__Attribute__c>>();
            for(cscfga__Attribute__c att : replacedattr){
                if(!replacedAttributesByProductConfig.containsKey(att.cscfga__Product_Configuration__c)){
                    replacedAttributesByProductConfig.put(att.cscfga__Product_Configuration__c, new List<cscfga__Attribute__c>());
                }
                
                replacedAttributesByProductConfig.get(att.cscfga__Product_Configuration__c).add(att);
            }
            
            Set<id> configchanged = new Set<id>();
            
            for(Id pcId : newAttributesByProductConfig.keySet()){
                for (cscfga__Attribute__c natt : newAttributesByProductConfig.get(pcId)) {
                    for (cscfga__Attribute__c ratt : replacedAttributesByProductConfig.get(productConfigPair.get(pcId))) {
                        if (natt.name == ratt.name) {
                            System.debug('Comparing attribute ' + natt.Name + ' - NV: ' + natt.cscfga__Value__c + ', OV = ' + ratt.cscfga__Value__c);
                            if ((natt.cscfga__Value__c != ratt.cscfga__Value__c)
                            || (natt.cscfga__Value__c != null && ratt.cscfga__Value__c == null)
                            || (natt.cscfga__Value__c == null && ratt.cscfga__Value__c != null)) {
                                configchanged.add(natt.cscfga__Product_Configuration__c);
                                break;
                            }
                        }
                    }
                }
            }
            if(!configchanged.IsEmpty()){
                for(csord__Service__c service :serviceList){
                    if(service.csordtelcoa__Product_Configuration__c != null && configchanged.contains(service.csordtelcoa__Product_Configuration__c)){
                        service.Updated__c = true;
                    }
                }
            }            
        }
        return serviceList;
    }

/**
* @author Telstra Accenture
* @description Clone and link the non replaced services to the new subscription
* @param servListreplaced List of Replaced services
* @param newSubscriptions subscriptions having the services to update 
* @return List of Cloned services
*/ 
    @testvisible
    private List<csord__Service__c> cloneServices(List<csord__Service__c> servListreplaced , List<csord__Subscription__c> newSubscriptions) {
        Map<Id, csord__Subscription__c> replacedSubscriptionToNewSubscription = new Map<Id, csord__Subscription__c>(newSubscriptions);
        for (csord__Subscription__c newSubscription : newSubscriptions) {
            replacedSubscriptionToNewSubscription.put(newSubscription.csordtelcoa__Replaced_Subscription__c, newSubscription);
        }
        
        /** Clone and link the non replaced services to the new subscription **/
        List<csord__Service__c> clones = new List<csord__Service__c>();
        for (csord__Service__c replacedService: servListreplaced) {
            csord__Subscription__c newSubscription = replacedSubscriptionToNewSubscription.get(replacedService.csord__Subscription__c);
            if (newSubscription == null) {
                System.debug(System.LoggingLevel.WARN, 'Cound not find a replacement subscription for Service : ' + replacedService.Id);
                continue;
            }
            clones.add(cloneService(replacedService, newSubscription));
        }
        
        if (clones.size() > 0) {
            insert clones;
            /** Set the replacement value on replaced services **/
            List<csord__Service__c> originalServices = new List<csord__Service__c>();
            for (csord__Service__c clone : clones) {
                csord__Service__c originalService = clone.csordtelcoa__Replaced_Service__r;
                originalService.csordtelcoa__Replacement_Service__c = clone.Id;
                originalServices.add(originalService);
            }
            update originalServices;
            
            rebuildServiceHierarchy(newSubscriptions, clones);
        }
        
        return clones;
    }
/**
* @author Telstra Accenture
* @description Updates the cloned service to make the new hierarchy of services use all the replements.
* @param newSubscriptions the subscriptions containing the services to update
* @param clones the cloned services to update 
*/    

    @testvisible
    private void rebuildServiceHierarchy(List<csord__Subscription__c> newSubscriptions, List<csord__Service__c> clones) {
        Set<Id> replacedSubscriptionIds = getReplacedSubscriptionIds(newSubscriptions);
        Map<Id, csord__Service__c> replacedServices = new Map<Id, csord__Service__c>(
        [SELECT Id, csordtelcoa__Replacement_Service__c
            FROM csord__Service__c
        WHERE csord__Subscription__c IN :replacedSubscriptionIds]);
        
        List<csord__Service__c> clonesToUpdate = new List<csord__Service__c>();        
        for (csord__Service__c clone : clones) {
            csord__Service__c replacedParent = replacedServices.get(clone.csord__Service__c);
            if (replacedParent != null) {
                clone.csord__Service__c = replacedParent.csordtelcoa__Replacement_Service__c;
                clonesToUpdate.add(clone);
            }
        }
        
        if (!clonesToUpdate.isEmpty()) {
            update clonesToUpdate;
        }
    }

/**
* @author Telstra Accenture
* @description Returns all services without a replacement from the given subscriptions
* @param subscriptions the subscriptions for which to retrun the non-replaced services
* @returns all services without a replacement, can be empty but never null  
*/    
   
    @testvisible
    private List<csord__Service__c> findAllNonReplacedServiceFromOriginalSubscriptions(List<csord__Subscription__c> subscriptions) {
        Map<Id, csord__Service__c> replacedServices = new Map<Id, csord__Service__c>();
        Map<Id, csord__Service__c> newServices = new Map<Id, csord__Service__c>();
        Set<Id> replacedSubscriptionIds = getReplacedSubscriptionIds(subscriptions);
        Set<Id> subscriptionIds = getReplacedSubscriptionIds(subscriptions);
        for(Id Ids :SObjectsUtil.getIds(subscriptions)){subscriptionIds.add(Ids);}
        
        String subsquery = CS_BatchOrderGenerationObserver.getAllFieldQuery('csord__Service__c', new List<Id>(subscriptionIds));
        for(csord__Service__c service :database.query(subsquery.replace('XXX','subscriptionIds'))){
            if(replacedSubscriptionIds.contains(service.csord__Subscription__c)){
                replacedServices.put(service.Id, service);
            } else if(service.csordtelcoa__Replaced_Service__c != null){
                newServices.put(service.csordtelcoa__Replaced_Service__c, service);
            }
        }
        for(Id srvcId :replacedServices.KeySet()){
            if(newServices.get(srvcId) != null){
                replacedServices.remove(srvcId);
            }
        }       
        if(!replacedServices.IsEmpty()){return replacedServices.Values();}
        
        return new List<csord__Service__c>();
    }

/**
* @author Telstra Accenture
* @description Returns the IDs of the subscribptions replaced by the given ones
The clone is meant to be used as a replacement for the cloned service on the new subscription.
* @param subscriptions a collection of subscriptions that replaces other subscriptions
* @return a Set of IDs of the replaced subscriptions
*/    

    @testvisible
    private Set<Id> getReplacedSubscriptionIds(List<csord__Subscription__c> subscriptions) {
        return SObjectsUtil.getIds(subscriptions, 'csordtelcoa__Replaced_Subscription__c');
    }
 

/**
* @author Telstra Accenture
* @description Clone a service from a replaced subscription
* @param replacedService the replaced service to clone
* @param newSubscription the subscription to link to the clone.
* @return the clone service, linked to the given subscription
*/ 

    @testvisible
    private csord__Service__c cloneService(csord__Service__c replacedService, csord__Subscription__c newSubscription) {
        csord__Service__c newService = replacedService.clone(false, true);
        
        newService.csord__Subscription__c              = newSubscription.Id;
        newService.csord__Order_Request__c             = newSubscription.csord__Order_Request__c;
        newService.AccountId__c                        = newSubscription.csord__Account__c;
        newService.csord__Order__c                     = newSubscription.csord__Order__c;
        newService.Opportunity__c                      = newSubscription.OpportunityRelatedList__c;
        newService.Solutions__c                        = newSubscription.Solutions__c;
        newService.Cease_Service_Flag__c               = true;
        newService.RAG_Status_Amber__c                 = false;
        newService.RAG_Status_Red__c                   = false;
        newService.csordtelcoa__Replacement_Service__c = null;
        newService.SNOW_Complete__c                    = false;
        newService.Order_Type_Final__c                 = 'Terminate';
        newService.csord__Status__c                    = 'Service Created';
        newService.Inventory_Status__c                 = 'LIVE - AS DESIGNED';
        newService.ROC_Line_Item_Status__c             = '';        
        newService.SN_request_item_number__c           = '';
        newService.Cloned_Service__c                   = replacedService.Id;
        newService.csordtelcoa__Replaced_Service__c    = replacedService.Id;
        newService.csordtelcoa__Replaced_Service__r    = replacedService;
        newService.csord__Client_Identifier__c         = CreateTerminateOrderP2A.getUniqueExternalId(replacedService.Id, String.ValueOf(system.now()));
        newService.csord__External_Identifier2__c      = CreateTerminateOrderP2A.getUniqueExternalId(newService.csord__Client_Identifier__c, String.ValueOf(system.now()));
        
        return newService;
    }
}