/**
* @author Telstra Accenture
* @description Contains all logic for in flight order changes
*/    
	
	public with sharing class InFlightManager{        
/**
* @author Telstra Accenture
* @description Cancel multiple in flight orders
* @param Orders List of Orders
* @return Orders List of Orders to be cancelled
*/
        public static List<csord__Order__c> cancelOrders(List<csord__Order__c> orders){
            for(csord__Order__c order : orders){
                order = setCancelOrder(order);
            }
            update orders;
            return orders;
        }
 /**
* @author Telstra Accenture
* @description Cancel an in flight order
* @param Order 
* @return Orderto be cancelled
*/       

        public static csord__Order__c cancelOrder(csord__Order__c order){
            order = setCancelOrder(order);
            
            update order;
            return order;
        }
 
/**
* @author Telstra Accenture
* @description Set relevant fields for order cancellation
* @param Order 
* @return Order to be cancelled
*/ 
        private static csord__Order__c setCancelOrder(csord__Order__c order){
            order.Is_InFlight_Cancel__c = true;
            order.Status__c = 'Cancelled';
            order.csord__Status__c = 'Cancelled';
            
            return order;
        }

/**
* @author Telstra Accenture
* @description Clone the basket which is currently synced to the Opportunity
* @param Opportunity under which inflight basket needs to cloned
* @param Orders List of Order for which new basket is required 
* @return product basket cloned inflight basket
*/ 
        
        public static cscfga__Product_Basket__c cloneSyncedBasket(Opportunity opp, List<csord__Order__c> ordersSelected){
            TriggerFlags.NoConfigurationRequestTriggerHandler = true;
            TriggerFlags.NoProductConfigurationTriggers = true;
            TriggerFlags.NoProductBasketTriggers = true;
            TriggerFlags.NoAttributeTriggers = true;
            Savepoint sp = Database.setSavepoint();
            cscfga__Product_Basket__c basket = null;
            Id batchProcessId;

            try{
                cscfga__Product_Basket__c oldBasket = [select Id, Quote_Status__c, csordtelcoa__Account__c, csordtelcoa__Change_Type__c, cscfga__total_contract_value__c 
                    From cscfga__Product_Basket__c 
                        Where cscfga__Opportunity__c =:opp.Id and csbb__Synchronised_With_Opportunity__c = true];
                    
                Map<Id, csord__Service__c> ServicesOfSelectedOrders = new Map<Id, csord__Service__c>([select Id, csordtelcoa__Product_Configuration__c from csord__Service__c where csord__Order__c IN :ordersSelected]);
                
                /** Cloning of the basket started **/
                basket = oldBasket.clone(false, true, false, false);
                basket.name = 'New Basket '+system.now();
                basket.csordtelcoa__Account__c = oldBasket.csordtelcoa__Account__c;
                basket.cscfga__Opportunity__c = opp.Id;
                basket.csordtelcoa__Synchronised_with_Opportunity__c = false;
                basket.csbb__Synchronised_With_Opportunity__c = false;
                basket.csordtelcoa__Change_Type__c = oldBasket.csordtelcoa__Change_Type__c;
                basket.cscfga__Basket_Status__c = 'Valid';
                basket.Quote_Status__c = 'Pending Pricing Approval';
                basket.cscfga__total_contract_value__c = oldBasket.cscfga__total_contract_value__c;
                
                if(basket!=null){insert basket;}
                /** Cloning of the basket finish **/
                
                CloneSubComponentsOfProductConfig bc = new CloneSubComponentsOfProductConfig(oldBasket.Id, basket.Id, ServicesOfSelectedOrders);
                batchProcessId = Database.executeBatch(bc,1);
                
                cscfga__Product_Basket__c bskt = new cscfga__Product_Basket__c(Id=basket.Id, InFlight_Change_Batch_Job_Id__c=batchProcessId);
                update bskt;

            } catch(Exception e){
                Database.rollback(sp);
                System.debug('Error: "AllProductConfigurationTriggerHandler; setProductBasketExchangeRate" method update failure - ' +e);
              }
            return basket;
        }

/**
* @author Telstra Accenture
* @description Transfer content from cloned in flight service line items onto the orginal service line items
* @param Basket Id from which content needs to be transfered to Service line item
*/ 
        
        public static void transferClonedServiceLineItemDetails(Id basketId){
            List<csord__Service_Line_Item__c> SLIsToUpdate = new List<csord__Service_Line_Item__c>();
            Map<Id, csord__Service_Line_Item__c> nonSelectedServiceLineItems = new Map<Id, csord__Service_Line_Item__c>
                ([
                    Select Id, Net_MRC_Price__c, Net_NRC_Price__c, csord__Total_Price__c, csord__Service__r.csordtelcoa__Product_Configuration__r.cscfga__Recurring_Charge__c,csord__Service__r.csordtelcoa__Product_Configuration__r.cscfga__One_Off_Charge__c, csord__Is_Recurring__c 
                        From csord__Service_Line_Item__c 
                            Where csord__Service__r.csordtelcoa__Product_Configuration__r.cscfga__Product_Basket__c =:basketId
                                and csord__Service__r.csordtelcoa__Product_Configuration__r.csordtelcoa__Ignore_For_Order_Decomposition__c = true
                            ]);
            
            for(csord__Service_Line_Item__c sli :nonSelectedServiceLineItems.Values()){
                if(sli.csord__Service__r.csordtelcoa__Product_Configuration__r.cscfga__One_Off_Charge__c != null && !sli.csord__Is_Recurring__c){
                    sli.Net_NRC_Price__c = sli.csord__Service__r.csordtelcoa__Product_Configuration__r.cscfga__One_Off_Charge__c;
                    sli.csord__Total_Price__c = sli.Net_NRC_Price__c;
                    SLIsToUpdate.add(sli);
                }
                else if(sli.csord__Service__r.csordtelcoa__Product_Configuration__r.cscfga__Recurring_Charge__c != null && sli.csord__Is_Recurring__c){
                    sli.Net_MRC_Price__c = sli.csord__Service__r.csordtelcoa__Product_Configuration__r.cscfga__Recurring_Charge__c;
                    sli.csord__Total_Price__c = sli.Net_MRC_Price__c;
                    SLIsToUpdate.add(sli);
                }
            }
            if(!SLIsToUpdate.IsEmpty()){update SLIsToUpdate;}
        }

/**
* @author Telstra Accenture
* @description Set the relevant screen flows for the given product configs
* @param Product configuration
*/		
        public static void setScreenFlows(List<cscfga__Product_Configuration__c> prodConfigs){
            // TODO: Manish has said this will be completed as a separate exercise as read only flows need to be created for each product
         }      
    }