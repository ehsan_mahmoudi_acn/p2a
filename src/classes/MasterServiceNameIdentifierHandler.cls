public with sharing class MasterServiceNameIdentifierHandler {

    // private constants
    private static string masterIPVPNServiceProdDefId = Product_Definition_Id__c.getvalues('Master_IPVPN_Service_Definition_Id').Product_Id__c;
    private static string masterVPLSServiceProdDefId = Product_Definition_Id__c.getvalues('Master_VPLS_Service_Definition_Id').Product_Id__c;
    
    //public static constants
    public static set<Id> basketIdSetMasterIPVPNServices = new set<Id>();
    public static set<Id> basketIdSetMasterVPLSTranServices = new set<Id>();
    public static set<Id> basketIdSetMasterVPLSVLANServices = new set<Id>();
    public static Map<Id,Integer> basketConfigCountMap = new Map<Id,Integer>();
    public static Set<Id> configsToBeUpdated = new Set<Id>();
    public static Boolean isFirstRun = false;    
    public static void  masterServiceNameIdentifierlogic(List<cscfga__Product_Configuration__c> masterConfigs)
    {
       if(!isFirstRun)
       {
        isFirstRun = true; 
        for(cscfga__Product_Configuration__c pc : masterConfigs)
            {
                if(pc.cscfga__Product_Definition__c == masterIPVPNServiceProdDefId && pc.cscfga__Product_Basket__c!=null)
                {
                    basketIdSetMasterIPVPNServices.add(pc.cscfga__Product_Basket__c);
                }
                else if(pc.cscfga__Product_Definition__c == masterVPLSServiceProdDefId && pc.cscfga__Product_Basket__c!=null) 
                {   
                    if(pc.Solution_Type__c == 'Transparent Mode'){
                        basketIdSetMasterVPLSTranServices.add(pc.cscfga__Product_Basket__c);
                    } else if(pc.Solution_Type__c == 'VLAN Mode'){
                        basketIdSetMasterVPLSVLANServices.add(pc.cscfga__Product_Basket__c);
                    }
                }
            
            }
            
            if(basketIdSetMasterIPVPNServices != null && !basketIdSetMasterIPVPNServices.isEmpty())
            {
                AggregateResult[] ar = [Select Count(Id) countId , cscfga__Product_Basket__c basketId 
                                        from cscfga__Product_Configuration__c 
                                        where cscfga__Product_Basket__c in:basketIdSetMasterIPVPNServices 
                                        and cscfga__Product_Definition__c =: masterIPVPNServiceProdDefId  
                                        Group By cscfga__Product_Basket__c];
                
                for(AggregateResult result : ar)
                {
                    basketConfigCountMap.put((Id)result.get('basketId'),Integer.valueOf(result.get('countId'))+1);
                    
                }
            }
            
            if(basketIdSetMasterVPLSTranServices!= null && !basketIdSetMasterVPLSTranServices.isEmpty())
            {
                /*AggregateResult[] ar1 = [Select Count(Id) countId , cscfga__Product_Basket__c basketId
                                         from cscfga__Product_Configuration__c 
                                         where cscfga__Product_Basket__c in:basketIdSetMasterVPLSTranServices 
                                         and cscfga__Product_Definition__c =: masterVPLSServiceProdDefId
                                         and Solution_Type__c = 'Transparent Mode'
                                         Group By cscfga__Product_Basket__c];*/
                
                for(AggregateResult result : [Select Count(Id) countId , cscfga__Product_Basket__c basketId
                                         from cscfga__Product_Configuration__c 
                                         where cscfga__Product_Basket__c in:basketIdSetMasterVPLSTranServices 
                                         and cscfga__Product_Definition__c =: masterVPLSServiceProdDefId
                                         and Solution_Type__c = 'Transparent Mode'
                                         Group By cscfga__Product_Basket__c])
                {
                    basketConfigCountMap.put((Id)result.get('basketId'),Integer.valueOf(result.get('countId'))+1);
                    
                }
            }

            if(basketIdSetMasterVPLSVLANServices!= null && !basketIdSetMasterVPLSVLANServices.isEmpty())
            {
                /*AggregateResult[] ar1 = [Select Count(Id) countId , cscfga__Product_Basket__c basketId 
                                         from cscfga__Product_Configuration__c 
                                         where cscfga__Product_Basket__c in:basketIdSetMasterVPLSVLANServices 
                                         and cscfga__Product_Definition__c =: masterVPLSServiceProdDefId
                                         and Solution_Type__c = 'VLAN Mode' 
                                         Group By cscfga__Product_Basket__c];*/
                
                for(AggregateResult result : [Select Count(Id) countId , cscfga__Product_Basket__c basketId 
                                         from cscfga__Product_Configuration__c 
                                         where cscfga__Product_Basket__c in:basketIdSetMasterVPLSVLANServices 
                                         and cscfga__Product_Definition__c =: masterVPLSServiceProdDefId
                                         and Solution_Type__c = 'VLAN Mode' 
                                         Group By cscfga__Product_Basket__c])
                {
                    basketConfigCountMap.put((Id)result.get('basketId'),Integer.valueOf(result.get('countId'))+1);
                    
                }
            }
                
              System.debug('******basketConfigCountMap:'+basketConfigCountMap);
              if(!basketConfigCountMap.isEmpty())
              {
                    Integer counter = 0;
                    for(cscfga__Product_Configuration__c pc : masterConfigs)
                    {   
                       if(basketConfigCountMap.get(pc.cscfga__Product_Basket__c) != null)
                        {
                           Integer total = counter + basketConfigCountMap.get(pc.cscfga__Product_Basket__c);
                           pc.name_Identifier__c= pc.name + ' '+ total;
                           counter++;
                        }
                        
                        
                    }
                
                
              }
        
        }
    }
}