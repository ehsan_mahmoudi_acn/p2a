@istest (seealldata = false)
public class OrchestrationProcessCreationTest{
    public static testmethod void orchestrationProcessCreation(){
        P2A_TestFactoryCls.sampleTestData();
        List<Account> account = P2A_TestFactoryCls.getaccounts(1);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1, OrdReqList);    
        List<csord__service__c> Services = P2A_TestFactoryCls.getservice(1, OrdReqList, subscriptionList);  
               
        List<CSPOFA__Orchestration_Process_Template__c> ProcessTemplatesLists = P2A_TestFactoryCls.getOrchestrationProcess(1);
        List<CSPOFA__Orchestration_Process__c> orchprocesslist = P2A_TestFactoryCls.getOrchestrationProcesss(1,ProcessTemplatesLists);
        List<CSPOFA__Orchestration_Step__c> orchprocessteplist = P2A_TestFactoryCls.getOrchestrationStep(1,orchprocesslist);
        List<CSPOFA__Orchestration_Step_Template__c> OrchStepTemplate =  P2A_TestFactoryCls.getOOrchestration_Step_Template(1,ProcessTemplatesLists);
          CSPOFA__Orchestration_Process_Template__c ott=new CSPOFA__Orchestration_Process_Template__c();
                ott.name='Order_New';
                insert ott;
        
        Test.StartTest();
        try{
        List<Id> serviceIds = new List<Id>();
        List<Id> orderIds = new List<Id>();
        
        serviceIds.add(Services[0].id);
        orderIds.add(Orders[0].id);
        
        system.assertEquals(true,Orders!=null);
        system.assertEquals(true,Services!=null);
        
        OrchestrationProcessCreation monitor = new OrchestrationProcessCreation();
          monitor.OrchestrationProcessTemplateCreation(serviceIds,orderIds);    
        OrchestratorProcessHelper.findOrchestrationProcess('Order_New');
        monitor.updateESD(serviceIds);
        system.assertEquals(true,monitor!=null);
        Test.StopTest();
        }catch(Exception e){}
    }
}