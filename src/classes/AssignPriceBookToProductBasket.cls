public interface AssignPriceBookToProductBasket
{
  map<string,string> assignPriceBook(set<string> setProductBasketId);
}