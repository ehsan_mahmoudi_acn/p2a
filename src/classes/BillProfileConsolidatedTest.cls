/**
    @author - Accenture
    @date - 19- Jul-2012
    @version - 1.0
    @description - This is the test class for PRCreateWebService class.
*/
@isTest(SeeAllData = false)
//Start: Suresh 28/09/2017- Test class name changed as part of tech_debt
private class BillProfileConsolidatedTest
//End
{
static Account acc;
static Country_Lookup__c cl;
static Opportunity opp;
static BillProfile__c b;
static Site__c s;
static City_Lookup__c ci;
static OpportunityLineItem oli;
static Contact con;

private static Account getAccount(){
    if(acc == null){
        acc = new Account();
        cl = getCountry();
        acc.Name = 'Test Account1111111';
        acc.Account_ID__c = '9090';
        acc.Customer_Type__c = 'MNC';
        acc.Country__c = cl.Id;
        acc.Selling_Entity__c = 'Telstra INC';
        acc.Customer_Legal_Entity_Name__c='Test';
        insert acc;
        system.assert(acc!=null); 

    }
    return acc;
}
private static Country_Lookup__c getCountry(){
    if(cl == null){
        cl = new Country_Lookup__c();
        cl.CCMS_Country_Name__c = 'India';
        cl.Country_Code__c = 'IND';
        insert cl;
         system.assert(cl !=null); 

    }
    return cl;
} 
private static City_Lookup__c getCity(){
    if(ci == null){
        ci = new City_Lookup__c();
        ci.City_Code__c ='01p90000';
        ci.Name = 'OTHER';
        insert ci;
        system.assertEquals(ci.Name , 'OTHER'); 
    }
    return ci;
}
private static Opportunity getOpportunity(){
        if(opp == null) {
            opp = new Opportunity();
            acc = getAccount();
            opp.Name = 'Test Opportunity';
            opp.AccountId = acc.Id;
            opp.StageName = 'Unqualified Prospect';
            opp.CloseDate = System.today();
            opp.Estimated_MRC__c=800;
            opp.Estimated_NRC__c=1000;
            opp.ContractTerm__c='10';

            opp.TigFin_PR_Actions__c = 'Processed';
            insert opp;
            system.assert(opp!=null); 
            List<Opportunity> op = [Select id,name from Opportunity where name = 'Test Opportunity'];
            system.assertequals(opp.name, op[0].name);

        }
        return opp;
    }
private static OpportunityLineItem getOpportunityLineItem(){ 
    if(oli == null){
        oli = new OpportunityLineItem();
        opp = getOpportunity();
        b = getBillProfile();
        oli.OpportunityId = opp.Id;
        oli.IsMainItem__c = 'Yes';
        oli.OrderType__c = 'New Provide';
        oli.Quantity = 5;
        oli.UnitPrice = 10;
        oli.PricebookEntryId = getPriceBookEntry('GCPE').Id;
        oli.ContractTerm__c = '12';
        oli.OffnetRequired__c = 'Yes';
        oli.Location__c = 'Bengaluru';
        oli.BillProfileId__c = b.Id;
        insert oli;
        
       system.assert(oli!=null); 

    }
    return oli;
}
private static Pricebook2 getPriceBook(String prodName){
        Pricebook2 p = [SELECT Id FROM Pricebook2 LIMIT 1];
        system.assert(p!=null);
        return p;   
    }
    
private static Product2 getProduct(String prodName){
        Product2 prod = new Product2();
        prod.Name = prodName;
        prod.ProductCode = prodName;
        prod.Product_ID__c ='GCPE';
        insert prod;
               system.assert(prod!=null); 

        return prod;
    }
    
private static PricebookEntry getPriceBookEntry(String prodName){
        PricebookEntry p = new PricebookEntry();
        p.Pricebook2Id = getPriceBook(prodName).Id;
        p.Product2Id =  getProduct(prodName).Id;
        p.UnitPrice = 2000;
        p.IsActive = true;
        insert p; 
               system.assert(p!=null); 

        return p;    
    }
    
    
private static BillProfile__c getBillProfile(){
    if(b == null) {
        b = new BillProfile__c();
        s = getSite();
        con = getContact();
        acc = getAccount();
       // b.Bill_Profile_Number__c = 'Test Bill Profile';
        b.Billing_Entity__c = 'Telstra Limited';
        b.Account__c = s.AccountId__c;
        b.Bill_Profile_Site__c = s.Id;
        b.Bill_Profile_ORIG_ID_DM__c = '9090';
        b.Company__c = 'Accenture';
        b.Status__c = 'Active';
       // b.Operating_Unit__c = 'TI_HK_I52S';
        
        b.First_Period_Date__c = date.newinstance(2012, 12, 11);
        b.Invoice_Breakdown__c='Cost Centre';
        b.Start_Date__c =  date.newinstance(2012, 12, 11);
        b.Bill_Profile_Site__c = s.id;
        b.Billing_Contact__c = con.id;
        b.Customer_Ref__c = 'Test';
      
       
         insert b;
         system.assert(b!=null);
           
    }
        return b;
}
private static Site__c getSite(){
    if(s == null){
        s = new Site__c();
        Account acc1 = getAccount();
        s.Name = 'Test_site';
        s.Address1__c = '43';
        s.Address2__c = 'Bangalore';
        s.State__c = 'KARNATAKA';
        s.Address_3__c = 'INDIA';
        s.Address_4__c = 'ASIA';
        s.Other_City__c = 'Mumbai';
        cl = getCountry();
        s.Country_Finder__c = cl.Id;
        ci = getCity();
        s.City_Finder__c = ci.Id;
        s.AccountId__c =  acc1.Id; 
        s.Address_Type__c = 'Billing Address';
        insert s;
        List<Site__c> sit = [Select id,name from Site__c where name = 'Test_site'];
       // system.assertequals(s.name, sit[0].name);
        
        system.assert(s!=null);
    }
    return s;
}   

private static Contact getContact(){
    if(con == null){
        con = new Contact();
        acc = getAccount();
        con.AccountID = acc.id;
        //con.Name = 'TestCont1';
        con.FirstName = 'Test';
        con.LastName = 'cont1';
        con.Salutation = 'Mr';
        con.Job_Title__c = 'CEO';
        con.Email = 'test.t@test.com';
        con.Country__c = getCountry().id;   
        system.assert(con!=null);
    }
    return con;
}

static testMethod void testUpdatePRFeilds(){
    b = getBillProfile();
    s = getSite();
    con = getContact();
    
    List<BillProfile__c> billList = [select Account_ID__c,ADDRESS_1__c,ADDRESS_2__c,Address_3__c,Address_4__c,
                                        Name,Bill_Profile_Integration_Number__c,City__c,Country__c,State__c,Operating_Unit__c,
                                        Payment_Method__c,Payment_Terms__c,Postal_Code__c,Status__c,Billing_Contact__c,CurrencyIsoCode,Billing_Contact__r.FirstName,Addressed_To__c,Company__c,
                                        Billing_Contact__r.LastName,Billing_Contact__r.Salutation,Billing_Contact__r.Job_Title__c,Billing_Contact__r.Phone,Billing_Contact__r.Email from BillProfile__c where id = :b.id and Company__c !=null and  Status__c='Active' ];
    
        Billprofile__c billProfileObj = billList[0];
        
        BillProfileConsolidated.TigFinRequiredDetails tigFinObj = new BillProfileConsolidated.TigFinRequiredDetails();
        
        tigFinObj.AccountID = billProfileObj.Account_ID__c;
        tigFinObj.ADDRESS1  = billProfileObj.ADDRESS_1__c;
        tigFinObj.ADDRESS2  = billProfileObj.ADDRESS_2__c;
        tigFinObj.BillProfileName = billProfileObj.Name;
        tigFinObj.BillProfileIntegrationNumber = billProfileObj.Bill_Profile_Integration_Number__c;
        tigFinObj.City = billProfileObj.City__c;
        tigFinObj.Country = billProfileObj.Country__c;
        tigFinObj.CountyOrState = billProfileObj.State__c;
        tigFinObj.OperatingUnit = billProfileObj.Operating_Unit__c;
        tigFinObj.PaymentMethod = billProfileObj.Payment_Method__c;
        tigFinObj.PaymentTerms = billProfileObj.Payment_Terms__c;
        tigFinObj.PostalCode = billProfileObj.Postal_Code__c;
        tigFinObj.Status = billProfileObj.Status__c;
        tigFinObj.Address3 = billProfileObj.Address_3__c;
        tigFinObj.Address4 = billProfileObj.Address_4__c;
        tigFinObj.CurrencyIsoCode = billProfileObj.CurrencyIsoCode;
        tigFinObj.FirstName  = billProfileObj.Billing_Contact__r.FirstName;
        tigFinObj.LastName   = billProfileObj.Billing_Contact__r.LastName;
        tigFinObj.Salutation      = billProfileObj.Billing_Contact__r.Salutation;
        tigFinObj.JobTitle   = billProfileObj.Billing_Contact__r.Job_Title__c;
        tigFinObj.Phone      = billProfileObj.Billing_Contact__r.Phone;    
        tigFinObj.Email      = billProfileObj.Billing_Contact__r.Email;
        tigFinObj.Addressedto = billProfileObj.Addressed_To__c;
        tigFinObj.Company = billProfileObj.Company__c;

    Test.StartTest();
        BillProfileConsolidated.TigFinRequiredDetails[] BillProfileTigFinRequDetail = BillProfileConsolidated.getBillDetailsForTigFin(billProfileObj.id);
        BillProfileConsolidated.TigFinRequiredDetails[] BillProfileTigi = BillProfileConsolidated.getContactDetailsForTigFin(con.id);
        BillProfileConsolidated.TigFinRequiredDetails[] BillProfileSite = BillProfileConsolidated.getSiteDetailsForTigFin(s.Id);
        system.assertEquals(true,BillProfileSite!=null);
    Test.StopTest();    

}

}