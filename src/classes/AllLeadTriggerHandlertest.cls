@isTest(seealldata=false)
public class AllLeadTriggerHandlertest{

    static testmethod void leadstart()
    {
            AllLeadTriggerHandler AlLeadTrHdlr = new AllLeadTriggerHandler();     
            LeadBeforeTrgHandler leadBfrTrgHdlr = new LeadBeforeTrgHandler();
            LeadAfterTrgHandler leadAftTrgHdlr = new LeadAfterTrgHandler();
            system.assert(AlLeadTrHdlr !=null);
    }
    static testmethod void testLeadDelTrigger1()
    {
         Test.startTest();
         
         Profile profObj = [select Id ,name from Profile  where name ='System Administrator'];
        
         Lead leadToDelete = new Lead(Company = 'Telstra', Country_Picklist__c = 'HONG KONG',
                               Status = 'Sales Accepted Lead (SAL)', Disqual_Reason__c= 'Bad fit for Telstra',
                               LeadSource= 'Advertising', Industry = 'BPO', Email='testleadDelTrgHdlr@abc.com',
                                    LastName='test' );
        insert leadToDelete; 
        system.assert(leadToDelete!=null);
        try
        {
            delete leadToDelete;
           
        } catch (Exception e) {
            System.assert(true, 'Deletion failed appropriately');
        }
        Test.stopTest();
    }        
                        
            //TestLeadBeforeTrgHandler testLeadBfrTrgHdlr = new TestLeadBeforeTrgHandler();
            
          
           // TestLeadBeforeTrgHandler.TestLeadBeforeTrgHandler1();
            //TestLeadBeforeTrgHandler.TestLeadBeforeTrgHandler2();
            //TestLeadBeforeTrgHandler.TestLeadBeforeTrgHandler3();
           // AlLeadTrHdlr.beforeInsert(leadist);
            //AlLeadTrHdlr.afterInsert (leadist);
            //AlLeadTrHdlr.afterInsert(leadist);
            //AlLeadTrHdlr.beforeDelete(leadist,newdMap,oldMap);
            //AlLeadTrHdlr.beforeUpdate(leadist,newdMap,oldMap);
     
    /*private static List<Lead> leadist = new List<Lead>(); 
    static Account a;
    static Country_Lookup__c cl;
    static Lead ld;
   private static Map<Id, Lead> oldMap = new Map<Id, Lead>();
      private static  Map<Id, Lead> newdMap = new Map<Id, Lead>();  
    Static Account acc1 = getAccount1();

    static opportunity opp1=getOpportunity();
    //static Lead lead1=getLead();
    static List<Lead> lead1 = getLead();
    
    
      private static Account getAccount1(){                
        Account acc = new Account();            
        Country_Lookup__c c = getCountry();            
        acc.Name = 'Test Account';            
        acc.Customer_Type__c = 'MNC';            
        acc.Country__c = c.Id;            
        acc.Selling_Entity__c = 'Telstra INC';  
        acc.Activated__c= true;  
        acc.Account_ID__c = '9090';  
        acc.Customer_Legal_Entity_Name__c = 'Test' ;        
        insert acc;       
        return acc;    
   } 
   private static Country_Lookup__c getCountry1(){        
           Country_Lookup__c country = new Country_Lookup__c();            
           country.CCMS_Country_Code__c = 'HK';            
           country.CCMS_Country_Name__c = 'India';            
           country.Country_Code__c = 'HK';            
           insert country;        
           return country;    
         } 
           private static Opportunity getOpportunity(){                 
            Opportunity opp = new Opportunity();            
            //Account acc = getAccount();           
            opp.Name = 'Test Opportunity';            
            opp.AccountId = acc1.Id;   
             //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement         
            opp.StageName = 'Identify & Define' ;  
            opp.Stage__c= 'Identify & Define' ;          
            opp.CloseDate = System.today();   
            opp.Opportunity_Type__c = 'Complex';
            opp.Estimated_MRC__c=8;
                opp.Estimated_NRC__c=2;
                opp.ContractTerm__c='1';
                opp.Order_Type__c='New';
            //opp.Stage_Gate_1_Action_Created__c = true;         
            //opp.Approx_Deal_Size__c = 9000;                 
            return opp;    
           } 
           private static cscfga__Product_Basket__c  getProductbasket(){
              cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c ();
              pb.cscfga__Opportunity__c =opp1.id;
              pb.csordtelcoa__Synchronised_with_Opportunity__c =true;
              pb.csbb__Synchronised_With_Opportunity__c =true;
             
             return pb;  
           }
       
    
    private static Account getAccount(){
        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Account' LIMIT 1];
        if(a == null){    
            a = new Account();
            Country_Lookup__c country = getCountry();
            a.Name = 'Test Account1995';
            a.Customer_Type__c = 'GSP';
            //a.Customer_Type__c = 'MNC';
            a.Country__c = country.Id;
            a.Selling_Entity__c = 'Telstra INCN';
            a.Account_Manager__c = 'Albert Thomas';
            a.Activated__c = true;
            a.RecordTypeId = rt.Id;
            a.Account_Status__c ='Active';
            a.Region__c='US';
            a.Account_ID__c='123456abcd1';
            a.Customer_Legal_Entity_Name__c='Test';
            insert a;
        }
        return a;
    }
    private static Country_Lookup__c getCountry(){
        if(cl == null){ 
            cl = new Country_Lookup__c();
            cl.CCMS_Country_Code__c = 'SG';
            cl.CCMS_Country_Name__c = 'India';
            cl.Country_Code__c = 'SG';
            insert cl;
        }
        return cl;
    }
    
    //private static Lead  getLead(){
    private static List<Lead> getLead(){ 
            ld = new Lead();
            ld.Region__c = 'US';
            ld.Company = 'Telstra International12345';
            ld.LastName = 'Adp123';
            ld.Status = 'Marketing Qualified Lead (MQL)';
            ld.LeadSource = 'Direct Mail';
            ld.Email='test@email.com';
            ld.Industry='Telecommunications';
            ld.Country_Picklist__c = 'BANGLADESH';
            leadist.add(ld);
            insert leadist;
            return leadist;
            
            
            
           // Test.startTest();
            //Util.assignOwnersToRegionalQueue(usr,objActionItem1,'Bill Profile');
          //  AllLeadTriggerHandler  AlLeadTrHdlr = new AllLeadTriggerHandler();     
           // AlLeadTrHdlr.beforeInsert(leadist);
           // AlLeadTrHdlr.afterInsert;
           // AlLeadTrHdlr.beforeUpdate({ld},newdMap,oldMap);
           // AlLeadTrHdlr.afterUpdate;
          //  AlLeadTrHdlr.beforeDelete({ld},newdMap,oldMap);
      //  Test.stopTest();
        
                
    }*/
    
}