@isTest
private class AssignGroupTest {

    Static TestMethod Void testcase1()
    {
       
        User_Group_Mapper__c usrgroup = new User_Group_Mapper__c();           
         usrgroup.Group_Name__c = 'Test Group';       
         usrgroup.User_Region__c = 'US';        
         usrgroup.Profile_Name__c = 'TI Finance';         
         insert usrgroup; 
        
        Group groupm = new Group();           
         groupm.Name = 'Test Group';       
         groupm.Type = 'Regular';       
         insert groupm; 
        
        User_Group_Mapper__c usrgroup1 = new User_Group_Mapper__c();           
         usrgroup1.Group_Name__c = 'Test Group1';       
         usrgroup1.User_Region__c = 'US';        
         usrgroup1.Profile_Name__c = 'TI Credit Control';         
         insert usrgroup1; 
        
        Group groupm1 = new Group();           
         groupm1.Name = 'Test Group1';       
         groupm1.Type = 'Regular';       
         insert groupm1; 
            user u;
    User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    System.runAs ( thisUser ) {          
        User userObj1 = new User();
       userObj1.FirstName = 'Test';
       userObj1.LastName = 'SFDC';
       userObj1.IsActive  = true;
       userObj1.Username = 'Test.SFDC@team.telstra.com.evolution';
       userObj1.Email = 'Test.SFDC@team.telstra.com';
       userObj1.Region__c = 'US';
       userObj1.TimeZoneSidKey = 'Australia/Sydney';
       userObj1.LocaleSidKey = 'en_AU';
       userObj1.EmailEncodingKey = 'ISO-8859-1';
       userObj1.LanguageLocaleKey = 'en_US';
       userObj1.Alias = 'TSFDC';
       userObj1.EmployeeNumber='123456';
       Profile p = [select id from Profile where name = 'TI Finance'];
       
       userObj1.ProfileId = p.id;
       
       insert userObj1; 
       system.assert(userObj1!=null);
    }
       u=new user();
        u=[select id from user where profile.Name='System Administrator' and Isactive=true limit 1];
        System.runAs(u)
        {
        Test.startTest();
        Profile p1 = [select id from Profile where name = 'TI Credit Control'];
        
        user upuser=[select id from user limit 1];
           
        upuser.ProfileId = p1.id;
      
        update upuser;
        
        
            
        Test.stopTest();
        }
       
        
    }
  
}