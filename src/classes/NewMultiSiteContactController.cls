/**
    @author - Accenture
    @date - 26-March-2012
    @version - 1.0
    @description - This class is used to display the All Existing Sites related 
                   to Contact in the component.
*/

public class NewMultiSiteContactController {
    
// Variable 
private String controllerId;

    // Constructor 
    // @param - ApexPages.StandardController controller
    public NewMultiSiteContactController (ApexPages.StandardController controller) {
        String Id = System.currentPageReference().getParameters().get('retURL');
        Id = Id.Substring(1,Id.length());
        availableList = new List<SelectOption>();   
        chosenList=new List<SelectOption>();
        controllerId = Id;
        // Get the Associated Account From the Contact
        Contact cont = [Select Id, Name,AccountId from Contact where Id=:Id ];
        
        // query Site to get Site Object based on request account ID
        List<Site__c> AllSites = [Select Id,Name,AccountId__c  from Site__c where AccountId__c=:cont.AccountId and valid__c= true ];
        
        // query Sites_Contacts to get Sites_Contacts Object based on request contact ID
        //List<Sites_Contacts__c> siteContacts = [Select Id,Name,Site__c from Sites_Contacts__c where contact__c=:controllerId];
        
        // Creating set to store IDs
        Set<Id> availableSiteId = new Set<Id>();
        
        // Adding the multiple Site IDs related to contact
        for (Sites_Contacts__c siteContact : [Select Id,Name,Site__c from Sites_Contacts__c where contact__c=:controllerId]){
            availableSiteId.add(siteContact.site__c);
        }
        for (integer i=0; i<AllSites.size();i++){
            if (availableSiteId.contains(AllSites.get(i).Id)){
                AllSites.remove(i);
            }
        }
        
        // Adding the selected sites
        for (Site__c site : AllSites){
            availableList.add(new selectOption( site.id+'',site.Name));
        } 
        
     }

// Getter Setter Methods
public List<SelectOption> availableList{get;set;}
public List<SelectOption> chosenList{get;set;}

    // Default Constructor
    public NewMultiSiteContactController (){ 
    
    }
     
 //  Action on button "saveSite"
 public PageReference saveSite(){
 // Delete All the Contacts and Sites Specific to the Contact
 //List<Sites_Contacts__c> allSiteContactSpecific = [Select Id,Name,Site__c from Sites_Contacts__c where contact__c=:controllerId];
 //delete allSiteContactSpecific;
 // Then Save the Entire List
 List<Sites_Contacts__c> siteContacttoSave = new List<Sites_Contacts__c>();
 for (SelectOption chosenOne : chosenList){
    Sites_Contacts__c siteContact = new Sites_Contacts__c(); 
     siteContact.site__c = chosenOne.getValue();
     siteContact.contact__c = controllerId;
     siteContacttoSave.add(siteContact);
 }
 insert siteContacttoSave;
 String redirectUrl ='/'+controllerId;
  return new PageReference(redirectURL);
 }   
 
 //  Action on button "cancelSite"
 public PageReference cancelSite(){
     String redirectUrl ='/'+controllerId;
  return new PageReference(redirectURL);
 }
}