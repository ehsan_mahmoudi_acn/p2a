/**
        @author - Accenture
        @date - 25-Feb-2016
        @description - This class will be called to update the attribute values in the product configuration. 
    */  
Public Class SetProductConfigAttributeValue{
    String status;
    public String setValue(String destinationId, String sourceId, String productName){
        string sourceField;
        String sourceObjectAPIName;
        //Set the query string for the source object from the custom setting
        try{
            List<custom_object_to_product_configuration__c> mcs = custom_object_to_product_configuration__c.getall().values();
            for(custom_object_to_product_configuration__c iterator : mcs ){
                if(iterator.product_name__c == productName){
                    if(sourceField==Null){
                        sourceField = iterator.source_field__c;
                        sourceObjectAPIName = iterator.source_object__c;
                    }else{
                        sourceField = sourceField + ',' + iterator.source_field__c;
                    }
                }
            }
            
            String query = 'SELECT ' + sourceField + ' FROM ' + sourceObjectAPIName + ' WHERE ID = \'' + sourceID + '\'';
            //Fetch the value from the Source object
            SObject sourceObject = database.query(query);
            //system.debug(sourceObject.get('recurring_Cost__c'));
            // Initialize the CloudSense APIs
            cscfga__Product_Configuration__c ProductConfiguration = [SELECT Id, cscfga__Product_Bundle__c,cscfga__Product_Basket__c, cscfga__Configuration_Offer__c, cscfga__Product_Configuration__c.Product_Name__c  
                                                                     FROM cscfga__Product_Configuration__c 
                                                                     WHERE Id =: destinationId];
             
            cscfga.API_1.ApiSession apiSession = cscfga.API_1.getApiSession();
            apiSession.setConfigurationToEdit(ProductConfiguration);
            cscfga.ProductConfiguration config = apiSession.getConfiguration();
            // Set the value of the product configuration attribute
            for(custom_object_to_product_configuration__c iterator : mcs ){
                if(iterator.product_name__c == productName){
                    String setValue = String.valueof(sourceObject.get(iterator.Source_Field__c));
                    config.getAttribute(iterator.Production_Configuration_Attribute_Name__c).setValue(setvalue);
                    //system.debug(config.getAttribute(iterator.Production_Configuration_Attribute_Name__c).getValue());
                }
            }
            //Execute rules and persist the configuration
            apiSession.executeRules();
            apiSession.updateLineItems();
            apiSession.persistConfiguration();
            apiSession.close();
        } catch (Exception e){
            //Return error due to an exception
            return 'Error';
        }
        //Return success
        return 'Success';
    }
}