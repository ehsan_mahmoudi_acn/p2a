global with sharing class CustomButtonPricingRequests extends csbb.CustomButtonExt {
    
    public String performAction(String basketId) {
        String Margin;
        //String newUrl = CustomButtonPricingRequest.CreateCase(basketId);
        // fetch data from product config
        id uid = userinfo.getUserId();
        id pid = userinfo.getProfileId();
        Integer Count = 0, TargetCount = 1;
        system.debug('Sowmya-------------CountInitial'+Count);
        cscfga__Product_Basket__c baskets = [select id,csbb__Account__c, cscfga__Opportunity__c, csbb__Account__r.Customer_Type_New__c, Account_Owner_ID__c, Opportunity_Owner_ID__c from cscfga__Product_Basket__c where id =: basketId];
        string OppTeamOpp = baskets.cscfga__Opportunity__c;
        List<OpportunityTeamMember> OppTeam = [SELECT Id,Name,TeamMemberRole,UserId FROM OpportunityTeamMember WHERE OpportunityId =: OppTeamOpp];
        
        if(baskets.csbb__Account__r.Customer_Type_New__c == 'ISO')
        {
            return '{"status":"error","title":"Error","text":"You cannot raise a Pricing approval request for ISO Accounts"}';  
        }
		//Sumit -- Validation Added for Null Opportunity for TGP0041869
        else if(OppTeamOpp == '' || OppTeamOpp == null)
        {
            return '{"status":"error","title":"Error","text":"Opportunity is not linked with this Basket, Kindly link the Opportunity with Basket before clicking the Pricing Approval Button"}';
        }
        else if(uid != baskets.Account_Owner_ID__c && uid != baskets.Opportunity_Owner_ID__c && pid != Id.valueOf(Label.Profile_System_Administrator) && pid != Id.valueOf(Label.Profile_TI_Terminate_Team) && pid != Id.valueOf(Label.Profile_TI_Account_Manager) && pid != Id.valueOf(Label.Profile_TI_Sales_Admin) && pid != Id.valueOf(Label.Profile_TI_Order_Desk))
        {
            return '{"status":"error","title":"Error","text":"You are not authorized to request this action"}';
        }
        else
        {
            //Check status Pending supplier quotation error message
            List<cscfga__Product_Basket__c> basketIDs = [select id,Quote_Status__c from cscfga__Product_Basket__c where id =:basketId];
            for(cscfga__Product_Basket__c basket : basketIDs){
                if(basket.Quote_Status__c=='Draft'){
                    return '{"status":"error","title":"Error","text":"The quote status is draft, hence cannot request for Pricing"}';
                }            
                if(basket.Quote_Status__c=='Pending supplier quotation'){
                    return '{"status":"error","title":"Error","text":"The quote contains off-net components and a supplier quote has not yet been populated"}';
                }
                if(basket.Quote_Status__c=='Approved' || basket.Quote_Status__c=='Accepted'){
                    return '{"status":"error","title":"Error","text":"The quote is already approved/accepted"}';
                }
            }
            List<Case> CaseListtocheck = [select id,status from Case where Product_Basket__c =:basketId and Is_Price_Approval_Request__c=True and status NOT IN ('Approved','Rejected')];
            for(cscfga__Product_Basket__c basket : basketIDs){
                if(CaseListtocheck.size()>0){
                    return '{"status":"error","title":"Error","text":"Pricing approval has been already requested for this quote"}';
                }
            }
            
            //List<cscfga__Product_Configuration__c> prodConFigLst= [select name,id, Cost_MRC__c, Cost_NRC__c, cscfga_Offer_Price_MRC__c, cscfga_Offer_Price_NRC__c, cscfga__Contract_Term__c,cscfga__Product_Basket__c, cscfga__Product_Basket__r.cscfga__Opportunity__c,cscfga__Product_Basket__r.cscfga__Opportunity__r.accountID,cscfga__Product_Basket__r.cscfga__Opportunity__r.account.Customer_Type__c,cscfga__Product_Basket__r.cscfga__Opportunity__r.account.Selling_Entity__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c  = :basketId ];            
            
            List<cscfga__Attribute__c > attLst = [select cscfga__Product_Configuration__c,cscfga__Product_Configuration__r.name, name,cscfga__Value__c from cscfga__Attribute__c where 
                ( name = 'ContractTerm_Gen__c'  or name = 'Target_Check__c' )  
            and cscfga__Product_Configuration__c IN (select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId ) ];
            
            system.debug('Sowmya-------------attLst'+attLst);
            system.debug('Sowmya-------------attLst'+attLst.size());       
            for(cscfga__Product_Configuration__c pc : [select name,id, Cost_MRC__c, Cost_NRC__c, cscfga_Offer_Price_MRC__c, cscfga_Offer_Price_NRC__c, cscfga__Contract_Term__c,cscfga__Product_Basket__c, cscfga__Product_Basket__r.cscfga__Opportunity__c,cscfga__Product_Basket__r.cscfga__Opportunity__r.accountID,cscfga__Product_Basket__r.cscfga__Opportunity__r.account.Customer_Type__c,cscfga__Product_Basket__r.cscfga__Opportunity__r.account.Selling_Entity__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c  = :basketId ]){
                
                for(cscfga__Attribute__c att : attLst){
                    if((att.name =='ContractTerm_Gen__c' && pc.cscfga__Product_Basket__r.cscfga__Opportunity__r.account.Customer_Type__c == 'MNC' && (att.cscfga__Value__c == '48' || att.cscfga__Value__c == '60') && (att.cscfga__Product_Configuration__r.name == 'GCPE' || att.cscfga__Product_Configuration__r.name == 'IPVPN' || att.cscfga__Product_Configuration__r.name == 'EPL' ||att.cscfga__Product_Configuration__r.name== 'IPL' || att.cscfga__Product_Configuration__r.name == 'GIE' || att.cscfga__Product_Configuration__r.name == 'VPLS' || att.cscfga__Product_Configuration__r.name == 'EVPL')) || (pc.cscfga__Product_Basket__r.cscfga__Opportunity__r.account.Customer_Type__c == 'MNC' && (att.cscfga__Product_Configuration__r.name == 'Colo' || att.cscfga__Product_Configuration__r.name == 'EPLX')) || (att.name == 'ContractTerm_Gen__c' && pc.cscfga__Product_Basket__r.cscfga__Opportunity__r.account.Customer_Type__c == 'MNC' && (att.cscfga__Value__c == 'Other')))
                    {
                        Count = 1;
                        system.debug('Sowmya-------------CountafterCheck'+Count);
                    }
                }
                for(cscfga__Attribute__c att : attLst){
                    if((att.name == 'Target_Check__c' && att.cscfga__Value__c == 'Yes' )){
                        TargetCount = 0;
                        system.debug('Sowmya-------------TargetCount'+TargetCount);
                    }
                }
            }
            
            String newURL = CustomButtonPricingRequests.rtnURL(basketId);
            return '{"status":"ok","redirectURL":"' + newUrl + '","title":"Success","text":"Pricing case created successfully"}';
            //return '{"status":"ok","title":"Success","text":"Pricing case created successfully" "redirectURL":"' + newUrl + '"}';
        }
    }
    
    public static String rtnURL(String basketId) {
        
        system.debug('intomymethod');
        String queryString = 'select Busines_Capex__c,Business_Opex__c,cscfga__total_contract_value__c,csordtelcoa__Account__r.Current_IT_Spend__c,csordtelcoa__Account__r.Telstra_Legal_Entity_Name__c,cscfga__Opportunity__r.Order_Type__c,Margin__c, CurrencyIsoCode,Product_Basket_Id__c,cscfga__Opportunity__r.csordtelcoa__Change_Type__c,cscfga__Opportunity__c,cscfga__Opportunity__r.OwnerID,csordtelcoa__Account__c from cscfga__Product_Basket__c where id=:'+ 'basketid';
        cscfga__Product_Basket__c basketRecord = database.query(queryString);
        Id recordTypeID = [Select id from RecordType where sObjectType = 'case' and DeveloperName = 'Price_Request'].id;
        Map<Id,cscfga__Product_Configuration__c> prodConFigMap= new Map<Id,cscfga__Product_Configuration__c>([select name,id, Cost_MRC__c, Cost_NRC__c, cscfga_Offer_Price_MRC__c, cscfga_Offer_Price_NRC__c, cscfga__Contract_Term__c,cscfga__Product_Basket__c, cscfga__Product_Basket__r.cscfga__Opportunity__c,cscfga__Product_Basket__r.cscfga__Opportunity__r.accountID,cscfga__Product_Basket__r.cscfga__Opportunity__r.account.Customer_Type__c,cscfga__Product_Basket__r.cscfga__Opportunity__r.account.Selling_Entity__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c  = :basketId ]); 
        //Creating Pricing case        
        case caseObj = new case();
        caseObj.CurrencyIsoCode              = basketRecord.CurrencyIsoCode;
        caseObj.Subject                      = 'Pricing approval request for Quote: ' +basketRecord.Product_Basket_Id__c;      
        caseObj.Order_Type__c                = basketRecord.cscfga__Opportunity__r.csordtelcoa__Change_Type__c != null ? basketRecord.cscfga__Opportunity__r.csordtelcoa__Change_Type__c: basketRecord.cscfga__Opportunity__r.Order_Type__c;
        caseObj.Contracting_Entity__c        = basketRecord.csordtelcoa__Account__r.Telstra_Legal_Entity_Name__c != null ? basketRecord.csordtelcoa__Account__r.Telstra_Legal_Entity_Name__c: '';
        caseObj.Product_Basket__c            = basketRecord.id;
        caseObj.Opportunity_Name__c          = basketRecord.cscfga__Opportunity__c;
        caseObj.Opportunity_Owner__c         = basketRecord.cscfga__Opportunity__r.OwnerID;
        caseObj.Account_Name__c              = basketRecord.csordtelcoa__Account__c;
        caseObj.AccountId                    = basketRecord.csordtelcoa__Account__c;
        caseObj.status                       = 'Unassigned';
        caseObj.Is_Price_Approval_Request__c = true;
        //caseObj.Margin_Value__c              = basketRecord.Margin__c;
        //caseObj.TCV__c                       = basketRecord.cscfga__total_contract_value__c;
        caseObj.Current_Spend__c             = basketRecord.csordtelcoa__Account__r.Current_IT_Spend__c;
        //Manish -- Added capex and opex as part of the defect fix for 13488
        caseObj.Business_Capex__c            = basketRecord.Busines_Capex__c;
        caseObj.Business_Opex__c             = basketRecord.Business_Opex__c;   
        
        insert caseObj;
		
		ProductConfigurationSequence.setPcHierarchy(prodConFigMap.values());
        PricingApproval.updateProductInformationFromPCTrigger(prodConFigMap.keyset(),system.today());
		LinkAndAssociateServiceWithClonedPC.updateLinkedServiceIdOnEnrichment(prodConFigMap.values(),null); 
		
        PricingFinancialSummarycls controller  = new PricingFinancialSummarycls( new ApexPages.StandardController(caseObj));
        caseObj.TCV__c = controller.revenueGrandTotal;
        caseObj.Margin_Value__c = controller.grossmarginPerGrandTotal;          
        update caseObj;
        return '/'+caseObj.id;
    }
}