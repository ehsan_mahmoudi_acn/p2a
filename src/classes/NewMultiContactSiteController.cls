/**
    @author - Accenture
    @date - 27-March-2012
    @version - 1.0
    @description - This class is used to display the All Existing Contacts related 
                   to Site in the component.
*/
public class NewMultiContactSiteController {
    
    // Variable
    private String controllerId;
    
    // Constructor
    // @param - ApexPages.StandardController controller
    public NewMultiContactSiteController(ApexPages.StandardController controller) {
        String Id = System.currentPageReference().getParameters().get('retURL');
        Id = Id.Substring(1,Id.length());
        availableList = new List<SelectOption>();   
        chosenList=new List<SelectOption>();
        controllerId = Id;
        
        // query Site to get Site Object based on request Site ID
        Site__c sit = [Select Id, Name, AccountId__c from Site__c where Id=:Id ];
        
        // query Contact to get Contact Object based on request Account ID
        List<Contact> AllContacts = [Select Id,Name  from Contact where AccountId=:sit.AccountId__c];
        
        // query Sites_Contacts to get Sites_Contacts Object based on request Account ID
        //List<Sites_Contacts__c> siteContacts = [Select Id,Name,Site__c,contact__c from Sites_Contacts__c where site__c=:controllerId];
        
        // Creating the Set to store IDs
        Set<Id> availablecontactId = new Set<Id>();
        
        // Adding the multiple Contact IDs related to site
        for (Sites_Contacts__c siteContact : [Select Id,Name,Site__c,contact__c from Sites_Contacts__c where site__c=:controllerId]){
            availablecontactId .add(siteContact.contact__c);
        }
        for (integer i=0; i<AllContacts.size();i++){
            if (availablecontactId.contains(AllContacts.get(i).Id)){
                AllContacts.remove(i);
            }
        }
        // Adding the selected contacts
        for (Contact cont : AllContacts){
            availableList.add(new selectOption( cont.id+'',cont.Name));
        }         
    }
    
    // Getter Setter Methods
    public List<SelectOption> availableList{get;set;}
    public List<SelectOption> chosenList{get;set;}

    // Action on button "saveContact"
    public PageReference saveContact(){
         List<Sites_Contacts__c> siteContacttoSave = new List<Sites_Contacts__c>();
         for (SelectOption chosenOne : chosenList)
         {
             Sites_Contacts__c siteContact = new Sites_Contacts__c(); 
             siteContact.contact__c = chosenOne.getValue();
             siteContact.site__c = controllerId;
             siteContacttoSave.add(siteContact);
         }
     insert siteContacttoSave;
     String redirectUrl ='/'+controllerId;
     return new PageReference(redirectURL);
 }   
 
// Action on button "cancelContact"
public PageReference cancelContact(){
     String redirectUrl ='/'+controllerId;
  return new PageReference(redirectURL);
 }

}