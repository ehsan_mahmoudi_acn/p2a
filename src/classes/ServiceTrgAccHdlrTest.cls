@isTest(SeeAllData = false)
public with sharing class ServiceTrgAccHdlrTest 
{   
        public static List<Account> acclist =  P2A_TestFactoryCls.getAccounts(1);
        public static List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        public static List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        public static List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
        public static List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1,orderRequestList);
        public static List<csord__Order__c> ordList = P2A_TestFactoryCls.getorder(1,orderRequestList);       
        public static List<csord__Service__c> servicelist = P2A_TestFactoryCls.getService(1,orderRequestList,subList);
    
        static testmethod void serviceTrgAccHdlrTest(){   
        
        Map<Id,csord__Service__c>oldServiceMap = new Map<Id,csord__Service__c>();
        
        for(csord__Service__c serv:servicelist) {
           oldServiceMap.put(serv.id,serv);  
        }
        
        servicelist[0].AccountId__c = acclist[0].id;
        servicelist[0].Inventory_Status__c = 'Decommissioned';//'LIVE - AS DESIGNED';
        servicelist[0].Path_Instance_ID__c = '1438070';
        servicelist[0].Primary_Service_ID__c = 'SYD IPT 9751508';
        update servicelist[0];  
        
        List<NewLogo__c>  NewLogolist = new List<NewLogo__c>
        {
            new NewLogo__c(name = 'Active',Action_Item__c = 'Active'),
            new NewLogo__c(name = 'Decommissioned',Action_Item__c = 'Decommissioned'),
            new NewLogo__c(name = 'CANCELLED',Action_Item__c = 'CANCELLED'),
        new NewLogo__c(name = 'Logo_Former_Customer',Action_Item__c = 'Former Customer')
        };
        
        insert NewLogolist;
        system.assert(NewLogolist!=null);
        ServiceTrgAccHdlr service = new ServiceTrgAccHdlr();
        Test.starttest();
              service.serTrgAccHdlrMthd(servicelist,oldServiceMap);
        Test.stoptest();
    }
}