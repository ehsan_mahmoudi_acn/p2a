public class MasterServiceLogicOld {
    
    // private context data
    private static Map<Id, Object> contextBasketMap;
    private static Map<Id, cscfga__Product_Configuration__c> contextProductMap;
    private static Map<Id, cscfga__Product_Definition__c> definitionMap;
    /*
    private static Map<String, String> productToMasterServiceMap = new Map<String, String> { 
        key15(Label.IPVPN_Port_Definition_Id)         => key15(Label.Master_IPVPN_Service_Definition_Id),
        //key15(Label.ASBR_Definition_Id)               => key15(Label.Master_IPVPN_Service_Definition_Id),
        //key15(Label.Standalone_ASBR_Definition_Id)    => key15(Label.Master_IPVPN_Service_Definition_Id),
        key15(Label.SMA_Gateway_Definition_Id)    => key15(Label.Master_IPVPN_Service_Definition_Id),
        key15(Label.VPLS_Transparent_Definition_Id)   => key15(Label.Master_VPLS_Service_Definition_Id),
        key15(Label.VPLS_VLAN_Port_Definition_Id)     => key15(Label.Master_VPLS_Service_Definition_Id)  
        //key15(Label.EVPL_Definition_Id)               => key15(Label.Master_EVPL_Service_Definition_Id) 
    };
    */
    
    private static Map<String, String> productToMasterServiceMap = new Map<String, String> { 
        key15(Product_Definition_Id__c.getvalues('IPVPN_Port_Definition_Id').Product_Id__c) => key15(Product_Definition_Id__c.getvalues('Master_IPVPN_Service_Definition_Id').Product_Id__c),
        key15(Product_Definition_Id__c.getvalues('SMA_Gateway_Definition_Id').Product_Id__c) => key15(Product_Definition_Id__c.getvalues('Master_IPVPN_Service_Definition_Id').Product_Id__c),
        key15(Product_Definition_Id__c.getvalues('VPLS_Transparent_Definition_Id').Product_Id__c)   => key15(Product_Definition_Id__c.getvalues('Master_VPLS_Service_Definition_Id').Product_Id__c),
        key15(Product_Definition_Id__c.getvalues('VPLS_VLAN_Port_Definition_Id').Product_Id__c) => key15(Product_Definition_Id__c.getvalues('Master_VPLS_Service_Definition_Id').Product_Id__c)  
    };
    
    /* private static void initMasterServiceMap() {
        if (productToMasterServiceMap == null) {
            productToMasterServiceMap = new Map<String, String>();
            for (Master_Service_Map__c ms: Master_Service_Map__c.getall().values()) {
                productToMasterServiceMap.put(key15(ms.Product_Definition_Id__c), key15(ms.Master_Product_Definition_Id__c));
            }
        }   
    } */
   
    /* 
    
    // these methods are used for two phase scenario, in order to be able to add master by hand, and then update the references afterwards:
    // 1. create masters first
    // 2. update the master references
    
    public createMasters(Set<Id> basketIds) {
        // initialize static data
        initDefinitionMap();
        
        // initialize context data 
        initContextData(basketIds);
    
        // check the basket map if has items to process for
        if (contextBasketMap.size() > 0) {
        
            // create missing master service products
            createBasketMasterService();
        }
    }

    public updateMasterReferences(Set<Id> basketIds) {
        // initialize static data
        initDefinitionMap();
        
        // initialize context data 
        initContextData(basketIds);
    
        // check the basket map if has items to process for
        if (contextBasketMastersMap.size() > 0) {
        
            // update master services references
            updateProductsMasterService();
        }
    }
    
    */
   
    // main business logic method
    public static void process(Set<Id> basketIds) {

        // initialize static data
        initDefinitionMap();
        
        // initialize context data 
        initContextData(basketIds);
        
        // process context data
        processContextData();
    }
    
    @TestVisible
    private static void initDefinitionMap() {

        // ensure that they exists!
        definitionMap = new Map<Id, cscfga__Product_Definition__c>([SELECT id
                , Name
                , cscfga__Product_Category__c 
            FROM 
                cscfga__Product_Definition__c 
            WHERE 
                Id in :productToMasterServiceMap.keySet() /* :definitionIds */ or 
                Id in :productToMasterServiceMap.values() /* :masterIPVPNServiceProdDefId */ ]);
    
        system.debug('definitionMap: ' + definitionMap.keySet());
    }
    
    @TestVisible
    private static void initContextData(Set<Id> basketIds) {
        
        Map<Id, cscfga__Product_Configuration__c> allProductsMap = new Map<Id, cscfga__Product_Configuration__c>([SELECT Id
            , Name
            , cscfga__Product_Basket__r.Name
            , cscfga__Product_Basket__c
            , cscfga__Product_Definition__r.Name
            , cscfga__Product_Definition__c
            , Master_IPVPN_Configuration__c
            , cscfga__Configuration_Offer__c 
        FROM 
            cscfga__Product_Configuration__c 
        WHERE 
            cscfga__Product_Basket__c in :basketIds 
                and cscfga__Product_Definition__c in :productToMasterServiceMap.keySet() /*:definitionIds */ 
                and cscfga__Configuration_Offer__c = '' ORDER BY Id DESC]);/* added order by clause to attach it to the newest Master service created-- Mamta*/
        
        system.debug('productToMasterServiceMap.keySet: ' + productToMasterServiceMap.keySet());
        system.debug('allProductsMap.keySet: ' + allProductsMap.keySet());
        
        // initialize data context map
        contextBasketMap = new Map<Id, Object>();
        contextProductMap = new Map<Id, cscfga__Product_Configuration__c>();
        
        // iterate and collect data
        for (cscfga__Product_Configuration__c pc: allProductsMap.values()) {
            contextProductMap.put(pc.Id, pc);
            contextBasketMap.put(pc.cscfga__Product_Basket__c, 'proccess');
        }
    }
    
    @TestVisible
    private static void processContextData() {
        
        // check the basket map if has items to process for
        if (contextBasketMap.size() > 0) {
        
            // create missing master service products
            createBasketMasterService();

            // update ipvpn master services references
            updateProductsMasterService();
        }
    }
    
    @TestVisible
    private static string key15(Id sfdcId) {
        return 
            string.valueOf(sfdcId).substring(0, 15);
    }

    @TestVisible
    private static String getMasterId(cscfga__Product_Configuration__c config) {
        return 
            productToMasterServiceMap.get(key15(config.cscfga__Product_Definition__c));
    }
    
    @TestVisible
    private static String getBasketMasterId(cscfga__Product_Configuration__c config) {
        return 
            key15(config.cscfga__Product_Basket__c) + productToMasterServiceMap.get(key15(config.cscfga__Product_Definition__c));
    }
    
    @TestVisible
    private static Map<String, cscfga__Product_Configuration__c> createBasketMasterMap(Set<Id> basketIds) {
        
        //select all Master IPVPN Service product configurations whose basketId equals IPVPN Port basketId
        List<cscfga__Product_Configuration__c> masters = [SELECT Id
                , Name
                , cscfga__Product_Basket__r.Name
                , cscfga__Product_Basket__c
                , cscfga__Product_Definition__c
                , cscfga__Product_Definition__r.Name  
            FROM 
                cscfga__Product_Configuration__c 
            WHERE 
                cscfga__Product_Definition__c in :productToMasterServiceMap.values() /*:masterIPVPNServiceProdDefId*/ AND 
                cscfga__Product_Basket__c in :basketIds];
        
        //Create Map<ID,cscfga__Product_Configuration__c> Where ID = basketId and cscfga__Product_Configuration__c = IPVPN Port object
        Map<String, cscfga__Product_Configuration__c> mastersMap = new Map<String, cscfga__Product_Configuration__c>();
        for (cscfga__Product_Configuration__c config : masters) {
            mastersMap.put(key15(config.cscfga__Product_Definition__c), config);
        }
        
        return mastersMap;
    }
    
    @TestVisible
    private static void createBasketMasterService() { 
        
        // take the basketIds from the context map
        Set<Id> basketIds = contextBasketMap.keySet();
        
        // check and create missing IPVPNService config
        Map<String, cscfga__Product_Configuration__c> basketMasterMap = createBasketMasterMap(basketIds);
        
        Map<String, cscfga__Product_Configuration__c> mastersToCreateMap = new Map<String, cscfga__Product_Configuration__c>();
        for (cscfga__Product_Configuration__c config: contextProductMap.values()) {
            if (basketMasterMap.containsKey(getMasterId(config)) == false && mastersToCreateMap.containsKey(getBasketMasterId(config)) == false) {
                mastersToCreateMap.put(getBasketMasterId(config)
                    , new cscfga__Product_Configuration__c(Name = definitionMap.get(getMasterId(config)).Name
                        , cscfga__Product_Definition__c = getMasterId(config)
                        , cscfga__Product_Basket__c = config.cscfga__Product_Basket__c ));
            }
        }
        
        // insert the configs
        List<cscfga__Product_Configuration__c> toInsert = mastersToCreateMap.values();
        if (mastersToCreateMap.size() > 0) {
            
            insert toInsert;
            
            // creating config requests is needed to have them in the basket
            createMasterConfigurationRequests(toInsert);
            
            // execute rules and update line item
            finishMasterConfigurations(toInsert);
        }
    }
    
    @TestVisible
    private static void createMasterConfigurationRequests(List<cscfga__Product_Configuration__c> masters) {
        
        List<csbb__Product_Configuration_Request__c> toInsert = new List<csbb__Product_Configuration_Request__c>();
        for (cscfga__Product_Configuration__c master: masters) {
            toInsert.add (new csbb__Product_Configuration_Request__c(csbb__Product_Basket__c = master.cscfga__Product_Basket__c
                , csbb__Product_Category__c = definitionMap.get(master.cscfga__Product_Definition__c).cscfga__Product_Category__c
                , csbb__Product_Configuration__c = master.Id
                , csbb__Optionals__c = '{}'));
        }
        
        if (toInsert.size() > 0) {
            insert toInsert;
        }
    }
    
    @TestVisible
    private static void finishMasterConfigurations(List<cscfga__Product_Configuration__c> masters) {

        // this should go into batch but the business logic shouldn't permit executing this more than once
        // , as only one master service should be creted within the basket.
        /* cscfga.API_1.ApiSession apiSession; */
        Set<Id> masterIds = new Set<Id>();
        for (cscfga__Product_Configuration__c master: masters) {
            /* apiSession = cscfga.API_1.getApiSession();
            apiSession.setConfigurationToEdit(master);
            cscfga.ProductConfiguration config = apiSession.getConfiguration(); 
            apiSession.executeRules();
            apiSession.updateLineItems();
            apiSession.persistConfiguration();
            apiSession.close(); */
            
            /* apiSession = cscfga.API_1.getApiSession(master); 
            apiSession.executeRules();
            apiSession.updateLineItems();
            apiSession.persistConfiguration();
            apiSession.close(); */
            
            masterIds.add(master.Id);
        }
        
        if (masterIds.size() > 0) {
            cscfga.ProductConfigurationBulkActions.revalidateConfigurations(masterIds);
        }
    }
    
    @TestVisible
    private static void updateProductsMasterService() {
        
        // take the basketIds from the context map
        Set<Id> basketIds = contextBasketMap.keySet();
        
        // take the product configuration ids
        List<cscfga__Product_Configuration__c> pcs = contextProductMap.values();
        
        // create the basket to master ipvpn service map
        Map<String, cscfga__Product_Configuration__c> basketMasterMap = createBasketMasterMap(basketIds);
        
        // update master service for every port
        List<cscfga__Product_Configuration__c> toUpdate = new List<cscfga__Product_Configuration__c>();
        for(cscfga__Product_Configuration__c pc : pcs) {
            toUpdate.add(new cscfga__Product_Configuration__c(Id = pc.Id
                , Master_IPVPN_Configuration__c = basketMasterMap.get(getMasterId(pc)).Id));
        }
        
        if (toUpdate.size() > 0) {
            system.debug('toUpdate' + toUpdate);
            update toUpdate;
        }
    
    } 
}