@isTest
private class CleanOrchestrationstepsTest
{
    public static List<CSPOFA__Orchestration_Process_Template__c> OrcProTem = P2A_TestFactoryCls.getOrchestrationProcess(1);
    public static List<CSPOFA__Orchestration_Process__c> OrcPro = P2A_TestFactoryCls.getOrchestrationProcesss(1, OrcProTem);
    public static List<CSPOFA__Orchestration_Step__c> OrcSte = P2A_TestFactoryCls.getOrchestrationStep(1, OrcPro);
    static testmethod void unittest() 
    {
        Test.startTest();
            CleanOrchestrationsteps  cleanorch = new CleanOrchestrationsteps();
            cleanorch.execute(null, OrcSte);
            Database.executeBatch(cleanorch);
        Test.stopTest();
          system.assertEquals(true,cleanorch!=null);
    }
}