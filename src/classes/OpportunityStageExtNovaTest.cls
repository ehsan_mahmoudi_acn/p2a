/**
 * Name : OpportunityStageExt_Nova_Test
 * Author : NovaCop Unit Test Generator
 * Description : Test class used for testing the OpportunityStageExt
 * Date : 8/12/14 3:55 PM 
 * Version : <intial Draft> 
 * TODO : This code is auto-generated. Developer should update the inputs for unit tests as well as expected results in assertions
 */
@isTest
private class OpportunityStageExtNovaTest { 
    /*
     *@name testSaveStage_Scenario1()
     *@return void 
     *@description This method for SaveStage () In CLASS OpportunityStageExt.cls
     */
     static testMethod void  testSaveStageScenario1(){ 
        User stdUser = UtilNovaUnitTest.getUser();
        Opportunity  opportunityNovaTest  = UtilNovaUnitTest.getOpportunity();
        opportunityNovaTest.StageName='Identify & Define';
        opportunityNovaTest.Stage__c='Identify & Define';
        opportunityNovaTest.Sales_Status__c='Open';
        opportunityNovaTest.Win_Loss_Reasons__c=null;
        Account acc=UtilNovaUnitTest.getAccount();
        insert acc;
      
        System.assert(acc!=null); 
        
        ApexPages.StandardController ctr = new ApexPages.StandardController(opportunityNovaTest );
        OpportunityStageExt opportunityStageExtTest  =  new  OpportunityStageExt(ctr);
        Test.startTest();
        Test.setCurrentPage(Page.SimplePopup);
        System.runAs(stdUser) {
            opportunityNovaTest.AccountId=acc.id;
            insert opportunityNovaTest;
            opportunityStageExtTest.oppId=opportunityNovaTest.Id;
            opportunityStageExtTest.opportunityNextStage='Qualify';
            opportunityStageExtTest.winlossReason=false;
            PageReference pagereferenceTest = opportunityStageExtTest.SaveStage();
            Test.stopTest();
            // System.assert(pagereferenceTest!=null);
            
        }
    }
    
        @isTest static void testExceptions(){
         OpportunityStageExt alls=new OpportunityStageExt(null);
        
         try{alls.saveStage();}catch(Exception e){}
       
          system.assertEquals(true,alls!=null);    
         
         
     }  
}