@istest
public class AsyncWwwTibcoComAffOrderserviceTest{
    
    
    static testMethod void beginSuspendOrder() {
    
    AsyncWwwTibcoComAffOrderservice.SuspendOrderResponse_elementFuture Asyncoffordservice = new AsyncWwwTibcoComAffOrderservice.SuspendOrderResponse_elementFuture();
    
    AsyncWwwTibcoComAffOrderservice.SyncSubmitOrderResponse_elementFuture SyncSubmitOrderResponse = new AsyncWwwTibcoComAffOrderservice.SyncSubmitOrderResponse_elementFuture();
    
    AsyncWwwTibcoComAffOrderservice.GetOrdersResponse_elementFuture GetOrdersResponse = new AsyncWwwTibcoComAffOrderservice.GetOrdersResponse_elementFuture();
    
    AsyncWwwTibcoComAffOrderservice.PerformBulkOrderActionResponse_elementFuture PerformBulkOrderActionResponse = new AsyncWwwTibcoComAffOrderservice.PerformBulkOrderActionResponse_elementFuture();
    
    wwwTibcoComAffOrderservice.PerformBulkOrderActionResponse_element PerformBulkAction = PerformBulkOrderActionResponse.getValue();
    
    
     //Test.setMock(WebServiceMock.class, new MainMockClass.SyncSubmitOrderResponse_GetOrderDetails());
     //AsyncWwwTibcoComAffOrderservice.GetOrderDetailsResponse_elementFuture GetOrderDetailsResponse = new AsyncWwwTibcoComAffOrderservice.GetOrderDetailsResponse_elementFuture();
     //wwwTibcoComAffOrder.orderType Ordertype =  GetOrderDetailsResponse.getValue();
    
    AsyncWwwTibcoComAffOrderservice.AmendOrderResponse_elementFuture AmendOrderResponse = new AsyncWwwTibcoComAffOrderservice.AmendOrderResponse_elementFuture();
    
    AsyncWwwTibcoComAffOrderservice.SubmitOrderResponse_elementFuture SubmitOrderResponse = new AsyncWwwTibcoComAffOrderservice.SubmitOrderResponse_elementFuture();
    
    AsyncWwwTibcoComAffOrderservice.CancelOrderResponse_elementFuture CancelOrderResponse = new AsyncWwwTibcoComAffOrderservice.CancelOrderResponse_elementFuture();
    
    //AsyncWwwTibcoComAffOrderservice.GetOrderExecutionPlanResponse_elementFuture GetOrderExecutionPlanResponse = new AsyncWwwTibcoComAffOrderservice.GetOrderExecutionPlanResponse_elementFuture();
    //wwwTibcoComAffPlan.planType planType  = GetOrderExecutionPlanResponse.getValue();
    
    AsyncWwwTibcoComAffOrderservice.WithdrawOrderResponse_elementFuture WithdrawOrderResponse = new AsyncWwwTibcoComAffOrderservice.WithdrawOrderResponse_elementFuture();
     
    AsyncWwwTibcoComAffOrderservice.GetEnrichedExecutionPlanResponse_elementFuture GetEnrichedExecutionPlanResponse = new AsyncWwwTibcoComAffOrderservice.GetEnrichedExecutionPlanResponse_elementFuture();
     
    AsyncWwwTibcoComAffOrderservice.ActivateOrderResponse_elementFuture ActivateOrderResponse = new AsyncWwwTibcoComAffOrderservice.ActivateOrderResponse_elementFuture();
    
    Test.startTest();
        wwwTibcoComAffOrderservice.SuspendOrderResponse_element SuspendOrderResponse =  Asyncoffordservice.getValue();
        wwwTibcoComAffOrderservice.SyncSubmitOrderResponse_element SyncResponse = SyncSubmitOrderResponse.getValue();
        wwwTibcoComAffOrderservice.GetOrdersResponse_element GetorderResp =  GetOrdersResponse.getValue();
        AmendOrderResponse.getValue();
        SubmitOrderResponse.getValue();
        wwwTibcoComAffOrderservice.CancelOrderResponse_element cancelorder = CancelOrderResponse.getValue();
        wwwTibcoComAffOrderservice.WithdrawOrderResponse_element Withdrawrespose = WithdrawOrderResponse.getValue();
        wwwTibcoComAffOrderservice.GetEnrichedExecutionPlanResponse_element GetEnrichPlanResponse =  GetEnrichedExecutionPlanResponse.getValue();
        wwwTibcoComAffOrderservice.ActivateOrderResponse_element   ActivateResponse =  ActivateOrderResponse.getValue();
    Test.stopTest();
      
        system.assertEquals(true,Asyncoffordservice!=null);  
         system.assertEquals(true,SyncSubmitOrderResponse!=null);  
         system.assertEquals(true,GetOrdersResponse!=null);  
        system.assertEquals(true,PerformBulkOrderActionResponse!=null);  
       system.assertEquals(true,AmendOrderResponse!=null);  
        system.assertEquals(true,SubmitOrderResponse!=null);  
        system.assertEquals(true,CancelOrderResponse!=null);  
        system.assertEquals(true,WithdrawOrderResponse!=null);  
        system.assertEquals(true,GetEnrichedExecutionPlanResponse!=null);  
          system.assertEquals(true,ActivateOrderResponse!=null);        
                        
    
    
    }
}