public class AllOpportunitySplitTriggersHandler extends BaseTriggerHandler{
    public override Boolean isDisabled(){
    if(test.isrunningtest()){
    return false;
    }
        if (TriggerFlags.NoOpportunitySplitTriggers || !label.MuteSPT.equalsignorecase('FALSE') ) {
            return true;
        }
        return triggerDisabled;
    }
    
   public override void beforeInsert(List<SObject> newOpportunitySplit) 
    {
        createOpportunitySplit((List<OpportunitySplit>) newOpportunitySplit);
    }   
    
    
 public override void beforeUpdate(List<SObject> newOpportunitySplit, Map<Id, SObject> newOpportunitySplitMap,Map<Id, SObject> oldnewOpportunitySplitMap) 
    
    {
        if(!Util.IsOppOwnerchange){
            validationOnOpportunitydeletion((List<OpportunitySplit>) newOpportunitySplit, (Map<Id, OpportunitySplit>) newOpportunitySplitMap,(Map<Id, OpportunitySplit>) oldnewOpportunitySplitMap);
    
        }
     }   
  public override void beforeDelete(Map<Id, SObject> oldnewOpportunitySplitMap) 
    
    {
        if(!Util.IsOppOwnerchange){
        validationOnOpportunitydeletion(null,null,(Map<Id, OpportunitySplit>) oldnewOpportunitySplitMap);
        }
    }
 public override void afterInsert(List<SObject> newOpportunitySplit, Map<Id, SObject> newOpportunitySplitMap) 
    {
    }   
    
    
 public override void afterUpdate(List<SObject> newOpportunitySplit, Map<Id, SObject> newOpportunitySplitMap,Map<Id, SObject> oldnewOpportunitySplitMap) 
    
    {
    }   
    public void createOpportunitySplit(List<OpportunitySplit> newOpportunitySplit){
        if(newOpportunitySplit.size()>0){
        List<Opportunity> opp=[select id,ownerId,NewOppSplit__c from Opportunity where id=:newOpportunitySplit[0].opportunityId];
        ID splittypeidactive = [select DeveloperName,isactive,id from OpportunitySplitType where isactive=:true][0].id;
        for(OpportunitySplit oppSplit:newOpportunitySplit){
       // oppSplit.SplitPercentage=100;
       if(opp[0].NewOppSplit__c){
        oppSplit.SplitTypeid=splittypeidactive;
        oppSplit.Team_Role__c='Opportunity Owner';
        oppSplit.SplitNote=oppSplit.Team_Role__c;
        
        }
        }   
        }   
    }
    public void validationOnOpportunitydeletion(List<OpportunitySplit> newOpportunitySplit, Map<Id, OpportunitySplit> newOpportunitySplitMap,Map<Id, OpportunitySplit> oldnewOpportunitySplitMap){
        
        try{
                Set<Id> opIdsSet=new Set<Id>();
                List<OpportunitySplit> oppSplitList=new List<OpportunitySplit>();
                Map<id,OpportunitySplit> mapOppSplit=new Map<id,OpportunitySplit>();
                if(trigger.isdelete){oppSplitList=oldnewOpportunitySplitMap.values();mapOppSplit=oldnewOpportunitySplitMap;}else{oppSplitList=newOpportunitySplit;mapOppSplit=newOpportunitySplitMap;}
                List<UserRole> userRoleList=[select Id,parentRoleId from UserRole];
                List<OpportunitySplit> oppSplit=[select id,CurrUserProfileName__c,Opportunity.Owner.userRoleId,Opportunity.StageName from Opportunitysplit where id in:mapOppSplit.keyset()];
                for(OpportunitySplit opp:oppSplit){
                if(!System.Label.ProfileNotForSplitOpp.contains(opp.CurrUserProfileName__c)){
                if(opp.Opportunity.StageName=='Closed Won' && !System.Label.ProfileForSplitOpp.contains(opp.CurrUserProfileName__c)){
                mapOppSplit.get(opp.id).adderror('Opportunity Split Update is restricted to users other than Administrator');
                return;
            }
        }
    }

Set<Id> ownerRoleSet=new Set<Id>();

for(OpportunitySplit opp:oppSplit){

ownerRoleSet.add(opp.Opportunity.Owner.userRoleId);
}


        Id tempid=null;
    for(Id rId:ownerRoleSet){
       tempid=rId;
      // system.debug('=====tempid1===='+tempid);
        for(integer i=0;i<userRoleList.size();i++){ 
        
            if(userRoleList[i].Id==tempid){
           //system.debug('=====tempid2===='+tempid);
            ownerRoleSet.add(userRoleList[i].ParentRoleID);
            tempid=userRoleList[i].ParentRoleID;
            i=0;
           //system.debug('=====tempid3===='+tempid);
            }
        
            }   
        
    } 
system.debug('===ownerRoleSet==='+ownerRoleSet);
system.debug('===UserInfo.getUserRoleId()==='+UserInfo.getUserRoleId());
system.debug('====frferree'+!ownerRoleSet.contains(UserInfo.getUserRoleId()));

for(Opportunitysplit opp:oppSplitList){
if(!System.Label.ProfileNotForSplitOpp.contains(opp.CurrUserProfileName__c)){
if(!TriggerFlags.NoOpportunitySplitValError && !ownerRoleSet.contains(UserInfo.getUserRoleId()) && !System.Label.ProfileForSplitOpp.contains(opp.CurrUserProfileName__c)){
opp.adderror('Opportunity Split Update is restricted to users other than Opportunity Owner and Administrator.');
}
}
}
}catch(exception ex){
  ErrorHandlerException.ExecutingClassName='AllOpportunitySplitTriggersHandler:validationOnOpportunitydeletion';
  ErrorHandlerException.objectList=newOpportunitySplit;
  ErrorHandlerException.sendException(ex);
system.debug('ex'+ex.getmessage());
}
        
        
    }
}