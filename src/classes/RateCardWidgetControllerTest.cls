@isTest
public class RateCardWidgetControllerTest {
    
    static testmethod void testRateCardMethod1() {
        cspmb__Rate_Card__c rc = new cspmb__Rate_Card__c(
            Name = 'Test'
        );
        insert rc;
        
        cspmb__Rate_Card_Line__c rcl = new cspmb__Rate_Card_Line__c(
            Name = 'Test',
            cspmb__Off_Peak__c = 10,
            cspmb__Rate_Card__c = rc.Id
        );
        insert rcl;
        
        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(
            Name = 'Test'
        );
        insert priceItem;
        
        cspmb__Price_Item_Rate_Card_Association__c rcAssoc = new cspmb__Price_Item_Rate_Card_Association__c(
            cspmb__Price_Item__c = priceItem.Id,
            cspmb__Rate_Card__c = rc.Id
        );
        insert rcAssoc;
        
        Rate_Card_Widget_Settings__c rcSettings = new Rate_Card_Widget_Settings__c(
            Name = 'Test',            
            Rate_Card_Fields__c = 'cspmb__Is_Active__c',
            Rate_Card_Line_Fields__c = 'cspmb__Rate_Card_Line_Unit__c,cspmb__Weekend__c,cspmb__Off_Peak__c,cspmb__Peak__c',
            Editable_Rate_Card_Line_Fields__c = 'cspmb__Rate_Card_Line_Unit__c,cspmb__Weekend__c,cspmb__Off_Peak__c,cspmb__Peak__c'
        );
        insert rcSettings;
        
        Rate_Card_Widget_Settings__c rcSettings1 = new Rate_Card_Widget_Settings__c(
            Name = 'Test1',
            Rate_Card_Line_Fields__c = 'cspmb__Rate_Card_Line_Unit__c,cspmb__Weekend__c,cspmb__Off_Peak__c,cspmb__Peak__c',
            Editable_Rate_Card_Line_Fields__c = 'cspmb__Rate_Card_Line_Unit__c,cspmb__Weekend__c,cspmb__Off_Peak__c,cspmb__Peak__c',
            Save_as_JSON__c = true
        );
        insert rcSettings1;
         //Creating Account object records
        Account act = new Account(Name='Test112233M1',Customer_Type__c='MNC',Selling_Entity__c ='Telstra Incorporated',cscfga__Active__c='Yes',
                Activated__c= True,
                Account_ID__c = '33331',
                Account_Status__c = 'Active',
                Customer_Legal_Entity_Name__c = 'Test');
        insert act;
        cscfga__Configuration_Offer__c csfa = new cscfga__Configuration_Offer__c(name='Master  VPLS VLAN Service',cscfga__Active__c =true,cscfga__Template__c=false);
        insert csfa;
        List<Offer_Id__c> offIdlist = new List<Offer_Id__c>{
                 new Offer_Id__c(name ='Master_IPVPN_Service_Offer_Id',Offer_Id__c = csfa.id),
                 new Offer_Id__c(name ='Master_VPLS_Transparent_Offer_Id',Offer_Id__c = csfa.id),
                 new Offer_Id__c(name ='Master_VPLS_VLAN_Offer_Id',Offer_Id__c = csfa.id)
       };
       insert offIdlist;
        //Creating Opportunity object records
        Opportunity oppObj = new Opportunity(Name='GFTS Ph 2',AccountID=act.Id,Opportunity_Type__c='Simple',
                                           CurrencyIsoCode = 'USD', CloseDate = Date.today(),StageName='Identify & Define',Stage__c='Identify & Define',
                                            QuoteStatus__c ='Approved',Sales_Status__c= 'Won',Win_Loss_Reasons__c ='Product',
                                            Order_Type__c= 'New', ContractTerm__c = '24');
        insert oppObj; 
        
        oppObj.Quote_Simplification__c= true;
        //oppObj.R3_Products_Opportunity__c = true;
        oppObj.Product_Type__c ='Global Multi-Cloud';
        oppObj.Estimated_NRC__c = 10.00;
        oppObj.Estimated_MRC__c = 10.00;
        update oppObj;
        
        //insert contact object records
         Contact contObj = new Contact(AccountId=act.Id,LastName='tech',
                                  email='test@gmail.com');
        insert contObj; 
        //insert Product Basket object record
        cscfga__Product_Basket__c pbasket = new cscfga__Product_Basket__c();
        pbasket.csbb__Account__c = act.id;
        pbasket.csordtelcoa__Account__c = act.id;
        pbasket.cscfga__Opportunity__c = oppObj.id;
        insert pbasket;
        
        //insert product definition object record
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.name = 'Master IPVPN Service';
        pd.cscfga__Description__c ='112312331';
        insert pd;
        
        
        cscfga__Product_Definition__c pd1 = new cscfga__Product_Definition__c();
        pd1.name = 'Master VPLS Service';
        pd1.cscfga__Description__c ='Test master VPLS';
        insert pd1;
        Product_Definition_Id__c setting = new Product_Definition_Id__c();
        setting.Name = 'VLANGroup_Definition_Id';
        setting.Product_Id__c = pd.ID;
        insert setting;
        //insert custom setting records
        Product_Definition_Id__c pd6 = new Product_Definition_Id__c();
        pd6.name = 'IPVPN_Port_Definition_Id';
        pd6.Product_Id__c =pd1.ID;
        insert pd6;
        Product_Definition_Id__c pd7 = new Product_Definition_Id__c();
        pd7.name = 'SMA_Gateway_Definition_Id';
        pd7.Product_Id__c =pd.id;
        insert pd7;
        Product_Definition_Id__c pd8 = new Product_Definition_Id__c();
        pd8.name = 'VPLS_VLAN_Port_Definition_Id';
        pd8.Product_Id__c =pd1.id;
        insert pd8;
        Product_Definition_Id__c pd9 = new Product_Definition_Id__c();
        pd9.name = 'VPLS_Transparent_Definition_Id';
        pd9.Product_Id__c =pd.id;
        insert pd9;
        Product_Definition_Id__c pd2 = new Product_Definition_Id__c();
        pd2.name = 'Master_IPVPN_Service_Definition_Id';
        pd2.Product_Id__c =pd1.id;
        insert pd2;
        Product_Definition_Id__c pdk = new Product_Definition_Id__c();
        pdk.name = 'Master_VPLS_Service_Definition_Id';
        pdk.Product_Id__c =pd.id;
        insert pdk;
        
        //insert product configuration record for Master IPVPN Service
        List<cscfga__Product_Configuration__c> pclist= new List<cscfga__Product_Configuration__c>{
            new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd.id,cscfga__Product_Basket__c = pbasket.Id),
            new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd1.id,cscfga__Product_Basket__c = pbasket.Id,Solution_Type__c='Transparent Mode'),
            new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd1.id,cscfga__Product_Basket__c = pbasket.Id,Solution_Type__c='VLAN Mode')
           };
        insert pclist;
        system.assert(pclist!=null);
        
        Map<String, Object> rcMap = new Map<String, Object>();        
        rcMap.put('rateCardLines', JSON.serialize(rcl));
        rcMap.put('Name', 'Test');
        
        List<Object> objList = new List<Object>{rcMap};
        
        Test.startTest();
        
        RateCardWidgetController.ParsedRateCardWrapper rcWrapper = new RateCardWidgetController.ParsedRateCardWrapper();
        rcWrapper.rateCardId = rc.Id;
        rcWrapper.rateCard = rc;
        rcWrapper.rateCardLines = new List<cspmb__Rate_Card_Line__c>{rcl};
        
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        account acc = accList[0];
        
        ObjectPrefixIdentifiers__c op=new ObjectPrefixIdentifiers__c();
        op.ObjectIdPrefix__c='a07';
        op.name='Contract';
        insert op;
        system.assert(op!=null);
        // test rc get method
        String rateCards = RateCardWidgetController.getAvailableRateCards(null, priceItem.Id, null, 'Test');
        String rateCards1 = RateCardWidgetController.getAvailableRateCards(null, priceItem.Id, acc.id, 'Test');
        String rateCards3 = RateCardWidgetController.getAvailableRateCards(null, null, acc.id, 'Test');
        
        // test cloning
        String cloneResult = RateCardWidgetController.cloneRateCard(pclist[0].Id, priceItem.Id, null, 'Test', JSON.serialize(objList));
        String cloneResult2 = RateCardWidgetController.cloneRateCard(pclist[1].Id, priceItem.Id, null, 'Test1', JSON.serialize(objList));
        String cloneResult3 = RateCardWidgetController.cloneRateCard(pclist[1].Id, priceItem.Id, acc.id, 'Test1', JSON.serialize(objList));
        String cloneResult4 = RateCardWidgetController.cloneRateCardAsJSON(pclist[0].Id, JSON.serialize(objList));
        String cloneResult5 = RateCardWidgetController.cloneRateCardAsJSON(pclist[1].Id, JSON.serialize(objList));
        
        // test edit
        String editResult = RateCardWidgetController.editRateCard(pclist[2].Id, priceItem.Id, null, 'Test', JSON.serialize(objList));
        String editResult2 = RateCardWidgetController.editRateCard(pclist[0].Id, priceItem.Id, null, 'Test1', JSON.serialize(objList));
         String editResult3 = RateCardWidgetController.editRateCard(pclist[0].Id, priceItem.Id, acc.id, 'Test1', JSON.serialize(objList));
        
        // test delete 
        String deleteResult = RateCardWidgetController.deleteRateCard('Test', rc.Id);
        String deleteResult2 = RateCardWidgetController.deleteRateCard('Test1', rc.Id);
        
        
        
        Test.stopTest();        
    }
    static testmethod void testRateCardMethod2() {
        cspmb__Rate_Card__c rc = new cspmb__Rate_Card__c(
            Name = 'Test'
        );
        insert rc;
        system.assert(rc!=null);
        cspmb__Rate_Card_Line__c rcl = new cspmb__Rate_Card_Line__c(
            Name = 'Test',
            cspmb__Off_Peak__c = 10,
            cspmb__Rate_Card__c = rc.Id
        );
        insert rcl;
        
        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(
            Name = 'Test'
        );
        insert priceItem;
        system.assert(priceItem!=null);
        cspmb__Price_Item_Rate_Card_Association__c rcAssoc = new cspmb__Price_Item_Rate_Card_Association__c(
            cspmb__Price_Item__c = priceItem.Id,
            cspmb__Rate_Card__c = rc.Id
        );
        insert rcAssoc;
        system.assert(rcAssoc!=null);
        Rate_Card_Widget_Settings__c rcSettings = new Rate_Card_Widget_Settings__c(
            Name = 'Test1',
            Rate_Card_Line_Fields__c = 'cspmb__Rate_Card_Line_Unit__c,cspmb__Weekend__c,cspmb__Off_Peak__c,cspmb__Peak__c',
            Editable_Rate_Card_Line_Fields__c = 'cspmb__Rate_Card_Line_Unit__c,cspmb__Weekend__c,cspmb__Off_Peak__c,cspmb__Peak__c',
            Save_as_JSON__c = true
        );
        insert rcSettings;
        
        Rate_Card_Widget_Settings__c rcSettings1 = new Rate_Card_Widget_Settings__c(
            Name = 'Test',
            Save_as_JSON__c = true,            
            Rate_Card_Fields__c = 'cspmb__Is_Active__c',
            Rate_Card_Line_Fields__c = 'cspmb__Rate_Card_Line_Unit__c,cspmb__Weekend__c,cspmb__Off_Peak__c,cspmb__Peak__c',
            Editable_Rate_Card_Line_Fields__c = 'cspmb__Rate_Card_Line_Unit__c,cspmb__Weekend__c,cspmb__Off_Peak__c,cspmb__Peak__c'
        );
        insert rcSettings1;
         //Creating Account object records
        Account act = new Account(Name='Test112233M1',Customer_Type__c='MNC',Selling_Entity__c ='Telstra Incorporated',cscfga__Active__c='Yes',
                Activated__c= True,
                Account_ID__c = '33331',
                Account_Status__c = 'Active',
                Customer_Legal_Entity_Name__c = 'Test');
        insert act;
        cscfga__Configuration_Offer__c csfa = new cscfga__Configuration_Offer__c(name='Master  VPLS VLAN Service',cscfga__Active__c =true,cscfga__Template__c=false);
        insert csfa;
        List<Offer_Id__c> offIdlist = new List<Offer_Id__c>{
                 new Offer_Id__c(name ='Master_IPVPN_Service_Offer_Id',Offer_Id__c = csfa.id),
                 new Offer_Id__c(name ='Master_VPLS_Transparent_Offer_Id',Offer_Id__c = csfa.id),
                 new Offer_Id__c(name ='Master_VPLS_VLAN_Offer_Id',Offer_Id__c = csfa.id)
       };
       insert offIdlist;
       system.assert(offIdlist!=null);
        //Creating Opportunity object records
        Opportunity oppObj = new Opportunity(Name='GFTS Ph 2',AccountID=act.Id,Opportunity_Type__c='Simple',
                                           CurrencyIsoCode = 'USD', CloseDate = Date.today(),StageName='Identify & Define',Stage__c='Identify & Define',
                                            QuoteStatus__c ='Approved',Sales_Status__c= 'Won',Win_Loss_Reasons__c ='Product',
                                            Order_Type__c= 'New', ContractTerm__c = '24');
        insert oppObj; 
        
        oppObj.Quote_Simplification__c= true;
        //oppObj.R3_Products_Opportunity__c = true;
        oppObj.Product_Type__c ='Global Multi-Cloud';
        oppObj.Estimated_NRC__c = 10.00;
        oppObj.Estimated_MRC__c = 10.00;
        update oppObj;
        
        //insert contact object records
         Contact contObj = new Contact(AccountId=act.Id,LastName='tech',
                                  email='test@gmail.com');
        insert contObj; 
        //insert Product Basket object record
        cscfga__Product_Basket__c pbasket = new cscfga__Product_Basket__c();
        pbasket.csbb__Account__c = act.id;
        pbasket.csordtelcoa__Account__c = act.id;
        pbasket.cscfga__Opportunity__c = oppObj.id;
        insert pbasket;
        
        //insert product definition object record
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.name = 'Master IPVPN Service';
        pd.cscfga__Description__c ='112312331';
        insert pd;
        
        
        cscfga__Product_Definition__c pd1 = new cscfga__Product_Definition__c();
        pd1.name = 'Master VPLS Service';
        pd1.cscfga__Description__c ='Test master VPLS';
        insert pd1;
        Product_Definition_Id__c setting = new Product_Definition_Id__c();
        setting.Name = 'VLANGroup_Definition_Id';
        setting.Product_Id__c = pd.ID;
        insert setting;
        //insert custom setting records
        Product_Definition_Id__c pd6 = new Product_Definition_Id__c();
        pd6.name = 'IPVPN_Port_Definition_Id';
        pd6.Product_Id__c =pd1.ID;
        insert pd6;
        Product_Definition_Id__c pd7 = new Product_Definition_Id__c();
        pd7.name = 'SMA_Gateway_Definition_Id';
        pd7.Product_Id__c =pd.id;
        insert pd7;
        Product_Definition_Id__c pd8 = new Product_Definition_Id__c();
        pd8.name = 'VPLS_VLAN_Port_Definition_Id';
        pd8.Product_Id__c =pd1.id;
        insert pd8;
        Product_Definition_Id__c pd9 = new Product_Definition_Id__c();
        pd9.name = 'VPLS_Transparent_Definition_Id';
        pd9.Product_Id__c =pd.id;
        insert pd9;
        Product_Definition_Id__c pd2 = new Product_Definition_Id__c();
        pd2.name = 'Master_IPVPN_Service_Definition_Id';
        pd2.Product_Id__c =pd1.id;
        insert pd2;
        Product_Definition_Id__c pdk = new Product_Definition_Id__c();
        pdk.name = 'Master_VPLS_Service_Definition_Id';
        pdk.Product_Id__c =pd.id;
        insert pdk;
        
        //insert product configuration record for Master IPVPN Service
        List<cscfga__Product_Configuration__c> pclist= new List<cscfga__Product_Configuration__c>{
            new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd.id,cscfga__Product_Basket__c = pbasket.Id),
            new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd1.id,cscfga__Product_Basket__c = pbasket.Id,Solution_Type__c='Transparent Mode'),
            new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd1.id,cscfga__Product_Basket__c = pbasket.Id,Solution_Type__c='VLAN Mode')
           };
        insert pclist;
        system.assert(pclist!=null);
        Map<String, Object> rcMap = new Map<String, Object>();
        rcMap.put('Name', 'Test');
        rcMap.put('rateCardLines', JSON.serialize(rcl));
        
        List<Object> objList = new List<Object>{rcMap};
        
        Test.startTest();
        
        RateCardWidgetController.ParsedRateCardWrapper rcWrapper = new RateCardWidgetController.ParsedRateCardWrapper();
        rcWrapper.rateCardId = rc.Id;
        rcWrapper.rateCard = rc;
        rcWrapper.rateCardLines = new List<cspmb__Rate_Card_Line__c>{rcl};
        
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        account acc = accList[0];
        
        ObjectPrefixIdentifiers__c op=new ObjectPrefixIdentifiers__c();
        op.ObjectIdPrefix__c='a07';
        op.name='Contract';
        insert op;
        system.assert(op!=null);
        // test rc get method
        String rateCards = RateCardWidgetController.getAvailableRateCards(null, priceItem.Id, null, 'Test');
        String rateCards1 = RateCardWidgetController.getAvailableRateCards(null, priceItem.Id, acc.id, 'Test');
        String rateCards3 = RateCardWidgetController.getAvailableRateCards(null, null, acc.id, 'Test');
        
        // test cloning
        String cloneResult = RateCardWidgetController.cloneRateCard(pclist[0].Id, priceItem.Id, null, 'Test', JSON.serialize(objList));
        String cloneResult2 = RateCardWidgetController.cloneRateCard(pclist[1].Id, priceItem.Id, null, 'Test1', JSON.serialize(objList));
        String cloneResult3 = RateCardWidgetController.cloneRateCard(pclist[1].Id, priceItem.Id, acc.id, 'Test1', JSON.serialize(objList));
        String cloneResult4 = RateCardWidgetController.cloneRateCardAsJSON(pclist[0].Id, JSON.serialize(objList));
        String cloneResult5 = RateCardWidgetController.cloneRateCardAsJSON(pclist[1].Id, JSON.serialize(objList));
        
        // test edit
        String editResult = RateCardWidgetController.editRateCard(pclist[2].Id, priceItem.Id, null, 'Test', JSON.serialize(objList));
        String editResult2 = RateCardWidgetController.editRateCard(pclist[0].Id, priceItem.Id, null, 'Test1', JSON.serialize(objList));
         String editResult3 = RateCardWidgetController.editRateCard(pclist[0].Id, priceItem.Id, acc.id, 'Test1', JSON.serialize(objList));
        
        // test delete 
        String deleteResult = RateCardWidgetController.deleteRateCard('Test', rc.Id);
        String deleteResult2 = RateCardWidgetController.deleteRateCard('Test1', rc.Id);
        
        Test.stopTest();        
    }
}