public class ExportExcelApprovalRequestData {
   
    public Case caseObj;
    //for @Testvisible modified by Anuradha on 2/20/2017
    @Testvisible
    private list<case> casequery;
    public string header{get;set;}
    public List<wrapper> lstwrapper {get; set;}
    public String casenumber {get; set;}

    public class wrapper{
      public string id {get;set;}
      public string RateRc {get; set;}
      public string RateNrc{get; set;}
      public string OfferRc{get; set;}
      public string OfferNrc{get; set;}
      public string ApprovedRC{get; set;}
      public string ApprovedNRC{get; set;}
      public string Product{get;set;}
      public string ProductDescription{get;set;}
      public string CostRC{get; set;}
      public string CostNrc{get; set;}
      public string ContractTerm{get; set;}
      public string Margin{get; set;}
    }
   
    public string Filetype{get;set;}
    public boolean isExcel {get;set;}
    public boolean isCsv {get;set;}   
   
    public ExportExcelApprovalRequestData(ApexPages.StandardController controller){
            this.caseObj = (case)controller.getrecord();
            casequery = [select id, casenumber, Product_Basket__c from case where id=: caseobj.id];
            casenumber = casequery[0].casenumber;
            Filetype = '';
            lstwrapper = new List<wrapper>();
            header = 'Salesforce ID, Product Name,Product Information, Rate NRC, Rate MRC, Offer NRC,Offer MRC,Approved NRC,  Approved MRC, Cost NRC,  Cost MRC,  Contract Term,Margin';                     
    }
 
   
    public void exportToExcel(){
          string queryString = 'Select id, Product_Configuration__r.Name, Product_Configuration__r.Product_Definition_Name__c, Product_Configuration__r.Internet_Onnet_BillingType__c, Product_Configuration__r.Product_Code__c, Product_Information__c, Product_Configuration__r.Hierarchy__c, Rate_RC__c, Rate_NRC__c,Offer_RC__c,Offer_NRC__c,Approved_RC__c, Approved_NRC__c,Cost_RC__c,Cost_NRC__c,Contract_Term__c, Remaining_Contract_Term__c, Margin__c from Pricing_Approval_Request_Data__c where Product_Configuration__c != null AND Product_Basket__c = \''+casequery[0].product_basket__c+'\' order by Product_Configuration__r.Hierarchy__c'; 
          List<Pricing_Approval_Request_Data__c> lstpricingApprovalData = DataBase.Query(queryString);
          List<Pricing_Approval_Request_Data__c> pricingApprovalData = new List<Pricing_Approval_Request_Data__c>();
          system.debug('lstpricingApprovalData:'+lstpricingApprovalData.size());
          
          Boolean skip = false;
          
          if(lstpricingApprovalData.size()>0){
                for(Pricing_Approval_Request_Data__c pard :lstpricingApprovalData){
                    skip = false;                   
                    
                        if(pard.Product_Configuration__r.Product_Definition_Name__c == 'Master IPVPN Service'){
                            skip = true;
                        } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'ASBR'){
                            if(pard.Product_Configuration__r.Name.startsWith('Standalone ASBR')){
                                skip = false;
                            } else if (pard.Product_Configuration__r.Name.startsWith('ASBR')){
                                skip = true;
                            }
                        } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'ASBR (GIE)' && pard.Product_Configuration__r.Product_Code__c == 'ASBRB'){
                            skip = true;
                        } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'Master VPLS Service'){
                            skip = true;
                        } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'VLAN Group'){
                            skip = true;
                        } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'TWI Singlehome'){
                            skip = true;
                        } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'TWI Multihome'){
                            skip = true;
                        } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'IPT Singlehome'){
                            skip = true;
                        } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'Internet-Onnet' && (pard.Product_Configuration__r.Name == 'IPT' || pard.Product_Configuration__r.Name == 'TWI') && (pard.Product_Configuration__r.Internet_Onnet_BillingType__c != null && pard.Product_Configuration__r.Internet_Onnet_BillingType__c == 'Aggregated')){
                            skip = true;
                        } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'EVP Port'){
                            skip = true;
                        } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'GVOIP'){
                            skip = true;
                        } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'Customer Site'){
                            skip = true;
                        } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'IPC'){
                            skip = true;
                        } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'Colocation'){
                            skip = true;
                        } else if (pard.Product_Configuration__r.Product_Definition_Name__c == 'Local Access' &&
                          pard.Product_Information__c.contains('Local access organized by:Customer')) {
                          skip = true;
                        }
                     
                     if(!skip){
                         pricingApprovalData.add(pard);
                     }
                }
          }
          
          try{
          if(pricingApprovalData.size()>0){
              for(Pricing_Approval_Request_Data__c csd :pricingApprovalData){
                  wrapper w = new wrapper();
                  w.id = csd.id;
                  w.product = csd.Product_Configuration__r.Name.Replace(',',' ');
                  w.raterc= string.valueof(csd.rate_rc__c) ;
                  w.ratenrc = string.valueof(csd.rate_nrc__c);
                  w.offerrc = string.valueof(csd.offer_rc__c);
                  w.offernrc = string.valueof(csd.offer_nrc__c) ;
                  w.approvedrc = string.valueof(csd.approved_rc__c);
                  w.approvednrc = string.valueOf(csd.approved_nrc__c);
                  w.costrc = string.valueOf(csd.cost_rc__c);
                  w.costnrc = string.valueOf(csd.cost_nrc__c);

                  // Show Remaining Contract Term instead of Contract Term.
                  if (csd.Remaining_Contract_Term__c != null && csd.Product_Information__c.contains('Remaining Contract Term:')) {
                    w.contractterm = string.valueof(csd.Remaining_Contract_Term__c);
                  }
                  else if (csd.Contract_Term__c != null) {
                    w.contractterm = string.valueof(csd.contract_term__c);
                  }
                  w.margin = string.valueOf(csd.margin__c);
                  if(csd.Product_Information__c != null)
                  w.productdescription = csd.Product_Information__c.replace(',','+');
                 
                  lstwrapper.add(w);               
              }             
          }
          }catch(exception e){
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while exporting the pricing data'+e.getMessage());
                ApexPages.addMessage(errormsg);
          }
          system.debug('lstwrapper :'+lstwrapper.size());
    }
}