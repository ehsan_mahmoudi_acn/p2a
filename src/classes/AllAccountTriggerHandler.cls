/*As part of production Ticket# TGCTASK0209037, commented //with sharing */
//public with sharing class AllAccountTriggerHandler extends BaseTriggerHandler{
public class AllAccountTriggerHandler extends BaseTriggerHandler{
    final Integer NAME_MAX_LENGTH = 80;

    public override void beforeInsert(List<SObject> newAccounts){
        validatePostalCode((List<Account>)newAccounts);
        updateActivatedAccountFields((List<Account>)newAccounts, null);
        updateAccountManager((List<Account>)newAccounts, null);
    }

    public override void afterInsert(List<SObject> newAccounts, Map<Id, SObject> newAccountsMap) {
        createGlobalAccounts((List<Account>)newAccounts);
    }

    public override void beforeUpdate(List<SObject> newAccounts, Map<Id, SObject> newAccountsMap, Map<Id, SObject> oldAccountsMap){
        flagSentToTibco((List<Account>)newAccounts, (Map<Id, Account>)oldAccountsMap);
        validatePostalCode((List<Account>)newAccounts);
        manageNewLogo((List<Account>)newAccounts, (Map<Id, Account>)oldAccountsMap);
        updateActivatedAccountFields((List<Account>)newAccounts, (Map<Id, Account>)oldAccountsMap);
        updateAccountManager((List<Account>)newAccounts, (Map<Id, Account>)oldAccountsMap);
    }

    public override void afterUpdate(List<SObject> newAccounts, Map<Id, SObject> newAccountsMap, Map<Id, SObject> oldAccountsMap) {
        updateBillProfile((List<Account>)newAccounts, (Map<Id, Account>)oldAccountsMap);
        manageNewLogoWinBack((List<Account>)newAccounts, (Map<Id, Account>)newAccountsMap, (Map<Id, Account>)oldAccountsMap);
        updateGlobalAccounts((List<Account>)newAccounts, (Map<Id, Account>)newAccountsMap);
    }

    public override void beforeDelete(Map<Id, SObject> oldAccountsMap){   
        beforeGlobalAccountDelete((Map<Id, Account>)oldAccountsMap);
    }

    public override void afterUndelete(List<SObject> newAccounts, Map<Id, SObject> newAccountsMap){
        afterGlobalAccountUndelete((List<Account>)newAccounts);
    }

    /*
    *   Updates the Account Manager lookup field and name field with account owner.
    */
    private void updateAccountManager(List<Account> newAccounts, Map<Id, Account> oldAccountsMap){
        Set<Id> setOfOwnersIds = new Set<Id>();
        String callOnce = 'callOnce';
        for (Account acc : newAccounts) {
            setOfOwnersIds.add(acc.OwnerId);
        }
        if(CheckRecursive.canExecuteBlocking(callOnce)){
            Map<Id,User> userMap = new Map<Id,User>([Select Firstname, Lastname from User where Id In :setOfOwnersIds]);

            for (Account acc : newAccounts){
                if (oldAccountsMap == null || oldAccountsMap.get(acc.Id).Ownerid != acc.Ownerid){
                    acc.Accounts_Manager__c = acc.Ownerid;
                    
                    if(userMap.containsKey(acc.OwnerId)){
                        acc.Account_Manager__c = userMap.get(acc.OwnerId).Firstname + ' ' + userMap.get(acc.OwnerId).Lastname;
                    }  
                }
            }
        }
    }

    /*
    *   Update the fields of Activated Accounts
    */
    private void updateActivatedAccountFields(List<Account> newAccounts, Map<Id, Account> oldAccountsMap){
        for (Account a : newAccounts) {  
           if((oldAccountsMap == null && a.Activated__c == true) || (oldAccountsMap != null &&oldAccountsMap.containskey(a.Id)&&oldAccountsMap.get(a.Id).Activated__c != a.Activated__c && a.Activated__c == true)) { 
             // added condition to check for condition where oldaccountsmap is not null a.Activated_By__c = UserInfo.getUserId();
                a.Account_Verified__c = true;
                a.Account_Status__c = 'Active';
            }
        }
    }

    /*
    *   Create the global account
    */
    private void createGlobalAccounts(List<Account> newAccounts){
        List<Global_Account__c> globalAccountList = new List<Global_Account__c>();
        Id userId = System.Label.Data_Migration_User_ID; 

        for(Account acc : newAccounts){  
            Global_Account__c globalAccount = new Global_Account__c();
            globalAccount.account__c = acc.id;
            globalAccount.Name = acc.Name.abbreviate(NAME_MAX_LENGTH);
            globalAccount.OwnerId = userId;
            globalAccount.AccountOwner__c = acc.OwnerId;
            globalAccount.Country__c = acc.Country__c;
                
            globalAccountList.add(globalAccount);
        }
        if(globalAccountList.size() > 0){
            insert globalAccountList; 
        }
    }

    /*
    *   Update global account with account latest values
    */
    private void updateGlobalAccounts(List<Account> newAccounts, Map<Id, Account> newAccountsMap){
        String callOnce = 'updglblacc';
        if(CheckRecursive.canExecuteBlocking(callOnce)){
            List<Global_Account__c> updateGlobalAccListValues = new List<Global_Account__c>();
            List<Global_Account__c> updateGlobalAccList = [Select Id, Name, Account__c, Country__c, AccountOwner__c from Global_Account__c where Account__c in :newAccounts];
            
            for(Global_Account__c globalAccList: updateGlobalAccList){ 
                if(globalAccList.AccountOwner__c != newAccountsMap.get(globalAccList.Account__c).OwnerId || globalAccList.Name != newAccountsMap.get(globalAccList.Account__c).Name || globalAccList.Country__c != newAccountsMap.get(globalAccList.Account__c).Country__c){
                    globalAccList.Name = newAccountsMap.get(globalAccList.Account__c).Name.abbreviate(NAME_MAX_LENGTH);
                    globalAccList.Account__c= newAccountsMap.get(globalAccList.Account__c).id;
                    globalAccList.Country__c = newAccountsMap.get(globalAccList.Account__c).Country__c;
                    globalAccList.AccountOwner__c = newAccountsMap.get(globalAccList.Account__c).OwnerId;
                    updateGlobalAccListValues.add(globalAccList);
                }
            }
            if(updateGlobalAccListValues.size() > 0){
                update updateGlobalAccListValues;  
            }
        }
    }

    /*
    *   Update the Bill Profile when the Account Owner is changed.
    */
    private void updateBillProfile(List<Account> newAccounts, Map<Id, Account> oldAccountsMap){
        List<id> lst_AccIds = new List<id>();
         
        for(Account accObj : newAccounts){
            Account oldAccObj = oldAccountsMap.get(accObj.id);
            if(accObj.Ownerid != oldAccObj.Ownerid || accObj.Account_ID__c != oldAccObj.Account_ID__c || accObj.Accounts_Manager__c != oldAccObj.Accounts_Manager__c){
                lst_AccIds.add(accObj.id);
            }  
        }
        
        if(lst_AccIds.size() > 0){
            AccountSequenceController ObjAccSequence = new AccountSequenceController();
            ObjAccSequence.updateBillProfile(lst_AccIds);
        }
    }

    /*
    *   Update the Is Updated flag when Account changes are sent to TIBCO 
    */
	//for @testvisible, modified by Anuradha
	@testvisible
    private void flagSentToTibco(List<Account> newAccounts, Map<Id, Account> oldAccountsMap){
        for(Account acc : newAccounts){
            acc.Industry__c = acc.Industry;
            acc.countryval__c=acc.Country_Name__c;			
            if (acc.Sent_to_Tibco__c == true && oldAccountsMap.get(acc.id).Sent_to_Tibco__c == true && OpportunityUpdate.getcount() == 0){
                acc.Is_updated__c = true;
            }       
        }
    }
	//for @testvisible, modified by Anuradha
	@testvisible
    private void manageNewLogo(List<Account> newAccounts, Map<Id, Account> oldAccountsMap){
        if(Util.notAIForNewLogo){    
            for(Account accObj : newAccounts){
                if(accObj.New_Logo__c == false && accObj.Win_Back__c == true && oldAccountsMap.get(accObj.Id).OwnerId == accObj.OwnerId){
                    accObj.Created_By_For_New_Customer__c = null;
                    accObj.Completion_Date_For_New_Customer__c = null;
                }
                else if(accObj.Win_Back__c == false && accObj.New_Logo__c == true && oldAccountsMap.get(accObj.Id).OwnerId == accObj.OwnerId){
                    accObj.Created_By_For_Win_Back_Customer__c = null;
                    accObj.Completion_Date_For_WinBack_Customer__c = null;
           
                }
                else if (accObj.New_Logo__c == false  && accObj.Win_Back__c == false && oldAccountsMap.get(accObj.Id).OwnerId == accObj.OwnerId ){
                    accObj.Created_By_For_New_Customer__c = null;
                    accObj.Completion_Date_For_New_Customer__c = null;
                    accObj.Created_By_For_Win_Back_Customer__c = null;
                    accObj.Completion_Date_For_WinBack_Customer__c = null;
                }  
            }
        }
    }

    /*
    *   The NewLogoWinBackHandler call needs to be looked at as should not be tied to a single trigger context  
    */
	//for @testvisible, modified by Anuradha
	@testvisible
    private void manageNewLogoWinBack(List<Account> newAccounts, Map<Id, Account> newAccountsMap, Map<Id, Account> oldAccountsMap){
        NewLogoWinBackHandler newLogoWinBackHdlr = new NewLogoWinBackHandler();
        Map<String,NewLogo__c> newLogoCustMap = NewLogo__c.getAll();
        
        if(!newLogoCustMap.isEmpty()){
            for(Account accObj : newAccounts){
                if(accObj.Type != null && accObj.Account_Status__c != null && accObj.Account_Status__c == newLogoCustMap.get('Active').Action_Item__c && ((accObj.Type == newLogoCustMap.get('Logo_Prospect').Action_Item__c && accObj.Account_Type__c == newLogoCustMap.get('Logo_Prospect').Action_Item__c)
                   ||(accObj.Type == newLogoCustMap.get('Logo_Former_Customer').Action_Item__c && accObj.Account_Type__c == newLogoCustMap.get('Logo_Former_Customer').Action_Item__c) 
                   ||(accObj.Type == newLogoCustMap.get('Logo_Customer').Action_Item__c && accObj.Account_Type__c == newLogoCustMap.get('Logo_Customer').Action_Item__c))){
                    if(Util.newLogoFlag == true){    
                        newLogoWinBackHdlr.newLogoWinBackHdlrMthd(newAccounts, true, true, oldAccountsMap, newAccountsMap);
                    } 
                }
            }
        }
        else{
            System.debug(LoggingLevel.ERROR, 'NewLogo__c custom settings are not defined');
        } 
    }

    /*
    *   Validates the format of a postal code
    */
	//for @testvisible, modified by Anuradha
	@testvisible
    private void validatePostalCode(List<Account> newAccounts){ 
        for(Account acc : newAccounts){
            if(acc.Country_Code__c == 'US' && acc.Postal_Code__c != null){
               if(!(Pattern.matches('[0-9]{5} [0-9]{4}',acc.Postal_Code__c) || 
                    Pattern.matches('[0-9]{5}-[0-9]{4}',acc.Postal_Code__c) || 
                    Pattern.matches('[0-9]{5}[0-9]{4}',acc.Postal_Code__c))){
                    acc.Postal_Code__c.addError('The postal code entered for country United States of America is not in the appropriate format.  The Postal Code format should be XXXXXYYYY(E.g. 123456789) OR XXXXX-XXXX(E.g. 12345-6789) OR XXXXX XXXX(e.g. 12345 6789). If have filled in the appropriate data and you are still getting this error message then please contact support team'); 
               }
            }
            else if(acc.Country_Code__c == 'US' && (acc.Postal_Code__c==null || acc.Postal_Code__c=='')){
                acc.Postal_Code__c.addError('The postal code entered for country United States of America is not in the appropriate format.  The Postal Code format should be XXXXXYYYY(E.g. 123456789) OR XXXXX-XXXX(E.g. 12345-6789) OR XXXXX XXXX(e.g. 12345 6789). If have filled in the appropriate data and you are still getting this error message then please contact support team');
            } 
        }
    }

    /*
    *   Deleting Global account on before deletion of account
    */
	//for @testvisible, modified by Anuradha
	@testvisible
    private void beforeGlobalAccountDelete(Map<Id, Account> oldAccountsMap){
        String callOnce = 'delglblacc';
        if(CheckRecursive.canExecuteBlocking(callOnce)){
            List<Global_Account__c> deleteGlobalAccountList = new List<Global_Account__c>();
            //List<Global_Account__c> deleteGlobalAccList = [Select Id, Name, Account__c from Global_Account__c where Account__c in :oldAccountsMap.values()];
            
            for(Global_Account__c gAcc : [Select Id, Name, Account__c from Global_Account__c where Account__c in :oldAccountsMap.values()]){
                deleteGlobalAccountList.add(gAcc);
            }
            if(deleteGlobalAccountList.size()>0){
                delete deleteGlobalAccountList ;
            }
        }   
    }

    /*
    *   Undeleting global account once account is undeleted from recycle bin.
    */
	//for @testvisible, modified by Anuradha
	@testvisible
    private void afterGlobalAccountUndelete(List<Account> newAccounts){
        String callOnce = 'undelglblacc';
        if(CheckRecursive.canExecuteBlocking(callOnce)){
            List<Global_Account__c> unDeleteGlobalAccountList = new List<Global_Account__c>();
            //List<Global_Account__c> unDeleteGlobalAccList= [Select Id,Name,Account__c from Global_Account__c where Isdeleted =: true and account__c in :newAccounts ALL ROWS];

            for(Global_Account__c gAcc : [Select Id,Name,Account__c from Global_Account__c where Isdeleted =: true and account__c in :newAccounts ALL ROWS]){
                unDeleteGlobalAccountList.add(gAcc);
            }
            if(unDeleteGlobalAccountList.size() > 0){
                undelete unDeleteGlobalAccountList; 
            }
        }
    }
}