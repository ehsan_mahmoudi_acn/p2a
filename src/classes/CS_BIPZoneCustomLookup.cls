global with sharing class CS_BIPZoneCustomLookup extends cscfga.ALookupSearch {

    public override String getRequiredAttributes(){
        return '["BIPPostcode", "BIPESACodeName"]';
    }
    
    
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){  
            String Postcode = searchFields.get('BIPPostcode');
            String ESACodeName = searchFields.get('BIPESACodeName');
            
                        

            List<CS_BIP_PostCode_ESACode_Zone__c> returnData = [SELECT ID,CS_BIP_Zone__c
                                     FROM CS_BIP_PostCode_ESACode_Zone__c
                                     WHERE Name = :Postcode 
                                     AND CS_BIP_ESACode__c = :ESACodeName  ORDER BY CS_BIP_Zone__c DESC limit 1];
            
           
           return returnData;
    } 
}