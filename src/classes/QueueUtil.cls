/*As part of production Ticket# TGCTASK0209037, commented //with sharing */
//public with sharing class QueueUtil {
public class QueueUtil {
	private static Map<String, Map<String, QueueSobject>> objectQueuesMap = new Map<String, Map<String, QueueSobject>>();
	private static Map<Id, List<GroupMember>> queueMembersMap = new Map<Id, List<GroupMember>>();
	
	/*
	*	Retrieve queue based on it's name
	*/
	public static QueueSobject getQueueByName(Schema.SobjectType objectType, String name){
		return getQueuesMapForObject(objectType).get(name);
	}

	/*
	*	Retrieve queue Id based on it's name
	*/
	public static Id getQueueIdByName(Schema.SobjectType objectType, String name){
		if(getQueuesMapForObject(objectType).containsKey(name)){
			return getQueuesMapForObject(objectType).get(name).QueueId;
		}
		return null;
	}

	/*
	*	Retrieve the list of queues for a given object type
	*/
	public static List<QueueSobject> getQueuesListForObject(Schema.SobjectType objectType){
		return getQueuesMapForObject(objectType).values();
	}

	/*
	*	Retrieve the list of users which are monitoring the queue
	*/
	public List<GroupMember> getQueueUsersByName(Schema.SobjectType objectType, String name){
		Id queueId = getQueueIdByName(objectType, name);

		if(!queueMembersMap.containsKey(queueId)){
			List<GroupMember> members = [select UserOrGroupId From GroupMember where groupId = :queueId];
			queueMembersMap.put(queueId, members);
		}
		
		return queueMembersMap.get(queueId);
	}

	/*
	*	Retrieve the Map of queues for a given object type
	*/
	public static Map<String, QueueSobject> getQueuesMapForObject(Schema.SobjectType objectType){
		if(!objectQueuesMap.containsKey(objectType.getDescribe().getName())){
			List<QueueSobject> queues = [Select Id, QueueId, Queue.Name from QueueSobject where SobjectType= :objectType.getDescribe().getName()];
			Map<String, QueueSobject> queueMap = new Map<String, QueueSobject>();
			for(QueueSobject queue : queues){
				queueMap.put(queue.Queue.Name, queue);
			}
			objectQueuesMap.put(objectType.getDescribe().getName(), queueMap);
		}

		return objectQueuesMap.get(objectType.getDescribe().getName());
	}
}