/*As part of production Ticket# TGCTASK0209037, commented //with sharing */
//public with sharing class RecordTypeUtil {
public class RecordTypeUtil {
	private static Map<String, Map<String, Schema.RecordTypeInfo>> objectRecordTypesByName = new Map<String, Map<String, Schema.RecordTypeInfo>>();
	private static Map<String, Map<Id, Schema.RecordTypeInfo>> objectRecordTypesById = new Map<String, Map<Id, Schema.RecordTypeInfo>>();

	/*
	*	Retrieve the Id of a record type on a given object
	*/
	public static Id getRecordTypeIdByName(Schema.SObjectType objectType, String recordTypeName){
		Schema.RecordTypeInfo recordType = getRecordTypeByName(objectType, recordTypeName);
		if(recordType != null){
			return recordType.getRecordTypeId();
		}
		else{
			return null;
		}
    }

    /*
	*	Retrieve the Name of a record type on a given object
	*/
	public static String getRecordTypeNameById(Schema.SObjectType objectType, Id recordTypeId){
		Schema.RecordTypeInfo recordType = getRecordTypeById(objectType, recordTypeId);
		if(recordType != null){
			return recordType.getName();
		}
		else{
			return null;
		}
    }

    /*
	*	Retrieve the record type on a given object by name
	*/
	public static Schema.RecordTypeInfo getRecordTypeByName(Schema.SObjectType objectType, String recordTypeName){
		if(getObjectRecordTypesByName(objectType).containsKey(recordTypeName)){
			return getObjectRecordTypesByName(objectType).get(recordTypeName);
		}
		else{
			return null;
		}
    }

    /*
	*	Retrieve the record type on a given object by Id
	*/
	public static Schema.RecordTypeInfo getRecordTypeById(Schema.SObjectType objectType, Id recordTypeId){
		if(getObjectRecordTypesById(objectType).containsKey(recordTypeId)){
			return getObjectRecordTypesById(objectType).get(recordTypeId);
		}
		else{
			return null;
		}
    }

    /*
    *	Retrieve record types for the given object type.
    *	If they are not already loaded they will be added to the map otherwise returned
    */
    private static Map<String, Schema.RecordTypeInfo> getObjectRecordTypesByName(Schema.SObjectType objectType){
    	if(!objectRecordTypesByName.containsKey(objectType.getDescribe().getName())){
    		objectRecordTypesByName.put(objectType.getDescribe().getName(), objectType.getDescribe().getRecordTypeInfosByName());
    	}

    	return objectRecordTypesByName.get(objectType.getDescribe().getName());
    }

    /*
    *	Retrieve record types for the given object type.
    *	If they are not already loaded they will be added to the map otherwise returned
    */
    private static Map<Id, Schema.RecordTypeInfo> getObjectRecordTypesById(Schema.SObjectType objectType){
    	if(!objectRecordTypesById.containsKey(objectType.getDescribe().getName())){
    		objectRecordTypesById.put(objectType.getDescribe().getName(), objectType.getDescribe().getRecordTypeInfosById());
    	}

    	return objectRecordTypesById.get(objectType.getDescribe().getName());
    }
}