public class FieldDateValueGenerator implements FieldValueGenerator {

    private Integer offsetInDays;

    public FieldDateValueGenerator(Integer offsetInDays) {
        this.offsetInDays = offsetInDays;
    }

    public FieldDateValueGenerator() {
        this.offsetInDays = 0;
    }
    
    public Boolean canGenerateValueFor(Schema.DescribeFieldResult fieldDesc) {
        return Schema.DisplayType.DATE == fieldDesc.getType();
    }
    
    public Object generate(Schema.DescribeFieldResult fieldDesc) {
        Date result = Date.today();
        result.addDays(this.offsetInDays);
        return result;
    }
}