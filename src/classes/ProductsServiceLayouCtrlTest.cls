@isTest
public class ProductsServiceLayouCtrlTest {
    Private static List<Product_Definition_Id__c> PdIdlist1;
     Private static List<Offer_Id__c> offIdlist;
    
    public static testMethod void testController() {
      P2A_TestFactoryCls.disableAll(UserInfo.getUserId());  
      P2A_TestFactoryCls.sampleTestData();
            
      List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
      List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
      List<Country_Lookup__c> countrylist =  P2A_TestFactoryCls.getcountry(1);
      List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
      List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
      List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
      List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
      List<cscfga__Product_Configuration__c> proconfigs = P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
      List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
      List<csord__Subscription__c> SUBList =  P2A_TestFactoryCls.getSubscription(1,OrdReqList);
      List<csord__Service__c> serviceList =  P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
      system.assertEquals(true,serviceList!=null);
      string Productname = serviceList[0].Name;
      
       
      Test.startTest();
        ApexPages.PageReference myVfPage = Page.reassigntosales;
        Test.setCurrentPage(myVfPage);
        ApexPages.StandardController  sc = new ApexPages.StandardController(serviceList[0]);
        ProductsServiceLayoutController testCtrl = new ProductsServiceLayoutController(sc);
        testCtrl.save();
        system.assertEquals(true,testCtrl!=null); 
      Test.stopTest();    
            
    }
    
 }