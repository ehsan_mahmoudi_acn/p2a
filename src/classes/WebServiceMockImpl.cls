@isTest
global class WebServiceMockImpl implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
           requestName='www.sas.com';
        TibcoComSchemasXsdgenerationBill.BillingActivateResponse_element respElement= new TibcoComSchemasXsdgenerationBill.BillingActivateResponse_element();
       respElement.Ack_xc= 'Mock response';
       response.put('response_x', respElement); 
   }
}