global  class TriggerBillingMiscellaneous {
 @future (callout=true)
  
     webservice static void sendBillingdata(String serviceId){
    
       
     
        List<csord__Service_Line_Item__c> serviceLineItemQueried = new List<csord__Service_Line_Item__c>();
       // csord__Subscription__c subscription = [SELECT Id, Account_Manager__c, Bundle_Label__c, Parent_of_the_bundle_flag__c, Bundle_Action__c FROM csord__Subscription__c WHERE Id = :subscriptionId];
      
        
        csord__Service__c serviceItems = [SELECT id,Acity_Side__r.PostalCode__c,Zcity_Side__r.PostalCode__c,Site__r.City_Finder__r.Name,Site_B__r.City_Finder__r.Name,Site__r.Country_Finder__r.Country_Code__c,Site_B__r.Country_Finder__r.Country_Code__c,Product__r.Name,Product_Configuration_Type__c,OLI_Type__c,csordtelcoa__Service_Number__c,Order_Date__c,Billable_Flag__c,ResourceId__c, ROC_Line_Item_Status__c,Bundle_Flag__c,Parent_Bundle_Flag__c,Parent_ServiceID__c,Telstra_Billing_Entity__c, Product_Id__c, BillingProfileID__c, Root_Product_ID__c, Countries__c, Purchase_Order_Date__c, Contract_Sign_Date__c, Order_Type__c, 
                Primary_Service_ID__c,Bundle_Label_name__c,Bill_Activation_Flag__c,Generate_Credit_Flag__c,csordtelcoa__Parent_Product_Configuration__r.Name,Parent_Bundle_ID__c,Parent_Customer_PO__c,Bundle_Action__c,Account_Managers__c, Product_Code__c, Contract_Duration__c, Root_Bill_Text__c, Service_Bill_Text__c,Bandwidth__c,csord__Order__r.Is_InFlight_Cancel__c, Service_Type__c,Port_Type__c,Do_you_want_to_associate_a_UIA__c,csord__Order__r.Is_Terminate_Order__c,Stop_Billing_Date__c,Billing_Commencement_Date__c FROM csord__Service__c WHERE id = :serviceId];
      
        List<TibcoComSchemasXsdgenerationBill.Service_element> serviceElements = new List<TibcoComSchemasXsdgenerationBill.Service_element>();
        
        User u = [select id,firstname,EmployeeNumber from user where id=:userinfo.getuserid()]; 
        
        if(serviceId!=null)
        {
           serviceLineItemQueried = [select id,Approved_Rejected__c,Charge__c,Bill_Profile__r.In_Arrear_In_Advance__c,MISC_Cost__c,csord__Service__c,Country__c,U2C_Master_Code__c,Bill_Profile_Integration_Number__c,Pin_Service_ID__c,Bill_Profile__r.Align_with_Billing_Cycle__c,Bill_Profile__r.Pivot_Date__c,Zero_MISC_Charge_Flag__c,Cost_Centre__r.Cost_Centre_Id__c,Bill_Profile__r.Bill_Profile_Integration_Number__c,Bill_Profile__r.Country__c,Bill_Profile__r.Billing_Entity__c,Parent_Bundle_ID__c,Account_ID__r.Country__c,Billing_Entity__c,AEndCountryISOCode__c,Termination_Date__c,Bill_Profile__c,Root_Bill_Text__c,Parent_ServiceID__c,CurrencyISOCode,NRC_Bill_Text__c ,RC_Credit_Bill_Text__c,Bill_Text__c,Bill_Texts__c ,NRC_Credit_Bill_Text__c ,AEndZipPlus4__c,Align_with_Billing_Cycle__c,BEndCountryISOCode__c,BEndZipPlus4__c,Billing_Commencement_Date__c,Recurring_Credit_Amount__c,Charge_Amount__c,Charge_Amounts__c,Cost__c,Charge_IDs__c,Zero_Charge_Flag__c,Charging_Frequency__c,Charge_ID__c,NRC_Price__c,Cost_Center_Code__c,Credit_Approved_By__c,Credit_Duration__c,Display_line_item_on_Invoice__c,In_Advance_or_In_arrears__c,Installment_NRC_End_Date__c,Installment_NRC_Start_Date__c,Is_Miscellaneous_Credit_Flag__c,pivot_date__c,Path_Id__c,Site_A_For_ROC__c,Site_B_For_ROC__c,Stop_Billing_Date__c,Charge_Action__c,Type_of_Charge__c,Type_of_Miscellaneous_Credit__c,Zero_Charge_MRC_Flag__c,csord__Service__r.Product_Configuration_Type__c,csord__Service__r.Usage_Flag__c,Billing_MISC_Commencement_Date__c,Billing_End_Date__c,MISC_Charge_Amount__c,Frequency__c,Transaction_Status__c from csord__Service_Line_Item__c where csord__Service__c = :serviceId and Is_Miscellaneous_Credit_Flag__c = true and Transaction_Status__c=:'Approved'];
           
       }
        
       
        system.debug('serviceId is'+serviceId);//ritesh change
       
             List<TibcoComSchemasXsdgenerationBill.ServiceLineItem_element> serviceLineElements = new List<TibcoComSchemasXsdgenerationBill.ServiceLineItem_element >();
             TibcoComSchemasXsdgenerationBill.ServiceLineItem_element serviceLineElement=null;           
            for(csord__Service_Line_Item__c serviceLineItemElement : serviceLineItemQueried )
            {
                    
              serviceLineElement = new TibcoComSchemasXsdgenerationBill.ServiceLineItem_element ();
              serviceLineElement.ChargeValue = serviceLineItemElement.MISC_Charge_Amount__c;
              serviceLineElement.ChargeAction = 'CREATE';
              serviceLineElement.TypeofCharge = serviceLineItemElement.Type_of_Charge__c;
              serviceLineElement.BillText = serviceLineItemElement.Type_of_Charge__c=='RCR - Recurring credit'?serviceLineItemElement.RC_Credit_Bill_Text__c:serviceLineItemElement.Type_of_Charge__c=='OCR - One off credit'?serviceLineItemElement.NRC_Credit_Bill_Text__c:serviceLineItemElement.Bill_Texts__c;
              serviceLineElement.Cost = serviceLineItemElement.MISC_Cost__c;
              serviceLineElement.InstallmentNRCStartDate = String.valueof(serviceLineItemElement.Installment_NRC_Start_Date__c);
              serviceLineElement.InstallmentNRCEndDate = String.valueof(serviceLineItemElement.Installment_NRC_End_Date__c);
              serviceLineElement.TerminationDate =null;
              serviceLineElement.BillingCommencementDate = String.valueof(serviceLineItemElement.Billing_MISC_Commencement_Date__c);
              serviceLineElement.StartDate = String.valueof(serviceLineItemElement.Billing_MISC_Commencement_Date__c);
              serviceLineElement.EndDate = String.valueof(serviceLineItemElement.Billing_End_Date__c);
              serviceLineElement.CreditDuration = serviceLineItemElement.Type_of_Charge__c=='OCH - One off charge' || serviceLineItemElement.Type_of_Charge__c=='OCR - One off credit'?'1':String.valueof(serviceLineItemElement.Billing_MISC_Commencement_Date__c.daysBetween(serviceLineItemElement.Billing_End_Date__c));
              serviceLineElement.ChargeFrequency = serviceLineItemElement.Frequency__c;
              serviceLineElement.CurrencyISOCode = serviceLineItemElement.CurrencyISOCode;
              serviceLineElement.SiteA_SiteAcityname = serviceLineItemElement.Site_A_For_ROC__c;
              serviceLineElement.SiteB_SiteBCityname = serviceLineItemElement.Site_B_For_ROC__c;
              serviceLineElement.AEndZipPlus4 = serviceItems.Acity_Side__r.PostalCode__c;
              serviceLineElement.BEndZipPlus4 = serviceItems.Zcity_side__r.PostalCode__c;
              serviceLineElement.AEndCountryISOCode = serviceLineItemElement.AEndCountryISOCode__c;
              serviceLineElement.BEndCountryISOCode = serviceLineItemElement.BEndCountryISOCode__c;
              serviceLineElement.InAdvanceorInarrears = serviceLineItemElement.Bill_Profile__r.In_Arrear_In_Advance__c;
              serviceLineElement.AlignwithBillingcycle = String.valueof(serviceLineItemElement.Bill_Profile__r.Align_with_Billing_Cycle__c);
              serviceLineElement.PivotDate = serviceLineItemElement.Bill_Profile__r.Pivot_Date__c;
              serviceLineElement.DisplaylineitemonInvoice = 'true';
              serviceLineElement.CreditApprovedBy = serviceLineItemElement.Approved_Rejected__c;
              serviceLineElement.ChargeID = serviceLineItemElement.Charge__c;
              serviceLineElement.ZeroChargeFlag = serviceLineItemElement.Zero_MISC_Charge_Flag__c;
              serviceLineElement.IsMiscellaneousFlag = 'true';
              
              serviceLineElement.TypeofMiscellaneousCredit ='Service';
              serviceLineElement.CostCentreCode = serviceLineItemElement.Cost_Centre__r.Cost_Centre_Id__c;
              serviceLineElement.U2CMasterCode = serviceLineItemElement.U2C_Master_Code__c;
              serviceLineElement.UsageBaseFlag = false;
              //serviceLineElement.PinServiceID = serviceItems.Primary_Service_ID__c;
              serviceLineElement.PinBillText = serviceItems.Root_Bill_Text__c;
              serviceLineElement.Country = serviceLineItemElement.Bill_Profile__r.Country__c;
              serviceLineElement.BillProfileId = serviceLineItemElement.Bill_Profile__r.Bill_Profile_Integration_Number__c;
             // serviceLineElement.Parentofthebundleflag = serviceLineItemElement.Parent_Bundle_ID__c;
              serviceLineElement.BillingEntity = serviceLineItemElement.Bill_Profile__r.Billing_Entity__c;
              serviceLineElements.add(serviceLineElement);
            
              
             }
                        
            TibcoComSchemasXsdgenerationBill.Service_element serviceElement = new TibcoComSchemasXsdgenerationBill.Service_element();
            serviceElement.OrderType ='New Provide';
            
            serviceElement.AccountManager = serviceItems.Account_Managers__c;
           // serviceElement.BundleLabel = serviceItems.Bundle_Label_name__c;
            serviceElement.BundleLabel = '';
            serviceElement.BundleFlag = 'false';
           
            serviceElement.ParentBundleID ='';
           
            serviceElement.BundleAction = '';
            
            serviceElement.OLIType = serviceItems.OLI_Type__c;
          
            serviceElement.BillableFlag = 'true';
            
            system.debug('@@@@serviceElement.BillableFlag'+serviceElement.BillableFlag);
            serviceElement.ProductID = serviceItems.Product_Id__c;
            serviceElement.RootProductID = serviceItems.Root_Product_ID__c;
            serviceElement.OrderDate = String.valueof(serviceItems.Order_Date__c);
            serviceElement.PrimaryServiceID = serviceItems.Primary_Service_ID__c;
            serviceElement.ParentCustomerPO = serviceItems.Parent_Customer_PO__c;
            serviceElement.ProductCode = serviceItems.Product_Code__c;
            serviceElement.ContractStartDate=serviceItems.Billing_Commencement_Date__c; 
            serviceElement.ParentServiceID = serviceItems.Parent_ServiceID__c;
            serviceElement.ContractDuration = String.valueof(serviceItems.Contract_Duration__c);
            serviceElement.RootProductBillText = serviceItems.Root_Bill_Text__c;
            serviceElement.ServiceBillText = serviceItems.Service_Bill_Text__c;
            serviceElement.ProductName = serviceItems.Product_Code__c;
            serviceElement.BillActivationFlag = 'true';
            serviceElement.GenerateCreditFlag = 'true';
            serviceElement.ResourceId = serviceItems.ResourceId__c;
            serviceElement.OliId = serviceItems.id;
            serviceElement.UserName = u.EmployeeNumber;
            serviceElement.ServiceLineItem = serviceLineElements;
            serviceElements.add(serviceElement);
             
            
       // }
        system.debug('@@@@@serviceElements=outsideloop'+serviceElements);
              
        TibcoComSchemasXsdgenerationBill.Subscription_element request= new TibcoComSchemasXsdgenerationBill.Subscription_element();
       // request.AccountManager=subscription.Account_Manager__c;
       // request.BundleLabel=subscription.Bundle_Label__c;
        request.Service = serviceElements;
       
        system.debug('@@@@@request'+request);
        TibcoComBillingactivate.BillingActivatePortTypeEndpoint1 billingActivate = new TibcoComBillingactivate.BillingActivatePortTypeEndpoint1();
        billingActivate.timeout_x =40000;
        TibcoComSchemasXsdgenerationBill.BillingActivateResponse_element respone= billingActivate.BillingActivateOperation(serviceElements);

}
}