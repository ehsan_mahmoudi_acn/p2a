@isTest
private class FieldDateTimeValueGeneratorTest{
    
    static testMethod void unittest() {
        
        CurrencyValue__c cv = new CurrencyValue__c();
        cv.CreatedDate = Datetime.now();
        cv.name = 'Currency';
        cv.Currency_Value__c = 45;
        insert cv;
        System.assertEquals(45,cv.Currency_Value__c );
        
        Schema.DescribeFieldResult SdFRcuurency = CurrencyValue__c.Currency_Value__c.getDescribe();
        
        Test.startTest();
        FieldDateTimeValueGenerator fdtgcon = new FieldDateTimeValueGenerator(2);
        FieldDateTimeValueGenerator  fcvg = new FieldDateTimeValueGenerator();  
        Boolean  b = fcvg.canGenerateValueFor(SdFRcuurency);
        object obj = fcvg.generate(SdFRcuurency);
        Test.stopTest();
    
    }
}