@isTest (seealldata = false)

public class CS_RateCardLookup_newTest {

      /*  private static List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        private static List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        private static List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        private static List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        private static List<cscfga__Configuration_Screen__c> configLst = P2A_TestFactoryCls.getConfigScreen(1, ProductDeflist);
        private static List<cscfga__Screen_Section__c> ScreenSec = P2A_TestFactoryCls.getScreenSec(1, configLst);
        private static List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        private static List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        private static List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
        private static List<cscfga__Attribute_Definition__c> Attributedeflist = P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist, configLst, ScreenSec);
        private static list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);*/
        private static List<Account> accountList;
        private static List<CS_Country__c> countryList;
        private static List<CS_City__c> cityList;
        private static List<CS_POP__c> popList;
        private static List<CS_Route_Segment__c> routeSegmentList;
        private static Map<String, String> searchFields  = new Map<String, String>();
        private static List <String> ProductTypeNames = new list<string>();
        private static List <String> Resiliencefilter;
        private static Id[] excludeIds = new List<Id>();
        private static Integer pageOffset;
        private static Integer pageLimit;
        private static List<CS_Resilience__c> resilienceList;
        private static List<CS_Bandwidth__c> bandwidthList;
        private static List<cspmb__Price_Item__c> priceItemList;
        //private static List<Network_Technology__c> networkType;
        private static List<CS_Bandwidth_Product_Type__c> bandwidthProdTypeList;        
        private static List<CS_Route_Bandwidth_Product_Type_Join__c> routeBandwidthProductTypeJoinList;
        private static List<CS_PTP_Rate_Card_Dummy__c>rateCardDummyList;

        
        private static void initTestData(){
        
         
        
         
        
         accountList = new List<Account>{
            new Account(Name = 'Account 1', Customer_Type_New__c = 'MNC'),
            new Account(Name = 'Account 2', Customer_Type_New__c = 'MNC')
        };
        insert accountList;
        list<Account> CCC = [select id,Name from Account where Name = 'Account 1'];
                                system.assertEquals(accountList[0].name , CCC[0].name);
                                System.assert(accountList!=null); 

        countryList = new List<CS_Country__c>{
            new CS_Country__c(Name = 'India'),
            new CS_Country__c(Name = 'Pakistan')            
        };  
        insert countryList;
        list<CS_Country__c> CBB = [select id,Name from CS_Country__c where Name = 'India'];
                                system.assertEquals(countryList[0].name , CBB[0].name);
                                System.assert(countryList!=null);
        
        cityList = new List<CS_City__c>{
            new CS_City__c(Name = 'Delhi', CS_Country__c = countryList[0].id),
            new CS_City__c(Name = 'Lahor', CS_Country__c = countryList[1].id)           
        };  
        insert cityList;
        list<CS_City__c> CDD = [select id,Name from CS_City__c where Name = 'Delhi'];
                                system.assertEquals(cityList[0].name , CDD[0].name);
                                System.assert(cityList!=null);
        
        popList = new List<CS_POP__c>{
            new CS_POP__c(Name = 'POP 1', CS_Country__c = countryList[0].Id),
            new CS_POP__c(Name = 'POP 2', CS_Country__c = countryList[1].Id)
            
        };        
        insert popList;   
        list<CS_POP__c> CEE = [select id,Name from CS_POP__c where Name = 'POP 1'];
                                system.assertEquals(popList[0].name , CEE[0].name);
                                System.assert(popList!=null);             
        
        resilienceList = new List<CS_Resilience__c>{
            new CS_Resilience__c(Name = 'Protected'),
            new CS_Resilience__c(Name = 'Unprotected')            
        };        
        insert resilienceList; 
        list<CS_Resilience__c> CFF = [select id,Name from CS_Resilience__c where Name = 'Protected'];
                                system.assertEquals(resilienceList[0].name , CFF[0].name);
                                System.assert(resilienceList!=null);     
            
        
        routeSegmentList = new List<CS_Route_Segment__c>{
            new CS_Route_Segment__c(Name = 'Route Segment 1', Product_Type__c = 'IPL',  POP_A__c = popList[0].Id, POP_Z__c = popList[1].Id, CS_Resilience__c = resilienceList[0].Id, A_End_City__c = cityList[0].id, Z_End_City__c = cityList[1].id),
            new CS_Route_Segment__c(Name = 'Route Segment 2', Product_Type__c = 'EPL',  POP_A__c = popList[1].Id, POP_Z__c = popList[0].Id, CS_Resilience__c = resilienceList[1].Id, A_End_City__c = cityList[1].id, Z_End_City__c = cityList[0].id),
            new CS_Route_Segment__c(Name = 'Route Segment 3', Product_Type__c = 'EPLX',  POP_A__c = popList[0].Id, POP_Z__c = popList[1].Id, CS_Resilience__c = resilienceList[0].Id, A_End_City__c = cityList[0].id, Z_End_City__c = cityList[1].id),
            new CS_Route_Segment__c(Name = 'Route Segment 4', Product_Type__c = 'ICBS',  POP_A__c = popList[1].Id, POP_Z__c = popList[0].Id, CS_Resilience__c = resilienceList[1].Id, A_End_City__c = cityList[1].id, Z_End_City__c = cityList[0].id),
            new CS_Route_Segment__c(Name = 'Route Segment 5', Product_Type__c = 'EVPL', POP_A__c = popList[0].Id, POP_Z__c = popList[1].Id, CS_Resilience__c = resilienceList[0].Id, A_End_City__c = cityList[0].id, Z_End_City__c = cityList[1].id)
            //new CS_Route_Segment__c(Name = 'Route Segment 6', Product_Type__c = 'EPLX', POP_A__c = popList[1].Id, POP_Z__c = popList[0].Id, CS_Resilience__c = resilienceList[1].Id, A_End_City__c = cityList[1].id, Z_End_City__c = cityList[0].id),            
            //new CS_Route_Segment__c(Name = 'Route Segment 7', Product_Type__c = 'ICBS', POP_A__c = popList[0].Id, POP_Z__c = popList[1].Id, CS_Resilience__c = resilienceList[0].Id, A_End_City__c = cityList[0].id, Z_End_City__c = cityList[1].id),
            //new CS_Route_Segment__c(Name = 'Route Segment 8', Product_Type__c = 'ICBS', POP_A__c = popList[1].Id, POP_Z__c = popList[0].Id, CS_Resilience__c = resilienceList[1].Id, A_End_City__c = cityList[1].id, Z_End_City__c = cityList[0].id),            
            //new CS_Route_Segment__c(Name = 'Route Segment 9', Product_Type__c = 'EVPL', POP_A__c = popList[0].Id, POP_Z__c = popList[1].Id, CS_Resilience__c = resilienceList[0].Id, A_End_City__c = cityList[0].id, Z_End_City__c = cityList[1].id),
            //new CS_Route_Segment__c(Name = 'Route Segment 10',Product_Type__c = 'EVPL', POP_A__c = popList[1].Id, POP_Z__c = popList[0].Id, CS_Resilience__c = resilienceList[1].Id, A_End_City__c = cityList[1].id, Z_End_City__c = cityList[0].id)          
        };        
        insert routeSegmentList;    
        list<CS_Route_Segment__c> CGG = [select id,Name from CS_Route_Segment__c where Name = 'Route Segment 1'];
                                system.assertEquals(routeSegmentList[0].name , CGG[0].name);
                                System.assert(routeSegmentList!=null);
        
        
        bandwidthList = new List<CS_Bandwidth__c>{
            new CS_Bandwidth__c(Name = '16', Bandwidth_Value_MB__c = 16 ),
            new CS_Bandwidth__c(Name = '32', Bandwidth_Value_MB__c = 32),
            new CS_Bandwidth__c(Name = '64', Bandwidth_Value_MB__c = 64),
            new CS_Bandwidth__c(Name = '128', Bandwidth_Value_MB__c = 128),
            new CS_Bandwidth__c(Name = '256', Bandwidth_Value_MB__c = 256)
        };        
        insert bandwidthList;
         list<CS_Bandwidth__c> CHH = [select id,Name from CS_Bandwidth__c where Name = '16'];
                                system.assertEquals(bandwidthList[0].name , CHH[0].name);
                                System.assert(bandwidthList!=null);
        
        bandwidthProdTypeList = new List<CS_Bandwidth_Product_Type__c>{
            new CS_Bandwidth_Product_Type__c(Name = 'Bandwidth Product Type 1', CS_Bandwidth__c = bandwidthList[0].Id, Bandwidth_Group__c='Test1,Test1', Product_Type__c = 'IPL' ),
            new CS_Bandwidth_Product_Type__c(Name = 'Bandwidth Product Type 2', CS_Bandwidth__c = bandwidthList[1].Id, Bandwidth_Group__c='Test2,Test1', Product_Type__c = 'EPL' ),
            new CS_Bandwidth_Product_Type__c(Name = 'Bandwidth Product Type 3', CS_Bandwidth__c = bandwidthList[2].Id, Bandwidth_Group__c='Test3,Test1', Product_Type__c = 'EPLX' ),
            new CS_Bandwidth_Product_Type__c(Name = 'Bandwidth Product Type 4', CS_Bandwidth__c = bandwidthList[3].Id, Bandwidth_Group__c='Test4,Test1', Product_Type__c = 'ICBS' ),
            new CS_Bandwidth_Product_Type__c(Name = 'Bandwidth Product Type 5', CS_Bandwidth__c = bandwidthList[4].Id, Bandwidth_Group__c='Test5,Test1', Product_Type__c = 'EVPL' )
        };        
        insert bandwidthProdTypeList;
        list<CS_Bandwidth_Product_Type__c> CII = [select id,Name from CS_Bandwidth_Product_Type__c where Name = 'Bandwidth Product Type 1'];
                                system.assertEquals(bandwidthProdTypeList[0].name , CII[0].name);
                                System.assert(bandwidthProdTypeList!=null);
        
        routeBandwidthProductTypeJoinList = new List<CS_Route_Bandwidth_Product_Type_Join__c>{
            new CS_Route_Bandwidth_Product_Type_Join__c(Name = 'Product Type Join 1', CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[0].Id, CS_Route_Segment__c = routeSegmentList[0].Id),
            new CS_Route_Bandwidth_Product_Type_Join__c(Name = 'Product Type Join 2', CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[1].Id, CS_Route_Segment__c = routeSegmentList[1].Id),
            new CS_Route_Bandwidth_Product_Type_Join__c(Name = 'Product Type Join 3', CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[2].Id, CS_Route_Segment__c = routeSegmentList[2].Id),
            new CS_Route_Bandwidth_Product_Type_Join__c(Name = 'Product Type Join 4', CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[3].Id, CS_Route_Segment__c = routeSegmentList[3].Id),
            new CS_Route_Bandwidth_Product_Type_Join__c(Name = 'Product Type Join 5', CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[4].Id, CS_Route_Segment__c = routeSegmentList[4].Id)
        };
        insert routeBandwidthProductTypeJoinList;    
        list<CS_Route_Bandwidth_Product_Type_Join__c> CJJ = [select id,Name from CS_Route_Bandwidth_Product_Type_Join__c where Name = 'Product Type Join 1'];
                                system.assertEquals(routeBandwidthProductTypeJoinList[0].name , CJJ[0].name);
                                System.assert(routeBandwidthProductTypeJoinList!=null);
        
        
         priceItemList = new List<cspmb__Price_Item__c>{
            new cspmb__Price_Item__c(Name = 'Price Item 1', CS_Route_Bandwidth_Product_Type_Join__c=CJJ[0].id, Country__c = countryList[0].Id, City__c = cityList[0].Id, CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[0].Id, cspmb__Account__c = accountList[0].Id, cspmb__Is_Active__c = true, IPVPN_Type__c = 'Standard', Connectivity__c = 'Onnet', Pricing_Segment__c = 'MNC'),
            new cspmb__Price_Item__c(Name = 'Price Item 2', CS_Route_Bandwidth_Product_Type_Join__c=CJJ[0].id, Country__c = countryList[0].Id, City__c = cityList[0].Id, CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[0].Id, cspmb__Account__c = null, cspmb__Is_Active__c = true, IPVPN_Type__c = 'Standard', Connectivity__c = 'Onnet', Pricing_Segment__c = 'MNC')
        };
        insert priceItemList;
         list<cspmb__Price_Item__c> CKK = [select id,Name from cspmb__Price_Item__c where Name = 'Price Item 1'];
                                system.assertEquals(priceItemList[0].name , CKK[0].name);
                                System.assert(priceItemList!=null);
        
        rateCardDummyList = new list<CS_PTP_Rate_Card_Dummy__c  >();
        CS_PTP_Rate_Card_Dummy__c  rcd = new CS_PTP_Rate_Card_Dummy__c(); 
        rcd.Product__c = 'EPL';
        rcd.MRC__c = 456;
        rcd.CS_A_Country__c = countryList[0].id;
        //rcd.CS_Cable_Path__c = cablePathList[0].id;
        rcd.CS_Route_Segment__c = routeSegmentList[0].id;
        rcd.CS_Z_Country__c = countryList[0].id;
        rcd.CS_Z_POP__c = popList[0].id;
        rcd.Price_Item__c = priceItemList[0].id;
        rcd.A_End_Country_Name__c = 'Hong kong' ;
        rcd.Bandwidth_Name__c = 'Bandwidth';
        rcd.Cable_Path_Name__c = 'Cable Systems';
        rcd.Circuit_Type__c = 'Circuit Type';
        rcd.Network_Technology__c = 'Technology';
        rcd.Z_End_Country_Name__c = 'Austrila' ;
        rcd.A_POP_Name__c = 'PopUp-A';
        rcd.Z_POP_Name__c = 'PopUp-Z';
        rcd.NRC__c = 526;
        rcd.Partner_Name__c = 'Partner';
        rcd.NNI_Service_ID__c = 'Service';
        rcd.CS_Resilience__c = resilienceList[0].id;
        rcd.Resilience_Name__c = 'Resilience';
        rcd.NNI_Location1__c = 'Location';
        rateCardDummyList.add(rcd);
        insert  rateCardDummyList;    
       System.assert(rateCardDummyList!=null);
                               
        
        }
        
        
        public static testMethod void cSRateCardLookupnewTestMethod(){  
        
        P2A_TestFactoryCls.sampletestdata();
        
         List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
         List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
         List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
         List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
         List<cscfga__Configuration_Screen__c> configLst = P2A_TestFactoryCls.getConfigScreen(1, ProductDeflist);
         List<cscfga__Screen_Section__c> ScreenSec = P2A_TestFactoryCls.getScreenSec(1, configLst);
        List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
         List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
        List<cscfga__Attribute_Definition__c> Attributedeflist = P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist, configLst, ScreenSec);
         list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
         system.assertEquals(true,AttributesList!=null);
        
        initTestData();  
              
        
        CS_RateCardLookup_new cs = new CS_RateCardLookup_new();
        String requiredAtts = cs.getRequiredAttributes();        
        
        searchFields.put('EVPL Transparent','Yes');
        searchFields.put('EVPL VLAN','Yes');        
        searchFields.put('IPL','Yes');
        searchFields.put('EPL','Yes');
        searchFields.put('EPLX','Yes');
        searchFields.put('ICBS','Yes'); 
        searchFields.put('EVPL','Yes');        
        searchFields.put('ICBS Transparent','Yes');
        searchFields.put('CountForButtons','1');
        searchFields.put('start','1');
        searchFields.put('last','1');
        
        cs.doLookupSearch(searchFields,ProductDeflist[0].id, excludeIds, pageOffset,pageLimit );
        CS_RateCardLookup_new.updateAttribute(AttributesList[0].Name ,AttributesList[0].cscfga__value__c,AttributesList );  

        searchFields.put('Show Protected Routes','Yes');
        searchFields.put('Show Unprotected Routes','Yes');
        searchFields.put('Show Restored Routes','Yes');
        searchFields.put('Show Always On Routes','Yes');
        searchFields.put('A-End Country',countryList[0].id);        
        searchFields.put('Z-End Country',countryList[1].id);      
        searchFields.put('A End City',cityList[0].id);
        searchFields.put('Z End City', cityList[1].id);
        searchFields.put('Bandwidth', bandwidthList[0].id);        
                        
        test.starttest();
        
        Object[] data=cs.doLookupSearch(searchFields,ProductDeflist[0].id, excludeIds, pageOffset,pageLimit );
        CS_RateCardLookup_new.updateAttribute(AttributesList[0].Name ,AttributesList[0].cscfga__value__c,AttributesList );
        
        searchFields.put('A-End POPCLS 1',popList[0].id);
        searchFields.put('Z-End POPCLS 1',popList[1].id);
        
        data=cs.doLookupSearch(searchFields,ProductDeflist[0].id, excludeIds, pageOffset,pageLimit );
        CS_RateCardLookup_new.updateAttribute(AttributesList[0].Name ,AttributesList[0].cscfga__value__c,AttributesList );
        
        data.clear();
        
        searchFields.put('Show Protected Routes','Yes');
        searchFields.put('Show Unprotected Routes','Yes');
        searchFields.put('Show Restored Routes','Yes');
        searchFields.put('Show Always On Routes','Yes');
        searchFields.put('A-End Country',countryList[0].id);        
        searchFields.put('Z-End Country',countryList[1].id);      
        searchFields.put('A End City',cityList[0].id);
        searchFields.put('Z End City', cityList[1].id);
        searchFields.put('Bandwidth', bandwidthList[0].id); 
         
        cs.doLookupSearch(searchFields,ProductDeflist[0].id, excludeIds, pageOffset,pageLimit );
        
system.assertEquals(true,cs!=null);

        test.stoptest();
        
    }
    
}