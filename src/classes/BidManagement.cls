global with sharing class  BidManagement{
    webservice static void bidManagementCase(List<Opportunity> optylist){
        Set<ID> optyIDs = new Set<id>();
        for(Opportunity opp : optylist){
            optyIDs.add(opp.id);
        }
        
        List<Opportunity> optyFeasibleSync = new List<Opportunity>();
        List<Case> newFeasiblityCase = new List<Case>();
        List<cscfga__Product_Basket__c> syncOppotyPB = new list<cscfga__Product_Basket__c>();
        List<Case> parentCaseLst = new List<Case>();
        List<Case> childcase = new List<Case>();
        List<Case> nosync = new List<Case>();
        Map<Id,Id> pctoParentCaseId = new Map<Id, Id>();
        System.debug('optyidsssss-------' + optyIDs );
        syncOppotyPB =[select Id ,cscfga__Products_In_Basket__c,cscfga__Opportunity__c from cscfga__Product_Basket__c where cscfga__Opportunity__c in :OptyIDs and csordtelcoa__Synchronised_with_Opportunity__c =true and csbb__Synchronised_With_Opportunity__c =true];

        QueueSobject bidManagementQueue = QueueUtil.getQueueByName(Case.SObjectType, 'Bid Management Queue');        

        List<Case> OpenFeasibilityCaseList = [select id,status from Case where Opportunity_Name__c =:Optylist[0].Id and status NOT IN ('Complete','Cancelled') and Is_BidManagement_request__c=True];
        for(opportunity pb: Optylist){
            if(OpenFeasibilityCaseList.size()==0){
                Case nonsynccase= new Case();  
                nonsynccase.Origin = 'Web';  
                nonsynccase.Status = 'Under Review';
                nonsynccase.Origin = 'Web';
                nonsynccase.Subject ='Bid Management Request'; 
                nonsynccase.Opportunity_Name__c=pb.id;
                nonsynccase.Opportunity_Owner__c =  pb.OwnerID; 
                nonsynccase.recordtypeID = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Bid Management Type');  
                nonsynccase.accountId = pb.accountId; 
               
                if(SyncOppotyPB.size()>0){
                    nonsynccase.Product_Basket__c=SyncOppotyPB[0].id ;
                }
              
                nonsynccase.ownerid = bidManagementQueue.QueueId; 
                nonsynccase.Is_BidManagement_request__c=true;
                nonsynccase.Order_Type__c=Optylist[0].Order_Type__c;
                nonsynccase.Contract_Term__c=Optylist[0].ContractTerm__c;
                nonsynccase.TCV__c=Optylist[0].Estimated_TCV_2__c;
                nonsynccase.Region__c =Optylist[0].account.Region__c;
                nonsynccase.Opportunity_Type__c=Optylist[0].Opportunity_Type__c;
                System.debug('===='+Optylist[0].Opportunity_Type__c);
                Nosync.add(nonsynccase);
            }
            
            //insert Nosync; 
        }
        insert Nosync;
        if(Nosync.size() > 0){ 
            List<String> toAddresses = new List<string>();
     
            //List<GroupMember> gm = [select UserOrGroupId From GroupMember where GroupId = :bidManagementQueue.QueueId ];
            List<Id> uid = new List<Id>();
            for(GroupMember g : [select UserOrGroupId From GroupMember where GroupId = :bidManagementQueue.QueueId ]) {
                uid.add(g.UserOrGroupId);   
            }
            
            //List<User> uemail = [select email from user where id =: uid];
            List<String> email = new List<string>();
            String emailadress;
            
            for( user u : [select email from user where id =: uid]) {
                email.add(u.email);  
                emailadress =u.email+':';
            }
            
            for(string a : email){
                toAddresses.add(a);   
            }        
                  
            List<user> usernameinfo = [select Firstname, Lastname from User where id = :optylist[0].ownerId];
            if(email.size() > 0){
                for(Opportunity oppty : optylist){             
                    String instanceURLofopty = URL.getSalesforceBaseUrl().toExternalForm() + '/'+oppty.id ;
                    String instanceuRlaccount =URL.getSalesforceBaseUrl().toExternalForm() + '/'+oppty.accountid;
                    String optyName = oppty.name;
                    String ownerid = oppty.ownerid;
                    String usernameemail;
                    
                    if(usernameinfo .size() > 0){
                        if(usernameinfo[0].firstname != null){
                            usernameemail = usernameinfo[0].firstname +''+usernameinfo[0].lastname;
                        }
                    else
                        usernameemail = usernameinfo[0].lastname;
                    }
                    
                    String body ='Hi '+'Team,'+'\n' +'An new Bid Mangement request has been requested for opportunity  ' + optyName + '\n\n';
                    body += 'Please visit the opportunity and take necessary actions for the Bid Management request assigned to you.' +'\n\n';
                    body += 'URL for the opportunity : '+ instanceURLofopty+'\n';                
                    body += 'URL for the account: ' + InstanceuRlaccount +'\n\n';
                    body += 'Thanks,'+'\n';
                    body += usernameemail ;

                    EmailUtil.EmailData mailData = new EmailUtil.EmailData();
                    mailData.addToAddresses(toAddresses);
                    mailData.message.setSubject('Bid Management');
                    mailData.message.setPlainTextBody(body);
                    mailData.message.setSaveAsActivity(false);

                    EmailUtil.sendEmail(mailData);
                }
            }           
        }
    }
}