@isTest
/**
    @author - Accenture
    @date - 23-April-2012
    @version - 1.0
    @description - This class is to test the GenericEditContactSiteControl.
*/
private class TestGenericEditContactSiteControl {
/**
    @author - Accenture
    @date - 23-April-2012
    @version - 1.0
    @description - This method passes the Sites&Contacts Id to the GenericEditContactSiteControl through controller.
*/
    static Account acc1;
    static testMethod void  testSiteContact() {
        //Getting Sites&Contacts 
         Sites_Contacts__c form = getSiteContact(); 
         insert form;
         system.debug('form.Id');
         //Passing the Sites&Contacts URL to GenericEditContactSiteControl
         system.currentPageReference().getParameters().put('retUrl', '/'+form.Id);
         ApexPages.StandardController sc1 = new ApexPages.StandardController(form);         
         GenericEditContactSiteControl ge = new GenericEditContactSiteControl(sc1);         
         System.assertEquals(ge.saveSiteContact().getUrl(),'/'+form.id);
         }   
         // Creating Test Data
         private static Sites_contacts__c getSiteContact(){           
                
            Sites_Contacts__c sc = new Sites_Contacts__c();    
            Site__c site = getSite();
            Contact cont = getContact();
            sc.Contact__c = cont.Id;
            sc.Site__c = site.Id;        
            return sc;    
                  
         } 
         private static Site__c getSite(){
                    
            Site__c s1 = new Site__c();
            s1.Name = 'TestSite';
            s1.Address1__c ='door no.10';
            s1.Address2__c='bangalore,Testdata';
            Country_Lookup__c c2 = getCountry();
            s1.Country_Finder__c = c2.Id;
            S1.AccountId__c = getAccount().id;
            City_Lookup__c city = getCity();
            s1.City_Finder__c = city.Id;
            insert s1;
            return s1;                  
         }            
         private static Contact getContact(){
            Contact c1 = new Contact();
            c1.LastName = 'TestData';
            Account acc= getAccount();
            c1.AccountId = acc.Id;
            c1.Contact_Type__c ='Billing';
            Country_Lookup__c c2 = getCountry();
            c1.Country__c = c2.Id;
            insert c1;
            return c1;
         }              
         private static Account getAccount(){
             if(acc1 == null){
                acc1 = new Account();
                acc1.Name = 'L&T Test';
                acc1.Customer_Type__c = 'MNC';
                acc1.CurrencyIsoCode ='GBP';
                Country_Lookup__c acc2 = getCountry();
                acc1.Country__c = acc2.Id;
                acc1.Customer_Legal_Entity_Name__c='Test';
                insert acc1;
             }
            return acc1;
         }             
         private static Country_Lookup__c getCountry(){
            Country_Lookup__c co = new Country_Lookup__c();
            co.Name = 'INDIA';
            co.Country_Code__c ='IN';
            insert co;
            return co;
         }
         private static City_Lookup__c getCity(){
            
            City_Lookup__c city = new City_Lookup__c();
            city.City_Code__c ='BGL';
            city.Name = 'BANGALORE';
    
            insert city;
            return city;
         }
                
    
}