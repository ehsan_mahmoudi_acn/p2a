/*As part of production Ticket# TGCTASK0209037, commented //with sharing */
//public with sharing class AllContactTriggerHandler extends BaseTriggerHandler{
public class AllContactTriggerHandler extends BaseTriggerHandler{

    public override void beforeInsert(List<SObject> newContacts){
        checkPrimaryContacts((List<Contact>)newContacts);
        populateManagers((List<Contact>)newContacts);
    }

    public override void beforeUpdate(List<SObject> newContacts, Map<Id,SObject> newContactsMap, Map<Id,SObject> oldContactsMap){
        checkPrimaryContacts((List<Contact>)newContacts);
        setIsUpdateAndAttachedToBillProfileFlags((List<Contact>)newContacts, (Map<Id, Contact>)oldContactsMap);
        populateManagers((List<Contact>)newContacts);
    }

    public override void beforeDelete(Map<Id, SObject> oldContactMap) {   
        //IR 168 Lead Management Changes
        profPermToDelete(trigger.old,(Map<Id, Contact>)oldContactMap);  
        ContactDeleteTriggerMethod((Map<Id, Contact>)oldContactMap);        
    }
    
    @TestVisible
    private void profPermToDelete(List<Contact> trigOld, Map<Id, Contact> oldContactMap)
    {
        if(oldContactMap.values() != null)
        {
        
        for(Profile profObj : [SELECT id, profile.name from profile where name != 'TI Marketing' And name != 'System Administrator'])
        {    
            for(Contact contObj : trigOld) 
            {
                try{
                    String uid=UserInfo.getProfileId();
                    if(uid == profObj.Id)
                    {
                        contObj.addError('Only Ti_Marketing users have Permission to delete Contact record');
                    }
                }catch(Exception e){
                 ErrorHandlerException.ExecutingClassName='AllContactTriggerHandler:profPermToDelete';
                 ErrorHandlerException.objectList=trigOld;
                 ErrorHandlerException.sendException(e);
                 System.debug('The following exception has occurred: ' + e.getMessage());
                }    
            }
        }   
        }
    } 
    /*
    *   Check on Primary Contact that Only Contact can have Primary Contact checked for an Account. 
    */
    private void checkPrimaryContacts(List<Contact> contacts){
        // Map to Know Number of Primary Contacts Associated to a Account.
        Map<Id,Integer> primaryContactSize = new Map<Id,Integer>();    
        // Map to Present the Primary Contact Associated to a Account.
        Map<Id,Contact> primaryContactMap = new Map<Id,Contact>();
        Integer dupListErrorCount = 0;
        Set<Id> allAccountIds = new Set<Id>();
        
        // Loop through the Contact List to set the Sizes
        for (Contact cont : contacts){        
            if (cont.Primary_Contact__c == true){
                if (!primaryContactSize.containsKey(cont.AccountId)){
                    primaryContactSize.put(cont.AccountId,1);
                }
                else{
                    Integer count = primaryContactSize.get(cont.AccountId);
                    count = count+1;
                    primaryContactSize.put(cont.AccountId,count);
                }
            }         
        }
        
        for (Contact cont : contacts){
            // Checking Duplicates for Primary Contact and Associating the Primary Contact in the List
            if(primaryContactSize.get(cont.AccountId) > 1){
                cont.addError('There are Duplicate Primary Contacts in the List Provided for this Account');
                dupListErrorCount++;
            }
            else{
                if (primaryContactSize.get(cont.AccountId) == 1){
                    System.debug('Inside the Primary Contact');
                    primaryContactMap.put(cont.AccountId, cont);
                }
            }
        }
        
        // Merge all the keySet of PrimaryContact, Billing Contact and Customer Satisfaction into One.
        for (Id accId : primaryContactMap.keyset()){
            if (!allAccountIds.contains(accId)){                
                allAccountIds.add(accId);
            }
        }        
        
        // Processing to Next Level only if there are no Errors from the List Provided
        if (dupListErrorCount == 0){        
            //List<Contact> allCheckedContacts = [Select Id,Name,Primary_Contact__c,AccountId from Contact where AccountId in : allAccountIds];        
            for (Contact cont: [Select Id,Name,Primary_Contact__c,AccountId from Contact where AccountId in : allAccountIds]){
                if (cont.Primary_Contact__c){                    
                    if (primaryContactMap.get(cont.AccountId)!=null){
                        if(primaryContactMap.get(cont.AccountId).Id != cont.Id){                            
                            primaryContactMap.get(cont.AccountId).addError('Contact '+cont.Name +' is already set as Primary Contact for this Account');
                        }
                    }
                }                    
            }                 
        }    
    }

    /*
    *   Update the Is Updated flag when Contact changes are sent to TIBCO. 
    *   Update the "Is Attached to Bill Profile" flag if Contact is associated to any Bill Profile.       
    */
    private void setIsUpdateAndAttachedToBillProfileFlags(List<Contact> contacts, Map<Id, Contact> oldContactMap){
        List<Id> conLst = new List<Id>(); 
        List<BillProfile__c> billList = new List<BillProfile__c>(); 
       
        for(Contact con : contacts){ 
            conLst.add(con.Id); 
            if (con.RecordTypeId == oldContactMap.get(con.Id).RecordTypeId && con.isOppUpdate__c == true && oldContactMap.get(con.Id).isOppUpdate__c == true && con.Sent_to_Tibco__c == true && oldContactMap.get(con.Id).Sent_to_Tibco__c == true ) {    
                con.Is_updated__c = true;
            }
        }
            
        // Checking if Contact is associated to bill profile or not.       
        Integer i = [Select count() from BillProfile__c where Billing_Contact__c IN: conLst];       
        for(Contact cont: contacts){       
            if (i > 0 ){            
                // if i is greater than 0 that means Bill Profile is associated to Contact so here we are updating the Flag as True            
                cont.Is_Attached_to_Bill_Profile__c = true;                    
            }        
            else{            
                cont.Is_Attached_to_Bill_Profile__c = false;                 
            }        
        }

        billList = [select Id, Billing_Contact__c, company__c, Status__c from BillProfile__c where Billing_Contact__c in :conLst ];
         
        for(Contact con : contacts){ 
            for(BillProfile__c bp :billList){
               if(bp.Company__c == Null && bp.Status__c == 'Active'){
                    con.adderror(System.label.Contact_BP_Error);
                }
            }
        }
    }

    /*
    *   Populate manager and postcode information for the contact
    */
    private void populateManagers(List<Contact> contacts){
        Set<Id> conOwnerIdSet = new Set<Id>();
    
        for(Contact con : contacts) {
            conOwnerIdSet.add(con.OwnerId);
        }
    
        //List<User> ownerList = [select Id,Name,ManagerId,Manager.name,Manager.Manager.name,Manager.Manager.Manager.name from User where Id in:conOwnerIdSet];
    
        // Creating a Map of Owner Id and Owner Record related to the Contact.  
        Map<Id,User> ownerMap = new Map<Id,User>();
        for(User u : [select Id,Name,ManagerId,Manager.name,Manager.Manager.name,Manager.Manager.Manager.name from User where Id in:conOwnerIdSet]){
            ownerMap.put(u.Id,u);
        }
    
        // Populating Manager Fields on Contact
        for(Contact con : contacts) {
            con.Regional_Sales_Manager__c = ownerMap.get(con.OwnerId).Manager.name;
            con.Sales_Director_Name__c = ownerMap.get(con.OwnerId).Manager.Manager.name;
            con.Regional_Country_CSM_Head_Name__c = ownerMap.get(con.OwnerId).Manager.Manager.Manager.name;
            
            //Cr-271 US postal code validation rule
            if(con.Country_ISO_Code__c == 'US' && con.Postal_Code__c != null){
                if(!(Pattern.matches('[0-9]{5} [0-9]{4}',con.Postal_Code__c)|| Pattern.matches('[0-9]{5}-[0-9]{4}',con.Postal_Code__c) || Pattern.matches('[0-9]{5}[0-9]{4}',con.Postal_Code__c))){
                    con.Postal_Code__c.addError('The postal code entered for country United States of America is not in the appropriate format.  The Postal Code format should be XXXXXYYYY(E.g. 123456789) OR XXXXX-XXXX(E.g. 12345-6789) OR XXXXX XXXX(e.g. 12345 6789). If have filled in the appropriate data and you are still getting this error message then please contact support team');  
                } 
            }
            else if(con.Country_ISO_Code__c == 'US' && (con.Postal_Code__c == null || con.Postal_Code__c=='')){
                con.Postal_Code__c.addError('The postal code entered for country United States of America is not in the appropriate format.  The Postal Code format should be XXXXXYYYY(E.g. 123456789) OR XXXXX-XXXX(E.g. 12345-6789) OR XXXXX XXXX(e.g. 12345 6789). If have filled in the appropriate data and you are still getting this error message then please contact support team');
            }
        }
    }
    
    public void contactDeleteTriggerMethod(Map<Id,contact> oldContactsMap){
     for (Contact contactObj : oldContactsMap.values())
     {
        try
        {
            String uid=UserInfo.getProfileId();
            /*list<Profile> profiles;
            system.debug('profiles size >>>>>> '+profiles.size());*/
            for(Profile profObj : [SELECT id, name from profile where name != 'TI Marketing']) 
            //for(Profile profObj: profiles )
            {
                if(uid == profObj.Id)
                {
                     contactObj.addError('Only Ti_Marketing profile users have Permission to delete contact record  ');
                }
            } 
         }catch(Exception e){
                 ErrorHandlerException.ExecutingClassName='ContactdDeleteTrigger';
                 ErrorHandlerException.sendException(e);
                 System.debug('The following exception has occurred: ' + e.getMessage()); 

         } 
     }
}
}