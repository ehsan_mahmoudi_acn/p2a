@isTest(seeAllData = false)
public class RoutingTableWrapperTest{
    private static List<CS_City__c> cityList;
    private static List<CS_Country__c> countryList;
    private static List<CS_POP__c> popList; 
    private static List<CS_Bandwidth__c> bandwidthList;
    private static List<CS_Bandwidth_Product_Type__c> bandwidthProdTypeList;
    private static List<cspmb__Price_Item__c> priceItemList;
    private static List<Account> accountList;
    private static List<CS_Cable_Path__c> cablePathList;
    private static List<CS_Resilience__c> resilienceList;
    private static List<CS_Route_Segment__c> routeSegmentList;
    private static List<CS_PTP_Rate_Card_Dummy__c>rateCardDummyList;
    
    private static String city;
    private static String country;
    
    private static testmethod void initTestData(){
            accountList = new List<Account>{
            new Account(Name = 'Account 1', Customer_Type_New__c = 'MNC'),
            new Account(Name = 'Account 2', Customer_Type_New__c = 'MNC')
        };
        insert accountList;
        list<Account> acc = [select id,Name from Account where Name = 'Account 1'];
        System.assert(acc!=null); 
        system.assertEquals(accountList[0].name, acc[0].name);
        
        System.debug('****Account List: ' + accountList);
        
        countryList = new List<CS_Country__c>{
            new CS_Country__c(Name = 'Croatia'),
            new CS_Country__c(Name = 'Australia'),
            new CS_Country__c(Name = 'India'),
            new CS_Country__c(Name = 'Germany'),
            new CS_Country__c(Name = 'Hong Kong'),
            new CS_Country__c(Name = 'New Zealand')
        };
        insert countryList;
        system.assertEquals(countryList[0].name, 'Croatia');
        System.debug('****Country List: ' + countryList);
        
        resilienceList = new List<CS_Resilience__c>{
            new CS_Resilience__c(Name = 'Resilience 1'),
            new CS_Resilience__c(Name = 'Resilience 2'),
            new CS_Resilience__c(Name = 'Resilience 3'),
            new CS_Resilience__c(Name = 'Resilience 4'),
            new CS_Resilience__c(Name = 'Resilience 5')
        };
        insert resilienceList;
        system.assert(resilienceList!=null);
        System.debug('**** Resilience List: ' + resilienceList );
        
        cityList = new List<CS_City__c>{
            new CS_City__c(Name = 'Zagreb', CS_Country__c = countryList[0].Id),
            new CS_City__c(Name = 'Sydney', CS_Country__c = countryList[1].Id)
        };
        insert cityList;
        system.assert(cityList!=null);
        System.debug('****City List: ' + cityList);
        
        popList = new List<CS_POP__c>{
            new CS_POP__c(Name = 'POP 1', CS_Country__c = countryList[0].Id),
            new CS_POP__c(Name = 'POP 2', CS_Country__c = countryList[1].Id),
            new CS_POP__c(Name = 'POP 3', CS_Country__c = countryList[2].Id),
            new CS_POP__c(Name = 'POP 4', CS_Country__c = countryList[3].Id),
            new CS_POP__c(Name = 'POP 5', CS_Country__c = countryList[4].Id)
        };
        insert popList;
        system.assert(popList!=null);
        System.debug('****POP List: ' + popList); 
        
        city = cityList[0].Name;
        country = countryList[0].Name;
        
        cablePathList = new List<CS_Cable_Path__c>{
            new CS_Cable_Path__c(Name = 'Cable path 1'),
            new CS_Cable_Path__c(Name = 'Cable path 2'),
            new CS_Cable_Path__c(Name = 'Cable path 3'),
            new CS_Cable_Path__c(Name = 'Cable path 4'),
            new CS_Cable_Path__c(Name = 'Cable path 5')
        };
        insert cablePathList;
        system.assert(cablePathList!=null);
        System.debug('**** Cable Path List: ' + cablePathList);
        
        routeSegmentList = new List<CS_Route_Segment__c>{
            new CS_Route_Segment__c(Name = 'Route Segment 1', Product_Type__c = 'IPL', POP_A__c = popList[0].Id, POP_Z__c = popList[4].Id, CS_Cable_Path__c = cablePathList[0].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 2', Product_Type__c = 'IPL', POP_A__c = popList[1].Id, POP_Z__c = popList[3].Id, CS_Cable_Path__c = cablePathList[1].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 3', Product_Type__c = 'IPL', POP_A__c = popList[2].Id, POP_Z__c = popList[2].Id, CS_Cable_Path__c = cablePathList[2].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 4', Product_Type__c = 'EPL', POP_A__c = popList[3].Id, POP_Z__c = popList[1].Id, CS_Cable_Path__c = cablePathList[3].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 5', Product_Type__c = 'EPLX', POP_A__c = popList[4].Id, POP_Z__c = popList[0].Id, CS_Cable_Path__c = cablePathList[4].Id)
        };
        insert routeSegmentList;
        system.assert(routeSegmentList!=null);
        System.debug('****Route Segment List: ' + routeSegmentList); 
        
        bandwidthList = new List<CS_Bandwidth__c>{
            new CS_Bandwidth__c(Name = '64k', Bandwidth_Value_MB__c = 64),
            new CS_Bandwidth__c(Name = '128k', Bandwidth_Value_MB__c = 128)
        };
        insert bandwidthList;
        System.debug('****Bandwidth List: ' + bandwidthList);
        
        bandwidthProdTypeList = new List<CS_Bandwidth_Product_Type__c>{
            new CS_Bandwidth_Product_Type__c(Name = 'BAndwidth Product Type 1', CS_Bandwidth__c = bandwidthList[0].Id, Product_Type__c = 'IPVPN'),
            new CS_Bandwidth_Product_Type__c(Name = 'BAndwidth Product Type 2', CS_Bandwidth__c = bandwidthList[1].Id, Product_Type__c = 'IPVPN')
        };
        insert bandwidthProdTypeList;
        System.debug('****Bandwidth Product Type List: ' + bandwidthProdTypeList);        
        
        priceItemList = new List<cspmb__Price_Item__c>{
            new cspmb__Price_Item__c(Name = 'Price Item 1', Country__c = countryList[0].Id, City__c = cityList[0].Id, CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[0].Id, cspmb__Account__c = accountList[0].Id, cspmb__Is_Active__c = true, IPVPN_Type__c = 'Standard', Connectivity__c = 'Onnet', Pricing_Segment__c = 'MNC'),
            new cspmb__Price_Item__c(Name = 'Price Item 2', Country__c = countryList[0].Id, City__c = cityList[0].Id, CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[0].Id, cspmb__Account__c = null, cspmb__Is_Active__c = true, IPVPN_Type__c = 'Standard', Connectivity__c = 'Onnet', Pricing_Segment__c = 'MNC')
        };
        insert priceItemList;
        System.debug('****Price Item List: ' + priceItemList);        
        
        rateCardDummyList = new list<CS_PTP_Rate_Card_Dummy__c  >();
        CS_PTP_Rate_Card_Dummy__c  rcd = new CS_PTP_Rate_Card_Dummy__c(); 
        rcd.Product__c = 'EPL';
        rcd.MRC__c = 456;
        rcd.CS_A_Country__c = countryList[0].id;
        rcd.CS_Cable_Path__c = cablePathList[0].id;
        rcd.CS_Route_Segment__c = routeSegmentList[0].id;
        rcd.CS_Z_Country__c = countryList[0].id;
        rcd.CS_Z_POP__c = popList[0].id;
        rcd.Price_Item__c = priceItemList[0].id;
        rcd.A_End_Country_Name__c = 'Hong kong' ;
        rcd.Bandwidth_Name__c = 'Bandwidth';
        rcd.Cable_Path_Name__c = 'Cable Systems';
        rcd.Circuit_Type__c = 'Circuit Type';
        rcd.Network_Technology__c = 'Technology';
        rcd.Z_End_Country_Name__c = 'Austrila' ;
        rcd.A_POP_Name__c = 'PopUp-A';
        rcd.Z_POP_Name__c = 'PopUp-Z';
        rcd.NRC__c = 526;
        rcd.Partner_Name__c = 'Partner';
        rcd.NNI_Service_ID__c = 'Service';
        rcd.CS_Resilience__c = resilienceList[0].id;
        rcd.Resilience_Name__c = 'Resilience';
        rcd.NNI_Location1__c = 'Location';
        rateCardDummyList.add(rcd);
        insert  rateCardDummyList;
        system.assert(rateCardDummyList!=null);
        Exception ee = null;
        try{
            Test.startTest();
            RoutingTableWrapper rtw = new RoutingTableWrapper(rcd);
            Integer i = rtw.compareTo(rtw);
            List<CS_PTP_Rate_Card_Dummy__c> prcd = RoutingTableWrapper.getSortedRoutongTable(rateCardDummyList); 
        
          } catch(Exception e){
           ErrorHandlerException.ExecutingClassName='RoutingTableWrapperTest:initTestData';         
           ErrorHandlerException.sendException(e); 
            ee = e;
        }  
        finally {
            Test.stopTest();
           
            if(ee != null){
                throw ee;
         }
      } 
   } 
}