/********************************************************************
* Class:       ReturnRateCardsForP2PDev
* Description: This is a class to return rate card records based on
               filters passed.

**********************************************************************/
public class ReturnRateCardsForP2PDev{

/****************************************************************************
 * Method Name       : returnRateCards
 * Method Description: method to query the route segment records 
                       based on filters passed and return Price Item records
*****************************************************************************/
  public static List<CS_PTP_Rate_Card_Dummy__c> returnRateCards(set<String> setproductType, Id countryA, Id countryZ , Id cityA, Id cityZ,Id popAEnd,Id popZend,List<String> bandwidthNames,ID acct) 
  { 
    List<CS_Route_Segment__c >  routeList = new List<CS_Route_Segment__c>();
    List<CS_Bandwidth_Product_Type__c> bandwidthList = new List<CS_Bandwidth_Product_Type__c>();
    List<cspmb__Price_Item__c> priceItemListGeneric = new List<cspmb__Price_Item__c>();
    List<cspmb__Price_Item__c> priceItemListAccount = new List<cspmb__Price_Item__c>();
     List<CS_PTP_Rate_Card_Dummy__c> rateCardDummyList = new List<CS_PTP_Rate_Card_Dummy__c>();
     Set<Id> cablePathIdSet = new Set<Id>();  

     Map<String,cspmb__Price_Item__c> priceItemKey = new Map<String,cspmb__Price_Item__c>();
     
    Map<String,CS_Route_Segment__c> routeKeyMap = new Map<String,CS_Route_Segment__c>();
    Set<ID> bandwidthProductTypeIdSet = new Set<ID>();
    List<String> serviceClassNames = new List<String>{'Low Priority Data','Critical Data','Interactive Data','Standard Data','Voice','Video','VLAN mode'};
  
    if(countryA != null && countryZ != null)
    {
        String queryString = 'Select id,Name,Network_Technology__c,Product_Type__c,CS_Cable_Path__c,Resilience_Id__c,A_End_City__c,Z_End_City__c,POP_A__c,POP_Z__c,Circuit_Type__c,RTD__c from CS_Route_Segment__c where Product_Type__c IN: setproductType  and Type__c=\'Route\' and A_End_City__r.CS_Country__c=:countryA and Z_End_City__r.CS_Country__c=:countryZ';
          
          if(cityA!=null)
              queryString = queryString +' and A_End_City__c =:cityA';
          if(cityZ!=null)
              queryString = queryString +' and Z_End_City__c =:cityZ';
          if(popAEnd!=null)
              queryString = queryString +' and POP_A__c =:popAEnd';
          if(popZEnd!=null)
              queryString = queryString +' and POP_Z__c =:popZEnd';
                 
           System.debug('^^^^queryString'+queryString);
           routeList = Database.query(queryString);
           
           system.debug('***routeList=' + routeList);
           
           
           for(CS_Route_Segment__c rt:routeList)
           {
               if(rt.CS_Cable_Path__c!=null)
               cablePathIdSet.add(rt.CS_Cable_Path__c);
               String key = returnCompositeKey(rt.Product_Type__c,rt.A_End_City__c,rt.Z_End_City__c,rt.POP_A__c,rt.POP_Z__c,rt.CS_Cable_Path__c);
               routeKeyMap.put(key,rt);   
           }
           
           system.debug('***cablePathIdSet=' + cablePathIdSet);
           system.debug('***routeKeyMap=' + routeKeyMap);
       }
       
       
    if(!bandwidthNames.isEmpty())
    {
          String querybWString  = 'Select id, Bandwidth_Name__c from CS_Bandwidth_Product_Type__c where Bandwidth_Name__c in:bandwidthNames and Product_Type__c IN:setproductType';
          bandwidthList = Database.query(querybWString); 
          system.debug('bandwidthList: '+bandwidthList);
          for(CS_Bandwidth_Product_Type__c bwPdt :bandwidthList)
      {
            bandwidthProductTypeIdSet.add(bwPdt.Id);
          }
      } 
      
      system.debug('***bandwidthProductTypeIdSet=' + bandwidthProductTypeIdSet); 
       
    String queryPriceItemGeneric ='Select id,Partner_Name__c,Low_Priority_MRC__c,Low_Priority_NRC__c,Critical_MRC__c,Port_Rating__c,Critical_NRC__c,Interactive_MRC__c,Interactive_NRC__c,Standard_MRC__c,' + 
      'Voice_MRC__c,Voice_NRC__c,Video_MRC__c,Video_NRC__c,cspmb__Recurring_Charge__c,cspmb__One_Off_Charge__c,CS_Bandwidth_Product_Type__c,CS_Bandwidth_Product_Type__r.Product_Type__c,' + 
      'CS_Bandwidth_Product_Type__r.Bandwidth_Name__c,City__c,City__r.Name,Z_City__c,Z_City__r.Name,Country__c,Country__r.Name,Z_Country__c,Z_Country__r.Name,POP__c,POP__r.Name,Z_POP__c,Z_POP__r.Name,' + 
      'Standard_NRC__c,CS_Cable_Path__c,CS_Cable_Path__r.Name, Generic_Product_Type__c from cspmb__Price_Item__c where CS_Bandwidth_Product_Type__c in: bandwidthProductTypeIdSet and cspmb__Account__c=Null and ' + 
      'Country__c=: countryA and Z_Country__c=: countryZ and cspmb__Is_Active__c =TRUE';
      
    String queryPriceItemAccount= 'Select id,Partner_Name__c,Low_Priority_MRC__c,Low_Priority_NRC__c,Critical_MRC__c,Port_Rating__c,Critical_NRC__c,Interactive_MRC__c,Interactive_NRC__c,' + 
      'Standard_MRC__c,Voice_MRC__c,Voice_NRC__c,Video_MRC__c,Video_NRC__c,cspmb__Recurring_Charge__c,cspmb__One_Off_Charge__c,CS_Bandwidth_Product_Type__c,CS_Bandwidth_Product_Type__r.Product_Type__c,' + 
      'CS_Bandwidth_Product_Type__r.Bandwidth_Name__c,City__c,City__r.Name,Z_City__c,Z_City__r.Name,Country__c,Country__r.Name,Z_Country__c,Z_Country__r.Name,POP__c,POP__r.Name,' + 
      'Z_POP__c,Z_POP__r.Name,Standard_NRC__c,CS_Cable_Path__c,CS_Cable_Path__r.Name, Generic_Product_Type__c from cspmb__Price_Item__c where CS_Bandwidth_Product_Type__c in: bandwidthProductTypeIdSet and cspmb__Account__c=: acct ' + 
      'and Country__c=: countryA and Z_Country__c=: countryZ and cspmb__Is_Active__c =TRUE';
      
      
      set<string> setOtherProductType = new set<string>();
      
      setOtherProductType.addAll(setproductType);
       
    if (setproductType.contains('ICBS'))
      {
      String queryForICBSGeneric = queryPriceItemGeneric + ' and Generic_Product_Type__c=\'ICBS\'';
      String queryForICBSAccount = queryPriceItemAccount + ' and Generic_Product_Type__c=\'ICBS\'';
         
         priceItemListGeneric = Database.query(queryForICBSGeneric);
          priceItemListAccount = Database.query(queryForICBSAccount);
          
          setOtherProductType.remove('ICBS');
      
    }
    
    if (setproductType.contains('EVPL'))
      {
      String queryForEVPLGeneric = queryPriceItemGeneric + ' and Generic_Product_Type__c=\'EVPL\'';
      String queryForEVPLAccount = queryPriceItemAccount + ' and Generic_Product_Type__c=\'EVPL\'';
         
         priceItemListGeneric = Database.query(queryForEVPLGeneric);
          priceItemListAccount = Database.query(queryForEVPLAccount);
          
          setOtherProductType.remove('EVPL');
      
    }
    
       
    if (setOtherProductType.size()>0)
    {
        queryPriceItemGeneric = queryPriceItemGeneric + ' and CS_Cable_Path__c in: cablePathIdSet and Generic_Product_Type__c in: setOtherProductType';
          queryPriceItemAccount = queryPriceItemAccount + ' and CS_Cable_Path__c in: cablePathIdSet and Generic_Product_Type__c in: setOtherProductType';
          
       priceItemListGeneric.addAll((List<cspmb__Price_Item__c>)Database.query(queryPriceItemGeneric));
          priceItemListAccount.addAll((List<cspmb__Price_Item__c>)Database.query(queryPriceItemAccount));        
    }
           
                
            
      system.debug('queryPriceItemGeneric******'+queryPriceItemGeneric);
      system.debug('queryPriceItemAccount******'+queryPriceItemAccount);
      
      system.debug('priceItemListGeneric******'+priceItemListGeneric);
      system.debug('priceItemListAccount******'+priceItemListAccount);
           
           
    for(cspmb__Price_Item__c pi:priceItemListAccount)
    {
        String key = returnCompositeKey(pi.CS_Bandwidth_Product_Type__r.Product_Type__c,pi.City__c,pi.Z_City__c,pi.POP__c,pi.Z_POP__c,pi.CS_Cable_Path__c);         
          priceItemKey.put(key,pi);         
    }    
           
      for(cspmb__Price_Item__c pi:priceItemListGeneric)
      {
        String key = returnCompositeKey(pi.CS_Bandwidth_Product_Type__r.Product_Type__c,pi.City__c,pi.Z_City__c,pi.POP__c,pi.Z_POP__c, pi.CS_Cable_Path__c);
          if(!priceItemKey.containsKey(key))
          {
            priceItemKey.put(key,pi); 
          }              
    } 
    
    system.debug('priceItemKey******'+priceItemKey);
              
      for(String rt:routeKeyMap.keySet())
    {
        Integer noOfRecords = 1;
                      
          if(routeKeyMap.get(rt).Product_Type__c == 'EVPL')
            noOfRecords =7;
                        
      if(priceItemKey.containsKey(rt))
          {         
        for(Integer i=0;i<noOfRecords;i++)
              {
                 System.debug('*****inside account price');
                 CS_PTP_Rate_Card_Dummy__c dummyRateCard = new CS_PTP_Rate_Card_Dummy__c();                 
                 dummyRateCard.CS_A_Country__c = countryA;
                 dummyRateCard.CS_A_POP__c=popAEnd;
                 dummyRateCard.CS_Bandwidth_Product_Type__c=priceItemKey.get(rt).CS_Bandwidth_Product_Type__c;
                 dummyRateCard.CS_Cable_Path__c=priceItemKey.get(rt).CS_Cable_Path__c;
                 dummyRateCard.CS_Route_Segment__c=routeKeyMap.get(rt).Id;
                 dummyRateCard.CS_Z_Country__c=countryZ;
                 dummyRateCard.CS_Z_POP__c=popZEnd;
                 dummyRateCard.Price_Item__c=priceItemKey.get(rt).Id;
                 dummyRateCard.A_End_Country_Name__c = priceItemKey.get(rt).Country__r.Name;
                 dummyRateCard.Bandwidth_Name__c = priceItemKey.get(rt).CS_Bandwidth_Product_Type__r.Bandwidth_Name__c;
                 dummyRateCard.Cable_Path_Name__c = priceItemKey.get(rt).CS_Cable_Path__r.Name;
                 dummyRateCard.Circuit_Type__c = routeKeyMap.get(rt).Circuit_Type__c;
                 dummyRateCard.Product__c = routeKeyMap.get(rt).Product_Type__c;
                 dummyRateCard.Network_Technology__c=routeKeyMap.get(rt).Network_Technology__c;
                 
                 //dummyRateCard.RTD__c = routeKeyMap.get(rt).RTD__c;
                 dummyRateCard.Z_End_Country_Name__c = priceItemKey.get(rt).Z_Country__r.Name;
                 if(popAEnd!=null)
                    dummyRateCard.A_POP_Name__c=priceItemKey.get(rt).POP__r.Name;
                 
                 if(popZEnd!=null)
                    dummyRateCard.Z_POP_Name__c=priceItemKey.get(rt).Z_POP__r.Name;
                 
                 if(routeKeyMap.get(rt).Product_Type__c == 'EPL' || routeKeyMap.get(rt).Product_Type__c == 'IPL' || routeKeyMap.get(rt).Product_Type__c == 'EPLX' || routeKeyMap.get(rt).Product_Type__c == 'ICBS')
                 {
                    dummyRateCard.MRC__c=priceItemKey.get(rt).cspmb__Recurring_Charge__c;
                    dummyRateCard.NRC__c=priceItemKey.get(rt).cspmb__One_Off_Charge__c;
                    dummyRateCard.Partner_Name__c = priceItemKey.get(rt).Partner_Name__c;
                    dummyRateCard.CS_Resilience__c = routeKeyMap.get(rt).Resilience_Id__c;
                 }
                 
                   
                   if(routeKeyMap.get(rt).Product_Type__c == 'EVPL')
                   {
                     dummyRateCard.Class_of_Service__c = serviceClassNames[i];
                     if(serviceClassNames[i] == 'Low Priority Data')
                      {
                          dummyRateCard.MRC__c= priceItemKey.get(rt).Low_Priority_MRC__c;
                          dummyRateCard.NRC__c = priceItemKey.get(rt).Low_Priority_NRC__c;
                      } 
                      else if(serviceClassNames[i] == 'Critical Data')
                      {
                          dummyRateCard.MRC__c= priceItemKey.get(rt).Critical_MRC__c;
                          dummyRateCard.NRC__c = priceItemKey.get(rt).Critical_NRC__c;
                      } 
                      else if(serviceClassNames[i] == 'Interactive Data')
                      {
                          dummyRateCard.MRC__c= priceItemKey.get(rt).Interactive_MRC__c;
                          dummyRateCard.NRC__c = priceItemKey.get(rt).Interactive_NRC__c;
                      } 
                      else if(serviceClassNames[i] == 'Standard Data')
                      {
                          dummyRateCard.MRC__c= priceItemKey.get(rt).Standard_MRC__c;
                          dummyRateCard.NRC__c = priceItemKey.get(rt).Standard_NRC__c;
                      } 
                      else if(serviceClassNames[i] == 'Voice')
                      {
                          dummyRateCard.MRC__c= priceItemKey.get(rt).Voice_MRC__c;
                          dummyRateCard.NRC__c = priceItemKey.get(rt).Voice_NRC__c;
                      } 
                      else if(serviceClassNames[i] == 'Video')
                      {
                          dummyRateCard.MRC__c= priceItemKey.get(rt).Video_MRC__c;
                          dummyRateCard.NRC__c = priceItemKey.get(rt).Video_NRC__c;
                      } 
                      else if(serviceClassNames[i] == 'VLAN mode')
                      {
                          dummyRateCard.MRC__c= priceItemKey.get(rt).cspmb__Recurring_Charge__c;
                          dummyRateCard.NRC__c = priceItemKey.get(rt).cspmb__One_Off_Charge__c;
                      } 
          }    
                  rateCardDummyList.add(dummyRateCard);
        }
      }       
    }
        
    System.debug('*****ratecardReturned'+rateCardDummyList);
      return rateCardDummyList ;  
  }  
     
  /****************************************************************************
   * Method Name       : returnCompositeKey
   * Method Description: method to form composite unique key by concatinating
                         parameters passed
  *****************************************************************************/ 
  //for @Testvisible modified by Anuradha on 2/20/2017
  @Testvisible
  private static String returnCompositeKey(String productType,Id CityA, Id CityZ, Id POPA, Id POPZ, Id CSCablePath)
  {
      String key = productType + '-' + CityA + '-' + CityZ + '-' + POPA + '-' + POPZ;
      if (CSCablePath==null)
        key=key + '-0';
      else
        key=key + '-' + CSCablePath;
        
      return key;
  }
}