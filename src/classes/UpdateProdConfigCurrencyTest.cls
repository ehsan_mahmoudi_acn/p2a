@isTest
private class UpdateProdConfigCurrencyTest{

    static testMethod void updateProdConfigCurrencyMethod1(){
    P2A_TestFactoryCls.sampleTestData();
        
    List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
    List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
    List<Country_Lookup__c> countrylist =  P2A_TestFactoryCls.getcountry(1);
    List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
    List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
    List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
    List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
    List<CS_Bandwidth__c> ListBandwidth = P2A_TestFactoryCls.getBandwidths(1);  
    List<cscfga__Product_Configuration__c> productconfigsList =P2A_TestFactoryCls.getProductonfig(1,prodBaskList,ProductDeflist,Pbundle,Offerlists);
    List<cscfga__Configuration_Screen__c> configLst = P2A_TestFactoryCls.getConfigScreen(1,ProductDeflist);
    List<cscfga__Screen_Section__c> ScreenSec = P2A_TestFactoryCls.getScreenSec(1, configLst);
    
    List<cscfga__Attribute_Definition__c> Attributedeflist = P2A_TestFactoryCls.getAttributesdef(1,productconfigsList,ProductDeflist, configLst, ScreenSec);
    
    cscfga__Product_Configuration__c pb = new cscfga__Product_Configuration__c();
    pb.cscfga__Product_basket__c = prodBaskList[0].id ;
                pb.cscfga__Product_Definition__c = ProductDeflist[0].id ;
                pb.name ='IPL';
                pb.cscfga__contract_term_period__c = 12;
                pb.Is_Offnet__c = 'yes';
                pb.cscfga_Offer_Price_MRC__c = 200;
                pb.cscfga_Offer_Price_NRC__c = 300;
                pb.cscfga__Product_Bundle__c = Pbundle[0].id;
                pb.Rate_Card_NRC__c = 200;
                pb.Rate_Card_RC__c = 300;
                pb.cscfga__total_contract_value__c = 1000;
                pb.cscfga__Contract_Term__c = 12;
                pb.CurrencyIsoCode = 'USD';
                pb.cscfga_Offer_Price_NRC__c = 100;
                pb.Child_COst__c = 100;
                pb.Cost_NRC__c = 100;
                pb.Cost_MRC__c = 100;
                pb.Product_Name__c = 'test product';
                pb.Added_Ports__c = null ;
                Pb.cscfga__Product_Family__c = 'Point to Point';
                Pb.csordtelcoa__Replaced_Product_Configuration__c = productconfigsList[0].id;
                Insert pb;
                system.assert(pb!=null);
    Product2 prod2=new product2();
    prod2.Name = 'Test product';
    prod2.Family = 'Test Family';
   
    cscfga__Attribute__c Attributes = new cscfga__Attribute__c();
    Attributes.name = 'Product Basket Currency Ratio';
    Attributes.cscfga__is_active__c = true;
    Attributes.cscfga__Line_Item_Description__c = 'Connected Colo NRC';
    Attributes.cscfga__Line_Item_Sequence__c = 2;
    Attributes.cscfga__List_Price__c = 200;
    Attributes.CurrencyIsoCode = 'USD';
    Attributes.cscfga__Price__c = 200;
    Attributes.cscfga__Is_Line_Item__c = true;
    Attributes.cscfga__Is_rate_line_item__c = true;
    Attributes.cscfga__Value__c = 'Test';
    Attributes.cscfga__Product_Configuration__c= productconfigsList[0].id;
    Attributes.cscfga__Value__c = 'Test';
    Attributes.cscfga__Attribute_Definition__c = Attributedeflist[0].id; 
    insert Attributes;
    
        id oppId= oppList[0].id;
        updateProdConfigCurrency.updateProductConfigCurrency(oppId,1,'USD');
    }
     @isTest static void testExceptions(){
         updateProdConfigCurrency alls=new updateProdConfigCurrency();        
         try
         {
         updateProdConfigCurrency.updateProductConfigCurrency(null,null,null);
         system.assert(alls!=null);
         }
         catch(Exception e)
         {
         ErrorHandlerException.ExecutingClassName='updateProdConfigCurrencyTest:testExceptions';         
         ErrorHandlerException.sendException(e); 
         }
         
          
     }        
            
}