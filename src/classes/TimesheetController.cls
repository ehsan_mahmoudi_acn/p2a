public class TimesheetController {
 /*[19-Jul-2012] Commenting the whole code since its SAAS tool kit trigger.


    public String worksheetId;
    public String timesheetId;
    List<Time_Sheet__c> timesheets = new List<Time_Sheet__c>();
    Set<String> existingTimeIds = new Set<String>();

//Constructor  
   public TimesheetController(){
                worksheetId=ApexPages.currentPage().getParameters().get('worksheet');
                timesheetId=ApexPages.currentPage().getParameters().get('timesheet');
       System.debug('worksheetId: '+worksheetId);
       System.debug('timesheetId: '+timesheetId);
    Time_Sheet__c timesheet;
    timesheets=getAllTimesheets();
    for(Integer i=0;i<4;i++)
    {
        timesheet = new Time_Sheet__c();
        timesheet.Worksheet__c = worksheetId;
        timesheets.add(timesheet);
    }

    if(timesheets.size()>=4){
        timesheet = new Time_Sheet__c();
        timesheet.Worksheet__c = worksheetId;
        timesheets.add(timesheet);
    }
    }
    
    //This method returns a list of all the records (Timesheets) to be saved and showed
    public List<Time_Sheet__c> getAllTimesheets(){
    List<Time_Sheet__c> newTimesheets = new List<Time_Sheet__c>();

    for (Time_Sheet__c ts : [Select t.id, t.Worksheet__c, Resource__c, Date__c, Type__c, Status__c, Actual_Hours__c, Comments__c From Time_Sheet__c t where t.Worksheet__c = :worksheetId]){
                            if (ts != null){
                                //Adding not null records to the newTimesheets List
                                newTimesheets.add(ts);
                                //Adding Ids of existing timesheets to the existingTimeIds Set
                                existingTimeIds.add(ts.Id);
                               }   
    }
    
    return newTimesheets;
    }

    public List<Time_Sheet__c> getTimesheets() {       
              return timesheets;
            }
            
    public void setTimesheets(List<Time_Sheet__c> ts) {
        timesheets=ts;
    }
    //Action on Cancel; the coressponding worksheet is displayed
    public PageReference cancel() {
        return new PageReference('/'+worksheetId);
       
    }

    //Action on Save; the coressponding worksheet is displayed after insert, update or delete
    public PageReference save() {
    List<Time_Sheet__c> updateTimesheets = new List<Time_Sheet__c>();
    List<Time_Sheet__c> newTimesheets = new List<Time_Sheet__c>();
    List<Time_Sheet__c> deleteTimesheets = new List<Time_Sheet__c>();

    for(Time_Sheet__c objTimesheet:timesheets){
                
        if(objTimesheet==null)continue;
        //If any of the fields is null; delete the record
        if((objTimesheet.Resource__c==null || objTimesheet.Date__c==null || objTimesheet.Status__c==null || objTimesheet.Actual_Hours__c==null) && existingTimeIds.Contains(objTimesheet.id)){
                                deleteTimesheets.add(objTimesheet);
                    }
        //If all the fields are not null and record exists previously, delete the record
        if(objTimesheet.Resource__c!=null && objTimesheet.Date__c!=null && objTimesheet.Status__c!=null && objTimesheet.Actual_Hours__c!=null && existingTimeIds.Contains(objTimesheet.id)){
                                updateTimesheets.add(objTimesheet);
                    }
        //If all the fields are not null and record does not exist previously, insert the record
        if(objTimesheet.Resource__c!=null && objTimesheet.Date__c!=null && objTimesheet.Status__c!=null && objTimesheet.Actual_Hours__c!=null && (!existingTimeIds.Contains(objTimesheet.id))){
                                newTimesheets.add(objTimesheet);
        }

        }
    
    System.debug('Delete Size: '+deleteTimesheets.size());
    System.debug('Update Size: '+updateTimesheets.size());
    System.debug('New Size: '+newTimesheets.size());

            if(updateTimesheets.size()>0)update updateTimesheets;
            if(newTimesheets.size()>0)insert newTimesheets; 
            if(deleteTimesheets.size()>0)delete deleteTimesheets;
            
        return new PageReference('/'+worksheetId);
    }
    */
}