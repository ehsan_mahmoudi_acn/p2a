/*
Test class was converted as class - so re-created@16/2/2017
*/
@isTest(SeeAllData = false)
public class AEndCountryLookupTest {

    private static List<IPL_Rate_Card_Item__c> rateCardItemList;
    private static List<Additional_Data__c> additionalDataList;
    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    private static Id[] excludeIds = new List<Id>();
    private static Integer pageOffset, pageLimit;
    private static Object[] data = new List<Object>();
     
    private static void initTestData(){
        
        searchFields.put('IPL', 'true');
        searchFields.put('EPL', 'false');
        searchFields.put('EPLx', 'true');
        searchFields.put('ICBS', 'false');
        searchFields.put('searchValue','');
        
                
        additionalDataList = new List<Additional_Data__c>{
            new Additional_Data__c(Name = 'Croatia', Type__c = 'Country'),
            new Additional_Data__c(Name = 'Australia', Type__c = 'Country'),
            new Additional_Data__c(Name = 'Germany', Type__c = 'Country'),
            new Additional_Data__c(Name = 'Poland', Type__c = 'Country'),
            new Additional_Data__c(Name = 'Peru', Type__c = 'Country')
        };

        insert additionalDataList;
          System.assertEquals('Croatia',additionalDataList[0].Name ); 
        
        rateCardItemList = new List<IPL_Rate_Card_Item__c>{
            new IPL_Rate_Card_Item__c(Aend_Country__c = additionalDataList[0].Id, Zend_Country__c = additionalDataList[1].Id, Product_Type__c = 'IPL'),
            new IPL_Rate_Card_Item__c(Aend_Country__c = additionalDataList[2].Id, Zend_Country__c = additionalDataList[3].Id, Product_Type__c = 'EPL')
        };
        
        insert rateCardItemList;
         System.assertEquals('IPL',rateCardItemList[0].Product_Type__c ); 
    }
    
    static testMethod void doLookupSearchTest(){
        Exception ee = null;
        
        try{
           CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
            initTestData();
            
            
            
            AEndCountryLookup aEndCountryLookup = new AEndCountryLookup();  
            aEndCountryLookup.doDynamicLookupSearch(searchFields, productDefinitionID);
            String requiredAtts = aEndCountryLookup.getRequiredAttributes();
            data = aEndCountryLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            
            System.debug('*******Data: ' + data);
            
 
            System.assert(data.size() > 0, '');
            Test.stopTest();
            
        } catch(Exception e){
            ee = e;
        } finally {
           // Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }
    }
    
    /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }
}