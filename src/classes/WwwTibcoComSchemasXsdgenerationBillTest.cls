@istest(seealldata=false)
public class WwwTibcoComSchemasXsdgenerationBillTest{
     
    static testMethod void allOrderTriggertest() {
    
      wwwTibcoComSchemasXsdgenerationBill.BillingActivateResponse_element tb1 = new wwwTibcoComSchemasXsdgenerationBill.BillingActivateResponse_element();
      wwwTibcoComSchemasXsdgenerationBill.Service_element tb2 = new wwwTibcoComSchemasXsdgenerationBill.Service_element();
      wwwTibcoComSchemasXsdgenerationBill.ServiceLineItem_element tb3 = new wwwTibcoComSchemasXsdgenerationBill.ServiceLineItem_element();
      wwwTibcoComSchemasXsdgenerationBill.UDF_element tb4 = new wwwTibcoComSchemasXsdgenerationBill.UDF_element();
      wwwTibcoComSchemasXsdgenerationBill.Subscription_element tb5 = new wwwTibcoComSchemasXsdgenerationBill.Subscription_element();
    
    system.assertEquals(true,tb1!=null);
    system.assertEquals(true,tb2!=null);
    system.assertEquals(true,tb3!=null);
    system.assertEquals(true,tb4!=null);
    system.assertEquals(true,tb5!=null);
    }
 }