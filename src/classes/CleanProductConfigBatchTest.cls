@isTest
private class CleanProductConfigBatchTest{
    private static List<cscfga__Product_Configuration__c> Productconfilist;
    private static List<cscfga__Product_Definition__c> Pdlist;
    private static List<Product_Definition_Id__c> PdIdlist; 
    
    static testmethod void test() {
        P2A_TestFactoryCls.sampleTestData();   
        List<cscfga__Product_Basket__c> pblist =  P2A_TestFactoryCls.getProductBasket(1); 

        Pdlist = new List<cscfga__Product_Definition__c>{
                 new cscfga__Product_Definition__c(name  = 'Master IPVPN Service',cscfga__Description__c = 'Test master IPVPN')
                };
        insert Pdlist;

        PdIdlist = new List<Product_Definition_Id__c>{
                   new Product_Definition_Id__c(name = 'Master IPVPN Service',Product_Id__c = Pdlist[0].Id)
                   };
        insert PdIdlist;
        System.assertEquals('Master IPVPN Service',PdIdlist[0].Name); 

        Productconfilist = new List<cscfga__Product_Configuration__c>{
                           new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[0].id,cscfga__Product_Basket__c = pblist[0].Id,cscfga__Product_Family__c = 'Test family')
                           };
        insert Productconfilist;      
        System.assertEquals('Test family',Productconfilist[0].cscfga__Product_Family__c); 

        String query = 'SELECT Id,Name,cscfga__Quantity__c FROM cscfga__Product_Configuration__c';

        Test.startTest();
         CleanProductConfigBatch cleanconfig = new CleanProductConfigBatch(query); 
         Database.executeBatch(cleanconfig);
        Test.stopTest();

        List<cscfga__Product_Configuration__c> delcleanconfig  = [select id from cscfga__Product_Configuration__c where id IN :Productconfilist limit 1];
         delete delcleanconfig;
         Integer i = [SELECT COUNT() FROM cscfga__Product_Configuration__c];
        System.assertEquals(i, 0); 
           
    }
}