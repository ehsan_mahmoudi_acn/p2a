@isTest(seealldata = false)
public class AllOppTeamMemberTriggerHandlerTest {
    
  
    static testMethod void emailTeamMembersmethod1()    {
            
            SalesSpecialistRoleOppTeam__c salesspecial = new SalesSpecialistRoleOppTeam__c();
            salesspecial.name = 'Testsalesspecial';
            salesspecial.OpportunityTeam__c = 'SSRoleOppTeam' ;
            salesspecial.SSRole__c = 'Cloud - AU Sales Specialist' ;
            insert salesspecial;
                 
            List<Account> accounts = P2A_TestFactoryCls.getAccounts(1);
            List<Opportunity> opportunitys = P2A_TestFactoryCls.getOpportunitys(1,accounts);
          
            List<OpportunityTeamMember> oppTeam = new List<OpportunityTeamMember>(); 
            
            for (Opportunity opp : opportunitys)
                {
                    OpportunityTeamMember otm = new OpportunityTeamMember();
                    otm.OpportunityId = opp.Id;
                    otm.UserId = Userinfo.getuserId();
                    //otm.TeamMemberRole = Userinfo.getUserRoleId();
                    otm.TeamMemberRole = 'Cross Country Application';
                    oppTeam.add(otm);
                }            
            Map<Id, OpportunityTeamMember> oppMap = new Map<Id, OpportunityTeamMember>();
            oppMap.put(oppTeam[0].id,oppTeam[0]);
            
            List<OpportunityShare> oppShare = new List<OpportunityShare>();
            if (oppTeam.size() > 0){
                
                    Database.Saveresult[] oppTeam_record = Database.insert(oppTeam, false);
                    for (Database.Saveresult recSave : oppTeam_record) 
                    {
                        if (recSave.isSuccess()) 
                            {
                                oppShare.add( new OpportunityShare(UserOrGroupID = oppTeam[0].UserId,
                                                        OpportunityId = oppteam[0].OpportunityId,
                                                        OpportunityAccessLevel = 'Edit'));
                            } 
                        else
                            {
                                Database.Error eMsg = recSave.getErrors()[0];
                            }
                        
                    }
                }
                
        OpportunitySplit oppSplit = new OpportunitySplit(Total_Product_SOV__c=25.35, Team_Role__c='sfdsdf', SplitPercentage_Product_SOV__c=54/*, OpportunityID=opportunitys[0].id*/);
          // insert oppSplit;
            
        Database.Saveresult[] dbSave2 = Database.insert(oppShare, false);
        for (Database.Saveresult recSave : dbSave2) 
        {
            if (!recSave.isSuccess()) 
            {
                Database.Error eMsg = recSave.getErrors()[0];
            }
            
            
        }
        
        Test.startTest();        
         AllOpportunityTeamMemberTriggerHandler tm = new AllOpportunityTeamMemberTriggerHandler();
         tm.emailTeamMembers(oppTeam);
         tm.afterInsert(oppTeam,oppMap);
         tm.afterUpdate(oppTeam,oppMap,oppMap);
         tm.createOpportuntiySplit(oppTeam,oppMap,oppMap);
         
         
        Test.stopTest();
        List<OpportunitySplit> oppsplist = [select id,OppSplitType__c,CurrUserProfileName__c  from OpportunitySplit];
        system.assertEquals(oppsplist[0].CurrUserProfileName__c, 'System Administrator');
    }
    
    static testMethod void emailTeamMembersmethod2()    {
            
            SalesSpecialistRoleOppTeam__c salesspecial = new SalesSpecialistRoleOppTeam__c();
            salesspecial.name = 'Testsalesspecial';
            salesspecial.OpportunityTeam__c = 'SSRoleOppTeam' ;
            salesspecial.SSRole__c = 'Cloud - AU Sales Specialist' ;
            insert salesspecial;
                 
            List<Account> accounts = P2A_TestFactoryCls.getAccounts(1);
            List<Opportunity> opportunitys = P2A_TestFactoryCls.getOpportunitys(1,accounts);
          
            List<OpportunityTeamMember> oppTeam = new List<OpportunityTeamMember>(); 
            
            for (Opportunity opp : opportunitys)
                {
                    OpportunityTeamMember otm = new OpportunityTeamMember();
                    otm.OpportunityId = opp.Id;
                    otm.UserId = Userinfo.getuserId();
                    //otm.TeamMemberRole = Userinfo.getUserRoleId();
                    //otm.TeamMemberRole = 'Cross Country Application';
                    otm.TeamMemberRole = 'Opportunity Owner';                       
                    oppTeam.add(otm);
                }            
            Map<Id, OpportunityTeamMember> oppMap = new Map<Id, OpportunityTeamMember>();
            oppMap.put(oppTeam[0].id,oppTeam[0]);
            
            List<OpportunityShare> oppShare = new List<OpportunityShare>();
            if (oppTeam.size() > 0){
                
                    Database.Saveresult[] oppTeam_record = Database.insert(oppTeam, false);
                    for (Database.Saveresult recSave : oppTeam_record) 
                    {
                        if (recSave.isSuccess()) 
                            {
                                oppShare.add( new OpportunityShare(UserOrGroupID = oppTeam[0].UserId,
                                                        OpportunityId = oppteam[0].OpportunityId,
                                                        OpportunityAccessLevel = 'Edit'));
                            } 
                        else
                            {
                                Database.Error eMsg = recSave.getErrors()[0];
                            }
                        
                    }
                }
                
        //OpportunitySplit oppSplit = new OpportunitySplit(Total_Product_SOV__c=25.35,OpportunityID=opportunitys[0].id, Team_Role__c='sfdsdf', SplitPercentage_Product_SOV__c=54/*, OpportunityID=opportunitys[0].id*/);
          //insert oppSplit;
            ID splittypeidactive = [select DeveloperName,isactive,id from OpportunitySplitType where isactive=:true][0].id;
         OpportunitySplit oppSplit=new OpportunitySplit();
         
        oppSplit.SplitOwnerId=Userinfo.getuserId();
        //oppSplit.SplitAmount=oppSplitList[0].Opportunity.Reported_Total_SOV__c;
        oppSplit.SplitPercentage=0;
        oppSplit.SplitTypeid=splittypeidactive;
        oppSplit.OpportunityID = opportunitys[0].Id;
        oppSplit.Team_Role__c='Account Manager';
        oppSplit.IsGAMSplit__c=true;
        oppSplit.SplitAmount_Product_SOV__c=0;
        //insert oppSplit;
       // oppSplit.SplitPercentage=20;
        //update oppSplit;
        Database.Saveresult[] dbSave2 = Database.insert(oppShare, false);
        for (Database.Saveresult recSave : dbSave2) 
        {
            if (!recSave.isSuccess()) 
            {
                Database.Error eMsg = recSave.getErrors()[0];
            }
            
            
        }
        
        Test.startTest();
        Util.FlagNeedReopen=false;
         AllOpportunityTeamMemberTriggerHandler tm = new AllOpportunityTeamMemberTriggerHandler();
         tm.emailTeamMembers(oppTeam);
         tm.afterInsert(oppTeam,oppMap);
         tm.afterUpdate(oppTeam,oppMap,oppMap);
         tm.createOpportuntiySplit(oppTeam,oppMap,oppMap);
         
        Test.stopTest();
        List<OpportunitySplit> oppsplist = [select id,OppSplitType__c,CurrUserProfileName__c  from OpportunitySplit];
        system.assertEquals(oppsplist[0].OppSplitType__c , true);
        //List<OpportunityTeamMember> oppteam = [select id ,NoTeamUpdateRequired__c from ]
        //system.assertEquals();
    }
}