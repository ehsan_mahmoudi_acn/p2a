@isTest(SeeAllData = false)
public class TibcoControllerTest  {
    
public static List<csord__Order__c> Orderlist;
public static List<csord__Order_Request__c > OrdReqList;
public static string requestName = 'test request';
public static string orderType = 'test order';
public static TibcoOrderLine line;
public static string nodeName = 'test node';
public static UDF[] udfList;
//public static DOM.XMLNode parent = submitOrderRequest.addChildElement('orderRequest',ord, null);
public static DOM.XMLNode parent;


    private static testMethod void tibcoControllerTestMethod(){
        
        OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        Orderlist = P2A_TestFactoryCls.getorder(1, OrdReqList);
        
        CS_TestUtil.disableAll(UserInfo.getUserId());       
        Test.startTest();
        ID orderId = Orderlist[0].id; 
        System.assertEquals(orderId , Orderlist[0].id);

        String ID = Orderlist[0].id;
         Test.setMock(HttpCalloutMock.class, new WebServiceConnectorMock());
         
        //public Dom.XmlNode createRootElement('test', 'test', 'test');
        
        Dom.Document doc = new Dom.Document(); 
        parent = doc.createRootElement('name', 'namespace', 'prefix');

        //parent = Dom.Document.createRootElement('test', 'test', 'test');
        
        AmendOrder amendOrdObj = new AmendOrder();
        amendOrdObj.orderId =  orderId;
        AmendOrder.amendOrderToFOM(ID); 
        TibcoController Tibco = new TibcoController();
        TibcoServiceOrder order = new TibcoServiceOrder();
        TibcoServiceOrderCreator creator = new TibcoServiceOrderCreator();
        TibcoServiceOrder order1 = creator.CreateOrderFromServiceId(ID);
        line = new TibcoOrderLine();
        
        //parent = DOM.getParent();
        Tibco.CreateLineChild(parent, line, requestName, orderType);
        Tibco.CreateAddressChild(parent,nodeName);
        //Tibco.CreateAddressChild(parent,  nodeName);
        //Tibco.CreateUDFChild(Dom.XmlNode parent, UDF[] udfList);
        Test.stopTest();
    }
}