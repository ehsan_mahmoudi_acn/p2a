/*
Test class was converted as class - so re-created@16/2/2017
*/
@isTest(SeeAllData = false)
public class CS_GIDProductConfigLookupTest {

    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId = '';
    private static Id[] excludeIds = new List<Id>();
    private static Integer pageOffset, pageLimit;
    private static Object[] data = new List<Object>();
    
    private static List<cscfga__Product_Basket__c> productBasketList = new List<cscfga__Product_Basket__c>();
    private static List<cscfga__Product_Configuration__c> pcs = new List<cscfga__Product_Configuration__c>();
    private static List<cscfga__Product_Configuration__c> gidPcs = new List<cscfga__Product_Configuration__c>();
    private static List<cscfga__Product_Definition__c> pds = new List<cscfga__Product_Definition__c>(); 
    
    private static void initTestData(){
        
        productBasketList = P2A_TestFactoryCls.getProductBasket(1);
       // pcs = P2A_TestFactoryCls.getProductonfig(3);
      //  gidPcs = P2A_TestFactoryCls.getProductonfig(1);
      
       list<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> pcs = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
           List<cscfga__Product_Configuration__c> gidPcs = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
      
        pds = P2A_TestFactoryCls.getProductdef(1);
        
        for(cscfga__Product_Definition__c pd : pds){
            pd.Name = 'Internet-Onnet';
        }
        
        update pds;
        System.debug('*****Product defs: ' + pds);
          System.assertEquals('Internet-Onnet',pds[0].Name );
        
        for(cscfga__Product_Configuration__c pc : pcs){
            pc.cscfga__Product_Basket__c = productBasketList[0].Id;
            pc.cscfga__Product_Definition__c = pds[0].Id;
            pc.Product_Id__c = 'GIDSSTD';
            pc.GID_Product_Configuration__c = null;
        }
        
        pcs[0].GID_Product_Configuration__c = gidPcs[0].Id;
        
        searchFields.put('BasketId', productBasketList[0].Id);
        pageOffset = 0;
        pageLimit = 0;
        
        update pcs;
        System.debug('*****Product configs: ' + pcs); 
        System.assertEquals('GIDSSTD',pcs[0].Product_Id__c );     
    }
    
    static testMethod void gidProdConfigTest(){
    Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
      initTestData();
            
            CS_GIDProdConfigLookup gidProdConfigLookup = new CS_GIDProdConfigLookup();  
            String basket = gidProdConfigLookup.getRequiredAttributes();
            data = gidProdConfigLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            
            System.debug('*******Basket: ' + basket);
            System.debug('*******Data: ' + data);
              System.assert(data.size() > 0, '');
            Test.stopTest();
            
        } catch(Exception e){
            ee = e;
        } finally {
            //Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }
    }
    
    
        /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }
}