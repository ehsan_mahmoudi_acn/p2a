public with sharing class PricingApprovalExtCr1 
{
    public ApexPages.StandardController sc;    
    public Boolean isVisible;
    public Boolean isVisible1 {get; set;}
    public Boolean UsageVisibleUI {get; set;}
    public Boolean isLApprovedRC {get; Set;}
    public Boolean isLApprovedNRC {get; Set;}
    public string isLastApprovedRC {get; Set;}
    public string isLastApprovedNRC {get; Set;}    
    public decimal TRateRC {get; Set;}
    public decimal TRateNRC {get; Set;}
    
    public decimal TTargetRC {get; Set;}
    public decimal TTargetNRC {get; Set;}
    public decimal TRateBurstRate {get; Set;}
    public decimal TRateChinaBurstRate {get; Set;} 
    public decimal TUsageCharge {get; Set;}
    public decimal TTargetBurstRate {get; Set;}
    public decimal TTargetChinaBurstRate {get; Set;}
    public decimal TApprovedBurstRate {get; Set;}
    public decimal TApprovedChinaBurstRate {get; Set;}
    
    public decimal TOfferRC {get; Set;}
    public decimal TOfferNRC {get; Set;}
    public decimal TApprovedRC {get; Set;}
    public decimal TApprovedNRC {get; Set;}
    public decimal TLApprovedRC {get; Set;}
    public decimal TLApprovedNRC {get; Set;}
    public decimal TCostRC {get; Set;}
    public decimal TCostNRC {get; Set;}
    public decimal Tmargin {get; Set;}
    public decimal TDiscount {get; Set;}    
    public Pricing_Approval_Request_Data__c TDiscountval{get; set;}
    public Boolean selected {get; set;}
    public decimal GCapex {get; Set;}
    public decimal discountcostNRC {get; Set;}
    public Boolean pageRefresh {get; Set;}
    public Boolean makeAttibuteReadOnly {get;set;}
    public List<Pricing_Approval_Request_Data__c > pdata;
    Public List<case> cases = new list<case>();
    public Boolean IsAccessible;
    
   Public boolean getIsAccessible()
    {
        ID pID = UserInfo.getProfileID();
        Boolean checkVisibility = false;
        String nameP = system.label.Profile_Access_For_Pricing_Financial_Summary;
        String[] arrTest = nameP.split(',');
        //list<profile> pList = [Select id from profile where name = :arrTest];
        for (profile p : [Select id from profile where name = :arrTest]) 
        {
            if (p.id == pID)
            checkVisibility = true;
    }
        return checkVisibility;
    }

    public PricingApprovalExtCr1(ApexPages.StandardController sc) 
    {
        Case caseObj =(case)sc.getRecord();
        this.sc = sc;
        TDiscountval=new Pricing_Approval_Request_Data__c();
        //PricingFinancialSummarycls controller  = new PricingFinancialSummarycls( new ApexPages.StandardController(caseObj));
        //GCapex  = controller.CapexrandTotal;        
        if(CheckRecursive.PricingApprovalExtCr1grand()) {
         GrandTotal();
            
        case c = [select Product_Basket__r.quote_status__c, Pricing_Approval_Case__c from case where id = :sc.getId()];                       
                makeAttibuteReadOnly=(c.Product_Basket__r.quote_status__c=='Approved'|| ((userinfo.getProfileId()).contains(label.Profile_TI_Order_Desk)  && c.Pricing_Approval_Case__c !='Order Desk'))?true:false;               
    }
    }
   
    //ID csid=ApexPages.currentPage().getParameters().get('id');  
       
    
    public PageReference updateItems()
    {
        
        List<Pricing_Approval_Request_Data__c > priceApplst = new List<Pricing_Approval_Request_Data__c >();
        if(pc != null)
        {
            for( PricingApprovalWrapper obj : pc) 
            {
                priceApplst.add(obj.priceObj);
            }
            
            try
            {
                if(pdata!=null && pdata.size()>0){update pdata;}
            }
            catch(exception e){
            ErrorHandlerException.ExecutingClassName='PricingApprovalExtCr1:updateItems';
            ErrorHandlerException.sendException(e);
            }
            update priceApplst;
            GrandTotal();
            Case caseObj =(case)sc.getRecord();
            PricingFinancialSummarycls controller  = new PricingFinancialSummarycls( new ApexPages.StandardController(caseObj));
            caseObj.TCV__c = controller.revenueGrandTotal;
            caseObj.Margin_Value__c = controller.grossmarginPerGrandTotal;          
            update caseObj;
        }
        pc = null;
        pageRefresh = true;
        return null;
    }
    
    public PricingApprovalWrapper[] pc
    {
        get 
        {
            if (pc == null) 
            { 
                case r = [
                    select Product_Basket__r.ID, Order_Type__c
                    from case
                    where id = :sc.getId()
                ];
                Id ppmId = r.Product_Basket__r.ID;
                if(r.Order_Type__c != null && !r.Order_Type__c.equalsignorecase('new')){
                    isLApprovedRC = False;
                    isLApprovedNRC = False;
                }
                else{                      
                    isLApprovedRC = True;
                    isLApprovedNRC = True;
                } 
                
                pc =  new List<PricingApprovalWrapper>();
                UsageVisibleUI = true;
                if(CheckRecursive.PricingApprovalExtCr1Class()) {
                for (Pricing_Approval_Request_Data__c obj : [
                    select Product_basket__c,Product_Configuration__r.Hierarchy_new__c,Product_Configuration__r.Hierarchy_Sequence_Key__c, Product_Configuration__c,Product_Configuration__r.cscfga__Product_Family__c, Product_Configuration__r.Offnet_Product__c,
                    Product_Configuration__r.Name,Product_Configuration__r.Internet_Onnet_BillingType__c, Rate_RC__c, Rate_NRC__c,Product_Configuration__r.Product_Definition_Name__c, Offer_RC__c,Last_Approved_NRC__c, Is_Offnet__c, Product_Information__c,
                    Offer_NRC__c, Approved_RC__c, Approved_NRC__c, Cost_RC__c, Cost_NRC__c, Last_Approved_RC__c, Discount_Approved_RC__c,
                    Margin__c, Contract_Term__c,Remaining_contract_term__c,Product_Configuration__r.change_in_contract__c,Rate_Burst_Rate__c, Rate_China_Burst_Rate__c, Target_Burst_Rate__c, Target_China_Burst_Rate__c,
                    Approved_Burst_Rate__c, Approved_China_Burst_Rate__c, Usage_Charge__c
                    from Pricing_Approval_Request_Data__c
                    where Product_Basket__c = :ppmId and Product_Configuration__c!= null order by Product_Configuration__r.Hierarchy_Sequence_Key__c  /*and (NOT Product_Configuration__r.cscfga__Product_Definition__r.Name like 'VLAN Group%')
                        and (NOT Product_Configuration__r.cscfga__Product_Definition__r.Name like 'Master VPLS Service%') and (NOT Product_Configuration__r.cscfga__Product_Definition__r.Name like 'Master IPVPN Service%')
                        and (NOT Product_Configuration__r.cscfga__Product_Definition__r.Name like 'Standalone ASBR%') and (NOT Product_Configuration__r.cscfga__Product_Definition__r.Name like 'ASBR%')
                    and (NOT Product_Configuration__r.cscfga__Product_Definition__r.Name like 'NID%') */])
                    {
                        if(obj.Offer_NRC__c != null)  obj.Offer_NRC__c = obj.Offer_NRC__c.setscale(2);
                        if(obj.Rate_NRC__c !=null)    obj.Rate_NRC__c  = obj.Rate_NRC__c.setscale(2);
                        if(obj.Rate_RC__c!=null)      obj.Rate_RC__c   = obj.Rate_RC__c.setscale(2);
                        if(obj.Offer_RC__c != null)   obj.Offer_RC__c = obj.Offer_RC__c.setscale(2);
                        if(obj.Offer_NRC__c!= null)   obj.Offer_NRC__c = obj.Offer_NRC__c.setscale(2);
                        if(obj.Last_Approved_NRC__c != null) obj.Last_Approved_NRC__c = obj.Last_Approved_NRC__c.setscale(2);
                        if(obj.Approved_RC__c != null) obj.Approved_RC__c = obj.Approved_RC__c.setscale(2);
                        if(obj.Approved_NRC__c != null) obj.Approved_NRC__c = obj.Approved_NRC__c.setscale(2);
                        if(obj.Usage_Charge__c != null) obj.Usage_Charge__c = obj.Usage_Charge__c.setscale(2);
                         if(obj.Product_Configuration__r.cscfga__Product_Family__c == 'Remote Eyes And Hands'){
                            UsageVisibleUI = False; 
                        }
                        if(obj.Cost_RC__c!=null)       obj.Cost_RC__c = obj.Cost_RC__c.setscale(2);
                        if(obj.Cost_NRC__c!=null)      obj.Cost_NRC__c = obj.Cost_NRC__c.setscale(2);
                        if(obj.Last_Approved_RC__c!= null) obj.Last_Approved_RC__c= obj.Last_Approved_RC__c;
                        if(obj.Discount_Approved_RC__c!= null)  obj.Discount_Approved_RC__c = obj.Discount_Approved_RC__c.setscale(2);
                        if(obj.Rate_Burst_Rate__c != null)      obj.Rate_Burst_Rate__c =  obj.Rate_Burst_Rate__c.setscale(2);
                        if(obj.Rate_China_Burst_Rate__c!= null) obj.Rate_China_Burst_Rate__c = obj.Rate_China_Burst_Rate__c.setscale(2);
                        if(obj.Target_Burst_Rate__c != null)    obj.Target_Burst_Rate__c = obj.Target_Burst_Rate__c.setscale(2); 
                        if(obj.Target_China_Burst_Rate__c != null) obj.Target_China_Burst_Rate__c =obj.Target_China_Burst_Rate__c.setscale(2);
                        obj.Discount_Approved_RC__c=TDiscountval.Discount_Approved_RC__c;
                        PricingApprovalWrapper  wrappObj =  new PricingApprovalWrapper (obj,false);
                        wrappObj.disable  = (obj.Product_Configuration__r.Offnet_Product__c==''|| obj.Product_Configuration__r.Offnet_Product__c=='No' );
                        wrappObj.fieldReadOnly = isFieldReadOnly(obj);
                        pc.add(wrappObj);
                        TDiscountval.Discount_Approved_RC__c=obj.Discount_Approved_RC__c ==null ? 0 : obj.Discount_Approved_RC__c;
                    }
                    }
                    /* 15002 based on this defect changed the code
                        List<Pricing_Approval_Request_Data__c> obj = [select Product_Configuration__c, Product_Configuration__r.cscfga__Product_Family__c, Product_Configuration__r.Name
                        from Pricing_Approval_Request_Data__c
                        where Product_Basket__c =:ppmid and Product_Configuration__r.cscfga__Product_Family__c IN ('TWI Multihome', 'Internet-Onnet', 'IPT Multihome', 'IPVPN Port', 'VPLS VLAN Port', 'VPLS Transparent')];
                        //{
                        if(obj.size() >0)
                        {
                        isVisible1 = false;
                        }
                        else
                        {
                        isVisible1 = true;
                        }
                        //}
                    Comment end */
                    boolean isBurstable = false;
                    if(CheckRecursive.PricingApprovalExtCr()) {
                    List<cscfga__Attribute__c> attlist = [select cscfga__Product_Configuration__c, name, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__r.cscfga__Product_Basket__c =:ppmid and (name = 'VLAN rating type' or name = 'Port Rating Type' or name = 'Rating type' or name = 'Port Rating')];
                    
                    if(!attlist.Isempty())
                    {
                        for(cscfga__Attribute__c att : attlist)
                        {
                            if(att.cscfga__Value__c != null)
                            {
                                if(!att.cscfga__Value__c.contains('Flat'))
                                {
                                    isBurstable = true;
                                    system.debug('rating=>'+att.cscfga__Value__c);
                                }
                            }
                        }
                    }
                    }
                    if(!isBurstable){
                        isVisible1 = true;
                    }
                    else
                    {
                        isVisible1 = false;
                    }
                    system.debug('isVisible1=>'+isVisible1+' isLApprovedRC=>'+isLApprovedRC);
            }
             //NC - set value of read only fields to 0.00
            for(PricingApprovalWrapper paw :pc){
                if(paw.fieldReadOnly){
                    paw.priceObj.Cost_RC__c = 0.00;
                    paw.priceObj.Cost_NRC__c = 0.00;
                    paw.priceObj.Approved_RC__c = 0.00;
                    paw.priceObj.Approved_NRC__c = 0.00;    
                    paw.priceobj.Usage_Charge__c = 0.00;
                    paw.priceObj.Approved_Burst_Rate__c = 0.00;
                    paw.priceObj.Approved_China_Burst_Rate__c = 0.00;
                }
            }
            
            //end
            return pc;
        }
        private set;
    }
    public void grandTotal()
    {
        TRateRC = 0; TRateNRC = 0; TOfferRC = 0; TOfferNRC = 0; TApprovedRC = 0; TApprovedNRC = 0; TCostRC = 0; TCostNRC = 0; TLApprovedRC = 0; TLApprovedNRC = 0;  discountcostNRC = 0;
        TUsageCharge = 0;
        TRateBurstRate = 0; TRateChinaBurstRate = 0; TTargetBurstRate = 0; TTargetChinaBurstRate = 0; TApprovedBurstRate = 0; TApprovedChinaBurstRate = 0; Tmargin = 0;
        GCapex   =  GCapex   == null ? 0 :    gcapex ; 
        Decimal TApprovedMargin = 0.00, TCostMargin = 0.00; 
        
        if(Pc.size()>0)
        {
            for(integer i     =0; i<pc.size(); i++)
            {
                TRateRC+=pc[i].priceObj.Rate_RC__c                                                   == null ? 0 :pc[i].priceObj.Rate_RC__c;
                
                TRateNRC+=PC[i].priceObj.Rate_NRC__c                                                 == null ? 0 :PC[i].priceObj.Rate_NRC__c;
                
                TOfferRC+=Pc[i].priceObj.Offer_RC__c                                                 == null ? 0 :Pc[i].priceObj.Offer_RC__c;
                
                TOfferNRC+=Pc[i].priceObj.Offer_NRC__c                                               == null ? 0 :Pc[i].priceObj.Offer_NRC__c;
                
                TApprovedRC+=Pc[i].priceObj.Approved_RC__c                                           == null ? 0 :Pc[i].priceObj.Approved_RC__c;
                
                TApprovedNRC+=Pc[i].priceObj.Approved_NRC__c                                         == null ? 0 :Pc[i].priceObj.Approved_NRC__c;
                TUsageCharge+=Pc[i].priceObj.Usage_Charge__c                                         == null ? 0 :Pc[i].priceObj.Usage_Charge__c;
                
                TCostRC+=Pc[i].priceObj.Cost_RC__c                                                  == null ? 0 :Pc[i].priceObj.Cost_RC__c;
                
                TCostNRC+=Pc[i].priceObj.Cost_NRC__c                                                 == null ? 0 :Pc[i].priceObj.Cost_NRC__c;
                
                TLApprovedRC+=Pc[i].priceObj.Last_Approved_RC__c                                     == null ? 0 :Pc[i].priceObj.Last_Approved_RC__c;
                
                TLApprovedNRC+=Pc[i].priceObj.Last_Approved_NRC__c                                   == null ? 0 :Pc[i].priceObj.Last_Approved_NRC__c;
                
                TRateBurstRate+=pc[i].priceObj.Rate_Burst_Rate__c                                    == null ? 0 :pc[i].priceObj.Rate_Burst_Rate__c;
                
                TRateChinaBurstRate+=pc[i].priceObj.Rate_China_Burst_Rate__c                         == null ? 0 :pc[i].priceObj.Rate_China_Burst_Rate__c;
                
                TTargetBurstRate+=pc[i].priceObj.Target_Burst_Rate__c                                == null ? 0 :pc[i].priceObj.Target_Burst_Rate__c;
                
                TTargetChinaBurstRate+=pc[i].priceObj.Target_China_Burst_Rate__c                     == null ? 0 :pc[i].priceObj.Target_China_Burst_Rate__c;
                
                TApprovedBurstRate+=pc[i].priceObj.Approved_Burst_Rate__c                            == null ? 0 :pc[i].priceObj.Approved_Burst_Rate__c;
                
                TApprovedChinaBurstRate+=pc[i].priceObj.Approved_China_Burst_Rate__c                 == null ? 0 :pc[i].priceObj.Approved_China_Burst_Rate__c;
                
               if(Pc[i].priceObj.Product_Configuration__r.change_in_contract__c!=null && Pc[i].priceObj.Product_Configuration__r.change_in_contract__c=='No'){ 
                TApprovedMargin+=(((Pc[i].priceObj.Approved_RC__c == null ? 0 :Pc[i].priceObj.Approved_RC__c) * (Pc[i].priceObj.Remaining_Contract_Term__c == null ? 0 :Pc[i].priceObj.Remaining_Contract_Term__c)) + (Pc[i].priceObj.Approved_NRC__c == null ? 0 :Pc[i].priceObj.Approved_NRC__c));

               TCostMargin+=(((Pc[i].priceObj.Cost_RC__c == null ? 0 :Pc[i].priceObj.Cost_RC__c) * (Pc[i].priceObj.Remaining_Contract_Term__c == null ? 0 :Pc[i].priceObj.Remaining_Contract_Term__c)) + (Pc[i].priceObj.Cost_NRC__c == null ? 0 :Pc[i].priceObj.Cost_NRC__c)); 
               
               }else{
               TApprovedMargin+=(((Pc[i].priceObj.Approved_RC__c == null ? 0 :Pc[i].priceObj.Approved_RC__c) * (Pc[i].priceObj.Contract_Term__c == null ? 0 :Pc[i].priceObj.Contract_Term__c)) + (Pc[i].priceObj.Approved_NRC__c == null ? 0 :Pc[i].priceObj.Approved_NRC__c));

               TCostMargin+=(((Pc[i].priceObj.Cost_RC__c == null ? 0 :Pc[i].priceObj.Cost_RC__c) * (Pc[i].priceObj.Contract_Term__c == null ? 0 :Pc[i].priceObj.Contract_Term__c)) + (Pc[i].priceObj.Cost_NRC__c == null ? 0 :Pc[i].priceObj.Cost_NRC__c)); 
              
              }

            }
            TApprovedRC = (TApprovedRC-TDiscountval.Discount_Approved_RC__c).setscale(2);
            // TCostNRC = (TCostNRC + gcapex).setscale(2);
            TCostNRC = TCostNRC.setscale(2);
            if(TApprovedMargin != 0 && TCostMargin != 0)
            {
                Tmargin = ((100*(TApprovedMargin - TCostMargin)/TApprovedMargin)).setscale(2);
            }
        }
        
    } 
    public class PricingApprovalWrapper 
    {
        public Boolean selectRecord {get;set;}
        public Pricing_Approval_Request_Data__c priceObj{get;set;}
        public Boolean disable {get;set;}
        Public  decimal targetrc {get;set;}       
        //CR52 grey out Pricing fields   
        public Boolean fieldReadOnly{get;set;} 
        //CR52 grey out Pricing fields  
        public PricingApprovalWrapper(Pricing_Approval_Request_Data__c  obj,Boolean selectRecord)
        {
            priceObj = obj;
            this.selectRecord  = selectRecord;
            disable  = false;
            fieldReadOnly = false;
        }          
    }
     /*
    * Nikola Culej - July 24, 2017
    * get the list of product configs that should be grayed out
    */
    
    public Boolean isFieldReadOnly(Pricing_Approval_Request_Data__c pard){
        Boolean retValue = false;
 
    if(pard.Product_Configuration__r.Product_Definition_Name__c == 'Master IPVPN Service'){
        retValue = true;
    } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'ASBR'){
        if(pard.Product_Configuration__r.Name.startsWith('Standalone ASBR')){
            retValue = false;
        } else if (pard.Product_Configuration__r.Name.startsWith('ASBR')){
            retValue = true;
        }
    }else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'ASBR (GIE)' && pard.Product_Configuration__r.Product_Code__c == 'ASBRB'){
        retValue = true;
    } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'Master VPLS Service'){
        retValue = true;
    } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'VLAN Group'){
        retValue = true;
    } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'TWI Singlehome'){
        retValue = true;
    } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'IPT Singlehome'){
        retValue = true;
    } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'Internet-Onnet' && (pard.Product_Configuration__r.Name == 'IPT' || pard.Product_Configuration__r.Name == 'TWI') && (pard.Product_Configuration__r.Internet_Onnet_BillingType__c != null && pard.Product_Configuration__r.Internet_Onnet_BillingType__c == 'Aggregated')){
        retValue = true;
    } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'EVP Port'){
        retValue = true;
    } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'GVOIP'){
        retValue = true;
    } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'Customer Site'){
        retValue = true;
    } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'IPC'){
        retValue = true;
    } else if(pard.Product_Configuration__r.Product_Definition_Name__c == 'Colocation'){
        retValue = true;
    }
    // PB-218 Update the code to grey out the Local loop line item in the 'Product Configuration Details'
    // section of the Pricing Approval Case and default the price to 0 when 'Local Access Organized By'
    // attribute value is 'Customer'.
    else if (pard.Product_Configuration__r.Product_Definition_Name__c == 'Local Access' &&
        pard.Product_Information__c.contains('Local access organized by:Customer')) {
        retValue = true;
    }

        return retValue;
    }
    // END Nikola Culej - July 24, 2017
    Public boolean getisVisible(){
        ID pID = UserInfo.getProfileID();
        Boolean checkVisibility = false;
        //Bhargav -- added TI Account Manager to the query parameter below -- 28 June 2016
        String nameP = system.label.Profile_Access_For_Pricing_Approval;
        String[] arrTest = nameP.split(',');
        //list<profile> pList = [Select id from profile where name = :arrTest];
        for (profile p : [Select id from profile where name = :arrTest]) {
            if (p.id == pID)
            checkVisibility = true;
        }
        return checkVisibility;
    }     
}