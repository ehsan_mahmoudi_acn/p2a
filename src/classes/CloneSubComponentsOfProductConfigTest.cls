@isTest(SeeAllData = false)
public class CloneSubComponentsOfProductConfigTest{

    private static Id sampleBasket(){
        P2A_TestFactoryCls.sampleTestData();        
        
        /** Insert product definition object record **/
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.name = 'Master IPVPN Service';
        pd.cscfga__Description__c ='Test master IPVPN';
        insert pd;
        system.assertEquals(pd.name , 'Master IPVPN Service');

        cscfga__Product_Definition__c pd1 = new cscfga__Product_Definition__c();
        pd1.name = 'Master VPLS Service';
        pd1.cscfga__Description__c ='Test master VPLS';
        insert pd1;
        system.assertEquals(pd1.name , 'Master VPLS Service');
        /** Insert custom setting records **/
        Product_Definition_Id__c pdIdCustomSetting = new Product_Definition_Id__c();
        pdIdCustomSetting.Name = 'Master_IPVPN_Service_Definition_Id';
        pdIdCustomSetting.Product_Id__c =pd.Id;
        insert pdIdCustomSetting;
        system.assertEquals(pdIdCustomSetting.Name , 'Master_IPVPN_Service_Definition_Id');

        Product_Definition_Id__c pdIdCustomSetting1 = new Product_Definition_Id__c();
        pdIdCustomSetting1.Name = 'Master_VPLS_Service_Definition_Id';
        pdIdCustomSetting1.Product_Id__c =pd1.Id;
        insert pdIdCustomSetting1;
        system.assertEquals(pdIdCustomSetting1.Name , 'Master_VPLS_Service_Definition_Id');
             
        Product_Definition_Id__c setting = new Product_Definition_Id__c();
        setting.Name = 'VLANGroup_Definition_Id';
        insert setting;
        system.assertEquals(setting.Name , 'VLANGroup_Definition_Id');
        string vlanGroupProductDefinitionId = '';
        vlanGroupProductDefinitionId = Product_Definition_Id__c.getvalues('VLANGroup_Definition_Id').Product_Id__c;
        
        List<cscfga__Product_Basket__c> productBasket = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Basket__c> productBasket1 = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> productDefinition = P2A_TestFactoryCls.getProductdef(1);
        List<Account> accountList = P2A_TestFactoryCls.getAccounts(1);      
        List<Opportunity> opportunityList = P2A_TestFactoryCls.getOpportunitys(1, accountList);
        List<cscfga__Product_Bundle__c> productBundleList = P2A_TestFactoryCls.getProductBundleHdlr(1, opportunityList);
        List<cscfga__Configuration_Offer__c> offerList = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> productConfigList = P2A_TestFactoryCls.getProductonfig(1, productBasket, productDefinition, productBundleList, offerList);
        List<cscfga__Product_Configuration__c> productConfigList1 = P2A_TestFactoryCls.getProductonfig(1, productBasket1, productDefinition, productBundleList, offerList);
        List<cscfga__Configuration_Screen__c> conigScreen = P2A_TestFactoryCls.getConfigScreen(1, productDefinition);
        List<cscfga__Screen_Section__c> screenSection = P2A_TestFactoryCls.getScreenSec(1, conigScreen);
        List<cscfga__Attribute_Definition__c > attributeDefinitionList = P2A_TestFactoryCls.getAttributesdef(2, productConfigList, productDefinition, conigScreen, screenSection);
        List<cscfga__Attribute__c> attributeList = P2A_TestFactoryCls.getAttributes(2, productConfigList, attributeDefinitionList);
        List<csbb__Product_Configuration_Request__c> productConfigReqList = P2A_TestFactoryCls.getProductonfigreq(1, getProductCategory());
        P2A_TestFactoryCls.getProductonfigappreq(2, productBasket, productConfigList);
        
        if(!productConfigList.IsEmpty()){
            productConfigList[0].cscfga__Parent_Configuration__c = productConfigList1[0].Id;
            productConfigList[0].cscfga__Root_Configuration__c = productConfigList1[0].Id;
            update productConfigList;
        }
        if(!attributeDefinitionList.IsEmpty()){
            attributeDefinitionList[0].cscfga__Type__c = 'Related Product';
            update attributeDefinitionList;
        }
        if(!attributeList.IsEmpty()){
            attributeList[0].cscfga__Value__c = productConfigList1[0].Id;
            update attributeList;
        }       
        if(!productConfigReqList.IsEmpty()){
            productConfigReqList[0].csbb__Product_Configuration__c = productConfigList[0].Id;
            productConfigReqList[0].csbb__Product_Basket__c = productBasket[0].Id;
            update productConfigReqList;
        }
        return productBasket[0].Id;
    }
    
    private static Id sampleBasket1(){
        P2A_TestFactoryCls.sampleTestData();
        
        /** Insert product definition object record **/
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.name = 'Master IPVPN Service';
        pd.cscfga__Description__c ='Test master IPVPN';
        insert pd;
        system.assertEquals(pd.name , 'Master IPVPN Service');

        cscfga__Product_Definition__c pd1 = new cscfga__Product_Definition__c();
        pd1.name = 'Master VPLS Service';
        pd1.cscfga__Description__c ='Test master VPLS';
        insert pd1;

        /** Insert custom setting records **/
        Product_Definition_Id__c pdIdCustomSetting = new Product_Definition_Id__c();
        pdIdCustomSetting.Name = 'Master_IPVPN_Service_Definition_Id';
        pdIdCustomSetting.Product_Id__c =pd.Id;
        insert pdIdCustomSetting;
        system.assertEquals(pdIdCustomSetting.Name , 'Master_IPVPN_Service_Definition_Id');

        Product_Definition_Id__c pdIdCustomSetting1 = new Product_Definition_Id__c();
        pdIdCustomSetting1.Name = 'Master_VPLS_Service_Definition_Id';
        pdIdCustomSetting1.Product_Id__c =pd1.Id;
        insert pdIdCustomSetting1;
        system.assertEquals(pdIdCustomSetting1.Name , 'Master_VPLS_Service_Definition_Id');
             
        Product_Definition_Id__c setting = new Product_Definition_Id__c();
        setting.Name = 'VLANGroup_Definition_Id';
        insert setting;
        string vlanGroupProductDefinitionId = '';
        vlanGroupProductDefinitionId = Product_Definition_Id__c.getvalues('VLANGroup_Definition_Id').Product_Id__c;

        List<cscfga__Product_Basket__c> productBasket = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> productDefinition = P2A_TestFactoryCls.getProductdef(1);
        List<Account> accountList = P2A_TestFactoryCls.getAccounts(1);      
        List<Opportunity> opportunityList = P2A_TestFactoryCls.getOpportunitys(1, accountList);
        List<cscfga__Product_Bundle__c> productBundleList = P2A_TestFactoryCls.getProductBundleHdlr(1, opportunityList);
        List<cscfga__Configuration_Offer__c> offerList = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> productConfigList = P2A_TestFactoryCls.getProductonfig(2, productBasket, productDefinition, productBundleList, offerList);
        List<cscfga__Configuration_Screen__c> conigScreen = P2A_TestFactoryCls.getConfigScreen(1, productDefinition);
        List<cscfga__Screen_Section__c> screenSection = P2A_TestFactoryCls.getScreenSec(1, conigScreen);
        List<cscfga__Attribute_Definition__c > attributeDefinitionList = P2A_TestFactoryCls.getAttributesdef(2, productConfigList, productDefinition, conigScreen, screenSection);
        List<cscfga__Attribute__c> attributeList = P2A_TestFactoryCls.getAttributes(2, productConfigList, attributeDefinitionList);
        List<csbb__Product_Configuration_Request__c> productConfigReqList = P2A_TestFactoryCls.getProductonfigreq(1, getProductCategory());
        P2A_TestFactoryCls.getProductonfigappreq(2, productBasket, productConfigList);
        
        if(!productConfigList.IsEmpty()){
            productConfigList[0].cscfga__Parent_Configuration__c = productConfigList[1].Id;
            productConfigList[0].cscfga__Root_Configuration__c = productConfigList[1].Id;
            update productConfigList;
        }
        if(!attributeDefinitionList.IsEmpty()){
            attributeDefinitionList[0].cscfga__Type__c = 'Related Product';
            update attributeDefinitionList;
        }
        if(!attributeList.IsEmpty()){
            attributeList[0].cscfga__Value__c = productConfigList[1].Id;
            update attributeList;
        }       
        if(!productConfigReqList.IsEmpty()){
            productConfigReqList[0].csbb__Product_Configuration__c = productConfigList[0].Id;
            productConfigReqList[0].csbb__Product_Basket__c = productBasket[0].Id;
            update productConfigReqList;
        }
        return productBasket[0].Id;
    }   

    private static List<cscfga__Product_Category__c> getProductCategory(){
        List<cscfga__Product_Category__c> productCategoryList = new List<cscfga__Product_Category__c>();
        cscfga__Product_Category__c PC = new cscfga__Product_Category__c();
        PC.Name = 'Sample';
        productCategoryList.add(PC);
        insert productCategoryList;
        system.assert(productCategoryList!=null);
        return productCategoryList;
    }
    
    private static Map<Id, csord__Service__c> serviceMap(){
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> order = P2A_TestFactoryCls.getorder(1, OrdReqList);

        Map<Id, csord__Service__c> serviceList = new Map<Id, csord__Service__c>();
        List<csord__Service__c> insertService = new List<csord__Service__c>();
        for(integer i=0;i<3;i++){           
            csord__Service__c s = new csord__Service__c();
            s.Name = 'Test Service'; 
            s.csord__Identification__c = 'Test-Catlyne-4238362';
            s.csord__Order_Request__c = OrdReqList[0].Id;
            s.csord__Subscription__c = SUBList[0].Id;
            s.csord__Order__c = order[0].Id;
            s.Billing_Commencement_Date__c = System.Today();
            s.Stop_Billing_Date__c = System.Today();
            s.RAG_Status_Red__c = false ; 
            s.RAG_Reason_Code__c = '';          
            s.Bundle_Flag__c = false;
            s.Product_Id__c = 'ATM';
            s.Product_Code__c = 'ATM';
            s.Inventory_Status__c = Label.PROVISIONED;
            s.Cease_Service_Flag__c = true;          
            insertService.add(s);
        }
        insert insertService;
        system.assert(insertService!=null);
        for(csord__Service__c service :insertService){
            serviceList.put(service.Id, service);           
        }
        return serviceList;
    }

    private static testMethod void testMainLogic(){
        Test.StartTest();
        Id basketId = sampleBasket();
        Map<Id, csord__Service__c> serviceList = serviceMap();
        cloneSubComponentsOfProductConfig testBatch = new cloneSubComponentsOfProductConfig(basketId, basketId, serviceList);
        Database.executeBatch(testBatch,1);
        system.assertEquals(true,testBatch!=null); 
        Test.StopTest();        
    }
    
    private static testMethod void testBatchSizeValidation(){
        try{
            Test.StartTest();
            Id basketId = sampleBasket1();
            Map<Id, csord__Service__c> serviceList = serviceMap();
            cloneSubComponentsOfProductConfig testBatch = new cloneSubComponentsOfProductConfig(basketId, basketId, serviceList);
            Database.executeBatch(testBatch,2);
            system.assertEquals(true,testBatch!=null);
            Test.StopTest();
        } 
        catch(Exception e) 
        {
        ErrorHandlerException.ExecutingClassName='cloneSubComponentsOfProductConfigTest:testBatchSizeValidation';         
        ErrorHandlerException.sendException(e); 
        }
    }

    private static testMethod void testNullBasketValidation(){
        try{        
            Test.StartTest();
            Id basketId = sampleBasket1();
            Map<Id, csord__Service__c> serviceList = serviceMap();
            cloneSubComponentsOfProductConfig testBatch = new cloneSubComponentsOfProductConfig(basketId, null, serviceList);
            Database.executeBatch(testBatch,1);
            system.assertEquals(true,testBatch!=null);
            Test.StopTest();
        } catch(Exception e) 
        {
        ErrorHandlerException.ExecutingClassName='cloneSubComponentsOfProductConfigTest:testNullBasketValidation';         
        ErrorHandlerException.sendException(e); 
        }
    }
}