global with sharing class CalloutResponseLLInternetAvailCheck extends csbb.CalloutResponseManagerExt {

  global CalloutResponseLLInternetAvailCheck (Map<String, csbb.CalloutResponse> mapCR, csbb.ProductCategory productCategory, csbb.CalloutProduct.ProductResponse productResponse) {
    system.debug('******** CalloutResponseLLInternetAvailCheck ******');
    this.setData(mapCR, productCategory, productResponse);

  }
  global CalloutResponseLLInternetAvailCheck () {
  }

  global void setData(Map<String, csbb.CalloutResponse> mapCR, csbb.ProductCategory productCategory, csbb.CalloutProduct.ProductResponse productResponse) {

    this.service = 'LLInternetAvailCheck'; 
    this.productCategoryId = productCategory.productCategoryId; 
    this.mapCR = mapCR;
    this.productCategory = productCategory; 
    this.productResponse = productResponse; 
    this.setPrimaryCalloutResponse();

  }

  global Map<String, Object> processResponseRaw (Map<String, Object> inputMap) {
    return new Map<String, Object>();
  }

  global Map<String, Object> getDynamicRequestParameters (Map<String, Object> inputMap) {
    return new Map<String, Object>();
  }

  global void runBusinessRules (String categoryIndicator) { 
    system.debug('******** LocalLoopAvailabilityCheck ****** categoryIndicator - '+categoryIndicator);
    this.productResponse.displayMessage = 'Sample message from LocalLoopAvailabilityCheck'; 
    this.productResponse.available = 'true';
    String resultJson = csbb.CalloutDisplay.takeString(crPrimary, 'Envelope.Body.doWorkResponse.result');
    
    Map<String, String> resultMap = (Map<String, String>)JSON.deserialize(resultJson, Map<String, String>.class);
    Id objectId = resultMap.get('id');

    if(objectId.getSobjectType() == Schema.Local_Access_Rate_Card__c.SObjectType){
      this.crPrimary.mapDynamicFields.put('id', resultMap.get('id'));
      this.crPrimary.mapDynamicFields.put('city', resultMap.get('city'));  
      this.crPrimary.mapDynamicFields.put('country', resultMap.get('country'));
      this.crPrimary.mapDynamicFields.put('pop', resultMap.get('pop'));
    }
    system.debug('******** LocalLoopAvailabilityCheck ****** this.crPrimary.mapDynamicFields - '+this.crPrimary.mapDynamicFields);
  }

  global csbb.Result canOffer (Map<String, String> attMap, Map<String, String> responseFields, csbb.CalloutProduct.ProductResponse productResponse) {

    csbb.Result canOfferResult = new csbb.Result(); 
    canOfferResult.status = 'OK';
    
    return canOfferResult;
  }

}