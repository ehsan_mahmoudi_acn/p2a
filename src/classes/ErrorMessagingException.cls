public class ErrorMessagingException extends Exception{

public static boolean isMessagingEnabled(){
try{
ErrorHandlerMechanism__c ehm=ErrorHandlerMechanism__c.getInstance('Logger');
if(ehm.isUserMessagingEnabled__c)return true;
else return false;
}catch(Exception e){
return false;
}
}

/*
public static string getErrorCode(Exception ex){

if(isMessagingEnabled()){

try{
Exception_Setting__c eset=Exception_Setting__c.getInstance(ex.getTypeName());
if(eset.isEnabled__c){
return eset.errorCode__c;
}
}catch(Exception e){
return null;
}


}

return null;

}
*/

public static void showError(Exception ex,SObject obj){


if(isMessagingEnabled() && !System.isBatch() && !System.isFuture() && !System.isScheduled()){
    Exception_Setting__c eset=Exception_Setting__c.getInstance(ex.getTypeName());
    if(ApexPages.currentPage()!=null && eset.isEnabled__c){
        String tempErr='Error Has Occurred : '+eset.errorCode__c;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,tempErr));

    }
    else if(obj!=null && eset.isEnabled__c /* && Trigger.isExecuting()*/){
        obj.addError(ex.getMessage());
    }
}

}
 
}