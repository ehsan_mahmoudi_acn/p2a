@isTest(SeeAllData = false)
private class GIAAS_UtilsTest{
    private static Map<String, String> pricingParameters= new Map<String, String>();
    private static List<GIAAS_Rate_Card__c>Ratecardlist;
    private static List<GIAAS_Plan_Description__c >Plandescriptionlist; 
    private static string productId;
    private static GIAAS_Rate_Card__c getStoragePrice;  

     @istest
     private static void initTestData(){

        String [] params = new List<string>();
        Ratecardlist = new List<GIAAS_Rate_Card__c>{
                          new GIAAS_Rate_Card__c(NRC__c = 700,MRC__c = 200,NRC_Cost__c =750,MRC_Cost__c =100,Product__c ='Storage',Storage_Type__c = 'test storage')
        };
        insert Ratecardlist;  
        system.assertEquals(true,Ratecardlist!=null);     


        for(GIAAS_Rate_Card__c Giasratcar:Ratecardlist) {
         params.add(Giasratcar.Field_String_1__c);
         pricingParameters.put(Giasratcar.Product__c,Giasratcar.Product__c);
        }

        productId = Ratecardlist[0].Product__c; 

        Plandescriptionlist = new List<GIAAS_Plan_Description__c>{
                             new GIAAS_Plan_Description__c (Internet_Data_Usage__c ='abc1',IP_Addresses__c ='dcf121',
                             Operating_system__c = 'afef342cf',Plan_Name__c ='test plan',Plan_Name_Code__c ='code',Server_Memory__c = 'memory',Service_CPU__c ='service',Storage__c = 'Storage'  )
        };
        insert Plandescriptionlist;
        system.assertEquals(true,Plandescriptionlist!=null); 
        }

        @istest
        private static void unitTest1(){
        initTestData();
        
        String [] params = new List<string>();     

        for(GIAAS_Rate_Card__c Giasratcar:Ratecardlist) {
         params.add(Giasratcar.Field_String_1__c);
         pricingParameters.put('product',Giasratcar.Product__c);
        }
        system.assertEquals(true,params!=null); 
        GIAAS_Utils.getPrice(pricingParameters);         
        GIAAS_Utils.getDescription('plan');     
        GIAAS_Utils.getDescription('test plan');
        GIAAS_Utils.getStoragePrice('test storage');
        GIAAS_Utils.getStoragePrice('test');
        
        
     }
     
  }