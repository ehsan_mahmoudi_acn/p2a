public with sharing class CagerackprivateroomdetailsCtrl 
{
    public case caseObj;
    private ApexPages.StandardController sc;
    private list<case> casequery;
    public Boolean pageRefresh {get; Set;}
    public RR_Cage_or_Rack__c priceObj{get;set;}
    public List<RR_Cage_or_Rack__c> RRcrlist{get;set;}
    public Set<ID> prodid = new Set<ID>();
    
    public CagerackprivateroomdetailsCtrl(ApexPages.StandardController controller) 
    {
        this.caseObj = (case)controller.getrecord();
        casequery = [select id, Product_Basket__c, Product_Configuration__c, Product_Configuration__r.cscfga__Root_Configuration__c from
        case where id=: caseobj.id];
        initlize();
    }   
    
    
    public void initlize()
    {
    for(Case cs : casequery)
    {
        prodid.add(cs.Product_Configuration__c); 
    }
        RRcrlist = [select id, Product_Configuration__c, Cage_or_Room_ID__c, Type__c, Width_per_rack_mm__c, Rack_IDs_allocated__c, Power_per_rack_KVA__c, No_of_ceeforms_commando_sockets_per_rack__c 
                                            from RR_Cage_or_Rack__c where Product_Configuration__c in: prodid and Is_Resource_Reservation__c = true and Is_Rack__c = true ];
    }   
    
    public PageReference updateItems()
    {
        List<RR_Cage_or_Rack__c> priceApplst = new List<RR_Cage_or_Rack__c>();
        for( RR_Cage_or_Rack__c obj : RRcrlist) 
        {
            priceApplst.add(obj); 
        }
        update priceApplst;
        pageRefresh = true;
        return null;
    }
}