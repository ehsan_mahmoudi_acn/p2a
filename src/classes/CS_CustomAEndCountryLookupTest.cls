@isTest(SeeAllData = false)
private class CS_CustomAEndCountryLookupTest {
    
    private static List<CS_Country__c> countryList;
    private static List<CS_POP__c> popList;
    private static List<CS_Route_Segment__c> routeSegmentList;
    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    private static Id[] excludeIds = new List<Id>();
    private static Integer pageOffset, pageLimit;
    private static Object[] data = new List<Object>();
    
    private static void initTestData(){
        countryList = new List<CS_Country__c>{
            new CS_Country__c(Name = 'Croatia'),
            new CS_Country__c(Name = 'Australia'),
            new CS_Country__c(Name = 'India'),
            new CS_Country__c(Name = 'Germany'),
            new CS_Country__c(Name = 'Hong Kong')
        };
        
        insert countryList;
        System.debug('****Country List: ' + countryList);
          System.assertEquals('Croatia',countryList[0].Name );
        
        popList = new List<CS_POP__c>{
            new CS_POP__c(Name = 'POP 1', CS_Country__c = countryList[0].Id),
            new CS_POP__c(Name = 'POP 2', CS_Country__c = countryList[1].Id),
            new CS_POP__c(Name = 'POP 3', CS_Country__c = countryList[2].Id),
            new CS_POP__c(Name = 'POP 4', CS_Country__c = countryList[3].Id),
            new CS_POP__c(Name = 'POP 5', CS_Country__c = countryList[4].Id)
        };
        
        insert popList;
        System.debug('****POP List: ' + popList);
          System.assertEquals('POP 1',popList[0].Name );
        
        routeSegmentList = new List<CS_Route_Segment__c>{
            new CS_Route_Segment__c(Name = 'Route Segment 1', Product_Type__c = 'IPL', POP_A__c = popList[0].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 2', Product_Type__c = 'IPL', POP_A__c = popList[1].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 3', Product_Type__c = 'IPL', POP_A__c = popList[2].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 4', Product_Type__c = 'EPL', POP_A__c = popList[3].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 5', Product_Type__c = 'EPLX', POP_A__c = popList[4].Id)
        };
        
        insert routeSegmentList;
        System.debug('****Route Segment List: ' + routeSegmentList);
          System.assertEquals('Route Segment 1',routeSegmentList[0].Name );    
    }
    

    private static testMethod void doLookupSearchTest() {
        Exception ee = null;
        
        try{
            disableAll(UserInfo.getUserId());
            Test.startTest();
            initTestData();
            
            searchFields.put('Product Type', 'IPL');
            searchFields.put('searchValue', '');
            
            CS_CustomAEndCountryLookup customAEndCountryLookup = new CS_CustomAEndCountryLookup(); 
            String requiredAtts = customAEndCountryLookup.getRequiredAttributes();
            data = customAEndCountryLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            
            System.debug('*******Data: ' + data);
            
 
            System.assert(data.size() > 0, '');

            
        } catch(Exception e){
            ee = e;
        } finally {
            Test.stopTest();
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }
        
    }
    
    /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }

}