global with sharing class CalloutResponseAddressCheckInternet extends csbb.CalloutResponseManagerExt {

  global CalloutResponseAddressCheckInternet (Map<String, csbb.CalloutResponse> mapCR, csbb.ProductCategory productCategory, csbb.CalloutProduct.ProductResponse productResponse) {
    system.debug('******** CalloutResponseCityAvailabilityCheck ******');
    this.setData(mapCR, productCategory, productResponse);

  }
  global CalloutResponseAddressCheckInternet () {
  }

  global void setData(Map<String, csbb.CalloutResponse> mapCR, csbb.ProductCategory productCategory, csbb.CalloutProduct.ProductResponse productResponse) {

    this.service = 'AddressCheckInternet'; 
    this.productCategoryId = productCategory.productCategoryId; 
    this.mapCR = mapCR;
    this.productCategory = productCategory; 
    this.productResponse = productResponse; 
    this.setPrimaryCalloutResponse();

  }

  global Map<String, Object> processResponseRaw (Map<String, Object> inputMap) {
    return new Map<String, Object>();
  }

  global Map<String, Object> getDynamicRequestParameters (Map<String, Object> inputMap) {
    return new Map<String, Object>();
  }

  global void runBusinessRules (String categoryIndicator) {
    this.productResponse.displayMessage = 'Sample message from AddressCheckInternet'; 
    this.productResponse.available = 'true';
    String resultJson = csbb.CalloutDisplay.takeString(crPrimary, 'Envelope.Body.doWorkResponse.result');
    
    Map<String, String> resultMap = (Map<String, String>)JSON.deserialize(resultJson, Map<String, String>.class);
    this.crPrimary.mapDynamicFields.put('topspeed', resultMap.get('topspeed'));

  }

  global csbb.Result canOffer (Map<String, String> attMap, Map<String, String> responseFields, csbb.CalloutProduct.ProductResponse productResponse) {

    csbb.Result canOfferResult = new csbb.Result(); 
    canOfferResult.status = 'OK';
    
    return canOfferResult;
  }

}