@isTest(SeeAllData = false)
public class MacdHandlerTest{
    
    public  static testmethod void testMacdHandler1() {
        
        P2A_TestFactoryCls.sampleTestData();
        List<cscfga__Configuration_Offer__c> offer = P2A_TestFactoryCls.getOffers(1);
        List<Account> ListAcc  = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> ListOpp = P2A_TestFactoryCls.getOpportunitys(1,ListAcc);
        List<cscfga__Product_basket__c> Products =  P2A_TestFactoryCls.getProductBasketHdlr(1,ListOpp); 
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,ListOpp);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, Products,ProductDeflist,Pbundle,Offerlists);
        List<cscfga__Configuration_Screen__c> configscreen = P2A_TestFactoryCls.getConfigScreen(1,ProductDeflist);
        List<cscfga__Screen_Section__c> screen = P2A_TestFactoryCls.getScreenSec(1,configscreen);
        List<cscfga__Attribute_Definition__c > Attributedeflist= P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist,configscreen,screen);
        Attributedeflist[0].Name = 'Attribute Defination';
        //Attributedeflist[0].Screens_Displaying_This_Attribute__c =1;
        Attributedeflist[0].cscfga__Type__c = 'Select List';
        update Attributedeflist[0];
        
        list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
        
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        list<CSPOFA__Orchestration_Process_Template__c> orchprocessTemplist = P2A_TestFactoryCls.getOrchestrationProcess(1);    
        List<CSPOFA__Orchestration_Process__c>processList = P2A_TestFactoryCls.getOrchestrationProcesss(1,orchprocessTemplist);    
        List<CSPOFA__Orchestration_Step__c>stepList=P2A_TestFactoryCls.getOrchestrationStep(1,processList);
        
        List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(2, OrdReqList);
        csord__Subscription__c subt = new csord__Subscription__c();
        subt.id = subscriptionList[0].id;
        subt.csordtelcoa__Replaced_Subscription__c = subscriptionList[1].id;
        update subt;
        
                List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);    
        List<csord__service__c> servList = P2A_TestFactoryCls.getservice(1,OrdReqList,subscriptionList);
          
        List<case>caseList=P2A_TestFactoryCls.getcase(1,ListAcc); 
        List<solutions__c> solution2 = P2A_TestFactoryCls.getsolutions(1,ListAcc);
        
        List<cscfga__Product_Configuration__c> proconfigs1= P2A_TestFactoryCls.getProductonfig(1, Products,ProductDeflist,Pbundle,Offerlists);
        proconfigs1[0].csordtelcoa__Replaced_Product_Configuration__c = proconfigs[0].id;
        update proconfigs1[0]; 
        
      /* csordtelcoa__Replaced_Service__c repo1 = new csordtelcoa__Replaced_Service__c();
        repo1.name = 'Test';
        insert repo1 ;*/
        
        
        List<csord__Service__c> Servicelist = new List<csord__Service__c>();
        csord__Service__c ser = new csord__Service__c();
        ser.Name = 'GCPE'; 
        ser.csord__Identification__c = 'Test-Catlyne-4238362';
        ser.csord__Service__c = servList[0].id;
        ser.csordtelcoa__Replacement_Service__c= servList[0].id;
        ser.csord__Order_Request__c = OrdReqList[0].id;
        ser.csord__Subscription__c = subscriptionList[0].id;
        ser.Billing_Commencement_Date__c = System.Today();
        ser.Stop_Billing_Date__c = System.Today();
        ser.RAG_Status_Red__c = false ; 
        ser.RAG_Reason_Code__c = ''; 
        ser.csordtelcoa__Product_Basket__c = Products[0].id;  
        ser.Product_Id__c = 'IPVPN'; 
        ser.csord__Status__c = 'Replaced';    
        ser.Path_Instance_ID__c = 'test';
        ser.Cease_Service_Flag__c = true;
        ser.Order_Type_Final__c = 'Terminate';
        ser.Primary_Port_Service__c = servList[0].id;
        ser.solutions__c = solution2[0].id;
        ser.csordtelcoa__Product_Configuration__c = proconfigs1[0].id;                           
        Servicelist.add(ser);
        Insert Servicelist;
        
        List<csord__Service__c> se = [Select id,Billing_Commencement_Date__c,name,csordtelcoa__Replaced_Service__c from csord__Service__c where name ='GCPE'];
        
        Test.starttest();    
        
            MacdHandler macd = new MacdHandler();
            macd.afterInsertChangeOrder(subscriptionList);
            macd.markServicesWithAnUpdatedConfiguration(Servicelist);
            List<csord__Service__c> servListreplaced = macd.findAllNonReplacedServiceFromOriginalSubscriptions(subscriptionList);
            List<csord__Subscription__c> subscpr = [select id,name,Count_of_Replaced_Services__c from csord__Subscription__c ];
            List<csord__Service__c> clone1 = macd.cloneServices(servList,subscriptionList);
            macd.rebuildServiceHierarchy(subscriptionList,Servicelist);
            macd.getReplacedSubscriptionIds(subscriptionList);
            csord__Service__c clonelist = macd.cloneService(servList[0],subscriptionList[0]);
        
        Test.stoptest();
        
        system.assertEquals(1,clone1.size());
        system.assertEquals(clone1[0].csordtelcoa__Replaced_Service__c,servList[0].id);
        system.assertEquals(1,subscpr[0].Count_of_Replaced_Services__c )  ;
        system.assertEquals(Servicelist[0].csord__Service__c,servList[0].id);
        system.assertNOTEquals(2,servListreplaced.size());  
        system.assert(proconfigs1[0].csordtelcoa__Replaced_Product_Configuration__c == proconfigs[0].id);
        System.assertequals(Ser.csordtelcoa__Replaced_Service__c ,se[0].csordtelcoa__Replaced_Service__c);
        
      /*  System.assertEquals(null,Servicelist[0].csordtelcoa__Replaced_Service__c);
        System.assertEquals('GCPE',AttributesList[0].name);
        System.assertEquals('GCPE',Servicelist[0].name);
        System.assertequals(Ser.csordtelcoa__Replaced_Service__c ,se[0].csordtelcoa__Replaced_Service__c); */
        
        
    }
}