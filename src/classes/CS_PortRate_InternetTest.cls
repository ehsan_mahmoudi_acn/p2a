@isTest(SeeAllData = false)
private class CS_PortRate_InternetTest{

    private static List<CS_Country__c> countryList;
    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    private static Id[] excludeIds = new List<Id>();
    private static Integer pageOffset =1;
    private static Integer pageLimit=1;
    private static Object[] data = new List<Object>();
    private static List<CS_Bandwidth__c> bandwidthList;
    
    
    private static void initTestData(){
        countryList = new List<CS_Country__c>{
            new CS_Country__c(Name = 'India'),
            new CS_Country__c(Name = 'Pakistan') 
        };
        insert countryList;
        list<CS_Country__c> prod = [select id,Name from CS_Country__c where Name = 'India'];
                                system.assertEquals(countryList[0].name , prod[0].name);
                                System.assert(countryList!=null); 

        System.debug('****Country List: ' + countryList);
        
        bandwidthList = new List<CS_Bandwidth__c>{
           new CS_Bandwidth__c(Name = '64k', Bandwidth_Value_MB__c = 64),
           new CS_Bandwidth__c(Name = '128k', Bandwidth_Value_MB__c = 128),
           new CS_Bandwidth__c(Name = '256k', Bandwidth_Value_MB__c = 256)     
        };
        insert bandwidthList;
        list<CS_Bandwidth__c> ATT = [select id,Name from CS_Bandwidth__c where Name = '64k'];
                                system.assertEquals(bandwidthList[0].name , ATT[0].name);
                                System.assert(bandwidthList!=null); 


        
    }
    
    private static testMethod void doLookupSearchTestMethod1() {
        Exception ee = null;

        try{
            disableAll(UserInfo.getUserId());
            Test.startTest();
            initTestData();
            
            searchFields.put('Maximum Bandwidth excluding direct China',bandwidthList[0].id);
            searchFields.put('Direct China Committed Bandwidth',bandwidthList[1].id);
            searchFields.put('Committed Bandwidth excluding direct China',bandwidthList[2].id);
            searchFields.put('Port Rating Type','Flat');
            
            CS_PortRate_Internet CSPortRateInternet= new CS_PortRate_Internet(); 
            String requiredAtts = CSPortRateInternet.getRequiredAttributes();
            data = CSPortRateInternet.doLookupSearch(searchFields,productDefinitionId,excludeIds,pageOffset,pageLimit);
            system.assert(CSPortRateInternet!=null);
            Test.stopTest();         
           
        } catch(Exception e){
        ErrorHandlerException.ExecutingClassName='CS_PortRate_InternetTest :doLookupSearchTest_Method1';         
        ErrorHandlerException.sendException(e); 
            ee = e;
        } finally {
            
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }
  }
  
    private static testMethod void doLookupSearchTestMethod2() {
        Exception ee = null;

        try{
            disableAll(UserInfo.getUserId());
            Test.startTest();
            initTestData();
            
            searchFields.put('Maximum Bandwidth excluding direct China',bandwidthList[0].id);
            searchFields.put('Direct China Committed Bandwidth',bandwidthList[1].id);
            searchFields.put('Committed Bandwidth excluding direct China',bandwidthList[2].id);           
            
            CS_PortRate_Internet CSPortRateInternet= new CS_PortRate_Internet(); 
            String requiredAtts = CSPortRateInternet.getRequiredAttributes();
            data = CSPortRateInternet.doLookupSearch(searchFields,productDefinitionId,excludeIds,pageOffset,pageLimit); 
            system.assert(CSPortRateInternet!=null);
            Test.stopTest();                 
           
        } catch(Exception e){
        ErrorHandlerException.ExecutingClassName='CS_PortRate_InternetTest :doLookupSearchTest_Method2';         
        ErrorHandlerException.sendException(e); 
            ee = e;
        } finally {
           
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }
  } 
  
      /**
     * Disables triggers, validations and workflows 
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);
        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        upsert globalMute;
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);
        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        upsert globalMute;
    }
}