@isTest(SeeAllData= false)
    public class MACDUtilitiesTest{
        private static List<cscfga__Product_Category__c> prodCategoryList;
        private static List<cscfga__Product_Definition__c> prodDefList;
        private static List<cscfga__Product_Configuration__c> pcList;
        private static List<csbb__Product_Configuration_Request__c> pcrList;
        private static List<csbb__Callout_Product_Result__c> calloutPrResList;
    
     
     public static testmethod void mACDUtilitieunitTest1(){
     
             Test.starttest();

     MACD_Service_Links__c mcd = new MACD_Service_Links__c(name='Internet-Onnet',Service_Field__c='GID_Service__c');
        insert mcd;
        system.assertEquals(true,mcd!=null); 
         
      Product_Definition_Id__c pdI = new Product_Definition_Id__c();
                pdI.Name = 'IPVPN_Product_ID';
                pdI.Product_Id__c = 'IPVPN';
                insert pdI;
                system.assertEquals(true,pdI!=null);
                Product_Definition_Id__c pdI1 = new Product_Definition_Id__c();
                pdI1.Name = 'VPLS_Product_ID';
                pdI1.Product_Id__c = 'VLM';
                insert pdI1;
                system.assertEquals(true,pdI1!=null); 
                Product_Definition_Id__c pdI2 = new Product_Definition_Id__c();
                pdI2.Name = 'IPC_Product_ID';
                pdI2.Product_Id__c = 'IPTS-C';
                insert pdI2;  
                system.assertEquals(true,pdI2!=null);               
       
        P2A_TestFactoryCls.SetupTestData();

        list<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
         
        
        List<cscfga__Attribute_Definition__c>  AttDef= new List<cscfga__Attribute_Definition__c>();
         cscfga__Attribute_Definition__c Attributesdef = new cscfga__Attribute_Definition__c(); 
                Attributesdef.name = 'Test Att';
                Attributesdef.cscfga__Product_Definition__c=  prodef[0].id;
                Attributesdef.cscfga__Column__c = 2;
                Attributesdef.cscfga__Row__c = 4;
                Attributesdef.Attribute_Type__c = 'Product Configuration';
                Attributesdef.Group_Name__c = 'Test Group name'; 
                Attributesdef.Relationship_Type__c ='ASBRPointerMultiple'; 
                AttDef.add(Attributesdef);              
            
            insert AttDef;
        System.assertEquals(AttDef[0].name,'Test Att'); 
            
   
       List<cscfga__Attribute__c> lstAt = new List<cscfga__Attribute__c>(); 
        cscfga__Attribute__c Attributes = new cscfga__Attribute__c();
                Attributes.name = 'GCPE';
                Attributes.cscfga__Product_Configuration__c= proconfig[0].id;
                Attributes.cscfga__Value__c = proconfig[0].id ;
                Attributes.cscfga__Display_Value__c = 'Test displayvalue' ;
                Attributes.cscfga__Attribute_Definition__c = attdef[0].id; 
                lstAt.add(Attributes);
                insert lstAt;            
        System.assertEquals(lstAt[0].name,'GCPE'); 
           
        Set<id> st = new Set<id>{servlist[0].id};
        Set<String> macdSerFieldListfields = new Set<String>();
        macdSerFieldListfields.add('name');
        Set<id> Proconfigsids = new Set<id>{proconfig[0].id};
        
        List<cscfga__Attribute_Definition__c>  AttDef1= new List<cscfga__Attribute_Definition__c>();
         cscfga__Attribute_Definition__c Attributesdef1 = new cscfga__Attribute_Definition__c(); 
                Attributesdef1.name = 'Test Att';
                Attributesdef1.cscfga__Product_Definition__c=  prodef[0].id;
                Attributesdef1.cscfga__Column__c = 2;
                Attributesdef1.cscfga__Row__c = 4;
                Attributesdef1.Attribute_Type__c = 'service';
                Attributesdef1.Group_Name__c = 'Test Group name'; 
                Attributesdef1.Relationship_Type__c ='ASBRPointerMultiple'; 
                AttDef1.add(Attributesdef1);
                insert AttDef1;
        System.assertEquals(AttDef1[0].name,'Test Att'); 
                
                 List<csord__Service__c> servicelist = new List<csord__Service__c>();
                csord__Service__c ser = new csord__Service__c();
                ser.Name = 'Test Service'; 
                ser.csord__Identification__c = 'Test-Catlyne-4238362';
                ser.csord__Order_Request__c = ordReq[0].id;
                ser.csord__Subscription__c =  SUBList[0].id;
                ser.Billing_Commencement_Date__c = System.Today();
                ser.csordtelcoa__Product_Configuration__c = proconfig[0].id;
                ser.Stop_Billing_Date__c = System.Today();
                ser.RAG_Status_Red__c = false ; 
                ser.RAG_Reason_Code__c = '';     
  
                servicelist.add(ser);
 
            insert servicelist;
        System.assertEquals(servicelist[0].name,'Test Service'); 
                                                 
                
                List<cscfga__Attribute__c> lstAt1 = new List<cscfga__Attribute__c>(); 
        cscfga__Attribute__c Attributes1 = new cscfga__Attribute__c();
                Attributes1.name = 'GCPE';
                Attributes1.cscfga__Product_Configuration__c= proconfig[0].id;
                Attributes1.cscfga__Value__c = servicelist[0].id ;
                Attributes1.cscfga__Display_Value__c = 'Test displayvalue' ;
                Attributes1.cscfga__Attribute_Definition__c = attdef1[0].id; 
                lstAt1.add(Attributes1);
                insert lstAt1;
        System.assertEquals(lstAt1[0].name,'GCPE'); 
                
                List<cscfga__Attribute_Definition__c>  AttDef2= new List<cscfga__Attribute_Definition__c>();
         cscfga__Attribute_Definition__c Attributesdef2 = new cscfga__Attribute_Definition__c(); 
                Attributesdef2.name = 'Test Att';
                Attributesdef2.cscfga__Product_Definition__c=  prodef[0].id;
                Attributesdef2.cscfga__Column__c = 2;
                Attributesdef2.cscfga__Row__c = 4;
                Attributesdef2.Attribute_Type__c = 'service';
                Attributesdef2.Group_Name__c = 'Test Group name'; 
                Attributesdef2.Relationship_Type__c = 'Master'; 
                AttDef2.add(Attributesdef2);
                insert AttDef2;
        System.assertEquals(AttDef2[0].name,'Test Att'); 
               
                 List<cscfga__Attribute__c> lstAt2 = new List<cscfga__Attribute__c>(); 
        cscfga__Attribute__c Attributes2 = new cscfga__Attribute__c();
                Attributes2.name = 'GCPE';
                Attributes2.cscfga__Product_Configuration__c= proconfig[0].id;
                Attributes2.cscfga__Value__c = servicelist[0].id ;
                Attributes2.cscfga__Display_Value__c = 'Test displayvalue' ;
                Attributes2.cscfga__Attribute_Definition__c = attdef2[0].id; 
                lstAt2.add(Attributes2);
                insert lstAt2;
        System.assertEquals(lstAt2[0].name,'GCPE'); 
               
   
      MACDUtilities.LinkProductConfigurationsInMACD(Proconfigsids);
     
      
      Test.stoptest();
}
   public static testmethod void mACDUtilitieunitTest2(){
  
        Test.starttest();

        P2A_TestFactoryCls.SetupTestData();

        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        
        List<cscfga__Product_Configuration__c> pcList = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
        
        
        List<cscfga__Product_Configuration__c> ProductConfiglist = new List<cscfga__Product_Configuration__c>();
            cscfga__Product_Configuration__c pb = new cscfga__Product_Configuration__c();
            pb.name ='IPL';
            pb.cscfga__Product_basket__c = Products[0].id;
            pb.cscfga__Product_Definition__c = Prodef[0].id;
            pb.cscfga__contract_term_period__c = 12;
            pb.Is_Offnet__c = 'yes';
            pb.cscfga_Offer_Price_MRC__c = 200;
            pb.cscfga_Offer_Price_NRC__c = 300;
            pb.Rate_Card_NRC__c = 200;
            pb.Rate_Card_RC__c = 300;
            pb.cscfga__total_contract_value__c = 1000;
            pb.cscfga__Contract_Term__c = 12;
            pb.CurrencyIsoCode = 'USD';
            pb.cscfga_Offer_Price_MRC__c = 100;
            pb.cscfga_Offer_Price_NRC__c = 100;
            pb.Child_COst__c = 100;
            pb.Cost_NRC__c = 100;
            pb.Cost_MRC__c = 100;
            pb.cscfga__Product_Bundle__c = pbundlelist[0].id;
            pb.Product_Name__c = 'test product';
            pb.Added_Ports__c = null ;
            Pb.cscfga__Product_Family__c = 'Point to Point';
            Pb.csordtelcoa__Replaced_Product_Configuration__c = pcList[0].id;
            
            ProductConfiglist.add(pb);
            
            insert ProductConfiglist; 
        System.assertEquals(ProductConfiglist[0].name,'IPL'); 
              
                List<cscfga__Attribute_Definition__c>  AttDef3= new List<cscfga__Attribute_Definition__c>();
         cscfga__Attribute_Definition__c Attributesdef3 = new cscfga__Attribute_Definition__c(); 
                Attributesdef3.name = 'Test Att';
                Attributesdef3.cscfga__Product_Definition__c= prodef[0].id;
                Attributesdef3.cscfga__Column__c = 2;
                Attributesdef3.cscfga__Row__c = 4;
                Attributesdef3.Attribute_Type__c = 'Product Configuration';
                Attributesdef3.Group_Name__c = 'Test Group name'; 
                Attributesdef3.Relationship_Type__c = 'Master'; 
                AttDef3.add(Attributesdef3);
                insert AttDef3;
        System.assertEquals(AttDef3[0].name,'Test Att'); 
                
                 List<cscfga__Attribute__c> lstAt3 = new List<cscfga__Attribute__c>(); 
        cscfga__Attribute__c Attributes3 = new cscfga__Attribute__c();
                Attributes3.name = 'GCPE';
                Attributes3.cscfga__Product_Configuration__c= ProductConfiglist[0].id;
                Attributes3.cscfga__Value__c = ProductConfiglist[0].id ;
                Attributes3.cscfga__Display_Value__c = 'Test displayvalue' ;
                Attributes3.cscfga__Attribute_Definition__c = attdef3[0].id; 
                lstAt3.add(Attributes3);
                insert lstAt3;             
        System.assertEquals(lstAt3[0].name,'GCPE'); 
           
            Set<id> Proconfigsetids = new Set<id>{ProductConfiglist[0].id};
  
   
    MACDUtilities.LinkProductConfigurationsInMACD(Proconfigsetids);
    
    Test.Stoptest();

   }
   
   public static testmethod void mACDUtilitieunitTest(){
    
            Test.starttest();

        MACD_Service_Links__c mcd = new MACD_Service_Links__c(name='Internet-Onnet',Service_Field__c='GID_Service__c');
        insert mcd; 
         
         Product_Definition_Id__c pdI = new Product_Definition_Id__c();
                pdI.Name = 'IPVPN_Product_ID';
                pdI.Product_Id__c = 'IPVPN';
                insert pdI;
                Product_Definition_Id__c pdI1 = new Product_Definition_Id__c();
                pdI1.Name = 'VPLS_Product_ID';
                pdI1.Product_Id__c = 'VLM';
                insert pdI1;
                Product_Definition_Id__c pdI2 = new Product_Definition_Id__c();
                pdI2.Name = 'IPC_Product_ID';
                pdI2.Product_Id__c = 'IPTS-C';
                insert pdI2;
       
        P2A_TestFactoryCls.SetupTestData();

        list<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
          List<cscfga__Configuration_Screen__c> ConScr = P2A_TestFactoryCls.getConfigScreen(1, Prodef);
        List<cscfga__Screen_Section__c> ScrSec = P2A_TestFactoryCls.getScreenSec(1, ConScr);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
        List<cscfga__Attribute_Definition__c> AttDef1 = P2A_TestFactoryCls.getAttributesdef(2, proconfig, Prodef, ConScr, ScrSec);
         List<cscfga__Attribute__c> Attrblist = P2A_TestFactoryCls.getAttributes(1, proconfig, AttDef1);  
         AttDef1[0].Attribute_Type__c='Check Box';
         update attdef1;
        List<cscfga__Attribute_Definition__c>  AttDef= new List<cscfga__Attribute_Definition__c>();
         cscfga__Attribute_Definition__c Attributesdef = new cscfga__Attribute_Definition__c(); 
                Attributesdef.name = 'Test Att';
                Attributesdef.cscfga__Product_Definition__c=  prodef[0].id;
                Attributesdef.cscfga__Column__c = 2;
                Attributesdef.cscfga__Row__c = 4;
                Attributesdef.Attribute_Type__c = 'Product Configuration';
                Attributesdef.Group_Name__c = 'Test Group name'; 
                Attributesdef.Relationship_Type__c ='ASBRPointerMultiple'; 
                AttDef.add(Attributesdef);              
            
            insert AttDef;
        
                
        System.assertEquals(AttDef[0].name,'Test Att'); 
   
       List<cscfga__Attribute__c> lstAt = new List<cscfga__Attribute__c>(); 
        cscfga__Attribute__c Attributes = new cscfga__Attribute__c();
                Attributes.name = 'GCPE';
                Attributes.cscfga__Product_Configuration__c= proconfig[0].id;
                Attributes.cscfga__Value__c = proconfig[0].id ;
                Attributes.cscfga__Display_Value__c = 'Test display value';
                Attributes.cscfga__Attribute_Definition__c = attdef[0].id; 
                lstAt.add(Attributes);
                insert lstAt;            
        System.assertEquals(lstAt[0].name,'GCPE'); 
           
        Set<id> st = new Set<id>{servlist[0].id};
        Set<String> macdSerFieldListfields = new Set<String>();
        macdSerFieldListfields.add('name');


        cscfga__Attribute__c ca = MACDUtilities.setCheckBoxAttributeInGroup(lstAt[0], Attrblist);
        List<cscfga__Attribute__c> lsm = MACDUtilities.setAttributeInGroup(lstAt[0], Attrblist, proconfig[0], 'Calculation');
       
        Test.stopTest();

    }
    public static testmethod void mACDUtilitieunitTest21(){
    
            Test.starttest();

        MACD_Service_Links__c mcd = new MACD_Service_Links__c(name='Internet-Onnet',Service_Field__c='GID_Service__c');
        insert mcd; 
         
         Product_Definition_Id__c pdI = new Product_Definition_Id__c();
                pdI.Name = 'IPVPN_Product_ID';
                pdI.Product_Id__c = 'IPVPN';
                insert pdI;
                Product_Definition_Id__c pdI1 = new Product_Definition_Id__c();
                pdI1.Name = 'VPLS_Product_ID';
                pdI1.Product_Id__c = 'VLM';
                insert pdI1;
                Product_Definition_Id__c pdI2 = new Product_Definition_Id__c();
                pdI2.Name = 'IPC_Product_ID';
                pdI2.Product_Id__c = 'IPTS-C';
                insert pdI2;
       
        P2A_TestFactoryCls.SetupTestData();

        list<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
          List<cscfga__Configuration_Screen__c> ConScr = P2A_TestFactoryCls.getConfigScreen(1, Prodef);
        List<cscfga__Screen_Section__c> ScrSec = P2A_TestFactoryCls.getScreenSec(1, ConScr);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
        List<cscfga__Attribute_Definition__c> AttDef1 = P2A_TestFactoryCls.getAttributesdef(2, proconfig, Prodef, ConScr, ScrSec);
         List<cscfga__Attribute__c> Attrblist = P2A_TestFactoryCls.getAttributes(1, proconfig, AttDef1);           
         AttDef1[0].Attribute_Type__c='Service';
         update attdef1;
         Attrblist[0].cscfga__Attribute_Definition__c=AttDef1[0].id;
         update Attrblist;
        List<cscfga__Attribute_Definition__c>  AttDef= new List<cscfga__Attribute_Definition__c>();
         cscfga__Attribute_Definition__c Attributesdef = new cscfga__Attribute_Definition__c(); 
                Attributesdef.name = 'Test Att';
                Attributesdef.cscfga__Product_Definition__c=  prodef[0].id;
                Attributesdef.cscfga__Column__c = 2;
                Attributesdef.cscfga__Row__c = 4;
                Attributesdef.Attribute_Type__c = 'Product Configuration';
                Attributesdef.Group_Name__c = 'Test Group name'; 
                Attributesdef.Relationship_Type__c ='ASBRPointerMultiple'; 
                AttDef.add(Attributesdef);              
            
            insert AttDef;
        
                
        System.assertEquals(AttDef[0].name,'Test Att'); 
   
       List<cscfga__Attribute__c> lstAt = new List<cscfga__Attribute__c>(); 
        cscfga__Attribute__c Attributes = new cscfga__Attribute__c();
                Attributes.name = 'GCPE';
                Attributes.cscfga__Product_Configuration__c= proconfig[0].id;
                Attributes.cscfga__Value__c = proconfig[0].id ;
                Attributes.cscfga__Display_Value__c = 'Test display value';
                Attributes.cscfga__Attribute_Definition__c = attdef[0].id; 
                lstAt.add(Attributes);
                insert lstAt;            
        System.assertEquals(lstAt[0].name,'GCPE'); 
           
        Set<id> st = new Set<id>{servlist[0].id};
        Set<String> macdSerFieldListfields = new Set<String>();
        macdSerFieldListfields.add('name');

        System.assertEquals('Service',AttDef1[0].Attribute_Type__c);
        cscfga__Attribute__c ca = MACDUtilities.setCheckBoxAttributeInGroup(lstAt[0], Attrblist);
        List<cscfga__Attribute__c> lsm = MACDUtilities.setAttributeInGroup(lstAt[0], Attrblist, proconfig[0], 'Calculation');
       
        Test.stopTest();

    }
    
    Public static void testutil(){
    
            Test.starttest();

          MACD_Service_Links__c mcd = new MACD_Service_Links__c(name='Internet-Onnet',Service_Field__c='GID_Service__c');
        insert mcd; 
        
         Product_Definition_Id__c pdI = new Product_Definition_Id__c();
                pdI.Name = 'IPVPN_Product_ID';
                pdI.Product_Id__c = 'IPVPN';
                insert pdI;
                Product_Definition_Id__c pdI1 = new Product_Definition_Id__c();
                pdI1.Name = 'VPLS_Product_ID';
                pdI1.Product_Id__c = 'VLM';
                insert pdI1;
                Product_Definition_Id__c pdI2 = new Product_Definition_Id__c();
                pdI2.Name = 'IPC_Product_ID';
                pdI2.Product_Id__c = 'IPTS-C';
                insert pdI2;
                
                
                
 
        
        P2A_TestFactoryCls.SetupTestData();

        list<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        Prodef[0].name='ASBR';
        update Prodef;

        List<cscfga__Product_Definition__c> Prodef1 = P2A_TestFactoryCls.getProductdef(1);
        Prodef1[0].name='IPVPN';
        update Prodef1;
        
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<csord__Service__c> servList1 = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<csord__Service__c> servList2 = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<csord__Service__c> servList3 = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
        proconfig[0].ASBR_Product_Configuration__c=String.valueOf(servList3[0].id);
        update proconfig;
        List<cscfga__Product_Configuration__c> proconfig1 = P2A_TestFactoryCls.getProductonfig(1,Products,prodef1,pbundlelist,Offerlists);
        proconfig1[0].ASBR_Product_Configuration__c=String.valueOf(servList3[0].id);
        update proconfig1;
        
        servList[0].csordtelcoa__Product_Configuration__c=proconfig[0].id;
        update servList;
        
        servList1[0].csordtelcoa__Product_Configuration__c=proconfig1[0].id;
        update servList1;
        
        servList2[0].csordtelcoa__Product_Configuration__c=proconfig[0].id;
        servList2[0].csordtelcoa__Replaced_Service__c=servList3[0].id;
        update servList2;
        
         List<cscfga__Attribute_Definition__c>  AttDef= new List<cscfga__Attribute_Definition__c>();
         cscfga__Attribute_Definition__c Attributesdef = new cscfga__Attribute_Definition__c(); 
                Attributesdef.name = 'Test Att';
                Attributesdef.cscfga__Product_Definition__c=  prodef[0].id;
                Attributesdef.cscfga__Column__c = 2;
                Attributesdef.cscfga__Row__c = 4;
                Attributesdef.Attribute_Type__c = 'Product Configuration';
                Attributesdef.Group_Name__c = 'Test Group name'; 
                Attributesdef.Relationship_Type__c ='ASBRPointerMultiple'; 
                AttDef.add(Attributesdef);              
            
            insert AttDef;
        System.assertEquals(AttDef[0].name,'Test Att'); 
  
               Product_Definition_Id__c pdI4=new Product_Definition_Id__c();
                pdI4.Name = 'ASBR_Definition_Id';//
                pdI4.Product_Id__c = String.valueOf(Prodef[0].id);
                insert pdI4;
                
                Product_Definition_Id__c pdI5=new Product_Definition_Id__c();
                pdI5.Name = 'IPVPN_Port_Definition_Id';
                pdI5.Product_Id__c = String.valueOf(Prodef1[0].id);
                insert pdI5;
                
         List<cscfga__Attribute__c> lstAt = new List<cscfga__Attribute__c>(); 
        cscfga__Attribute__c Attributes = new cscfga__Attribute__c();
                Attributes.name = 'GCPE';
                Attributes.cscfga__Product_Configuration__c= proconfig[0].id;
                Attributes.cscfga__Value__c = proconfig[0].id ;
                Attributes.cscfga__Display_Value__c = 'Test displayvalue';
                Attributes.cscfga__Attribute_Definition__c = attdef[0].id; 
                lstAt.add(Attributes);
                insert lstAt;            
        System.assertEquals(lstAt[0].name,'GCPE'); 

        CS_Port_ASBR_Service__c cspAsbr=new CS_Port_ASBR_Service__c();
        cspAsbr.ASBR__c=servList3[0].id;
        cspAsbr.Port__c=servList3[0].id;
        insert cspAsbr;
        CS_Port_ASBR_Service__c cspAsbr1=new CS_Port_ASBR_Service__c();
        cspAsbr1.ASBR__c=servList3[0].id;
        cspAsbr1.Port__c=servList3[0].id;
        insert cspAsbr1;
        
        Set<id> st = new Set<id>{servlist[0].id,servlist1[0].id,servlist2[0].id};
        Set<String> macdSerFieldListfields = new Set<String>();
        macdSerFieldListfields.add('name');
 
         MACDUtilities.setAttributeValueInGroup(lstAt[0], lstAt, new List<String>{'Test'}, 'name');
         //csord__service__c sercheck=[select csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__c from csord__Service__c where Id=:st.id limit 1];
         //System.assertEquals(sercheck.csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__c,Product_Definition_Id__c.getvalues('IPVPN_Port_Definition_Id').Product_Id__c);
         MACDUtilities.upsertASBRtoServiceLinks(st);
         MACDUtilities.updateServiceHierarchy(st);
         String qury = MACDUtilities.serviceQueryBuilder(st,macdSerFieldListfields);

       
        Test.stopTest();

    
    }
    
    
    Public static void utiltest(){
    
            Test.starttest();

        MACD_Service_Links__c mcd = new MACD_Service_Links__c(name='Internet-Onnet',Service_Field__c='GID_Service__c');
        insert mcd; 
        
         Product_Definition_Id__c pdI = new Product_Definition_Id__c();
                pdI.Name = 'IPVPN_Product_ID';
                pdI.Product_Id__c = 'IPVPN';
                insert pdI;
                Product_Definition_Id__c pdI1 = new Product_Definition_Id__c();
                pdI1.Name = 'VPLS_Product_ID';
                pdI1.Product_Id__c = 'VLM';
                insert pdI1;
                Product_Definition_Id__c pdI2 = new Product_Definition_Id__c();
                pdI2.Name = 'IPC_Product_ID';
                pdI2.Product_Id__c = 'IPTS-C';
                insert pdI2;
       
        
        P2A_TestFactoryCls.SetupTestData();

        list<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        
        
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
 
          
                
         List<cscfga__Attribute_Definition__c>  AttDef= new List<cscfga__Attribute_Definition__c>();
         cscfga__Attribute_Definition__c Attributesdef = new cscfga__Attribute_Definition__c(); 
                Attributesdef.name = 'Test Att';
                Attributesdef.cscfga__Product_Definition__c=  prodef[0].id;
                Attributesdef.cscfga__Column__c = 2;
                Attributesdef.cscfga__Row__c = 4;
                Attributesdef.Attribute_Type__c = 'Product Configuration';
                Attributesdef.Group_Name__c = 'Test Group name'; 
                Attributesdef.Relationship_Type__c ='ASBRPointerMultiple'; 
                AttDef.add(Attributesdef);              
            
            insert AttDef;
        System.assertEquals(AttDef[0].name,'Test Att'); 
        
 
        List<cscfga__Attribute__c> lstAt = new List<cscfga__Attribute__c>(); 
        cscfga__Attribute__c Attributes = new cscfga__Attribute__c();
                Attributes.name = 'GCPE';
                Attributes.cscfga__Product_Configuration__c= proconfig[0].id;
                Attributes.cscfga__Value__c = proconfig[0].id ;
                Attributes.cscfga__Display_Value__c = 'Test displayvalue';
                Attributes.cscfga__Attribute_Definition__c = attdef[0].id; 
                lstAt.add(Attributes);
                insert lstAt;            
        System.assertEquals(lstAt[0].name,'GCPE'); 

        Set<id> st = new Set<id>{servlist[0].id};
        Set<String> macdSerFieldListfields = new Set<String>();
        macdSerFieldListfields.add('name');
        
      
      List<csord__Service__c> lst = MACDUtilities.getLinkedServices(servlist[0].id, 'name',servList); 
  
        Test.stoptest();
    
    }
     
      private static testMethod void test2() {
        Exception ee = null;
        integer userid = 0;
        try {
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assertEquals(true,userid!=null); 
            MACDUtilitieunitTest1();
            } catch(Exception e) {
                ErrorHandlerException.ExecutingClassName='MACDUtilitiesTest :Test2';         
                ErrorHandlerException.sendException(e);
            ee = e;
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
   
      private static testMethod void test3() {
        Exception ee = null;
        integer userid = 0;
        try {
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assertEquals(true,userid!=null);
            MACDUtilitieunitTest2();
            } catch(Exception e) {
                ErrorHandlerException.ExecutingClassName='MACDUtilitiesTest :Test3';         
                ErrorHandlerException.sendException(e);
            ee = e;
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
    
        private static testMethod void test4() {
        Exception ee = null;
        integer userid = 0;
        try {
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assertEquals(true,userid!=null);
            MACDUtilitieunitTest();
            } catch(Exception e) {
                ErrorHandlerException.ExecutingClassName='MACDUtilitiesTest :Test4';         
                ErrorHandlerException.sendException(e);
            ee = e;
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
    
              private static testMethod void test5() {
        Exception ee = null;
        integer userid = 0;
        try {
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assertEquals(true,userid!=null);
            testutil();
            } catch(Exception e) {
                ErrorHandlerException.ExecutingClassName='MACDUtilitiesTest :Test5';         
                ErrorHandlerException.sendException(e);
            ee = e;
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
    
              private static testMethod void test6() {
        Exception ee = null;
        integer userid = 0;
        try {
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assertEquals(true,userid!=null);
            utiltest();
            } catch(Exception e) {
                ErrorHandlerException.ExecutingClassName='MACDUtilitiesTest :Test6';         
                ErrorHandlerException.sendException(e);
            ee = e;
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
    
}