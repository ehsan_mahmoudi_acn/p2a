@istest(SeeAllData = false)
public class ActionItemCaseFieldAutopoulatetest
{
     public static testmethod void setup1(){
        
       List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        
        Opportunity o   = new Opportunity();                
                o.AccountId     =  accList[0].id;
                o.name          = 'Test Opp ';
                o.stagename     = 'Identify & Define';
                o.CloseDate     = system.today() + 10;
                o.Pre_Contract_Provisioning_Required__c = 'No';
                o.QuoteStatus__c = 'Draft';
                o.Product_Type__c = 'IPL';
                o.Estimated_MRC__c = 100.00;
                o.Estimated_NRC__c = 100.00;
                o.Opportunity_Type__c = 'Simple';
                o.Sales_Status__c = 'Open';
                o.Order_Type__c = 'New';
                o.Stage__c='Identify & Define';
                o.ContractTerm__c='10';
                o.Approved_By__c=UserInfo.getUserId();
                o.CreditCheckDoneBy__c='Sumit Suman';
                Product2 prod2=new product2();
                prod2.Name = 'Test product';
                prod2.Family = 'Test Family';
                //ProdList.add(Prod2);
                
                
                insert o;
                
                System.assert(o!=null);
                
                case c = new case();
                c.Status ='Unassigned';
                c.Opportunity_Name__c= o.id;
                c.Accountid= accList[0].id;
                //c.ownerid = groups[0].id;
                c.Is_Feasibility_Request__c= true;
                c.Is_Resource_Reservation__c = true;
                c.Pricing_Approval_Case__c = 'pricing';
                c.Subject = 'Pricing approval request for Quote';
                insert c;
                
                System.assert(c!=null);
                
                profile pr = [select id from profile where name='TI Commercial'];
                  list<User> Userslist = new list<User>();
                 User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = pr.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser123456@testorg.com',EmployeeNumber='123');
                Userslist.add(u);
                insert Userslist ;
              map<id,Action_Item__c> acitemmap=new map<id,Action_Item__c>();
                
                
                
        id recordtype = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType, 'Pricing Validation');        
      List<Action_Item__c> newActionItems =new List<Action_Item__c>();
      Action_item__c ai = new Action_item__c();
      ai.Opportunity__c = o.id;
      ai.Status__c = 'Approved';
      ai.recordtypeid = recordtype;
      ai.Opportunity_Email__c = 'test@gmail.com';
      ai.Quote_Subject__c = 'Pricing Validation';
      ai.Pricing_Case_Approval__c = 'Pricing';
     ai.Assigned_User__c = 'test@gmail.com';
      ai.Approved_by_Email__c = 'test@gmail.com';
      ai.Commercial_Impact__c = 'yes';
      ai.Due_Date__c = system.today()+ 5;
      ai.Assigned_To__c = Userslist[0].id;
    //  ai.Rejection_Reason__c = 'Insufficient';
      ai.Order_Approval_Comments__c = 'Testtest';
      
      newActionItems.add(ai);
      insert newActionItems ;
      System.assert(newActionItems !=null);
      
      for(Action_Item__c action:newActionItems)
              {
                acitemmap.put(ai.id,ai);
              }
      test.starttest();
        ActionItemCaseFieldAutopoulate.Fieldpopulate(newActionItems);
      test.stoptest();
                
                
    }            
}