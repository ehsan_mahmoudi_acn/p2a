public without sharing class OpportunityLineItemTriggerHandler extends BaseTriggerHandler{
	Id StdPriceBookId = Label.StandardPriceBookId;
    public override void beforeInsert(List<SObject> newItems){
        upsertPriceBookEntries((List<OpportunityLineItem>)newItems);
        updatePriceBookEntriesReferences((List<OpportunityLineItem>)newItems);
        if(CheckRecursive.canExecuteBlocking('SOVCheck')){
        upsertSOVDetails((List<OpportunityLineItem>)newItems);
        }
        assignReportedSOV((List<OpportunityLineItem>) newItems);
        
    }
    
    /**
      * Before update handler
      */
    public override void beforeUpdate(List<SObject> newOppLineItem, Map<Id, SObject> newOppLineItemMap, Map<Id, SObject> oldOppLineItemMap){
    if(CheckRecursive.canExecuteBlocking('SOVCheck')){
    upsertSOVDetails((List<OpportunityLineItem>) newOppLineItem);   
     }
     updateReportedSOV((List<OpportunityLineItem>) newOppLineItem,(Map<Id, OpportunityLineItem> )newOppLineItemMap,(Map<Id, OpportunityLineItem> )oldOppLineItemMap);
    }
     /**
      * After update handler
      */
    public override void afterUpdate(List<SObject> newOppLineItem, Map<Id, SObject> newOppLineItemMap, Map<Id, SObject> oldOppLineItemMap){
    updateOpportunityonSOVChange((List<OpportunityLineItem>) newOppLineItem,(Map<Id, OpportunityLineItem> )newOppLineItemMap,(Map<Id, OpportunityLineItem> )oldOppLineItemMap);
    
    }
    
    public void updateOpportunityonSOVChange(List<OpportunityLineItem> newOppLineItem,Map<Id, OpportunityLineItem> newOppLineItemMap,Map<Id, OpportunityLineItem> oldOppLineItemMap){
        set<Id> oppId=new Set<Id>();
        for(OpportunityLineItem opplitm:newOppLineItem){
            system.debug('=====stagename==='+opplitm.Opportunity.StageName);
            //if(opplitm.Opportunity.StageName == 'Closed Won'){
            if(opplitm.Reported_Renewal_SOV__c!=oldOppLineItemMap.get(opplitm.id).Reported_Renewal_SOV__c || opplitm.Reported_New_SOV__c!=oldOppLineItemMap.get(opplitm.id).Reported_New_SOV__c){
                oppId.add(opplitm.opportunityId);
            //}
        }
        }
        if(oppId.size()>0){
    List<Opportunity> oppList = [Select Id,Total_Reported_SOV__c,Estimated_MRC__c,QuoteStatus__c, ContractTerm__c,Estimated_NRC__c,Total_Incremental_MRC__c,Product_Type__c,Existing_MRC__c,Calculated_New_SOV__c,Calculated_Renewal_SOV__c,Reported_New_SOV__c,Reported_Renewal_SOV__c,Estimated_TCV__c from Opportunity where id IN:oppId and stagename=:'Closed Won'];
       for(Opportunity opp:oppList){
        opp.Total_Reported_SOV__c=opp.Reported_Renewal_SOV__c+opp.Reported_New_SOV__c;
        
       }
        
        if(oppList.size()>0){update oppList;}
        }   
    }
    /**
      * This method updates SOV Type Order
      */
    //for @testvisible, modified by Anuradha
    @testvisible
        private void upsertSOVDetails(List<OpportunityLineItem> newOppLineItem){
            Map<Id,cscfga__Attribute__c> mapAttr = new Map<Id,cscfga__Attribute__c>();
            set<Id> attIdSet = new Set<Id>();
            Decimal ContractTerm;
            Decimal NetMRC;
            Map<Id,Id> mapprodconfig =new Map<Id,Id>();
            List<csord__Service__c> servList=new List<csord__Service__c>();
            Set<Id> replacedProdConfig=new Set<Id>();
            Map<Id,date> mapProdConfigCntExp=new Map<Id,date>();
             Map<Id,csord__Service__c> mapServiceIdCntExp=new Map<Id,csord__Service__c>();
             Map<String,CurrencyType> CurrRateMap=new Map<String,CurrencyType>();
            Map<Id,boolean> mapparentOrderType=new Map<Id,boolean>();
            Map<Id,integer> mapparentChildCount=new Map<Id,integer>();
            Map<Id,String> mapClonedServiceOrderType=new Map<Id,String>();
            List<CurrencyType> Currencylist=[Select Id,ISOCode,ConversionRate from CurrencyType];
            for(CurrencyType currencytypeObj:Currencylist){
            CurrRateMap.put(currencytypeObj.ISOCode,currencytypeObj);
            }
            Map<Id,String> masterProductConfigOrderType=new Map<Id,String>();
            Map<Id,String> parentProductConfigOrderType=new Map<Id,String>();
            Map<Id,boolean> masterProductConfigProductIds=new Map<Id,boolean>();
            System.debug('CurrRateMap'+ CurrRateMap);
            for(OpportunityLineItem oli :newOppLineItem){
                attIdSet.add(oli.cscfga__Attribute__c);
                // attIdSet.add(oli.opportunityid);
            }
            
            if(attIdSet.size()>0){
            
                List<cscfga__Attribute__c> attList=[select id,cscfga__Product_Configuration__r.CurrencyIsoCode,cscfga__Product_Configuration__r.csordtelcoa__Replaced_Service__c,cscfga__Product_Configuration__r.Parent_Configuration_For_Sequencing__c,cscfga__Product_Configuration__r.Cloning_Parent__c,cscfga__Product_Configuration__r.Linked_Service_ID__c,cscfga__Product_Configuration__r.Associated_Service_ID__c,cscfga__Product_Configuration__r.Root_Parent_Configuration_For_Sequencing__c,cscfga__Product_Configuration__r.EvplId__c,cscfga__Product_Configuration__r.Master_IPVPN_Configuration__c,cscfga__Product_Configuration__r.cscfga__Root_Configuration__r.Master_IPVPN_Configuration__c,cscfga__Product_Configuration__r.Order_Type__c,cscfga__Product_Configuration__r.cscfga__Parent_Configuration__r.Order_Type__c,cscfga__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__r.Offer_new_MRC_c__c,cscfga__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__r.currencyisocode,cscfga__Product_Configuration__r.cscfga__Root_Configuration__r.SOV_Usage__c,cscfga__Product_Configuration__r.SOV_Usage__c,cscfga__Product_Configuration__r.cscfga__Parent_Configuration__c,cscfga__Product_Configuration__r.cscfga__root_Configuration__c,cscfga__Product_Configuration__r.cscfga__root_Configuration__r.Order_type__c,cscfga__Product_Configuration__r.cscfga__Parent_Configuration__r.SOV_Usage__c,cscfga__Product_Configuration__c,cscfga__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c,cscfga__Product_Configuration__r.cscfga__Contract_Term__c, cscfga__Price__c, cscfga__Recurring__c from cscfga__Attribute__c where /*cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__c*/ Id in:attIdSet];
            
            system.debug('====attList===='+attList);
           
system.debug('====mapparentOrderType=='+mapparentOrderType);
                for(cscfga__Attribute__c att :attList){
                    mapAttr.put(att.id,att);
                    if(att.cscfga__Product_Configuration__c !=null && att.cscfga__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c!=null){
                    replacedProdConfig.add(att.cscfga__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c);
                    }
                } 
            } 
              if(replacedProdConfig.size()>0){
                servList=[select id,csordtelcoa__Product_Configuration__r.currencyisocode,Contract_Expiry_Date__c,csordtelcoa__Product_Configuration__c,csordtelcoa__Product_Configuration__r.Offer_new_MRC_c__c from csord__Service__c where csordtelcoa__Product_Configuration__c in:replacedProdConfig];
              for(csord__Service__c servObj:servList){
                mapServiceIdCntExp.put(String.valueOf(servObj.id).substring(0, 15),servObj);
               // mapProdConfigCntExp.put(servObj.csordtelcoa__Product_Configuration__c,servObj.Contract_Expiry_Date__c);
              
              }
                               
                
                
                } 
              
                   system.debug('mapServiceIdCntExp==='+mapServiceIdCntExp);      
             for(OpportunityLineItem oli :newOppLineItem){
                       system.debug('======'+oli.Existing_New_Product_Config__c);    
                if(mapServiceIdCntExp.containskey(oli.Existing_New_Product_Config__c)){
                 oli.Contract_Expiry_Date__c=mapServiceIdCntExp.get(oli.Existing_New_Product_Config__c).Contract_Expiry_Date__c;
                }else{
                  system.debug('===in==='+oli.Existing_New_Product_Config__c);
                oli.Contract_Expiry_Date__c=oli.Closed_Date__c;
                
               }
               
               
               
                if(mapAttr != null && mapAttr.containskey(oli.cscfga__Attribute__c) && oli.Opportunity.StageName != 'Closed Won'){
                    
                    if(mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.cscfga__Parent_Configuration__c!=null){
                        oli.Parent_Config_SOV_Usage__c=mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.cscfga__Root_Configuration__r.SOV_Usage__c;
                    } else{
                        oli.Parent_Config_SOV_Usage__c=mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.SOV_Usage__c;
                    }
                 
                    if(mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.Root_Parent_Configuration_For_Sequencing__c!=null ){
                        oli.Main_Parent_Product_Configuration__c=mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.Root_Parent_Configuration_For_Sequencing__c;
                    }else{
                        oli.Main_Parent_Product_Configuration__c=mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__c;
                    }
                    
                    if(mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.Root_Parent_Configuration_For_Sequencing__c!=null && mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__c==mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.Root_Parent_Configuration_For_Sequencing__c){
                      // cscfga__Product_Configuration__r.cscfga__Parent_Configuration__c
                       //cscfga__Product_Configuration__r.cscfga__root_Configuration__c
                        masterProductConfigOrderType.put(mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__c,mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.Order_type__c);
                   
                  
                   
                    }
                    
                    if(mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.cscfga__Parent_Configuration__c!=null){
                        oli.Parent_Product_Configuration__c=mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.cscfga__Parent_Configuration__c;
                   
                    parentProductConfigOrderType.put(oli.Parent_Product_Configuration__c,mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.cscfga__Parent_Configuration__r.Order_Type__c);
                    }
                                     
                   
                    
                    if(mapAttr.get(oli.cscfga__Attribute__c).cscfga__Recurring__c == false){
                        oli.Contract_Value__c = mapAttr.get(oli.cscfga__Attribute__c).cscfga__Price__c;
                        oli.NetNRCPrice__c = mapAttr.get(oli.cscfga__Attribute__c).cscfga__Price__c;
                        oli.Net_MRC_Price__c = 0.00;
                    }
                    
                    if(mapAttr.get(oli.cscfga__Attribute__c).cscfga__Recurring__c == true){
                        oli.Net_MRC_Price__c =  mapAttr.get(oli.cscfga__Attribute__c).cscfga__Price__c;
                        NetMRC = mapAttr.get(oli.cscfga__Attribute__c).cscfga__Price__c;
                        oli.NetNRCPrice__c = 0.00;
                    }
                    
                    if(mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.cscfga__Contract_Term__c != null){
                        oli.ContractTerm__c = String.ValueOf(mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.cscfga__Contract_Term__c);
                        ContractTerm = mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.cscfga__Contract_Term__c;
                    }

                    if(mapAttr.get(oli.cscfga__Attribute__c).cscfga__Recurring__c == true && ContractTerm == null){
                        oli.Contract_Value__c = 0.00;
                    } else if(mapAttr.get(oli.cscfga__Attribute__c).cscfga__Recurring__c == true && ContractTerm != null && NetMRC != null){
                        oli.Contract_Value__c = ContractTerm * NetMRC;
                    }
                    
                                     // if(mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__c!=null && mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c!=null && mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__r.Offer_new_MRC_c__c!=null){
                   if(mapServiceIdCntExp.containskey(oli.Existing_New_Product_Config__c)){ 
                    
                    Double opprate = CurrRateMap.get(mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.CurrencyIsoCode).ConversionRate;
                    Double serRate = CurrRateMap.get(mapServiceIdCntExp.get(oli.Existing_New_Product_Config__c).csordtelcoa__Product_Configuration__r.currencyisocode).ConversionRate;
                    System.debug('CurrRateMapopprate'+ opprate + 'CurrRateMapserrate' + serRate);
                   system.debug('===='+mapServiceIdCntExp.get(oli.Existing_New_Product_Config__c).csordtelcoa__Product_Configuration__r.currencyisocode+'=====222'+oli.CurrencyIsoCode+'====0000'+mapServiceIdCntExp.get(oli.Existing_New_Product_Config__c).csordtelcoa__Product_Configuration__r.Offer_new_MRC_c__c);
                    oli.Existing_MRC_Calculated__c = (mapServiceIdCntExp.get(oli.Existing_New_Product_Config__c).csordtelcoa__Product_Configuration__r.Offer_new_MRC_c__c * opprate)/serRate;
                }
                                      
                }
            } 
           
 
            system.debug('==mapparentChildCount='+mapparentChildCount);
            if(Util.syncOpp){
             for(OpportunityLineItem oli :newOppLineItem){  
                             
              String linkedService=mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.Linked_Service_ID__c; 
              
              String associatedServiceId=mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.Associated_Service_ID__c; 
                
              system.debug('associatedServiceId==='+associatedServiceId+'===='+linkedService+'==='+masterProductConfigOrderType.get(oli.Main_Parent_Product_Configuration__c));  
            if(oli.order_type__c=='New Provide' && (associatedServiceId==null && linkedService==null)&& (oli.Parent_Product_Configuration__c==null || (oli.Main_Parent_Product_Configuration__c!=null && masterProductConfigOrderType!=null && masterProductConfigOrderType.get(oli.Main_Parent_Product_Configuration__c)=='New Provide'))){
            oli.SOV_Type_of_Order__c='New';
            }else{
            oli.SOV_Type_of_Order__c='Renewal';
            }






           /* if(mapAttr !=null  && oli.cscfga__Attribute__c!=null && mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__c!=null && mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.Linked_Service_ID__c!=null || mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.Associated_Service_ID__c!=null || (mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.Cloning_Parent__c!=null && mapClonedServiceOrderType!=null && (mapClonedServiceOrderType.get(mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.Cloning_Parent__c)=='Parallel Upgrade' || mapClonedServiceOrderType.get(mapAttr.get(oli.cscfga__Attribute__c).cscfga__Product_Configuration__r.Cloning_Parent__c)=='Parallel Downgrade'))){
            oli.SOV_Type_of_Order__c = 'Renewal';
                
            } */
                           
          }  
            }
          

        }
    
    /**
      * This method updates Reported New SOV and Reported Renewal SOV with Calculated New SOV and Calculated Renewal SOV respectively
      */
    @testvisible
    private void updateReportedSOV(List<OpportunityLineItem> newOppLineItem, Map<Id, OpportunityLineItem> newOppLineItemMap, Map<Id, OpportunityLineItem> oldOppLineItemMap){
          
          for(OpportunityLineItem oli : newOppLineItem){
            system.debug(oli.Reported_New_SOV__c+'===='+oldOppLineItemMap.get(oli.Id).Reported_New_SOV__c);
            if(oli.Reported_New_SOV__c!=null && oldOppLineItemMap!=null && oli.Reported_New_SOV__c == oldOppLineItemMap.get(oli.Id).Reported_New_SOV__c){
               
               
                oli.Reported_New_SOV__c = oli.Calculated_New_SOV__c;
                System.debug('I am in for reported new SOV'+ oli.Id + '---' + oli.Reported_New_SOV__c);
            }
           if(oli.Reported_Renewal_SOV__c!=null && oldOppLineItemMap!=null && oli.Reported_Renewal_SOV__c== oldOppLineItemMap.get(oli.Id).Reported_Renewal_SOV__c){
                oli.Reported_Renewal_SOV__c = oli.Calculated_Renewal_SOV__c;
                System.debug('I am in for reported renewal SOV'+ oli.Id + '---' + oli.Reported_Renewal_SOV__c);
            }
            
          oli.Calculated_Total_SOV__c=  oli.Calculated_Renewal_SOV__c==null?0:oli.Calculated_Renewal_SOV__c+  oli.Calculated_New_SOV__c==null?0:oli.Calculated_New_SOV__c; 

           //oli.Reported_Total_SOV__c=  oli.Reported_Renewal_SOV__c+  oli.Reported_New_SOV__c;  
      }
    
    }  
public void assignReportedSOV(List<OpportunityLineItem> newItems){
   for(OpportunityLineItem oli : newItems){
              
                oli.Reported_New_SOV__c = oli.Calculated_New_SOV__c;
                System.debug('I am in for reported new SOV');
          
                oli.Reported_Renewal_SOV__c = oli.Calculated_Renewal_SOV__c;
                System.debug('I am in for reported renewal SOV');
            
           oli.Calculated_Total_SOV__c=  oli.Calculated_Renewal_SOV__c==null?0:oli.Calculated_Renewal_SOV__c+  oli.Calculated_New_SOV__c==null?0:oli.Calculated_New_SOV__c; 
          // oli.Reported_Total_SOV__c=  oli.Reported_Renewal_SOV__c+  oli.Reported_New_SOV__c;  
                   
      }
  
}
         @testvisible
        private void upsertPriceBookEntries(List<OpportunityLineItem> items) {
        List<Id> oppsIds = new List<Id>();
        List<Id> prodIds= new List<Id>();
        for (OpportunityLineItem item: items) {
            oppsIds.add(item.OpportunityId);
            prodIds.add(item.Product2Id);
        }
        Map<Id, Opportunity> opptyMap = getOpportunityMap(oppsIds);
        Map<String, PriceBookEntry> productPriceMap = getProductPriceMap(prodIds);
        List<PriceBookEntry> toInsert = new List<PriceBookEntry>();
        for (OpportunityLineItem item: items) {
            Opportunity oppty = opptyMap.get(item.OpportunityId);  
            String key = getProductPriceBookCurrencyKey(StdPriceBookId, item.Product2Id, oppty.CurrencyIsoCode);
            if (productPriceMap.containsKey(key) == false) {
                PriceBookEntry pbe = new PriceBookEntry(CurrencyIsoCode = oppty.CurrencyIsoCode
                    , Pricebook2Id = StdPriceBookId
                    , Product2Id = item.Product2Id
                    , UnitPrice = item.UnitPrice
                    , IsActive = true);  
                toInsert.add(pbe);
                productPriceMap.put(key, pbe);
            }  
        }
        if(!Test.isRunningTest()){
        insert toInsert;
        }
    }
     @testvisible
    private void updatePriceBookEntriesReferences(List<OpportunityLineItem> items) {
        List<Id> oppsIds = new List<Id>();
        List<Id> prodIds= new List<Id>();
        for (OpportunityLineItem item: items) {
            oppsIds.add(item.OpportunityId);
            prodIds.add(item.Product2Id);
        }
        Map<Id, Opportunity> opptyMap = getOpportunityMap(oppsIds);
        Map<String, PriceBookEntry> productPriceMap = getProductPriceMap(prodIds);
        for (OpportunityLineItem item: items) {
            Opportunity oppty = opptyMap.get(item.OpportunityId);  
            String key;
            if(!Test.isRunningTest()){
                key = getProductPriceBookCurrencyKey(StdPriceBookId, item.Product2Id, oppty.CurrencyIsoCode);
                item.PriceBookEntryId = productPriceMap.get(key).Id;
            }
        }
    }
    @testvisible
    private Map<String, PriceBookEntry> getProductPriceMap(List<Id> productIds) { 
        Map<String, PriceBookEntry> productPriceMap = new Map<String, PriceBookEntry>();
        for (PriceBookEntry pbe: [select Id, Product2Id, Pricebook2Id, UnitPrice, CurrencyIsoCode from PriceBookEntry where Product2Id in: productIds]) {  
            String key = getProductPriceBookCurrencyKey(pbe.Pricebook2Id, pbe.Product2Id, pbe.CurrencyIsoCode); 
            productPriceMap.put(key, pbe);
        }
        return productPriceMap;
    }
   @testvisible
    private Map<Id, Opportunity> getOpportunityMap(List<Id> opportunityIds) {
        return new Map<Id, Opportunity> ([select Id, CurrencyIsoCode, Pricebook2Id from Opportunity where Id in: opportunityIds]);
    }
   @testvisible
    private string getProductPriceBookCurrencyKey(Id priceBookId, Id product2Id, string currencyIsoCode) {
        return priceBookId + '|' + product2Id + '|' + currencyIsoCode;
    }

}