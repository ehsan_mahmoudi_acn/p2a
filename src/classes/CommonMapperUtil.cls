/**
    @author - Accenture
    @date - 7-Aug-2012
    @version - 1.0
    @description - This class is a util class to create and update records using common mapper.
*/

public class CommonMapperUtil {
    private class CommonMapperUtilException extends Exception {}
    
    //public CommonMapperUtil (){}
    public static sObject createUpdateRecords(String srcObjName, String destObjName, sObject srcRecord, String dmlType, sObject destRecord, Boolean insertNull){
        
        if(srcRecord.getSObjectType()!= Schema.getGlobalDescribe().get(srcObjName)){
            
            throw new CommonMapperUtilException(System.Label.Common_Mapper_SourceObj_Error);
        }
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(destObjName);
        if(targetType == null)
            throw new CommonMapperUtilException(System.Label.Common_Mapper_DesObj_Error);
            
    try{
        //List<Common_Mapper__c> cm = [select Source_Field_API_Name__c, Destination_Field_API_Name__c from Common_Mapper__c where Source_Object_API_Name__c =:srcObjName and Destination_Object_API_Name__c =:destObjName];
        
        sObject sObj = null;
        if(dmlType == System.Label.Create_DML_Type)
            sObj = targetType.newSObject();
        else if(dmlType == System.Label.Update_DML_Type){
            if(destRecord == null)
                throw new CommonMapperUtilException(System.Label.Common_Mapper_NullObj_Error);
            
            sObj = destRecord;
        }
        else
            throw new CommonMapperUtilException(System.Label.Common_Mapper_DML_Error);
         
         Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(destObjName.toLowerCase()).getDescribe().Fields.getMap();
            
        for(Common_Mapper__c fld : [select Source_Field_API_Name__c, Destination_Field_API_Name__c from Common_Mapper__c where Source_Object_API_Name__c =:srcObjName and Destination_Object_API_Name__c =:destObjName]){
            if((dmlType == System.Label.Create_DML_Type && fMap.get(fld.Destination_Field_API_Name__c).getDescribe().isCreateable()) || (dmlType == System.Label.Update_DML_Type && fMap.get(fld.Destination_Field_API_Name__c).getDescribe().isUpdateable()))
                if(insertNull == true)
                    sObj.put(fld.Destination_Field_API_Name__c, srcRecord.get(fld.Source_Field_API_Name__c));
                else
                    if(srcRecord.get(fld.Source_Field_API_Name__c) != null)
                        sObj.put(fld.Destination_Field_API_Name__c, srcRecord.get(fld.Source_Field_API_Name__c));
        }
            
        return sObj;
        }
        /*catch(QueryException qex){
            
            CreateApexErrorLog.InsertHandleException(System.Label.error_log1,System.Label.error_log2,qex.getMessage(),System.Label.error_log3,Userinfo.getName());
            qex.setMessage(System.Label.Common_Mapper_Util_Excep +srcObjName + System.Label.Common_Mapper_Excep +destObjName);
            throw new CommonMapperUtilException(qex.getMessage());
        }
        catch(DMLException dex){
            
            CreateApexErrorLog.InsertHandleException(System.Label.error_log1,System.Label.error_log2,dex.getMessage(),System.Label.error_log3,Userinfo.getName());
            dex.setMessage(System.Label.Common_Mapper_Execp1 +srcObjName +System.Label.Common_Mapper_Excep +destObjName);
            throw new CommonMapperUtilException(dex.getMessage());
        }
        */
        catch(Exception ex){
            
            CreateApexErrorLog.InsertHandleException(System.Label.error_log1,System.Label.error_log2,ex.getMessage(),System.Label.error_log3,Userinfo.getName());
            ex.setMessage(System.Label.Common_Mapper_Excep2 + srcObjName + System.Label.Common_Mapper_Excep + destObjName);
            throw new CommonMapperUtilException(ex.getMessage());
        }        
        
    }
    
    public static sObject createUpdateRecords(String srcObjName, String destObjName, sObject srcRecord){
        return createUpdateRecords(srcObjName, destObjName, srcRecord, System.Label.Create_DML_Type, null);
    }
    
    public static sObject createUpdateRecords(String srcObjName, String destObjName, sObject srcRecord, String dmlType, sObject destRecord){
        return createUpdateRecords(srcObjName, destObjName, srcRecord, dmlType, destRecord, true);
    }
}