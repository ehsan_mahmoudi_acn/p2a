/**
* @author Telstra Accenture
* @description This batch class populates the MRC and NRC Prices/Cost from Pricing case to Product congfiguration
*/

public class ApproveCaseCtrlBatch implements Database.Batchable<Pricing_Approval_Request_Data__c>, Database.Stateful{
    
    private Integer m_counter;
    private Id m_basketId;
    private Map<Id, cscfga__Product_Configuration__c> m_finalUpdateMap;
    private Map<String, List<custom_object_to_product_configuration__c>> m_cstObjToProdCfgMap;
    private Id rootConfigIdforPrice;

    private static string key15(Id sfdcId){
        return 
            string.valueOf(sfdcId).substring(0, 15);
    }

/**
* @author Telstra Accenture
* @description This method is used to initilaize the data
*/
    private void initContextData(){
        
        /** Initialize custom object to product configuration*/
        this.m_finalUpdateMap = new Map<Id, cscfga__Product_Configuration__c>();
        this.m_cstObjToProdCfgMap = new Map<String, List<custom_object_to_product_configuration__c>>();
        this.rootConfigIdforPrice = null;
        
        /** Get the 'custom object to product configuration' setting records*/
        /*List<custom_object_to_product_configuration__c> mcs = [Select id
                , Product_Name__c
                , Source_Field__c
                , Source_Object__c
                , Production_Configuration_Attribute_Name__c 
            from 
                custom_object_to_product_configuration__c 
            where 
                Product_Name__c 
            like 
                '%Pricing%'];*/
        
        /** Populate 'custom object to product configuration' by product name map*/
        String productName;
        for(custom_object_to_product_configuration__c obj :[Select id, Product_Name__c, Source_Field__c, Source_Object__c , Production_Configuration_Attribute_Name__c from custom_object_to_product_configuration__c where Product_Name__c like '%Pricing%']){
            productName = obj.product_name__c.trim().toLowercase();
            if(this.m_cstObjToProdCfgMap.containsKey(productName) == false){
                this.m_cstObjToProdCfgMap.put(productName, new List<custom_object_to_product_configuration__c>());
            }
            this.m_cstObjToProdCfgMap.get(productName).add(obj);
        } 
    }
  
/**
* @author Telstra Accenture
* @description Throws an exception in case of basket is blank
* @param Id basket Id
*/  
    public ApproveCaseCtrlBatch(Id basketId){
        if(basketId == null){
            throw new ApproveCaseCtrlBatchException('Basket Id should not be Empty!');
        }
        this.m_counter = 0;
        this.m_basketId = basketId;
    }

/**
* @author Telstra Accenture
* @description iterable data, that will be processed by the steps of the batch
*/
    
    public Iterable<Pricing_Approval_Request_Data__c> start(Database.BatchableContext ctx){
        
        /** Initialize context data, that will be shared throgh the all the batch steps*/
        initContextData();
        
        /** Return the iterable data, that will be processed by the steps of the batch.*/
        return 
            [select Id
                , Product_Configuration__r.cscfga__Product_Family__c
                , Product_Configuration__r.name
                , Product_Basket__c
                , Product_Configuration__c
                , Product_Configuration__r.Product_Name__c
                , Product_Configuration__r.cscfga__Product_Basket__c
                , Product_Configuration__r.cscfga__Configuration_Status__c 
                , Product_Configuration__r.cscfga__Root_Configuration__c
                , Act_Approved_NRC__c
                , Act_Approved_RC__c
                , Cost_NRC__c
                , Cost_RC__c
                , Act_Approved_Burst_Rate__c
                , Act_Approved_China_Burst_Rate__c
                ,Usage_Charge__c
            from 
                Pricing_Approval_Request_Data__c 
            where 
                Product_Basket__c = :this.m_basketId 
                    and Product_Configuration__r.cscfga__Product_Family__c != null ORDER BY Product_Configuration__r.cscfga__Root_Configuration__c DESC NULLS LAST];  
    }
 

/**
* @author Telstra Accenture
* @description Execute the batch to populate the details in the product connfiguration
*/ 
    public void execute(Database.BatchableContext BC, List<Pricing_Approval_Request_Data__c> requests){
        m_counter++;

        /** Intializing to 'true', this flag will stop execution of the method 'AttributeValueIsChanged'. Details can be found in the 'CheckRecursive' class. **/
        CheckRecursive.AttributeValueIsChangedDontExecute = true;
        
        /** Check the number of items, throw if more than 1, due to heavy workload*/
        Pricing_Approval_Request_Data__c request = null;
        if(requests.size() == 1){
            request = requests[0];
        } else if(requests.size() > 1){
            throw new ApproveCaseCtrlBatchException('Set the step size on 1 when executing this batch!');
        }

        Id configId = request.Product_Configuration__c;
        Id rootConfigId = request.Product_Configuration__r.cscfga__Root_Configuration__c != null 
            ? request.Product_Configuration__r.cscfga__Root_Configuration__c 
            : request.Product_Configuration__c;
        this.rootConfigIdforPrice=rootConfigId;        
       
        /** Obtained the product name*/
        String productName = (request.Product_Configuration__r.cscfga__Product_Family__c + 'PricingApproval').trim().tolowerCase();
        
        /** Get the attributes from custom setting for this product into a set*/        
        //List<custom_object_to_product_configuration__c> mcs = [Select Id, Product_Name__c, Source_Field__c, Source_Object__c,Production_Configuration_Attribute_Name__c from custom_object_to_product_configuration__c where Product_Name__c = :productName];
        Map<String, String> configToAttrMap = new Map<String, String>();

        for(custom_object_to_product_configuration__c  iterator :[Select Id, Product_Name__c, Source_Field__c, Source_Object__c,Production_Configuration_Attribute_Name__c from custom_object_to_product_configuration__c where Product_Name__c = :productName]){        
            String setValue = String.valueof(request.get(iterator.Source_Field__c));
            if(iterator.Production_Configuration_Attribute_Name__c != null){
                configToAttrMap.put(iterator.Production_Configuration_Attribute_Name__c,setValue);
            } 
        }
                    
        /** Get the list of all attributes for root PC*/
        //List<cscfga__Attribute__c> AttrList = [select Name, cscfga__value__c, cscfga__Display_Value__c, cscfga__price__c, Product_Basket_CurrencyISOCode__c, Attribute_Value_IsChanged__c from cscfga__Attribute__c where cscfga__Product_Configuration__c =:configId];
        List<cscfga__Attribute__c> AttrUpdate = new List<cscfga__Attribute__c>();
        
        /** Update the override attributes and their prices*/
        for(cscfga__Attribute__c attr :[select Name, cscfga__value__c, cscfga__Display_Value__c, cscfga__price__c, Product_Basket_CurrencyISOCode__c, Attribute_Value_IsChanged__c from cscfga__Attribute__c where cscfga__Product_Configuration__c =:configId]){
            String tempname = attr.Name;
            if(configToAttrMap.containsKey(tempname)){
                attr.cscfga__value__c = configToAttrMap.get(tempname);
                attr.cscfga__Display_Value__c = configToAttrMap.get(tempname);
                attr.cscfga__price__c = configToAttrMap.get(tempname)!=null?Decimal.valueOf(configToAttrMap.get(tempname)):attr.cscfga__price__c;
                attr.CurrencyIsoCode = attr.Product_Basket_CurrencyISOCode__c;
                attr.Attribute_Value_IsChanged__c = false;
                AttrUpdate.add(attr);
            } 
            else if(attr.Attribute_Value_IsChanged__c){
                attr.Attribute_Value_IsChanged__c = false;
                AttrUpdate.add(attr);
            }
        }
        if(AttrUpdate.size() > 0){update AttrUpdate;}         
                 
        /** Force valid status of the product configuration, get all the childs products, through all the levels downwards, and force valid status*/
        for(cscfga__Product_Configuration__c pc :[select Id 
            from 
                cscfga__Product_Configuration__c 
            where 
                cscfga__Product_Basket__c =: this.m_basketId and
                (Id = :configId or 
                cscfga__Root_Configuration__c = :configId or 
                Id = :rootConfigId or 
                cscfga__Root_Configuration__c = :rootConfigId)]){
            this.m_finalUpdateMap.put(pc.Id
                , new cscfga__Product_Configuration__c (Id=pc.Id, cscfga__Configuration_Status__c='Valid', PricingAttributeValueChanged__c=false));
        }   
    }      
/**
* @author Telstra Accenture
* @description Finish the batch
*/   
   
    public void finish(Database.BatchableContext BC){

        if(this.m_finalUpdateMap.size() > 0){
            TriggerFlags.NoProductConfigurationTriggers = true;
            TriggerFlags.NoAttributeTriggers = true;
            update this.m_finalUpdateMap.values();
            TriggerFlags.NoProductConfigurationTriggers=false;
            cscfga.ProductConfigurationBulkActions.calculateTotals(new Set<Id> {this.m_basketId});
            
            /** Update Quote Status of the product basket to Approved.*/
            cscfga__Product_Basket__c bskt = new cscfga__Product_Basket__c(Id=this.m_basketId, Quote_Status__c='Approved');
            update bskt;
            /*Sync Opportunity to Product basket*/
            CustomButtonSynchronizeWithOpportunity.skipValidation =true;
			CustomButtonSynchronizeWithOpportunity sync2ProdBasket = new CustomButtonSynchronizeWithOpportunity();
            string syncMessage=sync2ProdBasket.performAction(bskt.id);
            CustomButtonSynchronizeWithOpportunity.skipValidation = false;
        }
        /** Initialization back to 'false', execution of the method 'AttributeValueIsChanged' is released now. Details can be found in the 'CheckRecursive' class. **/
        CheckRecursive.AttributeValueIsChangedDontExecute = false;
    }
    private class ApproveCaseCtrlBatchException extends Exception {}
    
}