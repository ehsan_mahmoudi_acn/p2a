global class SubmitOrderBatch implements Database.Batchable<csord__order__c>, Database.AllowsCallouts{ 

    public List<Id> OrderIdList{get;set;}
    public Set<Id> MasterOrderSet{get;set;}
    public boolean parallelOrder{get;set;}
    public boolean isChildSubmitible{get;set;}
    public boolean isBatchComplete{get;set;}
    public solutions__c solnObj{get;set;}
    public Id solutionId{get;set;}

    /** Constructor **/
    global SubmitOrderBatch(List<Id> OrderIdList, Set<Id> MasterOrderSet, boolean parallelOrder, boolean isChildSubmitible){
        this.OrderIdList = OrderIdList;
        this.MasterOrderSet = MasterOrderSet;
        this.parallelOrder = parallelOrder;
        this.isChildSubmitible = isChildSubmitible;
        isBatchComplete = isChildSubmitible;
        csord__order__c orderToSolution = [select Id, Solutions__c from csord__order__c where Id =:OrderIdList[0]];
        solnObj = [Select Id, batch_count__c, Batch_Job_Id__c from solutions__c where Id =:orderToSolution.Solutions__c];
        solnObj.batch_count__c = OrderIdList.Size();
        update solnObj;
        solutionId = solnObj.Id;
    }
    
    /** Start Method: will return list of order Sobject **/
    global List<csord__order__c> start(Database.BatchableContext BC){        
        return [select Id, Solutions__c, Is_Order_Submitted__c from csord__order__c where Id IN :MasterOrderSet];
    }
    
    /** Execute method will process the list of order sobject and submit the order **/
    global void execute(Database.BatchableContext BC, List<csord__order__c> orderSet){   

        List<csord__order__c> updateOrderList = new List<csord__order__c>();
            for(csord__order__c ord :orderSet){
                try{
                    if(MasterOrderSet.contains(ord.Id) && !ord.Is_Order_Submitted__c){
                       SubmitOrder.submitOrderToFOMFromBatch(ord.Id); /** Submission of Orders **/
                       ord.Is_Order_Submitted__c = true;
                       ord.Status__c = 'Submitted'; 
                       ord.Clean_Order_Check_Passed__c = true;
                       updateOrderList.add(ord);  
                    }
                } catch(Exception e){
                  ErrorHandlerException.ExecutingClassName='SubmitOrderBatch:execute';
                  ErrorHandlerException.objectList= orderSet;
                  ErrorHandlerException.sendException(e);
                  System.debug(e);
               }
            }
    
        if(updateOrderList.Size()>0){update updateOrderList;}
        
        if(parallelOrder){
            SleepBatch sb = new sleepBatch();
            sb.sleep(45000);
        }
    }

    /** Final method will be executed when batch is finished **/
    global void finish(Database.BatchableContext BC){
        if(isBatchComplete){
            solnObj = [Select Id, Name, Account_Name__r.name, Opportunity_Name__r.Name, batch_count__c, Submitted_Orders__c from solutions__c where Id =:solutionId];
            solnObj.batch_count__c = 0;
            update solnObj;
            SolEmailTempCtrl.SendEmail(solnObj); 
            System.debug('Execution Finished'); 
        }
    }

    private class SleepBatch{
        public void sleep(Long inputTime){
            Long startTime = System.currentTimeMillis();
            Long startTime1 = startTime+inputTime;
            Long startTime2 = 0;
            while(startTime2 < startTime1){
                startTime2 = System.currentTimeMillis();
            }
        }
    }

    private class SubmitOrderBatchException extends Exception {}

}