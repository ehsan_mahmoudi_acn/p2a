global class AmendOrder {
    public ID orderId {get;set;}
  
    @future (callout=true)
    public static void amendOrderToFOM(ID OrderId){
        TibcoServiceOrderCreator orderCreator = new TibcoServiceOrderCreator();
        TibcoServiceOrder amendOrder = orderCreator.CreateOrderFromServiceId(orderId);
        TibcoController controller1 = new TibcoController();
        String result = controller1.SubmitServiceOrder(amendOrder,Label.AmendOrder, 'AmendOrderResponse', 'AmendOrderRequest',Label.AmendOrderRequest);
    }
}