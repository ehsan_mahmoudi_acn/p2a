public class OrderAndServiceFactory {
    public static Map<Id, OrderAndServices> makeOrderAndServicesMapFromOrderIdSet(set<Id> orderIdSet)
    {
        Set<Id> orderIdList = new Set<Id>();
        for (Id orderId : orderIdSet)
        {
            orderIdList.add(orderId);
        }
        List<OrderAndServices> oandslist = makeOrderAndServicesFromOrders(orderIdList);
        Map<Id, OrderAndServices> oandsMap = new Map<Id, OrderAndServices>();
        for (OrderAndServices oands : oandslist)
        {
            oandsMap.put(oands.m_order.Id, oands);
        }
        return oandsMap;
    }
    public static List<OrderAndServices> makeOrderAndServicesFromOrders(set<Id> orderIds)
    {
        /*List<csord__Order__c> orders = [SELECT 
                                            Id,
                                            Customer_Required_Date__c
                                        FROM 
                                            csord__Order__c 
                                        WHERE 
                                            Id in :orderIds];*/
        List<csord__Service__c> services = [SELECT 
                                                Id, 
                                                csord__Subscription__r.csord__order__c 
                                            FROM 
                                                csord__Service__c 
                                            WHERE 
                                                csord__Subscription__r.csord__Order__c in :orderIds];
        List<cspofa__Orchestration_Process__c> orderProcesses = GetOrderProcessesFromIds(orderIds);
        List<Id> serviceIds = new List<Id>();
        for (csord__Service__c service : services)
        {
            serviceIds.add(service.Id);
        }
        List<cspofa__Orchestration_Process__c> serviceProcesses = GetServiceProcessesFromIds(serviceIds);
        List<cspofa__Orchestration_Step__c> orderSteps = GetProcessStepsFromProcesses(orderProcesses);
        List<cspofa__Orchestration_Step__c> serviceProcessSteps = GetProcessStepsFromProcesses(serviceProcesses);
        System.Debug('Got Service process steps '+serviceProcesses + ' = ' + serviceProcessSteps);
        List<OrderAndServices> result = new List<OrderAndServices>();
        
        Map<Id, List<csord__Service__c>> order2ServiceMap = new Map<Id, List<csord__Service__c>>();
        Map<Id, CSPOFA__Orchestration_Process__c> order2OrderProcessMap = new Map<Id, CSPOFA__Orchestration_Process__c>();
        Map<Id, List<CSPOFA__Orchestration_Process__c>> order2ServiceProcessesMap = new Map<Id, List<CSPOFA__Orchestration_Process__c>>();
        Map<Id, List<cspofa__Orchestration_Step__c>> order2OrderProcessStepMap = new Map<Id, List<cspofa__Orchestration_Step__c>>();
        Map<Id, List<cspofa__Orchestration_Step__c>> service2ServiceProcessStepMap = new Map<Id, List<cspofa__Orchestration_Step__c>>();
        for (csord__Service__c service : services)
        {
            Id orderId = service.csord__Subscription__r.csord__order__c;
            if (!order2ServiceMap.containsKey(orderId))
            {
                order2ServiceMap.put(orderId, new List<csord__Service__c>());
            }
            List<csord__Service__c> serviceList = order2ServiceMap.get(orderId);
            serviceList.add(service);
        }
        for (CSPOFA__Orchestration_Process__c process : orderProcesses)
        {
            order2OrderProcessMap.put(process.Order__r.Id, process);
        }
        for (CSPOFA__Orchestration_Process__c process : serviceProcesses)
        {
            Id orderId = process.csordtelcoa__Service__r.csord__Subscription__r.csord__Order__r.Id;
            if (!order2ServiceProcessesMap.containsKey(orderId))
            {
                order2ServiceProcessesMap.put(orderId, new List<CSPOFA__Orchestration_Process__c>());
            }
            List<CSPOFA__Orchestration_Process__c> processes = order2ServiceProcessesMap.get(orderId);
            processes.add(process);
        }
        for (cspofa__Orchestration_Step__c step : orderSteps)
        {
            Id orderProcessId = step.CSPOFA__Orchestration_Process__r.Id;
            if (ProcessStepTimeManager.IncludeInPathView(step))
            {
                if (!order2OrderProcessStepMap.containsKey(orderProcessId))
                {
                    order2OrderProcessStepMap.put(orderProcessId,new List<cspofa__Orchestration_Step__c>());
                }
                List<cspofa__Orchestration_Step__c> steps = order2OrderProcessStepMap.get(orderProcessId);
                steps.add(step);
            }
        }
        for (cspofa__Orchestration_Step__c step : serviceProcessSteps)
        {
            Id serviceProcessId = step.CSPOFA__Orchestration_Process__r.Id;
            if (ProcessStepTimeManager.IncludeInPathView(step))
            {
                if (!service2ServiceProcessStepMap.containsKey(serviceProcessId))
                {
                    service2ServiceProcessStepMap.put(serviceProcessId,new List<cspofa__Orchestration_Step__c>());
                }
                List<cspofa__Orchestration_Step__c> steps = service2ServiceProcessStepMap.get(serviceProcessId);
                steps.add(step);
            }
        }
        
        for (csord__Order__c order : [SELECT Id, Customer_Required_Date__c FROM csord__Order__c WHERE Id in :orderIds])
        {

            List<csord__Service__c> servicesOnOrder = order2ServiceMap.get(order.Id);
            CSPOFA__Orchestration_Process__c orderProcessOnOrder = order2OrderProcessMap.get(order.Id);
            List<CSPOFA__Orchestration_Process__c> serviceProcessesOnOrder = order2ServiceProcessesMap.get(order.Id);
            OrderAndServices os = new OrderAndServices(order, servicesOnOrder, orderProcessOnOrder, serviceProcessesOnOrder, order2OrderProcessStepMap, service2ServiceProcessStepMap);
            result.add(os);
        }
        return result;
    }
    
    
    /* BB updated: 03/05/2016 - begin */
    public static void makeOrderAndServicesFromServices(List<Id> serviceIds) {
        if (System.isFuture() || System.isBatch() || System.isScheduled()) {
            makeOrderAndServicesFromServicesBase(serviceIds);            
        } else {
            makeOrderAndServicesFromServicesFuture(serviceIds);
        }
    }
    
    @future
    private static void makeOrderAndServicesFromServicesFuture(List<Id> serviceIds) {
        System.Debug('makeOrderAndServicesFromServicesFuture >>> '+serviceIds);
        makeOrderAndServicesFromServicesBase(serviceIds);
    }
    
    public static void makeOrderAndServicesFromServicesBase(List<Id> serviceIds) {
        System.Debug('makeOrderAndServicesFromServicesBase >>> '+serviceIds);
        List<csord__Service__c> services = [SELECT csord__Subscription__r.csord__Order__r.Id FROM csord__Service__c WHERE id in :serviceIds];
        set<Id> orderIds = new set<Id>();
        for (csord__Service__c service : services) {
            orderIds.add(service.csord__Subscription__r.csord__Order__r.Id);
        }
        List<OrderAndServices> oandslist = makeOrderAndServicesFromOrders(orderIds);
        for (OrderAndServices oands : oandslist) {
            ProcessStepTimeManager.SetInitialETCForOrdersAndService(oands);
        }
    }
    
    public class OrderAndServicesFromServicesWorker implements IWorker {
        public void work(Object params, Object context) {
            makeOrderAndServicesFromServicesBase((List<Id>)params);
        }
    }
    
    /* BB updated: 03/05/2016 - end */
    
    
    
    @future
    public static void makeOrderAndServicesFromOrdersFuture(Set<Id> orderIds)
    {
        List<OrderAndServices> oandslist = makeOrderAndServicesFromOrders(orderIds);
        for (OrderAndServices oands : oandslist)
        {
            ProcessStepTimeManager.SetInitialETCForOrdersAndService(oands);
        }
    }
    
    public static List<CSPOFA__Orchestration_Process__c> getServiceProcessesFromIds(List<Id> serviceIds)
    {
                return [SELECT 
                        Id, 
                        CreatedDate,
                        csordtelcoa__Service__r.Id, 
                        csordtelcoa__Service__r.Estimated_Start_Date__c,
                        csordtelcoa__Service__r.csord__Subscription__r.csord__Order__r.Id,
                        Start_Date_Time__c,
                        End_Date_Time__c,
                        Estimated_Time_To_Complete__c
                        FROM 
                        cspofa__Orchestration_Process__c 
                        WHERE 
                        csordtelcoa__Service__r.Id in :serviceIds];
    }
    public static List<CSPOFA__Orchestration_Process__c> getOrderProcessesFromIds(Set<Id> orderIds)
    {
                return [SELECT 
                        Id, 
                        Order__r.Id, 
                        CreatedDate, 
                        Order__r.Customer_Required_Date__c,
                        Estimated_Time_To_Complete__c
                        FROM 
                        cspofa__Orchestration_Process__c 
                        WHERE 
                        Order__c in :orderIds];
    }
    public static List<CSPOFA__Orchestration_Step__c> getProcessStepsFromProcesses(List<CSPOFA__Orchestration_Process__c> processes)
    {
        List<CSPOFA__Orchestration_Step__c> steps = [SELECT 
                                                     Id, 
                                                     Name, 
                                                     CSPOFA__Orchestration_Process__r.Id,
                                                     CSPOFA__Orchestration_Step_Template__r.Name,
                                                     CSPOFA__Orchestration_Step_Template__r.Estimated_Time_To_Complete__c,
                                                     CSPOFA__Orchestration_Step_Template__r.CSPOFA__OLA_Unit__c,
                                                     CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c,
                                                     Start_Date_Time__c,
                                                     End_Date_Time__c,
                                                     CSPOFA__Status__c,
                                                     Estimated_Time_To_Complete__c,
                                                     OLA__c
                                                     FROM 
                                                     cspofa__Orchestration_Step__c 
                                                     WHERE 
                                                     CSPOFA__Orchestration_Process__r.Id in :processes];
        System.Debug('Found steps '+steps+' on processes '+processes);
        return steps;
    }
}