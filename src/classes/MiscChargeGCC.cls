/**
@author - Accenture
@date - 7-April-2017
@version - 1.0
@description - This is Extension class for Miscellaneous Credit(GCC Product).
*/

public with sharing class MiscChargeGCC{
    
    public MiscChargeGCC(){}
    public csord__Service_Line_Item__c miscChargeSLI{get;set;}
    public Id srvcId {get;set;}
    public String serviceName{get;set;}
    public Decimal chargeAmount{get;set;}
    public Decimal cost{get;set;}
    public String billText{get;set;}
    public String currencyName{get;set;}
    public String typeofCharge{get;set;}
    public String purchaseOrder{get;set;}
    public String Frequency{get;set;}
    public String billProfileName{get;set;}
    public Id billProfileId{get;set;}
    public String billingCommDate{get;set;}
    public Id CostCentreId{get;set;}
    public String CostCentreName{get;set;}
    public String typeofMAC{get;set;}
    public String serviceReferenceNo{get;set;}
    public Boolean zeroFlag{get;set;}
    public Boolean displayLineItemFlag{get;set;}
    public String chargeAction{get;set;}
    public String chargeID{get;set;}
    public String U2CMasterCode{get;set;}
    public String OrderRequest{get;set;}
    public String Identification{get;set;}
    public Id accId{get;set;}   
    public List<SelectOption> typeofChargeOptions{get;set;}    
    public List<SelectOption> frequencyOptions{get;set;}
    public List<SelectOption> typeofMACOptions{get;set;}
    public List<MISC_Chargeable_Product__c> miscChargeableList{get;set;}

    public MiscChargeGCC(ApexPages.StandardController controller){
        this.srvcId= Apexpages.currentPage().getParameters().get('id');
        //List<csord__Service__c> srvcList = [Select Name,Service_Bill_Text__c,CurrencyIsoCode,Bill_ProfileId__c,Bill_ProfileId__r.Name,Cost_Centre_Id__c,Cost_Centre_Id__r.Name,csord__Order_Request__c,csord__Identification__c,AccountId__c from csord__Service__c where Id =:srvcId limit 1];
        miscChargeableList = [Select Name,One_Time_Charge_Amount__c,One_Time_Charge_Text__c,Product_Code__c From MISC_Chargeable_Product__c Order By Name Asc];
        
        for(csord__Service__c srvc :[Select Name,Service_Bill_Text__c,CurrencyIsoCode,Bill_ProfileId__c,Bill_ProfileId__r.Name,Cost_Centre_Id__c,Cost_Centre_Id__r.Name,csord__Order_Request__c,csord__Identification__c,AccountId__c from csord__Service__c where Id =:srvcId limit 1]){
            if(srvc.Name != null){serviceName = srvc.Name;}
            if(srvc.Service_Bill_Text__c != null){billText = srvc.Service_Bill_Text__c;}
            if(srvc.CurrencyIsoCode != null){currencyName = srvc.CurrencyIsoCode;}
            if(srvc.Bill_ProfileId__c != null){billProfileId = srvc.Bill_ProfileId__c; billProfileName = srvc.Bill_ProfileId__r.Name;}
            if(srvc.Cost_Centre_Id__c != null){CostCentreId = srvc.Cost_Centre_Id__c; CostCentreName = srvc.Cost_Centre_Id__r.Name;}
            if(srvc.csord__Order_Request__c != null){OrderRequest = srvc.csord__Order_Request__c;}
            if(srvc.csord__Identification__c != null){Identification = srvc.csord__Identification__c;}
            if(srvc.AccountId__c != null){accId = srvc.AccountId__c;}
            String startDateString = date.today().format();
            String []startDateArray = startDateString.split('/');
            billingCommDate = startDateArray[1]+'/'+startDateArray[0]+'/'+startDateArray[2];
        }

        typeofChargeOptions = new List<SelectOption>();      
        typeofChargeOptions.add(new SelectOption('OCH - One off charge','OCH - One off charge'));
        typeofChargeOptions.add(new SelectOption('RCR - Recurring credit','RCR - Recurring credit'));
        typeofChargeOptions.add(new SelectOption('OCR - One off credit','OCR - One off credit'));
        
        frequencyOptions = new List<SelectOption>();      
        frequencyOptions.add(new SelectOption('One-Off','One-Off'));
        frequencyOptions.add(new SelectOption('Monthly','Monthly'));
        
        typeofMACOptions = new List<SelectOption>();      
        typeofMACOptions.add(new SelectOption('--None--','--None--'));
        for (Integer i = 0; i < miscChargeableList.size(); i++) {
            typeofMACOptions.add(new SelectOption(miscChargeableList[i].Id,miscChargeableList[i].Name)); 
        }       

        zeroFlag = true;
        displayLineItemFlag = true;
        chargeAction = 'CREATE';
    }
      
    //Inserting data to GCC Miscellaneous Service Line Item Object
    public PageReference insertToMiscChargeObj(){
   
        try{
            miscChargeSLI = new csord__Service_Line_Item__c();
            miscChargeSLI.Name = serviceName;
            miscChargeSLI.csord__Service__c = srvcId;
            miscChargeSLI.MISC_Charge_Amount__c = chargeAmount;
            miscChargeSLI.MISC_Cost__c = cost;
            miscChargeSLI.Bill_Texts__c = billText;
            miscChargeSLI.CurrencyIsoCode = currencyName;
            miscChargeSLI.Type_of_Charge__c = typeofCharge;
            miscChargeSLI.Purchase_MISC_Order__c = purchaseOrder;       
            miscChargeSLI.Frequency__c = Frequency;
            miscChargeSLI.Bill_Profile__c = billProfileId;
            miscChargeSLI.Billing_MISC_Commencement_Date__c = date.today();
            miscChargeSLI.Cost_Centre__c = CostCentreId;
            miscChargeSLI.Reference__c = serviceReferenceNo;
            miscChargeSLI.Zero_MISC_Charge_Flag__c = zeroFlag;
            miscChargeSLI.Display_line_item_on_Invoice__c = displayLineItemFlag;
            miscChargeSLI.Charge_Action__c = chargeAction;
            miscChargeSLI.Charge_ID__c = chargeID;
            miscChargeSLI.U2C_Master_Code__c=U2CMasterCode;
            miscChargeSLI.csord__Order_Request__c = OrderRequest;
            miscChargeSLI.csord__Identification__c = Identification;
            miscChargeSLI.Charging_Frequency__c = Frequency;
            miscChargeSLI.Account_ID__c = accId;
            miscChargeSLI.RecordTypeId = RecordTypeUtil.getRecordTypeIdByName(csord__Service_Line_Item__c.SObjectType, 'GCC Miscellaneous Service');
            miscChargeSLI.Is_Miscellaneous_Credit_Flag__c = true;

            if(typeofMAC!='--None--'){
                for(Integer k = 0; k < miscChargeableList.size(); k++){
                    if(typeofMAC == miscChargeableList[k].Id){
                        miscChargeSLI.Type_of_MAC__c = miscChargeableList[k].Name;
                    }
                }
            }
            insert miscChargeSLI;
                        //return new PageReference('/'+miscChargeSLI.Id);
             //Start- Suresh: 21/09/2017 changes for tech_debt to avoid hardcoded URL
               PageReference MiscChargeGCC= Page.MiscChargeGCC;
           MiscChargeGCC.getParameters().put('id',miscChargeSLI.Id );
           return MiscChargeGCC;
             //End- Suresh
            
        } catch(Exception e){
            system.debug('Error while inserting GCC Miscellaneous Service Line Item record: ' +e); 
            //ApexPages.Message myMsg = new  ApexPages.Message(ApexPages.Severity.ERROR,'Charge Amount should be more than 0');
            //ApexPages.addMessage(myMsg);
            return null;            
        }
    }
    
    //Getting One Time Charge and Bill Text as per the selected Type of MAC
    public PageReference getOneTimeChargeText(){    
        if(typeofMAC != '--None--'){
            for(Integer j = 0; j < miscChargeableList.size(); j++){
                if (typeofMAC == miscChargeableList[j].Id){
                    if(billText != null){
                        billText = miscChargeableList[j].One_Time_Charge_Text__c + ' - SD' + serviceReferenceNo;
                    }
                    chargeAmount = miscChargeableList[j].One_Time_Charge_Amount__c.setscale(2);
                    break;
                }
            }
        }
        return null;
    }   
   
    public PageReference cancel(){
        //return new PageReference('/'+srvcId); 
          //Start- Suresh: 21/09/2017 changes for tech_debt to avoid hardcoded URL
               PageReference MiscChargeGCC= Page.MiscChargeGCC;
           MiscChargeGCC.getParameters().put('id',srvcId );
           return MiscChargeGCC;
             //End- Suresh
    }
}