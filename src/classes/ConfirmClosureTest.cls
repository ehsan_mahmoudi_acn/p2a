@IsTest
public class ConfirmClosureTest {
   /* private static List<cscfga__Product_Configuration__c> pclist;
    private static List<Product_Definition_Id__c> PdIdlist;
    private static List<cscfga__Product_Definition__c> Pdlist;
    private static List<Offer_Id__c>offIdlist;
    
    public static void setupTestData()  {
      
    
  
         offIdlist = new List<Offer_Id__c>{
                     new Offer_Id__c(name ='Master_IPVPN_Service_Offer_Id',Offer_Id__c = null),
                     new Offer_Id__c(name ='Master_VPLS_Transparent_Offer_Id',Offer_Id__c = null),
                     new Offer_Id__c(name ='Master_VPLS_VLAN_Offer_Id',Offer_Id__c = null)         
        };
        System.assertEquals(offIdlist[0].name,'Master_IPVPN_Service_Offer_Id'); 
      
     insert offIdlist;
       
     Pdlist = new List<cscfga__Product_Definition__c>{
              new cscfga__Product_Definition__c(name  = 'Master IPVPN Service',cscfga__Description__c = 'Test master IPVPN'),
              new cscfga__Product_Definition__c(name  = 'Master VPLS Service', cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'VLANGroup_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'IPVPN_Port_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'SMA_Gateway_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'VPLS_Transparent_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'VPLS_VLAN_Port_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'Master_IPVPN_Service_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'Master_VPLS_Service_Definition_Id',cscfga__Description__c = 'Test master VPLS')
            };
     insert Pdlist;      
     System.assertEquals('Master IPVPN Service',Pdlist[0].Name);
     
     PdIdlist = new List<Product_Definition_Id__c>{
                new Product_Definition_Id__c(name = 'Master IPVPN Service',Product_Id__c = Pdlist[0].Id),
                new Product_Definition_Id__c(name = 'Master VPLS Service',Product_Id__c = Pdlist[0].id),
                new Product_Definition_Id__c(name = 'VLANGroup_Definition_Id',Product_Id__c = Pdlist[0].id),
                new Product_Definition_Id__c(name = 'IPVPN_Port_Definition_Id',Product_Id__c = Pdlist[0].id),
                new Product_Definition_Id__c(name = 'SMA_Gateway_Definition_Id',Product_Id__c = Pdlist[0].id),
                new Product_Definition_Id__c(name = 'VPLS_Transparent_Definition_Id',Product_Id__c = Pdlist[0].id),
                new Product_Definition_Id__c(name = 'VPLS_VLAN_Port_Definition_Id',Product_Id__c = Pdlist[0].id),
                new Product_Definition_Id__c(name = 'Master_IPVPN_Service_Definition_Id',Product_Id__c = Pdlist[0].id),
                new Product_Definition_Id__c(name = 'Master_VPLS_Service_Definition_Id',Product_Id__c = Pdlist[0].id)
     };
     insert PdIdlist;
     System.assertEquals('Master VPLS Service',PdIdlist[1].Name); 
       
     List<cscfga__Product_Basket__c> pblist =  P2A_TestFactoryCls.getProductBasket(1);  
         
     pclist= new List<cscfga__Product_Configuration__c>{
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[0].id,cscfga__Product_Basket__c = pblist[0].Id,cscfga__Product_Family__c = 'Test family'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[0].id,cscfga__Product_Basket__c = pblist[0].Id,cscfga__Product_Family__c = 'Test family1'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[0].id,cscfga__Product_Basket__c = pblist[0].Id,cscfga__Product_Family__c = 'Test family2'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[0].id,cscfga__Product_Basket__c = pblist[0].Id,cscfga__Product_Family__c = 'Test family3'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[0].id,cscfga__Product_Basket__c = pblist[0].Id,cscfga__Product_Family__c = 'Test family4'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[0].id,cscfga__Product_Basket__c = pblist[0].Id,cscfga__Product_Family__c = 'Test family5'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[0].id,cscfga__Product_Basket__c = pblist[0].Id,cscfga__Product_Family__c = 'Test family6'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[0].id,cscfga__Product_Basket__c = pblist[0].Id,cscfga__Product_Family__c = 'Test family7'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[0].id,cscfga__Product_Basket__c = pblist[0].Id,cscfga__Product_Family__c = 'Test family8')
            };
     insert pclist;      
     System.assertEquals('Test family2',pclist[2].cscfga__Product_Family__c); 
         
         case c = new case();
          c.Is_Feasibility_Request__c = true;
          c.Product_Configuration__c = pclist[0].id;
          c.status = 'Confirmed';
          c.Number_of_Child_Case__c = 2;
          c.Number_of_Child_Case_Closed__c = 1;
          c.Reason_for_selecting_one_supplier__c = 'SingleSource';
          c.Is_Resource_Reservation__c = true;
          insert c;
        System.assertEquals(c.Reason_for_selecting_one_supplier__c, 'SingleSource'); 
        
        Supplier__c sup = new Supplier__c();
        sup.name = 'At&T'; 
        insert sup;
        System.assertEquals(sup.name , 'At&T'); 
        
        VendorQuoteDetail__c v = new VendorQuoteDetail__c();
          //v.Supplier_Name__c = sup.id;
          v.Product_Type__c= 'GCPE';
          v.Interface_Type__c ='100';
          v.Non_Recurring_Cost__c = 777;
          v.Recurring_Cost__c = 999;
          v.Winning_Quote__c = true;
          v.Win_Reason__c = 'Lowest bid';
          // v.Expiration_date__c = System.Today()+10;
          v.Proposed_Quote__c = true;
          v.Winning_Quote__c  = true;
          v.Related_3PQ_Case_Record__c  = c.id;
          insert v;
        System.assertEquals(v.Product_Type__c, 'GCPE'); 
             
     }
         
     static void positiveTest() {   
         
         Pdlist = new List<cscfga__Product_Definition__c>{
              new cscfga__Product_Definition__c(name  = 'Master IPVPN Service',cscfga__Description__c = 'Test master IPVPN')
            };
     insert Pdlist;      
     System.assertEquals('Master IPVPN Service',Pdlist[0].Name);
     
     PdIdlist = new List<Product_Definition_Id__c>{
                new Product_Definition_Id__c(name = 'Master IPVPN Service',Product_Id__c = Pdlist[0].Id)
     };
     insert PdIdlist;
     System.assertEquals('Master IPVPN Service',PdIdlist[0].Name); 
       
     List<cscfga__Product_Basket__c> pblist =  P2A_TestFactoryCls.getProductBasket(1);  
         
     pclist= new List<cscfga__Product_Configuration__c>{
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[0].id,cscfga__Product_Basket__c = pblist[0].Id,cscfga__Product_Family__c = 'Test family') 
            };
     insert pclist;      
     System.assertEquals('Test family',pclist[0].cscfga__Product_Family__c); 
         
         Test.startTest();
         case c = new case();
         c.Is_Feasibility_Request__c = true;
         c.Product_Configuration__c = pclist[0].id;
         c.status = 'closed';
         c.Number_of_Child_Case__c = 2;
         c.Number_of_Child_Case_Closed__c = 1;
         c.Reason_for_selecting_one_supplier__c = 'SingleSource';
         c.Is_Resource_Reservation__c = true;
         insert c;
         System.assertEquals(c.Reason_for_selecting_one_supplier__c, 'SingleSource');
         //System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
         System.assertEquals('closed', c.status);
        
         case c1 = new case();
         c.Is_Supplier_Quote_Request__c = false;
         c.Is_Parent_Case__c = true;
         c.Parent_Feasibility__c = c.id;
         c1.Is_Feasibility_Request__c = true;
         c1.Product_Configuration__c = pclist[0].id;
         c1.status = 'open';
         c1.Number_of_Child_Case__c = 2;
         c1.Number_of_Child_Case_Closed__c = 1;
         c1.Reason_for_selecting_one_supplier__c = 'SingleSource';
         c1.Is_Resource_Reservation__c = true;
         insert c1;
         System.assertEquals('open', c1.status);
         
          case c11 = new case();
         c11.Is_Supplier_Quote_Request__c = false;
         c11.Is_Parent_Case__c = true;
         c11.Parent_Feasibility__c = c.id;
         c11.Is_Feasibility_Request__c = true;
         c11.Product_Configuration__c = pclist[0].id;
         c11.status = 'Confirmed';
         c11.Number_of_Child_Case__c = 2;
         c11.Number_of_Child_Case_Closed__c = 1;
         c11.Reason_for_selecting_one_supplier__c = 'SingleSource';
         c11.Is_Resource_Reservation__c = true;
         insert c11;
         
         System.assertEquals(c11.Reason_for_selecting_one_supplier__c, 'SingleSource');
         //System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
         System.assertEquals('Confirmed', c11.status);
        
         case c3 = [select id,status,Is_Parent_Case__c from case where id =:c1.id limit 1];
         c3.status = 'Confirmed';
         update c3;
         System.assertEquals('Confirmed', c3.status);
 
         Supplier__c sup = new Supplier__c();
         sup.name = 'At&T'; 
         insert sup;
         System.assertEquals('At&T', sup.name);
         
         VendorQuoteDetail__c v = new VendorQuoteDetail__c();
          //v.Supplier_Name__c = sup.id;
          v.Product_Type__c= 'GCPE';
          v.Interface_Type__c ='100';
          v.Non_Recurring_Cost__c = 777;
          v.Recurring_Cost__c = 999;
          v.Winning_Quote__c = true;
          v.Win_Reason__c = 'Lowest bid';
          // v.Expiration_date__c = System.Today()+10;
          v.Proposed_Quote__c = true;
          v.Winning_Quote__c  = true;
          v.Related_3PQ_Case_Record__c  = c1.id;
          insert v;
         System.assertEquals('GCPE', v.Product_Type__c);
         
         //Start Test
        
         PageReference pageRef = Page.CloseCase;
             Test.setCurrentPage(pageRef);
             pageRef.getParameters().put('id',c1.id);
             ApexPages.StandardController sc = new ApexPages.standardController(c1);
             ConfirmClosure controller = new ConfirmClosure(sc);
             controller.GotoDetailPage();
         Test.stopTest();    
         boolean b = true;
         String id = ApexPages.currentPage().getParameters().get('id');
         System.assert(b,id==null); 
        }
      
       private static testMethod void Test() {
       Exception ee = null;
        
        try{
        
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
         
         setupTestData(); 
         positiveTest(); 
       
       } catch(Exception e){
            ee = e;
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
    } */
     }