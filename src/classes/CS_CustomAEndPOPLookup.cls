global with sharing class CS_CustomAEndPOPLookup extends cscfga.ALookupSearch {

    public override String getRequiredAttributes(){ 
    	return '["Product Type", "A End Country", "Z End Country", "LookupMode"]';
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
        
        Set <Id> popIds = new Set<Id>();
        String productType = searchFields.get('Product Type');
        String aEndCountry = searchFields.get('A End Country');
        String zEndCountry = searchFields.get('Z End Country');
        String lookupMode = searchFields.get('LookupMode');

        System.Debug('Search field = ' + productType);
       	List<CS_Route_Segment__c> routeSegList; 

       	if(lookupMode == '1'){
       		routeSegList = [SELECT Id, POP_A__c, POP_A__r.CS_Country__c, POP_Z__r.CS_Country__c
       											  FROM CS_Route_Segment__c
       											  WHERE Product_Type__c = :productType 
       											  		AND POP_A__r.CS_Country__c = :aEndCountry 
       											  		AND POP_Z__r.CS_Country__c = :zEndCountry];
       	} else if(lookupMode == '2'){
       		routeSegList = [SELECT Id, POP_A__c, POP_A__r.CS_Country__c, POP_Z__r.CS_Country__c
       											  FROM CS_Route_Segment__c
       											  WHERE Product_Type__c = :productType 
       											  		AND POP_A__r.CS_Country__c = :aEndCountry];
       	}

       	for(CS_Route_Segment__c item : routeSegList){
       		popIds.add(item.POP_A__c);
       	}
        System.Debug('doLookupSearch');
        System.Debug(searchFields);
        String searchValue = searchFields.get('searchValue') +'%';
        List <CS_POP__c> data = [SELECT Id, Name, Address1__c, CS_City__c FROM CS_POP__c WHERE  Id IN :popIds AND Name LIKE :searchValue ORDER BY Name];
        System.Debug(data);
       return data;
    }
}