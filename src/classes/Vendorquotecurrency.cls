Public class Vendorquotecurrency{

public void vendorquocurrency(List<VendorQuoteDetail__c> caseVendorDetailsList){
    Set<Id> vqPBIds = new Set<Id>();
    //List<CurrencyValue__c> currencyValueList = new List<CurrencyValue__c>([select name, CurrencyIsoCode, Currency_Value__c from CurrencyValue__c]);
    Map<String, Decimal> cuurencyMap = new Map<String, Decimal>();
    for(CurrencyValue__c cv : [select name, CurrencyIsoCode, Currency_Value__c from CurrencyValue__c]){
        cuurencyMap.put(cv.name, cv.Currency_Value__c);
    }
    system.debug('cuurencyMap size >>>> '+cuurencyMap.size());
    List<VendorQuoteDetail__c> VQs = new List<VendorQuoteDetail__c> ();
    for(VendorQuoteDetail__c vq : caseVendorDetailsList)
    {
        vq.Vendor_Quote_Exchange__c = cuurencyMap.get(vq.CurrencyISOCode);
        vqPBIds.add(vq.Product_Basket__c);
        
        system.debug('vq.Vendor_Quote_Exchange__c   ::::: '+vq.Vendor_Quote_Exchange__c);
        system.debug('vq.Product_Basket__c   ::::: '+vq.Product_Basket__c);
        
    }
        Map<Id, cscfga__Product_Basket__c> pbMap = new Map<Id, cscfga__Product_Basket__c>([select Id, CurrencyISOCode, Exchange_Rate__c from cscfga__Product_Basket__c where Id in: vqPBIds]);
    for(VendorQuoteDetail__c vq : caseVendorDetailsList)
    {   
       
         vq.Basket_Exchange_Rate__c = pbMap.get(vq.Product_Basket__c).Exchange_Rate__c;
         system.debug('vq.Basket_Exchange_Rate__c ::: '+vq.Basket_Exchange_Rate__c);
        if(vq.Non_recurring_Charge__c != null && vq.Vendor_Quote_Exchange__c != null && vq.Basket_Exchange_Rate__c != null){
                vq.non_rec_new__c = (((vq.Non_recurring_Charge__c)/vq.Vendor_Quote_Exchange__c)*(vq.Basket_Exchange_Rate__c)).setscale(2);
        }
        if(vq.Recurring_Charge__c != null && vq.Vendor_Quote_Exchange__c != null && vq.Basket_Exchange_Rate__c != null){
                vq.Reccharge__c = (((vq.Recurring_Charge__c)/vq.Vendor_Quote_Exchange__c)*(vq.Basket_Exchange_Rate__c)).setscale(2);
        }
        VQs.add(vq);
    }
    update VQs;
    
  }
}