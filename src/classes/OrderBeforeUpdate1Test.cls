@isTest
public Class OrderBeforeUpdate1Test{
  /*  static City_Lookup__c city;
    static Site__c s1;
    static Order__c ord;
    static Opportunity opp;
    //static Order_Line_Item__c olit;
    static Product2 prod;
    static User u;
    static Contact con;
    static Account acc;
    static BillProfile__c b;
   public static testMethod void TestOrderBeforeUpdate1() {
           
          Order_Line_Item__c oli=new Order_Line_Item__c();
          
          oli.Site_A_SDPM__c=getSite().id;
          oli.Site_B_SDPM__c=getSite().id;
          oli.Bill_Profile__c=getBillProfile().Id;
          oli.AEndZipPlus4__c='123456789';
          oli.BEndZipPlus4__c='123456789';
          oli.Product__c = getProduct().Id;
          oli.ParentOrder__c = getOrder().Id;
          oli.Bill_Profile__c=getBillProfile().id;
          oli.Opportunity_Line_Item_ID__c = 'Opp Line Item 0012332';
          oli.OrderType__c = 'New Provide';
          oli.CPQItem__c = '1';  
          oli.Is_GCPE_shared_with_multiple_services__c='NO';
          oli.Billing_Commencement_Date__c=null;
          oli.line_item_status__c='New';
          Test.startTest();
          insert oli;
          test.stopTest();
          try{
              oli.line_item_status__c = 'Complete';
             // oli.Billing_Commencement_Date__c=System.today();
              update oli;
          }
        catch(DMLException e){
            system.debug('Error' +e.getMessage());
            ErrorHandlerException.ExecutingClassName='TestOrderBeforeUpdate1 :TestOrderBeforeUpdate1';         
ErrorHandlerException.sendException(e); 
         }  
    
    
    }
    
    
  //Country Record created  
 private static Country_Lookup__c getCountry(){
        Country_Lookup__c c2 = new Country_Lookup__c();
        c2.Country_Code__c = 'US';
        insert c2;
        return c2;
    } 
    
  //record for user..
   private static User getUser()
    {
        if(u == null){
            u = new User();
            u.FirstName = 'Test11';
            u.LastName = 'SFDC';
            u.IsActive  = true;
            u.Username = 'Test.SFDC@team.telstra.com.ev1olution';
            u.Email = 'Test.SFDC@team.telstra1.com';
            u.Region__c = 'US';
            u.TimeZoneSidKey = 'Australia/Sydney';
            u.LocaleSidKey = 'en_AU';
            u.EmployeeNumber = '1234214'; 
            u.EmailEncodingKey = 'ISO-8859-1';
            u.LanguageLocaleKey = 'en_US';
            u.Alias = 'TSFDC';
            Profile p = [select id from Profile where name = 'TI Service Delivery'];
            u.ProfileId = p.id;
                  
            insert u;
        }      
        return u;
    }  
  // City Record Created......
 /*  private static City_Lookup__c getCity(){
      
         city= new City_Lookup__c();
        //city.Generic_Site_Code__c = 'LOL';
        city.name = 'TestcityName';
        city.City_Code__c='ABCD';
        //city1.OwnerID = userinfo.getuserid();
        insert city;   
     return city;
     
    }  
  // Account Record Created...
  
   private static Account getAccount(){
   if(acc ==null){
        acc = new Account();
        Country_Lookup__c cl = getCountry();
        acc.Name = 'Test11 Account122';
        acc.Customer_Type__c = 'MNC';
        acc.Country__c = cl.Id;
        acc.Customer_Legal_Entity_Name__c = 'Sample Entity12';
        acc.Selling_Entity__c = 'Telstra INC';
        acc.Activated__c= true;
        acc.Account_Id__c ='1212321';
        acc.Postal_Code__c='123456789';
            try{
              insert acc;
              //list<Country_Lookup__c> prod = [select id,Name from Country_Lookup__c where Name = 'Test11 Account122'];
        //system.assertEquals(acc.Name , prod[0].Name);
        System.assert(acc!=null); 
              
          }
         catch(Exception e){
           //Assert Status Code
           System.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION' , 
                                e.getDmlStatusCode(0) );
           System.assert(e.getMessage().contains('The postal code entered for country United States of America is not in the appropriate format. The Postal Code format should be XXXXXYYYY(E.g. 123456789) OR XXXXX-XXXX(E.g. 12345-6789) OR XXXXX XXXX(e.g. 12345 6789). If have filled in the appropriate data and you are still getting this error message then please contact support team'));
        } 
        }
        return acc;
        
    }  
// Site record Created...
  private static Site__c getSite(){
  if(S1==null){
        s1 = new Site__c();
        s1.name = 'Acc11223';
        s1.Country_Finder__c = getCountry().id ;
       // s1.City_Finder__c = getCity().id;
        s1.Address_Type__c = 'Billing Address';
        s1.Address1__c = 'abc,2nd street, Mughal bloc21k';
        s1.Address2__c = 'xyz,Master street,2nd crown bloc12k';
        s1.AccountId__c = getAccount().id;
        s1.PostalCode__c='123456789';
        insert s1;
        }
        return s1;
   
    }
// Opportunity Record created...
 private static Opportunity getOpportunity(){
 if(opp==null){
        opp = new Opportunity();
        acc = getAccount();
        opp.Name = 'Test Opportunity121';
        opp.AccountId = acc.Id;
         //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
        opp.StageName = 'Identify & Define';
        opp.Stage__c='Identify & Define';
        opp.CloseDate = System.today();
        opp.Estimated_MRC__c=800;
        opp.Estimated_NRC__c=1000;
        opp.ContractTerm__c='10';

        insert opp;
        }
        return opp;
    }
 //Creating record for product...
  private static Product2 getProduct(){
            if(prod==null){
            prod= new Product2();
            prod.name = 'Test Produc12t';
            prod.CurrencyIsoCode='EUR';
            prod.Product_ID__c='IPL';        
            prod.Create_Resource__c=TRUE;
            insert prod;
            }
            return prod;
          
     }  
   //creating record for order...
     private static Order__c getOrder(){
        if(ord==null){
        ord = new Order__c();
        Opportunity o = getOpportunity();
        ord.Opportunity__c = o.Id;
        ord.Account__c=getAccount().id;
        ord.Is_Order_Submitted__C=True;
        ord.SD_OM_Assigned_Date__c = system.today();
        
       // ord.Technical_Specialist_Assigned_Date__c = system.today();
       // ord.SD_PM_Assigned_Date__c = system.today();
        insert ord;  
        }      
        return ord;
        ord.SD_PM_Contact__c =getUser().id;
        update ord;
        return ord;
    }     
   //creating record for bill profile
    private static BillProfile__c getBillProfile(){
    if(b==null){
        b = new BillProfile__c();
        b.Billing_Entity__c = 'Telstra Incorporated';
        b.Account__c = acc.Id;
        b.Billing_Contact__c=getContact().id;
       // b.Bill_Profile_Site__c=getSite().Id;
        b.Bill_Profile_Site__c=s1.Id;
        insert b;
        }
        return b;
    } 
   //creating record for contact..
   private static Contact getContact(){
                  
            Country_Lookup__c c = getCountry();
            con = new Contact();
            con.LastName = 'Tech12';
            con.AccountId = getAccount().Id;
            con.Contact_Type__c = 'Technical';
            con.Country__c = c.Id;
            con.Primary_Contact__c = false;
            con.MobilePhone = '123333';
            con.Postal_Code__c='123456789';
            try{
             insert con; 
          }
         catch(Exception e){
           //Assert Status Code
           System.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION' , 
                                e.getDmlStatusCode(0) );
           System.assert(e.getMessage().contains('The postal code entered for country United States of America is not in the appropriate format. The Postal Code format should be XXXXXYYYY(E.g. 123456789) OR XXXXX-XXXX(E.g. 12345-6789) OR XXXXX XXXX(e.g. 12345 6789). If have filled in the appropriate data and you are still getting this error message then please contact support team'));
        } 
                   
            return con;
    } */
}