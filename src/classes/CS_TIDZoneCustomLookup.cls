global with sharing class CS_TIDZoneCustomLookup extends cscfga.ALookupSearch {

    public override String getRequiredAttributes(){
        return '["TIDPostcode", "ESACodeName"]';
    }
    

    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){  
            String Postcode = searchFields.get('TIDPostcode');
            String ESACodeName = searchFields.get('ESACodeName');
            
                        

            List<CS_TID_PostCode_ESACode_Zone__c> returnData = [SELECT ID,CS_TID_Zone__c
                                     FROM CS_TID_PostCode_ESACode_Zone__c
                                     WHERE Name = :Postcode 
                                     AND CS_TID_ESACode__c = :ESACodeName  ORDER BY CS_TID_Zone__c DESC limit 1];
            
           
           return returnData;
    } 
}