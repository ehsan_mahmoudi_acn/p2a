public class MacdProcessFromSubscriptionsJob implements Queueable {
    
    private String m_macdType {get; set;}
    private String m_changeType {get;set;}
    private String m_recordType {get; set;}
    private String m_title {get; set;}
    private List<String> m_subscriptionIds {get; set;}
    private Map<String, csordtelcoa__Change_Types__c> m_changeTypeMap {get; set;}
    
    public MacdProcessFromSubscriptionsJob(String macdType, List<String> subscriptionIds, String changeType, String title, String recordType) {
        this.m_macdType = macdType;
        this.m_subscriptionIds = subscriptionIds;
        this.m_changeType = changeType;
        this.m_recordType = recordType;
        this.m_title = title;
        this.m_changeTypeMap = csordtelcoa__Change_Types__c.getAll(); 
    }
    
    public void execute(QueueableContext context) {
        Id newId = null; 
        List<csord__Subscription__c> subscriptions = [SELECT Id, Name, csord__Account__c, CreatedDate, CurrencyIsoCode FROM csord__Subscription__c WHERE Id in :this.m_subscriptionIds];
        if (this.m_macdType == 'MACDOppty') {
            newId = csordtelcoa.API_V1.createMACOpportunityFromSubscriptions(subscriptions, this.m_changeTypeMap.get(this.m_changeType), this.m_recordType, this.m_title, null);
            Decimal exchangeRate = [select Currency_Value__c from CurrencyValue__c where CurrencyIsoCode =: subscriptions[0].CurrencyIsoCode limit 1].Currency_Value__c;
            system.debug('Before calling updateProdConfigCurrency class from MacdProcessFromSubscriptionsJob ');
            system.debug('MacdProcessFromSubscriptionsJob Opportunity ID  : '+newId+ ' exchangeRate '+exchangeRate);
            updateProdConfigCurrency.updateProductConfigCurrency(newId, exchangeRate, subscriptions[0].CurrencyIsoCode);
               system.debug('Returned from updateProdConfigCurrency MacdProcessFromSubscriptionsJob ');
               
             /* Changes made to fix Internet:Rate Card values are reflected under Last Approved RC and Last Approved NRC., QC #12634, Dev Name: Anuradha, Date changed: 22/11/2016*/
           List<Pricing_Approval_Request_Data__c> Pardnew = [select id , Name,Product_Basket__r.Exchange_rate__c,Product_Basket__c,Last_Approved_NRC__c,Last_Approved_RC__c,Approved_Offer_Price_MRC__c,Approved_Offer_Price_NRC__c,Offer_NRC__c,Offer_RC__c, Product_Basket__r.cscfga__Opportunity__r.id ,Product_Configuration__c  from Pricing_Approval_Request_Data__c where Product_Basket__r.cscfga__Opportunity__r.id = : newid ];
               for(Pricing_Approval_Request_Data__c par : pardnew){
             //offer_rc__c and offer_NRC__c are the values of the new Provider approved NRC and Mrc . 
            
             par.Last_Approved_NRC__c =par.offer_NRC__c.setscale(2);
             par.Last_Approved_RC__c  = par.offer_RC__c.setscale(2);
                //par.Last_Approved_NRC__c = ((par.Last_Approved_NRC__c/exchangerate)* par.Product_Basket__r.Exchange_rate__c).setScale(2);
                // par.Last_Approved_RC__c =  ((par.Last_Approved_RC__c/exchangerate)* par.Product_Basket__r.Exchange_rate__c).setScale(2);
           }
             update pardnew; 
       
          //for the Manish solution
         /* opportunity opp = new opportunity();
            List<opportunity> opplist = new List<opportunity>();
            Map<id,string> pbmap = new Map<id,string>();
            opp.id= newid;
            List<cscfga__Product_Basket__c> pblist = [select Exchange_Rate__c, cscfga__Opportunity__c,CurrencyIsoCode from cscfga__Product_Basket__c where cscfga__Opportunity__c =: newid ];
            system.debug('subscriptions.size()'+subscriptions.size());
            if(subscriptions.size()> 0)
            {
                opp.CurrencyIsoCode = subscriptions[0].CurrencyIsoCode;
                system.debug('  opp.CurrencyIsoCode '+  opp.CurrencyIsoCode );
               Decimal exchangeRate = [select Currency_Value__c from CurrencyValue__c where CurrencyIsoCode =: subscriptions[0].CurrencyIsoCode limit 1].Currency_Value__c;
               for(cscfga__Product_Basket__c pb : pblist)
                {
                    pb.CurrencyIsoCode = subscriptions[0].CurrencyIsoCode;
                    pb.Exchange_Rate__c = exchangeRate;
                    system.debug('pb.id'+ pb.id);
                    system.debug('exchangeRate  '+exchangeRate);
                    system.debug('pb.Exchange_Rate__c'+ pb.Exchange_Rate__c);
                    system.debug('subscriptions[0].CurrencyIsoCode;' + subscriptions[0].CurrencyIsoCode );
                    system.debug('pb.CurrencyIsoCode: ' + pb.CurrencyIsoCode);
                    system.debug('Printing time MacdProcessFromSubscriptionsJob ' +system.now());
                    
                    
                }
            }
            opplist.add(opp);
            //update pblist;
            update opplist;
          system.debug('newId: ' + newId );*/   
        } else if (this.m_macdType == 'MACDBasket') {
            newId = csordtelcoa.API_V1.createMacBasketFromSubscriptions(subscriptions, this.m_changeTypeMap.get(this.m_changeType), this.m_title, null);
			
			for(csord__Subscription__c subs :subscriptions){
				subs.MACD_Basket_Id__c = newId;
			}
            TriggerFlags.NoSubscriptionTriggers = true;
            TriggerFlags.NoServiceTriggers = true;
            TriggerFlags.NoOrderTriggers = true;
			update subscriptions;
			TriggerFlags.NoSubscriptionTriggers = false;
            TriggerFlags.NoServiceTriggers = false;
            TriggerFlags.NoOrderTriggers = false;
        }
        system.debug('newId: ' + newId );  
    }
}