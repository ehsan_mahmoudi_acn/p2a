@isTest
public class CS_StandardBandwidthLookup_Test 
{
    static testMethod void myUnitTest() 
    {
        List<CS_Bandwidth__c> bwList = P2A_TestFactoryCls.getBandwidths(1);
        Map<String,String> fieldMap = new Map<String,String>();
        fieldMap.put('aaaaa','ddddddd');
        fieldMap.put('Country Name','AUSTRALIA');
        fieldMap.put('Account type','MNC');
        fieldMap.put('Network','Australia (AS 1221)');
        String str1 = 'Idhj85555';        
        Integer i1 = 1;
        Integer i2 = 2;
         
        ID[] ids = new ID[]{'01pO0000000I4g2IAL','01pO0000000I4g2IAK'};
           Test.startTest();
        CS_StandardBandwidthLookup sbl = new CS_StandardBandwidthLookup();
        sbl.doDynamicLookupSearch(fieldMap,str1);
        sbl.getRequiredAttributes(); 
     
        sbl.doLookupSearch(fieldMap,str1,ids,i1,i2);
		system.assertEquals(true,sbl !=null);
        test.stopTest();
             
    }
}