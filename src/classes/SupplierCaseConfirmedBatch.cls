public class SupplierCaseConfirmedBatch implements Database.Batchable<VendorQuoteDetail__c>, Database.Stateful {
    
    private Integer m_counter;
    private Id m_productConfigurationId;
    private Set<Id> m_basketIds;
    private Map<Id, cscfga__Product_Configuration__c> m_finalUpdateMap;
    private Map<String, List<custom_object_to_product_configuration__c>> m_cstObjToProdCfgMap;
    
    private void initContextData() {
        
        // initialize custom object to product configuration
        this.m_basketIds = new Set<Id>();
        this.m_finalUpdateMap= new Map<Id, cscfga__Product_Configuration__c>();
        this.m_cstObjToProdCfgMap = new Map<String, List<custom_object_to_product_configuration__c>>();
        
        // get the 'custom object to product configuration' setting records
        /*List<custom_object_to_product_configuration__c> mcs = [Select id
                , Product_Name__c
                , Source_Field__c
                , Source_Object__c
                , Production_Configuration_Attribute_Name__c 
            from 
                custom_object_to_product_configuration__c 
            where 
                Product_Name__c 
            like 
                '%Supplier%'];*/
                //system.debug('size of mcs'+mcs.size());
        
        // populate 'custom object to product configuration' by product name map
        String productName;
        for (custom_object_to_product_configuration__c obj : [Select id, Product_Name__c, Source_Field__c, Source_Object__c, Production_Configuration_Attribute_Name__c from custom_object_to_product_configuration__c where Product_Name__c like '%Supplier%']) {
            productName = obj.product_name__c.trim().toLowercase();
            if (this.m_cstObjToProdCfgMap.containsKey(productName) == false) {
                this.m_cstObjToProdCfgMap.put(productName, new List<custom_object_to_product_configuration__c>());
            }
            this.m_cstObjToProdCfgMap.get(productName).add(obj);
        } 
    }
    
    public SupplierCaseConfirmedBatch(Id productConfigurationId) {
        if (productConfigurationId == null) {
            throw new SupplierCaseConfirmedBatchException('Product Configuration Id should not be empty!');
        }
        this.m_counter = 0;
        this.m_productConfigurationId = productConfigurationId;
        system.debug('product config id'+m_productConfigurationId);
    }
    
    public Iterable<VendorQuoteDetail__c> start(Database.BatchableContext ctx) {
        
        // initialize context data, that will be shared throgh the all the batch steps
        initContextData();
        
        // return the iterable data, that will be processed by the steps of the batch.
        return 
            [select id
                , Related_3PQ_Case_Record__r.Product_Configuration__c
                , Related_3PQ_Case_Record__r.PC_family__c
                , Related_3PQ_Case_Record__r.Product_Configuration__r.cscfga__Configuration_Status__c
                , Related_3PQ_Case_Record__r.Product_Configuration__r.cscfga__Root_Configuration__c
                , Related_3PQ_Case_Record__r.Product_Configuration__r.cscfga__Product_Basket__c
                , Recurring_Charge__c
                , Non_recurring_Charge__c
                , Supplier_Formula__c
                , Non_Recurring_Cost__c
                , recurring_Cost__c
                , Terms__c
                , Supplier_Quote_ID__c
                , Interface_Type__c
                , Telstra_Onnet_PoP__c
                , name
				, non_rec_new__c
                , Reccharge__c
            from 
                VendorQuoteDetail__c 
            where 
                Related_3PQ_Case_Record__r.Product_Configuration__c = :this.m_productConfigurationId 
                and 
                Winning_Quote__c = true FOR UPDATE];  
    }
    
    public void execute(Database.BatchableContext BC, List<VendorQuoteDetail__c> requests) {
        m_counter++;
        
        //check the number of items, throw if more than 1, due to heavy work/load
        VendorQuoteDetail__c request = null;
        if (requests.size() == 1) {
            request = requests[0];
        } else if (requests.size() > 1) {
            throw new SupplierCaseConfirmedBatchException('Set the step size on 1 when executing this batch!');
        }
        
        CheckRecursive.canExecuteToggle('supplierCaseBatchExecute');

        //process the request
        Id basketId = request.Related_3PQ_Case_Record__r.Product_Configuration__r.cscfga__Product_Basket__c;
        Id configId = request.Related_3PQ_Case_Record__r.Product_Configuration__c;
        Id rootConfigId = request.Related_3PQ_Case_Record__r.Product_Configuration__r.cscfga__Root_Configuration__c != null 
            ? request.Related_3PQ_Case_Record__r.Product_Configuration__r.cscfga__Root_Configuration__c 
            : request.Related_3PQ_Case_Record__r.Product_Configuration__c;
        
        String productName = (request.Related_3PQ_Case_Record__r.PC_family__c + 'SupplierApproval').trim().tolowerCase();
        System.debug('productName=> ' + productName);
        if (m_cstObjToProdCfgMap.containsKey(productName)) {
            
            this.m_basketIds.add(basketId);
            cscfga.API_1.ApiSession apiSession = cscfga.API_1.getApiSession(new cscfga__Product_Configuration__c(Id = rootConfigId)); 
            cscfga.ProductConfiguration config, rootConfig = apiSession.getConfiguration();
            if (configId != rootConfigId) {
                for (cscfga.ProductConfiguration cfg: rootConfig.getRootAndRelatedConfigs()) {
                    if (cfg.getId() == configId) {
                        config = cfg;
                        break;
                    }
                }
            } else {
                config = rootConfig;
            }
            
            if (config != null) {
                            
                for (custom_object_to_product_configuration__c  iterator :m_cstObjToProdCfgMap.get(productName)) {                                                                            
                    String setValue = String.valueof(request.get(iterator.Source_Field__c));
                    system.debug('done setValue=>'+setValue+' iterator.Production_Configuration_Attribute_Name__c'+iterator.Production_Configuration_Attribute_Name__c );
                    if (config.getAttribute(iterator.Production_Configuration_Attribute_Name__c) != null) {
                        config.getAttribute(iterator.Production_Configuration_Attribute_Name__c).setValue(setValue);
                        config.getAttribute(iterator.Production_Configuration_Attribute_Name__c).setDisplayValue(setValue);
                    } else {
                        throw new SupplierCaseConfirmedBatchException(
                            'The configuration ' + rootConfigId + ':' + configId + ' does not contain attribute: ' + iterator.Production_Configuration_Attribute_Name__c + 
                            '! Check your custom settings for: ' + productName);
                    
                    }
                }

                // force valid status of the product configuration, get all the childs products, through all the levels downwards, and force valid status
                for (cscfga__Product_Configuration__c pc: [select Id 
                    from 
                        cscfga__Product_Configuration__c 
                    where 
                        cscfga__Product_Basket__c = :basketId and
                        (Id = :configId or 
                        cscfga__Root_Configuration__c = :configId or 
                        Id = :rootConfigId or 
                        cscfga__Root_Configuration__c = :rootConfigId)]) {
                    this.m_finalUpdateMap.put(pc.Id
                        , new cscfga__Product_Configuration__c (Id = pc.Id
                            , cscfga__Configuration_Status__c = 'Valid'));
                }
                
                apiSession.executeRules();
                apiSession.updateLineItems();
                apiSession.persistConfiguration(true);
            }
            
            apiSession.close();
            
            CheckRecursive.canExecuteToggle('supplierCaseBatchExecute');
        }
    }
    
    public void finish(Database.BatchableContext BC) {
        if (this.m_finalUpdateMap.size() > 0) {
            update this.m_finalUpdateMap.values();
            cscfga.ProductConfigurationBulkActions.calculateTotals(this.m_basketIds);
            system.debug('final update map keys: ' + this.m_finalUpdateMap.keySet());
        }
        system.debug('SupplierCaseCtrlBatch - finished, num fo batches: ' + m_counter);
    }
    
    private class SupplierCaseConfirmedBatchException extends Exception {}
}