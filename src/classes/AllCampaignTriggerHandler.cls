public class AllCampaignTriggerHandler extends BaseTriggerHandler {
    @TestVisible
    private Boolean triggerDisabled = false;

    public override Boolean isDisabled(){
        if(Util.muteAllTriggers()){
            return true;
        }
        return triggerDisabled;
    }
    
    public override void beforeInsert(List<SObject> newCampaign){
        campaignHdlrMethod((List<Campaign>)newCampaign,null);
        
    }
    
    public override void beforeUpdate(List<SObject> newCampaign, Map<Id, SObject> newCampaignMap, Map<Id, SObject> oldCampaignMap)
    {
        campaignHdlrMethod((List<Campaign>)newCampaign, (Map<Id, Campaign>)oldCampaignMap);
    }
	
    @TestVisible
    private void campaignHdlrMethod(List<Campaign> newCampaign, Map<Id, Campaign> oldCampaignMap ) 
    {        
    	Map<String, Id> mapCmp = new Map<String, Id>();
    
    	Set<String> setCmpName = new Set<String>();
    	for(Campaign cmpObj : newCampaign)
       	setCmpName.add(cmpObj.Name);
      
    	for(Campaign cmpObj : [ SELECT Id, Name from Campaign where Name IN : setCmpName])  
    	mapCmp.put(cmpObj.Name,cmpObj.Id);
    
    	for(Campaign cmpObj : newCampaign)
    	{
            if(mapCmp.containsKey(cmpObj.name) && mapCmp.get(cmpObj.Name) != cmpObj.Id )
            cmpObj.addError('Campaign Name with '+ cmpObj.Name+' is already exist. Kindly create new Campaign. ');
        }
    }  
	
	
}