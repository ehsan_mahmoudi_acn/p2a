@isTest(SeeAllData = false)
private class NewCreditCheckPopulateTest{
 static Opportunity opp;
    static Account acc;
    static Country_Lookup__c co;
    static Action_Item__c actItem;
    
    
    static testMethod void creditCheck(){
    
           List<Global_Constant_Data__c> globalConstantDataObjList = new List<Global_Constant_Data__c>();
           
           Global_Constant_Data__c globalConstantDataObj;
           
           globalConstantDataObj = new Global_Constant_Data__c();
           globalConstantDataObj.Name = 'Action Item Status';
           globalConstantDataObj.Value__c = 'Assigned';
           globalConstantDataObjList.add(globalConstantDataObj);
           
           globalConstantDataObj = new Global_Constant_Data__c();
           globalConstantDataObj.Name = 'Action Item Priority';
           globalConstantDataObj.Value__c = 'High';
           globalConstantDataObjList.add(globalConstantDataObj);
           List<Action_Item__c> previousActionItem = new List<Action_Item__c>();
           insert globalConstantDataObjList;
           system.assert(globalConstantDataObjList!=null);
           Action_Item__c action = new Action_Item__c();
           List<Action_Item__c> lastAction= [Select Id,Name,Previous_MRC__c,Previous_NRC__c,Account_Number__c,Opportunity__c,Status__c,Feedback__c,Account__r.Account_ID__c,LastModifiedDate,MRC_Monthly_Recurring_Charges__c,NRC_Non_Recurring_Charges__c,Total_Estimated_MRC__c,Total_Estimated_NRC__c from Action_Item__c where Id =: globalConstantDataObjList[0].id];
         // action.Previous_Credit_Check_Date__c=Date.newInstance(lastAction.get(0).LastModifiedDate.year(),lastAction.get(0).LastModifiedDate.month(),lastAction.get(0).LastModifiedDate.day());
                       if(lastAction != null && lastAction.size()>0){
                            action.Action_Item_ID_Previous_Credit_Check__c=lastAction[0].Name;                    
                            action.Previous_MRC__c=lastAction[0].MRC_Monthly_Recurring_Charges__c;
                            action.Previous_NRC__c=lastAction[0].NRC_Non_Recurring_Charges__c;
                            action.Previous_Estimated_MRC__c=lastAction[0].Total_Estimated_MRC__c;
                            action.Previous_Estimated_NRC__c=lastAction[0].Total_Estimated_NRC__c;
                            } 
                        previousActionItem.add(action);
                        insert previousActionItem;
                        system.assert(previousActionItem!=null);  
         Opportunity oppt = getOpportunity(); 
         system.debug('oppt.Id');
         PageReference pageRef1 = new PageReference('/apex/NewCreditCheckPopulate?oppId='+ oppt.Id);
         Test.setCurrentPage(pageRef1);             
         NewCreditCheckPopulateController nCC = new NewCreditCheckPopulateController();
         //system.assertEquals(pc.save().getUrl(),'/'+opp.Id);
         nCC.save();
         nCC.cancel();
    }
    private static Opportunity getOpportunity(){
        if(opp == null){      
            opp = new Opportunity();
            opp.Name = 'TestOpp';
            acc = getAccount();
            opp.AccountId = acc.Id; 
            system.debug('accountIdopp*****'+ opp.AccountId);
            opp.Estimated_MRC__c=300;
            opp.Estimated_NRC__c=200;
            opp.ContractTerm__c='10';
            //opp.Approx_Deal_Size__c =10.00; 
            //opp.Close_Date_Month__c = '4/7/2012';
            opp.CloseDate = system.today();
             //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
            opp.StageName = 'Identify & Define';
            opp.Stage__c='Identify & Define';
            opp.Opportunity_Type__c = 'Simple';  
            insert opp;
           List<Opportunity> op =[Select id,name from Opportunity where name = 'TestOpp'];
           system.assertequals(opp.name, op[0].name); 
           
           system.assert(opp!=null); 
        }
     
        return opp;
        
    }
    private static Account getAccount(){
            if(acc == null){
            
                acc = new Account();
                acc.Name = 'Testacc';
                acc.Customer_Type__c = 'MNC';
                acc.CurrencyIsoCode ='USD';
                acc.Activated__c = true;
                acc.Selling_Entity__c ='Telstra Limited';
                co = getCountry();
                acc.Country__c = co.Id;
                acc.Activated__c = true;
                acc.Account_ID__c = '9090';
                acc.Customer_Legal_Entity_Name__c='Test';
                acc.Account_Status__c = 'Active';           
                insert acc;
                List<Account> a1 = [Select id,name from Account where name = 'Testacc'];
                system.assertequals(acc.name, a1[0].name);
                system.assert(acc!=null);
            }
               return acc;
         }  
         
         
           
  private static Country_Lookup__c getCountry(){
       if(co == null){
       
            co = new Country_Lookup__c();
            co.Name = 'INDIA';
            co.Country_Code__c ='IN';
       
            insert co;
            
        List<Country_Lookup__c> c = [Select id,name from Country_Lookup__c where name = 'INDIA'];
        System.assertequals(co.name, c[0].name);
        system.assert(co!=null);
            
            
       }
            return co;
         }
         
}