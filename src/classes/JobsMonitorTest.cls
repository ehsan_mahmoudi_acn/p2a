@isTest
public class JobsMonitorTest
{
   public static String CRON_EXP = '0 0 0 15 3 ? 2022';
   static testmethod void test()
   {
    Test.startTest();
    MonitorJob__c mjob = new MonitorJob__c ();
    mjob.Interval__c = 12;
    mjob.Last_Checked__c = DateTime.now();
    mjob.Last_Restart__c = DateTime.now();
    mjob.Support_Email__c = 'Test123@gmail.com';
    insert mjob;
    
      String jobId = System.schedule('Test my class',
                        CRON_EXP, 
                        new JobsMonitor());
                CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];
         System.assertEquals(CRON_EXP, 
         ct.CronExpression);

      // Verify the job has not run
      System.assertEquals(0, ct.TimesTriggered);

      // Verify the next time the job will run
      System.assertEquals('2022-03-15 00:00:00', 
         String.valueOf(ct.NextFireTime));
         JobsMonitor j = new JobsMonitor ();
        // j.sendEmail(mjob);        
    Test.stopTest();
    
   }
}