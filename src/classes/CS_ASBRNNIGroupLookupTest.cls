@isTest(SeeAllData = false)
private class CS_ASBRNNIGroupLookupTest {
    
    private static List<CS_Provider__c> providers;
    private static List<CS_NNI_Service__c> nniServices;
    private static List<CS_NNI_Group__c> nniGroups;
    private static List<CS_NNI_Service_NNI_Group__c> nniServiceNniGroups;
    
    private static Map<String, String> searchFields = new Map<String, String>();
    
    private static void initTestData(){
        
        providers = new List<CS_Provider__c>{
            new CS_Provider__c(Name = 'Provider 1'),
            new CS_Provider__c(Name = 'Provider 2')
        };
        
        insert providers;
        
        nniServices = new List<CS_NNI_Service__c>{
            new CS_NNI_Service__c(Name = 'NNI Service 1', NNI_Type__c = 'Type A'),
            new CS_NNI_Service__c(Name = 'NNI Service 2', NNI_Type__c = 'Type B')
        };
        
        insert nniServices;
        
        nniGroups = new List<CS_NNI_Group__c>{
            new CS_NNI_Group__c(Name = 'Group 1', City_Code__c = 'HK', CS_Provider__c = providers[0].Id),
            new CS_NNI_Group__c(Name = 'Group 2', City_Code__c = 'ZG', CS_Provider__c = providers[1].Id)
        };
        
        insert nniGroups;
        
        nniServiceNniGroups = new List<CS_NNI_Service_NNI_Group__c>{
            new CS_NNI_Service_NNI_Group__c(CS_NNI_Group__c = nniGroups[0].Id, CS_NNI_Service__c = nniServices[0].Id, ASBR1__c = true, NAS1__c = true, Customer_NNI1__c = true),
            new CS_NNI_Service_NNI_Group__c(CS_NNI_Group__c = nniGroups[1].Id, CS_NNI_Service__c = nniServices[1].Id, ASBR1__c = true, NAS1__c = true, Customer_NNI1__c = true)
        };
        
        insert nniServiceNniGroups;
        
        searchFields.put('Scenario', 'NAS');
        searchFields.put('Type Of NNI', 'Type A');
        searchFields.put('Type Of ASBR', 'Standard');  
        searchFields.put('CS Provider Calculated', providers[0].Id); 
    }
    
    private static testMethod void test() {
        Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
             
            initTestData();
            
            CS_ASBRNNIGroupLookup asbrGroupLookup = new CS_ASBRNNIGroupLookup();
            Object[] data = asbrGroupLookup.doLookupSearch(searchFields, '', null, 0, 0);
            
            System.assert(data.size() > 0, 'No data available');
            data.clear();
            
            searchFields.put('Type Of ASBR', 'NOTStandard'); 
            
            data = asbrGroupLookup.doLookupSearch(searchFields, '', null, 0, 0);
            
            System.assert(data.size() > 0, 'No data available');
            data.clear();
            
            searchFields.put('Scenario', 'NotNAS');
            data = asbrGroupLookup.doLookupSearch(searchFields, '', null, 0, 0);
            
            System.assert(data.size() > 0, 'No data available');
            data.clear();
            
        } catch(Exception e){
            ee = e;
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
    }

}