/*
     *Date:01-june-2017
     *CR:CR19
     *Developer:Allu,Ritesh
     *Description:The code will create a new Solution when an opportunity is closed won
    */

Public class SolutionSummary{ 


    //method 1: for commercial orders
    //will be called when enrichment case is closed   
    public static void createSolution(Id aiId, Id accountId,String customerType,Id oppId,Id prodBasketId){
        try{
        //Deactivating the triggers     
        //TriggerFlags.NoOpportunityTriggers=true;
        //TriggerFlags.NoProductBasketTriggers=true;
        
       // case c=[select Id,AccountId,Account.Customer_Type_New__c,Product_Basket__c,Opportunity_Name__c from case where Id=:caseId];     
        if(AccountId!=null && customerType!='ISO'){
        
        //initialisation        
        Opportunity opp=[select Id,StageName,Solutions__c,Account_Id__c from Opportunity where ID=:oppId LIMIT 1];
        cscfga__Product_Basket__c basket=[select Id,Solutions__c from cscfga__Product_Basket__c where ID=:prodBasketId LIMIT 1];           
        if(opp.solutions__c!=null){
             RemovePreviousReferenceToSolutionBeforeCaseClosure(opp.solutions__c);
           } 
                 Solutions__c soln = new Solutions__c();
                 if(opp!=Null && opp.Solutions__c == null){//solution will be created only when solution lookup on opp is null        
             
                        //soln.Name = 'Solution Summary';
                        soln.Opportunity_Name__c = opp.id;                                          
                        soln.Account_Name__c = opp.Account_Id__c;
                        insert soln;                        
                    }
                 else if(opp!=Null && opp.Solutions__c != null){//incase opportunity is re-opened
                        soln=[select Name,Account_Name__c ,Opportunity_Name__c from Solutions__c where id=:opp.Solutions__c limit 1];
                 }                       
                   
                //updating opportunity with solution Id
                //updating the basket with Solution Id
                 if(soln!=null && opp!=null && basket!=null){
                        
                        opp.Solutions__c=soln.id;
                        basket.Solutions__c=soln.id;
                        update opp;
                        update basket;
                     }
                
            //Activating the triggers    
            //TriggerFlags.NoOpportunityTriggers=false;  
            //TriggerFlags.NoProductBasketTriggers=false;         
            
        }
    }
    catch(Exception e){
    ErrorHandlerException.ExecutingClassName='SolutionSummary:createSolution';
    ErrorHandlerException.objectId= aiId;
    ErrorHandlerException.sendException(e);
    System.debug('Exception caught####:'+e.getMessage());
    }
  }
    
//Redirecting to cascadedetail page from SolutionSummary page   
public PageReference cascadeDetail() {  
        //START saravanan:20/09/2017 changes for tech_dept to avoid hardcoded url
        
        //PageReference cascadePage = new PageReference('/apex/CascadeOrderDetails');
        PageReference CascadeOrderDetails= Page.CascadeOrderDetails;
        CascadeOrderDetails.getParameters();
        CascadeOrderDetails.setRedirect(true);
        //END saravanan
        return CascadeOrderDetails;
}
    
//Method 2 
//for non-commercial Orders
//will be called from BasketTriggerHandler after insert
public Static void setSolutionIdForISO(Map<Id,cscfga__Product_Basket__c>newBasketMap,Map<Id,cscfga__Product_Basket__c>oldBasketMap) {
//Deactivating the triggers
//TriggerFlags.NoProductBasketTriggers=true;
try{
    
Boolean runFlag=false;

for(cscfga__Product_Basket__c pb:newBasketMap.values()){
    if(pb.Quote_Status__c=='Approved' && oldBasketMap.get(pb.id).Quote_Status__c!='Approved' && pb.Solutions__c==null){
        runFlag=true;        
        break;
        
    }
    
}
    
   
//initialisation
if(runFlag){
Map<Id,cscfga__Product_Basket__c>CurrentBasketMap=new Map<Id,cscfga__Product_Basket__c>([select id,Quote_Status__c,csordtelcoa__Account__c,Solutions__c,csordtelcoa__Account__r.Customer_Type_New__c
                                           from cscfga__Product_Basket__c where ID IN:newBasketMap.keyset()]);
system.debug('CurrentBasketMap---' +CurrentBasketMap);
List<Solutions__c>insertSolutionList=new List<Solutions__c>();
system.debug('insertSolutionList---' +insertSolutionList);
for(cscfga__Product_Basket__c bask:CurrentBasketMap.values()){

    if(bask.csordtelcoa__Account__c!=null && bask.csordtelcoa__Account__r.Customer_Type_New__c=='ISO' && bask.Solutions__c==null){
        Solutions__c soln = new Solutions__c();
        soln.Account_Name__c=bask.csordtelcoa__Account__c;
        soln.Product_Basket__c=bask.id;
        insertSolutionList.add(soln);
    }
}
if(insertSolutionList.size()>0)insert insertSolutionList;

for(Solutions__c sol:insertSolutionList){
    if(sol.Product_Basket__c!=null && newBasketMap.containsKey(sol.Product_Basket__c)){
        newBasketMap.get(sol.Product_Basket__c).Solutions__c=sol.id;
    }   
}
//if(newBasketMap.size()>0)update newBasketMap.values();

//Activating the triggers    
//TriggerFlags.NoProductBasketTriggers=false;

}
}catch(Exception e){
ErrorHandlerException.ExecutingClassName='SolutionSummary:setSolutionIdForISO';
ErrorHandlerException.objectList=newBasketMap.values();
ErrorHandlerException.sendException(e);
System.debug(e);
}

}

/*
  *Method 3
  *defect fix:
  *15440  DARWIN | CR19 | Colo: On a New Provide (Closed Won) Opportunity,
  *once a Inflight Revise Opportunity is done and New orders are created & Old Orders Cancelled,
  *the Solution Page should show only New Orders but not the cancelled orders.
  *Date:10-july-2017
  *Developer:Ritesh
  *CR019
*/
public static void removePreviousReferenceToSolutionBeforeCaseClosure(Id solutionId){
  
 
   try{   
   List<cscfga__Product_Basket__c>unsyncedBaskList=[select ID,Solutions__c from cscfga__Product_Basket__c where Solutions__c =:solutionId];
   set<Id>SolutionSet=new Set<Id>();
   
   //get the set of solution from basket
   //with the solution we will get all the orders,subscription and services linked to this solution
   //and remove the link on these objects
   for(cscfga__Product_Basket__c bask:unsyncedBaskList){
     if(!SolutionSet.contains(bask.Solutions__c))SolutionSet.add(bask.Solutions__c);
     bask.Solutions__c=null;
   }
   if(unsyncedBaskList.size()>0)update unsyncedBaskList;
   if(!SolutionSet.isEmpty()){
     //here the reference will be removed
     //first from service, then subscription and then order
     List<csord__service__c>servList=[select id,Solutions__c from csord__service__c where Solutions__c IN:SolutionSet];
     List<csord__order__c>ordList=[select id,Solutions__c from csord__order__c where Solutions__c IN:SolutionSet];
     List<csord__Subscription__c>subList=[select id,Solutions__c from csord__Subscription__c where Solutions__c IN:SolutionSet];
     
     //remove reference
     for(csord__service__c serv:servList){
       serv.Solutions__c=null;
     }
     for(csord__order__c ord:ordList){
       ord.Solutions__c=null;
     }
     for(csord__Subscription__c sub:subList){
       sub.Solutions__c=null;
     }
     
     //update sobjects with removed references
     if(servList.size()>0)update servList;
     if(ordList.size()>0)update ordList;
     if(subList.size()>0)update subList;
    }
     }catch(Exception e){
     ErrorHandlerException.ExecutingClassName='SolutionSummary:RemovePreviousReferenceToSolutionBeforeCaseClosure';
     ErrorHandlerException.objectId= solutionId;
     ErrorHandlerException.sendException(e);
     System.debug(e);
     }
     
}

//Method 4
public static void enableDisableTrigger (Boolean flag){
         TriggerFlags.NoProductBasketTriggers=flag; 
     TriggerFlags.NoOpportunityTriggers =flag;
     TriggerFlags.NoOrderTriggers =flag;
     TriggerFlags.NoServiceTriggers=flag; 
     TriggerFlags.NoSubscriptionTriggers =flag;
     TriggerFlags.NoAttributeTriggers=flag;  
}

//below class will be used to throw user defined errors/exception
public class applicationException extends Exception {}
}