/**
    @author - Accenture
    @date - 19- Jul-2012
    @version - 1.0
    @description - This is the test class for NewCompInformationOverrideController class.
*/
@isTest(SeeAllData = false)
private class NewCompInformationOverrideControllerTest {

static Account a;
static Opportunity opp;
static Country_Lookup__c cl;
static City_Lookup__c ci;


static testMethod void testoverrideNew(){    
       
       List<ObjectPrefixIdentifiers__c> objPrfList = new List<ObjectPrefixIdentifiers__c>();
          ObjectPrefixIdentifiers__c objprf = new ObjectPrefixIdentifiers__c(Name='Competitor Information',ObjectIdPrefix__c='a0R');
          objPrfList.add(objprf);
          ObjectPrefixIdentifiers__c objprf1 = new ObjectPrefixIdentifiers__c(Name='Account',ObjectIdPrefix__c='003');
          objPrfList.add(objprf1);
           ObjectPrefixIdentifiers__c objprf2 = new ObjectPrefixIdentifiers__c(Name='Sites & Contacts',ObjectIdPrefix__c='a0f');
          objPrfList.add(objprf2);
          insert objPrfList ;  
           
       PageReference pageRef1 = new PageReference('/apex/TestNewCompInformationOverrideController');
       Test.setCurrentPage(pageRef1);
       
       a = getAccount();
       opp = getOpportunity(); 
       Opportunity form = getOpportunity();
       insert form;  
       system.assert(form!=null);  
       system.debug('form.Id');
       
        List<Competitor_Information__c> NewLogolist = new List<Competitor_Information__c>{
                    new Competitor_Information__c(Name='Account',Value__c=a.id),
                    new Competitor_Information__c(Name='Account Lookup',Value__c=a.id),
                    new Competitor_Information__c(Name='Opportunity',Value__c=a.id),
                    new Competitor_Information__c(Name='Opportunity Lookup',Value__c=a.id)};
        Insert NewLogolist;      
       
       system.currentPageReference().getParameters().put('oppId',opp.Id);
       //system.currentPageReference().getParameters().put('accountId',a.Id);
       
       
                            
       ApexPages.StandardController sc1 = new ApexPages.StandardController(opp);         
       NewCompInformationOverrideController h = new NewCompInformationOverrideController(sc1);
       
       system.currentPageReference().getParameters().put('retURL', '/'+form.Id);
       ApexPages.StandardController sc2 = new ApexPages.StandardController(form);
       NewCompInformationOverrideController h1 = new NewCompInformationOverrideController(sc2); 
       NewCompInformationOverrideController h2 = new NewCompInformationOverrideController(); 
      // System.assertEquals(h1.overrideNew().getURL(),'/'+form.Id);  
      h.overrideNew();   
       
       
        
}

private static Opportunity getOpportunity(){
        if(opp == null){
            opp = new Opportunity();
            a = getAccount();
            opp.Name = 'Test Opportunity 1';
            opp.AccountId = a.Id;
             //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
            opp.StageName = 'Identify & Define';
            opp.Stage__c='Identify & Define';
            opp.CloseDate = System.today();
             opp.Estimated_MRC__c=300;
            opp.Estimated_NRC__c=200;
            opp.ContractTerm__c='10';
           // opp.Approx_Deal_Size__c = 5000;
           system.assert(opp!=null);
            
        }
            return opp;
}
private static Account getAccount(){
        if(a == null){
            a = new Account();
            cl = getCountry();
            a.Name = 'Test Account Test 1';
            a.Customer_Type__c = 'MNC';
            a.Country__c = cl.Id;
            a.Selling_Entity__c = 'Telstra INC';
            a.Activated__c = true;
            a.Account_ID__c = '9090';
            a.Account_Status__c = 'Active';
            a.Customer_Legal_Entity_Name__c='Test';
            insert a;
            List<Account> acc = [Select id, name from Account where name = 'Test Account Test 1'];
           // system.assetequals(a[0].name,acc[0].name);
           system.assert(a!=null);
        }
        return a;
    }
    
    
    private static Country_Lookup__c getCountry(){
        if(cl == null){
            cl = new Country_Lookup__c();
            cl.CCMS_Country_Name__c = 'India';
            cl.Country_Code__c = 'IND';
            insert cl;
            system.assert(cl!=null);
        }
        return cl;
    }
    private static City_Lookup__c getCity(){
        if(ci == null){     
            ci= new City_Lookup__c();
            ci.City_Code__c ='BGL';
            ci.Name = 'BANGALORE';
            insert ci;
            List<City_Lookup__c> c = [Select id,name from City_Lookup__c where name = 'BANGALORE'];
            system.assertequals(ci.name,c[0].name);
            
            system.assert(ci!=null);
        }
        return ci;
  }
}