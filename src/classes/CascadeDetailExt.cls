Public with sharing class CascadeDetailExt {  
    Public ApexPages.StandardController controler;  
    public csord__Order__c ord{get; set;}     
    public User user {get;set;}   
    public csord__Order__c order1{get; set;}
          
    
    public List<csord__Order__c> ordList;
    public Solutions__c soln {get;set;}
    Public ID solnId{get;set;}
    Public List<Solutions__c> solList{get;set;}     
    public List<csord__Order__c> listToUpdate = new List<csord__Order__c>();        
    public CascadeDetailExt(ApexPages.StandardController std) {  
        order1 = new csord__Order__c();                 
        controler = std; 
        soln = (Solutions__c)std.getRecord(); 
        System.debug('soln####'+soln);        
        solnId =soln.id;  
            
    }    
    
    //Redirecting to cascadedetail page from SolutionSummary page   
    public PageReference solnSummary() {
            System.debug('solnId '+solnId );  
            PageReference solnPage = new PageReference('/apex/SolutionSummary?id='+solnId);
            solnPage.setRedirect(false);//should load the same instance of solution summary page
            return solnPage;
    }
    
     
    //Cancel the changes and redirect to same page
    public PageReference cancel(){        
        PageReference solnPage = new PageReference('/apex/SolutionSummary?id='+solnId);
            solnPage.setRedirect(true);//should load a refreshed solution summary page
            return solnPage;
    } 
}