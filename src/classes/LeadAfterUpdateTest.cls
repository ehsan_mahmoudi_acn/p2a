/**
@author - Accenture
@date - 25- Jun-2012
@version - 1.0
@description - This is the test class for LeadAfterUpdate Trigger
*/
@isTest
private class LeadAfterUpdateTest {

    static testMethod void myUnitTest() {

        // providing the values to custom setting object
        List<Global_Constant_Data__c> globalConstantDataObjList = new List<Global_Constant_Data__c>();

        Global_Constant_Data__c globalConstantDataObj;

        globalConstantDataObj = new Global_Constant_Data__c();
        globalConstantDataObj.Name = 'Account Manager';
        globalConstantDataObj.Value__c = '00eO0000000DiL1IAK';
        globalConstantDataObjList.add(globalConstantDataObj);

        globalConstantDataObj = new Global_Constant_Data__c();
        globalConstantDataObj.Name = 'Sales Manager';
        globalConstantDataObj.Value__c = '00eO0000000DiKzIAK';
        globalConstantDataObjList.add(globalConstantDataObj);

        globalConstantDataObj = new Global_Constant_Data__c();
        globalConstantDataObj.Name = 'Sales Admin';
        globalConstantDataObj.Value__c = '00eO0000000DiKxIAK';
        globalConstantDataObjList.add(globalConstantDataObj);

        insert globalConstantDataObjList;
         system.assert(globalConstantDataObjList!=null);
        Lead leadObj = getLead();
        //User userListObj = [Select Id,ProfileId from User where User.Profile_Name__c = 'TI Marketing' and User.IsActive = True Limit 1];
        //User userListObj = [Select Id,ProfileId from User where id= :UserInfo.getUserId()];

        Profile p = [select id from profile where name='TI Marketing'];

        User userListObj1 = new User(alias = 'Raone', email='raoneg1@testorg.com',
        emailencodingkey='UTF-8', lastname='TesterRaone', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id,
        timezonesidkey='America/Los_Angeles', username='raoneg1@testorg.com',EmployeeNumber='123');

        insert userListObj1 ;
        system.assert(userListObj1!=null);
        leadObj.OwnerId = userListObj1.Id;
        insert leadObj;
        //       User userObj = [Select Id,ProfileId from User where User.Profile_Name__c = 'TI Sales Manager' and User.IsActive = True Limit 1];

        Profile p1 = [select id from profile where name='TI Sales Manager'];

        User userObj= new User(alias = 'rhtdm', email='rhtdm@testorg.com',
        emailencodingkey='UTF-8', lastname='TesterRHTDM', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p1.Id,
        timezonesidkey='America/Los_Angeles', username='rhtdm@testorg.com', EmployeeNumber = '12649');

        insert userObj;
        system.assert(userObj!=null);
        leadObj.OwnerId = userObj.Id;
        update leadObj;
    }

    // Creating the Test Data 
    private static Lead getLead()
    {
        Lead leadObj1 = new Lead();
        leadObj1.LastName = 'bnajk';
        leadObj1.Company = 'abc Ltd';
        leadObj1.Status = ' Marketing Qualified Lead (MQL)';
        leadObj1.LeadSource = 'Event';
        leadObj1.Email = 'testabc@def.com';
        leadObj1.Industry = 'BPO';
        leadObj1.Country_Picklist__c = 'AFGHANISTAN';
         system.assert(leadObj1!=null);
        return leadObj1;
       
    }
}