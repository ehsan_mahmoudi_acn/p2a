@isTest(seealldata = false)
public class GeoUtilitiesTest
{
    public static List<Account> acclist =  P2A_TestFactoryCls.getAccounts(1);
     public static List<Country_Lookup__c> countrylookuplist =  P2A_TestFactoryCls.getcountry(1);
    public static List<Site__c> siteList =  P2A_TestFactoryCls.getsites(1,acclist,countrylookuplist);
   public static testMethod void myUnitTest1() 
   {
   List<String> siteidlist = new List<String>();
   for(Site__c sites:siteList){
   siteidlist.add(sites.id);
   system.assertEquals(true,siteidlist!=null);
   }
   Test.StartTest();
   GeoUtilities.updateSiteGeo(siteidlist);
   Test.StopTest();
   
   }
}