/*
    @author - Accenture
    @date - 09-JUNE-2012
    @version - 1.0
    @description - This is Util class.
*/
public class Util {
    public static Boolean triggerOppLineItemSOV = True; 
    public static Boolean orderRejectionFlag = True; 
    public static Boolean orderCancelationFlag = True;
    public static Boolean orderLineItemUpdateFlag = True;
    public static Boolean orderSubmitFlag = True;
    public static Boolean skipOrderAfterUpdateFlag = True;
    public static Boolean etcActionItemFlag = True;
    public static Boolean syncOpp=false;
    public static Id newOpportunityOwnerId;
    //IR171
    public static Boolean FlagToSkipDebitTwice=true;
    public static Boolean FlagtoSkipSOVtrigger=true;
    public static Boolean FlagtoSkipOpportunityTeamUpdateTwice=true;
    public static Boolean FlagtoSkipOpportunityAfterTrigger=true;
    public static Boolean FlagtoSkipOpportunitysplitTrigger=true;
    public static Boolean IsOppOwnerchange=false;
    public static Map<Id,decimal> mapsplitPercenatgeOfAcc=new Map<Id,decimal>();
    public static Boolean FlagtoMuteOpportunitySplitTrg=true;
    public static Boolean FlagtoMuteOpportunityDeleteSplitTrg=true;        
    public static Boolean FlagNeedReopen=true;
    public static Boolean ISOppSizeisZero=true;
    public static decimal splitPercentageForOwner=0;
    public static Id OldOwnerId;
    public static Boolean NoTeamUpdateRequired=true;
    public static Boolean skipOpportunitySplitManualCodeFlag=true;
    public static Boolean skipOpportunitySplitTeamTrgFlag=true;
              //IR171:skip OpportunitySplit trigger
     public static Boolean skipOpportunitySplitFlag=true;
     public static Boolean OppoProductTypeFlag=true;
     
     //IR171:Skip OpportunitySplit trigger
     //Bhargav -- duplicate AI for Cross Country to be skipped
     public static Boolean duplicateAIcreationSkipForCrossCountryApplication=true;
     //Bhargav -- duplicate AI for Cross Country to be skipped
     //Bhargav -- update opportunity on OSplit Modify AI Update
     public static Boolean OpportunityUpdateOnAIUpdate = true;
    //Bhargav -- skip opp team member update on CCA AI update
    public static Boolean OppTeamMemberSkipOnCCAAIUpdate = true;
             public static Boolean restrictRecurPerfCallFlag=true;
    public static Boolean skipoppAfterFlag=true;
    public static Boolean fromOppstageExtFlag=true;
    
     //CR52
    public static boolean rjectionEmailforOD=false;
    public static boolean rjectionEmailforPV=false;
    
    // IR 222 New Logo Boolean value
     Public static Boolean newLogoFlag = true;
     Public static Boolean notAIForNewLogo = true; 
     //Public static Boolean UpdateNewLogoAccount= false; 
   
    public static string strStreamId = 'STRM-OFFNET,STRM-ONNET,STRM,IPL-OSS-HC-STRM,IPL-OSS-HC-STRM,IPL-OSS-MES-STRM,IPL-OSS-Offnet-STRM,IPLSWCS-CSTRM,IPLSHC-STRM,IPL-OSS-STRM,IPLSHC-CSTRM,IPLSWCS-STRM,IPL-OSS-CSTRM';
    
    /*
        @author - Accenture
        @date - 16-JULY-2012
        @return Boolean - true/false to mute/execute all the triggers for the running 
        
         or org default
        @description - This method return true/false based on Custom Setting Global_Mute__c whether to run all the triggers or not
    */
    public static Boolean muteAllTriggers() {        
        try {
            if(Global_Mute__c.getInstance(UserInfo.getUserId()) != null) {
                return Global_Mute__c.getInstance(UserInfo.getUserId()).Mute_Triggers__c;
            } else {
                return Global_Mute__c.getOrgDefaults().Mute_Triggers__c;
            }
            
        }
        catch (Exception e) {
            return false;
        }
    }
    /*
        @description - This method return true/false based on Custom Setting Order_SD_update__c to mute code which updating SD PM and SD OM assign date.
    */
    public static Boolean muteDateUpdateOnOrder() {        
        try {
            if(Mute_SD_Order_Date__c.getInstance(UserInfo.getUserId()) != null) {
                return Mute_SD_Order_Date__c.getInstance(UserInfo.getUserId()).Mute_SD_dates__c;
            } 
            else {
                return Mute_SD_Order_Date__c.getOrgDefaults().Mute_SD_dates__c;
            }
            
        } 
        catch (Exception e) {
            return false;
        }
    }
    
    /*
        @description - This method return true/false based on Custom Settings mute orderlineitem triggers in Mute SD Order Date to mute code which Causing too many SOQL.
    */
    public static Boolean muteCreateUpdateServiceOrResource() {        
        try {
            if(Mute_SD_Order_Date__c.getInstance(UserInfo.getUserId()) != null) {
                return Mute_SD_Order_Date__c.getInstance(UserInfo.getUserId()).MuteCreateUpdateServiceOrResource__c;
            } else {
                return Mute_SD_Order_Date__c.getOrgDefaults().MuteCreateUpdateServiceOrResource__c;
            }
            
        } 
        catch (Exception e) {
            return false;
        }
    }
    
    public static Boolean muteOrderLineItemAfterUpdate() {        
        try {
            if(Mute_SD_Order_Date__c.getInstance(UserInfo.getUserId()) != null) {
                return Mute_SD_Order_Date__c.getInstance(UserInfo.getUserId()).MuteOrderLineItemAfterUpdate__c;
            } else {
                return Mute_SD_Order_Date__c.getOrgDefaults().MuteOrderLineItemAfterUpdate__c;
            }
            
        } 
        catch (Exception e) {
            return false;
        }
    }
    
    public static Boolean muteOrderLineItemBeforeInsertUpdate() {        
        try {
            if(Mute_SD_Order_Date__c.getInstance(UserInfo.getUserId()) != null) {
                return Mute_SD_Order_Date__c.getInstance(UserInfo.getUserId()).MuteOrderLineItemBeforeInsertUpdate__c;
            } else {
                return Mute_SD_Order_Date__c.getOrgDefaults().MuteOrderLineItemBeforeInsertUpdate__c;
            }
            
        } 
        catch (Exception e) {
            return false;
        }
    }
    
    public static Boolean muteOrderLineItemBeforeUpdate() {        
        try {
            if(Mute_SD_Order_Date__c.getInstance(UserInfo.getUserId()) != null) {
                return Mute_SD_Order_Date__c.getInstance(UserInfo.getUserId()).MuteOrderLineItemBeforeUpdate__c;
            } else {
                return Mute_SD_Order_Date__c.getOrgDefaults().MuteOrderLineItemBeforeUpdate__c;
            }
            
        } 
        catch (Exception e) {
            return false;
        }
    }
    
    /*
        @author - Accenture
        @date - 12-JUNE-2012
        @param - RecordType rtId
        @param - Opportunity oppId
        @param - Account accId
        @return - Action_Item__c objActionItem
        @description - This method inserts an action item based on the record type and return the action item object.
    */  
    public static Action_Item__c createActionItem (String name, Map<ID,ID> accountOppbillProfileMapId, Id oppId, Id billProfileId ){
        //List<Opportunity> oppt =[Select Pre_Contract_Provisioning_Required__c,Estimated_TCV__c,Account.Industry,Account.Customer_Type_New__c,Account.Country__r.Name,Owner.Email from Opportunity where id=:oppId];
        User curntUser1 = [Select Name, Region__c from User where id= :UserInfo.getUserId() limit 1];    

        //Assigining correct RecordtypeId 
        Action_Item__c objActionItem = new Action_Item__c();
        objActionItem.RecordTypeId = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType, name);

        //Assigning the opportunity, account and bill profile Ids to the action item.
        if(oppId != null){
            objActionItem.Opportunity__c = oppId;
            objActionItem.Account__c = accountOppbillProfileMapId.get(oppId);
        }
        else if(billProfileId != null){
            objActionItem.Bill_Profile__c = billProfileId;
            objActionItem.Account__c = accountOppbillProfileMapId.get(billProfileId);
        }
        for (Opportunity oppt1:[Select Pre_Contract_Provisioning_Required__c,Estimated_TCV__c,Account.Industry,Account.Customer_Type_New__c,Account.Country__r.Name,Owner.Email from Opportunity where id=:oppId]){
            if(oppt1.Pre_Contract_Provisioning_Required__c=='Yes'){
                ObjActionItem.Customer_Type__c=oppt1.Account.Customer_Type_New__c;
                ObjActionItem.Account_industry__c=oppt1.Account.Industry;
                ObjActionItem.Country__c=oppt1.Account.Country__r.Name;
                ObjActionItem.Previous_NRC__c=oppt1.Estimated_TCV__c;
                ObjActionItem.Opportunity_Email__c=oppt1.Owner.Email;
                ObjActionItem.Created_By_Region__c=curntUser1.Region__c;
            }
        }
        objActionItem.Status__c = Global_Constant_Data__c.getvalues('Action Item Status').value__c;
        objActionItem.Priority__c = Global_Constant_Data__c.getvalues('Action Item Priority').value__c;
        objActionItem.Due_Date__c = System.Today() + 2;
    
        return objActionItem;
    }

    /*
        @author - Accenture
        @date - 12-JUNE-2012
        @description - This method assigns the action item owners to the respective regional queues.
    */      
    public static void assignOwnersToRegionalQueue(User usr,Action_Item__c objActionItem, String recordType){  
        //Find the Account Customer Type based on ActionItem Account
        String accCustomerType=null;
        String accLegalEntityName=null;
        String billingEntity=null;
        if (objActionItem.Account__c!=null){
            Account acc =[Select id,Customer_Type__c,Telstra_Legal_Entity_Name__c from Account where id=:objActionItem.Account__c limit 1];
            accCustomerType= acc.Customer_Type__c;
            accLegalEntityName= acc.Telstra_Legal_Entity_Name__c;  // added as part of IR215
        }
        
        // added as part of IR215        
        System.debug('Object Action ITem::'+objActionItem.Bill_Profile__c);        
        if (objActionItem.Bill_Profile__c!=null){            
             BillProfile__c billProf =[Select id,Billing_Entity__c from BillProfile__c where id=:objActionItem.Bill_Profile__c limit 1];            
             System.debug('Bill Profile Billing Enitity :::'+billProf.Billing_Entity__c);           
             billingEntity= billProf.Billing_Entity__c;                   
          }        
        System.debug('Billing Entity Test::'+billingEntity);
        
        if( recordType == 'Request Contract'){
            if(usr.Region__c!=null){        
                if(usr.Region__c=='North Asia'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Legal/ Contract Mgt - North Asia');
                }
                else if(usr.Region__c=='South Asia'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Legal/ Contract Mgt - South Asia');
                }
                else if (usr.Region__c=='Australia'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Legal/ Contract Mgt – Australia');
                }
                else if (usr.Region__c=='EMEA'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Legal/ Contract Mgt – EMEA');
                }
                else if(usr.Region__c=='US'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Legal/ Contract Mgt – US');
                }
            }
            else {
                objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Legal/ Contract Mgt – Australia');
            }           
        }else if( recordType == 'Request to Modify Opportunity Split'){
            if(usr.Region__c!=null){        
                if(usr.Region__c=='North Asia'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType,'Opp Split Approval - Asia');   
                }
                else if(usr.Region__c=='South Asia')
                {
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType,'Opp Split Approval - Asia');
                 }
                 else if (usr.Region__c=='Australia')
                {
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType,'Opp Split Approval - Australia');
                }
                
                else if (usr.Region__c=='EMEA')
                {
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType,'Opp Split Approval - EMEA');
                }
                else if(usr.Region__c=='US')
                {
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType,'Opp Split Approval - US');
                }
            }
            else 
            {
            
                objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType,'Opp Split Approval - Australia');
           }
        }
           else if( recordType == 'Request Opp Split Approval'){
            if(usr.Region__c!=null){        
                if(usr.Region__c=='North Asia'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType,'Opp Split Approval - Asia');   
                }
                else if(usr.Region__c=='South Asia')
                {
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType,'Opp Split Approval - Asia');
                 }
                 else if (usr.Region__c=='Australia')
                {
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType,'Opp Split Approval - Australia');
                }
                
                else if (usr.Region__c=='EMEA')
                {
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType,'Opp Split Approval - EMEA');
                }
                else if(usr.Region__c=='US')
                {
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType,'Opp Split Approval - US');
                }
            }
            else 
            {
            
                objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType,'Opp Split Approval - Australia');
           }
        
           }
        else if(recordType == 'Request for Bill Profile Approval'){
        
                Boolean IsPacnetEntity = false;
                List<PACNET_Entities__c> allPacnetEntities = PACNET_Entities__c.getall().values();  
                for(PACNET_Entities__c Pacnet: allPacnetEntities)
                    {
                      if (billingEntity!=null && billingEntity == Pacnet.PACNET_Entity__c){
                          IsPacnetEntity = true;
                          break;                       
                      }
                    }
           if(!IsPacnetEntity) {
            if(usr.Region__c!=null){        
                if(usr.Region__c=='North Asia'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Billing - North Asia');   
                }
                else if(usr.Region__c=='South Asia'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Billing - South Asia');
                }
                else if (usr.Region__c=='Australia'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Billing - Australia');
                }
                else if (usr.Region__c=='EMEA'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Billing - EMEA');
                }
                else if(usr.Region__c=='US'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Billing - US');
                }
            }
            else{
                objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Billing - Australia');
              }  
            } 
            // added as part of IR215          
            if(IsPacnetEntity){
                objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Billing - PACNET');         
            }   
        }
        else if( recordType == 'Adhoc Request'){
            if(usr.Region__c!=null){        
                if(usr.Region__c=='North Asia'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Legal/ Contract Mgt - North Asia');   
                }
                else if(usr.Region__c=='South Asia'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Legal/ Contract Mgt - South Asia');
                }
                else if (usr.Region__c=='Australia'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Legal/ Contract Mgt – Australia');
                }
                else if (usr.Region__c=='EMEA'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Legal/ Contract Mgt – EMEA');
                }
                else if(usr.Region__c=='US'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Legal/ Contract Mgt – US');
                }
            }
            else {
                objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Legal/ Contract Mgt – Australia');
            }           
        }
        else if(recordType == 'Request Credit Check'){
        
           Boolean IsPacnetEntity = false;
           List<PACNET_Entities__c> allPacnetEntities = PACNET_Entities__c.getall().values();  
            for(PACNET_Entities__c Pacnet : allPacnetEntities)
                    {
                      if (accLegalEntityName!=null && accLegalEntityName == Pacnet.PACNET_Entity__c){
                          IsPacnetEntity = true;
                          break;                        
                      }
                    }
        
          if(accLegalEntityName==null || !IsPacnetEntity){
            if(usr.Region__c!=null){      
                if(usr.Region__c=='North Asia'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Credit Control-North Asia');  
                }
                else if(usr.Region__c=='South Asia'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Credit Control-South Asia');
                }
                else if (usr.Region__c=='Australia'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Credit Control-Australia');
                }
                else if (usr.Region__c=='EMEA'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Credit Control-EMEA');
                }
                else if(usr.Region__c=='US'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Credit Control-US');
                }
            }
            else {
                objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Credit Control-Australia');
              } 
            } 
            // added as part of IR215
           else if(IsPacnetEntity){
                objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Credit Control-PACNET');             
            }  
        }
        // Added as a part of CR412
        else if(recordType == System.Label.REQUEST_SERVICE_DETAILS){
            if(usr.Region__c!=null){        
                if(usr.Region__c==System.Label.NORTH_ASIA){
                    objActionItem.OwnerId = System.Label.SERVICE_DELIVERY_NORTH_ASIA;   
                }
                else if(usr.Region__c==System.Label.SOUTH_ASIA){
                    objActionItem.OwnerId = System.Label.SERVICE_DELIVERY_SOUTH_ASIA;
                }
                else if (usr.Region__c==System.Label.AUSTRALIA){
                    objActionItem.OwnerId = System.Label.SERVICE_DELIVERY_AUSTRALIA;
                }
                else if (usr.Region__c==System.Label.EMEA){
                    objActionItem.OwnerId = System.Label.SERVICE_DELIVERY_EMEA;
                }
                else if(usr.Region__c==System.Label.US){
                    objActionItem.OwnerId = System.Label.SERVICE_DELIVERY_US;
                }
            }
            else {
                objActionItem.OwnerId = System.Label.SERVICE_DELIVERY_AUSTRALIA;
            }   
        
        }
        // CR412 changes ends here.
        //CR320 part2 code starts here
        else if(recordType == 'Request Feasibility Study'){
            if(usr.Region__c!=null && accCustomerType!=null){      
                if(usr.Region__c=='North Asia'&& accCustomerType=='MNC'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Tech Consulting-North Asia MNC');  
                }
                else if(usr.Region__c=='North Asia'&& accCustomerType=='GSP'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Tech Consulting-North Asia GSP');
                }
                else if(usr.Region__c=='South Asia'&& accCustomerType=='MNC'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Tech Consulting-South Asia MNC');
                }
                else if(usr.Region__c=='South Asia'&& accCustomerType=='GSP'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Tech Consulting-South Asia GSP');
                }
                else if (usr.Region__c=='Australia'&& accCustomerType=='MNC'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Tech Consulting-Australia MNC');
                }
                else if (usr.Region__c=='Australia'&& accCustomerType=='GSP'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Tech Consulting-Australia GSP');
                }
                else if (usr.Region__c=='EMEA'&& accCustomerType=='MNC'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Tech Consulting-EMEA');
                }
                else if (usr.Region__c=='EMEA'&& accCustomerType=='GSP'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Tech Consulting-EMEA');
                }
                else if(usr.Region__c=='US'&& accCustomerType=='MNC'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Tech Consulting-US MNC');
                }
                else if(usr.Region__c=='US'&& accCustomerType=='GSP'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Tech Consulting-US GSP');
                }
            }
            else {
                objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Tech Consulting-Australia MNC');
            } 
        }
        else if(recordType == 'Request Pricing Approval'){
            if(usr.Region__c!=null && accCustomerType!=null){      
                if(usr.Region__c=='North Asia'&& accCustomerType=='MNC'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'GPD Asia ANZ');  
                }
                else if(usr.Region__c=='South Asia'&& accCustomerType=='MNC'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'GPD Asia ANZ');
                }
                else if (usr.Region__c=='Australia'&& accCustomerType=='MNC'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'GPD Asia ANZ');
                }               
                else if(usr.Region__c=='US'&& accCustomerType=='MNC'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'GPD ENT US EMEA');
                }
                else if(usr.Region__c=='EMEA'&& accCustomerType=='MNC'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'GPD ENT US EMEA');
                }
                else if(accCustomerType=='GSP'){
                    objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'GPD GW');
                }
            }
            else {
                objActionItem.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'GPD Asia ANZ');
            }             
        }
    }
             
    /*
        @author - Accenture
        @date - 20-JAN-2014
        @param - newList oldMap
        @return -true or false
        @description - It compares old Roll-Up data with new Roll-Up data
    */
    public static Boolean  compareOldwithNewRollUpData(List<sObject> newList,Map<Id,sObject> oldMap){
          try{
            sObject newsObj=null;
            sObject oldsObj=null;
            Boolean flag=false;
            Map<String, Schema.SobjectField> fields=null;
            Map<Id,String> ObjValues = new Map<Id,string>();

            system.debug('===oldMap=='+oldMap);
            system.debug('===newList=='+newList);
            //In delete case,oldMap!=null and newList=null.. In insert case oldMap=null and newList!=null
            if(oldMap!=null && newList!=null){
                          
            fields = newList[0].getSObjectType().getDescribe().fields.getMap();
 
            for(sObject sObj:newList) {   
                oldsObj=oldMap.get(sObj.Id);
               for (String field : fields.keyset()) { 
                   system.debug('----field ----'+field);
                   system.debug('====Calculatedformula=='+String.isEmpty(fields.get(field).getDescribe().getCalculatedFormula()));
                   if(String.isEmpty(fields.get(field).getDescribe().getCalculatedFormula()) && fields.get(field).getDescribe().isCalculated()){
                   system.debug('============'+fields.get(field).getDescribe());
                   system.debug('======Name======'+fields.get(field).getDescribe().getSobjectField());
                   system.debug('====value======='+sObj.get(fields.get(field).getDescribe().getSobjectField()));
                   if(sObj.get(fields.get(field).getDescribe().getSobjectField())!=oldsObj.get(fields.get(field).getDescribe().getSobjectField())){
                                flag=true;
                                return true;
                            }
                         }
                     }
                  }    
             }
               
            system.debug('=====flag===='+flag);
            return  false;  
            }
            catch(Exception ex){
            system.debug('Exception ocurred in compareOldwithNewCalculatedData'+ex);
            return false;
        }        
     }             
}