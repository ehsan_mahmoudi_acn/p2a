public class TibcoServiceOrderCreator {
    csord__Order__c order = null;
    String udfAttributeName = 'A_Side_Site__c, Customer_Required_Date__c, Scheduled_Date__c, Service_Date__c, Technical_Customer_Contact_Email_Address__c, Technical_Customer_Contact_Name__c, Technical_Customer_Contact_Phone_Number__c, Z_Side_Site__c, Primary_Service_Id__c, Ordered_by_TELSTRA_CUSTOMER, Customer_Installation_Address__c';
    public tibcoServiceOrder createOrderFromServiceId(String OrderId)
    {
        
        csord__Order__c order = [SELECT Id,Name,csord__Order_Number__c,Account_ID__c, csord__Order_Type__c,Order_Type__c FROM csord__Order__c WHERE Id = :OrderId];
        TibcoServiceOrder serviceOrder = new TibcoServiceOrder(null, order.csord__Order_Number__c, order.Order_Type__c);        
        
        TibcoOrderHeader header = new TibcoOrderHeader();
        header.customerID = order.Account_ID__c;

        header.description = order.Name;
                
        serviceOrder.header = header;
        
        integer lineNumber = 1;
        string action;
    
        
        csord__Service__c[] lineItems = [SELECT Name,Linked_Service_ID__c,Inventory_Status__c,csord__Order__r.Sharepoint_URL__c,Linked_Service_ID__r.csord__Order__r.Is_Order_Submitted__c,Linked_Service_ID__r.Product_Configuration_Type__c,Linked_Service_ID__r.Name,Associated_Service_ID__c,Solutions__r.Solution_ID__c,csordtelcoa__Product_Configuration__r.Product_Name__c,Related_Service_for_Simple_L__r.name,Product_Id__c,Order_Type__c,Ordertype__c,Line_Number__c,csordtelcoa__Product_Configuration__c, Id, Account_ID__c,Technical_Customer_Contact_Phone_Number__c, Technical_Customer_Contact_Name__c, Carrier_Quote_obtained__c, End_Customer_Name__c, SD_PM_Name__c, Site_Contact_Number__c, Supplier_s_leadtime__c, Primary_Service_ID__c, Specific_quote_requirement__c, Customer_Site_Office_Hours__c, Technical_Customer_Contact_Email_Address__c,Purchase_Requisition_Number__c,Purchase_Order_Number__c,Oppurtunity_Id__c,acity_side__c,zcity_side__c,csord__Service__c,csord__order__r.Customer_Turn_Up_Test_Date__c,csord__order__r.GCC_Reseller_Account_ID__c,Parent_ServiceID__c FROM csord__Service__c WHERE csord__Order__c = :OrderId];
        
        TibcoOrderLine[] lineList = new TibcoOrderLine[350];
        TibcoOrderLine[] actionList = new TibcoOrderLine[350];
        
        Set<Id> lineItemsIDLists = new Set<Id>();
        set<Id>LineItemPcId=new Set<Id>();
        for(csord__Service__c lineItem : lineItems )
        {
            if(lineItem.Acity_Side__c!=null){
                lineItemsIDLists.add(lineItem.Acity_Side__c);
            }
            if(lineItem.Zcity_Side__c!=null){
                lineItemsIDLists.add(lineItem.Zcity_Side__c);
            }
            if(lineItem.csordtelcoa__Product_Configuration__c!=null){
                LineItemPcId.add(lineItem.csordtelcoa__Product_Configuration__c);
            }
        }
        Map<Id,Site__c> sitesLists = new Map<Id,Site__c>([select id,name from Site__c where Id in : lineItemsIDLists]);
       
        List<cscfga__Attribute__c>AllAttributesList = [Select Name,cscfga__Display_Value__c,cscfga__Value__c,cscfga__Product_Configuration__r.cscfga__Root_Configuration__c,cscfga__Product_Configuration__r.Name,cscfga__Product_Configuration__c from cscfga__Attribute__c where cscfga__Product_Configuration__c = :LineItemPcId AND name NOT IN('Customer_Required_Date__c')];
        Map<Id,List<cscfga__Attribute__c>>PcToAttListMap=new Map<Id,List<cscfga__Attribute__c>>();
        Map<Id,List<cscfga__Attribute__c>>RootPcToAttListMap=new Map<Id,List<cscfga__Attribute__c>>();
        Map<Id,csord__Service__c>PcToServiceNameMap=new Map<Id,csord__Service__c>();
        Map<Id,csord__Service__c>ASBRServiceNameMap=new Map<Id,csord__Service__c>();
        set<Id>linkedPcIdSet=new set<id>();
        set<Id>linkedASBRServiceSet=new set<id>();
        for(cscfga__Attribute__c attr:AllAttributesList){
            if(PcToAttListMap.containsKey(attr.cscfga__Product_Configuration__c)){
                PcToAttListMap.get(attr.cscfga__Product_Configuration__c).add(attr);
            }
            else{
                List<cscfga__Attribute__c>tempList=new List<cscfga__Attribute__c>();
                tempList.add(attr);
                PcToAttListMap.put(attr.cscfga__Product_Configuration__c,tempList);
            }
            
            //for root PC attribute List
            if(RootPcToAttListMap.containsKey(attr.cscfga__Product_Configuration__r.cscfga__Root_Configuration__c)){
                RootPcToAttListMap.get(attr.cscfga__Product_Configuration__r.cscfga__Root_Configuration__c).add(attr);
            }
            else{
                List<cscfga__Attribute__c>tempList=new List<cscfga__Attribute__c>();
                tempList.add(attr);
                RootPcToAttListMap.put(attr.cscfga__Product_Configuration__r.cscfga__Root_Configuration__c,tempList);
            }
            
            
            //this is different if
            if(attr.cscfga__Value__c!=null && attr.name=='LinkedParentID'){
                List<Id>tempLinkedPcIdList=attr.cscfga__Value__c.split(',');
                linkedPcIdSet.addAll(tempLinkedPcIdList);
            }
            if(attr.cscfga__Value__c!=null && attr.name=='ASBRAsServiceMultiple'){
                List<Id>linkedASBRList=attr.cscfga__Value__c.split(',');
                linkedASBRServiceSet.addAll(linkedASBRList);
            }
        }
        
        if(!linkedPcIdSet.isEmpty()){
        List<csord__Service__c>linkedServiceList=[select name,csordtelcoa__Product_Configuration__c,id from csord__Service__c where csordtelcoa__Product_Configuration__c IN:linkedPcIdSet];
            for(csord__Service__c ser:linkedServiceList){
                PcToServiceNameMap.put(ser.csordtelcoa__Product_Configuration__c,ser);
            }
        }
        if(!linkedASBRServiceSet.isEmpty()){
        List<csord__Service__c>linkedASBRServiceList=[select name,id from csord__Service__c where Id IN:linkedASBRServiceSet];
            for(csord__Service__c service:linkedASBRServiceList){
                ASBRServiceNameMap.put(service.id,service);
            }
        }
        
        for (csord__Service__c lineItem : lineItems)
        {
         Site__c site_a_name,site_z_name;  
     
         if(lineItem.Acity_Side__c!=null){
             site_a_name = sitesLists.get(lineItem.Acity_Side__c);
          //site_a_name=[select id,name from Site__c where Id=:lineItem.Acity_Side__c];
          }
          if(lineItem.Zcity_Side__c!=null){
              site_z_name = sitesLists.get(lineItem.Zcity_Side__c);
          //site_z_name=[select id,name from Site__c where Id=:lineItem.Zcity_Side__c];  
          }      
         
            TibcoOrderLine orderLine = new TibcoOrderLine();
            orderLine.lineNumber = string.valueof(lineNumber);
            orderLine.action = lineItem.Ordertype__c;
            /*
            *defect fix:13565 
            *RCA:The product Id is being sent as "IPL" for "OS" under GIAAS
            *fix:added &&lineItem.Product_Id__c!='OS' in condition
            *developer:ritesh
            *date:7-dec-2016
            */
           if(Label.IPL_product_IDs.Contains(lineItem.Product_Id__c) && lineItem.Product_Id__c!='OS'){
                orderLine.productID = 'IPL';
            }
            else if(Label.EPL_product_IDs.Contains(lineItem.Product_Id__c) && lineItem.Product_Id__c!='OS'){
                orderLine.productID = 'EPL';
                }
            else{
                orderLine.productID = lineItem.Product_Id__c; 
                }
            
            cscfga__Attribute__c[] Attributes =null;
            if(PcToAttListMap.containsKey(lineItem.csordtelcoa__Product_Configuration__c)){
                Attributes=PcToAttListMap.get(lineItem.csordtelcoa__Product_Configuration__c);
            }
           
            
            List<Udf> udfList = new List<Udf>();
            Id GVOIPId;
            Id TWIId;
            String ParentPcOfSharedASBR='';//added for CR19
            String ParentServiceOfSharedASBR='';//added for CR19
            String ASBRServiceMultiple='';
            String ASBRServices ='';
            for (cscfga__Attribute__c Attribute : Attributes)
            {
                //UAT defect fix:13048
                if(Attribute.cscfga__Value__c!=null){
				    
                    String UDFVal=Attribute.Name=='Customer Address'?Attribute.cscfga__Display_Value__c:Attribute.cscfga__Value__c;
					//String UDFVal=Attribute.cscfga__Value__c;
                    UDFVal=UDFVal.replaceAll('\\<.*?\\>','');
                    Udf udfElement = new Udf(Attribute.Name, UDFVal);                
                    udfList.add(udfElement);
                }
                //Added to get the parent services of Shared ASBR
                if(Attribute.cscfga__Value__c!=null && Attribute.name=='LinkedParentID'){
                    ParentPcOfSharedASBR=Attribute.cscfga__Value__c;
                }
                  
                //for GVOIP product, store the product configuration Id.
                if(Attribute.cscfga__Product_Configuration__r.Name == 'GVOIP' && GVOIPId == null){
                    GVOIPId = Attribute.cscfga__Product_Configuration__c;
                }
                
                //for TWI product, store the product configuration Id.
                if(Attribute.cscfga__Product_Configuration__r.Name == 'TWI' && TWIId == null){
                    TWIId = Attribute.cscfga__Product_Configuration__c;
                } 
                if(Attribute.cscfga__Value__c!=null && Attribute.name=='ASBRAsServiceMultiple'){
                    ASBRServiceMultiple=Attribute.cscfga__Value__c;
                }               
            }
            
            /**
             * Adding all attributes of GVOIP's child products to the UDF List.
             * and to send the udf list from the parent GVOIP line item. 
             */
            if(GVOIPId != null && RootPcToAttListMap!=null && RootPcToAttListMap.containsKey(GVOIPId)){
                
                cscfga__Attribute__c[] gvoipAttributes = RootPcToAttListMap.get(GVOIPId);
                Set<String>AvoidGvoipAttributes=new Set<String>{'Customer_Required_Date__c','Service Type','Product Code','Product ID'};
                for(cscfga__Attribute__c Attribute :gvoipAttributes){
                    if(Attribute.cscfga__Value__c != null && !AvoidGvoipAttributes.contains(Attribute.name)){
                        String UDFVal = Attribute.cscfga__Value__c;
                        UDFVal = UDFVal.replaceAll('\\<.*?\\>','');
                        Udf udfElement = new Udf(Attribute.Name, UDFVal);                
                        udfList.add(udfElement);
                    }
                }
            }

            /**
             * Adding all attributes of TWI's child products to the UDF List.
             * and to send the udf list from the parent TWI line item. 
             */
            if(TWIId != null && RootPcToAttListMap!=null && RootPcToAttListMap.containsKey(TWIId)){
                
                cscfga__Attribute__c[] TWIAttributes = RootPcToAttListMap.get(TWIId);
                Set<String>AvoidTwiAttributes=new Set<String>{'Customer_Required_Date__c','Service Type','Product Code','Product ID','Customer Address'};
                for(cscfga__Attribute__c Attribute :TWIAttributes){
                    if(Attribute.cscfga__Value__c != null && !AvoidTwiAttributes.contains(Attribute.name)){
                        String UDFVal = Attribute.cscfga__Value__c;
                        UDFVal = UDFVal.replaceAll('\\<.*?\\>','');
                        Udf udfElement = new Udf(Attribute.Name, UDFVal);                
                        udfList.add(udfElement);
                    }
                }
            }
            //CR19:For Shared ASBR, we will be sending the service Name of parents in a comma separated String
            set<String>ASBRProductId=new Set<String>{'OFFPOPA','OFFPOPB','VLL-A'};
             if(ParentPcOfSharedASBR!='' && ParentPcOfSharedASBR!=null && ASBRProductId.contains(lineitem.Product_Id__c)){
                 List<Id>parentPcListOfASBR=ParentPcOfSharedASBR.trim().split(',');
                                 
                 if(parentPcListOfASBR.size()>0){
                  //List<csord__service__c>parentServList=[select name from csord__service__c where csordtelcoa__Product_Configuration__c IN:parentPcListOfASBR]; 
                  for(Id pcId:parentPcListOfASBR){
                     if(PcToServiceNameMap.containsKey(pcId)){
                     ParentServiceOfSharedASBR=ParentServiceOfSharedASBR+PcToServiceNameMap.get(pcId).name+','; 
                     }
                  }
                  ParentServiceOfSharedASBR=ParentServiceOfSharedASBR.removeEnd(',');
                 }
                    
             }
             String AssociatedServiceIdValue=ParentServiceOfSharedASBR!='' && ParentServiceOfSharedASBR!=null?ParentServiceOfSharedASBR:lineitem.Associated_Service_ID__c;
             //CR19 logic ends
             
        
                //set<String>ASBRProductId=new Set<String>{'OFFPOPA','OFFPOPB','VLL-A'};
                Set<String> IpvpnWithASBRId = new Set<String>{'IPVPN-BACKPOP-OFF','CUSTSITEOFFNETPOP'};
                 if(ASBRServiceMultiple!='' && ASBRServiceMultiple!=null && IpvpnWithASBRId.contains(lineitem.Product_Id__c)){
                     List<Id> ASBRServiceMultipleId = ASBRServiceMultiple.trim().split(',');
                 
                 if(ASBRServiceMultipleId.size()>0){
                     for(Id serId : ASBRServiceMultipleId)
                     {
                         if(ASBRServiceNameMap.containsKey(serId)){
                             ASBRServices = ASBRServices + ASBRServiceNameMap.get(serId).name +',';
                         }
                         
                     }
                     ASBRServices = ASBRServices.removeEnd(',');
                 }
                 }
             String ASBRServicesWithIPVPN=ASBRServices!='' && ASBRServices!=null?ASBRServices:lineitem.Associated_Service_ID__c;
             
             Udf udfElement1 = new Udf('AccountIdService', lineItem.Account_ID__c);
             udfList.add(udfElement1);
             //Added try catch block as part ticket #TGP0041867 because for termination orders Sites are not mandatory.
            try{
             
             Udf udfElement2 = new Udf('a_end_site_name__c', site_a_name.name);
             udfList.add(udfElement2);
             
             Udf udfElement3 = new Udf('z_end_site_name__c', site_z_name.name);
             udfList.add(udfElement3);
             } catch(Exception e){                    
                    System.debug('Error: "AllProductConfigurationTriggerHandler; setProductBasketExchangeRate" mehotd update failure - ' +e);}
             
              Udf udfElement4 = new Udf('linkparentid',lineitem.id);
             udfList.add(udfElement4);

             Udf udfElement5 = new Udf('linkedparentid',lineitem.csord__Service__c);
             udfList.add(udfElement5);
             
             if(lineitem.csord__order__r.Customer_Turn_Up_Test_Date__c!=null){
             Udf udfElement6 = new Udf('Customer Turn Up Test Date',String.valueOf(lineitem.csord__order__r.Customer_Turn_Up_Test_Date__c));
             udfList.add(udfElement6);
             }
             
             Udf udfElement7 = new Udf('Order__c',Order.id);
             udfList.add(udfElement7); // Added to send Order's Id for updating back the Order Submission failure
       
             if(lineitem.csord__order__r.GCC_Reseller_Account_ID__c!=null){
             Udf udfElement8 = new Udf('GCC_Reseller_Account_ID__c',String.valueOf(lineitem.csord__order__r.GCC_Reseller_Account_ID__c));
             udfList.add(udfElement8);
             }
              
             Udf udfElement9 = new Udf('Product_Name__c',lineitem.csordtelcoa__Product_Configuration__r.Product_Name__c);
             udfList.add(udfElement9); // Added to send Product Name to T2R system when order submission
       
       
             Udf udfElement10 = new Udf('Related_Service_for_Simple_L__c',lineitem.Related_Service_for_Simple_L__r.Name);
             udfList.add(udfElement10);
        
             //CR019 change
       
             Udf udfElement11 = new Udf('Solution_ID__c',lineitem.Solutions__r.Solution_ID__c);
             udfList.add(udfElement11);
             //CR019 change          

            if(lineitem.Linked_Service_ID__c != null && (lineitem.Linked_Service_ID__r.csord__Order__r.Is_Order_Submitted__c || lineitem.Linked_Service_ID__r.Product_Configuration_Type__c != 'New Provide')){
                Udf udfElement12 = new Udf('Linked_Service_ID_Value__c',lineitem.Linked_Service_ID__r.Name);
                udfList.add(udfElement12); 
             }      

            if(ParentPcOfSharedASBR == null || ParentPcOfSharedASBR == ''){
                Udf udfElement13 = new Udf('Parent_ServiceID__c',lineitem.Parent_ServiceID__c);
                udfList.add(udfElement13);
             }
             
             Udf udfElement14 = new Udf('Associated_Service_ID__c',AssociatedServiceIdValue);
             udfList.add(udfElement14);
       
            //TGP0041949 - Share point URL
             Udf udfElement15 = new Udf('Share_Point_URL__c',lineitem.csord__Order__r.Sharepoint_URL__c);
             udfList.add(udfElement15);     
       
             udfList = getUdfValue(lineItem.ID, udfList);
             orderLine.udf=udfList;
             
             Udf udfElement16 = new Udf('Inventory_Status__c',lineitem.Inventory_Status__c);
             udfList.add(udfElement16);
             
             Set<String> IpvpnWithASBR = new Set<String>{'IPVPN-BACKPOP-OFF','CUSTSITEOFFNETPOP'};
             if(IpvpnWithASBR.contains(lineitem.Product_Id__c)){
                Udf udfElement17 = new Udf('Associated_Service_ID__c',ASBRServicesWithIPVPN);
             udfList.add(udfElement17); 
             }
             lineNumber += 1;
             lineList.add(orderLine);
             //actionList.add(orderLine);
           }

            serviceOrder.lines=lineList;
            /**
            * CR-23 changes 
            */
            TibcoOrderLine[]CatList=CreateOrderFromCATserviceId(OrderId,lineNumber);
        
            for(TibcoOrderLine tb:CatList){
                serviceOrder.lines.add(tb);
            }
            
            
            return serviceOrder;
   
       
        
    }
    //for @testvisible, modified by Anuradha
  @testvisible
    private List<Udf> getUdfValue(String serviceID, List<Udf> udfList){
        String udfQuery = 'select ID,Specific_quote_requirement__c,Customer_Site_Office_Hours__c,Purchase_Order_Number__c,Purchase_Requisition_Number__c,A_Side_Site__c,Customer_Required_Date__c, Scheduled_Date__c, Service_Date__c, Technical_Customer_Contact_Email_Address__c, Technical_Customer_Contact_Name__c, Technical_Customer_Contact_Phone_Number__c, Selling_Entity__c, Supplier_Name__c, Supplier_Quote_ID__c,'+ 
                        'Z_Side_Site__c, Primary_Service_Id__c,G_Bandwidth_Gen_name__c,MRC_Cost__c,NRC_Cost__c,Telstra_Billing_Entity__c,Bilateral_POP_ID__c,Onnet_Path_id__c,ParentInventoryId__c,Type_of_Update__c, Ordered_by_TELSTRA_CUSTOMER__c, Customer_Installation_Address__c, FCIDN__c, Contract_Sign_Date__c, Customer_PO__c,csord__order__r.Customer_Turn_Up_Test_Date__c, SN_request_item_sysid__c, csordtelcoa__Service_Number__c, Oppurtunity_Id__c,'+ 
                        'Customer_AS_Number__c,CCMS_RootProduct_PathId__c,Supplier_s_leadtime__c,Associated_Service_ID__c,Share_Point_URL__c,region__c,ResourceId__c,Countries__c,Site_Contact_Number__c,Carrier_Quote_obtained__c,End_Customer_Name__c,Estimated_Start_Date__c,Path_Instance_ID__c,Parent_Inventory_Instance_ID__c,Order_Types__c, Product_Code__c,Parent_Product__c,Product_Configuration_Type__c, SRF_document_links__c,Customer_Technical_Contact_Mobile__c,PO_End_Date__c,SD_PM_Contact_Number__c,SD_PM_Name__c,Contracting_Entity__c,Product_Id__c,End_Customer_Account_ID__c,End_Customer_Contact_ID__c,End_Customer_IPVPN_Opportunity_ID__c,End_Customer_IPVPN_Service_ID__c,End_Customer_Name_IPVPN__c,Parent_ServiceID__c' +
                        ' from csord__Service__c where id = '+'\''+serviceID+'\'';
       
        List<csord__Service__c> udfItems = Database.query(udfQuery);
        
   // Looping of Custom Setting " Service Values " to obtain field API Names to be passed     
    for(Service_Values__c ServiceVal : Service_Values__c.getAll().values()){
   // Check and add fields with non null values to the List
      if(String.valueof(udfItems[0].get(ServiceVal.Service_Types__c))!=null)
       {
   // Add API name and the value of the corresponding field to the list dynamically
         Udf udfElement1 = new Udf(ServiceVal.Name,String.valueof(udfItems[0].get(ServiceVal.Service_Types__c)));
         udfList.add(udfElement1);
       }
       /*
       *defect fix:13375
       *developer:ritesh
       *date:6-dec-2016
       *description of fix: for iso orders since there is no opportunity id, so hardcoded value will be sent from SFDC to avoid failure in CCMS(mandatory field at CCMS)
       */
       if(ServiceVal.Name=='Oppurtunity_Id__c'&& String.valueof(udfItems[0].get(ServiceVal.Service_Types__c))==null){
         Udf udfElement1 = new Udf(ServiceVal.Name,'ISO:12345');
       }
      }
        return udfList;
        
    }
    
    /**
    * Author:Pavan
    * CR-23  
    *
    */
    public TibcoOrderLine[] createOrderFromCATserviceId(String OrderId,Integer lineno) {
        
        system.debug('*******CAT');
        
        List<Udf> udfList = new List<Udf>();
        csord__Order__c order = [SELECT Id,Name,csord__Order_Number__c,Account_ID__c, csord__Order_Type__c,Order_Type__c FROM csord__Order__c WHERE Id = :OrderId];
        TibcoServiceOrder serviceOrder = new TibcoServiceOrder(null, order.csord__Order_Number__c, order.Order_Type__c);        
        
        TibcoOrderHeader header = new TibcoOrderHeader();
        header.customerID = order.Account_ID__c;

        header.description = order.Name;
                
        serviceOrder.header = header;
        
        integer lineNumber = lineno;
        string action;       
        
        Customer_Activation_Test__c[] lineItems = [SELECT Id, Name,Service_Name__c,Customer_contact_details__c, Scope_of_work__c,
                                        Time_zone_for_appointment__c, Request_Summary__c,Activation_appointment_Date__c,CAT_InFlight_Update__c,
                                        Start_time_for_activation_activity__c, End_time_for_activation_activity__c, Order__c,Type_of_Update__c, 
                                        Account__c,SN_request_item_sysid__c,Snow_Request_ID__c, Service_Activation_Queue__c,Customer_Name__c,Parent_Product__c,
                                        Estimated_Start_Date__c,order__r.Customer_Turn_Up_Test_Date__c,Region__c,Product_Configuration_Type__c,START_Time_Tibco__c,End_Time_Tibco__c,
                                        order__r.GCC_Reseller_Account_ID__c FROM Customer_Activation_Test__c where Order__c = :OrderId ORDER BY Name];
        
        system.debug('---lineItems ---'+lineItems);
                
        TibcoOrderLine[] lineList = new TibcoOrderLine[350];
        TibcoOrderLine[] actionList = new TibcoOrderLine[350];  
        
        for (Customer_Activation_Test__c lineItem : lineItems) {
            
            TibcoOrderLine orderLine = new TibcoOrderLine();
            orderLine.lineNumber = string.valueof(lineNumber);
            orderLine.action = 'PROVIDE';

             //orderLine.productID = lineItem.Parent_Product__c;  
             orderLine.productID = 'CAT'; 
             udfList = new List<Udf>();
             Udf udfElement1 = new Udf('ID',lineitem.id);
             udfList.add(udfElement1);
             
                       
       
             udf udfElement2 = new Udf('CAT_InFlight_Update__c', lineItem.CAT_InFlight_Update__c);
             udfList.add(udfElement2);
             
             /*Udf udfElement13 = new Udf('Service_Name__c',lineItem.Name);
             udfList.add(udfElement13);
             
             Udf udfElement3 = new Udf('TimezoneAppointment',lineitem.Time_zone_for_appointment__c);
             udfList.add(udfElement3);   
             */ 
                       
             
             Udf udfElement13 = new Udf('Service_Name__c',lineItem.Name);
             udfList.add(udfElement13);
             
             Udf udfElement4 = new Udf('Start_time_for_activation_activity__c', string.valueOfGmt(lineitem.START_Time_Tibco__c));
             udfList.add(udfElement4); 
             
             Udf udfElement5 = new Udf('Customer_contact_details__c',lineitem.Customer_contact_details__c);
             udfList.add(udfElement5);
             
                       
       
            /* Udf udfElement6 = new Udf('Service_Activation_Queue',lineitem.Service_Activation_Queue__c);
             udfList.add(udfElement6); */
       
                       
             
             Udf udfElement7 = new Udf('Estimated_Start_Date__c', string.valueOfGmt(lineitem.Estimated_Start_Date__c));
             udfList.add(udfElement7);
            
                       
       
                                     
                                                                   
                    
                       
       
        
             
             Udf udfElement9 = new Udf('Order__c',lineItem.Order__c);
             udfList.add(udfElement9); 
       
                      
       
                                    
                     

                      
             /* Udf udfElement10 = new Udf('Region__c',lineItem.Region__c);
             udfList.add(udfElement10); 
              */
                      
             
            /* Udf udfElement11 = new Udf('OrderType',lineItem.Product_Configuration_Type__c);
             udfList.add(udfElement11);  */
                          
             Udf udfElement12 = new Udf('Parent_Product__c',lineItem.Parent_Product__c);
             udfList.add(udfElement12); 
             
             Udf udfElement16 = new Udf('End_time_for_activation_activity__c', string.valueOfGmt(lineitem.End_Time_Tibco__c));
             udfList.add(udfElement16);
             
            /* Udf udfElement14 = new Udf('Product_Configuration_Type__c ',lineItem.Product_Configuration_Type__c);
             udfList.add(udfElement14); */
             
             Udf udfElement15 = new Udf('SN_request_item_sysid__c',lineItem.SN_request_item_sysid__c);
             udfList.add(udfElement15);
             
             Udf udfElement17 = new Udf('UpdateType',lineItem.Type_of_Update__c);
                                                                                        
             udfList.add(udfElement17);
             
             Udf udfElement18 = new Udf('Request_Summary__c',lineItem.Request_Summary__c.replaceAll('\n','~'));
             udfList.add(udfElement18);
             
             Udf udfElement19 = new Udf('Scope_of_work__c',lineItem.Scope_of_work__c.replaceAll('\n','~'));
             udfList.add(udfElement19);

             system.debug('--udfListElements--'+udfList);
              
             udfList = getCatUdfValue(lineItem.ID, udfList);
             System.debug('pawan check udfList '+udfList );
             orderLine.udf=udfList;
             
             
             lineNumber += 1;
             lineList.add(orderLine);
             system.debug('---lineListOrderLine---'+lineList);
      
         }        
            system.debug('---serviceOrderlines-----'+serviceOrder.lines);

                    
    
                                   
    
                
            return lineList;            
        
      } 
            
    @testvisible
    private List<Udf> getCatUdfValue(String CatserviceID, List<Udf> udfList){
    string udfQuery  = 'SELECT Id, Name, Customer_contact_details__c,Service_Name__c,START_Time_Tibco__c,End_Time_Tibco__c,Type_of_Update__c,Parent_Product__c,Region__c,Product_Configuration_Type__c,Scope_of_work__c,SN_request_item_sysid__c,Time_zone_for_appointment__c, Request_Summary__c,Activation_appointment_Date__c,Start_time_for_activation_activity__c, End_time_for_activation_activity__c, Order__c, Account__c, Snow_Request_ID__c, Service_Activation_Queue__c,Customer_Name__c,Estimated_Start_Date__c,order__r.Customer_Turn_Up_Test_Date__c,order__r.GCC_Reseller_Account_ID__c FROM Customer_Activation_Test__c where id =:CatserviceID';        
    List<Customer_Activation_Test__c> CATudfItems = Database.query(udfQuery);
    system.debug('--CATudfItems--'+CATudfItems);
        
    for(CATService__c CatServiceVal : CATService__c.getAll().values()){
      if(String.valueof(CATudfItems[0].get(CatServiceVal.Cat_Service__c))!=null)
       {
          Udf udfElement1 = new Udf(CatServiceVal.Name,String.valueof(CATudfItems[0].get(CatServiceVal.Cat_Service__c)));
          system.debug('---CustomSettings---'+udfElement1);
          udfList.add(udfElement1);
          system.debug('---udfListcustom--'+udfList);
       }
      }
       return udfList;        
    }
    
}