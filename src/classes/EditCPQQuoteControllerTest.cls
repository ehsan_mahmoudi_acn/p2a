@IsTest
private class EditCPQQuoteControllerTest{
               
        static testMethod void testEditCPQQuoteControllerLostOpp() {
                
                Opportunity opp = UnitTestOpportunityHelper.getValidDatesOpportunity();
                //updated stagename closed lost as part of SOMP
                opp.Sales_Status__c='won';
               
                opp.Win_Loss_Reasons__c='Withdrawn';
                //update opp;
             
              //opp.mh_Associated_Blue_Sheet__c = true;
              /*Getting the record type id for simple type
                modified by Ramya (SFDC Bangalore)
              */
              
               Quote__c q =  UnitTestOpportunityHelper.getOppQuote(opp);
               system.assert(opp!=null);
               test.starttest();
               
               PageReference pageRef = new PageReference('/apex/CPQQuoteEdit?id='+q.id);
               Test.setCurrentPage(pageRef); 
              
               ApexPages.StandardController sc = new ApexPages.StandardController(q);
               EditCPQQuoteController controller = new EditCPQQuoteController(sc);
               
               ApexPages.currentPage().getParameters().put('id', q.id);
               // Put the test Account SF Id on the parameter list
               //System.currentPageReference().getParameters().put('id', q.id);
               controller.setEditQuote(True);
              // boolean getEditQuote = true;
         //   Boolean editquote =  controller.getEditQuote();                     
            // System.assertEquals(false,editQuote);
             //controller.getEditQuote();
                Test.stopTest();
        }

       static testMethod void testEditCPQQuoteControllerWonOpp() {
        
               try{
               Opportunity opp1 = UnitTestOpportunityHelper.getValidDatesOpportunity();
               opp1.BillProfileComplete__c=true;
               opp1.Order_Admin_Approved__c=true;
               opp1.StageName='Identify & Define';
               opp1.Stage__c='Identify & Define';
               opp1.Approved_Date__c = system.today();
               
               opp1.Signed_Contract_Order_form_Recieved_Date__c = system.today();
               opp1.Upload_signed_order_Contract__c = true;
               //opp1.Signed_Contract_Upload_Date__c = system.today();
               
               opp1.Win_Loss_Reasons__c='Product';
               opp1.QuoteStatus__c = 'Order';
               opp1.SOF_signed_by_customer__c = false;
               Test.startTest();
               //update opp1;
              
               Quote__c q1 =  UnitTestOpportunityHelper.getOppQuote(opp1);
               system.assert(opp1!=null);
               //update q1;
              
               PageReference pageRef1 = new PageReference('/apex/CPQQuoteEdit?id='+q1.id); 
               Test.setCurrentPage(pageRef1);
               ApexPages.StandardController sc1 = new ApexPages.StandardController(q1);
               EditCPQQuoteController controller1 = new EditCPQQuoteController(sc1);
               Boolean editQuoteflag1 = controller1.getEditQuote();  
           
               //System.assertEquals(false,editQuoteflag1);
                Test.StopTest();
                
               }catch(Exception e){}
               
        }
        
       /*  static testMethod void testEditCPQQuoteControllerApprovalSubmission() {
        
               Opportunity opp2 = UnitTestOpportunityHelper.getValidDatesOpportunity();
               update opp2;
               Quote__c q2 =  UnitTestOpportunityHelper.getOppQuote(opp2);
               update q2;
               
               Approval.ProcessSubmitRequest req2 = new Approval.ProcessSubmitRequest();
               req2.setComments('Submitting request for approval.');
               req2.setObjectId(opp2.Id);
               Approval.ProcessResult result = Approval.process(req2);
               PageReference pageRef2 = new PageReference('/apex/CPQQuoteEdit?id='+q2.id); 
               Test.setCurrentPage(pageRef2);
               ApexPages.StandardController sc2 = new ApexPages.StandardController(q2);
               EditCPQQuoteController controller2 = new EditCPQQuoteController(sc2);
               Boolean editQuoteflag2 = controller2.getEditQuote();     
               System.assertEquals(false,editQuoteflag2);
               
        }*/
        
}