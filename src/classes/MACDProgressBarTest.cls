@isTest(SeeAllData = false)
public class MACDProgressBarTest{   
       
    private static testMethod void mACDProgresTest(){  
        P2A_TestFactoryCls.sampleTestData();         
        List<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1); 
        List<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
        system.assertEquals(true,SUBList!=null); 
        Test.StartTest();
        Map<Id, csord__Service__c> serviceList = serviceMap();
        CloneSubComponentsOfProductConfig bc = new CloneSubComponentsOfProductConfig(SUBList[0].Id, SUBList[0].Id, serviceList);
        Id batchProcessId = Database.executeBatch(bc, 1);
        SUBList[0].MACD_Batch_Job_Id__c = batchProcessId;
        SUBList[0].MACD_Basket_Id__c = Products[0].Id;
        upsert SUBList;
        system.assertEquals(true,SUBList!=null); 
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(SUBList[0]);

        MACDProgressBar mcd = new MACDProgressBar(sc);
        mcd.refreshJobStatus();
        mcd.isFinished='Completed';
        Test.StopTest();
        system.assertEquals(true,mcd!=null); 
   }
   
    private static Map<Id, csord__Service__c> serviceMap(){
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> order = P2A_TestFactoryCls.getorder(1, OrdReqList);

        Map<Id, csord__Service__c> serviceList = new Map<Id, csord__Service__c>();
        List<csord__Service__c> insertService = new List<csord__Service__c>();
        for(integer i=0;i<3;i++){           
            csord__Service__c s = new csord__Service__c();
            s.Name = 'Test Service'; 
            s.csord__Identification__c = 'Test-Catlyne-4238362';
            s.csord__Order_Request__c = OrdReqList[0].Id;
            s.csord__Subscription__c = SUBList[0].Id;
            s.csord__Order__c = order[0].Id;
            s.Billing_Commencement_Date__c = System.Today();
            s.Stop_Billing_Date__c = System.Today();
            s.RAG_Status_Red__c = false ; 
            s.RAG_Reason_Code__c = '';          
            s.Bundle_Flag__c = false;
            s.Product_Id__c = 'ATM';
            s.Product_Code__c = 'ATM';
            s.Inventory_Status__c = Label.PROVISIONED;
            s.Cease_Service_Flag__c = true;          
            insertService.add(s);
        }
        insert insertService;
        system.assertEquals(true,insertService!=null); 
        for(csord__Service__c service :insertService){
            serviceList.put(service.Id, service);           
        }
        return serviceList;
    }
}