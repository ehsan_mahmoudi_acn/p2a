/**
    @author - Accenture
    @date - 18- July-2012
    @version - 1.0
    @description - This is the test class for UpdateContractDocuments Trigger.
*/
@isTest
private class UpdateContractDocumentsTest { 
 /*  public static Account acc;

    static testMethod void myUnitTest() {
        acc=new Account();
        acc=getAccount();
        BillProfile__c billProfileObj = getBillProfileDetails();
       // billProfileObj.Activated__c = True;
        billProfileObj.Payment_Terms__c = '15 days';
        billProfileObj.Billing_Frequency__c = 'Quaterly';
        update billProfileObj;
        Action_Item__c actionItemObj = new Action_Item__c();
        actionItemObj.Bill_Profile__c = billProfileObj.Id;
        insert actionItemObj;
        
    }
    
    private static BillProfile__c getBillProfileDetails() {
        
        BillProfile__c billProfileObj = new BillProfile__c();
       // Account acc = getAccount();
        billProfileObj.Name = 'testbill';
        
        billProfileObj.Account__c = acc.Id;
        billProfileObj.Activated__c = True;
        Site__c siteObj = getSite();
        billProfileObj.Bill_Profile_Site__c = siteObj.Id;
        billProfileObj.Billing_Entity__c = 'Telstra Corporation';
        //billProfileObj.Currency = GBP-British;
        billProfileObj.Payment_Terms__c = '30 Days';
        billProfileObj.Billing_Frequency__c = 'Monthly';
        billProfileObj.Account__c = siteObj.AccountId__c;
        billProfileObj.Invoice_Breakdown__c = 'Summary Page';
        billProfileObj.First_Period_Date__c = date.today();
        billProfileObj.Start_Date__c = date.today();
       

        insert billProfileObj;
        return billProfileObj;
    }
    
   private static Account getAccount() {
        Account a = new Account();
        Country_Lookup__c co = getCountry();
        a.Name = 'UnitTest_001';
        a.BillingCountry = 'GB';
        a.Country__c = co.Id;
        insert a;
        return a;
    }    

    private static Contact getContact() {
        Contact c = new Contact();
       // Account acc = getAccount();
        c.FirstName = 'Unit';
        c.LastName = 'Test1';
        c.AccountId = acc.Id;
        
        insert c;
        return c;
    }
    
    private static Site__c getSite() { 
        
        Site__c s = new Site__c();
       // Account a = getAccount();
        Country_Lookup__c co = getCountry();
        City_Lookup__c ci = getCity();
        s.Name = 'UnitTestSite1';
        s.AccountId__c = acc.Id;
        s.Address1__c = '44';
        s.Address2__c = 'Lochart';
        s.Country_Finder__c = co.Id;
        s.City_Finder__c = ci.Id;
        s.Address_Type__c='Billing Address';
        insert s;
        
        return s;
    }
    
     private static Country_Lookup__c getCountry(){
        Country_Lookup__c cl = new Country_Lookup__c();
        cl.CCMS_Country_Name__c = 'HONG KONG';
        cl.Country_Code__c = 'HK';
        insert cl;
        return cl;
    } 
    private static City_Lookup__c getCity(){
        
        City_Lookup__c ci = new City_Lookup__c();
        ci.City_Code__c = 'ORI';
        ci.Name = 'Orissa';
        if(ci.id==null)
        insert ci;
        return ci;
    }
    */
}