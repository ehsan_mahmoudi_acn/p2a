public class OpportunityEditOverride {
       String recordId;
       public String deviceType{get;set;}
    public  OpportunityEditOverride(ApexPages.StandardController controller) {
        recordId = controller.getId();
        deviceType=getDeviceType();
        system.debug('=====recordId===='+recordId );
    }
    public PageReference redirect() {
        //added Sales_Status__c to the query as part of somp
    Opportunity opp = [Select id, StageName,Stage__c, Sales_Status__c from Opportunity where Id=:recordId limit 1];
    String retURL =  ApexPages.currentPage().getParameters().get('retURL');
    String usrProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name; 
    String userAgent=  System.currentPageReference().getHeaders().get('User-Agent');
    // Need to detect the Device the request comes from.
    System.debug('The User Agent is'+userAgent);
    if (opp!=null){
        if (!(Opp.Stage__c=='Prove & Close' && Opp.Sales_Status__c=='Won') || usrProfileName=='System Administrator' || usrProfileName=='TI Marketing'){
            // Redirect to the Standard Edit.
            PageReference newPage1 = new PageReference('/' + recordId + '/e');
            newPage1.setRedirect(true);
            newPage1.getParameters().put('nooverride', '1');
            newPage1.getParameters().put('retURL',retURL);
            return newpage1;
        }
    }
     return null;   
    }
     @RemoteAction
     public Static String getRedirectionForMobile(String newRdId)
     {
         String retVal='ERROR';
         //added Sales_Status__c to the query as part of SOMP
          Opportunity opp = [Select id,Stage__c, StageName,Sales_Status__c from Opportunity where Id=:newRdId limit 1];
          String usrProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name;        
          if (opp!=null)
          {
              if (!(Opp.Sales_Status__c=='Won') || usrProfileName=='System Administrator' || usrProfileName=='TI Marketing' )
              {
                  retVal='Won';    
              }
          }
          return retVal;
     }
              public static String getDeviceType(){
        String userAgent=System.currentPageReference().getHeaders().get('User-Agent');
        System.debug('Uger-Agent is : '+userAgent);
        if(Pattern.matches('.*(iPhone|(Android.*Mobile Safari)|BlackBerry).*',userAgent)){
            return 'mobile';
        }else if(Pattern.matches('.*iPad.*',userAgent)){
            return 'tablet';
        }else
            return 'default';
    }
       
}