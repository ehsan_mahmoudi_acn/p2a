/**
    @author - Accenture
    @date - 17- Jul-2012
    @version - 1.0
    @description - This is the test class for OrderSubmitValidator class
*/


@isTest
private class OrderSubmitValidatorTest {
/*    
    static Account acc;
    static Opportunity opp;
    static  Country_Lookup__c country;
    static OpportunityLineItem oli;
    
    static testMethod void testSubmitOrdr() {
        Order__c ordr = getOrder();
        Product2 prod1 =getProduct('Prod1', 'M');
        Product2 prod2 =getProduct('Prod2','SE');
        Product2 prod3 =getProduct('Prod3','ME');
        Site__c site1 =getSite('Site1','City1','ABC');
        Site__c site2 =getSite('Site2','City2','AAA');
        Site__c site3 =getSite('Site3','OTHER','OTHER');
        Order_Line_Item__c oli1 = new Order_Line_Item__c(ParentOrder__c=ordr.id, Product__c=prod1.id, Opportunity_Line_Item_ID__c= '1111',Is_GCPE_shared_with_multiple_services__c = 'No');
       // insert oli1;
        Product_Configuration__c pc1 = new Product_Configuration__c ( Order_Line_Item__c=oli1.id, Site__c=site1.id);
      //  insert pc1;
        Order_Line_Item__c oli2 = new Order_Line_Item__c(ParentOrder__c=ordr.id, Product__c=prod2.id, Opportunity_Line_Item_ID__c= '2222' ,Is_GCPE_shared_with_multiple_services__c = 'No');
       // insert oli2;
        Product_Configuration__c pc2 = new Product_Configuration__c ( Order_Line_Item__c=oli2.id, Site_B__c=site2.id);
      //  insert pc2;
        Order_Line_Item__c oli3 = new Order_Line_Item__c(ParentOrder__c=ordr.id, Product__c=prod3.id, Opportunity_Line_Item_ID__c= '3333' ,Is_GCPE_shared_with_multiple_services__c = 'No');
      //  insert oli3;
        Product_Configuration__c pc3 = new Product_Configuration__c ( Order_Line_Item__c=oli3.id, Site__c=site3.id, Site_B__c=site2.id);
     //   insert pc3;
        Order_Line_Item__c oli5 = new Order_Line_Item__c(ParentOrder__c=ordr.id, Product__c=prod3.id, Opportunity_Line_Item_ID__c= '4444' ,Is_GCPE_shared_with_multiple_services__c = 'No');
     //   insert oli5;
        Product_Configuration__c pc5 = new Product_Configuration__c ( Order_Line_Item__c=oli5.id, POP_A_Code__c='11234', POP_Z_Code__c='11234');
    //    insert pc5;
        
        
        Order__c ordr1 = getOrder();
        ordr1.Is_Order_Submitted__c = true;
        update ordr1;
        Order_Line_Item__c oli4 = new Order_Line_Item__c(ParentOrder__c=ordr1.id, Product__c=prod3.id, Opportunity_Line_Item_ID__c= '4444',Is_GCPE_shared_with_multiple_services__c = 'No' );
      //  insert oli4;
        Product_Configuration__c pc4 = new Product_Configuration__c ( Order_Line_Item__c=oli4.id, Site__c=site1.id, Site_B__c=site2.id);
      //  insert pc4;
        
        
        PageReference pageRef = Page.OrderSubmitValidationPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', ordr.id);
        OrderSubmitValidator controller = new OrderSubmitValidator();
        controller.validateOrder();
        controller.orderId = ordr1.id;
        controller.validateOrder();
    }
    private static Order__c getOrder(){
            Order__c ordr = new Order__c();
            opp = getOpportunity();
            ordr.Opportunity__c = opp.Id;
            ordr.Is_Order_Submitted__c = false;
            ordr.Clean_Order_Date__c = System.today();
            ordr.SRF_document_links__c = 'abc';
            
            insert ordr;
        return ordr;
    }
    
    private static OpportunityLineItem getOpportunityLineItem(){
        if(oli == null){
            oli = new OpportunityLineItem();
            opp = getOpportunity();
            BillProfile__c b = getBillProfile();
            oli.OpportunityId = opp.Id;
            oli.IsMainItem__c = 'Yes';
            oli.OrderType__c = 'New Provide';
            oli.Quantity = 5;
            oli.UnitPrice = 10;
            oli.PricebookEntryId = getPriceBookEntry('Global IPVPN').Id;
            insert oli;
        }    
        return oli;
    }
    private static Opportunity getOpportunity(){
        if(opp == null) {
            opp = new Opportunity();
            acc = getAccount();
            opp.Name = 'Test Opportunity';
            opp.AccountId = acc.Id;
             //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
            opp.StageName = 'Identify & Define';
            opp.Stage__c='Identify & Define';
            opp.CloseDate = System.today();
            opp.Estimated_MRC__c=800;
            opp.Estimated_NRC__c=1000;
            opp.ContractTerm__c='10';
            insert opp;
        }
        return opp;
    }
    private static Pricebook2 getPriceBook(String prodName){
        Pricebook2 p = [SELECT Id FROM Pricebook2 LIMIT 1];
        return p;   
    }
    
    private static Product2 getProduct(String prodName,String serTyp){
        Product2 prod = new Product2();
        prod.Name = prodName;
        prod.ProductCode = prodName;
        prod.Product_ID__c = prodName;
        prod.Create_Service__c =serTyp;
        prod.Create_Path__c =TRUE;
        insert prod;
        return prod;
    }
    
    private static PricebookEntry getPriceBookEntry(String prodName){
        PricebookEntry p = new PricebookEntry();
        p.Pricebook2Id = getPriceBook(prodName).Id;
        p.Product2Id =  getProduct(prodName,'M').Id;
        p.UnitPrice = 2000;
        p.IsActive = true;
        insert p; 
        return p;    
    }
    
    
    
    private static BillProfile__c getBillProfile(){
        BillProfile__c b = new BillProfile__c();
        acc = getAccount();
        b.name = 'Test Bill Profile';
        b.Billing_Entity__c = 'Telstra Limited';
        b.Account__c = acc.Id;
        insert b;
        return b;
    }
    private static Account getAccount(){
        if(acc == null) {
            acc = new Account();
            Country_Lookup__c c = getCountry();
            acc.Name = 'Test Account';
            acc.Customer_Type__c = 'MNC';
            acc.Country__c = c.Id;
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Activated__c = True;
            acc.Account_ID__c = '9090';
            acc.Customer_Legal_Entity_Name__c='Test';
            insert acc;
        }
        return acc;
    }
    
    private static Country_Lookup__c getCountry(){
        if(country == null) {
            country = new Country_Lookup__c();
            country.CCMS_Country_Code__c = 'HK';
            country.CCMS_Country_Name__c = 'India';
            country.Country_Code__c = 'HK';
            insert country;
        }
        return country;
    }
     private static City_Lookup__c getCity( String cityname, String citycod){
        City_Lookup__c city = new City_Lookup__c(Name=cityname, City_Code__c=citycod, Generic_Site_Code__c = citycod);
        insert city;
        return city;
     }
     private static Site__c getSite(String sitename, String Cityname, String citycode){
        City_Lookup__c city = getCity(Cityname, citycode);
        Country_Lookup__c con = getCountry();
        Site__c site = new Site__c(name = sitename, AccountId__c =acc.id, City_Finder__c=city.id, Address1__c='Address1__c', Country_Finder__c=con.id);
        if(citycode == 'OTHER')
            site.Other_City__c = 'abcd';
        insert site;
        return site;
     }
    */
}