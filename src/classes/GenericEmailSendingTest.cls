@isTest(SeeAllData=false)
public class GenericEmailSendingTest {
    
    static testmethod void emailaddresstest1(){
        
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId()); 
        
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Country_Lookup__c> countryList =  P2A_TestFactoryCls.getcountry(1); 
        List<Contact> contList = P2A_TestFactoryCls.getContact(1, accList);
        List<Site__c> siteList = P2A_TestFactoryCls.getsites(1, accList, countryList);
        //List<BillProfile__c>  billProfList = P2A_TestFactoryCls.getBPs(1, accList ,siteList,contList);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        servlist[0].AccountId__c = acclist[0].Id;
        update servlist;
        system.assert(servlist!=null);
        List<csord__Order__c> ordList = P2A_TestFactoryCls.getorder(1,ordReqList);
        List<csord__Order_Line_Item__c> OrderLineItemlist = P2A_TestFactoryCls.getOrderlineItem(1,ordList,ordReqList);
        
        /*List<BillProfile__c> BillProfList = new List<BillProfile__c>();
        BillProfile__c BProfObj = new BillProfile__c();
        //BProfObj.OwnerID = u2.Id;
        BProfObj.Account__c = acclist[0].Id;
        BProfObj.Billing_Entity__c = 'Austrila';
        BProfObj.Payment_Terms__c = '45 Days';
        BProfObj.Billing_Frequency__c = 'Hong Kong Annual Voice';
        BProfObj.Billing_Contact__c = contList[0].Id;
        BProfObj.Activated__c = true;
        BprofObj.Bill_Profile_Site__c= siteList[0].Id;
        BillProfList.add(BProfObj);
        insert BillProfList; */
        
        BillProfile__c bp = new BillProfile__c();
        bp.name = 'Test bill profile';
        bp.Activated__c = True;
        bp.Account__c = acclist[0].Id; 
        bp.Status__c = 'Active';
        bp.Approved__c = true;
        bp.Bill_Profile_Site__c = siteList[0].Id; 
        bp.Billing_Entity__c = 'Australia';
        insert bp;
        
        csord__Service__c ser = new csord__Service__c();
        ser.name = 'Test service';
        ser.AccountId__c = acclist[0].Id;
        ser.Bill_ProfileId__c = bp.Id;
        ser.AccountId__c = acclist[0].Id;
        ser.csord__Subscription__c = SUBList[0].Id;  
        ser.csord__Identification__c = 'Test identification';
        ser.csord__Order_Request__c = OrdReqList[0].Id;
        insert ser;  
        
        
        PACNET_Entities__c pacnet = new PACNET_Entities__c();
        pacnet.name = 'Entity1';
        pacnet.PACNET_Entity__c = 'Asia Netcom Pacnet (Ireland) Limited';
        insert pacnet;
        
        TELSTRA_Entities__c TE = new TELSTRA_Entities__c();
        TE.name = 'AustraliaRegion';
        TE.TELSTRA_Entity__c ='Telstra Corporation Limited,Telstra Network Services NZ Limited';
        insert TE;
        
        TELSTRA_Entities__c TE1 = new TELSTRA_Entities__c();
        TE1.name = 'EMEARegion';
        TE1.TELSTRA_Entity__c ='Telstra Limited';
        insert TE1;
        
        TELSTRA_Entities__c TE2 = new TELSTRA_Entities__c();
        TE2.name = 'NorthAsiaRegion';
        TE2.TELSTRA_Entity__c ='Beijing Australia Telecommunications Technical Consulting Services Company Limited,Telstra International (AUS) Limited - Taiwan Branch,Telstra International Limited,Telstra Japan K.K.,Telstra Services Korea Limited';
        insert TE2;
        
        TELSTRA_Entities__c TE3 = new TELSTRA_Entities__c();
        TE3.name = 'NorthAsiaRegion1';
        TE3.TELSTRA_Entity__c ='Telstra Services Asia Pacific (HK) Limited,Telstra Cable (HK) Limited,Telstra Global (Singapore) Pte. Ltd.,Telstra Services (USA) Inc.,Telstra Services (Taiwan) Inc.,Telstra Global (HK) Limited';
        insert TE3;
        
        TELSTRA_Entities__c TE4 = new TELSTRA_Entities__c();
        TE4.name = 'SouthAsiaRegion';
        TE4.TELSTRA_Entity__c ='PT. Reach Network Services Indonesia,Reach Network India Private Limited,Telstra ( Thailand ) Co., Limited,Telstra Malaysia Sdn. Bhd.,Telstra Philippines Inc,Telstra Singapore Pte Limited,Telstra Telecommunications Private Limited';
        insert TE4;
        
        TELSTRA_Entities__c TE5 = new TELSTRA_Entities__c();
        TE5.name = 'USRegion';
        TE5.TELSTRA_Entity__c ='Telstra Incorporated';
        insert TE5;
        
        TELSTRA_Entities__c TE6 = new TELSTRA_Entities__c();
        TE6.name = 'SouthAsiaRegion1';
        TE6.TELSTRA_Entity__c ='Telstra Network Services NZ Limited';
        insert TE6;     
        
        List<Order__c> order = new List<Order__c>();
        order.add(new Order__c(Status__c='test'));
        insert order;  
        
        List<Order__c> order1 = new List<Order__c>();
        order1.add(new Order__c(Status__c='test' , Account__c = acclist[0].Id));
        insert order1;
        
        Product2 prod1 = new Product2(name='Test Product', Product_ID__C ='TPD',CurrencyIsoCode='EUR');
        insert prod1;
        
        list<Order_Line_Item__c> oli1= new list<Order_Line_Item__c>();
        Order_Line_Item__c oli = new Order_Line_Item__c(ParentOrder__c = order[0].Id,CPQItem__c = '1', OrderType__c = 'New Provide', Product__c = prod1.Id,
                                                        Opportunity_Line_Item_ID__c ='111', Is_GCPE_shared_with_multiple_services__c='No');
        oli1.add(oli);
        insert oli1;
        system.assert(oli1!=null);
        List<QueueSobject> queueSObjList = QueueUtil.getQueuesListForObject(BillProfile__c.sobjecttype);
        
        
        Map<String ,TELSTRA_Entities__c> tmap = new Map<String ,TELSTRA_Entities__c>();
        tmap.put('AustraliaRegion' ,Te); 
        
        
        List<String> productNameList = new List<String>();
        productNameList.add('GCPE');
        
        GenericEmailSending Gmail = new GenericEmailSending();
        Gmail.getRegionBasedOnBillingEntity(pacnet.PACNET_Entity__c);
        Gmail.getRegionBasedOnBillingEntity(TE.TELSTRA_Entity__c);
        Gmail.getRegionBasedOnBillingEntity(TE1.TELSTRA_Entity__c); 
        Gmail.getRegionBasedOnBillingEntity(TE2.TELSTRA_Entity__c); 
        Gmail.getRegionBasedOnBillingEntity(TE3.TELSTRA_Entity__c); 
        Gmail.getRegionBasedOnBillingEntity(TE4.TELSTRA_Entity__c);  
        Gmail.getRegionBasedOnBillingEntity(TE5.TELSTRA_Entity__c);  
        Gmail.sendEmailtoBillingTeamonLiveisDesign(servList[0],productNameList);
        Gmail.sendEmailtoBillingTeamonLiveisDesign(ser ,productNameList);
        Gmail.getRegionBasedOnBillingEntity(ser.Telstra_Billing_Entity__c);  
        Gmail.getRequiredEmailAddresses(order);
        Gmail.getRequiredEmailAddresses(order1);
        //Gmail.sendEmailtoBillingTeam(servList[0],productNameList);
        Gmail.getEmailAddresses('EMEA');
        Gmail.getEmailAddresses('North Asia');
        Gmail.getEmailAddresses('PACNET');
        gmail.sendEmailForBlockingBilling('test@salesforce.com', 'Testsubject', 'Testbody');
        gmail.getProductNamesOfGIAAS(oli1);
        gmail.mail('Test123456@salesforce.com', 'Testsubject', 'Testbody');
        P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
    }
    
    static testmethod void emailaddresstest2(){
        
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId()); 
        
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        accList[0].Selling_Entity__c = 'Telstra Limited';
        update accList;
        System.debug('accList[0].Selling_Entity__c: ' + accList[0].Selling_Entity__c);
        List<Country_Lookup__c> countryList =  P2A_TestFactoryCls.getcountry(1); 
        List<Contact> contList = P2A_TestFactoryCls.getContact(1, accList);
        List<Site__c> siteList = P2A_TestFactoryCls.getsites(1, accList, countryList);
        //List<BillProfile__c>  billProfList = P2A_TestFactoryCls.getBPs(1, accList ,siteList,contList);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        servlist[0].AccountId__c = acclist[0].Id;
        update servlist;
        system.assert(servlist!=null);
        System.debug('servlist[0].Telstra_Billing_Entity__c: ' + servlist[0].Telstra_Billing_Entity__c);
        List<csord__Order__c> ordList = P2A_TestFactoryCls.getorder(1,ordReqList);
        List<csord__Order_Line_Item__c> OrderLineItemlist = P2A_TestFactoryCls.getOrderlineItem(1,ordList,ordReqList);
        
        BillProfile__c bp = new BillProfile__c();
        bp.name = 'Test bill profile';
        bp.Activated__c = True;
        bp.Account__c = acclist[0].Id; 
        bp.Status__c = 'Active';
        bp.Approved__c = true;
        bp.Bill_Profile_Site__c = siteList[0].Id; 
        bp.Billing_Entity__c = 'Telstra Limited';
        insert bp;
        system.assert(bp!=null);
        csord__Service__c ser = new csord__Service__c();
        ser.name = 'Test service';
        ser.AccountId__c = acclist[0].Id;
        ser.Bill_ProfileId__c = bp.Id;
        ser.AccountId__c = acclist[0].Id;
        ser.csord__Subscription__c = SUBList[0].Id;  
        ser.csord__Identification__c = 'Test identification';
        ser.csord__Order_Request__c = OrdReqList[0].Id;
        insert ser; 
        System.debug('ser.Telstra_Billing_Entity__c: ' + ser.Telstra_Billing_Entity__c); 
        
        
        PACNET_Entities__c pacnet = new PACNET_Entities__c();
        pacnet.name = 'Entity1';
        pacnet.PACNET_Entity__c = 'Asia Netcom Pacnet (Ireland) Limited';
        insert pacnet;
        
        TELSTRA_Entities__c TE = new TELSTRA_Entities__c();
        TE.name = 'AustraliaRegion';
        TE.TELSTRA_Entity__c ='Telstra Corporation Limited,Telstra Network Services NZ Limited';
        insert TE;
        
        TELSTRA_Entities__c TE1 = new TELSTRA_Entities__c();
        TE1.name = 'EMEARegion';
        TE1.TELSTRA_Entity__c ='Telstra Limited';
        insert TE1;
        
        TELSTRA_Entities__c TE2 = new TELSTRA_Entities__c();
        TE2.name = 'NorthAsiaRegion';
        TE2.TELSTRA_Entity__c ='Beijing Australia Telecommunications Technical Consulting Services Company Limited,Telstra International (AUS) Limited - Taiwan Branch,Telstra International Limited,Telstra Japan K.K.,Telstra Services Korea Limited';
        insert TE2;
        
        TELSTRA_Entities__c TE3 = new TELSTRA_Entities__c();
        TE3.name = 'NorthAsiaRegion1';
        TE3.TELSTRA_Entity__c ='Telstra Services Asia Pacific (HK) Limited,Telstra Cable (HK) Limited,Telstra Global (Singapore) Pte. Ltd.,Telstra Services (USA) Inc.,Telstra Services (Taiwan) Inc.,Telstra Global (HK) Limited';
        insert TE3;
        
        TELSTRA_Entities__c TE4 = new TELSTRA_Entities__c();
        TE4.name = 'SouthAsiaRegion';
        TE4.TELSTRA_Entity__c ='PT. Reach Network Services Indonesia,Reach Network India Private Limited,Telstra ( Thailand ) Co., Limited,Telstra Malaysia Sdn. Bhd.,Telstra Philippines Inc,Telstra Singapore Pte Limited,Telstra Telecommunications Private Limited';
        insert TE4;
        
        TELSTRA_Entities__c TE5 = new TELSTRA_Entities__c();
        TE5.name = 'USRegion';
        TE5.TELSTRA_Entity__c ='Telstra Incorporated';
        insert TE5;

        TELSTRA_Entities__c TE6 = new TELSTRA_Entities__c();
        TE6.name = 'SouthAsiaRegion1';
        TE6.TELSTRA_Entity__c ='Telstra Network Services NZ Limited';
        insert TE6;             
        
        List<Order__c> order = new List<Order__c>();
        order.add(new Order__c(Status__c='test'));
        insert order;  
        
        List<Order__c> order1 = new List<Order__c>();
        order1.add(new Order__c(Status__c='test' , Account__c = acclist[0].Id));
        insert order1;
        
        Product2 prod1 = new Product2(name='Test Product', Product_ID__C ='TPD',CurrencyIsoCode='EUR');
        insert prod1;
        
        list<Order_Line_Item__c> oli1= new list<Order_Line_Item__c>();
        Order_Line_Item__c oli = new Order_Line_Item__c(ParentOrder__c = order[0].Id,CPQItem__c = '1', OrderType__c = 'New Provide', Product__c = prod1.Id,
                                                        Opportunity_Line_Item_ID__c ='111', Is_GCPE_shared_with_multiple_services__c='No');
        oli1.add(oli);
        insert oli1;
        
        List<QueueSobject> queueSObjList = QueueUtil.getQueuesListForObject(BillProfile__c.sobjecttype);
        
        
        Map<String ,TELSTRA_Entities__c> tmap = new Map<String ,TELSTRA_Entities__c>();
        tmap.put('AustraliaRegion' ,Te); 
        
        
        List<String> productNameList = new List<String>();
        productNameList.add('GCPE');
        
        
        GenericEmailSending Gmail = new GenericEmailSending();
        Gmail.getRegionBasedOnBillingEntity(pacnet.PACNET_Entity__c);
        Gmail.getRegionBasedOnBillingEntity(TE.TELSTRA_Entity__c);
        Gmail.getRegionBasedOnBillingEntity(TE1.TELSTRA_Entity__c); 
        Gmail.getRegionBasedOnBillingEntity(TE2.TELSTRA_Entity__c); 
        Gmail.getRegionBasedOnBillingEntity(TE3.TELSTRA_Entity__c); 
        Gmail.getRegionBasedOnBillingEntity(TE4.TELSTRA_Entity__c);  
        Gmail.getRegionBasedOnBillingEntity(TE5.TELSTRA_Entity__c);  
        Gmail.sendEmailtoBillingTeamonLiveisDesign(servList[0],productNameList);
        Gmail.sendEmailtoBillingTeamonLiveisDesign(ser ,productNameList);
        Gmail.sendEmailtoBillingTeam(ser,productNameList);
        Gmail.getRegionBasedOnBillingEntity(ser.Telstra_Billing_Entity__c);  
        Gmail.getRequiredEmailAddresses(order);
        Gmail.getRequiredEmailAddresses(order1);
        //Gmail.sendEmailtoBillingTeam(servList[0],productNameList);
        Gmail.getEmailAddresses('EMEA');
        Gmail.getEmailAddresses('North Asia');
        Gmail.getEmailAddresses('PACNET');
        gmail.sendEmailForBlockingBilling('test@salesforce.com', 'Testsubject', 'Testbody');
        gmail.getProductNamesOfGIAAS(oli1);
        gmail.mail('Test123456@salesforce.com', 'Testsubject', 'Testbody');
        P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
    }
    
    static testmethod void emailaddresstest3(){
        
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId()); 
        
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Country_Lookup__c> countryList =  P2A_TestFactoryCls.getcountry(1); 
        List<Contact> contList = P2A_TestFactoryCls.getContact(1, accList);
        List<Site__c> siteList = P2A_TestFactoryCls.getsites(1, accList, countryList);
        //List<BillProfile__c>  billProfList = P2A_TestFactoryCls.getBPs(1, accList ,siteList,contList);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<Order__c> orderList = new List<Order__c>();
        orderList.add(new Order__c(Status__c='test',Account__c = acclist[0].Id));
        insert orderList;
        system.assert(orderList!=null);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        servlist[0].AccountId__c = acclist[0].Id;
        update servlist;
        
        
        List<csord__Order__c> ordList = P2A_TestFactoryCls.getorder(1,ordReqList);
        List<csord__Order_Line_Item__c> OrderLineItemlist = P2A_TestFactoryCls.getOrderlineItem(1,ordList,ordReqList);
        
        BillProfile__c bp = new BillProfile__c();
        bp.name = 'Test bill profile';
        bp.Activated__c = True;
        bp.Account__c = acclist[0].Id; 
        bp.Status__c = 'Active';
        bp.Approved__c = true;
        bp.Bill_Profile_Site__c = siteList[0].Id; 
        bp.Billing_Entity__c = 'Australia';
        insert bp;
        system.assert(bp!=null);
        PACNET_Entities__c pacnet = new PACNET_Entities__c();
        pacnet.name = 'Entity1';
        pacnet.PACNET_Entity__c = 'Asia Netcom Pacnet (Ireland) Limited';
        insert pacnet;
        
        TELSTRA_Entities__c TE = new TELSTRA_Entities__c();
        TE.name = 'AustraliaRegion';
        TE.TELSTRA_Entity__c ='Telstra Corporation Limited,Telstra Network Services NZ Limited';
        insert TE;
        
        TELSTRA_Entities__c TE1 = new TELSTRA_Entities__c();
        TE1.name = 'EMEARegion';
        TE1.TELSTRA_Entity__c ='Telstra Limited';
        insert TE1;
        
        TELSTRA_Entities__c TE2 = new TELSTRA_Entities__c();
        TE2.name = 'NorthAsiaRegion';
        TE2.TELSTRA_Entity__c ='Beijing Australia Telecommunications Technical Consulting Services Company Limited,Telstra International (AUS) Limited - Taiwan Branch,Telstra International Limited,Telstra Japan K.K.,Telstra Services Korea Limited';
        insert TE2;
        
        TELSTRA_Entities__c TE3 = new TELSTRA_Entities__c();
        TE3.name = 'NorthAsiaRegion1';
        TE3.TELSTRA_Entity__c ='Telstra Services Asia Pacific (HK) Limited,Telstra Cable (HK) Limited,Telstra Global (Singapore) Pte. Ltd.,Telstra Services (USA) Inc.,Telstra Services (Taiwan) Inc.,Telstra Global (HK) Limited';
        insert TE3;
        
        TELSTRA_Entities__c TE4 = new TELSTRA_Entities__c();
        TE4.name = 'SouthAsiaRegion';
        TE4.TELSTRA_Entity__c ='PT. Reach Network Services Indonesia,Reach Network India Private Limited,Telstra ( Thailand ) Co., Limited,Telstra Malaysia Sdn. Bhd.,Telstra Philippines Inc,Telstra Singapore Pte Limited,Telstra Telecommunications Private Limited';
        insert TE4;
        
        TELSTRA_Entities__c TE5 = new TELSTRA_Entities__c();
        TE5.name = 'USRegion';
        TE5.TELSTRA_Entity__c ='Telstra Incorporated';
        insert TE5;
        
        TELSTRA_Entities__c TE6 = new TELSTRA_Entities__c();
        TE6.name = 'SouthAsiaRegion1';
        TE6.TELSTRA_Entity__c ='Telstra Network Services NZ Limited';
        insert TE6; 
        
        Product2 prod1 = new Product2(name='Test Product', Product_ID__C ='TPD',CurrencyIsoCode='EUR');
        insert prod1;
        
        list<Order_Line_Item__c> oli1= new list<Order_Line_Item__c>();
        Order_Line_Item__c oli = new Order_Line_Item__c(ParentOrder__c = orderList[0].Id,CPQItem__c = '1', OrderType__c = 'New Provide', Product__c = prod1.Id,
                                                        Opportunity_Line_Item_ID__c ='111', Is_GCPE_shared_with_multiple_services__c='No');
        oli1.add(oli);
        insert oli1;
        
        List<QueueSobject> queueSObjList = QueueUtil.getQueuesListForObject(BillProfile__c.sobjecttype);
        
        
        Map<String ,TELSTRA_Entities__c> tmap = new Map<String ,TELSTRA_Entities__c>();
        tmap.put('AustraliaRegion' ,Te); 
        
        
        List<String> productNameList = new List<String>();
        productNameList.add('GCPE');
        
        GenericEmailSending Gmail = new GenericEmailSending();
        Gmail.getRegionBasedOnBillingEntity(pacnet.PACNET_Entity__c);
        Gmail.getRegionBasedOnBillingEntity(TE.TELSTRA_Entity__c);
        Gmail.getRegionBasedOnBillingEntity(TE1.TELSTRA_Entity__c); 
        Gmail.getRegionBasedOnBillingEntity(TE2.TELSTRA_Entity__c); 
        Gmail.getRegionBasedOnBillingEntity(TE3.TELSTRA_Entity__c); 
        Gmail.getRegionBasedOnBillingEntity(TE4.TELSTRA_Entity__c);  
        Gmail.getRegionBasedOnBillingEntity(TE5.TELSTRA_Entity__c);  
        Gmail.sendEmailtoBillingTeamonLiveisDesign(servList[0],productNameList);
        Gmail.getRequiredEmailAddresses(orderList);
        Gmail.getEmailAddresses('EMEA');
        Gmail.getEmailAddresses('North Asia');
        Gmail.getEmailAddresses('PACNET');
        gmail.sendEmailForBlockingBilling('test@salesforce.com', 'Testsubject', 'Testbody');
        gmail.getProductNamesOfGIAAS(oli1);
        gmail.mail('Test123456@salesforce.com', 'Testsubject', 'Testbody');
        P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
    }
    
}