/**
    @author - Accenture
    @date - 21-Aug-2012
    @version - 1.0
    @description - This is the test class for OrderLineItemBeforeInsertUpdate
*/
@isTest(SeeAllData = false)
private class OrderLineItemBeforeInsertUpdateTest {
    /*static Order__c ord;
    static Opportunity opp;
    static Order_Line_Item__c olit;
    static Order_Line_Item__c olit1;
    static Product2 prod;
    
    static Account acc;
    static Site__c s;
    static City_Lookup__c ci;
    static Contact con;
    static testMethod void myUnitTest(){
        olit = getOrderLineItem();
        olit.Line_Item_Status__c = 'Complete';
        olit1 = getOrderLineItem1();
        olit1.Line_Item_Status__c = 'Complete';
       
         
         try{
           update olit1;  
         }
         catch(DMLException e){
           system.debug('Error' +e.getMessage());
         }   
         try{
           update olit;  
         }
         catch(DMLException e){
           system.debug('Error' +e.getMessage());
         }   
    }
   private static void getOpportunityLineItem(){
        
        List<OpportunityLineItem> lst = new List<OpportunityLineItem>();
        
        OpportunityLineItem oli = new OpportunityLineItem();
        opp = getOpportunity();
        BillProfile__c b = getBillProfile();
        
        oli.OpportunityId = opp.Id;
        oli.IsMainItem__c = 'Yes';
        oli.OrderType__c = 'New Provide';
        oli.Quantity = 5;
        oli.UnitPrice = 10;
        oli.Is_GCPE_Shared_With_Multiple_services__c = 'No';
        oli.PricebookEntryId = getPriceBookEntry('Global IPVPN').Id;
        oli.ContractTerm__c = '12';    
        
        
        insert oli;
    }
    
    private static Site__c getSite(){
        if(s == null){
            Country_Lookup__c cl = getCountry();
            ci = getCity();
            acc = getAccount();
            con = getContact();
            s = new Site__c();
            s.Name = 'Test Site';
            s.AccountId__c = acc.Id;
            s.Address1__c = 'Bangalore';
            s.Address2__c = 'Karnataka';
            s.Country_Finder__c = cl.Id ;
            s.Address_Type__c = 'Site Address';
            s.City_Finder__c = ci.Id;
            s.Primary_Site_Contact__c = con.Id;
            insert s;
        }
        return s;
    }
    private static City_Lookup__c getCity(){
        if(ci == null){
            ci = new City_Lookup__c();
            ci.City_Code__c = 'ABC';
            ci.Generic_Site_Code__c = 'ABC&';
            insert ci;
        }
        return ci;
    }
     private static Pricebook2 getPriceBook(String prodName){
        Pricebook2 p = [SELECT Id FROM Pricebook2 LIMIT 1];
        return p;   
    }
    
    private static Product2 getProduct(String prodName){
        if(prod == null){
            prod = new Product2();
            prod.Name = prodName;
            prod.ProductCode = prodName;
            prod.Key_Milestone__c = true;
            prod.Supplier_Milestone__c = true;
            prod.Product_ID__c = 'GIP';
            insert prod;
        }
        return prod;
    }
    
    private static PricebookEntry getPriceBookEntry(String prodName){
        PricebookEntry p = new PricebookEntry();
        p.Pricebook2Id = getPriceBook(prodName).Id;
        p.Product2Id =  getProduct(prodName).Id;
        p.UnitPrice = 2000;
        p.IsActive = true;
        insert p; 
        return p;    
    }
     private static BillProfile__c getBillProfile(){
        BillProfile__c billProfileObj = new BillProfile__c();
        acc = getAccount();
        //b.Bill_Profile_Number__c = 'Test Bill Profile';
        billProfileObj.Billing_Entity__c = 'Telstra Limited';
        billProfileObj.Account__c = acc.Id;
        if(s == null){
            s = getSite();
        }
        if(con == null){
            con = getContact();
        }
        
        billProfileObj.Bill_Profile_Site__c = s.Id;
        billProfileObj.Billing_Entity__c = 'Telstra Corporation';
        billProfileObj.CurrencyIsoCode = 'GBP';
        billProfileObj.Payment_Terms__c = '30 Days';
        billProfileObj.Billing_Frequency__c = 'Monthly';
        billProfileObj.Start_Date__c = Date.today();
        billProfileObj.Invoice_Delivery_Method__c = 'Postal';
        billProfileObj.Postal_Delivery_Method__c = 'First Class';
        billProfileObj.Invoice_Breakdown__c = 'Summary Page';
        billProfileObj.Billing_Contact__c = con.id;
        billProfileObj.Status__c = 'Approved';
        billProfileObj.Payment_Method__c = 'Manual';
        billProfileObj.Minimum_Bill_Amount__c = 10.20;
        billProfileObj.First_Period_Date__c = date.today();
        billProfileObj.Rating_Rounding_Level__c = 2;
        billProfileObj.Bill_Decimal_Point__c = 0;
        billProfileObj.FX_Group__c = 'Corporate';
        billProfileObj.FX_Rule__c = 'FX Rate at end of period';
        
        
        insert billProfileObj;
        return billProfileObj;
    }
    
      private static Order_Line_Item__c getOrderLineItem(){
        if(olit == null){
        olit = new Order_Line_Item__c();
        s = getSite();
        prod = getProduct('Global IPVPN');
        ord = getOrder();
        olit.ParentOrder__c = ord.Id;
        olit.Product__c = prod.Id;
        olit.Is_GCPE_shared_with_multiple_services__c ='No';
        olit.Opportunity_Line_Item_ID__c = 'Opp Line Item 001';
        olit.Order_Placed_with_Supplier__c = system.today();
        olit.Supplier_Bill_Start_Date__c = system.today();
        olit.Supplier_Cancel_Date__c = system.today();
        olit.Supplier_Commitment_Date__c = system.today();
        olit.Standard_Delivery_Date__c = system.today();
        olit.Supplier_Order_Accepted_Date__c = system.today();
        olit.Actual_Supply_Date__c = system.today();
        olit.OrderType__c = 'New Provide';
        olit.CPQItem__c = '1';
        olit.Line_Item_Status__c = 'New';
        olit.Site_A__c = s.Id;
        olit.Site_B__c = s.Id;  
        olit.Last_Modified_User_Emp_No__c = '455544';
        olit.Pin_Service_ID__c = 'SYD SYD adjks';
        olit.Site_A_SDPM__c=s.Id;
        olit.Site_B_SDPM__c=s.Id;
        oli.Key_Milestone__c ='';
      
        insert olit;
        }
        system.debug('Milestone *' +olit.Supplier_Milestone__c);
        system.debug('Product Id' +olit.Product__c);
        
        return olit;
    }
    private static Order_Line_Item__c getOrderLineItem1(){
        if(olit1 == null){
        olit1 = new Order_Line_Item__c();
        prod = getProduct('Global IPVPN');
        s = getSite();
        ord = getOrder();
        olit1.ParentOrder__c = ord.Id;
        olit1.Product__c = prod.Id;
        olit1.Opportunity_Line_Item_ID__c = 'Opp Line Item 001';
        olit1.Order_Placed_with_Supplier__c = system.today();
        olit1.Supplier_Bill_Start_Date__c = system.today();
        olit1.Supplier_Cancel_Date__c = system.today();
        olit1.Supplier_Commitment_Date__c = system.today();
        olit1.Standard_Delivery_Date__c = system.today();
        olit1.Supplier_Order_Accepted_Date__c = system.today();
        olit1.Is_GCPE_shared_with_multiple_services__c ='No';
        olit1.Actual_Supply_Date__c = system.today();
        olit1.OrderType__c = 'Terminate';
        olit1.CPQItem__c = '1';
        olit1.Line_Item_Status__c = 'New';
        olit1.Last_Modified_User_Emp_No__c = '12323';
        olit1.Pin_Service_ID__c = 'SYD adjks';
        //oli.Key_Milestone__c ='';
        olit.Site_A__c = null;
        olit.Site_B__c = s.Id; 
        insert olit1;
        }
        system.debug('Milestone *' +olit1.Supplier_Milestone__c);
        system.debug('Product Id' +olit1.Product__c);
        
        return olit1;
    }
    private static Order__c getOrder(){
        if(ord == null){
            ord = new Order__c();
            Opportunity o = getOpportunity();
            acc = getAccount();
            ord.Opportunity__c = o.Id;
            ord.OLIUpdateRequired__c = True;
            ord.Account__c = acc.Id;
            insert ord; 
            
        }
        return ord;
    }
    private static Contact getContact(){
      if(con == null){
        con = new Contact();
        acc = getAccount();
        Country_Lookup__c c = getCountry();
        
        con.LastName = 'Talreja';
        con.FirstName = 'Dinesh';
        
        con.AccountId = acc.Id;
        con.Contact_Type__c = 'Technical';
        con.Country__c = c.Id;
        con.Primary_Contact__c = true;
        con.MobilePhone = '123333';
        con.Phone = '1223';
        insert con;         
      }
      return con;
    }
    private static Opportunity getOpportunity(){
        if(opp == null){
        opp = new Opportunity();
        acc= getAccount();
        opp.Name = 'Test Opportunity';
        opp.AccountId = acc.Id;
        //updated Stage name verbal as Propose as per SOMP requirement  
        opp.StageName = 'Identify & Define';
        opp.Stage__c='Identify & Define';
        opp.CloseDate = System.today();
        opp.Estimated_MRC__c=800;
        opp.Estimated_NRC__c=1000;
        opp.ContractTerm__c='10';
        opp.Contract_Signed_Date__c =system.today();
        opp.Signed_Contract__c = 'Yes';
        opp.Signed_Contract_Order_form_Recieved_Date__c = system.today();
        opp.Upload_signed_order_Contract__c = true;
    
        insert opp;
        }
        return opp;
    }
     private static Account getAccount(){
        if(acc == null){
          acc = new Account();
          Country_Lookup__c cl = getCountry();
          acc.Name = 'Test Account Test 1';
          acc.Customer_Type__c = 'MNC';
          acc.Country__c = cl.Id;
          acc.Customer_Legal_Entity_Name__c = 'abc';
          acc.Selling_Entity__c = 'Telstra INC';
          acc.Activated__c = True;
          acc.Account_ID__c = '9090';
          acc.Account_Status__c= 'Active';
          insert acc;
        }
         return acc;
    }
    private static Country_Lookup__c getCountry(){
        Country_Lookup__c cl = new Country_Lookup__c();
       cl.CCMS_Country_Code__c = 'IND';
       cl.CCMS_Country_Name__c = 'ARMENIA';
        cl.Name = 'ARMENIA';
        cl.Country_Code__c = 'AM';
        insert cl;
        return cl;
    } */
}