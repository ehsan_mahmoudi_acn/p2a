/**
    @author -- Accenture
    @Date -- 5-Nov-2013
    @description - This class is used to populates U2C Master code

**/
Public class U2CMasterCodePopulation{

   /* public Static boolean isU2CMasterCode = true;
 //Update U2C master codes
 public static void populateU2CMasterCode(List<OpportunityLineItem> oPPLIList){
     Map<String,String> ProdIdMap = new Map<String,String>();
     List<OPPLISortingWrapper> oPPLISortList = new List<OPPLISortingWrapper>();
     List<OpportunityLineItem> afterSortOPPLIFirstLList = new List<OpportunityLineItem>();
     Map<String,OpportunityLineItem> rootCPQItemOPPLIMap = new Map<String,OpportunityLineItem>();
     
     Map<String,List<OpportunityLineItem>> bundleNameOPPLIListMap = new Map<String,List<OpportunityLineItem>>();
     
     //Here populating bundle action based on order type of bundle
     for(OpportunityLineItem oppLIObj :oPPLIList){
        
        
        if(!bundleNameOPPLIListMap.keySet().contains(oppLIObj.Bundle_Label_Name__c)){
            List<OpportunityLineItem> oppLI1LIst = new List<OpportunityLineItem>();
            oppLI1LIst.add(oppLIObj);
            bundleNameOPPLIListMap.put(oppLIObj.Bundle_Label_Name__c,oppLI1LIst);
        
        }else{
            bundleNameOPPLIListMap.get(oppLIObj.Bundle_Label_Name__c).add(oppLIObj);
        }
        
     }
     
     if(bundleNameOPPLIListMap.keyset().size()>0){
     
        for(String bundleLabelName : bundleNameOPPLIListMap.keyset()){
            
            integer newProvidecount = 0;
            integer terminateCount = 0;
            String bundleAction = '';
            
            List<OpportunityLineItem> oPPLItmList = bundleNameOPPLIListMap.get(bundleLabelName);
            
            integer lengthOfBundle = oPPLItmList.size();
            
            for(OpportunityLineItem oPPLIObj: oPPLItmList){
                
                if(oPPLIObj.ordertype__c == 'New Provide'){
                    newProvidecount++;
                    
                }else if(oPPLIObj.ordertype__c == 'Terminate' || oPPLIObj.ordertype__c =='Downgrade - Provide & Cease' ||oPPLIObj.ordertype__c=='Upgrade - Provide & Cease'){
                    terminateCount++;
                }
            
            }
            //Here populating bundleAction of Bundle based on count
            if(lengthOfBundle == newProvidecount){
            
                bundleAction = 'New';
            }else if(lengthOfBundle == terminateCount){
                
                bundleAction = 'Terminate';
            }else{
                
                bundleAction = 'Modify';
            }
            //Populate bundle type 
            for(OpportunityLineItem oPPLIObj: oPPLItmList){
            
                oPPLIObj.Bundle_Type__c = bundleAction;
            }
            
            
        }
     
     
     }
     
     
    
    //Sorting oppli based on cpq line item
    for(OpportunityLineItem oppLIObj :oPPLIList){
      oPPLISortList.add(new OPPLISortingWrapper(new OPPLITreeSorting(oppLIObj.CPQItem__c,oppLIObj)));
    }
     //sort the OPPLI based on CPQItem
    oPPLISortList.sort();
    for(OPPLISortingWrapper oPPLISortObj: oPPLISortList){
        afterSortOPPLIFirstLList.add(oPPLISortObj.pac.oPPli);
        
     }
    
    //ends here
    
    //Here creating Map of key as CPQItem and value as OPPLI
    for(OpportunityLineItem opplineitemObj : afterSortOPPLIFirstLList){
        
        if(!(opplineitemObj.CPQItem__c).contains('.')){
            rootCPQItemOPPLIMap.put(opplineitemObj.CPQItem__c,opplineitemObj);
        }
    }
    
    
    
     for(OpportunityLineItem opplineitemObj : afterSortOPPLIFirstLList){
         if(opplineitemObj.ParentCPQItem__c == null || opplineitemObj.ParentCPQItem__c == ''){
           
           opplineitemObj.U2C_Master_Code__c =  opplineitemObj.Product_id2__c;
            
        }   
        else {
            opplineitemObj.U2C_Master_Code__c = ProdIdMap.get(opplineitemObj.ParentCPQItem__c)+ ':'+opplineitemObj.Product_id2__c ;
            
        }
        ProdIdMap.put(opplineitemObj.CPQItem__c,opplineitemObj.U2C_Master_Code__c);
        //Here updating child line items Root bill text with Root Product's root product bill text
         if(opplineitemObj.Product_id2__c != opplineitemObj.Root_Product_Id__c){
                String cPQItm = opplineitemObj.CPQItem__c;
                integer indexofDot = cPQItm.indexOf('.');
                String rootProdCPQItem;
                if(indexofDot != -1){
                    rootProdCPQItem = cPQItm.subString(0,indexofDot);
                }
                
                if(rootCPQItemOPPLIMap.get(rootProdCPQItem) != null){
                    opplineitemObj.Root_Product_Bill_Text__c = rootCPQItemOPPLIMap.get(rootProdCPQItem).Root_Product_Bill_Text__c;
                    
                    //here updating child line items Product code(if don't have) with root level product code
                    
                    if(opplineitemObj.Product_code__c == null || opplineitemObj.Product_code__c == ''){
                        
                        opplineitemObj.Product_code__c = rootCPQItemOPPLIMap.get(rootProdCPQItem).Product_code__c;
                        
                    }
                }
                
         }
        //Defect 6739,Usage flag population for customer sites of Root product as GID
         if(opplineitemObj.Product_id2__c=='CUSTSITE' &&(opplineitemObj.Root_Product_Id__c=='GIDSECO' || opplineitemObj.Root_Product_Id__c=='GIDSSTD'
           || opplineitemObj.Root_Product_Id__c=='GIDSECP' || opplineitemObj.Root_Product_Id__c=='GID-BACKPOP')){
            System.debug('In If loop=='+opplineitemObj.Product_id2__c+'opp root product=='+opplineitemObj.Root_Product_Id__c);
            opplineitemObj.Usage_Base_Flag__c=True; 
                
          } 
          // commented the above if part as part of TGCTASK0187087 request from Manish Sharma 
            
        //else {
           // System.debug('Inside Else');
            opplineitemObj.Usage_Base_Flag__c=opplineitemObj.PricebookEntry.product2.Usage_Based_Charge_Flag__c;
        
       // }
        
        
     
     }
     isU2CMasterCode  = false;
     if(afterSortOPPLIFirstLList.size()>0)
     update afterSortOPPLIFirstLList;
 
 }*/

}