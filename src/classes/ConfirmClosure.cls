// Change status to Confirmed on Case, after validating conditions on Vendor Object 

Public class ConfirmClosure
{   
    /*public String CaseredirectID{get;set;}
    String caseid;
    decimal flag=0;
    public case ca = new case ();
    List<case> StatusUpdate = new List<case>();
    Set<string> SuppName = new Set<String>();
    Boolean Flag1=true;
    public ConfirmClosure(ApexPages.StandardController controller) 
    {
        caseid =ApexPages.currentPage().getParameters().get('id') ;
        CaseredirectID = Apexpages.currentPage().getParameters().get('id');
        ca = [select id, Is_Supplier_Quote_Request__c, Product_Configuration__c, Reason_for_selecting_one_supplier__c,Is_Parent_Case__c,Parent_Feasibility__c,status,Number_of_Child_Case_Closed__c,Number_of_Child_Case__c from case where Id=:caseid];
        system.debug('----case id----'+caseid);                
    }    
    public PageReference GotoDetailPage()
    {
        //cpundir - Defect Fix #12442 [SIT | Internet:Unable to confirm and close parent supplier case. Error message saying "No vendor quote available" is being displayed.]    
        if(ca.Is_Parent_Case__c == True) //--Parent Case 
        {
              system.debug('----Inside Parent Case Login . value ---> '+ ca.Is_Parent_Case__c);
            if(ca.Status == 'Confirmed')
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Case is already Confirmed'));         
                return null;
            }
            else
            {
                    // find id ,status of all the child cases
                    List<Case> ChildCaseStatus = [SELECT CaseNumber,Status FROM Case where ParentId = :ca.id]; 
                   //   Map<Integer,Case> ChildCaseStatus = new Map<Number,Case>([SELECT CaseNumber,Status FROM Case where ParentId = :caseid]);
                    // if status of any child case is not confirmed , then raise an error "Below mentioned child case is not confirmed yet"
                   for (Case i :ChildCaseStatus)
                   {
                            if ((i.status) != 'Confirmed')
                        {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'One of the child case is not Confirmed'));         
                            system.debug('Not confirmed Case number' + i.CaseNumber);
                            system.debug('+++++Status+++' + i.status);
                            Flag1=false;        
                            return null;                    
                        }                   
                   }                                
             }
            
            If (Flag1 == true)
            {
                system.debug('Flag Value --> '+ca.status);
                        ca.status='Confirmed';
                        StatusUpdate.add(ca);
                        update StatusUpdate;
                        system.debug('valueof StatusUpdate -- >'+ StatusUpdate);
            }       
        }  //cpundir - Defect Fix #12442
        else
        {  //Child case
            if(ca.Status == 'Confirmed')
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Case is already Confirmed'));         
                return null;
            }
            else
            {
                List<VendorQuoteDetail__c> caseVendor = [SELECT Supplier_Name__c,Winning_Quote__c FROM VendorQuoteDetail__c WHERE Related_3PQ_Case_Record__c =: ca.id];
                
                if(caseVendor.isEmpty()){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No vendor quote available'));             
                    return null;
                }
                else
                {         
                    for(VendorQuoteDetail__c vndr :caseVendor){
                        system.debug('@@@'+vndr);
                        SuppName.add(vndr.Supplier_Name__c);
                        if(vndr.Winning_Quote__c){flag=1;}
                    }
                    if(flag==0){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a "Winning quote" first.'));                 
                        return null;
                    }
                }
            } //Child Case 
                system.debug('@@@ca.Is_Supplier_Quote_Request__c'+ca.Is_Supplier_Quote_Request__c+ca.Reason_for_selecting_one_supplier__c+SuppName.size());
                
                if(ca.Is_Supplier_Quote_Request__c){
                    if(SuppName.size()==1 && ca.Reason_for_selecting_one_supplier__c == null){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please fill the Reason for selecting only one supplier'));
                        return null;
                        //system.debug('List of vendor quote details***>'+caseVendor);
                    }
                    else{
                        system.debug('@@@@'+ca.status);
                        ca.status='Confirmed';
                        StatusUpdate.add(ca);
                        }
                }
                
                system.debug('@@@@StatusUpdate'+StatusUpdate);
                update StatusUpdate;
                if (StatusUpdate.size() > 0) {
                    system.debug('supcase product config=>'+StatusUpdate.get(0).Product_Configuration__c);
					List<VendorQuoteDetail__c> caseVendorDetailsList = [SELECT Basket_Exchange_Rate__c, CurrencyIsoCode, Expiration_date__c, non_rec_new__c, Non_recurring_Charge__c, Product_Basket__c, Reccharge__c, Recurring_Charge__c, Vendor_Quote_Exchange__c FROM VendorQuoteDetail__c WHERE Related_3PQ_Case_Record__c =: ca.id];
					vendorquotecurrency vcurrency = new  vendorquotecurrency();
					vcurrency.Vendorquocurrency((List<VendorQuoteDetail__c>) caseVendorDetailsList);
                    SupplierCaseConfirmedBatch bc = new SupplierCaseConfirmedBatch(StatusUpdate.get(0).Product_Configuration__c);
                    Database.executeBatch(bc, 1);
                }
              }
                //update StatusUpdate;
                PageReference page = new PageReference('/'+caseid );
                return page ;               
    } */
}