public interface FieldValueGenerator {
    Boolean canGenerateValueFor(Schema.DescribeFieldResult fieldDesc);
    Object generate(Schema.DescribeFieldResult fieldDesc);
}