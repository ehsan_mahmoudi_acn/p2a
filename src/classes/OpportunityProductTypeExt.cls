public  class OpportunityProductTypeExt {
        
        public List<Opp_Product_Type__c> products {get;set;}
        public Id productId {get; set;}
        public boolean refresh {get;set;}
        public List<wrapperItem> wItem{get;set;}
        public  wrapperItem selectedItems{get;set;}
        public String sSelectedProductType {get;set;}
        public  Opportunity opp {get;set;}
        public List<Opp_Product_Type__c> UpdateProd = new List<Opp_Product_Type__c>();
        public CurrencyType currencyRates = new CurrencyType();
        public CurrencyType ProdcurrencyRates = new CurrencyType();
        
        public  OpportunityProductTypeExt(ApexPages.StandardController stdController)
        {
          wItem=new List<wrapperItem>();
        
          sSelectedProductType='';
          refresh=false;
          this.opp=(Opportunity)stdController.getRecord();
          //products=[Select Id,/*Product_SOV__c,*/Opportunity__c, Product_Type_Name__c, MRC__c, NRC__C, Existing_MRC__c, Net_Incremental_MRC__c,Sales_Specialist__c,Name,Calculated_New_SOV__c,Calculated_Renewal_SOV__c,Calculated_Total_SOV__c,Reported_New_SOV__c,Reported_Renewal_SOV__c,Reported_Total_SOV__c,CurrencyIsoCode from Opp_Product_Type__c where Opportunity__c=: this.opp.Id order by Product_Type_Name__c ];
          
          wItem.clear();
                
           for(Opp_Product_Type__c prod:[Select Id,/*Product_SOV__c,*/Opportunity__c, Product_Type_Name__c, MRC__c, NRC__C, Existing_MRC__c, Net_Incremental_MRC__c,Sales_Specialist__c,Name,Calculated_New_SOV__c,Calculated_Renewal_SOV__c,Calculated_Total_SOV__c,Reported_New_SOV__c,Reported_Renewal_SOV__c,Reported_Total_SOV__c,CurrencyIsoCode from Opp_Product_Type__c where Opportunity__c=: this.opp.Id order by Product_Type_Name__c ])
          {
            Prod.CurrencyIsoCode = opp.CurrencyIsoCode;
            selectedItems=new wrapperItem();
            selectedItems.setItem(prod);
            wItem.add(selectedItems);
          }     
        
        }
        
        public PageReference selectProducts()
        {
         PageReference pr=new PageReference('/apex/selectProductType?Id=' + opp.Id);
         pr.setRedirect(true);
         return pr;
        }
        
        public PageReference deleteProduct()
        {
         
         PageReference pr=new PageReference('/apex/myProductBundle');
         return pr;
        }

         public List<SelectOption> getProductTypeVals()
    {
        List<SelectOption> values= new List<SelectOption>();
        Schema.DescribeFieldResult statusFieldDescription = Opportunity.Product_Type__c.getDescribe();

        for (Schema.Picklistentry picklistEntry : statusFieldDescription.getPicklistValues())
        {
            values.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));
        }
        return values;
    }
      public void saveSelectedType()
    {
     if(String.IsNotBlank(sSelectedProductType))
     {//insert the selected product type (without MRC/NRC at this time)
       Opp_Product_Type__c pt=new Opp_Product_Type__c();
       Opportunity opp = [Select Id,CurrencyIsoCode from Opportunity where id=: this.opp.Id];
       pt.Name= sSelectedProductType.substring(0,sSelectedProductType.length());
       pt.Opportunity__c=opp.Id;
       pt.Product_Type_Name__c=sSelectedProductType;
       pt.MRC__C=0.00;
       pt.NRC__C=0.00;
       pt.Existing_MRC__c=0.00;
       pt.CurrencyIsoCode = opp.CurrencyIsoCode; 
       insert pt;
       refreshMainItem(false);       
     }
    }
    
    public void refreshMainItem(Boolean UpdateSOV)
    {
               //products=[Select Id, Product_SOV__c,Sales_Specialist_Category__c,Opportunity__c, Product_Type_Name__c, MRC__c, NRC__C, Existing_MRC__c,TCV__c,Net_Incremental_MRC__c,Sales_Specialist__c,Name,Calculated_New_SOV__c,Calculated_Renewal_SOV__c,Calculated_Total_SOV__c,Reported_New_SOV__c,Reported_Renewal_SOV__c,Reported_Total_SOV__c,CurrencyIsoCode from Opp_Product_Type__c where Opportunity__c=: this.opp.Id order by Product_Type_Name__c];
       Opportunity opp = [Select Id,Estimated_MRC__c,QuoteStatus__c,StageName, ContractTerm__c,Estimated_NRC__c,Total_Incremental_MRC__c,Product_Type__c,Existing_MRC__c,Calculated_New_SOV__c,Calculated_Renewal_SOV__c,Reported_New_SOV__c,Reported_Renewal_SOV__c,CurrencyIsoCode,Estimated_TCV__c from Opportunity where id=: this.opp.Id];
                  
          // clear selection
          wItem.clear();
         opp.Estimated_MRC__c = 0.00;
         opp.Estimated_NRC__c =0.00;              
         opp.Existing_MRC__c=0.00;
          //IR144: Soumya : Make SOV fields at Opportunity  to 0.00 to initiate roll-up to opportunity level from Opportunity Product Type
          if(opp.StageName != 'closed Won'){	
            opp.Calculated_New_SOV__c=0.00;
            opp.Calculated_Renewal_SOV__c=0.00;
            opp.Reported_New_SOV__c=0.00;
            opp.Reported_Renewal_SOV__c=0.00;
            opp.Total_Incremental_MRC__c =0.00;
            opp.Estimated_TCV__c =0.00;
            opp.Reported_MRC__c =0.00;
            opp.Reported_NRC__c =0.00;
            opp.Existing_MRC__c=0.00;
          }
          String prodType='';
          currencyRates = [Select Id, ISOCode, ConversionRate from CurrencyType where ISOCode =: opp.CurrencyIsoCode];
          List<Opp_Product_Type__c>prodList=[Select Id, Product_SOV__c,Sales_Specialist_Category__c,Opportunity__c, Product_Type_Name__c, MRC__c, NRC__C, Existing_MRC__c,TCV__c,Net_Incremental_MRC__c,Sales_Specialist__c,Name,Calculated_New_SOV__c,Calculated_Renewal_SOV__c,Calculated_Total_SOV__c,Reported_New_SOV__c,Reported_Renewal_SOV__c,Reported_Total_SOV__c,CurrencyIsoCode from Opp_Product_Type__c where Opportunity__c=: this.opp.Id order by Product_Type_Name__c];
          Set<String>IsoCodeSet=new Set<String>();          
          for(Opp_Product_Type__c prod:prodList){
                if(Prod.CurrencyIsoCode!=null){
                    IsoCodeSet.add(Prod.CurrencyIsoCode);
                }
           }
          List<CurrencyType>currencyList=[Select Id, ISOCode, ConversionRate from CurrencyType where ISOCode IN: IsoCodeSet];
          Map<String,CurrencyType>IsoToCurrencyMap=new Map<String,CurrencyType>();
          for(CurrencyType ct:currencyList){
             IsoToCurrencyMap.put(ct.ISOCode,ct); 
          }
          
          
          
          //IR144: Soumya: TO update the CUrrency Code at opportunity product Type.
            for(Opp_Product_Type__c prod:prodList)
            {
            //IR144: Soumya: TO update the CUrrency Code at opportunity product Type.
            if(UpdateProd.size() == 0 && Prod.CurrencyIsoCode != opp.CurrencyIsoCode){
                system.debug('Run select query');
                
                //ProdcurrencyRates = [Select Id, ISOCode, ConversionRate from CurrencyType where ISOCode =: Prod.CurrencyIsoCode];system.debug('Run select query2');      
                  ProdcurrencyRates=IsoToCurrencyMap.containsKey(prod.CurrencyIsoCode)?IsoToCurrencyMap.get(prod.CurrencyIsoCode):null;
            }
            if(Prod.CurrencyIsoCode != opp.CurrencyIsoCode){
                Prod.CurrencyIsoCode = opp.CurrencyIsoCode;
                Prod.MRC__c = (Prod.MRC__c * currencyRates.ConversionRate)/ProdcurrencyRates.ConversionRate;
                Prod.NRC__C = (Prod.NRC__C * currencyRates.ConversionRate)/ProdcurrencyRates.ConversionRate;
                Prod.Existing_MRC__c = (Prod.Existing_MRC__c * currencyRates.ConversionRate)/ProdcurrencyRates.ConversionRate;
                Prod.Reported_New_SOV__c = (Prod.Reported_New_SOV__c * currencyRates.ConversionRate)/ProdcurrencyRates.ConversionRate;
                Prod.Reported_Renewal_SOV__c = (Prod.Reported_Renewal_SOV__c * currencyRates.ConversionRate)/ProdcurrencyRates.ConversionRate;
                system.debug('Updatexxxx');
            }
            system.debug('44444444' + UpdateSOV);
            if(UpdateSOV==true){
                system.debug('panda after update 3');
                if(opp.SOV_Contract_Type__c == 'Duplicate')
                {
                Prod.Reported_New_SOV__c = 0.00;
                Prod.Reported_Renewal_SOV__c = 0.00;
                }
                Prod.Reported_New_SOV__c=Prod.Calculated_New_SOV__c;
                Prod.Reported_Renewal_SOV__c=Prod.Calculated_Renewal_SOV__c;
                }
                
                UpdateProd.add(Prod);
            selectedItems=new wrapperItem();
             opp.Estimated_MRC__c+=prod.MRC__c;
             opp.Estimated_NRC__c+=prod.NRC__c;
             //opp.Total_Incremental_MRC__c+=prod.Net_Incremental_MRC__c;
             //IR144: Soumya: Rolling up to Opportunity from Opportunity Product Type when the Quote status is before Order stage.
                if((opp.StageName != 'closed Won' ) && opp.ContractTerm__c !=null){	
                opp.Calculated_New_SOV__c+=prod.Calculated_New_SOV__c;
                opp.Calculated_Renewal_SOV__c+=prod.Calculated_Renewal_SOV__c;
                opp.Reported_New_SOV__c+=prod.Reported_New_SOV__c;
                opp.Reported_Renewal_SOV__c+=prod.Reported_Renewal_SOV__c;
                opp.Total_Incremental_MRC__c+=prod.Net_Incremental_MRC__c;
                opp.Estimated_TCV__c+=prod.TCV__c;
                opp.Reported_MRC__c+=prod.MRC__c;
                opp.Reported_NRC__c+=prod.NRC__c;
                opp.Existing_MRC__c+=prod.Existing_MRC__c;
                System.debug('opp.Calculated_New_SOV__c'+opp.Calculated_New_SOV__c+'opp.Calculated_Renewal_SOV__c'+opp.Calculated_Renewal_SOV__c+'opp.Reported_New_SOV__c'+opp.Reported_New_SOV__c+'opp.Reported_Renewal_SOV__c'+opp.Reported_Renewal_SOV__c);
            }
             if (prod.Existing_MRC__c==null){
                 prod.Existing_MRC__c=0.00;
             }
             
             prodType+=prod.Product_Type_Name__c+';';
            selectedItems.setItem(prod);
            wItem.add(selectedItems);
          }
          update UpdateProd;
          if(UpdateProd.size() > 0){
            refresh=true;
           }    
          if (prodType.trim().length()!=0){     
              prodType= prodType.substring(0,prodType.length()-1);
           }
          opp.Product_Type__c = prodType;
      TriggerFlags.NoOpportunitySplitValError=true;
          update opp;

       
    }
    
    public PageReference updateItems()
    {
         List<Opp_Product_Type__c> toUpdate=new List<Opp_Product_Type__c>();
         List<OpportunityTeamMember> oppTeam = new List<OpportunityTeamMember>();
         // Add validation to check the MRC price.
         Boolean isError = false;
         String errorName ='Estimated MRC/Estimated NRC  Field is Mandatory';
         string errorName2='at the Following Positions';
         String atleastOneProduct='Please select atleast One Product Type for the Opportunity to Update';
         String position='';
         ID opportunityIDValue;
         if (wItem.size()==0){
             isError = true;
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,atleastOneProduct));
         }
         Opportunity opp2 = [Select Id,Order_Type__c,ownerId from Opportunity where id=: this.opp.Id];
         
         List<String> ErrorList = new List<String>();
         
      if(wItem.size()>0)
      {
        Set<String>specialistSet=new Set<String>();
        
        for(Integer n=0; n<wItem.size(); n++){
            if(witem[n].item.Sales_Specialist__c != null)specialistSet.add(witem[n].item.Sales_Specialist__c);
        }
        List<user>uList=[Select Id,Name from User where name IN:specialistSet];
        Map<String,user>uMap=new Map<String,user>();
        for(user u:uList){
            umap.put(u.name,u);
        }
        for(Integer n=0; n<wItem.size(); n++)
        {
            isError = false;
            if (opp2.Order_Type__c!='Termination')
            {//Added condition not to check for Pen Service #8305 : Anshu
                if( (witem[n].item.MRC__C==0.00||witem[n].item.MRC__C<=0) && (witem[n].item.NRC__C==0.00||witem[n].item.NRC__C<=0) && witem[n].item.Product_Type_Name__c!='PEN')
                {
                    Integer currpos = n+1;
                    position+=currpos+',';                    
                    isError = true;
                } 
            }
                if (isError){
                    position= position.substring(0,position.length()-1);
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,errorName ));
                    ErrorList.add('Error at Position '+position);
                }
                
                if (!isError)
                    toUpdate.add(wItem[n].item);  
                if(!isError &&  witem[n].item.Sales_Specialist__c != null){
                   OpportunityTeamMember oppTeamMember = new OpportunityTeamMember();
                    oppTeamMember.TeamMemberRole ='Sales Specialist';
                  oppTeamMember.Opportunityid = opp2.id; 
                   opportunityIDValue = opp2.id;
                   //oppTeamMember.OpportunityAccessLevel = 'Edit';
                   User usr =uMap.containsKey(witem[n].item.Sales_Specialist__c)?uMap.get(witem[n].item.Sales_Specialist__c):null;
                   if(usr != null){
                        System.debug('User name '+ usr.name);
                       oppTeamMember.userId = usr.Id;
                   }
                   oppTeam.add(oppTeamMember);
                }               
            //isError=false;
        }
          
           if (!isError && ErrorList.isEmpty()){   
                if(oppTeam.size()>0){
                   upsert oppTeam;
                }
                update toUpdate;
          }
       }
       
       //Here updating OpportunityAccessLevel as Read/Write by default when creating OpportunityTeamMembers
        // get all of the team members' sharing records
        List<OpportunityShare> shares = [select Id, OpportunityAccessLevel,RowCause from OpportunityShare where OpportunityId =:opportunityIDValue and RowCause = 'Team'];

        // set all team members access to read/write
        for (OpportunityShare share : shares)  
          share.OpportunityAccessLevel = 'Edit';

        update shares;
       
       
         if (!isError && ErrorList.isEmpty())
         {
              refreshMainItem(false);
         } 
         //PageReference pr=new PageReference('/' + opp.Id);
         //pr.setRedirect(true);
         if (!isError && ErrorList.isEmpty())
         {
             refresh =true;
          }
         return null;    
    }
        
        public PageReference deleteItem()
        {
          String sToDelete=ApexPages.currentPage().getParameters().get('del');
          products=[Select Id,/* Product_SOV__c,*/Opportunity__c, Product_Type_Name__c, MRC__c, NRC__C, Existing_MRC__c, Net_Incremental_MRC__c,Sales_Specialist__c,Name,Calculated_New_SOV__c,Calculated_Renewal_SOV__c,Calculated_Total_SOV__c,Reported_New_SOV__c,Reported_Renewal_SOV__c,Reported_Total_SOV__c,CurrencyIsoCode from Opp_Product_Type__c where Opportunity__c=: this.opp.Id order by Product_Type_Name__c];
          String atleastOneProduct='An Opportunity needs atleast one Product Type, this Product Type cannot be deleted';
          boolean isError = false;
          if(products.size()==1){
             isError = true;
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,atleastOneProduct));
          }
          if (!isError)
          {
              if(String.IsNotBlank(sTodelete))
              {//delete the line item
                Opp_Product_Type__c toDel=[Select Id from Opp_Product_Type__c where Id=:sToDelete];
                delete toDel;
                refreshMainItem(false); 
                refresh=true;
              }
                
          }                              
         return null;
        }
        
        public PageReference cancel()
        {
          refresh=true;
          PageReference pr=new PageReference('/'+ opp.Id);
          pr.setRedirect(true);
          return pr;
        }
        
        public class wrapperItem
        { 
                public Opp_Product_Type__c item{get;set;}
                
                public wrapperItem()
                {
                    item=new Opp_Product_Type__c();
                }           
                public void setItem(Opp_Product_Type__c i)
                {
                    item=i;
                }
            }
        }
