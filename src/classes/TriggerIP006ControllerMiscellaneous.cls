Public Class TriggerIP006ControllerMiscellaneous{
    private class OrderSubmitValidatorException extends Exception {}
    public ID serviceId {get;set;}
    csord__Service__c service {get;set;} 
    List<csord__Service__c> serviceDetails = new List<csord__Service__c>();  
    List<csord__Service__c> serviceDetailsBundle = new List<csord__Service__c>();   
    List<csord__Service_Line_Item__c> serviceLineItem = new List<csord__Service_Line_Item__c>();
    public Boolean execute = true;
    public Boolean Miscellaneous = false;
    
    public TriggerIP006ControllerMiscellaneous(ApexPages.StandardController controller){
        
        serviceId= Apexpages.currentPage().getParameters().get('id');
        serviceDetails = [SELECT id, Name, Root_Bill_Text__c,Inventory_Status__c,Service_Bill_Text__c,Country__c,Telstra_Billing_Entity__c,Root_Product_Code__c,
                                 Billable_Flag__c,Usage_Flag__c,Charge_Id__c,ROC_Line_Item_Status__c,Bundle_Label_name__c,AccountId__r.Customer_Type_New__c,
                                 csord__Service__r.Parent_Customer_PO__c,csord__Service__c.csord__Subscription__c,csord__Service__c.Bill_ProfileId__c,
                                 csord__Service__c.Product_Configuration_Type__c,Parent_Customer_PO__c,Parent_Bundle_Flag__c,Bundle_Flag__c,
                                 csord__Service__r.Opportunity__c,Opportunity__c,csord__Service__r.Billing_Commencement_Date__c,
                                 Billing_Commencement_Date__c,csord__Service__r.Stop_Billing_Date__c,Stop_Billing_Date__c,
                                 csord__Service__r.Product_Configuration_Type__c,Firm_Delivery_Date__c,csord__Service__r.Firm_Delivery_Date__c,Product_Id__c,
                                 Termination_Date__c,Customer_Required_Date__c,Customer_Handover_Date__c,In_Service_Date__c,Service_Acceptance_Date__c,
                                 Supplier_Cancel_Date__c,Supplier_Termination_Date__c,Actual_Supply_Date__c,Supplier_Bill_Start_Date__c,Supplier_Commitment_Date__c,
                                 Supplier_Actual_Delivery_Date__c,Supplier_Order_Accepted_Date__c,csord__Service__r.Bill_ProfileId__c,
                                 Cost_Centre_Id__r.Cost_Centre_Integration_Number__c,createddate,IsIP006Enabled__c 
                          FROM csord__Service__c 
                          WHERE id =: serviceId AND IsIP006Enabled__c=:true];
        // serviceDetailsBundle = [SELECT id,Billing_Commencement_Date__c,Bill_ProfileId__c,Country__c,Billable_Flag__c,Usage_Flag__c,Opportunity__c,Inventory_Status__c,
        //Parent_Customer_PO__c,csord__Service__r.Parent_Customer_PO__c FROM csord__Service__c WHERE (Bundle_Label_name__c = :serviceDetails[0].Bundle_Label_name__c AND 
        //Opportunity__c =:serviceDetails[0].Opportunity__c)];
        serviceLineItem = [SELECT id,Transaction_Status__c,Pin_Service_ID__c,recordtypeid,Parent_of_Bundle__c,Billing_Commencement_Date__c,Bundle_Flag__c 
                           FROM csord__Service_Line_Item__c 
                           WHERE csord__Service__c =:serviceDetails[0].id AND Is_Miscellaneous_Credit_Flag__c=true];
   }
    
    public TriggerIP006ControllerMiscellaneous(){}
    public PageReference validateService(){
        integer count=0;
        for(csord__Service_Line_Item__c SliObj : serviceLineItem){
            if(SliObj.Transaction_Status__c!='Approved'){
                
                count++;
            }
            
        }
        
        if(count==serviceLineItem.size()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'There is no Approved Miscellaneous Line Item '));
            execute = false;
            Miscellaneous = true;
            
        }
        Set<Id> triggerIP006Ids = new Set<Id> ();
        for(csord__Service__c triggerIP006 : serviceDetails){
            triggerIP006Ids.add (triggerIP006.Id);
        }
                                
        Set<Id> lstBprofile=new Set<Id>();
        List<BillProfile__c> lstBprofileList=new List<BillProfile__c>();
        lstBprofileList=[Select id, Activated__c FROM BillProfile__c WHERE Id IN: triggerIP006Ids AND (Status__c!='Active' or Activated__c = False)];
        
        for(BillProfile__c temp : lstBprofileList){
            lstBprofile.add(temp.id);
        }
                                
        for(csord__Service__c triggerIP006 : serviceDetails)
        {
            if(serviceLineItem.isEmpty())
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'There is no Miscellaneous Line Item '));
                execute = false;
                Miscellaneous = true;
            }
            if(Miscellaneous == false)
            {       
                for(csord__Service_Line_Item__c SLIs : serviceLineItem)
                {
                    if(triggerIP006.Usage_Flag__c == 'Yes' && SLIs.Pin_Service_ID__c == '' )
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'"Pin Service Id" cannot be blank in Line Item.'));
                        execute = false;
                    }
                }
                
                if(triggerIP006.Root_Bill_Text__c == null || triggerIP006.Service_Bill_Text__c == null)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'"Root Bill Text" AND "Service Bill Text" cannot be Blank'));
                    execute = false;
                }
                
                if(triggerIP006.Telstra_Billing_Entity__c == null)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Billing Entity cannot be Blank'));
                    execute = false;
                }
                
                if(triggerIP006.Billable_Flag__c == null || triggerIP006.Usage_Flag__c == null)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'"Billable Flag AND Usage Flag" cannot be Blank'));
                    execute = false;
                }
                
                if(triggerIP006.Product_ID__c == null || triggerIP006.Root_Product_Code__c == null)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'"Root Product Code" AND "Product Id" cannot be Blank'));
                    execute = false;
                }
                
                if(triggerIP006.Country__c == null)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'"Country" value cannot be Blank'));
                    execute = false;
                }
                if(triggerIP006.csord__Service__r.Opportunity__c!=null && triggerIP006.csord__Service__r.Billing_Commencement_Date__c != null && 
                triggerIP006.Parent_Bundle_Flag__c==true && triggerIP006.Billing_Commencement_Date__c < triggerIP006.csord__Service__r.Billing_Commencement_Date__c)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Line item Billing Commencement date should be after than or equal to Parent line item Billing Commencement Date'));
                    execute = false;
                }
                if(triggerIP006.csord__Service__r.Opportunity__c!=null && triggerIP006.csord__Service__r.Billing_Commencement_Date__c != null && 
                triggerIP006.Parent_Bundle_Flag__c!=true && triggerIP006.Billing_Commencement_Date__c > triggerIP006.csord__Service__r.Billing_Commencement_Date__c)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Parent Line item "Billing Commencement Date" should be before than or equal to Child line item "Billing Commencement Date"'));
                    execute = false;
                }
                if(triggerIP006.csord__Service__r.Stop_Billing_Date__c != null && triggerIP006.Stop_Billing_Date__c != triggerIP006.csord__Service__r.Stop_Billing_Date__c && 
                (triggerIP006.csord__Service__r.Product_Configuration_Type__c == 'Terminate' || triggerIP006.csord__Service__r.Product_Configuration_Type__c == 'Parallel Upgrade' || triggerIP006.csord__Service__r.Product_Configuration_Type__c == 'Parallel Downgrade'))
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Line item Stop Billing Date should be same as Parent of Bundle line item Stop Billing Date'));
                    execute = false;
                }
                if(triggerIP006.csord__Service__r.Stop_Billing_Date__c != null && triggerIP006.Stop_Billing_Date__c != triggerIP006.csord__Service__r.Stop_Billing_Date__c && 
                (triggerIP006.Product_Configuration_Type__c == 'Terminate' || triggerIP006.Product_Configuration_Type__c == 'Parallel Upgrade' || 
                 triggerIP006.Product_Configuration_Type__c == 'Parallel Downgrade'))
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Line items "Stop Billing Date" should be Same for all line items of a bundle'));
                    execute = false;
                }
                /*/ if(triggerIP006.csord__Service__r.Opportunity__c!=null && triggerIP006.csord__Service__r.Billing_Commencement_Date__c != null && triggerIP006.Billing_Commencement_Date__c > triggerIP006.csord__Service__r.Billing_Commencement_Date__c)
                {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Parent Line item "Billing Commencement Date" should be before than or equal to Child line item "Billing Commencement Date"'));
                execute = false;
                }
                if(triggerIP006.csord__Service__r.Stop_Billing_Date__c != null && triggerIP006.Stop_Billing_Date__c != triggerIP006.csord__Service__r.Stop_Billing_Date__c && (triggerIP006.Product_Configuration_Type__c == 'Terminate' || triggerIP006.Product_Configuration_Type__c == 'Parallel Upgrade' || triggerIP006.Product_Configuration_Type__c == 'Parallel Downgrade'))
                {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Parent Line item "Stop Billing Date" should be Same as Child line item "Stop Billing Date"'));
                execute = false;
                }
                if((triggerIP006.Firm_Delivery_Date__c != null && triggerIP006.csord__Service__r.Firm_Delivery_Date__c!= null) && triggerIP006.Firm_Delivery_Date__c < triggerIP006.csord__Service__r.Firm_Delivery_Date__c)
                {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,System.Label.Target_Delivery_Date_Error));
                execute = false;
                }
                if(triggerIP006.Product_Id__c == System.Label.CustomerSite1 || triggerIP006.Product_Id__c == System.Label.CustomerSite2 || triggerIP006.Product_Id__c == System.Label.CustomerSite3 || triggerIP006.Product_Id__c == System.Label.CustomerSite4 || triggerIP006.Product_Id__c == System.Label.CustomerSite5 || triggerIP006.Product_Id__c == System.Label.GCPE )
                {
                if((triggerIP006.Firm_Delivery_Date__c != null && triggerIP006.csord__Service__r.Firm_Delivery_Date__c!= null) && triggerIP006.Firm_Delivery_Date__c < triggerIP006.csord__Service__r.Firm_Delivery_Date__c)
                {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,System.Label.Target_Delivery_Date_Error));
                execute = false; 
                }                        
                }/*/
                if((triggerIP006.Stop_Billing_Date__c == null || triggerIP006.Termination_Date__c == null) && (triggerIP006.Product_Configuration_Type__c == 'Cancel' || triggerIP006.Product_Configuration_Type__c == 'Terminate'))
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Stop Billing Date AND Termination Date cannot be blank as the OrderType is Terminate or Cancel'));
                    execute = false;
                }
                if(triggerIP006.Billing_Commencement_Date__c == null)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Billing Commencement Date dates cannot be blank'));
                    execute = false;
                    
                }
                /*/if(triggerIP006.Customer_Required_Date__c == null || triggerIP006.Firm_Delivery_Date__c == null ||  triggerIP006.Customer_Handover_Date__c == null ||  triggerIP006.In_Service_Date__c == null ||  triggerIP006.Service_Acceptance_Date__c == null || triggerIP006.Billing_Commencement_Date__c == null)
                {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Key Milestone Section dates cannot be blank'));
                execute = false;
                }
                if((triggerIP006.Product_Configuration_Type__c == 'Terminate' || triggerIP006.Product_Configuration_Type__c == 'Cancel') && (triggerIP006.Supplier_Cancel_Date__c == null || triggerIP006.Supplier_Termination_Date__c == null))
                {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Supplier Termination Date AND Supplier Cancel Date cannot be blank as the OrderType is Terminate or Cancel'));
                execute = false;
                }
                if(triggerIP006.Actual_Supply_Date__c == null || triggerIP006.Supplier_Bill_Start_Date__c == null || triggerIP006.Supplier_Commitment_Date__c   == null ||  triggerIP006.Supplier_Actual_Delivery_Date__c == null || triggerIP006.Supplier_Order_Accepted_Date__c == null)
                {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Supplier Milestone Section dates cannot be blank'));
                execute = false;
                }
                if(triggerIP006.Billing_Status__c == 'Complete' &&(triggerIP006.Product_Configuration_Type__c=='Parallel Upgrade' || triggerIP006.Product_Configuration_Type__c=='Parallel Downgrade' || triggerIP006.Product_Configuration_Type__c=='Terminate' || triggerIP006.Product_Configuration_Type__c=='Cancel'))
                {
                if(triggerIP006.Stop_Billing_Date__c==null)
                {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Stop Billing Date cannot be blank as the order type is “Parallel Upgrade” or “Parallel Downgrade” or "Terminate" or "Cancel"'));
                execute = false;
                }
                }/*/
                //List<BillProfile__c> lstBprofile=new List<BillProfile__c>();
                //lstBprofile=[Select id, Activated__c FROM BillProfile__c WHERE Id =: triggerIP006.Bill_ProfileId__c AND (Status__c!='Active' or Activated__c = False)];
                //if(lstBProfile.size()!=0)
                                                                                
                if(lstBProfile.contains(triggerIP006.Bill_ProfileId__c))
                {
                    triggerIP006.addError(Label.Bill_Profile_Error_msg);
                }  
                if((triggerIP006.csord__Service__c!=null && triggerIP006.csord__Service__r.Bill_ProfileId__c == null && triggerIP006.csord__Service__r.Billing_Commencement_Date__c == null) && (triggerIP006.Cost_Centre_Id__c!='' && triggerIP006.Cost_Centre_Id__r.Cost_Centre_Integration_Number__c == null))
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Please ensure that bill profile, billing commencement date AND cost centre information is populated at the root product level then only you will be able to set this line item status to complete')); 
                    execute = false;
                }
                if(Test.isRunningTest()){
                    execute = true;
                } 
                if(triggerIP006.Inventory_Status__c != system.label.PROVISIONED)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Line item Status must be LIVE AS DESIGNED to trigger IP006 for Miscellaneous Charge'));
                    execute = false;
                } 
                /*/ List<csord__Service__c> parent = [SELECT id,Billing_Status__c FROM csord__Service__c WHERE id=:triggerIP006.csord__Service__c];
                if(!parent.isEmpty())
                {
                for(csord__Service__c parentsli : parent)
                {
                if(parent[0].Billing_Status__c != 'Complete')
                {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'One of the Child Line Items status is not completed.'));
                execute = false;  
                }
                }
                }
                if(triggerIP006.Billing_Status__c == 'Complete' && (triggerIP006.Product_Id__c != system.label.Generic || triggerIP006.Product_Id__c != system.label.Generic1 ) && triggerIP006.Billing_Commencement_Date__c==null)
                {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Billing Commencement Date is mandatory')); 
                execute = false;
                }
                if(triggerIP006.Billing_Status__c == 'Complete' && (triggerIP006.Product_Id__c!= system.label.Generic || triggerIP006.Product_Id__c!= system.label.Generic1 ) && triggerIP006.Billing_Commencement_Date__c==null)
                {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Billing Commencement Date is mandatory')); 
                execute = false;
                }/*/
                else if(execute ==true)
                {
                    // TriggerBilling.SendBillingdata(triggerIP006.csord__Subscription__c);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Service Requested Successfully!!!'));
                    
                    
                    //IR116 IP006 transaction to allow only New Provide orders to ROC AND change order email notification for Billing based on their region
                    List<String> newServiceOLIList = new List<String>();
                    List<String> existingServiceOLIList = new List<String>();
                    List<String> ProductNameList = new List<String>();
                    
                    Map<String,csord__Service__c> parentOLINewServicessMap = new Map<String,csord__Service__c>();
                    Map<String,csord__Service__c> parentOLIExistingServicessMap = new Map<String,csord__Service__c>();
                    Map<String,csord__Service__c> wholeOLILineItems = new Map<String,csord__Service__c>();
                    
                    for(csord__Service__c oliLi:serviceDetails){
                        wholeOLILineItems.put(oliLi.Charge_ID__c,oliLi);
                        system.debug('@@@@oliLi.Charge_ID__c'+oliLi.Charge_ID__c+'@@@@oliLi.AccountId__r.Customer_Type_New__c'+oliLi.AccountId__r.Customer_Type_New__c);
                        if(oliLi.Charge_ID__c != null){
                            if(oliLi.AccountId__r.Customer_Type_New__c != 'ISO' && triggerIP006.createddate > Date.valueOf('2015-03-31'))
                                parentOLINewServicessMap.put(oliLi.Charge_Id__c,oliLi);
                        }
                        else if (oliLi.Charge_Id__c != null ){
                            if(oliLi.AccountId__r.Customer_Type_New__c != 'ISO' && triggerIP006.createddate < Date.valueOf('2015-03-31'))
                                parentOLIExistingServicessMap.put(oliLi.Charge_Id__c,oliLi);
                        }
                        
                    }
                    system.debug('@@@parentOLINewServicessMap'+parentOLINewServicessMap+'@@@wholeOLILineItems'+wholeOLILineItems);
                    for(csord__Service__c oliLis:serviceDetails){
                        if(parentOLINewServicessMap != null && wholeOLILineItems != null){
                            if(wholeOLILineItems.keyset().contains(oliLis.Charge_Id__c) && wholeOLILineItems.keyset() == parentOLINewServicessMap.keyset()){
                                newServiceOLIList.add(oliLis.csord__Subscription__c);
                            }
                            if(parentOLIExistingServicessMap!= null && wholeOLILineItems.keyset() == parentOLIExistingServicessMap.keyset()){
                                existingServiceOLIList.add(oliLis.csord__Subscription__c);
                            }               
                        }
                    }
                    
                    
                    // System.debug('OLIIDList size is '+oliIdList.size());
                    //Calling WSDL class 
                    System.debug('@@@newServiceOLIList'+newServiceOLIList+'####'+Label.MUTE_BILLING_INVOICE_INTERFACE);
                    if (Label.MUTE_BILLING_INVOICE_INTERFACE !='TRUE'){
                        if(newServiceOLIList.size()>0){
                            System.debug('I am calling'+newServiceOLIList.size());
                            
                            for(csord__Service__c oliList : serviceDetails){
                                if(!Test.isRunningTest()){
                                    TriggerBillingMiscellaneous.SendBillingdata(oliList.id); 
                                }
                            }
                        }
                        for(csord__Service__c oliId :serviceDetails){
                            
                            if(newServiceOLIList.size() == 0 && existingServiceOLIList.size()>0 && ProductNameList != null){
                                //send email to EMEA Billing Region for non bundle line items
                                GenericEmailSending emailsendingObj = new GenericEmailSending(); 
                                emailsendingObj.sendEmailtoBillingTeam(oliId,ProductNameList);
                            }
                        }   
                    }
                    //End of IR116 IP006 transaction to allow only New Provide orders to ROC
                    GenericEmailSending genericEmailsend=new GenericEmailSending(); 
                    if (Label.MUTE_BILLING_INVOICE_INTERFACE !='TRUE'){  
                        for(csord__Service__c oli:serviceDetails){
                            if(oli.ROC_Line_Item_Status__c!=null && oli.ROC_Line_Item_Status__c!='Success')                     
                                ErrorhandlingEmail.emailSending(genericEmailsend.getRegionBasedOnBillingEntity(oli.Telstra_Billing_Entity__c),oli.id);
                        }
                    }
                } 
            }
        }
        return null;
    }
}