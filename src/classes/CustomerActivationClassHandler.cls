public with sharing class CustomerActivationClassHandler extends BaseTriggerHandler{

    public override void beforeInsert(List<SObject> newCAT){
        
    }
         
    public override void afterInsert(List<SObject> newCustomerActivation, Map<Id, SObject> newCustomerActivationmap){
        timezonechange((List<Customer_Activation_Test__c>)newCustomerActivation);
    }
     
    public override void afterUpdate(List<SObject> newCustomerActivation, Map<Id, SObject> newCustomerActivationMap, Map<Id, SObject>  oldCustomerActivationMap){
        if(!TriggerContext.IsMethodAlreadyExecuted('timezonechangeafterupdate')){timezonechangeafterupdate((List<Customer_Activation_Test__c>) newCustomerActivation, (Map<Id,Customer_Activation_Test__c>) newCustomerActivationMap, (Map<Id,Customer_Activation_Test__c>) oldCustomerActivationMap, true);}
    }
     
        public void timezonechange(List<Customer_Activation_Test__c> NewCustActiveTes){ 
        
            List<Customer_Activation_Test__c> CustomActiveObj = new List<Customer_Activation_Test__c>();
            List<Id> CatIdsnew = new List<Id>();
            List<Id> CatIds = new List<Id>();
            boolean InFlightFlag = false;
            Datetime myDate;
            Id OrderId;
            Datetime TodayDate = system.now();
            String str = userinfo.getTimeZone().getId();
            String  myDatetoday = TodayDate.format('yyyy-MM-dd HH:mm:ss', str);
            Datetime dttoday = DateTime.valueOf(myDatetoday);
            String UserDate;            
            
            for(Customer_Activation_Test__c cat :NewCustActiveTes){
                CatIds.add(cat.Id);
            }

            List<Customer_Activation_Test__c> CustomerActiveList = [SELECT Id, Customer_contact_details__c, Time_zone_for_appointment__c, Parent_Product__c, Scope_of_work__c, SN_request_item_sysid__c, Request_Summary__c,Activation_appointment_Date__c,Estimated_Start_Date__c, Product_Configuration_Type__c, CAT_InFlight_Update__c, Customer_Name__c, End_time_for_activation_activity__c, Order__c, Order__r.Is_InFlight_Update__c, Order__r.csord__Account__c, Order__r.Customer_Primary_Order_Contact__c, START_Time_Tibco__c, End_Time_Tibco__c, Start_time_for_activation_activity__c FROM Customer_Activation_Test__c Where Id IN :CatIds];
           
            if(CustomerActiveList.Size() > 0){
                
                for(Customer_Activation_Test__c Catobjnew :CustomerActiveList){
                    if(Catobjnew.Order__r.Is_InFlight_Update__c){InFlightFlag = true;}
                    if(OrderId == null){OrderId = Catobjnew.Order__c;}                  
                    Catobjnew.Account__c = Catobjnew.Order__r.csord__Account__c;
                    Catobjnew.Service_Name__c = Catobjnew.Name;
                    Catobjnew.Parent_Product__c = 'CAT';
                    Catobjnew.Product_Configuration_Type__c = 'New Provide';
                    Catobjnew.Type_of_Update__c = 'data_update';
                    Catobjnew.CAT_InFlight_Update__c = 'No';

                    Datetime thisDT = Catobjnew.Start_time_for_activation_activity__c;
                    Datetime thisDT1 = Catobjnew.End_time_for_activation_activity__c;
            
                    if(Catobjnew.Time_zone_for_appointment__c ==  'London'){
                        UserDate = TodayDate.format('yyyy-MM-dd HH:mm:ss', 'Europe/London');
                    } 
                    else if(Catobjnew.Time_zone_for_appointment__c ==  'japan'){
                        UserDate = TodayDate.format('yyyy-MM-dd HH:mm:ss', 'Asia/Tokyo');
                    }
                    else if(Catobjnew.Time_zone_for_appointment__c ==  'pune'){
                        UserDate = TodayDate.format('yyyy-MM-dd HH:mm:ss', 'Asia/Kolkata');
                    } 
                    else if(Catobjnew.Time_zone_for_appointment__c ==  'HONG KONG/KUALA LUMPUR'){
                        UserDate = TodayDate.format('yyyy-MM-dd HH:mm:ss', 'Asia/Hong_Kong');
                    } 
                    else if(Catobjnew.Time_zone_for_appointment__c == 'Sydney/Melbourne'){
                        UserDate = TodayDate.format('yyyy-MM-dd HH:mm:ss', 'Australia/Sydney');
                    }
                    else if(Catobjnew.Time_zone_for_appointment__c ==  'Tokyo'){
                        UserDate = TodayDate.format('yyyy-MM-dd HH:mm:ss', 'Asia/Tokyo');
                    } 
                    else if(Catobjnew.Time_zone_for_appointment__c ==  'Los Angeles'){
                        UserDate = TodayDate.format('yyyy-MM-dd HH:mm:ss', 'America/Los_Angeles');
                    } 
                    else if(Catobjnew.Time_zone_for_appointment__c ==  'New York'){
                        UserDate = TodayDate.format('yyyy-MM-dd HH:mm:ss', 'America/New_York');
                    }
            
                    Datetime UserDateValue = DateTime.valueOf(UserDate);
                    long decHours = ((dttoday.getTime())/1000/60) - ((UserDateValue.getTime())/1000/60);
                    Datetime dt3 = thisDT.addMinutes(decHours.intValue());
                    Datetime dt4 = thisDT1.addMinutes(decHours.intValue());
                    
                    Catobjnew.Estimated_Start_Date__c = dt3;
                    Catobjnew.START_Time_Tibco__c = dt3;
                    Catobjnew.End_Time_Tibco__c= dt4;
                    
                    CustomActiveObj.add(Catobjnew);
                }
        
                if(CustomActiveObj.size()>0){
                    update CustomActiveObj;

                    if(InFlightFlag){
                        AmendOrder.amendOrderToFOM(OrderId);
                    } else{
                        updateInflightFlag(OrderId, true);
                    }
                }
            }
        }
    
        Public void timezonechangeafterupdate(List<Customer_Activation_Test__c> newCAT, Map<Id,Customer_Activation_Test__c> newCATMap, Map<Id,Customer_Activation_Test__c> oldCATMap, Boolean isUpdate){

            List<Customer_Activation_Test__c> CustomActiveObj = new List<Customer_Activation_Test__c>(); 
            List<Id> CatIds = new List<Id>();
            boolean InFlightFlag = false;
            Datetime myDate;
            String UserDate;
            Id orderId;
            Datetime TodayDate = system.now();
            String str = userinfo.getTimeZone().getId();
            String  myDatetoday = TodayDate.format('yyyy-MM-dd HH:mm:ss', str);
            Datetime dttoday = DateTime.valueOf(myDatetoday);           

            if(isUpdate){
                
                for(Customer_Activation_Test__c cat :newCAT){ 
                    if(
                        (cat.Service_Activation_Queue__c!= oldCATMap.get(cat.Id).Service_Activation_Queue__c) ||
                        (cat.Activation_appointment_Date__c!= oldCATMap.get(cat.Id).Activation_appointment_Date__c) || 
                        (cat.Start_time_for_activation_activity__c!= oldCATMap.get(cat.Id).Start_time_for_activation_activity__c) ||
                        (cat.End_time_for_activation_activity__c!= oldCATMap.get(cat.Id).End_time_for_activation_activity__c) ||                    
                        (cat.Time_zone_for_appointment__c!= oldCATMap.get(cat.Id).Time_zone_for_appointment__c) ||
                        (cat.Request_Summary__c!= oldCATMap.get(cat.Id).Request_Summary__c) ||
                        (cat.Scope_of_work__c!= oldCATMap.get(cat.Id).Scope_of_work__c) ||
                        (cat.Customer_contact_details__c!= oldCATMap.get(cat.Id).Customer_contact_details__c) ||
                        (cat.Customer_Name__c!= oldCATMap.get(cat.Id).Customer_Name__c)
                        ){
                        CatIds.add(cat.Id);                     
                    }
                }
                
                if(!CatIds.isEmpty()){
                    
                    List<Customer_Activation_Test__c> CustomerActiveList = [SELECT Id, Name, Customer_contact_details__c, Time_zone_for_appointment__c, Parent_Product__c, Scope_of_work__c, SN_request_item_sysid__c, Request_Summary__c,Activation_appointment_Date__c,Estimated_Start_Date__c, Product_Configuration_Type__c, CAT_InFlight_Update__c, Service_Activation_Queue__c, Account__c, Service_Name__c, Customer_Name__c, End_time_for_activation_activity__c, Order__c, Order__r.Is_InFlight_Update__c, Order__r.csord__Account__c, Order__r.Customer_Primary_Order_Contact__c, Start_time_for_activation_activity__c, START_Time_Tibco__c, End_Time_Tibco__c FROM Customer_Activation_Test__c Where Id IN :CatIds];

                    if(CustomerActiveList.size() > 0){
                        
                        for(Customer_Activation_Test__c Catobjnew :CustomerActiveList){
                            if(Catobjnew.Order__r.Is_InFlight_Update__c){InFlightFlag = true;}
                            if(orderId == null){orderId = Catobjnew.Order__c;}
                            Catobjnew.CAT_InFlight_Update__c = 'Yes';
                            
                            Datetime thisDT = Catobjnew.Start_time_for_activation_activity__c;
                            Datetime thisDT1 = Catobjnew.End_time_for_activation_activity__c;

                            if(Catobjnew.Time_zone_for_appointment__c ==  'London'){
                                UserDate = TodayDate.format('yyyy-MM-dd HH:mm:ss', 'Europe/London');
                            }
                            else if(Catobjnew.Time_zone_for_appointment__c ==  'japan'){
                                UserDate = TodayDate.format('yyyy-MM-dd HH:mm:ss', 'Asia/Tokyo');
                            }
                            else if(Catobjnew.Time_zone_for_appointment__c ==  'pune'){
                                UserDate = TodayDate.format('yyyy-MM-dd HH:mm:ss', 'Asia/Kolkata');
                            } 
                            else if(Catobjnew.Time_zone_for_appointment__c ==  'HONG KONG/KUALA LUMPUR'){
                                UserDate = TodayDate.format('yyyy-MM-dd HH:mm:ss', 'Asia/Hong_Kong');
                            } 
                            else if(Catobjnew.Time_zone_for_appointment__c == 'Sydney/Melbourne'){
                                UserDate = TodayDate.format('yyyy-MM-dd HH:mm:ss', 'Australia/Sydney');
                            } 
                            else if(Catobjnew.Time_zone_for_appointment__c ==  'Tokyo'){
                                UserDate = TodayDate.format('yyyy-MM-dd HH:mm:ss', 'Asia/Tokyo');
                            } 
                            else if(Catobjnew.Time_zone_for_appointment__c ==  'Los Angeles'){
                                UserDate = TodayDate.format('yyyy-MM-dd HH:mm:ss', 'America/Los_Angeles');
                            } 
                            else if(Catobjnew.Time_zone_for_appointment__c ==  'New York'){
                                UserDate = TodayDate.format('yyyy-MM-dd HH:mm:ss', 'America/New_York');
                            }

                            Datetime UserDateValue = DateTime.valueOf(UserDate);
                            long decHours = ((dttoday.getTime())/1000/60) - ((UserDateValue.getTime())/1000/60);
                            Datetime dt3 = thisDT.addMinutes(decHours.intValue());
                            Datetime dt4 = thisDT1.addMinutes(decHours.intValue());
                            Catobjnew.Estimated_Start_Date__c = dt3;
                            Catobjnew.START_Time_Tibco__c = dt3;
                            Catobjnew.End_Time_Tibco__c= dt4;
                            
                            CustomActiveObj.add(Catobjnew);
                        }

                        if(CustomActiveObj.size() > 0){
                            update CustomActiveObj;

                            if(InFlightFlag){
                                AmendOrder.amendOrderToFOM(OrderId);
                            } else{
                                updateInflightFlag(OrderId, true);
                            }                       
                        }
                    }
                }
            }
        }

        public void updateInflightFlag(Id OrderId, boolean isFlag){
            if(OrderId != null){
                csord__Order__c ordObj = new csord__Order__c(Id = OrderId, Is_InFlight_Update__c = isFlag);
                update OrdObj;
            }
        }       
    }