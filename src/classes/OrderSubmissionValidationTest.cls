@isTest(seealldata=false)
public with sharing class OrderSubmissionValidationTest {
    static testmethod void testisOrderSubmissionAllowed() {
       
        
      /* Id ordReqId = [select Id from csord__Order_Request__c ORDER BY CreatedDate DESC LIMIT 1].get(0).Id;
        csord__Order__c order = new csord__Order__c(
                                                    name='Test Order', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId,
                                                    Customer_Required_Date__c = Date.newInstance(2016,08,01));
        insert order;
        csord__Subscription__c sub = new csord__Subscription__c(csord__order__c = order.Id,
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId);
        insert sub;*/
        
       
      List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Order__c> orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
        List<csord__Subscription__c> sub = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        
         csord__Service__c masterServ = new csord__Service__c(); 
        List<csord__Service__c> childServs = new List<csord__Service__c>(); 
        csord__Service__c childServ = new csord__Service__c();
       
        try{
        masterServ.csordtelcoa__Service_Number__c = '21234-';// + xx;
        masterServ.Name = 'Test Master Serv 1234';
        //masterServ.Master_Service__c = '';
        masterServ.csord__Subscription__c = sub[0].Id;
        masterServ.csord__Identification__c = 'Order_a24O0000000OZ7aIAG';
        masterServ.Path_Instance_ID__c = 'abcd1234';
       // masterServ.csord__Order_Request__c = ordReqId;//'a2bO00000018nkO'; 
        insert masterServ;

        for(integer xx=0;xx<3;xx++){
            childServ = new csord__Service__c();
            childServ.csordtelcoa__Service_Number__c = '1234-' + xx;
            childServ.Name = 'Test Serv 1234-' + xx;
            childServ.Master_Service__c = masterServ.Id;
            childServ.csord__Subscription__c = sub[0].Id;
            childServ.csord__Identification__c = 'Order_a24O0000000OZ7aIAG'; 
           // childServ.csord__Order_Request__c = ordReqId;//'a2bO00000018nkO';
            childServs.add(childServ);
        }
        insert childServs;
        }catch(Exception e){
        }

        Test.startTest();
        //OrderSubmissionValidation osv = new OrderSubmissionValidation();
        System.assertEquals(OrderSubmissionValidation.isOrderSubmissionAllowed('1234'), true);
        Test.stopTest();
       
    } 
}