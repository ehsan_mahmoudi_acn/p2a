@isTest(SeeAllData = false) 
public class AllProductBasketTriggerHandlerTest {   
    
    @testSetUp static void setup(){
        List<Account> AccList;
        List<Opportunity> OppList;
        List<cscfga__Product_Configuration__c> ProConList;
        List<Product_Definition_Id__c> ProDefIdList;
        List<cscfga__Product_Definition__c> ProDefList;
        List<Offer_Id__c> OffIdList;
        List<cscfga__Attribute__c> AttList;
        List<cscfga__Attribute_Definition__c> AttDefList;
        List<cscfga__Configuration_Screen__c> ConScrList;
        List<cscfga__Screen_Section__c> ScrSecList;
        List<cscfga__Product_Basket__c> ProBasList;
        List<cscfga__Product_Basket__c> ProBasListUp;
        List<CS_Route_Segment__c> RouSegList;
        List<cscfga__Configuration_Offer__c> ConOffList;
        List<CS_Product_Configuration_Segment_Join__c> ProConSefJoiList;
        List<Fast_Quote__c> FasQuoList;
        List<CurrencyValue__c> CurValList; 
        List<CurrencyType> CurTypList; 
        List<Pricing_Approval_Request_Data__c> PriAppReqDatList; 
        List<case> CasList;
    
        //Maps
        Map<id, cscfga__Product_Basket__c> ProBasNewMap = new Map<id, cscfga__Product_Basket__c>();
        Map<id, cscfga__Product_Basket__c> ProBasOldMap = new Map<id, cscfga__Product_Basket__c>();
        
        CurValList = new List<CurrencyValue__c>{
            new CurrencyValue__c(CurrencyIsoCode = 'USD', Currency_Value__c = 1.0, Name = 'USD'),
            new CurrencyValue__c(CurrencyIsoCode = 'AUD', Currency_Value__c = 11.0, Name = 'AUD'),
            new CurrencyValue__c(CurrencyIsoCode = 'INR', Currency_Value__c = 62.0, Name = 'INR'),
            new CurrencyValue__c(CurrencyIsoCode = 'HKD', Currency_Value__c = 99.0, Name = 'HKD')
        };
        insert CurValList;
        
        System.assertEquals('USD', [SELECT CurrencyIsoCode FROM CurrencyValue__c WHERE Name = 'USD' limit 1].CurrencyIsoCode);
        System.assertEquals(4,CurValList.size()); 
        
        Acclist = new List<Account>{
            new Account(name = 'Test Normal', Customer_Type_New__c = 'GW', Selling_Entity__c = 'Telstra Incorporated',Account_Status__c = 'Active', Activated__c = True, Account_ID__c = '3333', Customer_Legal_Entity_Name__c = 'Test', Region__c = 'US'),
            new Account(name = 'ISO', Customer_Type_New__c = 'ISO', Selling_Entity__c = 'Telstra Incorporated',Account_Status__c = 'Active', Activated__c = True, Account_ID__c = '3333', Customer_Legal_Entity_Name__c = 'Test', Region__c = 'US')
        };
        insert AccList;
        
        
        OppList = new List<Opportunity>{
            new Opportunity(name = 'Opp', AccountID = AccList[0].id, Opportunity_Type__c = 'Complex', CloseDate = system.today()+ 10,CurrencyIsoCode = 'USD', StageName = 'Identify & Define', Sales_Status__c = 'Open', Order_Type__c = 'New', Win_Loss_Reasons__c = 'Duplicate' , Product_Type__c = 'IPL' , Estimated_MRC__c = 100.00 , Estimated_NRC__c = 100.00)
        };
        insert OppList;

        List<Opportunity> op = [Select id,name from opportunity where name = 'Opp'];
        //system.assertequals(oppList.name,op[0].name);
        System.assertEquals(AccList[0].ID, OppList[0].AccountId);

       
        
        ProBasList = new List<cscfga__Product_Basket__c>{
            new cscfga__Product_Basket__c(name = 'Product Basket 0',Exchange_Rate__c=17.00,csordtelcoa__Synchronised_with_Opportunity__c = false, cscfga__Opportunity__c = OppList[0].id,Margin__c = 90),
            new cscfga__Product_Basket__c(name = 'Product Basket 1',Exchange_Rate__c=18.00, cscfga__Opportunity__c = OppList[0].id),
            new cscfga__Product_Basket__c(name = 'Product Basket 2',Exchange_Rate__c=19.00, csordtelcoa__Synchronised_with_Opportunity__c = false, cscfga__Opportunity__c = OppList[0].id),
            new cscfga__Product_Basket__c(name = 'Product Basket 3',Exchange_Rate__c=20.00, csordtelcoa__Synchronised_with_Opportunity__c = false, cscfga__Opportunity__c = OppList[0].id),
            new cscfga__Product_Basket__c(name = 'IPL 4',Exchange_Rate__c=21.00,csordtelcoa__Synchronised_with_Opportunity__c = false, cscfga__Opportunity__c = OppList[0].id),
            new cscfga__Product_Basket__c(name = 'Dup 5',Exchange_Rate__c=22.00,csordtelcoa__Synchronised_with_Opportunity__c = false, cscfga__Opportunity__c = OppList[0].id)
        };
        checkRecursive.checkValues.remove('calculateTotalMargin');
        checkRecursive.checkValues.remove('getCurrencyRatioMap');
        checkRecursive.checkValues.remove('runAllProductBasketTrigger');
        checkRecursive.checkValues.remove('runAllProductBasketTriggerForAfter');
        
        System.assertNotEquals(ProBasList,null,'success');
        insert ProBasList;
        
        system.assertEquals(ProBasList[0].cscfga__Opportunity__c,OppList[0].id);
        
        checkRecursive.checkValues.remove('calculateTotalMargin');
        checkRecursive.checkValues.remove('getCurrencyRatioMap');
        checkRecursive.checkValues.remove('runAllProductBasketTrigger');
        checkRecursive.checkValues.remove('runAllProductBasketTriggerForAfter');
        
        
        ConOffList = new List<cscfga__Configuration_Offer__c>{
            new cscfga__Configuration_Offer__c(name = 'Master  VPLS VLAN Service', cscfga__Active__c =true, cscfga__Template__c = false),
            new cscfga__Configuration_Offer__c(name = 'Point To Point', cscfga__Active__c =true, cscfga__Template__c = false)
        };
        insert ConOffList;
        
        offIdlist = new List<Offer_Id__c>{
            new Offer_Id__c(name ='Master_IPVPN_Service_Offer_Id',Offer_Id__c = ConOffList[0].id),
            new Offer_Id__c(name ='Master_VPLS_Transparent_Offer_Id',Offer_Id__c = ConOffList[0].id),
            new Offer_Id__c(name ='Master_VPLS_VLAN_Offer_Id',Offer_Id__c = ConOffList[0].id),
            new Offer_Id__c(name ='Point To Point',Offer_Id__c = ConOffList[0].id)
        };
        insert offIdlist;
        
        system.assertEquals(offIdlist[2].Offer_Id__c,ConOffList[0].id);
        
        ProDeflist = new List<cscfga__Product_Definition__c>{
            new cscfga__Product_Definition__c(name  = 'Master IPVPN Service',cscfga__Description__c = 'Test master IPVPN'),
            new cscfga__Product_Definition__c(name  = 'Master VPLS Service', cscfga__Description__c = 'Test master VPLS'),
            new cscfga__Product_Definition__c(name  = 'VLANGroup_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
            new cscfga__Product_Definition__c(name  = 'IPVPN_Port_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
            new cscfga__Product_Definition__c(name  = 'SMA_Gateway_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
            new cscfga__Product_Definition__c(name  = 'VPLS_Transparent_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
            new cscfga__Product_Definition__c(name  = 'VPLS_VLAN_Port_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
            new cscfga__Product_Definition__c(name  = 'Master_IPVPN_Service_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
            new cscfga__Product_Definition__c(name  = 'Master_VPLS_Service_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
            new cscfga__Product_Definition__c(name  = 'Point to Point',cscfga__Description__c = 'Test 9'),
            new cscfga__Product_Definition__c(name  = 'Custom PTP',cscfga__Description__c = 'Test 10'),
            new cscfga__Product_Definition__c(name  = 'GCPE',cscfga__Description__c = 'Test 11'),
            new cscfga__Product_Definition__c(name  = 'GMNS',cscfga__Description__c = 'Test 12')
        };
        insert ProDeflist;
        
        System.assertEquals('Master IPVPN Service',ProDeflist[0].Name);
                
        cscfga__Product_Definition__c customPtp=new cscfga__Product_Definition__c(name  = 'Custom PTP',cscfga__Description__c = 'Test Custom PTP');
        insert customPtp;
        
        ProDefIdList = new List<Product_Definition_Id__c>{
            new Product_Definition_Id__c(name = 'Master IPVPN Service',Product_Id__c = ProDeflist[0].Id),
            new Product_Definition_Id__c(name = 'Master VPLS Service',Product_Id__c = ProDeflist[0].id),
            new Product_Definition_Id__c(name = 'VLANGroup_Definition_Id',Product_Id__c = ProDeflist[0].id),
            new Product_Definition_Id__c(name = 'IPVPN_Port_Definition_Id',Product_Id__c = ProDeflist[0].id),
            new Product_Definition_Id__c(name = 'SMA_Gateway_Definition_Id',Product_Id__c = ProDeflist[0].id),
            new Product_Definition_Id__c(name = 'VPLS_Transparent_Definition_Id',Product_Id__c = ProDeflist[0].id),
            new Product_Definition_Id__c(name = 'VPLS_VLAN_Port_Definition_Id',Product_Id__c = ProDeflist[0].id),
            new Product_Definition_Id__c(name = 'Master_IPVPN_Service_Definition_Id',Product_Id__c = ProDeflist[0].id),
            new Product_Definition_Id__c(name = 'Master_VPLS_Service_Definition_Id',Product_Id__c = ProDeflist[0].id),
            new Product_Definition_Id__c(name = 'Point to Point',Product_Id__c = ProDeflist[0].id),
            new Product_Definition_Id__c(name = 'Custom PTP',Product_Id__c = ProDeflist[0].id),
            new Product_Definition_Id__c(name = 'GCPE',Product_Id__c = ProDeflist[0].id),
            new Product_Definition_Id__c(name = 'GMNS',Product_Id__c = ProDeflist[0].id)
        };
        insert ProDefIdList;
        
        system.assert(ProDefIdList!=null);
        
        List<Product_Definition_Id__c> prodef = [Select id,name from Product_Definition_Id__c where name ='Master IPVPN Service'];
        
        system.assertequals(ProDefIdList[0].name,prodef[0].name);
        system.assertequals(ProDefIdList[0].Product_Id__c,ProDeflist[0].id);
        
        ProConList = new List<cscfga__Product_Configuration__c>{
            new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = ProDeflist[0].id,cscfga__Product_Basket__c = ProBasList[0].Id,cscfga__Product_Family__c = 'Test family',cscfga__Configuration_Offer__c = ConOffList[0].Id, CommaSepProdDetails__c = 'PEN', cscfga_Offer_Price_MRC__c = 0, cscfga_Offer_Price_NRC__c = 0, cscfga__Contract_Term_Period__c = 12, Cost_MRC__c = 10, Cost_NRC__c = 12,Product_Code__c = 'IPT'),
            new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = customPtp.id,cscfga__Product_Basket__c = ProBasList[0].Id,cscfga__Product_Family__c = 'Test family',cscfga__Configuration_Offer__c = ConOffList[0].Id, CommaSepProdDetails__c = 'PEN', cscfga_Offer_Price_MRC__c = 0, cscfga_Offer_Price_NRC__c = 0, cscfga__Contract_Term_Period__c = 12, Cost_MRC__c = 10, Cost_NRC__c = 12),
            new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = ProDeflist[0].id,cscfga__Product_Basket__c = ProBasList[0].Id,cscfga__Product_Family__c = 'Test family1',cscfga__Configuration_Offer__c = ConOffList[0].Id, cscfga_Offer_Price_MRC__c = 0, cscfga_Offer_Price_NRC__c = 0, cscfga__Contract_Term_Period__c = 12, Cost_MRC__c = 10, Cost_NRC__c = 12),
            new cscfga__Product_Configuration__c(name = 'IPL', cscfga__Product_Definition__c = ProDeflist[0].id, cscfga__Product_Basket__c = ProBasList[0].Id,cscfga__Product_Family__c = 'IPL',cscfga__Configuration_Offer__c = ConOffList[0].Id, cscfga_Offer_Price_MRC__c = 0, cscfga_Offer_Price_NRC__c = 0, cscfga__Contract_Term_Period__c = 12, Cost_MRC__c = 10, Cost_NRC__c = 12),
            new cscfga__Product_Configuration__c(name = 'EPL', cscfga__Product_Definition__c = ProDeflist[0].id, cscfga__Product_Basket__c = ProBasList[0].Id,cscfga__Product_Family__c = 'IPL',cscfga__Configuration_Offer__c = ConOffList[0].Id, cscfga_Offer_Price_MRC__c = 0, cscfga_Offer_Price_NRC__c = 0, cscfga__Contract_Term_Period__c = 12, Cost_MRC__c = 10, Cost_NRC__c = 12),
            new cscfga__Product_Configuration__c(name = 'ICBS', cscfga__Product_Definition__c = ProDeflist[0].id, cscfga__Product_Basket__c = ProBasList[0].Id,cscfga__Product_Family__c = 'IPL',cscfga__Configuration_Offer__c = ConOffList[0].Id, cscfga_Offer_Price_MRC__c = 0, cscfga_Offer_Price_NRC__c = 0, cscfga__Contract_Term_Period__c = 12, Cost_MRC__c = 10, Cost_NRC__c = 12),
            new cscfga__Product_Configuration__c(name = 'EPLX', cscfga__Product_Definition__c = ProDeflist[0].id, cscfga__Product_Basket__c = ProBasList[0].Id,cscfga__Product_Family__c = 'IPL',cscfga__Configuration_Offer__c = ConOffList[0].Id, cscfga_Offer_Price_MRC__c = 0, cscfga_Offer_Price_NRC__c = 0, cscfga__Contract_Term_Period__c = 12, Cost_MRC__c = 10, Cost_NRC__c = 12),
            new cscfga__Product_Configuration__c(name = 'GCPE', cscfga__Product_Definition__c = ProDeflist[0].id, cscfga__Product_Basket__c = ProBasList[0].Id,cscfga__Product_Family__c = 'IPL',cscfga__Configuration_Offer__c = ConOffList[0].Id, cscfga_Offer_Price_MRC__c = 0, cscfga_Offer_Price_NRC__c = 0, cscfga__Contract_Term_Period__c = 12, Cost_MRC__c = 10, Cost_NRC__c = 12),
            new cscfga__Product_Configuration__c(name = 'GMNS', cscfga__Product_Definition__c = ProDeflist[0].id, cscfga__Product_Basket__c = ProBasList[0].Id,cscfga__Product_Family__c = 'IPL',cscfga__Configuration_Offer__c = ConOffList[0].Id, cscfga_Offer_Price_MRC__c = 0, cscfga_Offer_Price_NRC__c = 0, cscfga__Contract_Term_Period__c = 12, Cost_MRC__c = 10, Cost_NRC__c = 12),
            new cscfga__Product_Configuration__c(name = 'IPL Dup', Is_Offnet__c = 'Yes', cscfga__Contract_Term_Period__c = 12, cscfga__Product_Definition__c = ProDeflist[0].id, cscfga__Product_Basket__c = ProBasList[0].Id,cscfga__Product_Family__c = 'IPL',cscfga__Configuration_Offer__c = ConOffList[0].Id, cscfga_Offer_Price_MRC__c = 0, cscfga_Offer_Price_NRC__c = 0, Cost_MRC__c = 10, Cost_NRC__c = 12),
            new cscfga__Product_Configuration__c(name = 'IPL Dup',Is_Offnet__c = 'Yes', cscfga__Contract_Term_Period__c = 12, cscfga__Product_Definition__c = ProDeflist[0].id, cscfga__Product_Basket__c = ProBasList[0].Id,cscfga__Product_Family__c = 'IPL',cscfga__Configuration_Offer__c = ConOffList[0].Id, cscfga_Offer_Price_MRC__c = 0, cscfga_Offer_Price_NRC__c = 0, Cost_MRC__c = 10, Cost_NRC__c = 12),
            new cscfga__Product_Configuration__c(name = 'Dup', Is_Offnet__c = 'Yes', cscfga__Contract_Term_Period__c = 12, cscfga__Product_Definition__c = ProDeflist[0].id, cscfga__Product_Basket__c = ProBasList[0].Id,cscfga__Product_Family__c = 'Dup',cscfga__Configuration_Offer__c = ConOffList[0].Id, cscfga_Offer_Price_MRC__c = 0, cscfga_Offer_Price_NRC__c = 0, Cost_MRC__c = 10, Cost_NRC__c = 12),
            new cscfga__Product_Configuration__c(name = 'newPc',Child_COst__c=20,Rate_Card_NRC__c=100,Rate_Card_RC__c=200, Is_Offnet__c = 'Yes', cscfga__Contract_Term_Period__c = 12, cscfga__Product_Definition__c = ProDeflist[0].id, cscfga__Product_Basket__c = ProBasList[0].Id,cscfga__Product_Family__c = 'Dup',cscfga__Configuration_Offer__c = ConOffList[0].Id, cscfga_Offer_Price_MRC__c = 0, cscfga_Offer_Price_NRC__c = 0, Cost_MRC__c = 10, Cost_NRC__c = 12)
        };
        
        checkRecursive.checkValues.remove('calculateTotalMargin');
        checkRecursive.checkValues.remove('getCurrencyRatioMap');
        checkRecursive.checkValues.remove('runAllProductBasketTrigger');
        checkRecursive.checkValues.remove('runAllProductBasketTriggerForAfter');
        
        insert ProConList;
        system.assert(ProConList!=null);
        system.assertEquals(ProConList[4].cscfga__Configuration_Offer__c,ConOffList[0].Id);
        
        checkRecursive.checkValues.remove('calculateTotalMargin');
        checkRecursive.checkValues.remove('getCurrencyRatioMap');
        checkRecursive.checkValues.remove('runAllProductBasketTrigger');
        checkRecursive.checkValues.remove('runAllProductBasketTriggerForAfter');
        
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Configuration_Screen__c> configscreen = P2A_TestFactoryCls.getConfigScreen(1,ProductDeflist);
        List<cscfga__Screen_Section__c> screen = P2A_TestFactoryCls.getScreenSec(1,configscreen);
        List<cscfga__Attribute_Definition__c > Attributedeflist= P2A_TestFactoryCls.getAttributesdef(1,ProConList,ProductDeflist,configscreen,screen);
        Attributedeflist[0].Fast_Quote_Result__c = true;
        update Attributedeflist;
        system.assert(Attributedeflist!=null);
        
        system.assertEquals(true,ProConList[1].cscfga__Product_Definition__c ==customPtp.id);
        AttList = new list<cscfga__Attribute__c>{
            new cscfga__Attribute__c(cscfga__Attribute_Definition__c=Attributedeflist[0].id,cscfga__Product_Configuration__c = ProConList[0].id,cscfga__Price__c=10,cscfga__Is_Line_Item__c=true, Name = 'SegmentJSON', CurrencyIsoCode = 'USD', cscfga__Value__c = '10')
            /*new cscfga__Attribute__c(cscfga__Attribute_Definition__c=Attributedeflist[0].id,cscfga__Product_Configuration__c = ProConList[1].id,cscfga__Price__c=10,cscfga__Is_Line_Item__c=true, Name = 'SegmentJSON', CurrencyIsoCode = 'USD', cscfga__Value__c = '10'),
            new cscfga__Attribute__c(cscfga__Attribute_Definition__c=Attributedeflist[0].id,cscfga__Product_Configuration__c = ProConList[0].id,cscfga__Price__c=10,cscfga__Is_Line_Item__c=true, Name = 'AttName', CurrencyIsoCode = 'USD', cscfga__Value__c = '20'),
            new cscfga__Attribute__c(cscfga__Attribute_Definition__c=Attributedeflist[0].id,cscfga__Product_Configuration__c = ProConList[0].id,cscfga__Price__c=10,cscfga__Is_Line_Item__c=true, Name = 'AttName', CurrencyIsoCode = 'HKD', cscfga__Value__c = '30'),
            new cscfga__Attribute__c(cscfga__Attribute_Definition__c=Attributedeflist[0].id,cscfga__Product_Configuration__c = ProConList[0].id,cscfga__Price__c=10,cscfga__Is_Line_Item__c=true, Name = 'AttName', CurrencyIsoCode = 'USD', cscfga__Value__c = '40')*/
        };
        insert AttList;
        system.assert(AttList!=null);
        system.assertEquals(AttList[0].cscfga__Attribute_Definition__c,Attributedeflist[0].id);
        
    }
    
    @isTest
    static void allBasketTrg(){
        test.startTest();
        try{
        List<cscfga__Product_Basket__c> ProBasList=[select id,Opp_Sales_Status__c from cscfga__Product_Basket__c where id!=''];
        checkRecursive.checkValues.remove('calculateTotalMargin');
        checkRecursive.checkValues.remove('getCurrencyRatioMap');
        checkRecursive.checkValues.remove('runAllProductBasketTrigger');
        checkRecursive.checkValues.remove('runAllProductBasketTriggerForAfter');
        
        cscfga__Product_Basket__c pb=ProBasList[3];     
        pb.csordtelcoa__Synchronised_with_Opportunity__c = true;            
        update pb;
        
        system.assert(pb!=null);
        System.assertEquals(6,ProBasList.size());
        System.assertEquals('Open',ProBasList[3].Opp_Sales_Status__c);
        }
        catch(Exception e){
        ErrorHandlerException.ExecutingClassName='AllProductBasketTriggerHandler :AllBasketTrg';         
        ErrorHandlerException.sendException(e); 
        }
        test.stopTest();
        
    }
    
    @isTest
    static void allBasketTrgLast(){
        
        List<cscfga__Product_Basket__c> ProBasList=[select id,Opportunity_Stage__c from cscfga__Product_Basket__c where id!=''];
        TriggerFlags.NoProductBasketTriggers = true;
        update ProBasList;
        
        system.assert(ProBasList!=null);
        System.assertEquals('Identify & Define',ProBasList[0].Opportunity_Stage__c);
        
        
    }
    
    @isTest
    static void testupdateProductConfigCurrency(){
        
        test.startTest();
        
        List<cscfga__Product_Basket__c> ProBasList=[select id,Formula_OptyId__c,cscfga__Opportunity__c,Total_Margin__c from cscfga__Product_Basket__c where id!=''];
        List<cscfga__Product_Configuration__c> pcList=[select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c in:ProBasList];
        List<cscfga__Attribute__c> attList=[select id,cscfga__Product_Configuration__c,Product_Code__c,/*PC_Id__c,*/CurrencyIsoCode from cscfga__Attribute__c where id!=''];
        
        List<cscfga__Product_Basket__c> updatebasket=new List<cscfga__Product_Basket__c>();
        List<cscfga__Product_Configuration__c> updatePC=new List<cscfga__Product_Configuration__c>();
        List<cscfga__Attribute__c> updateAttr=new List<cscfga__Attribute__c>();
        List<CurrencyValue__c> CurValList = null;
        
        Map<String, decimal> currencyMap = new Map<String, decimal>();
        
        CurValList = new List<CurrencyValue__c>{
            new CurrencyValue__c(CurrencyIsoCode = 'USD', Currency_Value__c = 1.0, Name = 'USD'),
            new CurrencyValue__c(CurrencyIsoCode = 'AUD', Currency_Value__c = 11.0, Name = 'AUD'),
            new CurrencyValue__c(CurrencyIsoCode = 'INR', Currency_Value__c = 62.0, Name = 'INR'),
            new CurrencyValue__c(CurrencyIsoCode = 'HKD', Currency_Value__c = 99.0, Name = 'HKD')
        };
        insert CurValList;
        
        for(CurrencyValue__c curr:CurValList) {
                    currencyMap.put(curr.Name, curr.Currency_Value__c);
            }
        
        for(integer i=0;i<ProBasList.size();i++){           
            cscfga__Product_basket__c pb=ProBasList[i];         
            pb.CurrencyIsoCode='HKD';
            pb.Exchange_Rate__c=40.00;
            pb.Quote_Status__c='Draft';
            pb.cscfga__Opportunity__c =null;
            pb.csordtelcoa__Synchronised_with_Opportunity__c=true;
            updatebasket.add(pb);
            system.assert(pb!=null);       
        }
        
        try{
            update ProBasList;
        }
        catch(exception e){
         ErrorHandlerException.ExecutingClassName='AllProductBasketTriggerHandler :testupdateProductConfigCurrency';         
         ErrorHandlerException.sendException(e);    
        }

        AllProductBasketTriggerHandler allp=new AllProductBasketTriggerHandler();       
        Map<String, decimal>testret=allp.getCurrencyRatioMap(1);
        Map<String, decimal>testret2=allp.getCurrencyRatioMap(2);
            
        test.stoptest();
        
        system.assertequals('IPT', attList[0].Product_Code__c);
        //system.assertequals(attList[0].cscfga__Product_Configuration__c, attList[0].PC_Id__c);
        //system.assertequals(ProBasList[0].cscfga__Opportunity__c, ProBasList[0].Formula_OptyId__c);
        system.assertequals(ProBasList[0].Total_Margin__c, 90.00);
        
        system.assertequals(11.0, currencyMap.get('AUD'));
        system.assertequals(true,pcList.size()>0);      
    }
    
    @isTest static void testExceptions(){
    
         Map<Id,Account>accMap=new Map<Id,Account>([select id,name from Account limit 10]);
         AllProductBasketTriggerHandler allPB=new AllProductBasketTriggerHandler(); 
        
         try{AllProductBasketTriggerHandler.UnSyncProductBasketsAfterInsertUpdate(null,null);}catch(Exception e){}
         try{allPB.createSegments(null,null);}catch(Exception e){}
         try{allPB.createFastQuoteResult(null,null);}catch(Exception e){}
         //try{allPB.updatePriceCase(null,null);}catch(Exception e){}
         try{allPB.changeProductBasketOwner(null);}catch(Exception e){}
         try{allPB.changeConfigOwner(accMap.keyset());}catch(Exception e){}
         try{allPB.updateBasketstatusnew(null);}catch(Exception e){} 
         try{allPB.updateIsColoForISO(null);}catch(Exception e){} 
         system.assert(allPB!=null);       
         
     }
    
    @isTest
    static void testgetMappingNames2(){
    test.startTest();
        
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<User> userList = P2A_TestFactoryCls.get_Users(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        List<cscfga__product_basket__c> prodBaskList1 = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        List<cscfga__product_basket__c> prodBaskList2 = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
    
        prodBaskList[0].csordtelcoa__Synchronised_with_Opportunity__c = true;
        update prodBaskList;
        
        prodBaskList1[0].csordtelcoa__Synchronised_with_Opportunity__c = false;
        update prodBaskList1;
        
        AllProductBasketTriggerHandler.UnSyncProductBasketsAfterInsertUpdate(prodBaskList,prodBaskList1);
        AllProductBasketTriggerHandler.UnSyncProductBasketsAfterInsertUpdate(prodBaskList,null);
        
        test.stoptest();    
        cscfga__product_basket__c prbskt = [select id,csordtelcoa__Synchronised_with_Opportunity__c from cscfga__product_basket__c where id =:prodBaskList1[0].id limit 1]; 

        system.assert(prodBaskList1!=null);
        System.assertNotEquals(null,prodBaskList[0].cscfga__Opportunity__c);
        System.assertEquals(true,prodBaskList[0].csordtelcoa__Synchronised_with_Opportunity__c);
        System.assertEquals(false,prodBaskList1[0].csordtelcoa__Synchronised_with_Opportunity__c);
    
    }
    @isTest
    static void testgetCutmptp3(){
    test.startTest();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<User> userList = P2A_TestFactoryCls.get_Users(1);
        //List<Contact> contList = P2A_TestFactoryCls.getContact(1,accList);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<Opportunity> oppList1 = P2A_TestFactoryCls.getOpportunitys(1, accList);
        
        List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        List<cscfga__product_basket__c> prodBaskList1 = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList1);
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        //List<cscfga__Attribute_Definition__c > Attributedeflist= P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist,configscreen,screen);
        //prodBaskList[0].csordtelcoa__Synchronised_with_Opportunity__c = true;
        //update prodBaskList;
    
        Contract__c contractObj1 = new Contract__c();       
        contractObj1.Account__c = accList[0].Id;
        contractObj1.Contract_Type__c = 'CRA';
        contractObj1.Status__c  = 'Active';
        contractObj1.Telstra_Contracting_Entity__c = 'Telstra Corporation Limited';
        contractObj1.Address_for_Legal_Notice__c = 'australia';
        contractObj1.Minimum_Spend_Conditions__c = 3;
        contractObj1.Payment_Terms__c = '15 Days';
        contractObj1.Initial_Period__c = '3';
        contractObj1.Customer_Signed_Date__c = System.today();
        contractObj1.Telstra_Signed_Date__c = System.today();
        contractObj1.Contract_Expiry_Date__c = System.today()+2;
        contractObj1.Contract_Effective_Date__c = System.today()+1;
        contractObj1.Tcorp_NDA__c = True;
        contractObj1.NDA_Signed__c = True;
        contractObj1.CIDN_T_Corp_Only__c = 'textarea';
        insert contractObj1;

        oppList1[0].StageName ='Closed Sales';
            update oppList1;    
            
        prodBaskList1[0].csordtelcoa__Synchronised_with_Opportunity__c = false;
        //prodBaskList1[0].Quote_Status__c
        update prodBaskList1;
        
        cscfga__Product_Definition__c pb = new cscfga__Product_Definition__c(name='Custom PTP',cscfga__Description__c= 'GCPEbdkjdvbhe;ghwfguwefkjwf');
        insert pb;
        
        List<cscfga__Configuration_Screen__c>ConfigLst= P2A_TestFactoryCls.getConfigScreen(1,new List<cscfga__Product_Definition__c>{pb});
        List<cscfga__Screen_Section__c> ssList1=P2A_TestFactoryCls.getScreenSec (1,ConfigLst);
        
        cscfga__Attribute_Definition__c  Attributesdef = new cscfga__Attribute_Definition__c();
        Attributesdef.name = 'Test Att';
        Attributesdef.cscfga__Product_Definition__c= pb.id;
        Attributesdef.cscfga__Configuration_Screen__c =ConfigLst[0].id;
        Attributesdef.cscfga__Screen_Section__c =ssList1[0].id;
        Attributesdef.cscfga__Column__c = 2;
        Attributesdef.cscfga__Row__c = 4;    
        Attributesdef.Fast_Quote_Result__c=false;
        insert Attributesdef;
    
        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(cscfga__Product_Family__c ='Custom PTP',
                                cscfga__Product_basket__c =prodBaskList[0].id,cscfga__Product_Definition__c=pb.id);
        insert pc;
        
        String attrVal='[{"cscfga__Attribute__c":"This is first attribute","AendPOPId":"aaa1","ZendPOPId":"bbb1"},'+
                    '{"cscfga__Attribute__c":"This is second attribute","AendPOPId":"aaa2","ZendPOPId":"bbb2"}]';
        
        cscfga__Attribute__c Attributes = new cscfga__Attribute__c(name = 'SegmentJSON',cscfga__Attribute_Definition__c=Attributesdef.id,cscfga__Product_Configuration__c=pc.id,cscfga__Value__c = attrVal);
        insert Attributes;
        
        cscfga__Attribute__c Attributes1 = new cscfga__Attribute__c(name = 'SegmentJSON',cscfga__Attribute_Definition__c=Attributesdef.id,cscfga__Product_Configuration__c=pc.id,cscfga__Value__c = attrVal);
        insert Attributes1;
        
        List<cscfga__Attribute__c> AttList = new List<cscfga__Attribute__c>();
        AttList.add(Attributes);
        AttList.add(Attributes1);
        
        cscfga__Product_Configuration__c oldPC = new cscfga__Product_Configuration__c(cscfga__Product_Family__c ='Custom PTP',
                    cscfga__Product_Basket__c =prodBaskList[0].id,cscfga__Configuration_Status__c='Valid',
                    cscfga__Product_Definition__c=pb.id);
        insert oldPC;
        
        List<cscfga__Product_Configuration__c> config = [Select id,name,cscfga__Product_Basket__c,cscfga__Product_Definition__r.name from cscfga__Product_Configuration__c
        Where cscfga__Product_Definition__c = :pb.id];
        
        
        
        Map<Id,cscfga__product_basket__c> newBsktMap = new Map<Id,cscfga__product_basket__c>();
        Map<Id,cscfga__product_basket__c> oldBsktMap = new Map<Id,cscfga__product_basket__c>();
          
    
        List<cscfga__Attribute__c> attList1 = [SELECT Id, Name, cscfga__Value__c, cscfga__Product_Configuration__r.cscfga__Product_Basket__c, cscfga__Product_Configuration__c
                                                FROM cscfga__Attribute__c 
                                                WHERE cscfga__Product_Configuration__r.cscfga__Product_Basket__c =:prodBaskList[0].id
                                                AND cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Name = :Label.CS_Custom_PTP_Definition_Name
                                                AND Name = :Label.CS_Custom_PTP_Attribute_Name];
                
        System.assert(attList1!=null);
        
        prodBaskList[0].csordtelcoa__Synchronised_with_Opportunity__c=true;
        update prodBaskList;
        newBsktMap.put(prodBaskList[0].id, prodBaskList[0]);
        
        
        cscfga__product_basket__c oldbsk=new cscfga__product_basket__c();
        oldbsk.id=prodBaskList[0].id;
        oldbsk.csordtelcoa__Synchronised_with_Opportunity__c=false;
        
        oldBsktMap.put(oldbsk.id, oldbsk);
        
        
        
        AllProductBasketTriggerHandler hndlr = new AllProductBasketTriggerHandler();
        hndlr.createSegments(newBsktMap,oldBsktMap);
        hndlr.createFastQuoteResult(newBsktMap,oldBsktMap);
        hndlr.updateBasketstatusnew(oldBsktMap);
        
    
    test.stoptest();    
    //System.assertNotEquals(null,prodBaskList[0].cscfga__Opportunity__c);
    System.assertNotEquals(oldbsk.csordtelcoa__Synchronised_with_Opportunity__c,prodBaskList[0].csordtelcoa__Synchronised_with_Opportunity__c);
    System.assertEquals('Custom PTP',config[0].cscfga__Product_Definition__r.Name);
    System.assertEquals('SegmentJSON',AttList[0].Name);
    System.assertEquals(prodBaskList[0].id,config[0].cscfga__Product_Basket__c);
    System.assertEquals('Closed Sales',oppList1[0].StageName,oppList1[0].StageName);
    
    
    }
    
    @isTest
    static void testgetMappingNames(){
    
    test.startTest();
        
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<User> userList = P2A_TestFactoryCls.get_Users(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        List<cscfga__product_basket__c> prodBaskList1 = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
        List<cscfga__Configuration_Screen__c> configscreen = P2A_TestFactoryCls.getConfigScreen(1,ProductDeflist);
        List<cscfga__Screen_Section__c> screen = P2A_TestFactoryCls.getScreenSec(1,configscreen);
        List<cscfga__Attribute_Definition__c > Attributedeflist= P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist,configscreen,screen);
        list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
        List<CS_Route_Segment__c>segList=P2A_TestFactoryCls.getSegment(1);
        AllProductBasketTriggerHandler.PCSegment pcSeg = new AllProductBasketTriggerHandler.PCSegment(AttributesList[0].cscfga__Product_Configuration__c);
            
        prodBaskList[0].OwnerId = userList[0].id;
        prodBaskList[0].csordtelcoa__Synchronised_with_Opportunity__c = true;
        prodBaskList[0].CurrencyIsoCode ='HKD';
        prodBaskList[0].Exchange_Rate__c  =1.0;
        
        update prodBaskList;
            
        oppList[0].CurrencyIsoCode ='USD';
        update oppList[0];
        
        cscfga__product_basket__c oldbask=prodBaskList[0];
        oldbask.id=prodBaskList[0].id;
        oldbask.OwnerId=userList[0].id;
        oldbask.CurrencyIsoCode='HKD';
        oldbask.Exchange_Rate__c=2.0;
       //oldbask.csordtelcoa__Synchronised_with_Opportunity__c=false;
         
        cscfga__Product_Definition__c pb = new cscfga__Product_Definition__c(name = 'Custom PTP',cscfga__Description__c= 'GCPEbdkjdvbhe;ghwfguwefkjwf');
        insert pb; 
        
        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(cscfga__Product_Family__c ='GCPE',
        cscfga__Product_basket__c = prodBaskList[0].id,cscfga__Product_Definition__c=pb.id);
        insert pc;
        
            
        Set<ID> ids = new Set<ID>();
        ids.add(prodBaskList[0].id);
            
        
        cscfga__Attribute__c Attributes = new cscfga__Attribute__c(name = 'SegmentJSON',cscfga__Product_Configuration__c=pc.id,cscfga__Value__c = 'Test',cscfga__Attribute_Definition__c =Attributedeflist[0].id);
        insert Attributes;
        
        
        
        Map<Id,cscfga__product_basket__c> prodBaskMap = new Map<Id,cscfga__product_basket__c>();
            prodBaskMap.put(prodBaskList[0].id,prodBaskList[0]);
        
        Map<Id,cscfga__product_basket__c> oldProdBaskMap = new Map<Id,cscfga__product_basket__c>();
            oldProdBaskMap.put(oldbask.id,oldbask);
        
        AllProductBasketTriggerHandler allp=new AllProductBasketTriggerHandler();
        Map<String,String>abc=allp.getMappingNames(AttributesList);
        
        allp.setProductBasketCurrency(prodBaskList);
        allp.changeConfigOwner(ids);    
        allp.changeProductBasketOwner(prodBaskList);
        allp.beforeDelete(prodBaskMap);
        allp.createSegments(prodBaskMap,oldProdBaskMap);
        allp.updateBasketstatusnew(prodBaskMap);
        allp.afterUpdate(prodBaskList,prodBaskMap,oldProdBaskMap);
        
        AllProductBasketTriggerHandler.runManagedSharing(ids);
        AllProductBasketTriggerHandler.UnSyncProductBasketsAfterInsertUpdate(prodBaskList,prodBaskList);    
        AllProductBasketTriggerHandler.DeleteOLIsProductDetailsAfterUpdate(prodBaskList,prodBaskList);
        AllProductBasketTriggerHandler.InsertOLIsProductDetailsAfterUpdate(prodBaskList,prodBaskList);
        AllProductBasketTriggerHandler.clearBasketAndSubComponents(prodBaskMap);    
        CS_Route_Segment__c testpc=allp.getCSsegment(segList,pcSeg);
        
        
        test.stoptest(); 
        
        //cscfga__Product_Configuration__c delprodconfig  = [Select Id from cscfga__Product_Configuration__c where id =:proconfigs[0].id];
        
        Map<Id, Opportunity> opptyMap = new Map<Id, Opportunity>([select Id, Name, CurrencyIsoCode from Opportunity where Id =: oppList[0].id]);
        
        
        for (cscfga__Product_Basket__c basket: prodBaskList) {
            basket.CurrencyIsoCode = opptyMap.get(basket.cscfga__Opportunity__c).CurrencyIsoCode;
            System.assertEquals(basket.CurrencyIsoCode,'USD');}
        /*
         try {delete delprodconfig;
              System.assert(false);}
           catch (DMLException e){
               System.assert(e.getMessage().contains('You cannot delete an ProductConfiguration which is linked to the external system.'));}
        
        System.assertEquals(delprodconfig.IsDeleted, false);
        */
        
        System.assertEquals(prodBaskList[0].csordtelcoa__Synchronised_with_Opportunity__c,true);        
        System.assertEquals(prodBaskList[0].csordtelcoa__Synchronised_with_Opportunity__c,true);
        System.assertEquals(prodBaskMap.values()[0].id,oldProdBaskMap.values()[0].id);
        system.assert(Attributes!=null);
        system.assertequals(13, [select count() from cscfga__Product_Configuration__c]);      
        //System.assertEquals('Custom PTP',Attributes.cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Name);
        System.assertEquals('SegmentJSON',Attributes.Name);
           
    }
        
}