public class TibcoOrderLine {
    public String lineNumber='';

    public String subscriberID='';

    public String productID='';

    public String productVersion='';

    public String quantity=null;

    public String uom='';

    public TibcoOrderAddress deliveryAddress=null;

    public String action='';

    public String actionMode='';

    public Date requiredByDate=null;

    public Date requiredOnDate=null;

    public String linkID='';

    public String inventoryID='';

    public String notes='';

    public String[] slaID;

    public Udf[] udf;

    public TibcoOrderCharacteristic[] characteristic=null;

    public String customerItemID='';
    public TibcoOrderLine()
    {
        
    }
    public TibcoOrderLine (
           String lineNumber,
           String subscriberID,
           String productID,
           String productVersion,
           String quantity,
           String uom,
           TibcoOrderAddress deliveryAddress,
           String action,
           String actionMode,
           Date requiredByDate,
           Date requiredOnDate,
           String linkID,
           String inventoryID,
           String notes,
           String[] slaID,
           Udf[] udf,
           TibcoOrderCharacteristic[] characteristic,
           String customerItemID){           
           this.lineNumber = lineNumber;
           this.subscriberID = subscriberID;
           this.productID = productID;
           this.productVersion = productVersion;
           this.quantity = quantity;
           this.uom = uom;
           this.deliveryAddress = deliveryAddress;
           this.action = action;
           this.actionMode = actionMode;
           this.requiredByDate = requiredByDate;
           this.requiredOnDate = requiredOnDate;
           this.linkID = linkID;
           this.inventoryID = inventoryID;
           this.notes = notes;
           this.slaID = slaID;
           this.udf = udf;
           this.characteristic = characteristic;
           this.customerItemID = customerItemID;
           }
 }