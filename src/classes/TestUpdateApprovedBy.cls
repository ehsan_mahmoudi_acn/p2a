/**
    @author - Accenture
    @date - 25- Jun-2012
    @version - 1.0
    @description - This is the test class for UpdateApprovedBy Trigger
*/
@isTest
private class TestUpdateApprovedBy {

    static Country_Lookup__c country;
    static Site__c site;
    static Account account;
    
    static testMethod void updateApprovedDetails() {
        Test.startTest();
        BillProfile__c billProfile = getBillProfile();
        insert billProfile;     
        billProfile.Approved__c = true;
        update billProfile;
        billProfile = [SELECT Approved_By__c FROM BillProfile__c WHERE Id =: billProfile.Id];
       system.assert(billProfile!=null);
        Test.stopTest();
    }
    private static BillProfile__c getBillProfile(){
        BillProfile__c b = new BillProfile__c();
        account = getAccount();
        b.Billing_Entity__c = 'Telstra Limited';
        b.Approved__c= false;
        b.Account__c = account.Id;
        b.Invoice_Breakdown__c = 'Summary Page';
        b.First_Period_Date__c = date.today();
        b.Start_Date__c=date.today();
        site = getSite();
        b.Bill_Profile_Site__c= site.id;
    
        return b;
    }
    private static Account getAccount(){
        if(account == null){
            account = new Account();
            Country_Lookup__c cl= getCountry();
            account.Customer_Legal_Entity_Name__c = 'Sample Entity Name';
            account.Name = 'Test Account';
            account.Customer_Type__c = 'MNC';
            account.Country__c = cl.Id;
            account.Selling_Entity__c = 'Telstra INC';
            insert account;
            }
        return account;
    }
    private static Contact getContact(){
        Contact con = new Contact();
        Account a = getAccount();
        country = getCountry();
        con.LastName = 'Gupta';
        con.AccountId = a.Id;
        con.Contact_Type__c = 'Sales';
        con.Country__c = country.Id;
        insert con;
        return con;
    }
    private static Site__c getSite(){
        if(site == null){
            site = new Site__c();
            country = getCountry();
            City_Lookup__c city = getCity();
            site.Name = 'Test Site';
            site.Address1__c = 'ABC';
            site.Address2__c = 'PQR';
            site.Address_Type__c = 'Billing Address';
            site.Country_Finder__c = country.Id;
            site.City_Finder__c = city.Id;
            account = getAccount();
            site.AccountId__c = account.Id;
            insert site;
        }
        return site;
    }
    private static City_Lookup__c getCity(){
        City_Lookup__c c = new City_Lookup__c();
        c.City_Code__c = 'Mum';
        insert c;
        return c;
    }
    private static Country_Lookup__c getCountry(){
        if(country == null){
            country = new Country_Lookup__c();
            country.CCMS_Country_Code__c = 'IND';
            country.CCMS_Country_Name__c = 'India';
            country.Country_Code__c = 'IND';
            insert country;
        }
        return country;
    } 
    
}