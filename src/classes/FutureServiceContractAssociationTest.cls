@isTest
public class FutureServiceContractAssociationTest{

/* static Service__c ServiceObj;
static Account acc;
static Opportunity opp;

static Country_Lookup__c cl;
static Contract__c con;
static Contract__c con1;


static testMethod void getContractDetails() {
    ServiceObj = getService();
    acc = getAccount();
    cl = getCountry();
    con = getContracts();
    opp = getOpportunity();
    con1 = getContracts1();
    
    ServiceObj.Name = 'Test';
    opp.Name = 'Test Opportunity';
    acc.Name = 'TestAccount';
    
    update acc;
    update opp;
    update ServiceObj;
   
    
}
    private static Service__c getService()
    {
       if(ServiceObj == null){
       
           ServiceObj = new Service__c();
           acc = getAccount();
           con = getContracts();
           opp = getOpportunity();
           ServiceObj.AccountId__c = acc.Id;
           system.debug('AccountId' +acc.Id);
           ServiceObj.Name = 'Test1234';
           Product2 prod = getProduct('EPL Express');
           ServiceObj.Product__c  = prod.Id;
           ServiceObj.Product_Code__c = 'GIP';
           ServiceObj.Net_MRC_Price__c = 34;
           ServiceObj.Net_NRC_Price__c = 12;
           ServiceObj.Contract_Duration__c = 24;
           ServiceObj.Bandwidth__c = 300;
           ServiceObj.Type_of_Topology__c = 'Mesh';
           ServiceObj.Contract_Form__c = con.Id; 
           ServiceObj.Opportunity__c = opp.Id;
           ServiceObj.Is_GCPE_shared_with_multiple_services__c ='No';
           insert ServiceObj;
       }
       return ServiceObj;
    }
     private static Product2 getProduct(String prodName){
        Product2 prod = new Product2();
        prod.Name = prodName;
        prod.ProductCode = prodName;
        prod.Product_ID__c ='EPL';
        insert prod;
        return prod;
    }
    private static Opportunity getOpportunity(){
        if(opp == null){
            opp = new Opportunity();
            acc = getAccount();
            opp.Name = 'Test Opportunity 1';
            opp.AccountId = acc.Id;
             //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
            opp.StageName = 'Identify & Define';
            opp.Stage__c='Identify & Define';
            opp.CloseDate = System.today();
            opp.Estimated_MRC__c=300;
            opp.Estimated_NRC__c=200;
            opp.ContractTerm__c='10';
            insert opp;
         }
         return opp;
    }
    private static Account getAccount(){ 
        if(acc == null){       
             acc = new Account();        
             cl= getCountry();        
             acc.Name = 'TestAccount1234';       
             acc.Customer_Type__c = 'MNC';        
             acc.Country__c = cl.Id;        
             acc.Selling_Entity__c = 'Telstra INC';  
             acc.Activated__c = True;
             acc.Account_ID__c = '9090';  
             acc.Customer_Legal_Entity_Name__c='Test';        
             insert acc;   
        }
        return acc; 
    }
    private static Country_Lookup__c getCountry(){     
         if(cl == null){
             cl = new Country_Lookup__c();       
             cl.CCMS_Country_Name__c = 'ARGENTINA';      
             cl.Country_Code__c = 'AR';            
             insert cl;  
         } 
         return cl;  
    }     
    private static Contract__c getContracts(){
         if(con == null){
             con = new Contract__c();
             RecordType rt = [SELECT Name, Id, SobjectType FROM RecordType where SobjectType='Contract__c' and Name='Order Form'];
             acc = getAccount();
             con1 = getContracts1();
             con.Account__c = acc.Id;
             system.debug('Contract Id' +con.Account__c);
             con.Related_Contract_ID__c = con1.Id;
             system.debug('Related Contract Id' +con.Related_Contract_ID__c);
             con.Customer_Signed_Date__c = system.today();
             con.Telstra_Signed_Date__c = system.today();
             con.Telstra_Contracting_Entity__c = 'Telstra Limited';
             con.Product_ID__c = 'EPL';
             con.Type_of_Order__c = 'New';
             con.Total_Order_Value__c = 2000;
             con.RecordType = rt;
             insert con;
         }
         return con;
     }   
     private static Contract__c getContracts1(){
         if(con1 == null){
             con1 = new Contract__c();
             acc = getAccount();
             RecordType rt1 = [SELECT Name, Id, SobjectType FROM RecordType where SobjectType='Contract__c' and Name='Contract Form'];
             acc = getAccount();
             con1.Account__c = acc.Id;
             con = getContracts();
             system.debug('Contract Id' +con.Account__c);
             con1.Related_Contract_ID__c = con.Id;
             system.debug('Related Contract Id' +con.Related_Contract_ID__c);
             con1.Customer_Signed_Date__c = system.today();
             con1.Telstra_Signed_Date__c = system.today();
             con1.Telstra_Contracting_Entity__c = 'Telstra Limited';
             con1.Product_ID__c = 'IPL';
             con1.Type_of_Order__c = 'New';
             con1.Total_Order_Value__c = 2000;
             con1.RecordType = rt1;
             insert con1;
         }
         return con1;
     }   
*/

}