global class CaseOverdueBatch implements Database.Batchable<sObject> {
    
        String query = 'select Id, Owner.Email from Case where Type = \'Enrichment\' and Due_Date__c < today';
    
    global CaseOverdueBatch() {
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Case> cases) {
        for(Case currentCase : cases){
            currentCase.Send_Overdue_Email__c = true;
        }   

        update cases;
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}