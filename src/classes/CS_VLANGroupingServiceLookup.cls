global with sharing class CS_VLANGroupingServiceLookup extends cscfga.ALookupSearch {
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
        
        String vlansInService = searchFields.get('VLANs From Service');
        String vlansPortService = searchFields.get('VLAN Port Service Ids');
        String replaceMasterService = searchFields.get('Replaced Master Service');
        String masterService = searchFields.get('Master Service Calculated');
        String basketId = searchFields.get('BasketId');
        String configId = searchFields.get('ConfigId');
        String vlansPortIds = searchFields.get('VLAN Ports Ids');
        
        String searchValue = searchFields.get('searchValue');
        Integer recordOffset = pageOffset * pageLimit;

        List<String> vlansServiceIDs = new List<String>();
        if(vlansPortService != null){
            vlansServiceIDs =  vlansPortService.split(',');

        }
        List<String> vlansIDs = new List<String>();
        if(vlansPortIds != null){
            vlansIDs =  vlansPortIds.split(',');
        }
        //List<cscfga__Product_Configuration__c> pcPortList = [SELECT Id, csordtelcoa__Replaced_Service__c FROM cscfga__Product_Configuration__c Where Id in :vlansIDs];
        //System.debug('***** CS_VLANGroupingServiceLookup pcPortList: '+vlansIDs+' --- ' + pcPortList);
        for(cscfga__Product_Configuration__c pc : [SELECT Id, csordtelcoa__Replaced_Service__c FROM cscfga__Product_Configuration__c Where Id in :vlansIDs]){
            vlansServiceIDs.add(pc.csordtelcoa__Replaced_Service__c);
        }
        System.debug('***** CS_VLANGroupingServiceLookup vlansServiceIDs: '+vlansServiceIDs);
        List <csord__Service__c> VLANSList;
        List <cscfga__Attribute__c> VLANGroupList;
         if(masterService != null && masterService != ''){
            VLANSList = [SELECT Id, Name, csord__Service__c, csord__Service__r.Master_Service__c, 
                                csordtelcoa__Product_Configuration__r.Product_Code__c,
                                csordtelcoa__Product_Configuration__r.Pop_Country__c,
                                csordtelcoa__Product_Configuration__r.Pop_City__c,
                                csordtelcoa__Product_Configuration__r.Class_of_Service_Mix__c,
                                csordtelcoa__Product_Configuration__r.Port_Speed__c,
                                csordtelcoa__Product_Configuration__r.Port_Speed_Name__c,
                                csordtelcoa__Product_Configuration__r.POP__c
                                                        FROM csord__Service__c
                                                        WHERE csord__Service__c NOT IN :vlansServiceIDs 
                                                            AND csord__Service__r.Master_Service__c = :replaceMasterService
                                                            AND csordtelcoa__Product_Configuration__r.Product_Code__c= 'VLV'];

            VLANGroupList =[SELECT Id, Name, cscfga__Product_Configuration__c,
                                cscfga__Product_Configuration__r.cscfga__Product_Basket__c, 
                                cscfga__Product_Configuration__r.Product_Definition_Name__c,
                                cscfga__Product_Configuration__r.Master_IPVPN_Configuration__c,
                                cscfga__Value__c
                        FROM cscfga__Attribute__c
                        WHERE cscfga__Product_Configuration__r.cscfga__Product_Basket__c = :basketId
                              AND cscfga__Product_Configuration__r.cscfga__Product_Definition__c = :productDefinitionId
                              AND cscfga__Product_Configuration__r.Master_IPVPN_Configuration__c = :masterService
                              AND Name = 'VLANs From Service'
                              AND cscfga__Product_Configuration__c != :configId];
        } else {
            VLANSList = [SELECT Id, Name, csord__Service__c, csord__Service__r.Master_Service__c, 
                        csordtelcoa__Product_Configuration__r.Product_Code__c,
                        csordtelcoa__Product_Configuration__r.Pop_Country__c,
                        csordtelcoa__Product_Configuration__r.Pop_City__c,
                        csordtelcoa__Product_Configuration__r.Class_of_Service_Mix__c,
                        csordtelcoa__Product_Configuration__r.Port_Speed__c,
                        csordtelcoa__Product_Configuration__r.Port_Speed_Name__c,
                        csordtelcoa__Product_Configuration__r.POP__c
                                                        FROM csord__Service__c
                                                        WHERE csord__Service__c NOT IN :vlansServiceIDs 
                                                            AND csordtelcoa__Product_Configuration__r.Product_Code__c= 'VLV'];
            
            VLANGroupList =[SELECT Id, Name, cscfga__Product_Configuration__c,
                                cscfga__Product_Configuration__r.cscfga__Product_Basket__c, 
                                cscfga__Product_Configuration__r.Product_Definition_Name__c,
                                cscfga__Product_Configuration__r.Master_IPVPN_Configuration__c,
                                cscfga__Value__c
                        FROM cscfga__Attribute__c
                        WHERE cscfga__Product_Configuration__r.cscfga__Product_Basket__c = :basketId
                              AND cscfga__Product_Configuration__r.cscfga__Product_Definition__c = :productDefinitionId
                              AND Name = 'VLANs From Service'
                              AND cscfga__Product_Configuration__c != :configId];
        }
        
        System.debug('***** CS_VLANGroupingServiceLookup VLANSList: '+VLANSList);

        
        
        System.debug('***** CS_VLANGroupingServiceLookup VLANGroupList: '+VLANGroupList);

        String takenVLANS ='';                                                
        for(cscfga__Attribute__c att : VLANGroupList){
            takenVLANS += att.cscfga__Value__c;
        }
        System.debug('***** CS_VLANGroupingServiceLookup takenVLANS: '+takenVLANS);
        
        List <csord__Service__c> returnList = new List <csord__Service__c>();
        List <Id> serIds= new List <Id>();

        for(csord__Service__c ser : VLANSList){
            if(!takenVLANS.contains(ser.Id)){
                serIds.add(ser.Id);
            }
        }   

    Integer resultSize = serIds.size();


    returnList = [SELECT Id, Name, csord__Service__c, csord__Service__r.Master_Service__c, 
                        csordtelcoa__Product_Configuration__r.Product_Definition_Name__c,
                        csordtelcoa__Product_Configuration__r.Pop_Country__c,
                        csordtelcoa__Product_Configuration__r.Pop_City__c,
                        csordtelcoa__Product_Configuration__r.Class_of_Service_Mix__c,
                        csordtelcoa__Product_Configuration__r.Port_Speed__c,
                        csordtelcoa__Product_Configuration__r.Port_Speed_Name__c,
                        csordtelcoa__Product_Configuration__r.POP__c
                            FROM csord__Service__c
                            WHERE Id IN :serIds
                            AND Name LIKE :searchValue + '%'
                            ORDER BY Name
                            LIMIT :resultSize OFFSET :recordOffset];
        
        return returnList;
    }

    public override String getRequiredAttributes(){ 
        return '["BasketId","VLANs From Service","Replaced Master Service", "VLAN Port Service Ids","VLAN Ports Ids","Master Service Calculated","ConfigId"]';
    }

    
    
}