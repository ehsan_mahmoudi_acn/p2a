global with sharing class CS_CustomResilienceLookup extends cscfga.ALookupSearch {

    public override String getRequiredAttributes(){ 
    	return '["Product Type", "A End POP City", "Z End POP City"]';
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
        
        Set <Id> resilIds = new Set<Id>();
        String productType = searchFields.get('Product Type');
        String aEndPOP = searchFields.get('A End POP City');
        String zEndPOP = searchFields.get('Z End POP City');

        System.Debug('Search field = ' + productType);
       	/*List<CS_Route_Segment__c> routeSegList = [SELECT Id, POP_A__c, POP_Z__c, CS_Resilience__c 
       											  FROM CS_Route_Segment__c
       											  WHERE Product_Type__c = :productType 
       											  		AND POP_A__c = :aEndPOP
       											  		AND POP_Z__c = :zEndPOP];*/

       	for(CS_Route_Segment__c item : [SELECT Id, POP_A__c, POP_Z__c, CS_Resilience__c 
       											  FROM CS_Route_Segment__c
       											  WHERE Product_Type__c = :productType 
       											  		AND POP_A__c = :aEndPOP
       											  		AND POP_Z__c = :zEndPOP]){
       		resilIds.add(item.CS_Resilience__c);
       	}
        System.Debug('doLookupSearch');
        System.Debug(searchFields);
        String searchValue = searchFields.get('searchValue') +'%';
        List <CS_Resilience__c> data = [SELECT Id, Name FROM CS_Resilience__c WHERE  Id IN :resilIds AND Name LIKE :searchValue ORDER BY Name];
        System.Debug(data);
       return data;
    }
}