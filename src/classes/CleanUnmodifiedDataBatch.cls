/**
 * @name CleanUnmodifiedDataBatch 
 * @description framework class for batch processes
 * @revision
 * Boris Bjelan 08-03-2016 Created class
 */
public class CleanUnmodifiedDataBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    List<Id> deletedOpptyIds = new List<Id>();
     
    public CleanUnmodifiedDataBatch() {
        deletedOpptyIds = new List<Id>();
    }
     
    public Database.QueryLocator start(Database.BatchableContext ctx) {
        return 
            Database.getQueryLocator([select Id, CreatedDate, LastModifiedDate 
                from 
                    Opportunity 
                where 
                    lastmodifieddate< :system.today().addDays(-10)]);
    }
    
    public void execute(Database.BatchableContext ctx, List<Opportunity> opps) {
        boolean noErrors = true;
        List<Id> oppIds= new List<Id>();
        try {
            // take oppty ids
            for (Opportunity opp: opps) oppIds.add(opp.Id);
        
            // get all attributes
            delete [select Id from cscfga__Attribute__c where cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__c in :oppIds];
           
            // get all configurations
            delete [select Id from cscfga__Product_Configuration__c where cscfga__Product_Basket__r.cscfga__Opportunity__c in :oppIds];

            // get all baskets
            delete [select Id from cscfga__Product_Basket__c where cscfga__Opportunity__c in: oppIds];

            // delete orch steps dependecies
            delete [select Id from CSPOFA__Orchestration_Step_Dependency__c 
                    where CSPOFA__Orchestration_Process__r.csordtelcoa__Service__r.Opportunity__c in: oppIds or 
                        CSPOFA__Orchestration_Process__r.csordtelcoa__Subscription__r.OpportunityRelatedList__c in: oppIds or 
                        CSPOFA__Orchestration_Process__r.Order__r.csordtelcoa__Opportunity__c in: oppIds];

            // delete orch steps
            delete [select Id from CSPOFA__Orchestration_Step__c 
                    where CSPOFA__Orchestration_Process__r.csordtelcoa__Service__r.Opportunity__c in: oppIds or 
                        CSPOFA__Orchestration_Process__r.csordtelcoa__Subscription__r.OpportunityRelatedList__c in: oppIds or 
                        CSPOFA__Orchestration_Process__r.Order__r.csordtelcoa__Opportunity__c in: oppIds];

            // delete orch process
            delete [select Id from CSPOFA__Orchestration_Process__c 
                    where csordtelcoa__Service__r.Opportunity__c in: oppIds or 
                        csordtelcoa__Subscription__r.OpportunityRelatedList__c in: oppIds or 
                        Order__r.csordtelcoa__Opportunity__c in: oppIds];

            // delete oppties;
            delete [select Id from Opportunity where Id in: oppIds];

        } catch (Exception ee) {
            noErrors = false;
        } finally {
            if (noErrors) {
                deletedOpptyIds.addAll(oppIds);    
            }
        }
    }
    
    public void finish(Database.BatchableContext ctx) {
        system.debug('CleanUnmodifiedDataBatch finished|' + system.now() + '|' + deletedOpptyIds.size());
    }
 }