@isTest(SeeAllData=false) 
public class AllOrderSubmissionFailuresTriggerTest
 {
    static testMethod void allOrderSubmissionFailuresTriggerHandlerTest() 
    {
         AllOrderSubmissionFailuresTriggerHandler  allOrdSubFailHdlrDtls = new AllOrderSubmissionFailuresTriggerHandler();
         Map<Id,Order_Submission_Failures__c> ordSubFailMap = new Map<Id,Order_Submission_Failures__c>();
         Map<id,String>serviceToErrorMap=new Map<id,String>();
         String casesub='Service is in RAG Red state.';
         String caseStatus='new';
         
        Test.startTest();
        allOrdSubFailHdlrDtls.isDisabled();
        allOrdSubFailHdlrDtls.getName();
        
         //getorder(Integer totalRecords, List<csord__Order_Request__c> OrdReqList){
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Case> caseList = P2A_TestFactoryCls.getcase(1,accList);
        List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1, orderRequestList);
        List<csord__Order__c> orderList = P2A_TestFactoryCls.getorder(1, orderRequestList);
        List<csord__service__c> servList =P2A_TestFactoryCls.getService(1,orderRequestList,subList);        
        servList[0].csord__order__c=orderList[0].id;
        servList[0].product_id__c='allu';
        update servList;
        system.assert(servList!=null);
        caseList[0].subject = casesub;
        caseList[0].status = caseStatus;
        caseList[0].CS_Service__c = servList[0].id;
        
        update caseList;
        system.assert(caseList!=null);
        /*
        List<Order_Submission_Failures__c> ordSubFailList = P2A_TestFactoryCls.getOrderSubFailure(1,orderList);
        // public static List<Order_Submission_Failures__c> getOrderSubFailure(Integer totalRecords, List<csord__Order__c> OrderListFail) {
        ordSubFailList[0].Failed_Order_Line_Item__c=servList[0].id;
        ordSubFailList[0].Error_Description__c='failed in CCMS';
        update ordSubFailList;
        */
        List<Order_Submission_Failures__c> ordSubFailList =new List<Order_Submission_Failures__c>();
        Order_Submission_Failures__c ordSubFail = new Order_Submission_Failures__c();
        ordSubFail.Error_Description__c = 'Test Failure';
        //ordSubFail.Order__c = orderList[0].Id;    
        
        ordSubFail.Order__c =orderList[0].Id;        
        //ordSubFail.Failed_Order_Line_Item__c=servList[0].id;
        ordSubFail.Error_Description__c='failed in CCMS';        
        insert ordSubFail;
        ordSubFailList.add(ordSubFail);
        ordSubFailList[0].Failed_Order_Line_Item__c=servList[0].id;
                
        serviceToErrorMap.put(ordSubFailList[0].Failed_Order_Line_Item__c,ordSubFailList[0].Error_Description__c);
        
        for(Order_Submission_Failures__c ordSubF : ordSubFailList) {
            ordSubFailMap.put(ordSubF.Id,ordSubF);
        }
      
        allOrdSubFailHdlrDtls.afterInsert(ordSubFailList, ordSubFailMap);
        allOrdSubFailHdlrDtls.afterUpdate(ordSubFailList, ordSubFailMap, ordSubFailMap);
          
    }
    @istest
    public static void testcatchblock()
    {
       AllOrderSubmissionFailuresTriggerHandler  allOrdSubFailHdlrDtls1 = new AllOrderSubmissionFailuresTriggerHandler();
       try
       {
         allOrdSubFailHdlrDtls1.afterInsert(null,null);
       }catch(Exception e){}
       try
       {
         allOrdSubFailHdlrDtls1.afterUpdate(null,null,null);
       }catch(Exception e){}
       try
       {
         allOrdSubFailHdlrDtls1.OrderSubFailureMethod(null);
       }catch(Exception e){}
       
        system.assertEquals(true,allOrdSubFailHdlrDtls1!=null);  
    
    }
 }