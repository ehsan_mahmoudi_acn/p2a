public class FieldPicklistValueGenerator implements FieldValueGenerator {
    
    public Boolean canGenerateValueFor(Schema.DescribeFieldResult fieldDesc) {
        return Schema.DisplayType.PICKLIST == fieldDesc.getType();
    }
    
    public Object generate(Schema.DescribeFieldResult fieldDesc) {
        List<Schema.PicklistEntry> values = fieldDesc.getPicklistValues();
        if (values.isEmpty()) {
            return null;
        }
        return values.get(0).getValue();
    }

}