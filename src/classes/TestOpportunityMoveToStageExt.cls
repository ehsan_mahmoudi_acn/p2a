/*
@author Hengky I. Djapar <hengky.djapar@team.telstra.com>
@date 13 Dec 2017
@description Test class for OpportunityMoveToStageExt class.
*/

@isTest(SeeAllData = false)
private class TestOpportunityMoveToStageExt { 

    /*
     *@description Unit Test class for OpportunityMoveToStageExt
     */
    static testMethod void testSaveStageScenario1(){ 
        List<User> users = P2A_TestFactoryCls.get_Users(1);

        List<Account> accs = P2A_TestFactoryCls.getAccounts(1);
        for (Account acc : accs) {
            acc.Customer_Legal_Entity_Name__c = acc.Name;
            acc.Telstra_Legal_Entity_Name__c = 'Telstra Limited';
        }
        update(accs);
        System.assertEquals(accs.size(), [SELECT Id FROM Account].size(), 'ERROR on inserting Account records.');

        List<Contact> contacts = P2A_TestFactoryCls.getContact(1, accs);
        System.assertEquals(contacts.size(), [SELECT Id FROM Contact].size(), 'ERROR on inserting Contact records.');

        List<Opportunity> opps = P2A_TestFactoryCls.getOpportunitys(1, accs);
        System.assertEquals(1, opps.size());

        Opportunity opp = opps.get(0);
        System.assert(opp.Id != null);

        opp.StageName = 'Identify & Define';
        opp.Stage__c = 'Identify & Define';
        opp.Sales_Status__c = 'Open';
        opp.Quote_Simplification__c = true;
        opp.Product_Type__c = 'BOX';
        opp.Estimated_MRC__c = 100;
        update(opp);
        

        Test.setCurrentPage(Page.OpportunityMoveToStage);

        Test.startTest();
            
        System.runAs(users.get(0)) {

            ApexPages.StandardController con = new ApexPages.StandardController(opp);
            OpportunityMoveToStageExt conExt = new OpportunityMoveToStageExt(con);

            // move to qualify
            PageReference resultPf = conExt.SaveStage();
            System.assert(conExt.oppt.StageName == 'Qualify' && conExt.errorMsg == '', 'Opportunity didn\'t move to Qualify as expected.');
            
            // move to develop
            con = new ApexPages.StandardController(conExt.oppt);
            conExt = new OpportunityMoveToStageExt(con);
            resultPf = conExt.SaveStage();
            System.assert(conExt.oppt.StageName == 'Develop' && conExt.errorMsg == '', 'Opportunity didn\'t move to Develop as expected.');

            // move to Propose
            con = new ApexPages.StandardController(conExt.oppt);
            conExt = new OpportunityMoveToStageExt(con);
            resultPf = conExt.SaveStage();
            System.assert(conExt.oppt.StageName == 'Propose' && conExt.errorMsg == '', 'Opportunity didn\'t move to Propose as expected.');

            // move to Prove & Close
            conExt.oppt.Win_Loss_Reasons__c = 'Price';

            /*
            PB-3 Release 1B. Validation on moving to Prove & Close.
            The below validation rules will be moved to run when the Opportunity Stage is changes from Propose to Prove & Close
            - 'Customer Primary Contact' & 'Telstra Global Technical Sales Contact' are not blank.
            - 'Date of Order/Contract Sign Date' is entered and 'Upload signed order/Contract' is checked if 'Signed Contract/Order Received'' is Yes.
            - 'Firm Quote Acceptance date' is entered and 'Email Approval Uploaded' is checked if the 'Pre-Contract Provisioning' is Yes.
            - Either 'Signed Contract/Order Received' or 'Pre-Contract Provisioning Required' is set to 'Yes'.
            - 'Telstra Contracting Entity' & 'Telstra Legal Entity Name' fields from Account must NOT be blank.
            */
            conExt.oppt.Customer_Primary_Contact__c = contacts.get(0).Id;
            conExt.oppt.Technical_Sales_Contact__c = UserInfo.getUserId();

            conExt.oppt.Signed_Contract__c = 'Yes';
            conExt.oppt.Signed_Contract_Order_form_Recieved_Date__c = Date.today(); // cannot be future date (custom validation rule)
            conExt.oppt.Upload_signed_order_Contract__c = true;

            conExt.oppt.Pre_Contract_Provisioning_Required__c = 'Yes';
            conExt.oppt.Email_Approval_Uploaded__c = true;
            conExt.oppt.Quote_Acceptance_Date__c = Date.today(); // cannot be future date (custom validation rule)

            con = new ApexPages.StandardController(conExt.oppt);
            conExt = new OpportunityMoveToStageExt(con);
            resultPf = conExt.SaveStage();
            System.assert(conExt.oppt.StageName == 'Prove & Close' && conExt.errorMsg == '', 'Opportunity didn\'t move to Prove & Close as expected.');

        }

        Test.stopTest();
    }
    
}