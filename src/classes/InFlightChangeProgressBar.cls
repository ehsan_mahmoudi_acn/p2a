public class InFlightChangeProgressBar{

    public AsyncApexJob job{get; set;}
    public Id basketId{get; set;}
    public String isFinished{get; set;}
    public String percent{get; set;}

    public InFlightChangeProgressBar(ApexPages.StandardController ctrl){
        basketId = ctrl.getId();
    }

    public void refreshJobStatus(){
        cscfga__product_basket__c baskets = [
            Select Id, Name, InFlight_Change_Batch_Job_Id__c
            From cscfga__product_basket__c
            Where Id =:basketId Limit 1
        ];
        if(baskets != null && baskets.InFlight_Change_Batch_Job_Id__c != null && baskets.InFlight_Change_Batch_Job_Id__c != '') {
             job = [
                Select Id, JobItemsProcessed, NumberOfErrors, Status, TotalJobItems
                From AsyncApexJob
                Where Id = :baskets.InFlight_Change_Batch_Job_Id__c Limit 1
            ];
        }
        percent = job.TotalJobItems > 0? String.ValueOf((Integer.ValueOf(job.JobItemsProcessed)*100)/Integer.ValueOf(job.TotalJobItems)) : '0';
    }
    
    public String getBatchOrderStatus(){
        if(job != null){
            if(job.Status == 'Preparing' || job.Status == 'Holding' || job.Status == 'Queued'){
                return 'InFlight process is in progress.';
            }
            else if(job.Status == 'Completed' && percent == '100'){
                isFinished = 'Completed';
                return 'Completed';
            } 
            else if(job.TotalJobItems > 0 && job.Status == 'Processing'){
                percent = String.ValueOf((Integer.ValueOf(job.JobItemsProcessed)*100)/Integer.ValueOf(job.TotalJobItems));
                return String.ValueOf((Integer.ValueOf(job.JobItemsProcessed)*100)/Integer.ValueOf(job.TotalJobItems)) +'%';
            } 
            else if(job.Status == 'Failed'){
                return 'Error in - ' + job.NumberOfErrors + '/' + job.TotalJobItems + ' batches.';
            } 
            else if(job.Status == 'Aborted'){
                return 'Inflight cloning process has been Aborted.';
            } else{
                return '';
            }               
        } else{
            return '';
        }
    }
}