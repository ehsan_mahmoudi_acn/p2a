<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>BillProfile__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>BillProfile</label>
        <referenceTo>BillProfile__c</referenceTo>
        <relationshipLabel>CostCentres</relationshipLabel>
        <relationshipName>CostCentres</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Bill_Profile_Integration_Number__c</fullName>
        <description>This field is using for integration. This will be populating unique identifier of Bill Profile</description>
        <externalId>false</externalId>
        <formula>BillProfile__r.Bill_Profile_Integration_Number__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Bill Profile Integration Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cost_Centre_Code__c</fullName>
        <externalId>false</externalId>
        <label>Cost Centre Code</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cost_Centre_ID__c</fullName>
        <description>SFDC generated unique identifier for Cost Center record</description>
        <displayFormat>COST-{00000000}</displayFormat>
        <externalId>true</externalId>
        <label>Cost Centre ID</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>AutoNumber</type>
    </fields>
    <fields>
        <fullName>Cost_Centre_Integration_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Cost_Centre_ID__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Cost Centre Integration Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cost_Centre_Status__c</fullName>
        <description>Cost Centre Status</description>
        <externalId>false</externalId>
        <label>Cost Centre Status</label>
        <picklist>
            <picklistValues>
                <fullName>Draft</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Active</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Obsolete</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>External_ID__c</fullName>
        <description>External cost centre ID from legacy and external system</description>
        <externalId>true</externalId>
        <label>Cost Centre Legacy ID</label>
        <length>30</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PO_Number_External_Ref__c</fullName>
        <externalId>false</externalId>
        <label>PO Number</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sent_To_TIBCO__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Automatically checks when the record send to TIBCO for first time.</description>
        <externalId>false</externalId>
        <label>Sent To TIBCO</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Symphona_Delete__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Symphona Delete?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>CostCentre</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Cost Centre Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>CostCentres</pluralLabel>
    <searchLayouts/>
    <sharingModel>Read</sharingModel>
    <validationRules>
        <fullName>Costcenter_Validation_BP_Active_Approve</fullName>
        <active>true</active>
        <description>User able to create the cost center, if the bill profile is Active or Approved only..</description>
        <errorConditionFormula>AND(
AND (
NOT(ISPICKVAL( BillProfile__r.Status__c , &apos;Active&apos;)),
NOT(ISPICKVAL( BillProfile__r.Status__c , &apos;Approved&apos;))),
Sent_To_TIBCO__c == FALSE)</errorConditionFormula>
        <errorDisplayField>BillProfile__c</errorDisplayField>
        <errorMessage>Costcentre can be created only when the bill profiles status is “Active”,  or “Approved”.</errorMessage>
    </validationRules>
</CustomObject>
