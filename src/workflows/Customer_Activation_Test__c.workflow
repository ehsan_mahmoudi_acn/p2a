<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_RecordType</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CATlayout</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change RecordType</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Region_Update</fullName>
        <field>Region__c</field>
        <formula>TEXT(Account__r.Region__c)</formula>
        <name>Region Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Region</fullName>
        <actions>
            <name>Region_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Account__c  &lt;&gt; Null</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Record type</fullName>
        <actions>
            <name>Change_RecordType</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Order__c  &lt;&gt; Null</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
