<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_OLA</fullName>
        <field>OLA__c</field>
        <formula>IF(  OR(ISNULL(CSPOFA__Orchestration_Process__r.csordtelcoa__Service__r.OLA__c),CSPOFA__Orchestration_Process__r.csordtelcoa__Service__r.OLA__c = 0)   = TRUE,  CSPOFA__Orchestration_Step_Template__r.Estimated_Time_to_Complete__c , CSPOFA__Orchestration_Process__r.csordtelcoa__Service__r.OLA__c)</formula>
        <name>Set OLA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Set OLA</fullName>
        <actions>
            <name>Set_OLA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CSPOFA__Orchestration_Step__c.Name</field>
            <operation>equals</operation>
            <value>Design &amp; Implement &amp; Test</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
