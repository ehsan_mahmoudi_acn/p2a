<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
	<alerts>
        <fullName>Email_to_Order_Owner_On_Termination_Order_Creation</fullName>
        <description>Email to Order Owner On Termination Order Creation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Termination_Order_Created</template>
    </alerts>
    <alerts>
        <fullName>Email_For_Reject</fullName>
        <description>Email is sent to Opportunity Owner for  Commercial Order Rejecton</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Notify_opportunity_owner_for_order_rejection</template>
    </alerts>
    <alerts>
        <fullName>Notify_Service_Order_Owner_if_the_Customer_Required_Termination_date_is_postpone</fullName>
        <description>Notify Service Order Owner if the Customer Required Termination date is postponed.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Notify_Service_Order_Owner_if_the_Customer_Required_Terminationdate_is_postponed</template>
    </alerts>
    <alerts>
        <fullName>Notify_Service_Order_Owner_if_the_Customer_Required_date_is_postponed</fullName>
        <description>Notify Service Order Owner if the Customer Required date is postponed.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Notify_Service_Order_Owner_s_if_the_Customer_Required_date_is_postponed</template>
    </alerts>
	<alerts>
        <fullName>Email_Notification_for_SD_OM_Contact</fullName>
        <description>Email Notification for SD OM Contact</description>
        <protected>false</protected>
        <recipients>
            <field>SD_OM_Contact__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Email_Notification_for_SD_PM_OM_Contact</template>
    </alerts>
	<alerts>
        <fullName>Email_Notification_for_SD_PM_Contact</fullName>
        <description>Email Notification for SD PM Contact</description>
        <protected>false</protected>
        <recipients>
            <field>SD_PM_Contact__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Email_Notification_for_SD_PM_OM_Contact</template>
    </alerts>
    <fieldUpdates>
        <fullName>CRD_is_changed</fullName>
        <field>CRD_changed__c</field>
        <literalValue>1</literalValue>
        <name>CRD is changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Rec_Type_as_SD_Cease_Order</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Cease_Order_for_SD</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Rec Type as SD Cease Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Rec_Type_as_SD_Cease_Order1</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Cease_Order_for_SD</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Rec Type as SD Cease Order1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Rec_Type_as_SD_Cease_Order2</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Cease_Order_for_SD</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Rec Type as SD Cease Order2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Rec_Type_as_SD_Cease_Order3</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Cease_Order_for_SD</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Rec Type as SD Cease Order3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Rec_Type_as_SD_Cease_Order4</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Cease_Order_for_SD</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Rec Type as SD Cease Order4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FieldUpdate_Order_RequiredStartDate</fullName>
        <field>RequiredStartDate_del__c</field>
        <literalValue>1</literalValue>
        <name>FieldUpdate_Order_RequiredStartDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Rejected_Order</fullName>
        <field>Reject_Order__c</field>
        <literalValue>1</literalValue>
        <name>Field Rejected Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_to_SD_Assigned_Date</fullName>
        <field>SD_OM_Assigned_Date__c</field>
        <formula>Today()</formula>
        <name>Field Update to SD Assigned Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_to_SD_PM_Assigned_Date</fullName>
        <field>SD_PM_Assigned_Date__c</field>
        <formula>TODAY()</formula>
        <name>Field Update to SD PM Assigned Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_to_Technical_Specialist_Ass</fullName>
        <field>Technical_Specialist_Assigned_Date__c</field>
        <formula>TODAY()</formula>
        <name>Field Update to Technical Specialist Ass</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_update</fullName>
        <field>csord__Status2__c</field>
        <formula>TEXT( Status__c )</formula>
        <name>Field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ISO_Engineering_account</fullName>
        <field>OwnerId</field>
        <lookupValue>Engineering</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>ISO Engineering account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ISO_IT_account</fullName>
        <field>OwnerId</field>
        <lookupValue>IT</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>ISO IT account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ISO_Product_account</fullName>
        <field>OwnerId</field>
        <lookupValue>Product</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>ISO Product account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ISO_Sales_Ops_account</fullName>
        <field>OwnerId</field>
        <lookupValue>Sales_Ops</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>ISO Sales Ops account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ISO_Sales_account</fullName>
        <field>OwnerId</field>
        <lookupValue>Sales</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>ISO Sales account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ISO_Service_Delivery_account</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>ISO Service Delivery account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ISO_System_account</fullName>
        <field>OwnerId</field>
        <lookupValue>System</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>ISO System account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Accepted_Date_FieldUpdate</fullName>
        <field>Order_Accepted_Date__c</field>
        <formula>NOW()</formula>
        <name>Order Accepted Date FieldUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Completed_Date_FieldUpdate</fullName>
        <field>Order_Completion_Date__c</field>
        <formula>NOW()</formula>
        <name>Order Completed Date FieldUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Enrichment_Australia</fullName>
        <field>OwnerId</field>
        <lookupValue>Order_Enrichment_Australia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Order Enrichment Australia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Enrichment_EMEA</fullName>
        <field>OwnerId</field>
        <lookupValue>Order_Enrichment_EMEA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Order Enrichment EMEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Enrichment_North_Asia</fullName>
        <field>OwnerId</field>
        <lookupValue>Order_Enrichment_North_Asia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Order Enrichment North Asia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Enrichment_South_Asia</fullName>
        <field>OwnerId</field>
        <lookupValue>Order_Enrichment_South_Asia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Order Enrichment South Asia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Enrichment_USA</fullName>
        <field>OwnerId</field>
        <lookupValue>Order_Enrichment_USA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Order Enrichment USA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Owner_SD_Queue_North_Asia</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_North_Asia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Order Owner SD Queue North Asia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Submitted_Field_update</fullName>
        <field>Order_Submitted_Date__c</field>
        <formula>Now()</formula>
        <name>Order Submitted Field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Opportunity_Share_field_update</fullName>
        <field>Is_opportunity_share__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity Share field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>P2A_Service_Delivery_US</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_US</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>P2A Service Delivery US</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetCleanOrderDate</fullName>
        <field>Clean_Order_Date__c</field>
        <formula>TODAY()</formula>
        <name>SetCleanOrderDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SubmitOrder</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Inflight_Order</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>SubmitOrder</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SubmitOrderTerminate</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Teminate</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>SubmitOrderTerminate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateOrderType</fullName>
        <field>Order_Type__c</field>
        <formula>Text(csordtelcoa__Opportunity__r.Order_Type__c)</formula>
        <name>UpdateOrderType</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Order_Name_to_Account</fullName>
        <field>Name</field>
        <formula>&quot;ODR - &quot; &amp; csord__Account__r.Name</formula>
        <name>Update the Order Name to Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Order_Name_to_same_as_Opportu</fullName>
        <field>Name</field>
        <formula>&quot;ODR - &quot;  &amp;  csord__Order_Number__c</formula>
        <name>Update the Order Name to same as Opportu</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OrderType</fullName>
        <field>Order_Type__c</field>
        <formula>IF(csord__Order_Type__c == &apos;Terminate&apos;, csord__Order_Type__c, Subscription__r.OrderType__c)</formula>
        <name>OrderType</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OrderTypeManaged</fullName>
        <field>csord__Order_Type__c</field>
        <formula>IF(csord__Order_Type__c == &apos;Terminate&apos;, csord__Order_Type__c, IF(Subscription__r.OrderType__c == &apos;New Provide&apos;, &apos;Subscription Creation&apos;, &apos;Subscription Change&apos;))</formula>
        <name>OrderTypeManaged</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Partially_Terminated_Order</fullName>
        <field>Partial_Terminate_Order__c</field>
        <literalValue>1</literalValue>
        <name>Partially Terminated Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_Assign_to_SD</fullName>
        <field>RecordTypeId</field>
        <lookupValue>csord__Cease_Order</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type - Assign to SD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_Order_Submitted</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Teminate</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type - Order Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_Order_Submitted_to_SD</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Cease_Order_for_SD</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type - Order Submitted to SD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_the_customer_Type</fullName>
        <field>Customer_Type__c</field>
        <formula>TEXT(csord__Account__r.Customer_Type_New__c)</formula>
        <name>update the customer Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	    <fieldUpdates>
        <fullName>Billing_team_notified</fullName>
        <field>Billing_Team_Notified__c</field>
        <literalValue>1</literalValue>
        <name>Billing team notified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>IS_SUBMITTED_FALSE</fullName>
        <description>makes the checkbox false</description>
        <field>Is_Order_Submitted__c</field>
        <literalValue>0</literalValue>
        <name>IS_SUBMITTED_FALSE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>update_date</fullName>
        <field>Suspend_due_to_Supplier_end_date__c</field>
        <formula>TODAY()</formula>
        <name>update date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_end_date</fullName>
        <field>Suspend_due_to_customer_end_date__c</field>
        <formula>TODAY()</formula>
        <name>update end date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_end_date_internal</fullName>
        <field>Suspend_due_to_Internal_end_date__c</field>
        <formula>TODAY()</formula>
        <name>update end date - internal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_start_date</fullName>
        <field>Suspend_due_to_customer_start_date__c</field>
        <formula>TODAY()</formula>
        <name>update start date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_start_date_Internal</fullName>
        <field>Suspend_due_to_Internal_start_date__c</field>
        <formula>TODAY()</formula>
        <name>update start date- Internal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_start_date_Supplier</fullName>
        <field>Suspend_due_to_Supplier_start_date__c</field>
        <formula>TODAY()</formula>
        <name>update start date- Supplier</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Is_submitted_to_False_When_Order_Failed</fullName>
        <actions>
            <name>IS_SUBMITTED_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>csord__Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Failed</value>
        </criteriaItems>
        <description>The workflow will update the Is_Submitted checkbox to false when the Order status updates to Failed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>update the customer Type</fullName>
        <actions>
            <name>update_the_customer_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>True</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>	
    <rules>
        <fullName>Set Record Type - Assign to SD</fullName>
        <actions>
            <name>Partially_Terminated_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Record_Type_Assign_to_SD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Record Type of partially terminated order - Assign to SD</description>
        <formula>AND(Subscription__r.Cease_Line_Items__c != 0, Subscription__r.Cease_Line_Items__c != Subscription__r.Count_of_services__c, Order_Submitted_to_SD__c == false,  Is_Order_Submitted__c == false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Record Type - Order Submitted</fullName>
        <actions>
            <name>Set_Record_Type_Order_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Record Type of partially terminated order - Order Submitted</description>
        <formula>AND(Subscription__r.Cease_Line_Items__c != 0, Subscription__r.Cease_Line_Items__c != Subscription__r.Count_of_services__c, Order_Submitted_to_SD__c == true,  Is_Order_Submitted__c == true)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Record Type - Order Submitted to SD</fullName>
        <actions>
            <name>Set_Record_Type_Order_Submitted_to_SD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Record Type of partially terminated order - Order Submitted to SD</description>
        <formula>AND(Subscription__r.Cease_Line_Items__c != 0, Subscription__r.Cease_Line_Items__c != Subscription__r.Count_of_services__c, Order_Submitted_to_SD__c == true,  Is_Order_Submitted__c == false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SetOrderType</fullName>
        <actions>
            <name>OrderType</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OrderTypeManaged</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(csord__Order_Type__c = &apos;Terminate&apos;,Subscription__c != null)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
	
    <rules>
        <fullName>CRD is changed</fullName>
        <actions>
            <name>CRD_is_changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Customer_Required_Date__c ) &amp;&amp;  Is_Order_Submitted__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ISO Engineering account</fullName>
        <actions>
            <name>ISO_Engineering_account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>And(
ISPICKVAL( csord__Account__r.Customer_Type_New__c , &apos;ISO&apos;),
 csord__Account__r.Name = &apos;Engineering&apos;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISO IT account</fullName>
        <actions>
            <name>ISO_IT_account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>And(
ISPICKVAL( csord__Account__r.Customer_Type_New__c , &apos;ISO&apos;),
 csord__Account__r.Name = &apos;IT&apos;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISO Product account</fullName>
        <actions>
            <name>ISO_Product_account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>And(
ISPICKVAL( csord__Account__r.Customer_Type_New__c , &apos;ISO&apos;),
 csord__Account__r.Name = &apos;Product&apos;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISO Sales Ops account</fullName>
        <actions>
            <name>ISO_Sales_Ops_account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>And(
ISPICKVAL( csord__Account__r.Customer_Type_New__c , &apos;ISO&apos;),
 csord__Account__r.Name = &apos;Sales Ops&apos;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISO Sales account</fullName>
        <actions>
            <name>ISO_Sales_account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>And(
ISPICKVAL( csord__Account__r.Customer_Type_New__c , &apos;ISO&apos;),
 csord__Account__r.Name = &apos;Sales&apos;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISO Service Delivery account</fullName>
        <actions>
            <name>ISO_Service_Delivery_account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>And(
ISPICKVAL( csord__Account__r.Customer_Type_New__c , &apos;ISO&apos;),
 csord__Account__r.Name = &apos;Service Delivery&apos;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISO System account</fullName>
        <actions>
            <name>ISO_System_account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>And(
ISPICKVAL( csord__Account__r.Customer_Type_New__c , &apos;ISO&apos;),
 csord__Account__r.Name = &apos;System&apos;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
  
    <rules>
        <fullName>Notify Service Order Owners if CR date postponed%2E</fullName>
        <actions>
            <name>Notify_Service_Order_Owner_if_the_Customer_Required_date_is_postponed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>notify all Service Order Owners if the Customer Required date is postponed by the Customer.</description>
        <formula>AND(
Text(PRIORVALUE(Customer_Required_Date__c))&lt;&gt;&apos;&apos;,
ISCHANGED(Customer_Required_Date__c),
 Is_Terminate_Order__c &lt;&gt; true,Is_Order_Submitted__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Service Order Owners if CRT date postponed%2E</fullName>
        <actions>
            <name>Notify_Service_Order_Owner_if_the_Customer_Required_Termination_date_is_postpone</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>notify all Service Order Owners if the Customer Required Termination date is postponed by the Customer.</description>
        <formula>AND(ISCHANGED(Requested_Termination_Date__c) &amp;&amp;  Is_Terminate_Order__c  = true)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify opportunity owner that order is rejected</fullName>
        <actions>
            <name>Email_For_Reject</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Field_Rejected_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( is_Rejected_Button_clicked__c, ISPICKVAL(Status__c, &apos;Rejected&apos;),! Reject_Order__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    
    <rules>
        <fullName>Order Enrichment Australia</fullName>
        <actions>
            <name>Order_Enrichment_Australia</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(csord__Account__r.Region__c,&apos;Australia&apos;) &amp;&amp;  NOT(Is_Terminate_Order__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order Enrichment EMEA</fullName>
        <actions>
            <name>Order_Enrichment_EMEA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(csord__Account__r.Region__c,&apos;EMEA&apos;) &amp;&amp;  NOT(Is_Terminate_Order__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order Enrichment North Asia</fullName>
        <actions>
            <name>Order_Enrichment_North_Asia</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(csord__Account__r.Region__c,&apos;North Asia&apos;) &amp;&amp;  NOT(Is_Terminate_Order__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order Enrichment South Asia</fullName>
        <actions>
            <name>Order_Enrichment_South_Asia</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(csord__Account__r.Region__c,&apos;South Asia&apos;) &amp;&amp;  NOT(Is_Terminate_Order__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order Enrichment US</fullName>
        <actions>
            <name>Order_Enrichment_USA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(csord__Account__r.Region__c,&apos;US&apos;) &amp;&amp;  NOT(Is_Terminate_Order__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Populate SD OM Assigned Date</fullName>
        <actions>
            <name>Email_Notification_for_SD_OM_Contact</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Field_Update_to_SD_Assigned_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(SD_OM_Contact__c), SD_OM_Contact__c!= NULL)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate SD PM Assigned Date</fullName>
        <actions>
            <name>Email_Notification_for_SD_PM_Contact</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Field_Update_to_SD_PM_Assigned_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(SD_PM_Contact__c ), SD_PM_Contact__c!= NULL)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Technical Specialist Assigned Date</fullName>
        <actions>
            <name>Field_Update_to_Technical_Specialist_Ass</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>PRIORVALUE( Technical_Specialist__c) = &apos;&apos; &amp;&amp; ISCHANGED(Technical_Specialist__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SetCleanOrderDate</fullName>
        <actions>
            <name>SetCleanOrderDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Order__c.Clean_Order_Check_Passed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status value update</fullName>
        <actions>
            <name>Field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Order__c.Status__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SubmitOrder</fullName>
        <actions>
            <name>SubmitOrder</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Order__c.Is_Order_Submitted__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Order__c.Is_Terminate_Order__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SubmitOrderTerminate</fullName>
        <actions>
            <name>SubmitOrderTerminate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Order__c.Is_Order_Submitted__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Order__c.Is_Terminate_Order__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update the Order Name to same as Account Name for ISO</fullName>
        <actions>
            <name>Update_the_Order_Name_to_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(Name &lt;&gt; Null,TEXT(csord__Account__r.Customer_Type_New__c) = &apos;ISO&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update the Order Name to same as Opportunity Name</fullName>
        <actions>
            <name>Update_the_Order_Name_to_same_as_Opportu</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(Name &lt;&gt; Null, 
Not( TEXT(csord__Account__r.Customer_Type_New__c) = &apos;ISO&apos;), 
Is_Terminate_Order__c = false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Workflow_Order_StartDate</fullName>
        <actions>
            <name>FieldUpdate_Order_RequiredStartDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( csord__Start_Date__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Share</fullName>
        <actions>
            <name>Opportunity_Share_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(Is_opportunity_share__c==false,NOT(ISNEW()))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>	
	 <rules>
        <fullName>Set Billing team notified</fullName>
        <actions>
            <name>Billing_team_notified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Order__c.Handover_Complete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
	<rules>
        <fullName>Updated suspension End date - Internal</fullName>
        <actions>
            <name>update_end_date_internal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISPICKVAL( PRIORVALUE( Status__c) ,&apos;Suspended – due to internal&apos;),OR(ISPICKVAL(Status__c , &apos;New&apos;) , ISPICKVAL(Status__c , &apos;Accepted&apos;),ISPICKVAL(Status__c , &apos;In-Progress&apos;),ISPICKVAL(Status__c , &apos;Submitted&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Updated suspension End date - Supplier</fullName>
        <actions>
            <name>update_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISPICKVAL( PRIORVALUE( Status__c) ,&apos;Suspended – due to supplier&apos;),OR(ISPICKVAL(Status__c , &apos;New&apos;) , ISPICKVAL(Status__c , &apos;Accepted&apos;),ISPICKVAL(Status__c , &apos;In-Progress&apos;),ISPICKVAL(Status__c , &apos;Submitted&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Updated suspension end date - customer</fullName>
        <actions>
            <name>update_end_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISPICKVAL( PRIORVALUE( Status__c) ,&apos;Suspended – due to customer&apos;),OR(ISPICKVAL(Status__c , &apos;New&apos;) , ISPICKVAL(Status__c , &apos;Accepted&apos;),ISPICKVAL(Status__c , &apos;In-Progress&apos;),ISPICKVAL(Status__c , &apos;Submitted&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Updated suspension start date - Internal</fullName>
        <actions>
            <name>update_start_date_Internal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Suspended – due to internal</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Updated suspension start date - Supplier</fullName>
        <actions>
            <name>update_start_date_Supplier</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Suspended – due to supplier</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Updated suspension start date - customer</fullName>
        <actions>
            <name>update_start_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Suspended – due to customer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
