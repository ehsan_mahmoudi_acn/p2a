<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
<alerts>
        <fullName>E_mail_to_Case_owner</fullName>
        <description>E-mail to Case owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SUPPORTSelfServiceNewCommentNotificationSAMPLE</template>
    </alerts>
	<alerts>
        <fullName>Pricing_Approval_Case_Email_Template</fullName>
        <description>Pricing Approval Case Email Template</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Pricing_Approval_Case_Email_Template</template>
    </alerts>
	<alerts>
        <fullName>RR_Email_Notification</fullName>
        <description>RR Email Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>X3PQ_Email_Templates/RR_Email_Notification_Template_HTML</template>
    </alerts>
    <alerts>
        <fullName>Case_Owner_Changed</fullName>
        <description>Case Owner Changed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Case_Owner_Changed_HTML</template>
    </alerts>
    <alerts>
        <fullName>ETC_Request_Cancelled</fullName>
        <description>ETC Request Cancelled</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Case_Assigned_ETC_Cancelled</template>
    </alerts>
    <alerts>
        <fullName>ETC_Request_To_AcctOnwer</fullName>
        <description>ETC Request To AcctOnwer</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Case_Assigned_ETC_AcctOwner</template>
    </alerts>
	<alerts>
        <fullName>Email_alert_to_the_creator</fullName>
        <description>Email alert to the creator</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Account_Activation</template>
    </alerts>
    <alerts>
        <fullName>ETC_Request_To_Billing</fullName>
        <description>ETC Request To Billing</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Case_Assigned_ETC_Billing</template>
    </alerts>
    <alerts>
        <fullName>ETC_Request_To_Sales</fullName>
        <description>ETC Request To Sales</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Case_Assigned_ETC_Sales</template>
    </alerts>
    <alerts>
        <fullName>Email_Account_Owner_Enrichment_Completed</fullName>
        <description>Email Account Owner Enrichment Completed</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Order_Enrichment/Order_Enrichment_Completed_HTML</template>
    </alerts>
    <alerts>
        <fullName>Email_Owner_Enrichment_Overdue</fullName>
        <description>Email Owner Enrichment Overdue</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Order_Enrichment/Order_Enrichment_Overdue_HTML</template>
    </alerts>
    <alerts>
        <fullName>ExpirationDate</fullName>
        <description>Expiration Date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Expiration_Date_HTML</template>
    </alerts>
    <alerts>
        <fullName>GCPE_field_values_updated</fullName>
        <description>GCPE field values updated.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>X3PQ_Email_Templates/assigning_for_Update_Supplier_Quote</template>
    </alerts>
    <alerts>
        <fullName>Local_Loop_field_values_updated</fullName>
        <description>Local Loop field values updated.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>X3PQ_Email_Templates/assigning_for_Update_Supplier_Quote</template>
    </alerts>
    <alerts>
        <fullName>Notify_Billing</fullName>
        <description>Notify Billing</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/BillingStageGateRequest</template>
    </alerts>
    <alerts>
        <fullName>Notify_Opportunity_owner_enrichment_rejected</fullName>
        <description>Notify Opportunity owner enrichment rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Order_Enrichment/Order_Enrichment_Rejected_HTML</template>
    </alerts>
    <alerts>
        <fullName>Notify_the_supplier_quote</fullName>
        <description>Notify the supplier quote</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>X3PQ_Email_Templates/assigning_for_supplier_quote_HTML</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_is_closed_as_Lost</fullName>
        <description>Opportunity is closed as Lost</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
         <template>X3PQ_Email_Templates/Notify_Supplier_Quote_Case_owner_HTML</template>
    </alerts>
    <alerts>
        <fullName>Pricing_request_Approved_Reject</fullName>
        <description>Pricing request Approved/Reject</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Opportunity_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>X3PQ_Email_Templates/Pricing_Approve_Reject_HTML</template>
    </alerts>
    <alerts>
        <fullName>RR_case_assignment_mail</fullName>
        <description>RR case assignment mail</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>X3PQ_Email_Templates/RR_Case_Owner_Changed_HTML</template>
    </alerts>
    <alerts>
        <fullName>Request_To_ETC_for_Commercial</fullName>
        <description>Request To ETC for Commercial</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Case_Assigned_ETC_Commercial</template>
    </alerts>
    <alerts>
        <fullName>Supplier_Quote_GCPE_Updated_Fields</fullName>
        <description>Supplier Quote GCPE Updated Fields</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>X3PQ_Email_Templates/Request_Suppliers_for_response_on_LocalLoop_HTML</template>
    </alerts>
    <alerts>
        <fullName>Supplier_Quote_LL_Updated_Fields</fullName>
        <description>Supplier Quote LL Updated Fields</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>X3PQ_Email_Templates/Request_Suppliers_for_response_on_GCPE_HTML</template>
    </alerts>
    <alerts>
        <fullName>case_assigned_to_you</fullName>
        <description>case assigned to you.</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Opportunity_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Case_assigned_to_user_HTML</template>
    </alerts>
    <alerts>
        <fullName>notify_the_sales</fullName>
        <description>notify the sales</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>X3PQ_Email_Templates/Notify_Opportunity_owner_when_case_assigned_back_to_them_HTML</template>
    </alerts>
	 <alerts>
        <fullName>Email_notification</fullName>
        <description>Email notification</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Case_status_email_template</template>
    </alerts>
	<alerts>
        <fullName>pricing_case_to_queue</fullName>
        <description>pricing case to queue</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/pricing_case_to_queue_HTML</template>
    </alerts>
	<alerts>
        <fullName>Email_alert_for_owner</fullName>
        <description>Email alert for owner</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Missing_Billing_Info_case_closed</template>
    </alerts>
    <fieldUpdates>
        <fullName>Case_Status</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Case Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Closed</fullName>
        <field>Is_Closed__c</field>
        <literalValue>1</literalValue>
        <name>Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Disable_Send_Overdue_Email_Flag</fullName>
        <description>Once an overdue email has been sent disable the flag which controls the daily send</description>
        <field>Send_Overdue_Email__c</field>
        <literalValue>0</literalValue>
        <name>Disable Send Overdue Email Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GCPE_Rec_Type_after_Renegotiate</fullName>
        <field>RecordTypeId</field>
        <lookupValue>GCPE_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>GCPE Rec Type after Renegotiate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GCPE_Renegotiation_field_update</fullName>
        <field>RecordTypeId</field>
        <lookupValue>GCPE_Renegotiation</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>GCPE Renegotiation field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LL_Rec_Type_after_Renegotiate</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Local_Loop_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>LL Rec Type after Renegotiate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Local_Loop_Renegotiation_field_update</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Local_Loop_Renegotiation</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Local Loop Renegotiation field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_Renegotiation_False</fullName>
        <field>Is_Renegotiation__c</field>
        <literalValue>0</literalValue>
        <name>Mark Renegotiation False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_Renegotiation_False1</fullName>
        <field>Is_Renegotiation__c</field>
        <literalValue>0</literalValue>
        <name>Mark Renegotiation False1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PricingRequestGPDAsia</fullName>
        <field>OwnerId</field>
        <lookupValue>GPD_Asia_ANZ</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>PricingRequestGPDAsia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PricingRequestGPDENT</fullName>
        <field>OwnerId</field>
        <lookupValue>GPD_ENT_US_EMEA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>PricingRequestGPDENT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PricingRequestGPDGW</fullName>
        <field>OwnerId</field>
        <lookupValue>GPD_GW_Case</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>PricingRequestGPDGW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RecordType_Escalated</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Escalate</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RecordType - Escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RecordType_Price_Request</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Price_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RecordType - Price Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RecordType_Price_Request_Escalated</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Price_Request_Escalated</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RecordType - Price Request Escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Enrichment_record_type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Enrichment</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Enrichment record type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Reconfirm_Request</fullName>
        <field>Status</field>
        <literalValue>Reconfirm Request</literalValue>
        <name>Status - Reconfirm Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    
    <fieldUpdates>
        <fullName>Supplier_Quotiing_team_HK</fullName>
        <field>OwnerId</field>
        <lookupValue>Supplier_Quoting_Team_HK</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Supplier Quotiing team HK</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>

    
   <fieldUpdates>
        <fullName>is_success</fullName>
        <field>Feasibility_Study_completed__c</field>
        <literalValue>1</literalValue>
        <name>is success</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>iscomplete</fullName>
        <field>IsComplete__c</field>
        <literalValue>1</literalValue>
        <name>iscomplete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Completed_By</fullName>
        <field>Completed_By__c</field>
        <formula>$User.FirstName +&apos; &apos; +$User.LastName</formula>
        <name>Completed By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Completion_Date</fullName>
        <field>Completion_Date__c</field>
        <formula>NOW()</formula>
        <name>Completion Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<rules>
        <fullName>Pricing Approval Case_Approved</fullName>
        <actions>
            <name>Pricing_Approval_Case_Email_Template</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Approval_Status__c</field>
            <operation>contains</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Pricing_Approval_Case__c</field>
            <operation>contains</operation>
            <value>Order Desk</value>
        </criteriaItems>
        <description>System will also send the below email notification to the Requester ,if Pricing Approval Case can be approved by the Order Desk.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>AutoPopulateFields</fullName>
        <actions>
            <name>Completed_By</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Completion_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>contains</operation>
            <value>Complete,Closed</value>
        </criteriaItems>
        <description>Auto Populate Completed By and Completion date field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
     </rules>
	 <rules>
        <fullName>RR Email Notification</fullName>
        <actions>
            <name>RR_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Is_Resource_Reservation__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Resource Reservation email notification to opportunity owner</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>3PQ Opp Closed Lost</fullName>
        <actions>
            <name>Opportunity_is_closed_as_Lost</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The ability to automatically notify the Supplier Quote Case owner if the associated Opportunity is Closed as Lost.</description>
        <formula>AND(ISPICKVAL( Status , &quot;Cancelled&quot;), 
OR(
 Is_Supplier_Quote_Request__c ,
 Is_Feasibility_Request__c ,
 Is_Resource_Reservation__c 
)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BillingStageGateRequest</fullName>
        <actions>
            <name>Notify_Billing</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>BillingStageGateRequest</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Closed</fullName>
        <actions>
            <name>Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Owner Child Feasibilty</fullName>
        <actions>
            <name>Case_Owner_Changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(Is_Feasibility_Request__c = true, Parent_Feasibility__c&lt;&gt;null)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Successful Feasibility</fullName>
        <actions>
            <name>is_success</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Feasibility_Result__c</field>
            <operation>equals</operation>
            <value>Successful,Conditional Success</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Complete</fullName>
        <actions>
            <name>iscomplete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Complete,Confirmed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Escalated Price Approval Case</fullName>
        <actions>
            <name>RecordType_Price_Request_Escalated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Is_Escalated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Is_Price_Approval_Request__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Approved</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Expiration Date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Is_Resource_Reservation__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ExpirationDate</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.F_Expiration_Date__c</offsetFromField>
            <timeLength>-5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
   
    
    <rules>
        <fullName>Margin %3C 30 %25 - Non Escalated</fullName>
        <actions>
            <name>RecordType_Escalated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>((Margin_In_number__c &lt;0.3 &amp;&amp; Margin_In_number__c  &gt;=0 &amp;&amp; (NOT( CONTAINS( Owner:Queue.QueueName , &apos;Commercial&apos;) )  ||  Owner:User.LastName != &apos;&apos;) ) || (Margin_In_number__c &lt;0.25 &amp;&amp; Margin_In_number__c &gt;=0 &amp;&amp; ( CONTAINS( Owner:Queue.QueueName , &apos;Commercial&apos;) )  )) &amp;&amp;  NOT(Is_Escalated__c) &amp;&amp;  Is_Price_Approval_Request__c &amp;&amp;  Not(ISPICKVAL( Status , &apos;On Hold&apos;)) &amp;&amp;  Not(ISPICKVAL( Status , &apos;Approved&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Margin %3E 30 %25 - Non Escalated</fullName>
        <actions>
            <name>RecordType_Price_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>((Margin_In_number__c &gt;=0.3 &amp;&amp; (NOT( CONTAINS( Owner:Queue.QueueName , &apos;Commercial&apos;) ) || Owner:User.LastName != &apos;&apos;)) || (Margin_In_number__c &gt;=0.25 &amp;&amp; ( CONTAINS( Owner:Queue.QueueName , &apos;Commercial&apos;) ) )) &amp;&amp; NOT(Is_Escalated__c) &amp;&amp; Is_Price_Approval_Request__c &amp;&amp; NOT(ISPICKVAL( Status , &apos;On Hold&apos;)) &amp;&amp; NOT(ISPICKVAL( Status , &apos;Approved&apos;))&amp;&amp; NOT(Is_Reassign_to_Sales__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Order Enrichment Completed</fullName>
        <actions>
            <name>Email_Account_Owner_Enrichment_Completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Enrichment</value>
        </criteriaItems>
        <description>Order enrichment case has been rejected and opportunity owner will be notified by email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Order Enrichment Overdue</fullName>
        <actions>
            <name>Email_Owner_Enrichment_Overdue</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Disable_Send_Overdue_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Send_Overdue_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Daily batch class sets flag on the case when it&apos;s overdue which will trigger this workflow to send the relevant email to assigned user</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Order Enrichment Rejected</fullName>
        <actions>
            <name>Notify_Opportunity_owner_enrichment_rejected</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Enrichment</value>
        </criteriaItems>
        <description>Order enrichment case has been rejected and opportunity owner will be notified by email</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Pricing Approve%2FReject</fullName>
        <actions>
            <name>Pricing_request_Approved_Reject</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Is_Price_Approval_Request__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
		<criteriaItems>
            <field>User.ProfileId</field>
            <operation>notEqual</operation>
            <value>TI Order Desk</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pricing case assigned</fullName>
        <actions>
            <name>case_assigned_to_you</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>And(
 Is_Price_Approval_Request__c ,
 ISCHANGED( Assigned_To__c ) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PricingRequestGPDAsia</fullName>
        <actions>
            <name>PricingRequestGPDAsia</name>
            <type>FieldUpdate</type>
        </actions>
		<actions>
            <name>pricing_case_to_queue</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL( Account_Name__r.Customer_Type_New__c , &apos;MNC&apos;) &amp;&amp; (ISPICKVAL( Account_Name__r.Region__c ,&apos;Australia&apos;) || ISPICKVAL(Account_Name__r.Region__c,&apos;South Asia&apos;) || ISPICKVAL(Account_Name__r.Region__c, &apos;North Asia&apos;)) &amp;&amp;  Is_Price_Approval_Request__c = True &amp;&amp; (ISPICKVAL( Status, &apos;Assigned&apos;) || ISPICKVAL(Status, &apos;Unassigned&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PricingRequestGPDENT</fullName>
        <actions>
            <name>PricingRequestGPDENT</name>
            <type>FieldUpdate</type>
        </actions>
		<actions>
            <name>pricing_case_to_queue</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(  Account_Name__r.Customer_Type_New__c  , &apos;MNC&apos;)  &amp;&amp;  (ISPICKVAL( Account_Name__r.Region__c , &apos;EMEA&apos;) || ISPICKVAL( Account_Name__r.Region__c , &apos;US&apos;) || ISPICKVAL(  Account_Name__r.Region__c  , &apos;Oceania&apos;) ) &amp;&amp; Is_Price_Approval_Request__c = True  &amp;&amp; (ISPICKVAL( Status, &apos;Assigned&apos;) || ISPICKVAL(Status, &apos;Unassigned&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PricingRequestGPDGW</fullName>
        <actions>
            <name>PricingRequestGPDGW</name>
            <type>FieldUpdate</type>
        </actions>
		 <actions>
            <name>pricing_case_to_queue</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL( Account_Name__r.Customer_Type_New__c ,&apos;GW&apos;) &amp;&amp;  Is_Price_Approval_Request__c = True &amp;&amp; (ISPICKVAL( Status, &apos;Assigned&apos;) || ISPICKVAL(Status, &apos;Unassigned&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RR Case assign</fullName>
        <actions>
            <name>RR_case_assignment_mail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Is_Resource_Reservation__c</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Request to ETC Case Billing</fullName>
        <actions>
            <name>ETC_Request_To_Billing</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND (ISCHANGED(Status),ISPICKVAL(Status,&apos;Billing User in Progress&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Request to ETC Case Cancelled</fullName>
        <actions>
            <name>ETC_Request_Cancelled</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND (ISCHANGED(Status),ISPICKVAL(Status,&apos;Cancelled&apos;), Subject==&apos;Request to calculate ETC&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Request to ETC Case Closed</fullName>
        <actions>
            <name>ETC_Request_To_AcctOnwer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND (ISCHANGED(Status),ISPICKVAL(Status,&apos;Closed&apos;), Subject==&apos;Request to calculate ETC&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Request to ETC Case Commercial</fullName>
        <actions>
            <name>Request_To_ETC_for_Commercial</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(Status),ISPICKVAL(Status,&apos;Commercial User in Progress&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Request to ETC Case Sales</fullName>
        <actions>
            <name>ETC_Request_To_Sales</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
         <formula>AND(ISNEW(),ETCCreatedManually__c,ISPICKVAL(Status,&apos;Sales User in Progress&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    
    <rules>
        <fullName>Set Enrichment record type</fullName>
        <actions>
            <name>Set_Enrichment_record_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Enrichment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
   

    
    
 
    <rules>
        <fullName>Update Status when feasibility case is assigned</fullName>
        <actions>
            <name>Case_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(OwnerId)/* &amp;&amp; ((OwnerId=LastModifiedBy.Id)|| (OwnerId&lt;&gt;LastModifiedBy.Id))*/ &amp;&amp;  Is_Feasibility_Request__c &amp;&amp;  Text(Status)&lt;&gt;&apos;Cancelled&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>case for comments</fullName>
        <actions>
            <name>notify_the_sales</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>( RecordType.Name = &apos;Local Loop Record Type&apos; || RecordType.Name = &apos;GCPE Record Type&apos;) &amp;&amp;  Comments__c != &apos;&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
	   
	
</Workflow>
