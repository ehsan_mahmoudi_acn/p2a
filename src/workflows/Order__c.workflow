<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_7_Days_prior_to_FDD</fullName>
        <description>Alert 7 Days prior to FDD</description>
        <protected>false</protected>
        <recipients>
            <field>SD_PM_Contact__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Delivery_Templates/Alert_7_Days_prior_to_FDD</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_the_SD_team_for_manual_provisioning</fullName>
        <description>Email alert to the SD team for manual provisioning</description>
        <protected>false</protected>
        <recipients>
            <field>SD_PM_Contact__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Delivery_Templates/Generic_Order_is_assigned_to_the_SD_PM_user</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Order_Creator_on_Cancellation</fullName>
        <description>Email to Order Creator on Cancellation</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_to_Order_Creator_On_Order_Cancellation</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Order_Owner_On_Cancellation</fullName>
        <description>Email to Order Owner On Cancellation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Telstra_International/Email_to_Order_Owner_On_Order_Cancellation</template>
    </alerts>
    <alerts>
        <fullName>Intimation_to_SD_team</fullName>
        <description>Intimation to SD team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>SD_PM_Contact__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Delivery_Templates/Generic_Order_is_assigned_to_the_SD_user_NP</template>
    </alerts>
    <alerts>
        <fullName>Intimation_to_SD_team_for_GFTS_product</fullName>
        <description>Intimation to SD team for GFTS product</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>SD_PM_Contact__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Delivery_Templates/Generic_Order_containing_GFTS_is_assigned_to_the_SD_user_NP</template>
    </alerts>
    <alerts>
        <fullName>Intimation_to_SD_team_for_Whispir_product</fullName>
        <description>Intimation to SD team for Whispir product</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>SD_PM_Contact__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Delivery_Templates/Generic_Order_containing_Whispir_is_assigned_to_the_SD_user_NP</template>
    </alerts>
    <alerts>
        <fullName>Mail_Notification_to_SD_team_for_GFTS_enepath_product</fullName>
        <description>Mail Notification to SD team for GFTS enepath product</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>SD_PM_Contact__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Delivery_Templates/Generic_Order_containing_GFTS_enepath_is_assigned_to_the_SD_user_NP</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_the_SD_PM_user</fullName>
        <description>Notification to the SD PM user</description>
        <protected>false</protected>
        <recipients>
            <field>SD_PM_Contact__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Delivery_Templates/Order_is_assigned_to_the_SD_PM_user</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_the_SD_PM_user1</fullName>
        <description>Notification to the SD PM user1</description>
        <protected>false</protected>
        <recipients>
            <field>SD_PM_Contact__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Delivery_Templates/Order_is_assigned_to_the_SD_PM_user1</template>
    </alerts>
    <alerts>
        <fullName>Notify_Australia_Service_Provisioning_User</fullName>
        <description>Notify Australia Service Provisioning User</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_to_Service_Provisioning_users_1</template>
    </alerts>
    <alerts>
        <fullName>Notify_EMEA_Service_Provisioning_User</fullName>
        <description>Notify EMEA Service Provisioning User</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_to_Service_Provisioning_users_1</template>
    </alerts>
    <alerts>
        <fullName>Notify_North_Asia_Service_Provisioning_User</fullName>
        <description>Notify North Asia Service Provisioning User</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_to_Service_Provisioning_users_1</template>
    </alerts>
    <alerts>
        <fullName>Notify_South_Asia_Service_Provisioning_User</fullName>
        <description>Notify South Asia Service Provisioning User</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_to_Service_Provisioning_users_1</template>
    </alerts>
    <alerts>
        <fullName>Notify_US_Service_Provisioning_User</fullName>
        <description>Notify US Service Provisioning User</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_to_Service_Provisioning_users_1</template>
    </alerts>
    <alerts>
        <fullName>Notify_opportunity_owner_of_order_rejection</fullName>
        <description>Notify opportunity owner of order rejection</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Notify_opportunity_owner_for_order_rejection</template>
    </alerts>
    <alerts>
        <fullName>Notify_sd_team_of_order_update</fullName>
        <description>Notify sd team of order update</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>SD_PM_Contact__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Delivery_Templates/Notify_SD_team_for_order_Update</template>
    </alerts>
    <alerts>
        <fullName>Order_is_assigned_to_the_Techical_specialist</fullName>
        <description>Order is assigned to the Techical specialist</description>
        <protected>false</protected>
        <recipients>
            <field>Technical_Specialist_Contact__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Delivery_Templates/Notification_is_sent_to_the_Technical_Specialist</template>
    </alerts>
    <alerts>
        <fullName>Prompt_Manager_if_FDD_not_set_in_20days</fullName>
        <description>Prompt Manager if FDD not set in 20days</description>
        <protected>false</protected>
        <recipients>
            <field>SD_PM_Contact__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Delivery_Templates/Prompt_Manager_if_FDD_not_set_in_20days</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_CPS_Team</fullName>
        <description>Send Email to CPS Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>CPS_Queue</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Delivery_Templates/C_PS_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Send_Intimation_Mail_when_Opportunity_has_been_closed_and_order_handed_over</fullName>
        <description>Send Intimation Mail when Opportunity has been closed and order handed over</description>
        <protected>false</protected>
        <recipients>
            <field>SD_OM_Contact__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Delivery_Templates/Opportunity_closed_and_handed_over</template>
    </alerts>
    
	
    <alerts>
        <fullName>Shell_Product_Notification_to_the_SD_PM_user1</fullName>
        <description>Shell Product Notification to the SD PM user1</description>
        <protected>false</protected>
        <recipients>
            <field>SD_PM_Contact__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Delivery_Templates/Shell_Order_is_assigned_to_the_SD_PM_user1</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_the_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Provisioning_Australia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change the Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_the_Owner_to_EMEA</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Provisioning_EMEA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change the Owner to EMEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_the_Owner_to_North_Asia</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Provisioning_North_Asia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change the Owner to North Asia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_the_Owner_to_South_Asia</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Provisioning_South_Asia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change the Owner to South Asia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_the_Owner_to_US</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Provisioning_US</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change the Owner to US</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Creation_Date</fullName>
        <field>Order_Date__c</field>
        <formula>CreatedDate</formula>
        <name>Creation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Reason_Update</fullName>
        <field>Reject_Order__c</field>
        <literalValue>1</literalValue>
        <name>Field Reason Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Full_Termination_Order_Page</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Full_Order_Termination</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Full Termination Order Page</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IsEmailSentGFTSEnepathUpdate</fullName>
        <field>IsEmailSentGFTSEnepath__c</field>
        <literalValue>1</literalValue>
        <name>IsEmailSentGFTSEnepathUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_CPS</fullName>
        <description>Assigns the Queue Owner to CPS Queue if Order contains only CPS Line Items</description>
        <field>OwnerId</field>
        <lookupValue>Consulting_and_Professional_Services</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner_Update_CPS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Customer_Type_on_Order</fullName>
        <description>This is to set the Customer Type (GW/MNC/ISO) so that it can be used in varierty of applications</description>
        <field>Customer_Type__c</field>
        <formula>TEXT(Account__r.Customer_Type_New__c)</formula>
        <name>Set Customer Type on Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Order_Type</fullName>
        <field>Order_Type__c</field>
        <formula>TEXT(Opportunity__r.Order_Type__c)</formula>
        <name>Set Order Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Order_Type_for_Termination</fullName>
        <field>Order_Type__c</field>
        <formula>&quot;Termination&quot;</formula>
        <name>Set Order Type for Termination</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_as_SD_OM_Australia</fullName>
        <description>Set Owner as SD OM - Australia</description>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_Australia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Owner as SD OM - Australia</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_as_SD_OM_EMEA</fullName>
        <description>Set Owner as SD OM - EMEA</description>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_EMEA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Owner as SD OM - EMEA</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_as_SD_OM_North_Asia</fullName>
        <description>Set Owner as SD OM - North Asia</description>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_North_Asia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Owner as SD OM - North Asia</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_as_SD_OM_South_Asia</fullName>
        <description>Set Owner as SD OM - South Asia</description>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_South_Asia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Owner as SD OM - South Asia</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_as_SD_OM_US</fullName>
        <description>Set Owner as SD OM - US</description>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_US</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Owner as SD OM - US</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>isEmailSentOrder11Update</fullName>
        <field>isEmailSentOrder11__c</field>
        <literalValue>1</literalValue>
        <name>isEmailSentOrder11Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>isEmailSentOrder1Update</fullName>
        <description>This will indicate that the email has been sent already once.</description>
        <field>isEmailSentOrder1__c</field>
        <literalValue>1</literalValue>
        <name>isEmailSentOrder1Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>isEmailSentOrder2Update</fullName>
        <description>Setting the flag for email if email has been sent for the first time.</description>
        <field>isEmailSentOrder2__c</field>
        <literalValue>1</literalValue>
        <name>isEmailSentOrder2Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>isEmailSentOrder2Updated</fullName>
        <field>isEmailSentOrder2__c</field>
        <literalValue>1</literalValue>
        <name>isEmailSentOrder2Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>isEmailSentOrder2Updates</fullName>
        <field>isEmailSentOrder2__c</field>
        <literalValue>1</literalValue>
        <name>isEmailSentOrder2Updates</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>isEmailSentOrder3</fullName>
        <description>Notify SP on Shell Order Submission</description>
        <field>isEmailSentOrder3__c</field>
        <literalValue>1</literalValue>
        <name>isEmailSentOrder3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>isEmailSentOrder4</fullName>
        <field>isEmailSentOrder4__c</field>
        <literalValue>1</literalValue>
        <name>isEmailSentOrder4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>isEmailSentOrder5</fullName>
        <field>isEmailSentOrder5__c</field>
        <literalValue>1</literalValue>
        <name>isEmailSentOrder5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>isEmailSentOrder6</fullName>
        <field>isEmailSentOrder6__c</field>
        <literalValue>1</literalValue>
        <name>isEmailSentOrder6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>isEmailSentOrder7</fullName>
        <field>isEmailSentOrder7__c</field>
        <literalValue>1</literalValue>
        <name>isEmailSentOrder7</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>isEmailSentOrder8</fullName>
        <field>isEmailSentOrder8__c</field>
        <literalValue>1</literalValue>
        <name>isEmailSentOrder8</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>isEmailSentOrder9</fullName>
        <field>isEmailSentOrder9__c</field>
        <literalValue>1</literalValue>
        <name>isEmailSentOrder9</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>make_update_order_true</fullName>
        <field>Updated_Order__c</field>
        <literalValue>0</literalValue>
        <name>make update order true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Message_to_TIBCO_for_Order_Submission</fullName>
        <apiVersion>25.0</apiVersion>
        <endpointUrl>https://abc.com</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>siddharth.x.sinha@accenture.com.prod</integrationUser>
        <name>Message to TIBCO for OrderSub_DontChange</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Assign Owner to CPS Queue</fullName>
        <actions>
            <name>Owner_Update_CPS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>((1 OR 3) AND 2) AND ((4 AND 5) OR 6)</booleanFilter>
        <criteriaItems>
            <field>Order__c.Count_of_CPS_Line_Items__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Count_of_Non_CPS_Line_Items__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Count_of_Elastica_Line_Items__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Order_Type__c</field>
            <operation>equals</operation>
            <value>Termination</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Signed_Termination_Form_Attached__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Order_Type__c</field>
            <operation>notEqual</operation>
            <value>Termination</value>
        </criteriaItems>
        <description>Assigns the Queue Owner to CPS Queue if Order contains only CPS Line Items</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FDD not set in 20 days after clean order</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Clean_Order_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Firm_Delivery_Date__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>FDD not set in 20 days after clean order</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Alert_7_Days_prior_to_FDD</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Order__c.Clean_Order_Date__c</offsetFromField>
            <timeLength>13</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Prompt_Manager_if_FDD_not_set_in_20days</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Order__c.Clean_Order_Date__c</offsetFromField>
            <timeLength>20</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Full Termination Order Page</fullName>
        <actions>
            <name>Full_Termination_Order_Page</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Order_Type_for_Termination</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Order__c.Full_Termination_Order__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Generic Product Order is assigned to the SD PM user</fullName>
        <actions>
            <name>Email_alert_to_the_SD_team_for_manual_provisioning</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>isEmailSentOrder1Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( SD_PM_Contact__c &lt;&gt; NULL, Is_Shell_Product_Count_in_OLI__c = 0,  Is_Order_Submitted__c = FALSE, Is_shared_local_loop_count_in_OLI__c = 0,  ( Total_count_of_line_items__c - Count_of_non_provisionable_line__c ) &lt;&gt; 0, Count_of_Provisionable_Network_products__c &lt;&gt; 0, NOT(isEmailSentOrder1__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Intimation for GFTS enepath line items</fullName>
        <actions>
            <name>Mail_Notification_to_SD_team_for_GFTS_enepath_product</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>IsEmailSentGFTSEnepathUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To send email template to SD user for GFTS enepath product</description>
        <formula>AND (OR( (Count_of_GFTS_enepath_Line_Items__c &gt; 0 &amp;&amp; Is_Order_Submitted__c = TRUE &amp;&amp; ( CreatedById = LastModifiedById )), ( Count_of_GFTS_enepath_Line_Items__c &gt; 0 &amp;&amp; SD_PM_Contact__c &lt;&gt; null)),NOT(  IsEmailSentGFTSEnepath__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Intimation for GFTS line items</fullName>
        <actions>
            <name>Intimation_to_SD_team_for_GFTS_product</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>isEmailSentOrder2Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To send email template to SD user for GFTS product</description>
        <formula>AND (OR( (Count_of_GFTS_Line_Items__c &gt; 0 &amp;&amp; Is_Order_Submitted__c = TRUE &amp;&amp; ( CreatedById = LastModifiedById )), ( Count_of_GFTS_Line_Items__c &gt; 0 &amp;&amp; SD_PM_Contact__c &lt;&gt; null)),NOT( isEmailSentOrder2__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Intimation for Non-provisionable line items</fullName>
        <actions>
            <name>Intimation_to_SD_team</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>isEmailSentOrder2Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sales closes the Opportunity and creates the Order</description>
        <formula>AND (OR((( Total_count_of_line_items__c - Count_of_non_provisionable_line__c )=0 &amp;&amp; Is_Order_Submitted__c = TRUE &amp;&amp; ( CreatedById = LastModifiedById )), ( Count_of_non_provisionable_line__c &gt; 0 &amp;&amp; SD_PM_Contact__c &lt;&gt; null)),NOT( isEmailSentOrder2__c),( Total_count_of_line_items__c - Count_of_Whispir_line__c )!=0,(Total_count_of_line_items__c-Count_of_CPS_Line_Items__c)!=0,(Count_of_non_provisionable_line__c - Count_of_CPS_Line_Items__c - Count_of_Whispir_line__c - Count_of_GFTS_Line_Items__c - Count_of_GFTS_enepath_Line_Items__c - Count_of_Elastica_Line_Items__c)!=0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Intimation for Whispir line items</fullName>
        <actions>
            <name>Intimation_to_SD_team_for_Whispir_product</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>isEmailSentOrder2Updates</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To send email template to SD user for Whispir product</description>
        <formula>AND (OR( (Count_of_Whispir_line__c &gt; 0 &amp;&amp; Is_Order_Submitted__c = TRUE &amp;&amp; ( CreatedById = LastModifiedById )), ( Count_of_Whispir_line__c &gt; 0 &amp;&amp; SD_PM_Contact__c &lt;&gt; null)),NOT( isEmailSentOrder2__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify COM on Order submission</fullName>
        <actions>
            <name>Message_to_TIBCO_for_Order_Submission</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Is_Order_Submitted__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>WF rule to trigger a Outbound message to TIBCO while Order submitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    
   
    <rules>
        <fullName>Notify SP EMEA on Shell Order Submit</fullName>
        <actions>
            <name>Notify_EMEA_Service_Provisioning_User</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Change_the_Owner_to_EMEA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order__c.Is_Order_Submitted__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Is_Shell_Product_Count_in_OLI__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>EMEA</value>
        </criteriaItems>
        <description>This reaches to Service Provision user, on shell product Order submission</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify SP North Asia on Shell Order Submit</fullName>
        <actions>
            <name>Notify_North_Asia_Service_Provisioning_User</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Change_the_Owner_to_North_Asia</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order__c.Is_Order_Submitted__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Is_Shell_Product_Count_in_OLI__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>North Asia</value>
        </criteriaItems>
        <description>This reaches to Service Provision user, on shell product Order submission</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify SP South Asia on Shell Order Submit</fullName>
        <actions>
            <name>Notify_South_Asia_Service_Provisioning_User</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Change_the_Owner_to_South_Asia</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order__c.Is_Order_Submitted__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Is_Shell_Product_Count_in_OLI__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>South Asia</value>
        </criteriaItems>
        <description>This reaches to Service Provision user, on shell product Order submission</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify SP US on Shell Order Submit</fullName>
        <actions>
            <name>Notify_US_Service_Provisioning_User</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Change_the_Owner_to_US</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order__c.Is_Order_Submitted__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Is_Shell_Product_Count_in_OLI__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <description>This reaches to Service Provision user, on shell product Order submission</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
  
    <rules>
        <fullName>Order Cancellation Notification to Order Creator</fullName>
        <actions>
            <name>Email_to_Order_Creator_on_Cancellation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow to send an email to the Order Creator if and when an order is cancelled by someone else other than the Order Creator</description>
        <formula>IF(AND( CreatedById &lt;&gt;$User.Id, ISPICKVAL(Status__c,&apos;Cancelled&apos;), Full_Termination_Order__c), true, false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Order Create Date</fullName>
        <actions>
            <name>Creation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Customer_Type_on_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Order_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order is Cancelled</fullName>
        <actions>
            <name>Email_to_Order_Owner_On_Cancellation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(AND(OwnerId&lt;&gt;$User.Id,OwnerId&lt;&gt;CreatedById), ISPICKVAL(Status__c,&apos;Cancelled&apos;), Full_Termination_Order__c), true, false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Order is assigned to the SD PM user</fullName>
        <actions>
            <name>Notification_to_the_SD_PM_user</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>isEmailSentOrder4</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.SD_PM_Contact__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Is_Shell_Product_Count_in_OLI__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Is_Order_Submitted__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.isEmailSentOrder4__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order is assigned to the SD PM user-2</fullName>
        <actions>
            <name>Notification_to_the_SD_PM_user</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>isEmailSentOrder5</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED( SD_PM_Contact__c ) , NOT (ISNULL(SD_PM_Contact__c)),  Is_Shell_Product_Count_in_OLI__c ==0, NOT( Is_Order_Submitted__c ),NOT( isEmailSentOrder5__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Order is assigned to the SD PM user1</fullName>
        <actions>
            <name>Notification_to_the_SD_PM_user1</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED( SD_PM_Contact__c ) , NOT (ISNULL(SD_PM_Contact__c)), Is_Shell_Product_Count_in_OLI__c ==0, NOT( Is_Order_Submitted__c ), Is_shared_local_loop_count_in_OLI__c !=0 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Order is assigned to the Technical Specialist</fullName>
        <actions>
            <name>Order_is_assigned_to_the_Techical_specialist</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>isEmailSentOrder6</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Technical_Specialist_Contact__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.isEmailSentOrder6__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order is assigned to the Technical Specialist-2</fullName>
        <actions>
            <name>Order_is_assigned_to_the_Techical_specialist</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>isEmailSentOrder7</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  Technical_Specialist_Contact__c !=null , ISCHANGED( Technical_Specialist_Contact__c),NOT( isEmailSentOrder7__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SD-OM assignment for Australia</fullName>
        <actions>
            <name>Set_Owner_as_SD_OM_Australia</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3 AND 4 ) OR (1 AND 2 AND 3 AND  5)</booleanFilter>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>Australia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Count_of_CPS_Line_Items__c</field>
            <operation>greaterOrEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Count_of_Non_CPS_Line_Items__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Full_Termination_Order__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Signed_Termination_Form_Attached__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This is the workflow to assign the SD-OM queue for Australia
Assignment is based on the region of sales person</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SD-OM assignment for EMEA</fullName>
        <actions>
            <name>Set_Owner_as_SD_OM_EMEA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3 AND 4 ) OR (1 AND 2 AND 3 AND  5)</booleanFilter>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>EMEA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Count_of_CPS_Line_Items__c</field>
            <operation>greaterOrEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Count_of_Non_CPS_Line_Items__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Full_Termination_Order__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Signed_Termination_Form_Attached__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This is the workflow to assign the SD-OM queue for EMEA
Assignment is based on the region of sales person</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SD-OM assignment for North Asia</fullName>
        <actions>
            <name>Set_Owner_as_SD_OM_North_Asia</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3 AND 4 ) OR (1 AND 2 AND 3 AND  5)</booleanFilter>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>North Asia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Count_of_CPS_Line_Items__c</field>
            <operation>greaterOrEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Count_of_Non_CPS_Line_Items__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Full_Termination_Order__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Signed_Termination_Form_Attached__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This is the workflow to assign the SD-OM queue for North Asia
Assignment is based on the region of sales person</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SD-OM assignment for South Asia</fullName>
        <actions>
            <name>Set_Owner_as_SD_OM_South_Asia</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3 AND 4 ) OR (1 AND 2 AND 3 AND  5)</booleanFilter>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>South Asia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Count_of_CPS_Line_Items__c</field>
            <operation>greaterOrEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Count_of_Non_CPS_Line_Items__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Full_Termination_Order__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Signed_Termination_Form_Attached__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This is the workflow to assign the SD-OM queue for South Asia
Assignment is based on the region of sales person</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SD-OM assignment for US</fullName>
        <actions>
            <name>Set_Owner_as_SD_OM_US</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Count_of_Non_CPS_Line_Items__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Count_of_CPS_Line_Items__c</field>
            <operation>greaterOrEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Full_Termination_Order__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Signed_Termination_Form_Attached__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This is the workflow to assign the SD-OM queue for US
Assignment is based on the region of sales person</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sales closes the Opportunity and creates the Order</fullName>
        <actions>
            <name>Send_Intimation_Mail_when_Opportunity_has_been_closed_and_order_handed_over</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>isEmailSentOrder8</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sales closes the Opportunity and creates the Order</description>
        <formula>true &amp;&amp; NOT( isEmailSentOrder8__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send Email to CPS Team</fullName>
        <actions>
            <name>Send_Email_to_CPS_Team</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>isEmailSentOrder11Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 3) AND 2</booleanFilter>
        <criteriaItems>
            <field>Order__c.Count_of_CPS_Line_Items__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.isEmailSentOrder11__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Count_of_Elastica_Line_Items__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>Sends an Email to the CPS Team when an Order that has CPS Line Items in it</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Shell Product Order is assigned to the SD PM user</fullName>
        <actions>
            <name>Shell_Product_Notification_to_the_SD_PM_user</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>isEmailSentOrder9</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.SD_PM_Contact__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Is_Shell_Product_Count_in_OLI__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Is_Order_Submitted__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Is_shared_local_loop_count_in_OLI__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.isEmailSentOrder9__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This is notification to SDPM user on creation of order if its having Shell products</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Shell Product Order is assigned to the SD PM user1</fullName>
        <actions>
            <name>Shell_Product_Notification_to_the_SD_PM_user1</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.SD_PM_Contact__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Is_Shell_Product_Count_in_OLI__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Is_Order_Submitted__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Is_shared_local_loop_count_in_OLI__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <description>This is notification to SDPM user on creation of order if its having Shell products</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
  
</Workflow>
