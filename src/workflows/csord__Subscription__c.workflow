<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Order_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Inflight_Subscription_RT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Order RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RT_Inflight</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Change_Order_RT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RT - Inflight</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SubscriptionName</fullName>
        <field>Name</field>
        <formula>Name + &apos; - &apos; +  csordtelcoa__Subscription_Number__c</formula>
        <name>SubscriptionName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Customertype_Subsciption</fullName>
        <description>Implemented the Sharing Rule for Subscriptions object</description>
        <field>Customer_Type__c</field>
        <formula>TEXT(csord__Account__r.Customer_Type_New__c)</formula>
        <name>Customertype_Subsciption</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>RT - Change Order</fullName>
        <actions>
            <name>Change_Order_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Count_of_Service_not_provisioned__c =  Count_of_services__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RT - Inflight</fullName>
        <actions>
            <name>RT_Inflight</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Count_of_Service_not_provisioned__c &lt;&gt; Count_of_services__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SubscriptionName</fullName>
        <actions>
            <name>SubscriptionName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>csord__Order__c &lt;&gt; Null</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
	<rules>
        <fullName>Customertype_subsciption1</fullName>
        <actions>
            <name>Customertype_Subsciption</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Implemented for the Sharing Rule for Subscriptions</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
