<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
	
	 <alerts>
        <fullName>An_email_notification_to_Sales_user_and_Marketing_queue_for_region_US_if_lead_in</fullName>
        <description>An email notification to Sales user and Marketing queue for region US if lead in MQL</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Assignment_US</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Lead_Object_task_for_new_logo_have_been_assigned_to_Lead_Owner_and_MarketingQueu</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_when_lead_status_in_MQL_for_US_Region</fullName>
        <description>Email notification when lead status in MQL for US Region</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Assignment_US</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Lead_Object_task_for_new_logo_have_been_assigned_to_Lead_Owner_and_MarketingQueu</template>
    </alerts>

    <alerts>
        <fullName>First_email_notification_when_lead_status_is_in_MQL_for_US_Region</fullName>
        <description>First email notification when lead status is in MQL for US Region</description>
		<protected>false</protected>
		 <recipients>
            <recipient>Lead_Assignment_US</recipient>
            <type>group</type>
        </recipients>
		 <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Second_email_notification_when_lead_status_is_in_MQL</template>
    </alerts>
	 
	 <alerts>
        <fullName>Email_Notification_to_Marketing_user_for_Australia_region</fullName>
        <description>Email Notification to Marketing user for Australia region</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Assignment_Australia</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Lead_Object_task_for_new_logo_have_been_assigned_to_Lead_Owner_and_MarketingQueu</template>
    </alerts>
	<alerts>
        <fullName>An_email_notification_to_Sales_user_and_Marketing_queue_for_Region_South_Asia_if</fullName>
        <description>An email notification to Sales user and Marketing queue for Region South Asia if lead in MQL</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Assignment_South_Asia</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Lead_Object_task_for_new_logo_have_been_assigned_to_Lead_Owner_and_MarketingQueu</template>
    </alerts>
    <alerts>
        <fullName>An_email_notification_to_Sales_user_and_Marketing_queue_for_country_Marketing_Qu</fullName>
        <description>An email notification to Sales user and Marketing queue for country Marketing Queue if lead in MQL</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Assignment_UK</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Lead_Object_task_for_new_logo_have_been_assigned_to_Lead_Owner_and_MarketingQueu</template>
    </alerts>
    <alerts>
        <fullName>An_email_notification_to_Sales_user_and_Marketing_queue_for_region_North_Asia_if</fullName>
        <description>An email notification to Sales user and Marketing queue for region North Asia if lead in MQL</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Assignment_North_Asia</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Lead_Object_task_for_new_logo_have_been_assigned_to_Lead_Owner_and_MarketingQueu</template>
    </alerts>
	
    <alerts>
        <fullName>Email_Notification_to_Marketing_user_for_NA_region</fullName>
        <description>Email Notification to Marketing user for NA region</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Assignment_North_Asia</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Lead_Object_task_for_new_logo_have_been_assigned_to_Lead_Owner_and_MarketingQueu</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_Marketing_user_for_South_Asia_region</fullName>
        <description>Email Notification to Marketing user for South Asia region</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Assignment_South_Asia</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Lead_Object_task_for_new_logo_have_been_assigned_to_Lead_Owner_and_MarketingQueu</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_Sales_user_and_Marketing_queue_for_region_EMEA</fullName>
        <description>Email Notification to Sales user and Marketing queue for region EMEA</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Assignment_EMEA</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Lead_Object_task_for_new_logo_have_been_assigned_to_Lead_Owner_and_MarketingQueu</template>
    </alerts>
	<alerts>
        <fullName>Email_notification_for_UK_Country_when_Lead_is_assigned_to_Marketing_queue</fullName>
        <description>Email notification for UK Country when Lead is assigned to Marketing queue</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Assignment_UK</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Lead_Object_task_for_new_logo_have_been_assigned_to_Lead_Owner_and_MarketingQueu</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_for_UK_Country_when_Lead_is_assigned_to_Marketing_queue1</fullName>
        <description>Email notification for UK Country when Lead is assigned to Marketing queue</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Assignment_UK</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Lead_Object_task_for_new_logo_have_been_assigned_to_Lead_Owner_and_MarketingQueu</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_to_Sales_user_and_Marketing_queue_for_region_Australia</fullName>
        <description>Email notification to Sales user and Marketing queue for region Australia</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Assignment_Australia</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Lead_Object_task_for_new_logo_have_been_assigned_to_Lead_Owner_and_MarketingQueu</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_when_lead_status_in_MQL_for_EMEA_Region</fullName>
        <description>Email notification when lead status in MQL for EMEA Region</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Assignment_EMEA</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Lead_Object_task_for_new_logo_have_been_assigned_to_Lead_Owner_and_MarketingQueu</template>
    </alerts>
	
   
   
   
    <alerts>
        <fullName>First_email_notification_when_lead_status_is_in_MQL_for_South_Asia_Region</fullName>
        <description>First email notification when lead status is in MQL for South Asia Region</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Assignment_South_Asia</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/First_email_notification_when_lead_status_is_in_MQL</template>
    </alerts>
   
    <alerts>
        <fullName>Second_email_notification_when_lead_status_in_MQL_for_Australia_Region</fullName>
        <description>Second email notification when lead status in MQL for Australia Region</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Assignment_Australia</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Second_email_notification_when_lead_status_is_in_MQL</template>
    </alerts>
    <alerts>
        <fullName>Second_email_notification_when_lead_status_in_MQL_for_South_Asia_Region</fullName>
        <description>Second email notification when lead status in MQL for South Asia Region</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Assignment_South_Asia</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Second_email_notification_when_lead_status_is_in_MQL</template>
    </alerts>
    <alerts>
        <fullName>Second_email_notification_when_lead_status_is_in_MQL_UK_country</fullName>
        <description>Second email notification when lead status is in MQL UK country</description>
        <protected>false</protected>
        <recipients>
            <recipient>Lead_Assignment_UK</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Second_email_notification_when_lead_status_is_in_MQL</template>
    </alerts>
 
	
	
	<fieldUpdates>
        <fullName>Update_Lead_status_date_for_MQL</fullName>
        <description>created for TGME0021095</description>
        <field>Lead_Status_Date_for_MQL__c</field>
        <formula>NOW()</formula>
        <name>Update Lead status date for MQL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_lead_status_date_for_SAL</fullName>
        <description>created for TGME0021095</description>
        <field>Lead_Status_Date_for_SAL__c</field>
        <formula>NOW()</formula>
        <name>update lead status date for SAL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>  
	
    <fieldUpdates>
        <fullName>Field_update_when_lead_assign_to_Aus_Reg</fullName>
        <field>OwnerId</field>
        <lookupValue>Lead_Assignment_Australia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Field update when lead assign to Aus Reg</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_update_when_lead_assign_to_NA_Regi</fullName>
        <field>OwnerId</field>
        <lookupValue>Lead_Assignment_North_Asia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Field update when lead assign to NA Regi</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_update_when_lead_assign_to_SA_Regi</fullName>
        <field>OwnerId</field>
        <lookupValue>Lead_Assignment_South_Asia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Field update when lead assign to SA Regi</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_update_when_lead_assign_to_UK_Ctry</fullName>
        <field>OwnerId</field>
        <lookupValue>Lead_Assignment_UK</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Field update when lead assign to UK Ctry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
	
    <fieldUpdates>
        <fullName>IsPrirorValues</fullName>
        <field>Email</field>
        <formula>LeadEmail__c</formula>
        <name>IsPrirorValues</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Assignment_Date_Australia_region</fullName>
        <field>Lead_Assignment_Date__c</field>
        <formula>Now()</formula>
        <name>Lead Assignment Date Australia region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Assignment_Date_EMEA_region</fullName>
        <field>Lead_Assignment_Date__c</field>
        <formula>Now()</formula>
        <name>Lead Assignment Date EMEA region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Assignment_Date_North_Asia_region</fullName>
        <field>Lead_Assignment_Date__c</field>
        <formula>Now()</formula>
        <name>Lead Assignment Date North Asia region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Assignment_Date_South_Asia_Region</fullName>
        <field>Lead_Assignment_Date__c</field>
        <formula>Now()</formula>
        <name>Lead Assignment Date South Asia Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	
	<fieldUpdates>
        <fullName>Field_update_when_lead_assign_to_US_Regi</fullName>
        <field>OwnerId</field>
        <lookupValue>Lead_Assignment_US</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Field update when lead assign to US Regi</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    
    <fieldUpdates>
        <fullName>Lead_Assignment_Date_US_region</fullName>
        <field>Lead_Assignment_Date__c</field>
        <formula>Now()</formula>
        <name>Lead Assignment Date US region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	
    <fieldUpdates>
        <fullName>Lead_Assignment_UK</fullName>
        <field>Lead_Assignment_Date__c</field>
        <formula>Now()</formula>
        <name>Lead Assignment UK</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_owner_to_LeadAss_EMEA</fullName>
        <field>OwnerId</field>
        <lookupValue>Lead_Assignment_EMEA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Lead owner to LeadAss EMEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
	
	
	<rules>
        <fullName>Auto assign lead owner to marketing queue for Australia region</fullName>
        <actions>
            <name>Email_Notification_to_Marketing_user_for_Australia_region</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Field_update_when_lead_assign_to_Aus_Reg</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Marketing Qualified Lead (MQL)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Region__c</field>
            <operation>equals</operation>
            <value>Australia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country_Picklist__c</field>
            <operation>notEqual</operation>
            <value>UNITED KINGDOM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Account_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Desc:Auto assign lead owner to marketing queue for Australia region.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Auto assign lead owner to marketing queue for EMEA region</fullName>
        <actions>
            <name>Email_notification_when_lead_status_in_MQL_for_EMEA_Region</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Lead_owner_to_LeadAss_EMEA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Marketing Qualified Lead (MQL)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Region__c</field>
            <operation>equals</operation>
            <value>EMEA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country_Picklist__c</field>
            <operation>notEqual</operation>
            <value>UNITED KINGDOM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Account_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Auto assign lead owner to marketing queue for EMEA region</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Auto assign lead owner to marketing queue for North Asia region</fullName>
        <actions>
            <name>Email_Notification_to_Marketing_user_for_NA_region</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Field_update_when_lead_assign_to_NA_Regi</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Marketing Qualified Lead (MQL)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Region__c</field>
            <operation>equals</operation>
            <value>North Asia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country_Picklist__c</field>
            <operation>notEqual</operation>
            <value>UNITED KINGDOM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Account_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Auto assign lead owner to marketing queue for North Asia region</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Auto assign lead owner to marketing queue for South Asia region</fullName>
        <actions>
            <name>Email_Notification_to_Marketing_user_for_South_Asia_region</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Field_update_when_lead_assign_to_SA_Regi</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Marketing Qualified Lead (MQL)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Region__c</field>
            <operation>equals</operation>
            <value>South Asia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country_Picklist__c</field>
            <operation>notEqual</operation>
            <value>UNITED KINGDOM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Account_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Auto assign lead owner to marketing queue for South Asia region</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
		
  
   
   
	
    <rules>
        <fullName>IsEmailChanged</fullName>
        <actions>
            <name>IsPrirorValues</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule will execute when Lead Account ID is not Null</description>
        <formula>AND(Lead_Account_Id__c!= NULL,ISNEW())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    
    
    
    
	
	<rules>
        <fullName>Assign Rejected Leads to Marketing</fullName>
        <actions>
            <name>Assign_Rejected_Leads_to_Marketing_and_notify</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_Rejected_Leads_Marketing</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Lead_Rating_Cold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Lead.Disqual_Reason__c</field>
            <operation>contains</operation>
            <value>Assign to Another Telstra BU,Bad fit for Telstra,Budget restrictions,Duplicate,Incorrect contact information,Long-term competitor engagement,Never reached,No decision making authority,No requirement,Not ready to engage</value>
        </criteriaItems>
        <description>All rejected lead by sales will become a cold lead and pass back to marketing</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>	
	<rules>
        <fullName>To update the lead status date for MQL Leads</fullName>
        <actions>
            <name>Update_Lead_status_date_for_MQL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Marketing Qualified Lead (MQL)</value>
        </criteriaItems>
        <description>Created for TGME0021095</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>To update the lead status date for SAL Leads</fullName>
        <actions>
            <name>update_lead_status_date_for_SAL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Sales Accepted Lead (SAL)</value>
        </criteriaItems>
        <description>Created for TGME0021095</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
	
	<rules>
        <fullName>Auto assign lead owner to marketing queue for US region</fullName>
        <actions>
            <name>Email_notification_when_lead_status_in_MQL_for_US_Region</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Field_update_when_lead_assign_to_US_Regi</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Marketing Qualified Lead (MQL)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country_Picklist__c</field>
            <operation>notEqual</operation>
            <value>UNITED KINGDOM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Account_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Auto assign lead owner to marketing queue for US region</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    
   
	
    
    
</Workflow>
