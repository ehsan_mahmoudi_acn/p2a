UpdateProductBasketHasOffnet


private static void UpdateProductBasketHasOffnet(set<Id> setProductBasketId){
    List<cscfga__Product_Configuration__c> lstAllProductConfiguration = [select Id,Is_Offnet__c,cscfga__Product_Basket__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c in : setProductBasketId];
    
    Map<Id,cscfga__Product_Basket__c> mapProductBasket = new map<Id,cscfga__Product_Basket__c>();
    cscfga__Product_Basket__c tmpProductBasket;
    
    for (cscfga__Product_Configuration__c tmpPC : lstAllProductConfiguration){
        if (!mapProductBasket.containsKey(tmpPC.cscfga__Product_Basket__c)){
            tmpProductBasket = new cscfga__Product_Basket__c();
            tmpProductBasket.Id=tmpPC.cscfga__Product_Basket__c;
            tmpProductBasket.Has_Offnet_Products__c='No';
            if (tmpPC.Is_Offnet__c=='Yes') {
                tmpProductBasket.Has_Offnet_Products__c='Yes';
            }
            mapProductBasket.put(tmpPC.cscfga__Product_Basket__c,tmpProductBasket);
        }
        else{
            if (tmpPC.Is_Offnet__c=='Yes'){
                tmpProductBasket=mapProductBasket.get(tmpPC.cscfga__Product_Basket__c);
                tmpProductBasket.Has_Offnet_Products__c='Yes';
            }
        }
    }
    
    if (mapProductBasket.size()>0) {
        update mapProductBasket.values();
    }
}  
}