<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Update the Bill Decimal Point</fullName>
        <actions>
            <name>Update_the_Bill_Decimal_Point</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BillProfile__c.Currency__c</field>
            <operation>equals</operation>
            <value>JPY,KRW,IDR</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
	 <fieldUpdates>
        <fullName>Update_the_Bill_Decimal_Point</fullName>
        <field>Bill_Decimal_Point__c</field>
        <formula>0</formula>
        <name>Update the Bill Decimal Point</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
