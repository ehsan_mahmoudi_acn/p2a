<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SNOW_Complete</fullName>
        <field>SNOW_Complete__c</field>
        <literalValue>1</literalValue>
        <name>SNOW Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Service_ID__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_service_latest_update_comment</fullName>
        <field>Latest_Integration_Update_Comment__c</field>
        <formula>Comment__c</formula>
        <name>Set service latest update comment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Service_ID__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_service_latest_update_date</fullName>
        <field>Latest_Integration_Update_Date__c</field>
        <formula>CreatedDate</formula>
        <name>Set service latest update date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Service_ID__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_service_latest_update_name</fullName>
        <field>Latest_Integration_Update_Name__c</field>
        <formula>Name</formula>
        <name>Set service latest update name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Service_ID__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_service_latest_update_status</fullName>
        <field>Latest_Integration_Update_Status__c</field>
        <formula>Status__c</formula>
        <name>Set service latest update status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Service_ID__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_service_latest_update_text</fullName>
        <field>Latest_Integration_Update_Message__c</field>
        <formula>Name &amp; &quot;: &quot; &amp; Status__c &amp; &quot;: &quot; &amp; Comment__c</formula>
        <name>Set service latest update text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Service_ID__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_service_latest_update_user</fullName>
        <field>Latest_Integration_Update_User__c</field>
        <formula>User__c</formula>
        <name>Set service latest update user</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Service_ID__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Service_RAG</fullName>
        <field>RAG_Status_Amber__c</field>
        <literalValue>1</literalValue>
        <name>Update Service RAG</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Service_ID__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Service_RAG2</fullName>
        <field>RAG_Status_Red__c</field>
        <literalValue>1</literalValue>
        <name>Set Service RAG_Statuts_Red</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Service_ID__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Amber_State</fullName>
        <field>RAG_Status_Amber__c</field>
        <literalValue>0</literalValue>
        <name>Clear Amber State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Service_ID__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reason_Code</fullName>
        <field>RAG_Reason_Code__c</field>
        <formula>&quot;Fulfilment on hold (&quot;+Comment__c+&quot;)&quot;</formula>
        <name>Set Reason Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Service_ID__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reason_Code_2</fullName>
        <field>RAG_Reason_Code__c</field>
        <formula>&quot;OLA Breached (&quot;+Comment__c+&quot;)&quot;</formula>
        <name>Set Service RAG Reason Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Service_ID__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Clear RAG status if normal message received</fullName>
        <actions>
            <name>Clear_Amber_State</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Integration_Update__c.Status__c</field>
            <operation>notEqual</operation>
            <value>On Hold</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OLA breached RAG Update</fullName>
        <actions>
            <name>Set_Reason_Code_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Service_RAG2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Integration_Update__c.OLA_Breached__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>On Hold RAG Update</fullName>
        <actions>
            <name>Set_Reason_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Service_RAG</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Integration_Update__c.Status__c</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>    
	<rules>
        <fullName>SNOW Complete</fullName>
        <actions>
            <name>SNOW_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(Name  = &quot;Design, Implement and Testing has completed in SNOW&quot;, NOT(AND(NOT(Service_ID__r.Ordertype__c=&quot;IGNORE&quot;),  NOT(Service_ID__r.csord__Order__r.Order_Type__c =&quot;Renewal&quot;), Comment__c = &quot;Tibco generated Response&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set service latest update text</fullName>
        <actions>
            <name>Set_service_latest_update_comment</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_service_latest_update_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_service_latest_update_name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_service_latest_update_status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_service_latest_update_text</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_service_latest_update_user</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Integration_Update__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
