<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Action_Item_Assigned_Re_assigned</fullName>
        <description>Action Item Assigned/Re-assigned</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Action_Item_Assigned</template>
    </alerts>
	<alerts>
        <fullName>Order_Configuration_Action_Item_Status_Assigned</fullName>
        <description>Order Configuration Action Item_Status_Assigned</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_User__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Order_Support_Action_Item_to_Assigned</template>
    </alerts>
    <alerts>
        <fullName>Order_Support_Action_Item_to_Approved</fullName>
        <description>Order Support Action Item to Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Order_Support_Action_Item_to_Approved</template>
    </alerts>
    <alerts>
        <fullName>Pricing_Validation_Action_Item_Assigned_To</fullName>
        <description>Pricing Validation Action Item Assigned To</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To_VO__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Pricing_Validation_Action_Item_Assigned_To</template>
    </alerts>	
	<alerts>
        <fullName>Email_alert_to_Maestro_team</fullName>
        <description>Email alert to Maestro team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>telstra.tghris.p20.supportteam@accenture.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Service_Delivery_Templates/Action_Item_Assigned_for_Validate_Team</template>
    </alerts>
	<alerts>
        <fullName>Validate_Oppo_Asignedto</fullName>
        <description>Validate Oppo_Asignedto</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To_VO__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>telstra.tghris.p20.supportteam@accenture.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Service_Delivery_Templates/Action_Item_Assigned_for_Validate_Team_Assigned_to</template>
    </alerts>
	<alerts>
        <fullName>Email_alert_to_Pre_order</fullName>
        <description>Email alert to Pre order</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>telstra.tghris.p20.supportteam@accenture.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Service_Delivery_Templates/Action_Item_Assigned_for_Pre_Order_Service</template>
    </alerts>
    <alerts>
        <fullName>Action_Item_Assigned_to_Credit_control_Team</fullName>
        <description>Action Item Assigned to Credit control Team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Action_Item_Assigned_to_CreditControl_Team</template>
    </alerts>
    <alerts>
        <fullName>Action_Item_Closed</fullName>
        <description>Action Item Closed</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Action_Item_is_Closed</template>
    </alerts>
	
    <alerts>
        <fullName>Action_Item_has_not_been_Followed_Up</fullName>
        <description>Action Item has not been Followed Up</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Telstra_International/Action_Item_Due</template>
    </alerts>
    <alerts>
        <fullName>Alert_the_Assigned_To_User</fullName>
        <description>Alert the Assigned To User</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Action_Item_Assigned_to_Pricing_User</template>
    </alerts>
    <alerts>
        <fullName>Approval_Status</fullName>
        <description>Approval Status preprov</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Opportunity_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Sales_manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Notification_about_approval_to_opportunity_owner</template>
    </alerts>
    <alerts>
        <fullName>Approval_status_of_pre_provisioning</fullName>
        <description>Approval status of pre provisioning</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Opportunity_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Sales_manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Notification_about_approval_to_opportunity_owner</template>
    </alerts>
    <alerts>
        <fullName>Assign_Missing_Service_to_Service_Delivery</fullName>
        <description>Assign Missing Service to Service Delivery</description>
        <protected>false</protected>
        <recipients>
            <recipient>Service_Delivery</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Service_Delivery_PM</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Missing_Service_Action_Item_Assigned_Reassigned</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_AUS</fullName>
        <description>Email Notification_AUS</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Action_Item_Assigned_Sales</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_AUS_MNC</fullName>
        <description>Email Notification_AUS MNC</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Action_Item_Assigned_Sales</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_Asia</fullName>
        <description>Email Notification_Asia</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Action_Item_Assigned_Sales</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_EMEA</fullName>
        <description>Email Notification_EMEA</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Action_Item_Assigned_Sales</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_US</fullName>
        <description>Email Notification_US</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Action_Item_Assigned_Sales</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Billing_User_the_information_filled_in_by_the_Commercial_User</fullName>
        <description>Email to Billing User the information filled in by the Commercial User</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Telstra_International/Action_Item_Email_to_Billing_after_Commercial_completes_it</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_SD_user</fullName>
        <description>Notification to SD user</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Delivery_Templates/Notification_to_SD_team</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_opportunity_owner</fullName>
        <description>Notification to opportunity owner</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Opportunity_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Rejection_by_Sales_pre_provisioning</template>
    </alerts>
    <alerts>
        <fullName>NotifyOppOwnerOnPricingApproval</fullName>
        <description>NotifyOppOwnerOnPricingApproval</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Templates/NotifyOppOwnerOnPricingApproval</template>
    </alerts>
    <alerts>
        <fullName>Notify_Account_Owner_on_Request_Order_Support_AI_rejection</fullName>
        <description>Notify Account Owner on Request Order Support AI rejection</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Order_Support_AI_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Notify_Action_Item_Creator</fullName>
        <description>Notify Action Item Creator</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Action_Item_Reassign1</template>
    </alerts>
    <alerts>
        <fullName>Notify_Action_Item_OppOwner</fullName>
        <description>Notify Action Item Opp Owner</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Action_Item_Reassign2</template>
    </alerts>
    <alerts>
        <fullName>Notify_for_Action_Item_Not_Addressed</fullName>
        <description>Notify for Action Item Not Addressed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Notify_for_Action_Item_Not_Addressed</template>
    </alerts>
    <alerts>
        <fullName>Notify_for_Action_Item_Not_Addressed2</fullName>
        <description>Notify for Action Item Not Addressed2</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Notify_for_Action_Item_Not_Addressed</template>
    </alerts>
    <alerts>
        <fullName>Notify_to_Billing_Team_for_Request_to_ETC</fullName>
        <description>Notify to Billing Team for Request to ETC</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Request_To_Calculate_ETC_BillingUser</template>
    </alerts>
    <alerts>
        <fullName>Once_the_credit_check_is_complete_the_Opportunity_Owner_is_notified_along_with_t</fullName>
        <description>Once the credit check is complete the Opportunity Owner is notified along with the result.</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_related_user__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>X3PQ_Email_Templates/Notify_Opp_Owner_on_Credit_check_completes</template>
    </alerts>
    <alerts>
        <fullName>Request_for_Billing_Information</fullName>
        <description>Request for Billing Information</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Telstra_International/Notify_to_Billing_User_for_Billing_Information</template>
    </alerts>
    <alerts>
        <fullName>Request_to_ETC_Notification</fullName>
        <description>Request to ETC Notification</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Telstra_International/Action_Item_Assigned_ETC_Sales</template>
    </alerts>
    <alerts>
        <fullName>Request_to_ETC_Notification_for_Order_Creator</fullName>
        <description>Request to ETC Notification for Order Creator</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Notify_Request_ETC_Action_Item_Completed</template>
    </alerts>
    <alerts>
        <fullName>Request_to_ETC_Notification_to_Commercial_team</fullName>
        <description>Request to ETC Notification to Commercial team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Request_To_Calculate_ETC_CommercialUser</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_AI_Creator</fullName>
        <description>Send Email to AI Creator</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Action_Item_Reassign1</template>
    </alerts>
    <alerts>
        <fullName>Service_Action_Item_Assigned_Reassigned</fullName>
        <description>Service Action Item Assigned/ Reassigned</description>
        <protected>false</protected>
        <recipients>
            <recipient>Service_Delivery</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Sales_Templates/Service_Action_Item_Assigned_Reassigned</template>
    </alerts>
    <alerts>
        <fullName>Service_Provisioning_Request</fullName>
        <description>Service Provisioning Request</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Service_Provisioning_Action_Item_Assigned_Reassigned</template>
    </alerts>
    <alerts>
        <fullName>notify_opp_owner</fullName>
        <description>notify opp owner</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Opportunity_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Sales_manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Rejection_by_Sales_pre_provisioning</template>
    </alerts>
	<alerts>
        <fullName>Email_alert_to_Maestro_team_assigned_to_pre_order</fullName>
        <description>Email alert to Maestro team assigned to pre order</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To_VO__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>telstra.tghris.p20.supportteam@accenture.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Service_Delivery_Templates/Action_Item_Assigned_for_Pre_Order_Service_Assigned_to</template>
    </alerts>
    <alerts>
        <fullName>email_for_opp_owner</fullName>
        <description>email for opp owner</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Opportunity_related_user__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/email_for_opportunity_owner</template>
    </alerts>	
	<fieldUpdates>
        <fullName>Assigned_User_Update</fullName>
        <field>Assigned_User__c</field>
        <formula>Assigned_To_OD__r.Email</formula>
        <name>Assigned User Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OrderconfAI_Assigned_User_Update</fullName>
        <field>Assigned_User__c</field>
        <formula>Assigned_To__r.Email</formula>
        <name>OrderconfAI Assigned User Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pricing_ValidationStatus_Assigned</fullName>
        <field>Status__c</field>
        <literalValue>Assigned</literalValue>
        <name>Pricing Validation_Status_Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Assigned</fullName>
        <field>Status__c</field>
        <literalValue>Assigned</literalValue>
        <name>Status_Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_AI_Region</fullName>
        <field>Region__c</field>
        <literalValue>Australia</literalValue>
        <name>Assign AI Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Owner_assignment_Validate_Opp</fullName>
        <field>OwnerId</field>
        <lookupValue>Maestro_Group</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Updating_the_subject</fullName>
        <field>Subject__c</field>
        <formula>TEXT(Subject_for_Pre_Order__c)</formula>
        <name>Updating the subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Carrier_Quote_to_Carrier_Quote_GP</fullName>
        <field>OwnerId</field>
        <lookupValue>UK_Carrier_Quote_Group</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Carrier Quote to Carrier Quote GP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Contract_Approval_to_Legal</fullName>
        <description>Assign Contract Approval to Legal</description>
        <field>OwnerId</field>
        <lookupValue>Legal</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Contract Approval to Legal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Owner_Sales</fullName>
        <field>OwnerId</field>
        <lookupValue>Sales_Admin_Australia_MNC</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Owner Sales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Completed_By_1Update</fullName>
        <field>Completed_By__c</field>
        <formula>$User.Id</formula>
        <name>Completed By Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Completed_By_Update</fullName>
        <field>Completed_By__c</field>
        <formula>&quot;00590000001SuMy&quot;</formula>
        <name>Completed By Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Completed_Date</fullName>
        <field>Completion_Date__c</field>
        <formula>LastModifiedDate</formula>
        <name>Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Completed_Dates</fullName>
        <field>Completion_Date__c</field>
        <formula>LastModifiedDate</formula>
        <name>Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Completed_by_sd</fullName>
        <field>Completed_By_Service_delivery__c</field>
        <formula>$User.Id</formula>
        <name>Completed by sd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Completed_date_SD</fullName>
        <field>Completed_By_Service_delivery_date__c</field>
        <formula>LastModifiedDate</formula>
        <name>Completed date SD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Subject1</fullName>
        <field>Subject__c</field>
        <formula>TEXT(Quote_Subject__c)</formula>
        <name>Copy Subject as Support Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Subject2</fullName>
        <field>Subject__c</field>
        <formula>TEXT(Quote_Subject__c)</formula>
        <name>Copy Subject as Support Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Subject3</fullName>
        <field>Subject__c</field>
        <formula>TEXT(Quote_Subject__c)</formula>
        <name>Copy Subject as Support Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Subject4</fullName>
        <field>Subject__c</field>
        <formula>TEXT(Quote_Subject__c)</formula>
        <name>Copy Subject as Support Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Subject5</fullName>
        <field>Subject__c</field>
        <formula>TEXT(Quote_Subject__c)</formula>
        <name>Copy Subject as Support Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Subject6</fullName>
        <field>Subject__c</field>
        <formula>TEXT(Quote_Subject__c)</formula>
        <name>Copy Subject as Support Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email_field</fullName>
        <field>Sales_manager_Email__c</field>
        <formula>LastModifiedBy.Email</formula>
        <name>Email field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
		</fieldUpdates>
    <fieldUpdates>
        <fullName>Feedback_Apporved</fullName>
        <field>Feedback__c</field>
        <literalValue>Approved</literalValue>
        <name>Feedback Apporved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Is_sales_approved_yes</fullName>
        <field>Is_senior_sales_approved__c</field>
        <literalValue>1</literalValue>
        <name>Is sales approved yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Is_sd_team_approved</fullName>
        <field>Is_senior_delivery_approved__c</field>
        <literalValue>1</literalValue>
        <name>Is sd team approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Make_Feedback_1Reversed</fullName>
        <field>Feedback__c</field>
        <literalValue>Rejected</literalValue>
        <name>Make Feedback Reversed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Make_Feedback_Reversed</fullName>
        <field>Feedback__c</field>
        <literalValue>Rejected</literalValue>
        <name>Make Feedback Reversed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Make_status_cancell</fullName>
        <field>Feedback__c</field>
        <literalValue>Rejected</literalValue>
        <name>Make Feedback Reversed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Owner</fullName>
        <field>Opportunity_Email__c</field>
        <formula>Opportunity__r.Owner.Email</formula>
        <name>Opportunity Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Owner_Notification</fullName>
        <field>Opportunity_Email__c</field>
        <formula>Opportunity__r.Owner.Email</formula>
        <name>Opportunity Owner Notification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Autralia</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_Australia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Autralia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Pre_prov_emea</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Service_delivery_EMEA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Pre prov emea</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_SD_1</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_US</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner SD 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_SD_4</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_South_Asia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner SD 4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_South_Ease_ASIA</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Sales_Manager_North_Asia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner South Ease ASIA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_To_RSP_ASIA</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Sales_Manager_GW_RSP_INDIA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner To RSP ASIA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_To_US</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Sales_Manager_US</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner To US</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_assignment</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Service_delivery_Australia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_pre_prov_Asia</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Service_delivery_Asia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner pre prov Asia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_to_EMEA</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Sales_Manager_EMEA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner to EMEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_to_GW_ESP</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Sales_Manager_GW_ESP</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner to GW ESP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_to_GW_GSP</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Sales_Manager_GW_GSP</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner to GW GSP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_to_India</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Sales_Manager_INDIA_MNC</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner to India</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_to_Japan</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Sales_Manager_Japan_Korea_MNC</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner to Japan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_to_MNO</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Sales_Manager_GW_MNO</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner to MNO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_to_OTT</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Sales_Manager_GW_OTT</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner to OTT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_to_RSP_EMEA</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Sales_Manager_GW_RSP_EMEA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner to RSP EMEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_to_china</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Sales_Manager_China_MNC</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner to china</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_to_hongkong</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Sales_Manager_Hongkong_taiwan_MNC</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner to hongkong</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_to_other</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Sales_Manager_GW_Other</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner to other</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_us_preprov</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Service_delivery_US</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner us preprov</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_type_assignment</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Request_Pre_Provisioning</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record type assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Billing_Information</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Request_Billing_Information</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Request Billing Information</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Sales_Support_US2</fullName>
        <field>Region__c</field>
        <literalValue>US</literalValue>
        <name>Request Sales Support US2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_to_ETC_AI_for_SU</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Request_To_Calculate_ETC_SalesUser</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Request to ETC AI for SU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>STatus_Update</fullName>
        <field>Status__c</field>
        <literalValue>Completed</literalValue>
        <name>STatus Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>STatus_Update_1</fullName>
        <field>Status__c</field>
        <literalValue>Completed</literalValue>
        <name>STatus Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SalesAdminAsia_Request</fullName>
        <field>OwnerId</field>
        <lookupValue>Sales_Admin_Asia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SalesAdminAsia-Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SalesAdminAsia_Request2</fullName>
        <field>Region__c</field>
        <literalValue>Asia</literalValue>
        <name>SalesAdminAsia-Request2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SalesAdminAus_Request</fullName>
        <field>OwnerId</field>
        <lookupValue>Sales_Admin_Asia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SalesAdminAus-Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SalesAdminEMEA_Request</fullName>
        <field>OwnerId</field>
        <lookupValue>Sales_Admin_EMEA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SalesAdminEMEA-Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SalesAdminUS_Request</fullName>
        <field>OwnerId</field>
        <lookupValue>Sales_Admin_US</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SalesAdminUS-Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Admin_Australia2</fullName>
        <field>Region__c</field>
        <literalValue>Australia</literalValue>
        <name>Sales Admin Australia2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Admin_EMEA2</fullName>
        <field>Region__c</field>
        <literalValue>EMEA</literalValue>
        <name>Sales Admin EMEA2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Manager_Email</fullName>
        <field>Sales_manager_Email__c</field>
        <formula>LastModifiedBy.Email</formula>
        <name>Sales Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Order_Desk_Approval_Date</fullName>
        <field>Order_Desk_Approval_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Order Desk Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_reject</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status reject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_rejects</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status reject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Competed_Date</fullName>
        <field>Completion_Date__c</field>
        <formula>Today()</formula>
        <name>Update Competed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Completed_By</fullName>
        <field>Completed_By__c</field>
        <formula>$User.FirstName+&apos; &apos;+ $User.LastName</formula>
        <name>Update Completed By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ETC_Checkbox</fullName>
        <description>Update the checkbox to false once the record type is flipped</description>
        <field>ETC_Checkbox__c</field>
        <literalValue>0</literalValue>
        <name>Update ETC Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Account_Admin_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>owner</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Sales_Manager_Australia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>owner_SD_2</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_EMEA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>owner SD 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>owner_SD_3</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_North_Asia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>owner SD 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>owner_sd</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_Australia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>owner sd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>owner_to_sales</fullName>
        <field>OwnerId</field>
        <lookupValue>Senior_Sales_Manager_Australia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>owner to sales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>queue_name_update</fullName>
        <field>Queue_Name__c</field>
        <formula>$User.Alias</formula>
        <name>queue name update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>queue_name_updated</fullName>
        <field>Queue_Name__c</field>
        <formula>&quot;ayushyaanshu&quot;</formula>
        <name>queue name updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>New_Logo_approved_date</fullName>
        <field>Completion_Date__c</field>
        <formula>Today()</formula>
        <name>New Logo approved date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>New_Logo_created_user_to_update</fullName>
        <field>Completed_By__c</field>
        <formula>$User.FirstName+&apos; &apos;+ $User.LastName</formula>
        <name>New Logo created user to update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>	
	<rules>
        <fullName>Order Configuration Action Item_Status_Assigned</fullName>
        <actions>
            <name>Order_Configuration_Action_Item_Status_Assigned</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assigned_User_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Status_Assigned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow -- when the status of Order Support Action Item to “Assigned” and send the email notification to the members of the Queue.</description>
        <formula>AND(RecordType.Name=&apos;Request Order Support&apos;, NOT(ISBLANK(Assigned_To_OD__c)),ISCHANGED(Assigned_To_OD__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>email for opportunity owner</fullName>
        <actions>
            <name>email_for_opp_owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Request Credit Check</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>	
    <rules>
        <fullName>Order Support AI is Approved</fullName>
        <actions>
            <name>Order_Support_Action_Item_to_Approved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND
( 
RecordType.Name=&apos;Request Order Support&apos;, ISCHANGED(Status__c),


ISPICKVAL(Status__c , &quot;In Progress&quot;) 

)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Pricing Validation Action Item_Assigned</fullName>
        <actions>
            <name>Pricing_Validation_Action_Item_Assigned_To</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>OrderconfAI_Assigned_User_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Pricing_ValidationStatus_Assigned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 
RecordType.Name=&apos;Pricing Validation&apos;, NOT(ISBLANK(Assigned_To__c)), ISCHANGED(Assigned_To__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Action Item Completed</fullName>
        <actions>
            <name>Action_Item_Closed</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Competed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Completed_By</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>notEqual</operation>
            <value>Request to Calculate Early Termination Charge</value>
        </criteriaItems>
		<criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>notEqual</operation>
            <value>Request Cross Country Application Approval</value>
        </criteriaItems>
		<criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>notEqual</operation>
            <value>Request to Modify Opportunity Split</value>
        </criteriaItems>

        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>notEqual</operation>
            <value>Request Pre Provisioning</value>
        </criteriaItems>
        <description>When the action item is completed, then we need to log who closed the request and when.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Action Item Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>contains</operation>
            <value>In Progress,Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Request Credit Check,Request Feasibility Study,Request for Bill Profile Approval,Validate Opportunity,Pre Order Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>notEqual</operation>
            <value>Request Pre Provisioning</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Action_Item_has_not_been_Followed_Up</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Action_Item__c.Due_Date__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Action Item Reminder-BP</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>contains</operation>
            <value>In Progress,Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Request for Bill Profile Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Bill_Profile_Status__c</field>
            <operation>notEqual</operation>
            <value>Active</value>
        </criteriaItems>
		<criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Validate Opportunity,Pre Order Service</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Action_Item_has_not_been_Followed_Up</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Action_Item__c.Due_Date__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Action Item Reminder-FS%26CC</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>contains</operation>
            <value>In Progress,Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Request Credit Check,Request Feasibility Study</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Opportunity_Order_Type__c</field>
            <operation>notEqual</operation>
            <value>Termination</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Action_Item_has_not_been_Followed_Up</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Action_Item__c.Due_Date__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Action item owner SD team Aus</fullName>
        <actions>
            <name>Notification_to_SD_user</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>owner_sd</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>queue_name_updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>equals</operation>
            <value>Request Pre Provisioning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Created_By_Region__c</field>
            <operation>equals</operation>
            <value>Australia</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Action item owner SD teamEMEA</fullName>
        <actions>
            <name>Notification_to_SD_user</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>owner_SD_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>queue_name_updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>equals</operation>
            <value>Request Pre Provisioning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Is_senior_delivery_approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Created_By_Region__c</field>
            <operation>equals</operation>
            <value>EMEA</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Action item owner SD teamNAsia</fullName>
        <actions>
            <name>Notification_to_SD_user</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>owner_SD_3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>queue_name_updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>equals</operation>
            <value>Request Pre Provisioning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Is_senior_delivery_approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Created_By_Region__c</field>
            <operation>equals</operation>
            <value>North Asia</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Action item owner SD teamSAsia</fullName>
        <actions>
            <name>Notification_to_SD_user</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Owner_SD_4</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>queue_name_updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>equals</operation>
            <value>Request Pre Provisioning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Is_senior_delivery_approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Created_By_Region__c</field>
            <operation>equals</operation>
            <value>South Asia</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Action item owner SD teamUS</fullName>
        <actions>
            <name>Notification_to_SD_user</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Owner_SD_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>queue_name_updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>equals</operation>
            <value>Request Pre Provisioning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Is_senior_delivery_approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Created_By_Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Action item owner for pre prov</fullName>
        <actions>
            <name>Email_field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Owner_assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>queue_name_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>equals</operation>
            <value>Request Pre Provisioning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Is_senior_sales_approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Queue_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Created_By_Region__c</field>
            <operation>equals</operation>
            <value>Australia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Is_senior_delivery_approved__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Action item owner for pre provASIA</fullName>
        <actions>
            <name>Email_field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Owner_pre_prov_Asia</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>queue_name_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>equals</operation>
            <value>Request Pre Provisioning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Is_senior_sales_approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Queue_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Created_By_Region__c</field>
            <operation>contains</operation>
            <value>Asia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Is_senior_delivery_approved__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Action item owner for pre provEMEA</fullName>
        <actions>
            <name>Email_field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Owner_Pre_prov_emea</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>queue_name_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>equals</operation>
            <value>Request Pre Provisioning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Is_senior_sales_approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Queue_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Created_By_Region__c</field>
            <operation>equals</operation>
            <value>EMEA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Is_senior_delivery_approved__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Action item owner for pre provUS</fullName>
        <actions>
            <name>Email_field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Owner_us_preprov</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>queue_name_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>equals</operation>
            <value>Request Pre Provisioning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Is_senior_sales_approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Queue_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Created_By_Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Is_senior_delivery_approved__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign Carrier Quote to Carrier Quote GP</fullName>
        <actions>
            <name>Action_Item_Assigned_Re_assigned</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_Carrier_Quote_to_Carrier_Quote_GP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Request Carrier Quote</value>
        </criteriaItems>
        <description>This rules assign the action to carrier quote group for carrier quote</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign Contract Approval to Legal</fullName>
        <actions>
            <name>Action_Item_Assigned_Re_assigned</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_Contract_Approval_to_Legal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Request Contract Approval</value>
        </criteriaItems>
        <description>This rules assign the action to Legal for Contract Approval.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign Credit Check to Credit Control</fullName>
        <actions>
            <name>Action_Item_Assigned_to_Credit_control_Team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Request Credit Check</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Opportunity_Order_Type__c</field>
            <operation>notEqual</operation>
            <value>Termination</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>notEqual</operation>
            <value>Request Pre Provisioning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Opportunity_Order_Type__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This rules assign the action to credit control team for credit check</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign Feasibility to Tech Consulting</fullName>
        <actions>
            <name>Action_Item_Assigned_Re_assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Request Feasibility Study</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Opportunity_Order_Type__c</field>
            <operation>notEqual</operation>
            <value>Termination</value>
        </criteriaItems>
        <description>This rules assign the action to tech consulting team for feasibility check and Third Party Quote</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign Missing Service to Service Delivery</fullName>
        <actions>
            <name>Assign_Missing_Service_to_Service_Delivery</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Assign Missing Service to Service Delivery</description>
        <formula>AND( ISPICKVAL( Status__c , &apos;Assigned&apos;),  RecordType.Name ==&apos;Request_Service_Details&apos;, Subject__c == $Label.SERVICE_NOT_PROPERLY_ASSOCIATED)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign Pricing Approval to Pricing</fullName>
        <actions>
            <name>Action_Item_Assigned_Re_assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Request Pricing Approval</value>
        </criteriaItems>
        <description>This rules assign the pricing approval to pricing team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign Service to Service Delivery</fullName>
        <actions>
            <name>Service_Action_Item_Assigned_Reassigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This Rule assigns Service to Service delivery.</description>
        <formula>AND( ISPICKVAL( Status__c , &apos;Assigned&apos;),  RecordType.Name ==&apos;Request_Service_Details&apos;,Subject__c =  $Label.REQUEST_SERVICE_DETAILS )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Bill Profile Approval Request</fullName>
        <actions>
            <name>Action_Item_Assigned_Re_assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Request for Bill Profile Approval</value>
        </criteriaItems>
        <description>Request for Bill Profile Approval</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email to Billing User after Commercial User edits ETC AI</fullName>
        <actions>
            <name>Email_to_Billing_User_the_information_filled_in_by_the_Commercial_User</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow to send an email alert to the Billing User containing the details of the Waiver Request and the New ETC that is filled by the Commercial User</description>
        <formula>Subject__c = &apos;Request to Calculate Early Termination Charge&apos; &amp;&amp; ISPICKVAL(Status__c,&apos;Completed&apos;) &amp;&amp; ISPICKVAL( PRIORVALUE( Status__c ) ,&apos;Commercial User in Progress&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Conversion Required</fullName>
        <actions>
            <name>Action_Item_Assigned_Re_assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>equals</operation>
            <value>Lead Conversion Required</value>
        </criteriaItems>
        <description>Lead Conversion Required</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notification on Owner Change</fullName>
        <actions>
            <name>Notify_Action_Item_Creator</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Notify_Action_Item_OppOwner</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Opportunity_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(if (AND(ISCHANGED( OwnerId ),  Opportunity__c!=null ), true, false),RecordType.Name !=&apos;Request to Modify Opportunity Split&apos;,RecordType.Name !=&apos;Pre Order Service&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notification on Owner%2FAssigned To Changes</fullName>
        <actions>
            <name>Alert_the_Assigned_To_User</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_Email_to_AI_Creator</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(Assigned_To__c),RecordType.Name=&apos;Request Pricing Approval&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Opp Owner On Pricing Approval</fullName>
        <actions>
            <name>NotifyOppOwnerOnPricingApproval</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Opportunity_Owner_Notification</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Request Pricing Approval</value>
        </criteriaItems>
        <description>TGME0020836 : SFDC - Email Notification to Opp Owner once pricing approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
   
    <rules>
        <fullName>Notify Request Contract</fullName>
        <actions>
            <name>Action_Item_Assigned_Re_assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Request Contract</value>
        </criteriaItems>
        <description>This rules assign the action to Legal for Contract Approval.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pre provisioning record type</fullName>
        <actions>
            <name>Record_type_assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>equals</operation>
            <value>Request Pre Provisioning</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Request Account Admin Support</fullName>
        <actions>
            <name>Copy_Subject6</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Owner_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Request Account Support</value>
        </criteriaItems>
        <description>This has been created by PSM on TGME0020957 to separate AI created on Account for Request Sales Support from that on Opportunity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Request For Account  Activation</fullName>
        <actions>
            <name>Action_Item_Assigned_Re_assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>equals</operation>
            <value>Account Activation Required</value>
        </criteriaItems>
        <description>Request For Account  Activation</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Request For Billing Information</fullName>
        <actions>
            <name>Request_Billing_Information</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>contains</operation>
            <value>Request For Billing Information</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Assigned</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    
    <rules>
        <fullName>Request Sales Support US</fullName>
        <actions>
            <name>Email_Notification_US</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Copy_Subject3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Request_Sales_Support_US2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SalesAdminUS_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>contains</operation>
            <value>Request Quote Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Request Service Delivery to Provision</fullName>
        <actions>
            <name>Service_Provisioning_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Request Service Delivery to Provision the Service</description>
        <formula>AND( ISPICKVAL( Status__c , &apos;Assigned&apos;),  RecordType.Name ==&apos;Request Service Details&apos;, Subject__c == $Label.REQUEST_SERVICE_PROVISIONING_SUBJECT)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Request to ETC Action Item</fullName>
        <actions>
            <name>Request_to_ETC_AI_for_SU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_ETC_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISPICKVAL(Status__c,&apos;Sales User in Progress&apos;), ETC_Checkbox__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Request to ETC Action Item For Billing Team</fullName>
        <actions>
            <name>Notify_to_Billing_Team_for_Request_to_ETC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>equals</operation>
            <value>Request to Calculate Early Termination Charge</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Billing User in Progress</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Request to ETC Action Item For CommercialTeam</fullName>
        <actions>
            <name>Request_to_ETC_Notification_to_Commercial_team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>equals</operation>
            <value>Request to Calculate Early Termination Charge</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Commercial User in Progress</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Request to ETC Action Item for Order Creator</fullName>
        <actions>
            <name>Request_to_ETC_Notification_for_Order_Creator</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>notEqual</operation>
            <value>Request Pre Provisioning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Subject__c</field>
            <operation>equals</operation>
            <value>Request to Calculate Early Termination Charge</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stage Gate Review Notification</fullName>
        <actions>
            <name>Action_Item_Assigned_Re_assigned</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Action_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Stage Gate Review Request</value>
        </criteriaItems>
        <description>Send a notification to the opportunity creator for the assignment of stage gate review action item</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
	
	
	
	<rules>
        <fullName>New Logo created user to update</fullName>
        <actions>
            <name>New_Logo_approved_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>New_Logo_created_user_to_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(OR($Profile.Name == &apos;System Administrator&apos;,$Profile.Name == &apos;TI Sales Admin&apos;,$Profile.Name == &apos;TI HR&apos;, $Profile.Name == &apos;TI Sales Manager&apos;), OR(RecordType.Name == &apos;New Logo for Manual Process&apos;, RecordType.Name == &apos;Win Back for Manual Process&apos;, RecordType.Name == &apos;New Logo&apos;, RecordType.Name == &apos;Win Back&apos;), OR(ISPICKVAL(Status__c, &apos;Approved&apos;), ISPICKVAL(Status__c, &apos;Rejected&apos;)), OR(ISPICKVAL(Subject_For_NewLogo__c,&apos;New Customer&apos;),ISPICKVAL(Subject_For_NewLogo__c,&apos;Win Back Customer&apos;), Subject__c == &apos;New Customer&apos;, Subject__c == &apos;Win Back Customer&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
	<rules>
        <fullName>Validate Oppo</fullName>
        <actions>
            <name>Email_alert_to_Maestro_team</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Owner_assignment_Validate_Opp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Validate Opportunity</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Validate Oppo_Asignedto</fullName>
        <actions>
            <name>Validate_Oppo_Asignedto</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Validate Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Assigned_To_VO__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
	
	<rules>
        <fullName>Updating the subject</fullName>
        <actions>
            <name>Updating_the_subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pre Order Service</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
	<rules>
        <fullName>Email alert on changing owner</fullName>
        <actions>
            <name>Email_alert_to_Pre_order</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(OwnerId), RecordType.DeveloperName ='Pre_Order_Service')</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
	
	<rules>
        <fullName>Pre order service Assigned to</fullName>
        <actions>
            <name>Email_alert_to_Maestro_team_assigned_to_pre_order</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pre Order Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Assigned_To_VO__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Request Sales Support EMEA</fullName>
        <actions>
            <name>Email_Notification_EMEA</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Copy_Subject2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SalesAdminEMEA_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sales_Admin_EMEA2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>contains</operation>
            <value>Request Quote Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Account_Owner_Region__c</field>
            <operation>equals</operation>
            <value>EMEA</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Request Sales Support North Asia</fullName>
        <actions>
            <name>Email_Notification_Asia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Copy_Subject1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SalesAdminAsia_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SalesAdminAsia_Request2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Action_Item__c.RecordTypeId</field>
            <operation>contains</operation>
            <value>Request Quote Support,Request Order Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Action_Item__c.Account_Owner_Region__c</field>
            <operation>equals</operation>
            <value>North Asia</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    
</Workflow>
