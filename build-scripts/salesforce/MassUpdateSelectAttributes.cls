//List<cscfga__Attribute_Definition__c> contractTermDefs = [select Id, Name, (select Id, Name, cscfga__Value__c, cscfga__Sequence__c, csexpimp1__guid__c from cscfga__Select_Options__r) from cscfga__Attribute_Definition__c where cscfga__reference_name__c = 'Contract_Term' and cscfga__Product_Definition__r.cscfga__Product_Category__c in ('a23O0000000s2u7','a23O0000000s27Z')];
List<cscfga__Attribute_Definition__c> contractTermDefs = [select Id, Name, (select Id, Name, cscfga__Value__c, cscfga__Sequence__c, csexpimp1__guid__c from cscfga__Select_Options__r) from cscfga__Attribute_Definition__c where Id = 'a1YO0000002Oeru'];
Map<Id, OptionsWrapper> definitionMap = new Map<Id, OptionsWrapper>();

for(cscfga__Attribute_Definition__c def : contractTermDefs){
    if(!definitionMap.containsKey(def.Id)){
        definitionMap.put(def.Id, new OptionsWrapper(def));
    }
    
    for(cscfga__Select_Option__c opt : def.cscfga__Select_Options__r){
        if('1'.equals(opt.Name) || '1 Month'.equals(opt.Name)){
            opt.Name = '1 Month';
            opt.cscfga__Value__c = '1';
            opt.cscfga__Sequence__c = 1;
            
            definitionMap.get(def.Id).monthOne = opt;
       	}
        else if('6'.equals(opt.Name) || '6 Months'.equals(opt.Name)){
            opt.Name = '6 Months';
            opt.cscfga__Value__c = '6';
            opt.cscfga__Sequence__c = 2;
            
            definitionMap.get(def.Id).monthSix = opt;
       	}
        else if('12'.equals(opt.Name) || '12 Months'.equals(opt.Name)){
            opt.Name = '12 Months';
            opt.cscfga__Value__c = '12';
            opt.cscfga__Sequence__c = 3;
            
            definitionMap.get(def.Id).monthTwelve = opt;
       	}
        else if('24'.equals(opt.Name) || '24 Months'.equals(opt.Name)){
            opt.Name = '24 Months';
            opt.cscfga__Value__c = '24';
            opt.cscfga__Sequence__c = 4;
            
            definitionMap.get(def.Id).monthTwentyFour = opt;
       	}
        else if('36'.equals(opt.Name) || '36 Months'.equals(opt.Name)){
            opt.Name = '36 Months';
            opt.cscfga__Value__c = '36';
            opt.cscfga__Sequence__c = 5;
            
            definitionMap.get(def.Id).monthThirtySix = opt;
       	}
        else if('48'.equals(opt.Name) || '48 Months'.equals(opt.Name)){
            opt.Name = '48 Months';
            opt.cscfga__Value__c = '48';
            opt.cscfga__Sequence__c = 6;
            
            definitionMap.get(def.Id).monthFortyEight = opt;
       	}
        else if('60'.equals(opt.Name) || '60 Months'.equals(opt.Name)){
            opt.Name = '60 Months';
            opt.cscfga__Value__c = '60';
            opt.cscfga__Sequence__c = 7;
            
            definitionMap.get(def.Id).monthSixty = opt;
       	}
        else if('Other'.equals(opt.Name) || 'Other'.equals(opt.Name)){
            opt.Name = 'Other';
            opt.cscfga__Value__c = 'Other';
            opt.cscfga__Sequence__c = 8;
            
            definitionMap.get(def.Id).monthOther = opt;
       	}
    }
}

List<cscfga__Select_Option__c> optionsToUpsert = new List<cscfga__Select_Option__c>();
cscfga__Select_Option__c opt;
for(OptionsWrapper optWrapper : definitionMap.values()){
    if(optWrapper.monthOne == null){
        opt = new cscfga__Select_Option__c();
        opt.Name = '1 Month';
        opt.cscfga__Value__c = '1';
        opt.cscfga__Attribute_Definition__c = optWrapper.definition.Id;
        opt.cscfga__Sequence__c = 1;
        opt.csexpimp1__guid__c = GuidUtil.NewGuid();
        optWrapper.monthOne = opt;
    }
    if(optWrapper.monthSix == null){
        opt = new cscfga__Select_Option__c();
        opt.Name = '6 Months';
        opt.cscfga__Value__c = '6';
        opt.cscfga__Attribute_Definition__c = optWrapper.definition.Id;
        opt.cscfga__Sequence__c = 2;
        opt.csexpimp1__guid__c = GuidUtil.NewGuid();
        optWrapper.monthSix = opt;
    }
    if(optWrapper.monthTwelve == null){
        opt = new cscfga__Select_Option__c();
        opt.Name = '12 Months';
        opt.cscfga__Value__c = '12';
        opt.cscfga__Attribute_Definition__c = optWrapper.definition.Id;
        opt.cscfga__Sequence__c = 3;
        opt.csexpimp1__guid__c = GuidUtil.NewGuid();
        optWrapper.monthTwelve = opt;
    }
    if(optWrapper.monthTwentyFour == null){
        opt = new cscfga__Select_Option__c();
        opt.Name = '24 Months';
        opt.cscfga__Value__c = '24';
        opt.cscfga__Attribute_Definition__c = optWrapper.definition.Id;
        opt.cscfga__Sequence__c = 4;
        opt.csexpimp1__guid__c = GuidUtil.NewGuid();
        optWrapper.monthTwentyFour = opt;
    }
    if(optWrapper.monthThirtySix == null){
        opt = new cscfga__Select_Option__c();
        opt.Name = '36 Months';
        opt.cscfga__Value__c = '36';
        opt.cscfga__Attribute_Definition__c = optWrapper.definition.Id;
        opt.cscfga__Sequence__c = 5;
        opt.csexpimp1__guid__c = GuidUtil.NewGuid();
        optWrapper.monthThirtySix = opt;
    }
    if(optWrapper.monthFortyEight == null){
        opt = new cscfga__Select_Option__c();
        opt.Name = '48 Months';
        opt.cscfga__Value__c = '48';
        opt.cscfga__Attribute_Definition__c = optWrapper.definition.Id;
        opt.cscfga__Sequence__c = 6;
        opt.csexpimp1__guid__c = GuidUtil.NewGuid();
        optWrapper.monthFortyEight = opt;
    }
    if(optWrapper.monthSixty == null){
        opt = new cscfga__Select_Option__c();
        opt.Name = '60 Months';
        opt.cscfga__Value__c = '60';
        opt.cscfga__Attribute_Definition__c = optWrapper.definition.Id;
        opt.cscfga__Sequence__c = 7;
        opt.csexpimp1__guid__c = GuidUtil.NewGuid();
        optWrapper.monthSixty = opt;
    }
    if(optWrapper.monthOther == null){
        opt = new cscfga__Select_Option__c();
        opt.Name = 'Other';
        opt.cscfga__Value__c = 'Other';
        opt.cscfga__Attribute_Definition__c = optWrapper.definition.Id;
        opt.cscfga__Sequence__c = 8;
        opt.csexpimp1__guid__c = GuidUtil.NewGuid();
        optWrapper.monthOther = opt;
    }
    
    optionsToUpsert.addAll(optWrapper.getAllOptions());
}

upsert optionsToUpsert;

global class GuidUtil {

    private static String kHexChars = '0123456789abcdef';

    global static String NewGuid() {

        String returnValue = '';
        Integer nextByte = 0;

        for (Integer i=0; i<16; i++) {

            if (i==4 || i==6 || i==8 || i==10) 
                returnValue += '-';

            nextByte = (Math.round(Math.random() * 255)-128) & 255;

            if (i==6) {
                nextByte = nextByte & 15;
                nextByte = nextByte | (4 << 4);
            }

            if (i==8) {
                nextByte = nextByte & 63;
                nextByte = nextByte | 128;
            }

            returnValue += StringUtils.getCharAtIndex(kHexChars, nextByte >> 4);
            returnValue += StringUtils.getCharAtIndex(kHexChars, nextByte & 15);
        }

        return returnValue;
    }

    global static String getCharAtIndex(String str, Integer index) {

        if (str == null) return null;

        if (str.length() <= 0) return str;    

        if (index == str.length()) return null;    

        return str.substring(index, index+1);
    }
}

class OptionsWrapper{
    public cscfga__Attribute_Definition__c definition;
    public cscfga__Select_Option__c monthOne;
    public cscfga__Select_Option__c monthSix;
    public cscfga__Select_Option__c monthTwelve;
    public cscfga__Select_Option__c monthTwentyFour;
    public cscfga__Select_Option__c monthThirtySix;
    public cscfga__Select_Option__c monthFortyEight;
    public cscfga__Select_Option__c monthSixty;
    public cscfga__Select_Option__c monthOther;
    
    public OptionsWrapper(cscfga__Attribute_Definition__c definition){
        this.definition = definition;
    }
    
    public List<cscfga__Select_Option__c> getAllOptions(){
        return new list<cscfga__Select_Option__c>{monthOne, monthSix, monthTwelve, monthTwentyFour, monthThirtySix, monthFortyEight, monthSixty, monthOther};
    }     
}