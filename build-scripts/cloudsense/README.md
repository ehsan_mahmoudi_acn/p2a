# Clousense automation utilities

# Prerequisite

The following tools needs to be installed on your system to be able to run the scripts :

 * [PhantomJS](http://phantomjs.org/)
 * [CasperJS](http://casperjs.org/)

Please refer to the tools documentation to get their installation procedure.

# The scripts

To get help for a specific script, use the following command:

```
casperjs <SCRIPT_FILE> --usage
```

 - **compile_product_uis.js** : Compiles the UI template for a given product definition


