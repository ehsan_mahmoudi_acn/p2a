/* jshint strict: true, phantom: true */

/*
 *  Usage: casperjs compile_product_uis.js [OPTIONS] id1 id2 id3
 *  Options:
 *    --username=USERNAME : sets the username for login (defaults to SFDC_USERNAME env variable)
 *    --password=PASSWORD : sets the password for login (defaults to SFDC_PASSWORD env variable)
 *    --host=HOST         : sets the host for login (defaults to test.salesforce.com)
 */

  'use strict';
(function() {

  var system = require('system');
  var casper = require('casper').create();
  //casper.options.logLevel = 'info';
  //casper.options.verbose = true;
  casper.options.waitTimeout = 30000;
  casper.options.pageSettings.loadImages = false;

  casper.on('error', function (msg, backtrace) {
    this.die('Exiting due to unhandled error: ' + msg );
  });

  var usage = [
      'Usage: casperjs compile_product_uis.js [OPTIONS] id1 id2 id3...',
      'Options:',
      '  --usage             : print this usage info and exit',
      '  --username=USERNAME : username for login (defaults to SFDC_USERNAME env variable)',
      '  --password=PASSWORD : password for login (defaults to SFDC_PASSWORD env variable)',
      '  --host=HOST         : host for login (defaults to test.salesforce.com)',
      '  --template=TEMPLATE : name of the template to use (defaults to "cscfga__StandardOnlineTemplate")'
    ].join('\n'); 

  var login = function(username, password, sfdcHost) {
    return function() {
      this.echo('Logging in... username="' + username + '"');
      this.thenOpen('https://' + sfdcHost, function() {
        this.fillSelectors('form#login_form', {
          'input#username': username,
          'input#password': password
        }, true);
        this.waitForUrl(/home\.jsp$/, function() {
          this.echo('Login Successful username="' + username + '"', "INFO");
        });
      });
    };
  };

  var extractHostname= function(uri) {
    var regex = /^http[s]?:\/\/([^\/\.]+)\..*\/?/;
    var matches = regex.exec(uri);
    return matches[1];
  };

  var compile = function(id, templateName) {
    return function() {
      this.echo('Starting compilation id="' + id + '" template="' + templateName + '" status="starting"');
      var hostname = extractHostname(this.getCurrentUrl());
      this.thenOpen('https://cscfga.' + hostname + '.visual.force.com/apex/CompileAllDefinitionUIs?id=' + id + '&template=' + templateName, function() {
          var compComplete = new RegExp('\/' + id + '.{0,3}$');

          this.waitForUrl(
              compComplete,
              function() { this.echo('Compilation finished. id="' + id + '" template="' + templateName + '" status="success"', "INFO"); },
              function() { this.echo('Compilation timeout id="' + id + '" template="' + templateName + '" status="failure"', "WARNING"); }
         );
      });
    };
  };

  var ids = casper.cli.args;
  // set default values
  var sfdcUsername = system.env['SFDC_USERNAME'];
  var sfdcPassword = system.env['SFDC_PASSWORD'];
  var sfdcHost = 'test.salesforce.com';
  var templateName = 'cscfga__StandardOnlineTemplate';

  // override with commandline values if provided
  var cliOptions = casper.cli.options;
  if (cliOptions.username !== undefined) {
    sfdcUsername = cliOptions.username;
  }
  if (cliOptions.password !== undefined) {
    sfdcPassword = cliOptions.password;
  }
  if (cliOptions.host !== undefined) {
    sfdcHost = cliOptions.host;
  }
  if (cliOptions.template !== undefined) {
    templateName = cliOptions.template;
  }

  // if help was requested, print usage and exit
  if (casper.cli.options.usage === true) {
    casper.echo(usage);
    casper.exit(0);
    return;
  }

  if (ids.length === 0) {
    casper.echo('No id provided, exiting....', 1);
    casper.exit(1);
    return;
  }

  casper.start();
  casper.then(function() { this.echo('About to start...'); });

  casper.then(login(sfdcUsername, sfdcPassword, sfdcHost));
  for (var i = 0; i < ids.length; i++) {
    casper.then(compile(ids[i], templateName));
  }

  casper.then(function() { casper.echo('Done.'); });
  casper.run();
})();
